/***************************************************************************

   error.h - error handling

 ---------------------------------------------------------------------------
   Author: PTM: PT/RP        Date: 14.05.13
   For   : PTM
   Compil: CCSv5.2     Target: non specific
   descr.: handles system errors
   Edit  :

 **************************************************************************/


#ifndef _H_ERR_
#define _H_ERR_

/* exported constant definitions */
/* ============================= */
/* ERROR PRIORITY concept :
   MAX priority errors = Serious hardware or software failures
   ===========================================================
   MAX priority errors cannot be overwritten, even not by another MAX priority
   MAX priority errors can overwrite MID and STD errors
   
   MID priority errors = Exception errors
   ======================================
   MID priority errors can only be overwritten by MAX priority errors
   MID priority errors can overwrite STD errors

   STD priority errors = Positionning & Safety errors
   ==================================================
   STD priority errors can be overwritten by any other error
   STD priority errors can overwrite STD errors
*/

                           /* priority level */
#define ERRPRIO_MAX    3   /* Maximum error priority */
#define ERRPRIO_MID    2   /* Medium error priority */
#define ERRPRIO_STD    1   /* Standard error priority */
#define ERRPRIO_NUL    0   /* Not an error state */

/* error numbers */
#define ERR_NOERROR       0x00 //
#define ERR_SELFTEST      0x01 // -- bit0 -- 01 00		// not used in excel
#define ERR_OVERCURRENT   0x02 // -- bit1 -- 02 00		// OK
#define ERR_OVERVOLTAGE   0x04 // -- bit2 -- 04 00		// OK
#define ERR_UNDERVOLTAGE  0x08 // -- bit3 -- 08 00		// OK
#define ERR_DIGOVLD       0x10 // -- bit4 -- 10 00		// OK not used in excel
#define ERR_TEMPERATURE   0x20 // -- bit5 -- 20 00		// OK
#define ERR_PARAMINIT     0x40 // -- bit6 -- 40 00 		/// not used in Excel sheet
#define ERR_COMM          0x80 // -- bit7 -- 80 00		// OK

#define ERR_ISO             			0x100  //-- bit8  -- 00 01
#define ERR_CONTACTOR_WELDED_PLUS       0x200  //-- bit9  -- 00 02
#define ERR_CONTACTOR_WELDED_NEGATIVE   0x400  //-- bit10 -- 00 04
#define ERR_CONTACTOR_PRECHARGE         0x800  //-- bit11 -- 00 08
#define Delta_ERR                       0x1000 //-- bit12 -- 00 10

/*
#define DUMMY             0x1000 //-- bit13
#define DUMMY             0x4000 //-- bit15
*/

#define WARN_CYCLE 			0x10000 // -- bit16 -- 00 01 ---bit0	// not used in excel
#define WARN_CURRENT 		0x20000 // -- bit17 -- 00 02 ---bit1
#define WARN_TEMPERATURE 	0x40000 // -- bit18 -- 00 04 ---bit2
#define WARN_CRC 			0x80000 // -- bit19 -- 00 08 ---bit3	// not used in excel
#define WARN_LOW_CAPACITY 	0x100000 // -- bit20 -- 00 10 ---bit4
#define WARN_OVERCAPACITY 	0x200000 // -- bit21 -- 00 20 ---bit5
#define WARN_ISO			0x400000 // -- bit22 -- 00 40 ---bit6	// not used in excel
#define WARN_CELLDELTA  	0x800000 // -- bit23 -- 00 80 ---bit7  standard Controller is for Delta cell voltage as there is no Insulation

//#define WARN_INSULATION    0x008000  -- bit16		/// change to warning 
/*
#define WARN_CYCLE        0x000100  -- bit9		// not used in excel
#define WARN_CURRENT      0x000200  -- bit10
#define WARN_TEMPERATURE  0x000400  -- bit11
#define WARN_CRC          0x000800  -- bit12		// not used in excel
#define WARN_LOW_CAPACITY 0x001000  -- bit13
#define WARN_OVERCAPACITY 0x002000  -- bit14
*/

//Module
#define ERR_MODULE  	0x80000000  // -- bi28 module1
#define ERR_MODULE_BIT  0x01000000  // -- bi25 module2
#define ERR_MODULE_MASK 0x7F000000  // -- bit?

/*
0x04000000 - 65536 -- bi19 module4
0x08000000 - 65536 -- bi20 module8
0x10000000 bit21 module16
0x20000000 - 65536 -- bi22 module32
0x40000000 - 65536 -- bi23 module64
0x7F000000 - 8323072 -- mask (module 127)
*/

//#define ERR_MODULE        0x004000  -- bit15
//#define ERR_MODULE_BIT    0x010000  -- bit17

/// free bits used for replacing ERR_DIGOVLD
//#define ERR_HVIL            			0x20000    // currently Inactive
//#define ERR_CONTACTOR_WELDED_PLUS  		0x40000  
//#define ERR_CONTACTOR_WELDED_NEGATIVE   0x80000  
//#define ERR_CONTACTOR_PRECHARGE   		0x100000 

//#define WARN_SLEEP        0x800000   // -- bit24  //auto sleep is activated

#define UNKNOWN_ERROR     99  /* Unknown error number            */

#define NO_WARNING          0
#define UNKNOWN_WARNING   255  /* Unknown warning number */

/*  Charger Error Declaration */
#define BULKS  			0x1
#define WARNINGHV		0x2
#define LIMTEMP			0x8
#define WARNLIMIT		0x20
#define ERRORLATCH		0x40
#define POWERENABLE     0x80
/* End of Charger Error Declaration */

/* exported functions */
/* ================== */
bool1  ERR_ClearError(void);
/* clear the ERROR output + led, the ERR_ErrorNumber and REG_Status.BError

   CLEAR ERROR RESTRICTION
   Some errors cannot be cleared or need special conditions to be cleared.
   If the error could not be cleared the function returns FALSE
   
   MAX priority errors cannot be cleared, except ERR_WATCHDOG when ClearWDog
   is set in SysConfig2
   
*/
void ChargerError(void);

void  ERR_HandleErrorNb(uint8 errornum);
/* for startup errors only : call the error function corresponding
   to the given error number */
void ERR_DisplayError(uint8 error);

void ERR_HandleWarning(uint32 warningnum);
/* display a warning message */
bool1  ERR_ClearWarnings(void);
/* clear the ODV_ErrorDsp_WarningNumber and REG_Status.BWarning */
void ERR_ClearWarning(uint32 warningno);
/* clear only specified warning */

/* these are the error functions */

/* serious hardware failures */
void ERR_ErrorDigOvld(void);
void ERR_ErrorParam(void);
void ERR_CONTACTOR_PLUS(void);
void ERR_ISOLATION(void);
void ERR_CONTACTOR_NEG(void);
void ERR_CONTACTOR_PRE(void);

/* emergency & safety errors */
#define OVERCUR_DETECT
#define TEMP_SENSOR

#ifdef OVERCUR_DETECT
void ERR_ErrorOverCurrent(void);
#endif
#ifdef TEMP_SENSOR
void ERR_ErrorOverTemp(void);
#endif
void ERR_ErrorComm(void);
void WARN_Insulation(void);
void ERR_ErrorOverVoltage(void);
void ERR_ErrorUnderVoltage(void);
void ERR_DELTA_Voltage(void);
void ERR_SetError(uint32 error_code);

/* exported variables */        /* R = READ ONLY   RW = READ AND WRITE */
/* ================== */
                                   

#endif /* !_H_ERR_ */
/*****************************************************/
/* END OF ERROR.H */

