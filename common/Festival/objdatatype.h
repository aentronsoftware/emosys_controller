#ifndef OBJDATATYPE_H_
#define OBJDATATYPE_H_

/************************* CONSTANTES **********************************/
/** this are static defined datatypes taken fCODE the canopen standard. They
 *  are located at index 0x0001 to 0x001B. As described in the standard, they
 *  are in the object dictionary for definition purpose only. a device does not
 *  to support all of this datatypes.
 */
#define DATA_TYPE_BOOLEAN         0x01
#define DATA_TYPE_INT8            0x02
#define DATA_TYPE_INT16           0x03
#define DATA_TYPE_INT32           0x04
#define DATA_TYPE_UINT8           0x05
#define DATA_TYPE_UINT16          0x06
#define DATA_TYPE_UINT32          0x07
#define DATA_TYPE_REAL32          0x08
#define DATA_TYPE_VISIBLE_STRING  0x09
#define DATA_TYPE_OCTET_STRING    0x0A
#define DATA_TYPE_UNICODE_STRING  0x0B
#define DATA_TYPE_TIME_OF_DAY     0x0C
#define DATA_TYPE_TIME_DIFFERENCE 0x0D
#define DATA_TYPE_DOMAIN          0x0F
#define DATA_TYPE_INT24           0x10
#define DATA_TYPE_REAL64          0x11
#define DATA_TYPE_INT40           0x12
#define DATA_TYPE_INT48           0x13
#define DATA_TYPE_INT56           0x14
#define DATA_TYPE_INT64           0x15
#define DATA_TYPE_UINT24          0x16
#define DATA_TYPE_UINT40          0x18
#define DATA_TYPE_UINT48          0x19
#define DATA_TYPE_UINT56          0x1A
#define DATA_TYPE_UINT64          0x1B
#define DATA_TYPE_UINT12          0x1C

#define SIZE_BOOLEAN      1
#define SIZE_INTEGER8     1
#define SIZE_INTEGER12    2
#define SIZE_INTEGER16    2  
#define SIZE_INTEGER32    4
#define SIZE_INTEGER40    5
#define SIZE_INTEGER48    6
#define SIZE_INTEGER56    7
#define SIZE_INTEGER64    8
#define SIZE_UNS8         1
#define SIZE_UNS12        2  
#define SIZE_UNS16        2  
#define SIZE_UNS24        3
#define SIZE_UNS32        4
#define SIZE_REAL32       4
#define SIZE_UNS40        5
#define SIZE_UNS48        6
#define SIZE_UNS56        7
#define SIZE_UNS64        8  


#define pdo_communication_parameter 0x20
#define pdo_mapping                 0x21
#define sdo_parameter               0x22
#define identity                    0x23

/* CanFestival is using 0x24 to 0xFF to define some types containing a 
 value range (See how it works in objdict.c)
 */


/** Each entry of the object dictionary can be READONLY (RO), READ/WRITE (RW),
 *  WRITE-ONLY (WO)
 */
#define RW     0x00  
#define WO     0x01
#define RO     0x02

#define TO_BE_SAVE  0x04
#define DCF_TO_SEND 0x08

#define DICT_COM  0
#define DICT_DSP  1
#define DICT_BOTH 2

#endif /*OBJDATATYPE_H_*/
