#include "gatewaycfg.h"
#include <clk.h>
#include <co_data.h>
#include <timer.h>  //SetAlarm
#include "ptmtype.h"

unsigned long TimeBuffer = 0;
unsigned long WaitTime = 0;
#define US_PER_TICK 1000U

void EnterMutex(void)
{
  SEM_pendBinary(&CanFestival_mutex, SYS_FOREVER);
}

void LeaveMutex(void)
{
  SEM_postBinary(&CanFestival_mutex);
}

#define maxval(a,b) ((a>b)?a:b)
void setTimer(TIMEVAL value)
{
  SEM_postBinary(&CanTimer_sem);			// continue timer_thread
}

void setTimer2(TIMEVAL value)
{
  WaitTime = maxval(value,1000)/US_PER_TICK;    // convert from microseconds to system clock ticks
  TimeBuffer = CLK_getltime();      // time in ticks
}

TIMEVAL getElapsedTime(void)
{
  unsigned long tmp;

  tmp = (CLK_getltime() - TimeBuffer);
  return tmp * US_PER_TICK;			/* 1 ticks/ms this convert time in ticks to us */
}

void timer_thread(void)
{
  TimeBuffer = CLK_getltime();      // time in cpu ticks
  while(1)
  {
    SEM_pendBinary(&CanTimer_sem, WaitTime);
    EnterMutex();
    TimeDispatch();
    LeaveMutex();
  }
}

