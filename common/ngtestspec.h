/*
 * ngtestspec.h
 *
 *  Created on: 14 mai 2012
 *      Author: remip
 */

#ifndef NGTESTSPEC_H_
#define NGTESTSPEC_H_

#define PTM060_1AA 60111


/* >>> select the hardware revision ! */
#define HW_REV PTM060_1AA

#define PAR_INITCRC 1479920862


typedef union{
  uint32 conf;
  struct{
    uint32 NotEn3V3A    :1; //bit0
    uint32 NotEn3V3B    :1; //bit1
    uint32 VDinCalUp    :1; //bit2
    uint32 VDinCalDw    :1; //bit3
    uint32 NotLedPwr    :1; //bit4
    uint32 NotLedErr    :1; //bit5
    uint32 Mux1_A0      :1; //bit6
    uint32 Mux2_A0      :1; //bit7
    uint32 Mux3_A0      :1; //bit8
    uint32 Mux4_A0      :1; //bit9
    uint32 unused       :22; //bit10..31
  }bit;
}TPicolloConfig;

typedef union{
  uint64 conf;
  struct{
    uint64 Gain1    :2; //bit0-1
    uint64 Gain2    :2; //bit
    uint64 Gain3    :2; //bit4-5
    uint64 Gain4    :2; //bit
    uint64 Gain5    :2; //bit8-9
    uint64 Gain6    :2; //bit
    uint64 Gain7    :2; //bit12-13
    uint64 Gain8    :2; //bit
    uint64 Gain9    :2; //bit16-17
    uint64 Gain10    :2; //bit
    uint64 Gain11    :2; //bit
    uint64 Gain12    :2; //bit
    uint64 Gain13    :2; //bit
    uint64 Gain14    :2; //bit26-27
    uint64 AoutA     :3; //bit28-30
    uint64 EnDiffIO1 :1; //bit31
    uint64 EnDiffIO2 :1; //bit
    uint64 EnDiffIO3 :1; //bit
    uint64 EnDiffIO4 :1; //bit
    uint64 EnDiffIO5 :1; //bit
    uint64 EnDiffIO6 :1; //bit
    uint64 EnDiffIO7 :1; //bit
    uint64 EnDiffIO8 :1; //bit
    uint64 EnDiffIO9 :1; //bit39
    uint64 unused       :24; //bit40..63
  }bit;
}TStellarisConfig;

typedef union{
  uint16 all;
  struct{
    uint16 Source1    :1; //bit0
    uint16 Source2    :1; //bit1
    uint16 Source3    :1; //bit2
    uint16 Source4    :1; //bit3
    uint16 Source5    :1; //bit4
    uint16 Source6    :1; //bit5
    uint16 Source7    :1; //bit6
    uint16 Source8    :1; //bit7
    uint16 Source9    :1; //bit8
    uint16 Source10   :1; //bit9
    uint16 Source11   :1; //bit10
    uint16 Source12   :1; //bit11
    uint16 Source13   :1; //bit12
    uint16 Source14   :1; //bit13
    uint16 Source15   :1; //bit14
    uint16 Source16   :1; //bit15
  }bit;
}TFlags;


#endif /* NGTESTSPEC_H_ */
