/**********************************************************************

   ptmtype.h - typedef description file

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: define variable types and bitfields
   Edit  :

 *********************************************************************/


#ifndef PTMTYPE_H_
#define PTMTYPE_H_

/* Integers */
#define int8  char
#define int12 short
#ifndef int16
  #define int16 short
#endif

#define int24 long
#ifndef int32
  #define int32 long
#endif
#define int40 long long
#define int48 long long
#define int56 long long
#ifndef int64
  #define int64 long long
#endif

/* Unsigned integers */
#ifndef TRUE
  #define TRUE              1
#endif

#ifndef FALSE
  #define FALSE             0
#endif

#define bool1   unsigned char
#define uint8   unsigned char
#define uint12  unsigned short
#define uint16  unsigned short
#define uint32  unsigned long
#define uint24  unsigned long
#define uint40  unsigned long long
#define uint48  unsigned long long
#define uint56  unsigned long long
#define uint64  unsigned long long

/* Reals */
#define real32  float
#define real64 double

/* Arrays */
typedef uint8 T2uint8[2];
typedef uint8 T4uint8[4];

/* Bits structure*/
typedef struct {
  uint8 b0:1;
  uint8 b1:1;
  uint8 b2:1;
  uint8 b3:1;
  uint8 b4:1;
  uint8 b5:1;
  uint8 b6:1;
  uint8 b7:1;
}bits8;

typedef struct {
  uint16 b0:1;
  uint16 b1:1;
  uint16 b2:1;
  uint16 b3:1;
  uint16 b4:1;
  uint16 b5:1;
  uint16 b6:1;
  uint16 b7:1;
  uint16 b8:1;
  uint16 b9:1;
  uint16 b10:1;
  uint16 b11:1;
  uint16 b12:1;
  uint16 b13:1;
  uint16 b14:1;
  uint16 b15:1;
}bits16;


#define BIT_00                  (0x0001)
#define BIT_01                  (0x0002)
#define BIT_02                  (0x0004)
#define BIT_03                  (0x0008)
#define BIT_04                  (0x0010)
#define BIT_05                  (0x0020)
#define BIT_06                  (0x0040)
#define BIT_07                  (0x0080)
#define BIT_08                  (0x0100)
#define BIT_09                  (0x0200)
#define BIT_10                  (0x0400)
#define BIT_11                  (0x0800)
#define BIT_12                  (0x1000)
#define BIT_13                  (0x2000)
#define BIT_14                  (0x4000)
#define BIT_15                  (0x8000)
#define BIT16               (0x00010000)
#define BIT17               (0x00020000)
#define BIT18               (0x00040000)
#define BIT19               (0x00080000)
#define BIT20               (0x00100000)
#define BIT21               (0x00200000)
#define BIT22               (0x00400000)
#define BIT23               (0x00800000)
#define BIT24               (0x01000000)
#define BIT25               (0x02000000)
#define BIT26               (0x04000000)
#define BIT27               (0x08000000)
#define BIT28               (0x10000000)
#define BIT29               (0x20000000)
#define BIT30               (0x40000000)
#define BIT31               (0x80000000)
              
// Read-Write Data Types
typedef signed int            SINT16;     // Signed 16-bit integer (15-bit magnitude)
typedef unsigned int          UINT16;     // Unsigned 16-bit integer
typedef signed long           SINT32;     // Signed 32-bit integer (31-bit magnitude)
typedef unsigned long         UINT32;     // Unsigned 32-bit integer
typedef signed long long      SINT64;     // Signed 64-bit integer
typedef unsigned long long    UINT64;     // Unsigned 64-bit integer
typedef int                   BOOL;       // Only TRUE/FALSE applied

// All pointers are 32 bits long
typedef SINT16                *PSINT16;   // Pointer to SINT16
typedef UINT16                *PUINT16;   // Pointer to UINT16
typedef SINT32                *PSINT32;   // Pointer to SINT32
typedef UINT32                *PUINT32;   // Pointer to UINT32


/* Function pointers */
typedef void (*PROC)(void); /* pointer to a void function */
typedef bool1 (*PBOOL)(void); /* pointer to a Bool function */


/* useful macros */
/*=====================*/
#define MIN(x, y)  (((x) < (y)) ? (x) : (y))
#define MAX(x, y)  (((x) > (y)) ? (x) : (y))
#define ABS(x)     (((x) >= 0) ? (x) : -(x))

#endif /* PTMTYPE_H_ */
