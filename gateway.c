
/**********************************************************************
   picollo.c - 

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 02.05.13
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060.1AA uC picollo TI DSP TMS320C2869
   descr.:
   Edit  :

 *********************************************************************/

#include "gatewaycfg.h"
#include "uc.h"
#include <clk.h>
#include "dspspecs.h"
#include "gateway_dict.h"
#include "recorder.h"
#include "hal.h"
#include "dict_util.h"
#include "sci1.h"
#include "error.h"
#include "param.h"
#include "crc.h"
#include "crc_tbl.h"
//#include "sincos.h"
#include "buffer.h"
#include "lcd.h"
#include "usb_dev_hid.h"
#include "ADS1015.h"
#include "convert.h"
#include "i2c.h"

extern UNS32 gateway_dict_obj1400_COB_ID_used_by_PDO;
extern UNS32 gateway_dict_obj1800_COB_ID_used_by_PDO;
extern UNS32 gateway_dict_obj1201_COB_ID_Client_to_Server_Receive_SDO;
extern UNS32 gateway_dict_obj1201_COB_ID_Server_to_Client_Transmit_SDO;

bool1 TestCurrentEnable = FALSE;
bool1 UnderCurrent = FALSE;
uint16 InitOK = 0;
bool1 TestPositionEnable = TRUE;
extern UNS32 GV_current_counter = 0;
extern UNS32 GV_Temp_counter = 0;
extern UNS32 GV_Temp_counter1 = 0;
uint8 Charger_Previous_State = 0;
//uint16 make10ms = 0;
extern UNS32 GV_Overvoltage_counter = 0;
extern UNS32 GV_Undervoltage_counter = 0;
int make10ms = 0;
bool1 SequenceRunning = FALSE;
bool1 ReceiveNew = FALSE;
uint32 TimerTemp = 0;
uint16 CANbus_timer = 0;
uint16 CANbus_timer2 = 0;
uint16 CANbus_timer3 = 0;
uint16 CANbus_ErrorReset = 0;
bool1 CounterReset = FALSE;
uint16 CANComm_ErrorReset = 0;
bool1 EventResetTemp  = TRUE;
bool1 EventResetVoltH = TRUE;
bool1 EventResetVoltL = TRUE;
int Charger_Current_State = -1;
Message MasterData_Frame;
Message SlaveData_Frame;
uint16 BatteryVoltage = 0;

bool1 NoCharge_Active = TRUE, FullCharge_Active = TRUE, TrickleCharge_Active = TRUE;

/**
 * @ingroup
 * @fn check_firmwareCRC
 * @brief
 * @param
 * - type: void
 * - range:
 * - description:
 * @return
 * - type: unsigned short
 * - range:
 * - description: returns 1 if CRCs match, 0 otherwise
 */
extern CRC_TABLE golden_CRC_values;
uint16 CheckfirmwareCRC(void)
{
  int i;
  uint32 temp;
  CRC_RECORD crc_rec;
  for (i = 0; i < golden_CRC_values.num_recs; i++)
  {
    crc_rec = golden_CRC_values.recs[i];
    temp = getCRC32_cpu(INIT_CRC32, (uint16*)crc_rec.addr, EVEN, crc_rec.size*2);
    if (temp != crc_rec.crc_value){
      return 0;
    }
    /**************************************************/
    /* COMPUTE CRC OF DATA STARTING AT crc_rec.addr */
    /* FOR crc_rec.size UNITS. USE */
    /* crc_rec.crc_alg_ID to select algorithm. */
    /* COMPARE COMPUTED VALUE TO crc_rec.crc_value. */
    /**************************************************/
  }
  /*if all CRCs match, return 1;
  else return 0;*/
  return 1;
}

//======================== NMT change state callback functions ================================
//called after a NMT_ResetNode (0x81)
void FnResetNode(CO_Data* d){
  //after this function, the state is changed to Initialization
  //and FnInitialiseNode(d) will be called
}

//called after a NMT_ResetNode (0x81)
void FnResetCommunications(CO_Data* d){
  uint16 node_id;
  char dummy_m[16]; //should be >= largest mailbox message

  //empty all send and receive mailboxes
  while (MBX_pend(&mailboxSDOout,&dummy_m,0));
  while (MBX_pend(&can_tx_mbox,&dummy_m,0));
  while (MBX_pend(&can_rx_mbox,&dummy_m,0));

  node_id = ODP_Board_RevisionNumber; //default value if reading canID fails  /// Johnny 100 test

  //adjust all CANopen communication parameters for the real node_id
  //gateway_dict_obj1400_COB_ID_used_by_PDO += node_id;
  //gateway_dict_obj1800_COB_ID_used_by_PDO += node_id;
  //gateway_dict_obj1201_COB_ID_Client_to_Server_Receive_SDO += node_id;
  //gateway_dict_obj1201_COB_ID_Server_to_Client_Transmit_SDO += node_id;
  setNodeId(d,(UNS8)node_id);  //set the node_id of the CO object

  //now initialize the CAN hardware
  canInit(ODP_Board_BaudRate, (UNS8)node_id);
  //set de nodeID of default networkdict
  DIC_SetNodeId(NODE_ID_STELLARIS);
}  //after this function, the state is changed to Initialization
   //and FnInitialiseNode(d) will be called


// called at startup (setState(OD,Initialization) and after NMT_Reset(0x81),
// or NMT_ResetCommunication(0x81)
void FnInitialiseNode(CO_Data* d) {
  uint16 CrcFirmwareTest = 0;
  //generate CRC32 table
  genCRC32Table();
  //compute firmware CRC
  CrcFirmwareTest = CheckfirmwareCRC();
  PieCtrlRegs.PIEIFR9.bit.INTx6 = 1; //to unlock can in case a com happened during reset
  ODV_Controlword.ControlWord = 0; //initialize motor control
  MMSConfig = (TMMSConfig*)&ODP_Board_Config;
  /// read ODV_Gateway_SOC from eeprom
  if (PAR_InitParam(d) && (CrcFirmwareTest == 1))
  {
    InitOK = 1;
  }
  else{
    InitOK = 2;
    //ERR_ErrorParam();
  }
  PAR_SetParamDependantVars();
}

// always called after FnInitialisation (startup, NMT_Reset, NMT_ResetCommunication)
// and after NMT_Enter PreOperational(0x80)
void FnEnterPreOperational(CO_Data* d) {
  ODV_Controlword.ControlWord = 6;
}

//called after a NMT_StartNode (0x01)
void FnStartNode(CO_Data* d) {

}

void ChargerCommError() {
	static int CANbus_ErrorResettick = 0;

	if ((ODV_ChargerData_MasterLimTemp > 0)	&& (ODV_ChargerData_SlaveLimTemp > 0)) {
		ODV_ChargerData_MasterLimTemp = 0;
		ODV_ChargerData_SlaveLimTemp = 0;
		ODV_ChargerData_ChargerError &= ~(POWERENABLE);
		CANbus_ErrorResettick = 0;
	} else {
		if (CANbus_ErrorResettick > 2) {
			CANbus_ErrorResettick++; /// 3 Sec
			ODV_ChargerData_ChargerError |= (POWERENABLE);
			CANbus_ErrorResettick = 0;
		}
	}

/*	if ((ODV_ChargerData_MasterErrorByte & POWERENABLE) && (ODV_ChargerData_SlaveErrorByte & POWERENABLE)) {
		CANComm_ErrorReset++;
		ODV_ChargerData_ChargerError &= ~(POWERENABLE);
	} else {
		CANComm_ErrorReset--;
	}
	if (CANComm_ErrorReset > 4) {
		ODV_ChargerData_MasterErrorByte &= ~(POWERENABLE);
		ODV_ChargerData_SlaveErrorByte &= ~(POWERENABLE);
		CANComm_ErrorReset = 1;
	}
	if (CANComm_ErrorReset == 0) {
		ODV_ChargerData_ChargerError |= (POWERENABLE);
	}*/
}

void InitialReset() {
	NoCharge_Active = TRUE;
	FullCharge_Active = TRUE;
	TrickleCharge_Active = TRUE;
}
bool1 CheckState_Change(int Present, int Next) {
	if(Present != Next) {
		return TRUE;
	} else return FALSE;
}
/// ****************NoCharge Mode *** ///
int Nocharge_Mode(int Voltage) {
	if (((Voltage >= 761) && (NoCharge_Active)) || (Voltage > 953)) {
		NoCharge_Active = TRUE;
	} else if ((Voltage < 761)) {
		NoCharge_Active = FALSE;
	}

	if(!NoCharge_Active) return MAX_DC_CURRENT * CHARGER_SCALING_FACTOR;

	return 0;
}
/// *************** FullCharge Mode *****///
int FullCharge_Mode(int Voltage){
	if ((Voltage <= 953) && FullCharge_Active) {
		FullCharge_Active = TRUE;
	} else if (Voltage > 953) {
		FullCharge_Active = FALSE;
		return 0;
	} else if ((Voltage < 943) && (!FullCharge_Active)) {
		FullCharge_Active = TRUE;
	}

	if(FullCharge_Active) return MAX_DC_CURRENT * CHARGER_SCALING_FACTOR;

	return 0;
}
/// *************** TrickleCharge Mode *****///
int TrickleCharge_Mode(int Voltage){
	if ((Voltage <= 857) && TrickleCharge_Active) {
		TrickleCharge_Active = TRUE;
	} else if (Voltage > 857) {
		TrickleCharge_Active = FALSE;
		return 0;
	} else if ((Voltage < 809) && (!TrickleCharge_Active)) {
		TrickleCharge_Active = TRUE;
	}

	if(TrickleCharge_Active) return MAX_DC_CURRENT * CHARGER_SCALING_FACTOR;

	return 0;
}
///**************** Start Charger Control CANBus Messages ******************************///////
void Charger_Control(Message EVOCharger_MasterData, Message EVOCharger_SlaveData, ChargerSettings ChargerSettings) {
	uint16 CANStatus, ACCurrent,DCVoltage,DCCurrent;

	CANStatus = ChargerSettings.CANEnable;
	//LEDStatus = ChargerSettings.LED;
	ACCurrent = ChargerSettings.MaxACCurrent;
	DCVoltage = ChargerSettings.MaxDCVoltage;
	DCCurrent = ChargerSettings.MaxDCCurrent;

	EVOCharger_MasterData.cob_id  = 0x568; EVOCharger_MasterData.len = 8;	EVOCharger_MasterData.rtr = 0;
	EVOCharger_MasterData.data[0] = CANStatus; EVOCharger_MasterData.data[1] = ACCurrent & 0xFF; EVOCharger_MasterData.data[2] = ACCurrent >> 8;
	EVOCharger_MasterData.data[3] = DCVoltage & 0xFF; EVOCharger_MasterData.data[4] = DCVoltage >> 8; EVOCharger_MasterData.data[5] = DCCurrent & 0xFF; EVOCharger_MasterData.data[6] = DCCurrent >> 8; EVOCharger_MasterData.data[7] = 0;

	EVOCharger_SlaveData.cob_id  = 0x578;  	EVOCharger_SlaveData.len = 8;	EVOCharger_SlaveData.rtr = 0;
	EVOCharger_SlaveData.data[0] = CANStatus; EVOCharger_SlaveData.data[1] = ACCurrent & 0xFF; EVOCharger_SlaveData.data[2] = ACCurrent >> 8;
	EVOCharger_SlaveData.data[3] = DCVoltage & 0xFF; EVOCharger_SlaveData.data[4] = DCVoltage >> 8; EVOCharger_SlaveData.data[5] = DCCurrent & 0xFF; EVOCharger_SlaveData.data[6] = DCCurrent >> 8; EVOCharger_SlaveData.data[7] = 0;

	MBX_post(&can_tx_mbox,&EVOCharger_MasterData,0);
	MBX_post(&can_tx_mbox,&EVOCharger_SlaveData,0);
}
///**************** End Charger Control CANBus Messages ******************************///////

#define MAX_MODULES 98
#define MAX_COMM_ERROR 6
uint16 Modules_Present = 0;
uint16 Sync_count = MAX_MODULES;
uint16 GAT_min = 0xFFFF, GAT_max = 1;
uint8 Heater_status = 0;
int8 Oldmaxtemp = 0, Oldmintemp = 0;
uint16 OldGAT_min = 0, OldGAT_max = 0;  /// V50
uint16 Delay_Error = 0;
extern UNS32 GV_Blinking_counter = 0;

void PreSync(CO_Data* d) {
	int i = 0;
	Heater_status = 0;
	int8 maxtemp = -55, mintemp = 127;
	uint32 SystemBatt = 0;

  --Sync_count;
  if (Sync_count == 0){
    Modules_Present = 0;
    while (i<=MAX_MODULES){
      if (ODV_Modules_Alarms[i] != 255)
      {
        if (GAT_min > ODV_Modules_MinCellVoltage[i]) GAT_min = ODV_Modules_MinCellVoltage[i];
        if (GAT_max < ODV_Modules_MaxCellVoltage[i]) GAT_max = ODV_Modules_MaxCellVoltage[i];
        if (maxtemp < ODV_Modules_Temperature[i]) maxtemp = ODV_Modules_Temperature[i];
        if (mintemp > ODV_Modules_Temperature_MIN[i]) mintemp = ODV_Modules_Temperature_MIN[i];
		Heater_status = Heater_status + ODV_Modules_Heater[i];
		SystemBatt = (SystemBatt + ODV_Modules_Voltage[i]);

        if (ODP_NbOfModules > 0)
        {
          Modules_Present += 1;//|= (1<<i);
          if (ODV_Modules_Alarms[i] > 0 && ODV_Modules_Alarms[i] < 255)  //if (ODV_Modules_Status[i] >= 0x80 && ODV_ErrorDsp_ErrorNumber == 0){
		  { 
        	  Delay_Error++;
        	  if (Delay_Error > ODP_Gateway_Delay_Relay_Error*2)   /// V50
        	  {
        		  ODV_ErrorDsp_ErrorNumber = (i+1) * ERR_MODULE_BIT + ERR_MODULE + ODV_Modules_Alarms[i];
        		  ERR_SetError(ODV_ErrorDsp_ErrorNumber);
        		  Delay_Error = 0;
        	  }
        	  //ERR_SetError(ODV_ErrorDsp_ErrorNumber);
		  }
          else      //if (ODV_Modules_Alarms[i] <= 0 && ODV_Modules_Alarms[i] >= 255) /// Johnny Test what if we have all values to 0 as reset button needed?
          {
        	 // ERR_ClearError();
        	 // Delay_Error = 0;
          }
        }
      }
      ODV_Modules_Alarms[i] = 255;
      ++i;
    }
	
    if (maxtemp != -55)   /// V50
     {
     	Oldmaxtemp = maxtemp;
     }

     if (mintemp != 127)
     {
     	Oldmintemp = mintemp;
     }
     if (GAT_min != 0xFFFF)
     {
     	OldGAT_min = GAT_min;
     }
     if (GAT_max != 1)
     {
     	OldGAT_max = GAT_max;
     }

     if (ODV_Gateway_Control_Heater == 0)
     {
    	 if (ODV_Gateway_MinModTemp < 35)
    	 {
			 if (((ODV_Gateway_MinModTemp >= ODP_Settings_AUD_TempHeater_OFF_Max) || ((ODV_Gateway_Voltage/VOLT_SCALE) < ODP_Settings_AUD_Heater_Voltage_OFF)) && (ODP_Settings_AUD_Heater_Voltage_OFF == 0))
			  {
				ODV_Gateway_Heater_Status = 0;
			  }
			  else if ((ODV_Gateway_MinModTemp < ODP_Settings_AUD_Temp_Heater_ON_Min) && (ODV_ErrorDsp_ErrorNumber == 0))
			  {
				ODV_Gateway_Heater_Status = 1;
			  }
    	 }
    	 else
    	 {
    		 ODV_Gateway_Heater_Status = 0;
    	 }
     }
     else if(ODV_Gateway_Control_Heater == 1)
     {
    	 ODV_Gateway_Heater_Status = 1;
     }
     else
     {
    	 ODV_Gateway_Heater_Status = 0;
     }

     if (TimerTemp > 200)  /// Delay in Updating values
     {
    	 TimerTemp = 0;
    	 ODV_Gateway_MaxModTemp = Oldmaxtemp;   /// V51
    	 ODV_Gateway_MinModTemp = Oldmintemp;
    	 ODV_Gateway_MinCellVoltage = OldGAT_min;
    	 ODV_Gateway_MaxCellVoltage = OldGAT_max;
    	 ODV_Gateway_MaxDeltaCellVoltage = OldGAT_max - OldGAT_min;
    	 BatteryVoltage = SystemBatt / 1000;  /// 1000mV

     }
 	// TimerTemp++;

	 if (ODP_NbOfModules > 0)
	 {
		 if (ODV_Gateway_MaxDeltaCellVoltage > ODP_SafetyLimits_Umax_bal_delta)    /// V50
		  {
			 if (CNV_Round(ODV_Gateway_Current/(float)CUR_SCALE) > 3)
			 {
					ERR_HandleWarning(WARN_LOW_CAPACITY); // if(ODV_ErrorDsp_ErrorNumber == 0) ERR_DELTA_Voltage();
			 }
			 else ERR_ClearWarning(WARN_LOW_CAPACITY);
	     }
		 if (ODV_Gateway_MaxDeltaCellVoltage > ODP_SafetyLimits_Umin_bal_delta)
		 {
			 ERR_HandleWarning(WARN_CELLDELTA);
		 }
		 else  ERR_ClearWarning(WARN_CELLDELTA);
	 }

    GAT_min = 0xFFFF;
    GAT_max = 1;
    Sync_count = MAX_MODULES+1;
  }
}

//called after a NMT_StopNode (0x01)
void FnStopNode(CO_Data* d) {
  //put here any code you think is useful if the node is stopped by the master
  ODV_Controlword.ControlWord = 0; //switch off motor control
}

CO_Data *BoardODdata;  //this is the pointer to our CANopen object

void canOpenInit(void)
/* CanOpen stack init */
{
  //Initialize the CO object
  BoardODdata = &ODI_gateway_dict_Data;
  //set the NMT change state callback functions
  BoardODdata->initialisation = FnInitialiseNode;
  BoardODdata->preOperational = FnEnterPreOperational;
  BoardODdata->operational = FnStartNode;
  BoardODdata->stopped = FnStopNode;
  BoardODdata->NMT_Slave_Node_Reset_Callback = FnResetNode;
  BoardODdata->NMT_Slave_Communications_Reset_Callback = FnResetCommunications;
  //set global callback functions
  BoardODdata->storeODSubIndex = PAR_StoreODSubIndex;
  //function called after writing to TO_BE_SAVED entries (write to non-volatile memory)
  BoardODdata->globalCallback = NULL; //function called when accessing any OD entry
  BoardODdata->pre_sync = PreSync;
  //Initialize the PDO and SDO setting and the low level can
  //FnResetCommunications(BoardODdata);
  setState(BoardODdata, Initialisation);
  FnResetCommunications(BoardODdata);
  /* this will call FnInitialisation followed by FnPreOperational
   * so at this point ODV_MachineState == STATE_PREOPERATIONAL
   * note that after NMT 80,81,82 the ODV_MachineState will be changed to
   * STATE_PREOPERATIONAL which covers all pre-operational states
   * (Stopped, PreOperational)
   */
  setState(BoardODdata,Operational);

}

#define BOOT_COMMAND_DOWNLOAD_WAIT 0x52656D69
#define BOOT_COMMAND_NONE          0

#pragma DATA_SECTION(BootCommand,"BootCommand");
#pragma DATA_SECTION(BootNodeId,"BootCommand");
uint32 BootNodeId = 99;		
uint32 BootCommand = BOOT_COMMAND_NONE;

void taskFnGestionMachine(void){
  //uint16 val, adpos = 0;
  uint32 old_time;
  //Initialize the CanOpen stack and BoardODdata pointer;

  //canOpenInit();
  TSK_sleep(2);
  HAL_Init();
  ADS_Init();
  while(InitOK == 0) {
    TSK_sleep(1);
  }
  USB_Start();
  old_time = ODP_OnTime;
  while(BootCommand == BOOT_COMMAND_NONE){

    /********** RTC Handling *********/
    // toutes les 64s de fonctionnement
    if (((ODP_OnTime & 63) == 63) && ((old_time & 63) == 62))
    {
      //I2C_Command(RTC_READ,(char *)&ODV_Gateway_Date_Time,7,0);//only if new error
      PAR_WriteStatisticParam(-1);
      PAR_WriteStatisticParam(-5);
    }
    old_time = ODP_OnTime;
    USB_Unlock();
    TSK_sleep(2);
  } //while(1);
  USB_Stop();
  TSK_sleep(2);
  HAL_Reset();
  BootCommand = BOOT_COMMAND_DOWNLOAD_WAIT;
  asm(" LB 0x3F7FF6 ");//code_start
}

#define INSULATION_THRESHOLD  4080
#define INSULATION_TIMEOUT   60000
#ifdef PTM068
#define LED_VALUE   GpioDataRegs.GPADAT.bit.GPIO12
#define SET_RELAY_LED LED_VALUE = 1
#define CLR_RELAY_LED LED_VALUE = 0
#else
#define LED_VALUE   GpioDataRegs.GPADAT.bit.GPIO24
#define SET_RELAY_LED LED_VALUE = 0
#define CLR_RELAY_LED LED_VALUE = 1
#endif
#define RELAY_VALUE ODV_Write_Outputs_16_Bit[0] //0 negative, 1 precharge, 2 positive
//#define CHARGE_IN  GpioDataRegs.GPADAT.bit.GPIO23

void TaskSciSendReceive(void){
  volatile uint32 start, insulation_counter = 0, current_counter = 0, voltage_counter = 0,Precharge_counter = 0, sleep_counter = 0, current_counter2 = 0, reset_relay = 0, repeatcheck = 0;
  uint16 com_counter = 0;
  int16 cur, BatteryCurrent;
  uint16 volt;

  float curf;
  bool1 checkenable = FALSE;
  ODV_MachineMode = GATEWAY_MODE_NOINI;
  while(InitOK == 0){//wait for init
    TSK_sleep(1);
  };
  while (InitOK != 1) //|| ((ODV_Gateway_Errorcode & ERR_INSULATION) != 0))
  {//if bad init or insulation error
    TSK_sleep(1);//stop here
  }
  TSK_sleep(10);
  ODV_MachineEvent = EVENT_NONE;
  ODV_Gateway_State = GATEWAY_STATE_STILL;
  ODV_MachineMode = GATEWAY_MODE_STILL;
  ODV_Gateway_Errorcode = 0;
  I2C_Command(RTC_READ,(char *)&ODV_Gateway_Date_Time,7,0);

  //if (MMSConfig->en24v)
  //  GpioDataRegs.GPADAT.bit.GPIO6 = 0;//switch on modules ///  Test LED

  if (MMSConfig->can_wk == 0 && MMSConfig->sw_wk == 1) {
    ODP_CommError_TimeOut = 0;
    ODV_Gateway_State = GATEWAY_MODE_NORM;
  }

  TSK_sleep(500);//wait for modules to be ready
  Modules_Present = ODP_NbOfModules; //set nb of module to be ok at startup
  while (1) {

	  TimerTemp++;
	  CANbus_timer++;
	  CANbus_timer2++;

		if (CANbus_timer > 1000) {

			EVOCharger.CANEnable = 0x88;
			//EVOCharger.LED = 1;
			EVOCharger.MaxACCurrent = MAX_AC_CURRENT * CHARGER_SCALING_FACTOR;
			EVOCharger.MaxDCVoltage = MAX_DC_VOLTAGE * CHARGER_SCALING_FACTOR;
			EVOCharger.MaxDCCurrent = 0;

			//BatteryVoltage = ODV_Gateway_Voltage / VOLT_SCALE;
			BatteryCurrent = CNV_Round((float) ODV_Gateway_Current / (float) CUR_SCALE);

			/// ************** Start Charger Control Stage ************* ///
			if (CheckState_Change(Charger_Current_State, ODV_Gateway_Requested_ChargerStage)) {
				Charger_Current_State = InitialSate;
			}

			switch (Charger_Current_State) {
			case InitialSate:
				InitialReset();
				Charger_Current_State = ODV_Gateway_Requested_ChargerStage;
				break;
			case NoCharge:
				EVOCharger.MaxDCCurrent = Nocharge_Mode(BatteryVoltage);
				break;
			case FullCharge:
				EVOCharger.MaxDCCurrent = FullCharge_Mode(BatteryVoltage);
				break;
			case TrickleCharge:
				EVOCharger.MaxDCCurrent = TrickleCharge_Mode(BatteryVoltage);
				break;
			default:
				break;
			};

			if ((EVOCharger.MaxDCCurrent > 0) && (BatteryCurrent <= 5) && (BatteryVoltage <= 750) && (BatteryVoltage >= 30)) {
				CANbus_timer3++;
				if (CANbus_timer3 > 20000) {
					if (CounterReset) {
						if(ODP_CommError_Charge_NotSucessful > 8) ODP_CommError_Charge_NotSucessful = 0;
						ODP_CommError_Charge_NotSucessful++;
						PAR_WriteStatisticParam(-5);
						PAR_AddLog();
						CounterReset = FALSE;
					}
				}
			} else {
				CounterReset = TRUE;
				CANbus_timer3 = 0;
			}

			/// ************** Start Charger Error Checking ************* ///
			ChargerCommError();
			ChargerError();
			/// Checking Charger Error Every 1s
			/// ************** End Charger Error Checking ************* ///

			CANbus_timer = 0;
		}

		if(CANbus_timer2 > 100) {
			CANbus_timer2 = 0;
			Charger_Control(MasterData_Frame, SlaveData_Frame, EVOCharger);
		}

  	ODV_Board_RelayStatus = RELAY_VALUE;

    if ((ODV_Gateway_State&0xF) == GATEWAY_STATE_CRASH && ODV_MachineMode != GATEWAY_MODE_CRASH){
      start = ODV_SysTick_ms;
      ODV_MachineMode = GATEWAY_MODE_CRASH;
    }
    if (Sync_count == MAX_MODULES+1){
      --Sync_count;//to avoid reentrency next ms
      if (Modules_Present != ODP_NbOfModules) {
        ++com_counter;
        if (com_counter == (MAX_COMM_ERROR + ODP_Gateway_IsoResistor_Limit_Max)) {
        	ERR_HandleWarning(WARN_CYCLE);
        	/// V50
        	//  ODV_ErrorDsp_ErrorNumber = ERR_MODULE + ERR_COMM; /// V51 15.04.2021 Deactive Communication Error when number of Modules are not Equal
        	//  ERR_SetError(ODV_ErrorDsp_ErrorNumber);
        }
      }
      else  {
    	  com_counter = 0;
          ERR_ClearWarning(WARN_CYCLE);
      }
    }
    volt = ODV_Gateway_Voltage/VOLT_SCALE;

    //ODV_Gateway_Test_Voltage = ODV_Gateway_Voltage/2; /// convert from 2 to 1 byte
    if (ODV_MachineMode > GATEWAY_MODE_STILL && ODV_MachineMode <= GATEWAY_MODE_DONLY){
      if (ODV_Gateway_State == GATEWAY_STATE_STILL)
      {
        //if (MMSConfig->en24v == 0)
        //  GpioDataRegs.GPADAT.bit.GPIO6 = 1;//switch off 24V/led relay  ///  Test LED
        RELAY_VALUE = 0;
        ODV_MachineMode = GATEWAY_MODE_STILL;
        CLR_RELAY_LED;
      }

      curf = (float)ODV_Gateway_Current/(float)CUR_SCALE;
      cur = CNV_Round(curf);//A

	  /// Johnny Test 10.05.2019
      if (cur > ODP_SafetyLimits_Overcurrent || cur < ODP_SafetyLimits_UnderCurrent)
	  {		     
    	  GV_current_counter++;
    	  if (GV_current_counter > (uint32)ODP_SafetyLimits_Current_delay*1000)
    	  {
    		  if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorOverCurrent();

			  if (cur > ODP_Settings_AUD_Gateway_Current_Ringsaver)
				{
					//ODP_Gateway_Relay_Error_Count++;
					//PAR_WriteStatisticParam(-5);
				}
    	  }    	  
      }
      else
      {
    	  GV_current_counter = 0;
      }
	  /// error high Voltage
	  if (volt >= ODP_SafetyLimits_OverVoltage)
	  {		     
		  GV_Overvoltage_counter++;
    	  if (GV_Overvoltage_counter > (uint32)ODP_SafetyLimits_Voltage_delay*1000)
    	  {
    		  if(ODP_CommError_OverVoltage_ErrCounter > 250) ODP_CommError_OverVoltage_ErrCounter = 255;
    		  if(ODV_ErrorDsp_ErrorNumber == 0)
    		  {
    			  ODP_CommError_OverVoltage_ErrCounter++;
    			  ERR_ErrorOverVoltage();
    		  }
    	  }
      }
	  else
	  {
		  GV_Overvoltage_counter = 0;
	  }
	  
	  /// error low voltage
	  if (volt <= ODP_SafetyLimits_UnderVoltage)  /// ->V39
	  {
		  ODV_MachineMode = GATEWAY_MODE_CONLY;
		  start = ODV_SysTick_ms;
		  checkenable = FALSE;

		  GV_Undervoltage_counter++;
    	  if (GV_Undervoltage_counter > (uint32)ODP_SafetyLimits_Voltage_delay*1000){
    		  if(ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorUnderVoltage(); // DEEP Discharge Protection
    	 }
      }
	  else
	  {
		  GV_Undervoltage_counter = 0;
	  }

	  /// warning Low voltage
	  if (volt < ODP_SafetyLimits_Umin)
	  {
		  ERR_HandleWarning(WARN_LOW_CAPACITY);
      }
	  else
	  {
		  ERR_ClearWarning(WARN_LOW_CAPACITY);
	  }

	  /// warning Low voltage
	  if (volt > ODP_SafetyLimits_Umax)
	  {
		  ERR_HandleWarning(WARN_OVERCAPACITY);
      }
	  else
	  {
		  ERR_ClearWarning(WARN_OVERCAPACITY);
	  }

      if (ABS(curf*1000) < (float)ODP_Sleep_Current){ //mA
        ++sleep_counter;
        if (sleep_counter > (uint32)ODP_Sleep_Timeout*1000){
          sleep_counter = 0;
         // ODV_MachineMode = GATEWAY_MODE_ERROR_IN;
        }
      }
      else if (sleep_counter > 0) --sleep_counter;
      if (ODP_RelayResetTime > 0 && volt < 8){
        ++reset_relay;
        if (reset_relay >= (uint32)ODP_RelayResetTime*60000){
          reset_relay = 0;
          RELAY_VALUE = 5;//switch back on relays
        }
        else if (reset_relay >= 50000) RELAY_VALUE = 0;//switch off
      }
      else if (reset_relay > 0) reset_relay = 0;
      if (ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP2] >= (int)ODP_SafetyLimits_Resistor_Tmax*10 || ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP2] <= (int)ODP_SafetyLimits_Resistor_Tmin*10
          || ODV_Gateway_Temperature > ODP_SafetyLimits_Tmax || ODV_Gateway_Temperature < ODP_SafetyLimits_Tmin)
      {
    	  if ((ODV_Read_Analogue_Input_16_Bit[PIC_MEASURE_TEMP2] == -550) || (ODV_Gateway_Temperature == -55))
    	  {
    		 GV_Temp_counter = 0;
    	  }

    	  GV_Temp_counter++;
    	  if (GV_Temp_counter > (uint32)ODP_Settings_AUD_Temperature_Delay*1000)
    	  {
    		  if (cur < ODP_Settings_AUD_Gateway_Current_Ringsaver)
    		  {
    			  if (ODV_ErrorDsp_ErrorNumber == 0) ODP_Gateway_Relay_Error_Count++;
    			  PAR_WriteStatisticParam(-5);
    		  }
    		  if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorOverTemp();
    	  }
      }
      else
      {
    	  GV_Temp_counter = 0;
      }
    	  if(ODV_Gateway_Temperature > 50)
    	  {
    	      GV_Temp_counter1++;

    	      if (GV_Temp_counter1 > (uint32)ODP_Settings_AUD_Temperature_Delay*1000)
    	      {
    	    	  if(EventResetTemp)
    	    	  {
					  if(ODP_CommError_OverTemp_ErrCounter >250) ODP_CommError_OverTemp_ErrCounter=255;
					  if(ODV_ErrorDsp_ErrorNumber == 0) ODP_CommError_OverTemp_ErrCounter++;
					  EventResetTemp = FALSE;
					  PAR_WriteStatisticParam(-5);
				  }
    	     }
    	     else
    	     {
    	    	 GV_Temp_counter1 = 0;
    	     }
    	  }
    	  else
    	  {
    		  EventResetTemp = TRUE;
    	  }

      if (ODV_Gateway_MaxModTemp > ODP_Temperature_WarningMax || ODV_Gateway_MinModTemp < ODP_Temperature_WarningMin)
      {
        ERR_HandleWarning(WARN_TEMPERATURE);
      }
      else ERR_ClearWarning(WARN_TEMPERATURE);
      if (cur < -(int16)ODV_Current_DischargeAllowed || cur > (int16)ODV_Current_ChargeAllowed )
      {
        ERR_HandleWarning(WARN_CURRENT);
        /*++current_counter;
        if (current_counter > (uint32)ODP_SafetyLimits_Current_delay*1000){
          if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorOverCurrent();
        }*/
      }
      else ERR_ClearWarning(WARN_CURRENT);
      //else if (current_counter > 0) --current_counter;
    }
    switch (ODV_MachineMode){
      case GATEWAY_MODE_STILL:
      //Mode still relay opened
        if (MMSConfig->sw_wk == 1)
        {
          ODV_Gateway_State = GATEWAY_MODE_NORM;
        }
        else if (MMSConfig->can_wk == 0)
        {
        	ODV_Gateway_Alive_Counter = 1;
        	if (GATEWAY_STATE_STILL == 0)
        	{
        		ODV_Gateway_State = GATEWAY_STATE_STILL;
        	}
        	else if(GATEWAY_STATE_STILL == 1)
        	{
        		ODV_Gateway_State = GATEWAY_STATE_NORMAL;
        	}
        }
       /* if  (volt < ODP_SafetyLimits_UnderVoltage)
        {  //(MMSConfig->ch_wk == 1 && (CHARGE_IN==0)){
        	ODV_Gateway_State = GATEWAY_STATE_NORMAL;
        }*/
         if(ODV_Gateway_State > GATEWAY_STATE_STILL)
        {
          if (MMSConfig->onerelay == 0) RELAY_VALUE = 1; //switch on gnd relay for measuring load
         // GpioDataRegs.GPADAT.bit.GPIO6 = 0;//switch on 24V/led relay  ///  Test LED
          SET_RELAY_LED;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
          ODV_MachineMode = GATEWAY_MODE_24ON;
        }
      break;

      case GATEWAY_MODE_24ON:
        if (ODV_SysTick_ms - start >= 100) checkenable = TRUE;
        if (checkenable){
          if ((ODV_Gateway_Voltage >= 0)) { //polarity ok
            start = ODV_SysTick_ms;
            if (MMSConfig->onerelay == 0){
#ifdef HV_MODE_ENABLE
              RELAY_VALUE = 3; //relay1 + 2
              ODV_MachineMode = GATEWAY_MODE_PRECH;
#else
              RELAY_VALUE = 5; //relay1 + 3 no precharge in lv mode
              ODV_MachineMode = GATEWAY_MODE_PRECH;

#endif
            }
            else{
              RELAY_VALUE = 4; //relay 3+
              ODV_MachineMode = GATEWAY_MODE_NORM;
            }
          }
          else{
            ERR_SetError(ERR_SELFTEST);
          }
        }
      break;

      case GATEWAY_MODE_DONLY:
        if (ODV_SysTick_ms - start >= 50) checkenable = TRUE;
        if (volt >= ODP_SafetyLimits_OverVoltage && checkenable)
        {
          ++voltage_counter;
          if (voltage_counter > (uint32)ODP_SafetyLimits_Voltage_delay*1000)
          {
        	  if(ODP_CommError_OverVoltage_ErrCounter > 250) ODP_CommError_OverVoltage_ErrCounter = 255;
        	  if(ODV_ErrorDsp_ErrorNumber == 0)
        	  {
        		  ODP_CommError_OverVoltage_ErrCounter++;
        		  ERR_ErrorOverVoltage();
        	  }

        /*  if(EventResetVoltH)
        	  {
        		  if(ODP_CommError_OverVoltage_ErrCounter > 250) ODP_CommError_OverVoltage_ErrCounter = 255;
        		  ODP_CommError_OverVoltage_ErrCounter++;
        		  EventResetVoltH = FALSE;
        		  PAR_WriteStatisticParam(-5);
        	  }*/
          }
        /*  else
          {
        	  EventResetVoltH = TRUE;
          }*/
        }
        else voltage_counter = 0;
        if (cur >= (int)ODP_Current_C_D_Mode && checkenable)
        {
          ++current_counter2;
          if (current_counter2 > (uint32)ODP_SafetyLimits_Current_delay*1000)
        	  {
        	  	  //ERR_ErrorOverCurrent();
        	  /*	if (cur < ODP_Settings_AUD_Gateway_Current_Ringsaver)
        	  	  {
        	  		ODP_Gateway_Relay_Error_Count++;
        	  		PAR_WriteStatisticParam(-5);
        	  	  }*/
        	  }
        }
        else if (current_counter2 > 0) --current_counter2;
        if (volt < ODP_SafetyLimits_UnderVoltage && checkenable){
          ODV_MachineMode = GATEWAY_MODE_CONLY;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
        if ((ODV_Gateway_State&0xF) == GATEWAY_STATE_NORMAL && volt < ODP_SafetyLimits_Umax && checkenable){
          ODV_MachineMode = GATEWAY_MODE_NORM;
          ERR_ClearWarning(WARN_OVERCAPACITY);
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
        if (checkenable && curf >= 0.2)
        {
          //PAR_Capacity_Left = PAR_Capacity_Total;
        }
      break;

      case GATEWAY_MODE_CONLY:
    	  if (ODV_SysTick_ms - start >= 50) checkenable = TRUE;
    	  if (RELAY_VALUE != 5)
    	  {
    		  ODP_Settings_AUD_Safety_Low_Voltage_Delay = ODP_SafetyLimits_Resistor_Delay;
    		  checkenable = TRUE;
    	  }
    	  else
    	  {
    		  ODP_Settings_AUD_Safety_Low_Voltage_Delay = ODP_Settings_AUD_Safety_Low_Voltage_Delay; /// Warning TEST i may assign old value Code Analysis Report
    		  checkenable = FALSE;
    	  }

    	  ERR_HandleWarning(WARN_LOW_CAPACITY);

        if (volt <= ODP_SafetyLimits_UnderVoltage)
        {
      	  Precharge_counter++;

        	if (Precharge_counter > (uint32)(ODP_Settings_AUD_Safety_Low_Voltage_Delay * 1000))
        	{
        		if(ODP_CommError_LowVoltage_ErrCounter > 250) ODP_CommError_LowVoltage_ErrCounter = 255;
        		if((ODV_ErrorDsp_ErrorNumber == 0) && (checkenable))
        		{
        			ERR_ErrorUnderVoltage();
        			ODP_CommError_LowVoltage_ErrCounter++;
        		}
        	}
        }
        else Precharge_counter = 0;

        if (volt >= ODP_SafetyLimits_OverVoltage)
        {
          ++voltage_counter;
          if (voltage_counter > (uint32)ODP_SafetyLimits_Voltage_delay*1000)
        	  {
        	  if(ODP_CommError_OverVoltage_ErrCounter > 250) ODP_CommError_OverVoltage_ErrCounter = 255;
        	  if(ODV_ErrorDsp_ErrorNumber == 0)
        		  {
        		  	  ODP_CommError_OverVoltage_ErrCounter++;
        		  	  ERR_ErrorOverVoltage();
        		  }

        	  	  if (cur < ODP_Settings_AUD_Gateway_Current_Ringsaver)
        	  	  	{
        	  		  ODP_Gateway_Relay_Error_Count++;
        	  		  PAR_WriteStatisticParam(-5);
        	  	  	}
        	  }
        }
        else voltage_counter = 0;

        if (cur <= -(int)ODP_Current_C_D_Mode || (cur <= -(int)ODP_SafetyLimits_Charge_In_Thres_Cur && checkenable))
        { /// && MMSConfig->ch_wk == 1 && CHARGE_IN==0) && checkenable) {
          ++current_counter2;
          if (current_counter2 > (uint32)ODP_SafetyLimits_Current_delay*1000)
          {
        	  //if (ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorOverCurrent();
          }
        }
        else if (current_counter2 > 0) --current_counter2;

        if ((ODV_Gateway_State&0xF) == GATEWAY_STATE_NORMAL && volt >ODP_SafetyLimits_UnderVoltage && checkenable){ // && ((MMSConfig->ch_wk == 1 && CHARGE_IN == 1) || MMSConfig->ch_wk == 0)){  ODP_SafetyLimits_Umin //if state normal, voltage greater than limit and charge in removed
          ODV_MachineMode = GATEWAY_MODE_NORM;
          ERR_ClearWarning(WARN_LOW_CAPACITY);
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
      break;

      case GATEWAY_MODE_NORM:
      //Mode normal relay closed
    	if (RELAY_VALUE != 5) RELAY_VALUE = 5;//relay1+3

        if (ODV_SysTick_ms - start >= 50) checkenable = TRUE;
        if (voltage_counter > 0) --voltage_counter;
        if (current_counter2 > 0) --current_counter2;
        if ((ODV_Gateway_State&0xF) == GATEWAY_STATE_CHARGE_ONLY || volt <= ODP_SafetyLimits_UnderVoltage && checkenable){
          ERR_HandleWarning(WARN_LOW_CAPACITY);
          ODV_MachineMode = GATEWAY_MODE_CONLY;
          //ODV_Gateway_State = GATEWAY_STATE_CHARGE_ONLY;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
        if (volt < ODP_SafetyLimits_UnderVoltage){ //(MMSConfig->ch_wk == 1 && CHARGE_IN == 0 && checkenable){//charger in
          ODV_MachineMode = GATEWAY_MODE_CONLY;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
        if ((ODV_Gateway_State&0xF) == GATEWAY_STATE_DISCHARGE_ONLY || volt >= ODP_SafetyLimits_Umax && checkenable){
          ERR_HandleWarning(WARN_OVERCAPACITY);
          ODV_MachineMode = GATEWAY_MODE_DONLY;
          //ODV_Gateway_State = GATEWAY_STATE_DISCHARGE_ONLY;
          start = ODV_SysTick_ms;
          checkenable = FALSE;
        }
      break;

      case GATEWAY_MODE_PRECH:
        if (volt >= ODP_SafetyLimits_UnderVoltage)
        { //voltage min
          RELAY_VALUE = 5;//relay1+3
          start = ODV_SysTick_ms;
          checkenable = FALSE;
          ODV_MachineMode = (ODV_Gateway_State&0xF);
        }
        //max 30s precharge & 100�C
        else
        {
        	ODV_MachineMode = GATEWAY_MODE_CONLY;  /// We need this orelse it will be waste to have low voltage
        	start = ODV_SysTick_ms;

          if (ODV_SysTick_ms - start >= (uint32)ODP_SafetyLimits_Resistor_Delay*1000)
          {
        	  //if(ODV_ErrorDsp_ErrorNumber == 0) ERR_ErrorUnderVoltage();
        	   // ODV_MachineMode = GATEWAY_MODE_CONLY;  /// We need this orelse it will be waste to have low voltage
        	   // start = ODV_SysTick_ms;
        	   // checkenable = FALSE;
          /*  if (cur < ODP_Settings_AUD_Gateway_Current_Ringsaver)
            	{
            		ODP_Gateway_Relay_Error_Count++;
            		PAR_WriteStatisticParam(-5);
            	}
            if(EventResetVoltL)
            {
            	if(ODP_CommError_LowVoltage_ErrCounter > 250) ODP_CommError_LowVoltage_ErrCounter = 255;
            	ODP_CommError_LowVoltage_ErrCounter++;
            	EventResetVoltL = FALSE;
            	PAR_WriteStatisticParam(-5);
            }
            else
            {
            	EventResetVoltL = TRUE;
            }*/
          }
        }
      break;

      case GATEWAY_MODE_CRASH:
        if (ODV_SysTick_ms - start >= ODP_CommError_Delay){
          RELAY_VALUE = 0;
          CLR_RELAY_LED;
         // if (MMSConfig->en24v == 0) GpioDataRegs.GPADAT.bit.GPIO6 = 1;//switch off 24V/led relay   ///  Test LED
        }
        break;

      case GATEWAY_MODE_ERROR_IN:
        start = ODV_SysTick_ms;
        ODV_MachineMode = GATEWAY_MODE_ERROR;
        checkenable = FALSE;
      case GATEWAY_MODE_ERROR:
      default:
    	if ((ODV_SysTick_ms - start >= ODP_CommError_Delay))
        {
    		if (!checkenable)
    		{
    			RELAY_VALUE = 0;
    			CLR_RELAY_LED;
    			//if (!checkenable) PAR_AddLog(); /// 25.06 Deactivated because i am using this TimeStamp for Charger Status Error
    			checkenable = TRUE;
    		}
        }

    	// if (MMSConfig->en24v == 0) GpioDataRegs.GPADAT.bit.GPIO6 = 1;//switch off 24V/led relay   ///  Test LED
      /* if (ODV_Gateway_State == GATEWAY_STATE_STILL || (ODV_ErrorDsp_ErrorNumber == 0))  // && (ODV_SysTick_ms - start >= 10000))){
        {
          if (ERR_ClearError()){
            ERR_ClearWarnings();
     //     ODV_Gateway_Errorcode = 0; ///  test
            ODV_MachineMode = GATEWAY_MODE_STILL;
            ODV_Gateway_State = GATEWAY_STATE_STILL;

          }
        }  */

        if (ODV_MachineMode > GATEWAY_MODE_STILL && ODV_MachineMode <= GATEWAY_MODE_DONLY){
          if (ODV_Gateway_State == GATEWAY_STATE_STILL)
          {
        	  if (repeatcheck == 0)
        	  {
        		  RELAY_VALUE = 0;
        		  ODV_MachineMode = GATEWAY_MODE_STILL;
        		  CLR_RELAY_LED;
        	  }
          }
        }
        repeatcheck++;

      break;
    }//end switch
    TSK_sleep(1);
  }
}


/***************************** Object Dictionary Callback functions ********************************/
UNS32 SecurityCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_READ) {
    if (ODP_RandomNB == 0) HAL_Random();
  }
  if (access == DIC_WRITE) {
    if (ODP_RandomNB == 0) HAL_Random();
    else HAL_Unlock();
  }
  return(0);
}

extern int8 TimeLogIndex;
UNS32 LogNBCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  /* callback to get log */
  if (access == DIC_WRITE) {
    PAR_GetLogNB(ODV_Gateway_LogNB);
  }
  else{
    ODV_Gateway_LogNB = TimeLogIndex;
  }
  return(OD_SUCCESSFUL);
}

UNS32 BatteryCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){
    if (bSubindex == 1){
      PAR_Capacity_Total = CNV_Round(ODP_Battery_Capacity*3600*(float)CUR_SCALE);  ///1ms to 1Sec
     // PAR_Capacity_Left = (uint64)PAR_Capacity_Total * ODV_SOC_SOC2 / 100;
      PAR_Capacity_TotalLife_Used = 0;
    }
  }
  return(OD_SUCCESSFUL);
}

UNS32 WriteTextCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  /* callback for the lcd to write some text */
  if (access == DIC_WRITE) {
    //LCD_WriteText(ODV_LCD_Pos,(char *)&ODV_LCD_Text,ODV_LCD_Len);
    I2C_Command(RTC_WRITE,(char *)&ODV_RTC_Text,8,0);
  }
  else{
    //LCD_ReadText(ODV_LCD_Pos,(char *)&ODV_LCD_Text,ODV_LCD_Len);
    I2C_Command(RTC_READ,(char *)&ODV_RTC_Text,8,0);
  }
  return(OD_SUCCESSFUL);
}

UNS32 MotCurCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if(access == DIC_WRITE){
    if (bSubindex == 2){
      HAL_NewCurPoint = TRUE;
    }
  }
  return(OD_SUCCESSFUL);
}


UNS32 SciSendCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if(!MBX_post(&sci_rx_mbox, &ODV_SciSend, 0)){

  }
  return(OD_SUCCESSFUL);
}

UNS32 SinCosCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){

  return(OD_SUCCESSFUL);
}


UNS32 StartRecorderCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
/* callback for the recorder to record what desired */
  if (access == DIC_WRITE) {
    REC_StartRecorder();
  }
  return(OD_SUCCESSFUL);
}

UNS32 MultiunitsCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
/* Function called whenever the GUI want to know all the units of the board */
  if (access == DIC_WRITE) {
    ODV_Recorder_Multiunits = PAR_AddMultiUnits(ODV_Recorder_Multiunits);
  }
  return(OD_SUCCESSFUL);
}

UNS32 VariablesCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
/* Function called whenever the GUI want to know all the vars of the board */
  if (access == DIC_WRITE){
    ODV_Recorder_Variables = PAR_AddVariables(ODV_Recorder_Variables);
  }
  return(OD_SUCCESSFUL);
}


UNS32 WriteOutputs8BitCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE);
  return(OD_SUCCESSFUL);
}

UNS32 WriteOutputs16BitCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){
    //nothing to do see relay state machine
  }
  return(OD_SUCCESSFUL);
}

UNS32 ReadInputs8BitsCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  //if (access == DIC_READ) ODV_Read_Inputs_8_Bit[DUT_DIG_OUTPUT_DSP-1] = HAL_GetInput8bits();
  return(OD_SUCCESSFUL);
}

UNS32 ControlWordCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE) {}
  return(OD_SUCCESSFUL);
}

UNS32 VersionCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_READ) ODV_Version = NGTEST_VERSION;
  return(OD_SUCCESSFUL);
}

UNS32 WriteAnalogueOutputsCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  //if (access == DIC_WRITE) HAL_SetValveDuty(bSubindex-1,ODV_Write_Analogue_Output_16_Bit[bSubindex-1]);
  return(OD_SUCCESSFUL);
}


UNS32 CommErrorSetCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){

  }
  return(OD_SUCCESSFUL);
}

UNS32 SaveAllParameters(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (ODV_StoreParameters == 0x73617665) {
    ODV_StoreParameters = 0;
    PAR_WriteAllPermanentParam(BoardODdata);
  }
  return(0);
}

UNS32 LoadDefaultParameters(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (ODV_RestoreDefaultParameters == 0x6C6F6164) {
    ODV_RestoreDefaultParameters = 0;
    PAR_UpdateCode(FALSE);
    BootCommand = 1;
  }
  return(0);
}


UNS32 DebugCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  ODV_Debug = 0;
  return(0);
}

UNS32 ConfigCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){
    if (ODV_MachineMode == GATEWAY_MODE_STILL || ODV_MachineMode == GATEWAY_MODE_ERROR)
    {

    }
    /*
    	if (MMSConfig->en24v)
      {
       // GpioDataRegs.GPADAT.bit.GPIO6 = 0;//switch on 24V ///  Test LED
      }
      else
        //GpioDataRegs.GPADAT.bit.GPIO6 = 1;//switch off 24V   ///  Test LED

    }*/
  }
  return(0);
}

UNS32 ResetCallBack(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (ODV_ResetHW == 0x7A65726F) {
    ODV_ResetHW = 0;
    BootCommand = 1;
  }
  return(0);
}




