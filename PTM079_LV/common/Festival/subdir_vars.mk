################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../common/Festival/dcf.c \
../common/Festival/emcy.c \
../common/Festival/lifegrd.c \
../common/Festival/nmtMaster.c \
../common/Festival/nmtSlave.c \
../common/Festival/objacces.c \
../common/Festival/pdo.c \
../common/Festival/sdo.c \
../common/Festival/states.c \
../common/Festival/sync.c \
../common/Festival/timer.c \
../common/Festival/timers_dsp.c 

OBJS += \
./common/Festival/dcf.obj \
./common/Festival/emcy.obj \
./common/Festival/lifegrd.obj \
./common/Festival/nmtMaster.obj \
./common/Festival/nmtSlave.obj \
./common/Festival/objacces.obj \
./common/Festival/pdo.obj \
./common/Festival/sdo.obj \
./common/Festival/states.obj \
./common/Festival/sync.obj \
./common/Festival/timer.obj \
./common/Festival/timers_dsp.obj 

C_DEPS += \
./common/Festival/dcf.pp \
./common/Festival/emcy.pp \
./common/Festival/lifegrd.pp \
./common/Festival/nmtMaster.pp \
./common/Festival/nmtSlave.pp \
./common/Festival/objacces.pp \
./common/Festival/pdo.pp \
./common/Festival/sdo.pp \
./common/Festival/states.pp \
./common/Festival/sync.pp \
./common/Festival/timer.pp \
./common/Festival/timers_dsp.pp 

C_DEPS__QUOTED += \
"common\Festival\dcf.pp" \
"common\Festival\emcy.pp" \
"common\Festival\lifegrd.pp" \
"common\Festival\nmtMaster.pp" \
"common\Festival\nmtSlave.pp" \
"common\Festival\objacces.pp" \
"common\Festival\pdo.pp" \
"common\Festival\sdo.pp" \
"common\Festival\states.pp" \
"common\Festival\sync.pp" \
"common\Festival\timer.pp" \
"common\Festival\timers_dsp.pp" 

OBJS__QUOTED += \
"common\Festival\dcf.obj" \
"common\Festival\emcy.obj" \
"common\Festival\lifegrd.obj" \
"common\Festival\nmtMaster.obj" \
"common\Festival\nmtSlave.obj" \
"common\Festival\objacces.obj" \
"common\Festival\pdo.obj" \
"common\Festival\sdo.obj" \
"common\Festival\states.obj" \
"common\Festival\sync.obj" \
"common\Festival\timer.obj" \
"common\Festival\timers_dsp.obj" 

C_SRCS__QUOTED += \
"../common/Festival/dcf.c" \
"../common/Festival/emcy.c" \
"../common/Festival/lifegrd.c" \
"../common/Festival/nmtMaster.c" \
"../common/Festival/nmtSlave.c" \
"../common/Festival/objacces.c" \
"../common/Festival/pdo.c" \
"../common/Festival/sdo.c" \
"../common/Festival/states.c" \
"../common/Festival/sync.c" \
"../common/Festival/timer.c" \
"../common/Festival/timers_dsp.c" 


