;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Mon Jul 25 10:11:42 2022                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Emosys_Controller\PTM075")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$120)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$121)
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$34)
	.dwendtag $C$DW$1


$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("memcmp")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_memcmp")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$126)
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$126)
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$33)
	.dwendtag $C$DW$5


$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("SetAlarm")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_SetAlarm")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$66)
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$13)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$129)
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$15)
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$15)
	.dwendtag $C$DW$9


$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("DelAlarm")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_DelAlarm")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$15


$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("memset")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_memset")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$3)
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$10)
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$33)
	.dwendtag $C$DW$17


$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("_setODentry")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("__setODentry")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$66)
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$9)
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$6)
$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$3)
$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$73)
$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$6)
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$21


$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$30, DW_AT_declaration
	.dwattr $C$DW$30, DW_AT_external
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$66)
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$9)
$C$DW$33	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$73)
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$89)
	.dwendtag $C$DW$30


$C$DW$35	.dwtag  DW_TAG_subprogram, DW_AT_name("_getODentry")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("__getODentry")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
$C$DW$36	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$66)
$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$6)
$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$3)
$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$73)
$C$DW$41	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$44)
$C$DW$42	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$6)
$C$DW$43	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
$C$DW$44	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$35

$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("usb_tx_mbox")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_usb_tx_mbox")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2298813 
	.sect	".text"
	.clink
	.global	_buildPDO

$C$DW$47	.dwtag  DW_TAG_subprogram, DW_AT_name("buildPDO")
	.dwattr $C$DW$47, DW_AT_low_pc(_buildPDO)
	.dwattr $C$DW$47, DW_AT_high_pc(0x00)
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_buildPDO")
	.dwattr $C$DW$47, DW_AT_external
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$47, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$47, DW_AT_TI_begin_line(0x30)
	.dwattr $C$DW$47, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$47, DW_AT_TI_max_frame_size(-44)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 49,column 1,is_stmt,address _buildPDO

	.dwfde $C$DW$CIE, _buildPDO
$C$DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg12]
$C$DW$49	.dwtag  DW_TAG_formal_parameter, DW_AT_name("numPdo")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_numPdo")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg0]
$C$DW$50	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pdo")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_pdo")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _buildPDO                     FR SIZE:  42           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            7 Parameter, 34 Auto,  0 SOE     *
;***************************************************************

_buildPDO:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#42                ; [CPU_U] 
	.dwcfi	cfa_offset, -44
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -10]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("numPdo")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_numPdo")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -11]
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("pdo")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_pdo")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_breg20 -14]
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("TPDO_com")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_TPDO_com")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_breg20 -16]
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("TPDO_map")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_TPDO_map")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_breg20 -18]
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("prp_j")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_prp_j")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -19]
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg20 -20]
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("pMappingCount")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_pMappingCount")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg20 -22]
        MOV       *-SP[11],AL           ; [CPU_] |49| 
        MOVL      *-SP[14],XAR5         ; [CPU_] |49| 
        MOVL      *-SP[10],XAR4         ; [CPU_] |49| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 50,column 30,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |50| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |50| 
        MOVU      ACC,*+XAR4[4]         ; [CPU_] |50| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |50| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |50| 
        LSL       ACC,2                 ; [CPU_] |50| 
        ADDL      XAR6,ACC              ; [CPU_] |50| 
        MOVU      ACC,*-SP[11]          ; [CPU_] |50| 
        LSL       ACC,2                 ; [CPU_] |50| 
        ADDL      XAR6,ACC              ; [CPU_] |50| 
        MOVL      *-SP[16],XAR6         ; [CPU_] |50| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 51,column 30,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |51| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |51| 
        MOVU      ACC,*+XAR4[5]         ; [CPU_] |51| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |51| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |51| 
        LSL       ACC,2                 ; [CPU_] |51| 
        ADDL      XAR6,ACC              ; [CPU_] |51| 
        MOVU      ACC,*-SP[11]          ; [CPU_] |51| 
        LSL       ACC,2                 ; [CPU_] |51| 
        ADDL      XAR6,ACC              ; [CPU_] |51| 
        MOVL      *-SP[18],XAR6         ; [CPU_] |51| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 53,column 14,is_stmt
        MOV       *-SP[19],#0           ; [CPU_] |53| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 54,column 15,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |54| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 55,column 29,is_stmt
        MOVL      XAR4,*-SP[18]         ; [CPU_] |55| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |55| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |55| 
        MOVL      *-SP[22],ACC          ; [CPU_] |55| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 57,column 3,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |57| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |57| 
        MOVB      XAR0,#12              ; [CPU_] |57| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |57| 
        MOVL      XAR5,*-SP[14]         ; [CPU_] |57| 
        AND       AL,*+XAR4[0],#0x07ff  ; [CPU_] |57| 
        MOV       *+XAR5[0],AL          ; [CPU_] |57| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 58,column 3,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |58| 
        MOV       *+XAR4[1],#0          ; [CPU_] |58| 
$C$L1:    

$C$DW$59	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("dataType")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg20 -23]
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("tmp")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_tmp")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -31]
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("pMappingParameter")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_pMappingParameter")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -34]
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -35]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("Size")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_Size")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -38]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 67,column 18,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |67| 
        SUBB      XAR4,#31              ; [CPU_U] |67| 
        RPT       #7
||     MOV       *XAR4++,#0            ; [CPU_] |67| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 70,column 32,is_stmt
        MOVL      XAR4,*-SP[18]         ; [CPU_] |70| 
        MOV       AL,*-SP[19]           ; [CPU_] |70| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |70| 
        ADDB      AL,#1                 ; [CPU_] |70| 
        MOVU      ACC,AL                ; [CPU_] |70| 
        LSL       ACC,3                 ; [CPU_] |70| 
        ADDL      XAR4,ACC              ; [CPU_] |70| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |70| 
        MOVL      *-SP[34],ACC          ; [CPU_] |70| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 72,column 19,is_stmt
        MOVL      XAR4,*-SP[34]         ; [CPU_] |72| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |72| 
        MOVH      *-SP[35],ACC << 0     ; [CPU_] |72| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 73,column 18,is_stmt
        MOVL      XAR4,*-SP[34]         ; [CPU_] |73| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |73| 
        MOVB      AH,#0                 ; [CPU_] |73| 
        ANDB      AL,#0xff              ; [CPU_] |73| 
        MOVL      *-SP[38],ACC          ; [CPU_] |73| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 76,column 7,is_stmt
        MOVL      ACC,*-SP[38]          ; [CPU_] |76| 
        BF        $C$L3,EQ              ; [CPU_] |76| 
        ; branchcc occurs ; [] |76| 
        MOVB      XAR6,#64              ; [CPU_] |76| 
        MOVU      ACC,*-SP[20]          ; [CPU_] |76| 
        ADDL      ACC,*-SP[38]          ; [CPU_] |76| 
        CMPL      ACC,XAR6              ; [CPU_] |76| 
        B         $C$L3,HI              ; [CPU_] |76| 
        ; branchcc occurs ; [] |76| 

$C$DW$65	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ByteSize")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ByteSize")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_breg20 -40]
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("subIndex")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -41]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 78,column 26,is_stmt
        MOVL      ACC,*-SP[38]          ; [CPU_] |78| 
        CLRC      SXM                   ; [CPU_] 
        SUBB      ACC,#1                ; [CPU_] |78| 
        SFR       ACC,3                 ; [CPU_] |78| 
        ADDB      ACC,#1                ; [CPU_] |78| 
        MOVL      *-SP[40],ACC          ; [CPU_] |78| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 79,column 25,is_stmt
        MOVL      XAR4,*-SP[34]         ; [CPU_] |79| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |79| 
        SFR       ACC,8                 ; [CPU_] |79| 
        ANDB      AL,#0xff              ; [CPU_] |79| 
        MOV       *-SP[41],AL           ; [CPU_] |79| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 86,column 11,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |86| 
        SUBB      XAR5,#40              ; [CPU_U] |86| 
        MOVZ      AR4,SP                ; [CPU_U] |86| 
        MOVU      ACC,AR5               ; [CPU_] |86| 
        SUBB      XAR4,#23              ; [CPU_U] |86| 
        MOVL      *-SP[2],ACC           ; [CPU_] |86| 
        MOV       AL,*-SP[35]           ; [CPU_] |86| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |86| 
        MOV       AH,*-SP[41]           ; [CPU_] |86| 
        MOV       *-SP[5],#0            ; [CPU_] |86| 
        MOVB      *-SP[6],#1,UNC        ; [CPU_] |86| 
        MOV       *-SP[7],#0            ; [CPU_] |86| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |86| 
        MOVZ      AR5,SP                ; [CPU_U] |86| 
        SUBB      XAR5,#31              ; [CPU_U] |86| 
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("__getODentry")
	.dwattr $C$DW$68, DW_AT_TI_call
        LCR       #__getODentry         ; [CPU_] |86| 
        ; call occurs [#__getODentry] ; [] |86| 
        MOVL      XAR6,ACC              ; [CPU_] |86| 
        MOVB      ACC,#0                ; [CPU_] |86| 
        CMPL      ACC,XAR6              ; [CPU_] |86| 
        BF        $C$L2,EQ              ; [CPU_] |86| 
        ; branchcc occurs ; [] |86| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 92,column 15,is_stmt
        MOVB      AL,#255               ; [CPU_] |92| 
        B         $C$L4,UNC             ; [CPU_] |92| 
        ; branch occurs ; [] |92| 
$C$L2:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 95,column 11,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |95| 
        MOV       *-SP[1],#0            ; [CPU_] |95| 
        ANDB      AL,#0x07              ; [CPU_] |95| 
        MOVZ      AR4,SP                ; [CPU_U] |95| 
        MOV       *-SP[2],AL            ; [CPU_] |95| 
        MOV       AL,*-SP[20]           ; [CPU_] |95| 
        MOV       *-SP[3],#0            ; [CPU_] |95| 
        LSR       AL,3                  ; [CPU_] |95| 
        MOVZ      AR6,AL                ; [CPU_] |95| 
        MOVL      ACC,*-SP[14]          ; [CPU_] |95| 
        ADDU      ACC,AR6               ; [CPU_] |95| 
        MOVL      XAR5,ACC              ; [CPU_] |95| 
        SUBB      XAR4,#31              ; [CPU_U] |95| 
        MOV       AL,*-SP[38]           ; [CPU_] |95| 
        MOVB      AH,#0                 ; [CPU_] |95| 
        ADDB      XAR5,#3               ; [CPU_] |95| 
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_name("_CopyBits")
	.dwattr $C$DW$69, DW_AT_TI_call
        LCR       #_CopyBits            ; [CPU_] |95| 
        ; call occurs [#_CopyBits] ; [] |95| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 98,column 11,is_stmt
        MOV       AL,*-SP[38]           ; [CPU_] |98| 
        ADD       *-SP[20],AL           ; [CPU_] |98| 
	.dwendtag $C$DW$65

$C$L3:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 100,column 7,is_stmt
        INC       *-SP[19]              ; [CPU_] |100| 
	.dwendtag $C$DW$59

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 102,column 10,is_stmt
        MOVL      XAR4,*-SP[22]         ; [CPU_] |102| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |102| 
        CMP       AL,*-SP[19]           ; [CPU_] |102| 
        B         $C$L1,HI              ; [CPU_] |102| 
        ; branchcc occurs ; [] |102| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 104,column 3,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |104| 
        MOVL      XAR4,*-SP[14]         ; [CPU_] |104| 
        ADDB      AL,#-1                ; [CPU_] |104| 
        LSR       AL,3                  ; [CPU_] |104| 
        ADDB      AL,#1                 ; [CPU_] |104| 
        MOV       *+XAR4[2],AL          ; [CPU_] |104| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 108,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |108| 
$C$L4:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 109,column 1,is_stmt
        SUBB      SP,#42                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$47, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$47, DW_AT_TI_end_line(0x6d)
	.dwattr $C$DW$47, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$47

	.sect	".text"
	.clink
	.global	_sendPDOrequest

$C$DW$71	.dwtag  DW_TAG_subprogram, DW_AT_name("sendPDOrequest")
	.dwattr $C$DW$71, DW_AT_low_pc(_sendPDOrequest)
	.dwattr $C$DW$71, DW_AT_high_pc(0x00)
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_sendPDOrequest")
	.dwattr $C$DW$71, DW_AT_external
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$71, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$71, DW_AT_TI_begin_line(0x77)
	.dwattr $C$DW$71, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$71, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 120,column 1,is_stmt,address _sendPDOrequest

	.dwfde $C$DW$CIE, _sendPDOrequest
$C$DW$72	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg12]
$C$DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_name("RPDOIndex")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_RPDOIndex")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _sendPDOrequest               FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_sendPDOrequest:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -2]
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("RPDOIndex")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_RPDOIndex")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -3]
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("pwCobId")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_pwCobId")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -6]
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$77, DW_AT_location[DW_OP_breg20 -7]
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("lastIndex")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[3],AL            ; [CPU_] |120| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |120| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 122,column 16,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |122| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |122| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |122| 
        MOV       *-SP[7],AL            ; [CPU_] |122| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 123,column 19,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |123| 
        MOVB      XAR0,#8               ; [CPU_] |123| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |123| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |123| 
        MOV       *-SP[8],AL            ; [CPU_] |123| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 131,column 3,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |131| 
        BF        $C$L10,EQ             ; [CPU_] |131| 
        ; branchcc occurs ; [] |131| 
        MOV       AL,*-SP[3]            ; [CPU_] |131| 
        CMP       AL,#5120              ; [CPU_] |131| 
        B         $C$L10,LO             ; [CPU_] |131| 
        ; branchcc occurs ; [] |131| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 133,column 7,is_stmt
        ADD       AL,*-SP[7]            ; [CPU_] |133| 
        SUB       AL,#5120              ; [CPU_] |133| 
        MOV       *-SP[7],AL            ; [CPU_] |133| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 134,column 7,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |134| 
        CMP       AL,*-SP[7]            ; [CPU_] |134| 
        B         $C$L10,LO             ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 137,column 11,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |137| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |137| 
        MOVU      ACC,*-SP[7]           ; [CPU_] |137| 
        LSL       ACC,2                 ; [CPU_] |137| 
        ADDL      XAR4,ACC              ; [CPU_] |137| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |137| 
        MOVB      XAR0,#12              ; [CPU_] |137| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |137| 
        MOVL      *-SP[6],ACC           ; [CPU_] |137| 

$C$DW$79	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("pdo")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_pdo")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_breg20 -19]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 142,column 13,is_stmt
        MOVL      XAR7,*-SP[6]          ; [CPU_] |142| 
        MOV       AL,*XAR7              ; [CPU_] |142| 
        MOV       *-SP[19],AL           ; [CPU_] |142| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 143,column 13,is_stmt
        MOVB      *-SP[18],#1,UNC       ; [CPU_] |143| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 144,column 13,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |144| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 145,column 13,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |145| 
        MOVB      XAR0,#249             ; [CPU_] |145| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |145| 
        BF        $C$L7,NEQ             ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
        MOVZ      AR5,SP                ; [CPU_U] |145| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |145| 
        MOVB      AL,#0                 ; [CPU_] |145| 
        SUBB      XAR5,#19              ; [CPU_U] |145| 
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_name("_MBX_post")
	.dwattr $C$DW$81, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |145| 
        ; call occurs [#_MBX_post] ; [] |145| 
        MOVB      AH,#0                 ; [CPU_] |145| 
        MOVB      XAR6,#0               ; [CPU_] |145| 
        CMPB      AL,#0                 ; [CPU_] |145| 
        BF        $C$L5,EQ              ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
        MOVB      AH,#1                 ; [CPU_] |145| 
$C$L5:    
        CMPB      AH,#0                 ; [CPU_] |145| 
        BF        $C$L6,NEQ             ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
        MOVB      XAR6,#1               ; [CPU_] |145| 
$C$L6:    
        MOV       AL,AR6                ; [CPU_] |145| 
        B         $C$L11,UNC            ; [CPU_] |145| 
        ; branch occurs ; [] |145| 
$C$L7:    
        MOVZ      AR5,SP                ; [CPU_U] |145| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |145| 
        MOVB      AL,#0                 ; [CPU_] |145| 
        SUBB      XAR5,#19              ; [CPU_U] |145| 
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_name("_MBX_post")
	.dwattr $C$DW$82, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |145| 
        ; call occurs [#_MBX_post] ; [] |145| 
        MOVB      AH,#0                 ; [CPU_] |145| 
        MOVB      XAR6,#0               ; [CPU_] |145| 
        CMPB      AL,#0                 ; [CPU_] |145| 
        BF        $C$L8,EQ              ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
        MOVB      AH,#1                 ; [CPU_] |145| 
$C$L8:    
        CMPB      AH,#0                 ; [CPU_] |145| 
        BF        $C$L9,NEQ             ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
        MOVB      XAR6,#1               ; [CPU_] |145| 
$C$L9:    
        MOV       AL,AR6                ; [CPU_] |145| 
        B         $C$L11,UNC            ; [CPU_] |145| 
        ; branch occurs ; [] |145| 
	.dwendtag $C$DW$79

$C$L10:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 150,column 3,is_stmt
        MOVB      AL,#255               ; [CPU_] |150| 
$C$L11:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 151,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$83	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$83, DW_AT_low_pc(0x00)
	.dwattr $C$DW$83, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$71, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$71, DW_AT_TI_end_line(0x97)
	.dwattr $C$DW$71, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$71

	.sect	".text"
	.clink
	.global	_proceedPDO

$C$DW$84	.dwtag  DW_TAG_subprogram, DW_AT_name("proceedPDO")
	.dwattr $C$DW$84, DW_AT_low_pc(_proceedPDO)
	.dwattr $C$DW$84, DW_AT_high_pc(0x00)
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_proceedPDO")
	.dwattr $C$DW$84, DW_AT_external
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$84, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$84, DW_AT_TI_begin_line(0xa2)
	.dwattr $C$DW$84, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$84, DW_AT_TI_max_frame_size(-42)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 163,column 1,is_stmt,address _proceedPDO

	.dwfde $C$DW$CIE, _proceedPDO
$C$DW$85	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg12]
$C$DW$86	.dwtag  DW_TAG_formal_parameter, DW_AT_name("m")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _proceedPDO                   FR SIZE:  40           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            5 Parameter, 34 Auto,  0 SOE     *
;***************************************************************

_proceedPDO:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#40                ; [CPU_U] 
	.dwcfi	cfa_offset, -42
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -8]
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -10]
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("numPdo")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_numPdo")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -11]
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("numMap")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_numMap")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -12]
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("pMappingCount")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_pMappingCount")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg20 -14]
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("pMappingParameter")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_pMappingParameter")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg20 -16]
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("pTransmissionType")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_pTransmissionType")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg20 -18]
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("pwCobId")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_pwCobId")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$94, DW_AT_location[DW_OP_breg20 -20]
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("Size")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_Size")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_breg20 -21]
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_breg20 -22]
$C$DW$97	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_breg20 -23]
$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("objDict")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_objDict")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_breg20 -26]
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("offsetObjdict")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_offsetObjdict")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_breg20 -27]
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("lastIndex")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -28]
        MOVL      *-SP[10],XAR5         ; [CPU_] |163| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |163| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 166,column 23,is_stmt
        MOVB      ACC,#0                ; [CPU_] |166| 
        MOVL      *-SP[14],ACC          ; [CPU_] |166| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 171,column 28,is_stmt
        MOVL      *-SP[16],ACC          ; [CPU_] |171| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 172,column 27,is_stmt
        MOVL      *-SP[18],ACC          ; [CPU_] |172| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 174,column 18,is_stmt
        MOVL      *-SP[20],ACC          ; [CPU_] |174| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 182,column 3,is_stmt
        MOVB      *-SP[23],#2,UNC       ; [CPU_] |182| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 185,column 3,is_stmt
        MOV       *-SP[22],#0           ; [CPU_] |185| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 186,column 3,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |186| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 187,column 3,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |187| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 188,column 3,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |188| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |188| 
        BF        $C$L22,NEQ            ; [CPU_] |188| 
        ; branchcc occurs ; [] |188| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 192,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |192| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |192| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |192| 
        MOV       *-SP[27],AL           ; [CPU_] |192| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 193,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |193| 
        MOVB      XAR0,#8               ; [CPU_] |193| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |193| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |193| 
        MOV       *-SP[28],AL           ; [CPU_] |193| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 196,column 5,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |196| 
        BF        $C$L39,EQ             ; [CPU_] |196| 
        ; branchcc occurs ; [] |196| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 197,column 7,is_stmt
        B         $C$L21,UNC            ; [CPU_] |197| 
        ; branch occurs ; [] |197| 
$C$L12:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 206,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |206| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |206| 
        MOVU      ACC,*-SP[27]          ; [CPU_] |206| 
        LSL       ACC,2                 ; [CPU_] |206| 
        ADDL      XAR4,ACC              ; [CPU_] |206| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |206| 
        MOVB      XAR0,#12              ; [CPU_] |206| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |206| 
        MOVL      *-SP[20],ACC          ; [CPU_] |206| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 211,column 15,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |211| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |211| 
        MOVL      XAR4,*-SP[20]         ; [CPU_] |211| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |211| 
        BF        $C$L13,NEQ            ; [CPU_] |211| 
        ; branchcc occurs ; [] |211| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 214,column 19,is_stmt
        MOVB      *-SP[23],#4,UNC       ; [CPU_] |214| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 217,column 19,is_stmt
        B         $C$L21,UNC            ; [CPU_] |217| 
        ; branch occurs ; [] |217| 
$C$L13:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 223,column 19,is_stmt
        INC       *-SP[11]              ; [CPU_] |223| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 224,column 19,is_stmt
        INC       *-SP[27]              ; [CPU_] |224| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 225,column 19,is_stmt
        MOVB      *-SP[23],#2,UNC       ; [CPU_] |225| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 226,column 19,is_stmt
        B         $C$L21,UNC            ; [CPU_] |226| 
        ; branch occurs ; [] |226| 
$C$L14:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 232,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |232| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |232| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |232| 
        MOV       *-SP[27],AL           ; [CPU_] |232| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 233,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |233| 
        MOVB      XAR0,#8               ; [CPU_] |233| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |233| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |233| 
        MOV       *-SP[28],AL           ; [CPU_] |233| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 234,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |234| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |234| 
        MOVU      ACC,*-SP[27]          ; [CPU_] |234| 
        LSL       ACC,2                 ; [CPU_] |234| 
        ADDL      XAR4,ACC              ; [CPU_] |234| 
        MOVU      ACC,*-SP[11]          ; [CPU_] |234| 
        LSL       ACC,2                 ; [CPU_] |234| 
        ADDL      XAR4,ACC              ; [CPU_] |234| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |234| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |234| 
        MOVL      *-SP[14],ACC          ; [CPU_] |234| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 237,column 15,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |237| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 238,column 15,is_stmt
        B         $C$L19,UNC            ; [CPU_] |238| 
        ; branch occurs ; [] |238| 
$C$L15:    

$C$DW$101	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("tmp")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_tmp")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -36]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("ByteSize")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_ByteSize")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -38]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 240,column 30,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |240| 
        SUBB      XAR4,#36              ; [CPU_U] |240| 
        RPT       #7
||     MOV       *XAR4++,#0            ; [CPU_] |240| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 242,column 19,is_stmt
        MOVL      XAR5,*-SP[8]          ; [CPU_] |242| 
        MOVZ      AR4,*-SP[12]          ; [CPU_] |242| 
        MOVL      XAR5,*+XAR5[2]        ; [CPU_] |242| 
        MOVU      ACC,*-SP[27]          ; [CPU_] |242| 
        LSL       ACC,2                 ; [CPU_] |242| 
        ADDB      XAR4,#1               ; [CPU_] |242| 
        ADDL      XAR5,ACC              ; [CPU_] |242| 
        MOVU      ACC,*-SP[11]          ; [CPU_] |242| 
        LSL       ACC,2                 ; [CPU_] |242| 
        ADDL      XAR5,ACC              ; [CPU_] |242| 
        MOVU      ACC,AR4               ; [CPU_] |242| 
        MOVL      XAR4,*+XAR5[0]        ; [CPU_] |242| 
        LSL       ACC,3                 ; [CPU_] |242| 
        ADDL      XAR4,ACC              ; [CPU_] |242| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |242| 
        MOVL      *-SP[16],ACC          ; [CPU_] |242| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 245,column 19,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |245| 
        BF        $C$L16,NEQ            ; [CPU_] |245| 
        ; branchcc occurs ; [] |245| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 249,column 23,is_stmt
        MOVB      AL,#255               ; [CPU_] |249| 
        B         $C$L40,UNC            ; [CPU_] |249| 
        ; branch occurs ; [] |249| 
$C$L16:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 258,column 19,is_stmt
        MOVL      XAR4,*-SP[16]         ; [CPU_] |258| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |258| 
        ANDB      AL,#0xff              ; [CPU_] |258| 
        MOV       *-SP[21],AL           ; [CPU_] |258| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 262,column 19,is_stmt
        BF        $C$L18,EQ             ; [CPU_] |262| 
        ; branchcc occurs ; [] |262| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |262| 
        MOV       ACC,*+XAR4[2] << #3   ; [CPU_] |262| 
        MOV       AH,*-SP[21]           ; [CPU_] |262| 
        ADD       AH,*-SP[22]           ; [CPU_] |262| 
        CMP       AH,AL                 ; [CPU_] |262| 
        B         $C$L18,HI             ; [CPU_] |262| 
        ; branchcc occurs ; [] |262| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 265,column 23,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |265| 
        MOV       AL,*-SP[22]           ; [CPU_] |265| 
        MOV       *-SP[2],#0            ; [CPU_] |265| 
        LSR       AL,3                  ; [CPU_] |265| 
        MOVZ      AR5,SP                ; [CPU_U] |265| 
        MOV       *-SP[3],#0            ; [CPU_] |265| 
        MOVZ      AR6,AL                ; [CPU_] |265| 
        MOVL      ACC,*-SP[10]          ; [CPU_] |265| 
        ADDU      ACC,AR6               ; [CPU_] |265| 
        MOVL      XAR4,ACC              ; [CPU_] |265| 
        AND       AH,*-SP[22],#0x0007   ; [CPU_] |265| 
        MOV       AL,*-SP[21]           ; [CPU_] |265| 
        SUBB      XAR5,#36              ; [CPU_U] |265| 
        ADDB      XAR4,#3               ; [CPU_] |265| 
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_name("_CopyBits")
	.dwattr $C$DW$104, DW_AT_TI_call
        LCR       #_CopyBits            ; [CPU_] |265| 
        ; call occurs [#_CopyBits] ; [] |265| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 268,column 23,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |268| 
        ADDB      AL,#-1                ; [CPU_] |268| 
        LSR       AL,3                  ; [CPU_] |268| 
        ADDB      AL,#1                 ; [CPU_] |268| 
        MOVU      ACC,AL                ; [CPU_] |268| 
        MOVL      *-SP[38],ACC          ; [CPU_] |268| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 270,column 23,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |270| 
        SUBB      XAR4,#38              ; [CPU_U] |270| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |270| 
        MOV       *-SP[3],#0            ; [CPU_] |270| 
        MOVB      *-SP[4],#1,UNC        ; [CPU_] |270| 
        MOV       *-SP[5],#0            ; [CPU_] |270| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |270| 
        MOVZ      AR5,SP                ; [CPU_U] |270| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |270| 
        MOVU      ACC,AH                ; [CPU_] |270| 
        MOVL      XAR4,*-SP[16]         ; [CPU_] |270| 
        CLRC      SXM                   ; [CPU_] 
        MOVZ      AR6,AL                ; [CPU_] |270| 
        SUBB      XAR5,#36              ; [CPU_U] |270| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |270| 
        SFR       ACC,8                 ; [CPU_] |270| 
        ANDB      AL,#0xff              ; [CPU_] |270| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |270| 
        MOV       AH,AL                 ; [CPU_] |270| 
        MOV       AL,AR6                ; [CPU_] |270| 
$C$DW$105	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$105, DW_AT_low_pc(0x00)
	.dwattr $C$DW$105, DW_AT_name("__setODentry")
	.dwattr $C$DW$105, DW_AT_TI_call
        LCR       #__setODentry         ; [CPU_] |270| 
        ; call occurs [#__setODentry] ; [] |270| 
        MOVL      *-SP[26],ACC          ; [CPU_] |270| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 275,column 23,is_stmt
        MOVL      ACC,*-SP[26]          ; [CPU_] |275| 
        BF        $C$L17,EQ             ; [CPU_] |275| 
        ; branchcc occurs ; [] |275| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 284,column 27,is_stmt
        MOVB      AL,#255               ; [CPU_] |284| 
        B         $C$L40,UNC            ; [CPU_] |284| 
        ; branch occurs ; [] |284| 
$C$L17:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 294,column 23,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |294| 
        ADD       *-SP[22],AL           ; [CPU_] |294| 
$C$L18:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 296,column 19,is_stmt
        INC       *-SP[12]              ; [CPU_] |296| 
	.dwendtag $C$DW$101

$C$L19:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 238,column 22,is_stmt
        MOVL      XAR4,*-SP[14]         ; [CPU_] |238| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |238| 
        CMP       AL,*-SP[12]           ; [CPU_] |238| 
        B         $C$L15,HI             ; [CPU_] |238| 
        ; branchcc occurs ; [] |238| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 299,column 15,is_stmt
        MOVB      AL,#0                 ; [CPU_] |299| 
        B         $C$L40,UNC            ; [CPU_] |299| 
        ; branch occurs ; [] |299| 
$C$L20:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 200,column 11,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |200| 
        CMPB      AL,#2                 ; [CPU_] |200| 
        BF        $C$L12,EQ             ; [CPU_] |200| 
        ; branchcc occurs ; [] |200| 
        CMPB      AL,#4                 ; [CPU_] |200| 
        BF        $C$L14,EQ             ; [CPU_] |200| 
        ; branchcc occurs ; [] |200| 
$C$L21:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 197,column 14,is_stmt
        MOV       AL,*-SP[28]           ; [CPU_] |197| 
        CMP       AL,*-SP[27]           ; [CPU_] |197| 
        B         $C$L20,HIS            ; [CPU_] |197| 
        ; branchcc occurs ; [] |197| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 303,column 3,is_stmt
        B         $C$L39,UNC            ; [CPU_] |303| 
        ; branch occurs ; [] |303| 
$C$L22:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 304,column 8,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |304| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |304| 
        CMPB      AL,#1                 ; [CPU_] |304| 
        BF        $C$L39,NEQ            ; [CPU_] |304| 
        ; branchcc occurs ; [] |304| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 307,column 5,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |307| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 308,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |308| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |308| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |308| 
        MOV       *-SP[27],AL           ; [CPU_] |308| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 309,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |309| 
        MOVB      XAR0,#8               ; [CPU_] |309| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |309| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |309| 
        MOV       *-SP[28],AL           ; [CPU_] |309| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 310,column 5,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |310| 
        BF        $C$L39,EQ             ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 311,column 7,is_stmt
        MOV       T,#14                 ; [CPU_] |344| 
        B         $C$L38,UNC            ; [CPU_] |311| 
        ; branch occurs ; [] |311| 
$C$L23:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 319,column 13,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |319| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |319| 
        MOVU      ACC,*-SP[27]          ; [CPU_] |319| 
        LSL       ACC,2                 ; [CPU_] |319| 
        ADDL      XAR4,ACC              ; [CPU_] |319| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |319| 
        MOVB      XAR0,#12              ; [CPU_] |319| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |319| 
        MOVL      *-SP[20],ACC          ; [CPU_] |319| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 320,column 13,is_stmt
        MOVL      XAR4,*-SP[10]         ; [CPU_] |320| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |320| 
        MOVL      XAR4,*-SP[20]         ; [CPU_] |320| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |320| 
        BF        $C$L24,NEQ            ; [CPU_] |320| 
        ; branchcc occurs ; [] |320| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 322,column 15,is_stmt
        MOVB      *-SP[23],#4,UNC       ; [CPU_] |322| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 323,column 15,is_stmt
        B         $C$L38,UNC            ; [CPU_] |323| 
        ; branch occurs ; [] |323| 
$C$L24:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 327,column 15,is_stmt
        INC       *-SP[11]              ; [CPU_] |327| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 328,column 15,is_stmt
        INC       *-SP[27]              ; [CPU_] |328| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 330,column 13,is_stmt
        MOVB      *-SP[23],#1,UNC       ; [CPU_] |330| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 331,column 11,is_stmt
        B         $C$L38,UNC            ; [CPU_] |331| 
        ; branch occurs ; [] |331| 
$C$L25:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 334,column 13,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |334| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |334| 
        MOVU      ACC,*-SP[27]          ; [CPU_] |334| 
        LSL       ACC,2                 ; [CPU_] |334| 
        ADDL      XAR4,ACC              ; [CPU_] |334| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |334| 
        MOVB      XAR0,#20              ; [CPU_] |334| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |334| 
        MOVL      *-SP[18],ACC          ; [CPU_] |334| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 336,column 13,is_stmt
        MOVL      XAR4,*-SP[18]         ; [CPU_] |336| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |336| 
        CMPB      AL,#253               ; [CPU_] |336| 
        BF        $C$L26,NEQ            ; [CPU_] |336| 
        ; branchcc occurs ; [] |336| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 338,column 15,is_stmt
        MOVB      *-SP[23],#5,UNC       ; [CPU_] |338| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 339,column 15,is_stmt
        B         $C$L38,UNC            ; [CPU_] |339| 
        ; branch occurs ; [] |339| 
$C$L26:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 342,column 18,is_stmt
        MOVL      XAR4,*-SP[18]         ; [CPU_] |342| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |342| 
        CMPB      AL,#252               ; [CPU_] |342| 
        BF        $C$L30,NEQ            ; [CPU_] |342| 
        ; branchcc occurs ; [] |342| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 344,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |344| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |344| 
        MPYXU     ACC,T,*-SP[11]        ; [CPU_] |344| 
        ADDL      XAR4,ACC              ; [CPU_] |344| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |344| 
        BF        $C$L29,NTC            ; [CPU_] |344| 
        ; branchcc occurs ; [] |344| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 347,column 17,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |347| 
        MOVB      XAR0,#249             ; [CPU_] |347| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |347| 
        BF        $C$L27,NEQ            ; [CPU_] |347| 
        ; branchcc occurs ; [] |347| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |347| 
        MPYXU     P,T,*-SP[11]          ; [CPU_] |347| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |347| 
        ADDL      ACC,P                 ; [CPU_] |347| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |347| 
        ADDB      ACC,#3                ; [CPU_] |347| 
        MOVL      XAR5,ACC              ; [CPU_] |347| 
        MOVB      AL,#0                 ; [CPU_] |347| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_MBX_post")
	.dwattr $C$DW$106, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |347| 
        ; call occurs [#_MBX_post] ; [] |347| 
        B         $C$L28,UNC            ; [CPU_] |347| 
        ; branch occurs ; [] |347| 
$C$L27:    
        MOVL      XAR4,*-SP[8]          ; [CPU_] |347| 
        MPYXU     P,T,*-SP[11]          ; [CPU_] |347| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |347| 
        ADDL      ACC,P                 ; [CPU_] |347| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |347| 
        ADDB      ACC,#3                ; [CPU_] |347| 
        MOVL      XAR5,ACC              ; [CPU_] |347| 
        MOVB      AL,#0                 ; [CPU_] |347| 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("_MBX_post")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |347| 
        ; call occurs [#_MBX_post] ; [] |347| 
$C$L28:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 348,column 17,is_stmt
        MOVB      AL,#0                 ; [CPU_] |348| 
        B         $C$L40,UNC            ; [CPU_] |348| 
        ; branch occurs ; [] |348| 
$C$L29:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 355,column 17,is_stmt
        MOVB      *-SP[23],#5,UNC       ; [CPU_] |355| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 357,column 15,is_stmt
        B         $C$L38,UNC            ; [CPU_] |357| 
        ; branch occurs ; [] |357| 
$C$L30:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 359,column 18,is_stmt
        MOVL      XAR4,*-SP[18]         ; [CPU_] |359| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |359| 
        CMPB      AL,#255               ; [CPU_] |359| 
        BF        $C$L31,EQ             ; [CPU_] |359| 
        ; branchcc occurs ; [] |359| 
        MOVL      XAR4,*-SP[18]         ; [CPU_] |359| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |359| 
        CMPB      AL,#254               ; [CPU_] |359| 
        BF        $C$L32,NEQ            ; [CPU_] |359| 
        ; branchcc occurs ; [] |359| 
$C$L31:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 363,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |363| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |363| 
        MOV       T,#14                 ; [CPU_] |363| 
        MPYXU     ACC,T,*-SP[11]        ; [CPU_] |363| 
        ADDL      XAR4,ACC              ; [CPU_] |363| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |363| 
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$108, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |363| 
        ; call occurs [#_DelAlarm] ; [] |363| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |363| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |363| 
        MOV       T,#14                 ; [CPU_] |363| 
        MOVZ      AR6,AL                ; [CPU_] |363| 
        MPYXU     ACC,T,*-SP[11]        ; [CPU_] |363| 
        ADDL      XAR4,ACC              ; [CPU_] |363| 
        MOV       *+XAR4[1],AR6         ; [CPU_] |363| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 364,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |364| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |364| 
        MPYXU     ACC,T,*-SP[11]        ; [CPU_] |364| 
        ADDL      XAR4,ACC              ; [CPU_] |364| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |364| 
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$109, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |364| 
        ; call occurs [#_DelAlarm] ; [] |364| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |364| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |364| 
        MOV       T,#14                 ; [CPU_] |364| 
        MOVZ      AR6,AL                ; [CPU_] |364| 
        MPYXU     ACC,T,*-SP[11]        ; [CPU_] |364| 
        ADDL      XAR4,ACC              ; [CPU_] |364| 
        MOV       *+XAR4[2],AR6         ; [CPU_] |364| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 365,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |365| 
        MPYXU     P,T,*-SP[11]          ; [CPU_] |365| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |365| 
        ADDL      ACC,P                 ; [CPU_] |365| 
        MOVL      XAR4,ACC              ; [CPU_] |365| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |365| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 368,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |368| 
        MOVU      ACC,*-SP[11]          ; [CPU_] |368| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_PDOEventTimerAlarm")
	.dwattr $C$DW$110, DW_AT_TI_call
        LCR       #_PDOEventTimerAlarm  ; [CPU_] |368| 
        ; call occurs [#_PDOEventTimerAlarm] ; [] |368| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 369,column 15,is_stmt
        MOVB      AL,#0                 ; [CPU_] |369| 
        B         $C$L40,UNC            ; [CPU_] |369| 
        ; branch occurs ; [] |369| 
$C$L32:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 375,column 15,is_stmt
        MOVB      AL,#255               ; [CPU_] |375| 
        B         $C$L40,UNC            ; [CPU_] |375| 
        ; branch occurs ; [] |375| 
$C$L33:    

$C$DW$111	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$112	.dwtag  DW_TAG_variable, DW_AT_name("pdo")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_pdo")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_breg20 -39]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 381,column 13,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |381| 
        MOV       AL,*-SP[11]           ; [CPU_] |381| 
        MOVZ      AR5,SP                ; [CPU_U] |381| 
        SUBB      XAR5,#39              ; [CPU_U] |381| 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_name("_buildPDO")
	.dwattr $C$DW$113, DW_AT_TI_call
        LCR       #_buildPDO            ; [CPU_] |381| 
        ; call occurs [#_buildPDO] ; [] |381| 
        CMPB      AL,#0                 ; [CPU_] |381| 
        BF        $C$L34,EQ             ; [CPU_] |381| 
        ; branchcc occurs ; [] |381| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 384,column 15,is_stmt
        MOVB      AL,#255               ; [CPU_] |384| 
        B         $C$L40,UNC            ; [CPU_] |384| 
        ; branch occurs ; [] |384| 
$C$L34:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 386,column 13,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |386| 
        MOVB      XAR0,#249             ; [CPU_] |386| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |386| 
        BF        $C$L35,NEQ            ; [CPU_] |386| 
        ; branchcc occurs ; [] |386| 
        MOVZ      AR5,SP                ; [CPU_U] |386| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |386| 
        MOVB      AL,#0                 ; [CPU_] |386| 
        SUBB      XAR5,#39              ; [CPU_U] |386| 
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("_MBX_post")
	.dwattr $C$DW$114, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |386| 
        ; call occurs [#_MBX_post] ; [] |386| 
        B         $C$L36,UNC            ; [CPU_] |386| 
        ; branch occurs ; [] |386| 
$C$L35:    
        MOVZ      AR5,SP                ; [CPU_U] |386| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |386| 
        MOVB      AL,#0                 ; [CPU_] |386| 
        SUBB      XAR5,#39              ; [CPU_U] |386| 
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_name("_MBX_post")
	.dwattr $C$DW$115, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |386| 
        ; call occurs [#_MBX_post] ; [] |386| 
$C$L36:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 387,column 13,is_stmt
        MOVB      AL,#0                 ; [CPU_] |387| 
        B         $C$L40,UNC            ; [CPU_] |387| 
        ; branch occurs ; [] |387| 
	.dwendtag $C$DW$111

$C$L37:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 314,column 9,is_stmt
        MOV       AL,*-SP[23]           ; [CPU_] |314| 
        CMPB      AL,#1                 ; [CPU_] |314| 
        BF        $C$L23,EQ             ; [CPU_] |314| 
        ; branchcc occurs ; [] |314| 
        CMPB      AL,#4                 ; [CPU_] |314| 
        BF        $C$L25,EQ             ; [CPU_] |314| 
        ; branchcc occurs ; [] |314| 
        CMPB      AL,#5                 ; [CPU_] |314| 
        BF        $C$L33,EQ             ; [CPU_] |314| 
        ; branchcc occurs ; [] |314| 
$C$L38:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 311,column 14,is_stmt
        MOV       AL,*-SP[28]           ; [CPU_] |311| 
        CMP       AL,*-SP[27]           ; [CPU_] |311| 
        B         $C$L37,HIS            ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
$C$L39:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 393,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |393| 
$C$L40:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 394,column 1,is_stmt
        SUBB      SP,#40                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$84, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$84, DW_AT_TI_end_line(0x18a)
	.dwattr $C$DW$84, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$84

	.sect	".text"
	.clink
	.global	_CopyBits

$C$DW$117	.dwtag  DW_TAG_subprogram, DW_AT_name("CopyBits")
	.dwattr $C$DW$117, DW_AT_low_pc(_CopyBits)
	.dwattr $C$DW$117, DW_AT_high_pc(0x00)
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_CopyBits")
	.dwattr $C$DW$117, DW_AT_external
	.dwattr $C$DW$117, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$117, DW_AT_TI_begin_line(0x197)
	.dwattr $C$DW$117, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$117, DW_AT_TI_max_frame_size(-18)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 410,column 1,is_stmt,address _CopyBits

	.dwfde $C$DW$CIE, _CopyBits
$C$DW$118	.dwtag  DW_TAG_formal_parameter, DW_AT_name("NbBits")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_NbBits")
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg0]
$C$DW$119	.dwtag  DW_TAG_formal_parameter, DW_AT_name("SrcByteIndex")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_SrcByteIndex")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg12]
$C$DW$120	.dwtag  DW_TAG_formal_parameter, DW_AT_name("SrcBitIndex")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_SrcBitIndex")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg1]
$C$DW$121	.dwtag  DW_TAG_formal_parameter, DW_AT_name("SrcBigEndian")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_SrcBigEndian")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$121, DW_AT_location[DW_OP_breg20 -19]
$C$DW$122	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DestByteIndex")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_DestByteIndex")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg14]
$C$DW$123	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DestBitIndex")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_DestBitIndex")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_breg20 -20]
$C$DW$124	.dwtag  DW_TAG_formal_parameter, DW_AT_name("DestBigEndian")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_DestBigEndian")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$124, DW_AT_location[DW_OP_breg20 -21]

;***************************************************************
;* FNAME: _CopyBits                     FR SIZE:  16           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  2 SOE     *
;***************************************************************

_CopyBits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -18
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("NbBits")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_NbBits")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_breg20 -1]
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("SrcByteIndex")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_SrcByteIndex")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_breg20 -4]
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("SrcBitIndex")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_SrcBitIndex")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_breg20 -5]
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("DestByteIndex")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_DestByteIndex")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[5],AH            ; [CPU_] |410| 
        MOVL      *-SP[8],XAR5          ; [CPU_] |410| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |410| 
        MOV       *-SP[1],AL            ; [CPU_] |410| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 414,column 10,is_stmt
        CMPB      AL,#0                 ; [CPU_] |414| 
        BF        $C$L53,EQ             ; [CPU_] |414| 
        ; branchcc occurs ; [] |414| 
$C$L41:    

$C$DW$129	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("Vect")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_Vect")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_breg20 -9]
$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("Aligned")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_Aligned")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$131, DW_AT_location[DW_OP_breg20 -10]
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("BoudaryLimit")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_BoudaryLimit")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_breg20 -11]
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("BitsToCopy")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_BitsToCopy")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_breg20 -12]
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("Mask")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_Mask")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_breg20 -13]
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("Filtered")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_Filtered")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -14]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 417,column 19,is_stmt
        MOV       AL,*-SP[20]           ; [CPU_] |417| 
        SUB       AL,*-SP[5]            ; [CPU_] |417| 
        MOV       *-SP[9],AL            ; [CPU_] |417| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 420,column 18,is_stmt
        B         $C$L42,LEQ            ; [CPU_] |420| 
        ; branchcc occurs ; [] |420| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |420| 
        MOV       T,*-SP[9]             ; [CPU_] |420| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |420| 
        LSL       AL,T                  ; [CPU_] |420| 
        B         $C$L43,UNC            ; [CPU_] |420| 
        ; branch occurs ; [] |420| 
$C$L42:    
        MOVL      XAR4,*-SP[4]          ; [CPU_] |420| 
        NEG       AL                    ; [CPU_] |420| 
        MOV       T,AL                  ; [CPU_] |420| 
        MOV       AH,*+XAR4[0]          ; [CPU_] |420| 
        LSR       AH,T                  ; [CPU_] |420| 
        MOV       AL,AH                 ; [CPU_] |420| 
$C$L43:    
        MOV       *-SP[10],AL           ; [CPU_] |420| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 424,column 23,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |424| 
        B         $C$L44,LEQ            ; [CPU_] |424| 
        ; branchcc occurs ; [] |424| 
        MOVB      AL,#8                 ; [CPU_] |424| 
        SUB       AL,*-SP[20]           ; [CPU_] |424| 
        B         $C$L45,UNC            ; [CPU_] |424| 
        ; branch occurs ; [] |424| 
$C$L44:    
        MOVB      AL,#8                 ; [CPU_] |424| 
        SUB       AL,*-SP[5]            ; [CPU_] |424| 
$C$L45:    
        MOV       *-SP[11],AL           ; [CPU_] |424| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 425,column 21,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |425| 
        CMP       AL,*-SP[11]           ; [CPU_] |425| 
        B         $C$L46,LO             ; [CPU_] |425| 
        ; branchcc occurs ; [] |425| 
        MOV       AL,*-SP[11]           ; [CPU_] |425| 
$C$L46:    
        MOV       *-SP[12],AL           ; [CPU_] |425| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 428,column 15,is_stmt
        ADD       AL,*-SP[20]           ; [CPU_] |428| 
        MOVB      AH,#255               ; [CPU_] |428| 
        MOV       T,AL                  ; [CPU_] |428| 
        LSL       AH,T                  ; [CPU_] |428| 
        MOV       T,#8                  ; [CPU_] |428| 
        MOV       AL,T                  ; [CPU_] |428| 
        SUB       AL,*-SP[20]           ; [CPU_] |428| 
        MOV       T,AL                  ; [CPU_] |428| 
        MOVB      AL,#255               ; [CPU_] |428| 
        LSR       AL,T                  ; [CPU_] |428| 
        OR        AL,AH                 ; [CPU_] |428| 
        MOV       *-SP[13],AL           ; [CPU_] |428| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 433,column 19,is_stmt
        NOT       AL                    ; [CPU_] |433| 
        AND       AL,*-SP[10]           ; [CPU_] |433| 
        MOV       *-SP[14],AL           ; [CPU_] |433| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 436,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |436| 
        MOV       AL,*-SP[13]           ; [CPU_] |436| 
        AND       *+XAR4[0],AL          ; [CPU_] |436| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 439,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |439| 
        MOV       AL,*-SP[14]           ; [CPU_] |439| 
        OR        *+XAR4[0],AL          ; [CPU_] |439| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 442,column 5,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |442| 
        ADD       AL,*-SP[5]            ; [CPU_] |442| 
        MOV       *-SP[5],AL            ; [CPU_] |442| 
        CMPB      AL,#7                 ; [CPU_] |442| 
        B         $C$L49,LOS            ; [CPU_] |442| 
        ; branchcc occurs ; [] |442| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 444,column 9,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |444| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 445,column 9,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |445| 
        BF        $C$L47,EQ             ; [CPU_] |445| 
        ; branchcc occurs ; [] |445| 
        MOV       AL,#-1                ; [CPU_] |445| 
        B         $C$L48,UNC            ; [CPU_] |445| 
        ; branch occurs ; [] |445| 
$C$L47:    
        MOVB      AL,#1                 ; [CPU_] |445| 
$C$L48:    
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,AL                ; [CPU_] |445| 
        ADDL      *-SP[4],ACC           ; [CPU_] |445| 
$C$L49:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 450,column 5,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |450| 
        ADD       AL,*-SP[20]           ; [CPU_] |450| 
        CMPB      AL,#7                 ; [CPU_] |450| 
        MOV       *-SP[20],AL           ; [CPU_] |450| 
        B         $C$L52,LOS            ; [CPU_] |450| 
        ; branchcc occurs ; [] |450| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 452,column 9,is_stmt
        MOV       *-SP[20],#0           ; [CPU_] |452| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 453,column 9,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |453| 
        BF        $C$L50,EQ             ; [CPU_] |453| 
        ; branchcc occurs ; [] |453| 
        MOV       AL,#-1                ; [CPU_] |453| 
        B         $C$L51,UNC            ; [CPU_] |453| 
        ; branch occurs ; [] |453| 
$C$L50:    
        MOVB      AL,#1                 ; [CPU_] |453| 
$C$L51:    
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,AL                ; [CPU_] |453| 
        ADDL      *-SP[8],ACC           ; [CPU_] |453| 
$C$L52:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 457,column 5,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |457| 
        SUB       *-SP[1],AL            ; [CPU_] |457| 
	.dwendtag $C$DW$129

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 414,column 10,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |414| 
        BF        $C$L41,NEQ            ; [CPU_] |414| 
        ; branchcc occurs ; [] |414| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 459,column 1,is_stmt
$C$L53:    
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$136	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$136, DW_AT_low_pc(0x00)
	.dwattr $C$DW$136, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$117, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$117, DW_AT_TI_end_line(0x1cb)
	.dwattr $C$DW$117, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$117

	.sect	".text"
	.clink
	.global	_sendPDOevent

$C$DW$137	.dwtag  DW_TAG_subprogram, DW_AT_name("sendPDOevent")
	.dwattr $C$DW$137, DW_AT_low_pc(_sendPDOevent)
	.dwattr $C$DW$137, DW_AT_high_pc(0x00)
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_sendPDOevent")
	.dwattr $C$DW$137, DW_AT_external
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$137, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$137, DW_AT_TI_begin_line(0x1d5)
	.dwattr $C$DW$137, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$137, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 470,column 1,is_stmt,address _sendPDOevent

	.dwfde $C$DW$CIE, _sendPDOevent
$C$DW$138	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _sendPDOevent                 FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_sendPDOevent:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |470| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 472,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |472| 
        MOVB      AL,#0                 ; [CPU_] |472| 
        MOVB      AH,#255               ; [CPU_] |472| 
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("__sendPDOevent")
	.dwattr $C$DW$140, DW_AT_TI_call
        LCR       #__sendPDOevent       ; [CPU_] |472| 
        ; call occurs [#__sendPDOevent] ; [] |472| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 473,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$137, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$137, DW_AT_TI_end_line(0x1d9)
	.dwattr $C$DW$137, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$137

	.sect	".text"
	.clink
	.global	_PDOEventTimerAlarm

$C$DW$142	.dwtag  DW_TAG_subprogram, DW_AT_name("PDOEventTimerAlarm")
	.dwattr $C$DW$142, DW_AT_low_pc(_PDOEventTimerAlarm)
	.dwattr $C$DW$142, DW_AT_high_pc(0x00)
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_PDOEventTimerAlarm")
	.dwattr $C$DW$142, DW_AT_external
	.dwattr $C$DW$142, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$142, DW_AT_TI_begin_line(0x1dc)
	.dwattr $C$DW$142, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$142, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 477,column 1,is_stmt,address _PDOEventTimerAlarm

	.dwfde $C$DW$CIE, _PDOEventTimerAlarm
$C$DW$143	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg12]
$C$DW$144	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pdoNum")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_pdoNum")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PDOEventTimerAlarm           FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_PDOEventTimerAlarm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$145, DW_AT_location[DW_OP_breg20 -2]
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("pdoNum")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_pdoNum")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$146, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],ACC           ; [CPU_] |477| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |477| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 479,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |479| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |479| 
        MOVB      ACC,#14               ; [CPU_] |479| 
        MOVL      XT,ACC                ; [CPU_] |479| 
        IMPYL     ACC,XT,*-SP[4]        ; [CPU_] |479| 
        ADDL      XAR4,ACC              ; [CPU_] |479| 
        MOV       *+XAR4[1],#-1         ; [CPU_] |479| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 481,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |481| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |481| 
        IMPYL     ACC,XT,*-SP[4]        ; [CPU_] |481| 
        ADDL      XAR4,ACC              ; [CPU_] |481| 
        MOV       *+XAR4[3],#0          ; [CPU_] |481| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 482,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |482| 
        MOVB      AL,#0                 ; [CPU_] |482| 
        MOVB      AH,#255               ; [CPU_] |482| 
$C$DW$147	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$147, DW_AT_low_pc(0x00)
	.dwattr $C$DW$147, DW_AT_name("__sendPDOevent")
	.dwattr $C$DW$147, DW_AT_TI_call
        LCR       #__sendPDOevent       ; [CPU_] |482| 
        ; call occurs [#__sendPDOevent] ; [] |482| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 483,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$148	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$148, DW_AT_low_pc(0x00)
	.dwattr $C$DW$148, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$142, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$142, DW_AT_TI_end_line(0x1e3)
	.dwattr $C$DW$142, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$142

	.sect	".text"
	.clink
	.global	_PDOInhibitTimerAlarm

$C$DW$149	.dwtag  DW_TAG_subprogram, DW_AT_name("PDOInhibitTimerAlarm")
	.dwattr $C$DW$149, DW_AT_low_pc(_PDOInhibitTimerAlarm)
	.dwattr $C$DW$149, DW_AT_high_pc(0x00)
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_PDOInhibitTimerAlarm")
	.dwattr $C$DW$149, DW_AT_external
	.dwattr $C$DW$149, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$149, DW_AT_TI_begin_line(0x1e5)
	.dwattr $C$DW$149, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$149, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 486,column 1,is_stmt,address _PDOInhibitTimerAlarm

	.dwfde $C$DW$CIE, _PDOInhibitTimerAlarm
$C$DW$150	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg12]
$C$DW$151	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pdoNum")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_pdoNum")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _PDOInhibitTimerAlarm         FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_PDOInhibitTimerAlarm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$152	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_breg20 -2]
$C$DW$153	.dwtag  DW_TAG_variable, DW_AT_name("pdoNum")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_pdoNum")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$153, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],ACC           ; [CPU_] |486| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |486| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 488,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |488| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |488| 
        MOVB      ACC,#14               ; [CPU_] |488| 
        MOVL      XT,ACC                ; [CPU_] |488| 
        IMPYL     ACC,XT,*-SP[4]        ; [CPU_] |488| 
        ADDL      XAR4,ACC              ; [CPU_] |488| 
        MOV       *+XAR4[2],#-1         ; [CPU_] |488| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 490,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |490| 
        IMPYL     P,XT,*-SP[4]          ; [CPU_] |490| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |490| 
        ADDL      ACC,P                 ; [CPU_] |490| 
        MOVL      XAR4,ACC              ; [CPU_] |490| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |490| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 491,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |491| 
        MOVB      AL,#0                 ; [CPU_] |491| 
        MOVB      AH,#255               ; [CPU_] |491| 
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("__sendPDOevent")
	.dwattr $C$DW$154, DW_AT_TI_call
        LCR       #__sendPDOevent       ; [CPU_] |491| 
        ; call occurs [#__sendPDOevent] ; [] |491| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 492,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$149, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$149, DW_AT_TI_end_line(0x1ec)
	.dwattr $C$DW$149, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$149

	.sect	".text"
	.clink
	.global	__sendPDOevent

$C$DW$156	.dwtag  DW_TAG_subprogram, DW_AT_name("_sendPDOevent")
	.dwattr $C$DW$156, DW_AT_low_pc(__sendPDOevent)
	.dwattr $C$DW$156, DW_AT_high_pc(0x00)
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("__sendPDOevent")
	.dwattr $C$DW$156, DW_AT_external
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$156, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$156, DW_AT_TI_begin_line(0x1f8)
	.dwattr $C$DW$156, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$156, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 505,column 1,is_stmt,address __sendPDOevent

	.dwfde $C$DW$CIE, __sendPDOevent
$C$DW$157	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$157, DW_AT_location[DW_OP_reg12]
$C$DW$158	.dwtag  DW_TAG_formal_parameter, DW_AT_name("isSyncEvent")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_isSyncEvent")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$158, DW_AT_location[DW_OP_reg0]
$C$DW$159	.dwtag  DW_TAG_formal_parameter, DW_AT_name("TPDOnum")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_TPDOnum")
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$159, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: __sendPDOevent                FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            6 Parameter, 30 Auto,  0 SOE     *
;***************************************************************

__sendPDOevent:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$160	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$160, DW_AT_location[DW_OP_breg20 -8]
$C$DW$161	.dwtag  DW_TAG_variable, DW_AT_name("isSyncEvent")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_isSyncEvent")
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$161, DW_AT_location[DW_OP_breg20 -9]
$C$DW$162	.dwtag  DW_TAG_variable, DW_AT_name("TPDOnum")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_TPDOnum")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$162, DW_AT_location[DW_OP_breg20 -10]
$C$DW$163	.dwtag  DW_TAG_variable, DW_AT_name("pTransmissionType")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_pTransmissionType")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$163, DW_AT_location[DW_OP_breg20 -12]
$C$DW$164	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$164, DW_AT_location[DW_OP_breg20 -13]
$C$DW$165	.dwtag  DW_TAG_variable, DW_AT_name("pdoNum")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_pdoNum")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_breg20 -14]
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("offsetObjdict")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_offsetObjdict")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -15]
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("offsetObjdictMap")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_offsetObjdictMap")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -16]
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("lastIndex")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$168, DW_AT_location[DW_OP_breg20 -17]
        MOV       *-SP[10],AH           ; [CPU_] |505| 
        MOV       *-SP[9],AL            ; [CPU_] |505| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |505| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 506,column 27,is_stmt
        MOVB      ACC,#0                ; [CPU_] |506| 
        MOVL      *-SP[12],ACC          ; [CPU_] |506| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 507,column 15,is_stmt
        MOVB      *-SP[13],#3,UNC       ; [CPU_] |507| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 508,column 15,is_stmt
        MOV       *-SP[14],#0           ; [CPU_] |508| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 509,column 23,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |509| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |509| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |509| 
        MOV       *-SP[15],AL           ; [CPU_] |509| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 510,column 26,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |510| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |510| 
        MOV       AL,*+XAR4[5]          ; [CPU_] |510| 
        MOV       *-SP[16],AL           ; [CPU_] |510| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 511,column 19,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |511| 
        MOVB      XAR0,#8               ; [CPU_] |511| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |511| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |511| 
        MOV       *-SP[17],AL           ; [CPU_] |511| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 513,column 3,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |513| 
        CMPB      AL,#255               ; [CPU_] |513| 
        BF        $C$L54,EQ             ; [CPU_] |513| 
        ; branchcc occurs ; [] |513| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 514,column 5,is_stmt
        MOV       *-SP[14],AL           ; [CPU_] |514| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 515,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |515| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |515| 
        ADD       AL,*+XAR4[4]          ; [CPU_] |515| 
        MOV       *-SP[15],AL           ; [CPU_] |515| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 516,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |516| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |516| 
        MOV       AL,*-SP[10]           ; [CPU_] |516| 
        ADD       AL,*+XAR4[5]          ; [CPU_] |516| 
        MOV       *-SP[16],AL           ; [CPU_] |516| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 517,column 5,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |517| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |517| 
        MOV       AL,*-SP[10]           ; [CPU_] |517| 
        ADD       AL,*+XAR4[4]          ; [CPU_] |517| 
        MOV       *-SP[17],AL           ; [CPU_] |517| 
$C$L54:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 521,column 3,is_stmt
        MOV       AL,*-SP[15]           ; [CPU_] |521| 
        BF        $C$L76,EQ             ; [CPU_] |521| 
        ; branchcc occurs ; [] |521| 

$C$DW$169	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("pdo")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_pdo")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_breg20 -28]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 524,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |524| 
        SUBB      XAR4,#28              ; [CPU_U] |524| 
        RPT       #10
||     MOV       *XAR4++,#0            ; [CPU_] |524| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 525,column 5,is_stmt
        B         $C$L75,UNC            ; [CPU_] |525| 
        ; branch occurs ; [] |525| 
$C$L55:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 530,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |530| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |530| 
        MOVU      ACC,*-SP[15]          ; [CPU_] |530| 
        LSL       ACC,2                 ; [CPU_] |530| 
        ADDL      XAR4,ACC              ; [CPU_] |530| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |530| 
        MOVB      XAR0,#12              ; [CPU_] |530| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |530| 
        MOVB      XAR6,#0               ; [CPU_] |530| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |530| 
        AND       ACC,#32768 << 16      ; [CPU_] |530| 
        CMPL      ACC,XAR6              ; [CPU_] |530| 
        BF        $C$L56,EQ             ; [CPU_] |530| 
        ; branchcc occurs ; [] |530| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 538,column 15,is_stmt
        MOVB      *-SP[13],#11,UNC      ; [CPU_] |538| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 539,column 15,is_stmt
        B         $C$L75,UNC            ; [CPU_] |539| 
        ; branch occurs ; [] |539| 
$C$L56:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 542,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |542| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |542| 
        MOVU      ACC,*-SP[15]          ; [CPU_] |542| 
        LSL       ACC,2                 ; [CPU_] |542| 
        ADDL      XAR4,ACC              ; [CPU_] |542| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |542| 
        MOVB      XAR0,#20              ; [CPU_] |542| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |542| 
        MOVL      *-SP[12],ACC          ; [CPU_] |542| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 548,column 11,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |548| 
        BF        $C$L58,EQ             ; [CPU_] |548| 
        ; branchcc occurs ; [] |548| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |548| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |548| 
        BF        $C$L58,EQ             ; [CPU_] |548| 
        ; branchcc occurs ; [] |548| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |548| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |548| 
        CMPB      AL,#240               ; [CPU_] |548| 
        B         $C$L58,HI             ; [CPU_] |548| 
        ; branchcc occurs ; [] |548| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |548| 
        MOV       T,#14                 ; [CPU_] |548| 
        MPYXU     P,T,*-SP[14]          ; [CPU_] |548| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |548| 
        ADDL      ACC,P                 ; [CPU_] |548| 
        MOVL      XAR4,ACC              ; [CPU_] |548| 
        INC       *+XAR4[0]             ; [CPU_] |548| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |548| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |548| 
        MOVZ      AR6,AL                ; [CPU_] |548| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |548| 
        CMPL      ACC,XAR6              ; [CPU_] |548| 
        BF        $C$L58,NEQ            ; [CPU_] |548| 
        ; branchcc occurs ; [] |548| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 555,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |555| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |555| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |555| 
        ADDL      XAR4,ACC              ; [CPU_] |555| 
        MOV       *+XAR4[0],#0          ; [CPU_] |555| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 558,column 15,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |558| 
        SUBB      XAR4,#28              ; [CPU_U] |558| 
        RPT       #10
||     MOV       *XAR4++,#0            ; [CPU_] |558| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 563,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |563| 
        MOV       AL,*-SP[14]           ; [CPU_] |563| 
        MOVZ      AR5,SP                ; [CPU_U] |563| 
        SUBB      XAR5,#28              ; [CPU_U] |563| 
$C$DW$171	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$171, DW_AT_low_pc(0x00)
	.dwattr $C$DW$171, DW_AT_name("_buildPDO")
	.dwattr $C$DW$171, DW_AT_TI_call
        LCR       #_buildPDO            ; [CPU_] |563| 
        ; call occurs [#_buildPDO] ; [] |563| 
        CMPB      AL,#0                 ; [CPU_] |563| 
        BF        $C$L57,EQ             ; [CPU_] |563| 
        ; branchcc occurs ; [] |563| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 567,column 19,is_stmt
        MOVB      *-SP[13],#11,UNC      ; [CPU_] |567| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 568,column 19,is_stmt
        B         $C$L75,UNC            ; [CPU_] |568| 
        ; branch occurs ; [] |568| 
$C$L57:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 570,column 15,is_stmt
        MOVB      *-SP[13],#5,UNC       ; [CPU_] |570| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 572,column 13,is_stmt
        B         $C$L75,UNC            ; [CPU_] |572| 
        ; branch occurs ; [] |572| 
$C$L58:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 573,column 16,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |573| 
        BF        $C$L61,EQ             ; [CPU_] |573| 
        ; branchcc occurs ; [] |573| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |573| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |573| 
        CMPB      AL,#252               ; [CPU_] |573| 
        BF        $C$L61,NEQ            ; [CPU_] |573| 
        ; branchcc occurs ; [] |573| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 575,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |575| 
        MOV       T,#14                 ; [CPU_] |575| 
        MPYXU     P,T,*-SP[14]          ; [CPU_] |575| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |575| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |575| 
        ADDL      ACC,P                 ; [CPU_] |575| 
        ADDB      ACC,#3                ; [CPU_] |575| 
        MOVL      XAR5,ACC              ; [CPU_] |575| 
        MOV       AL,*-SP[14]           ; [CPU_] |575| 
$C$DW$172	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$172, DW_AT_low_pc(0x00)
	.dwattr $C$DW$172, DW_AT_name("_buildPDO")
	.dwattr $C$DW$172, DW_AT_TI_call
        LCR       #_buildPDO            ; [CPU_] |575| 
        ; call occurs [#_buildPDO] ; [] |575| 
        CMPB      AL,#0                 ; [CPU_] |575| 
        BF        $C$L59,EQ             ; [CPU_] |575| 
        ; branchcc occurs ; [] |575| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 580,column 19,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |580| 
        MOV       T,#14                 ; [CPU_] |580| 
        MPYXU     P,T,*-SP[14]          ; [CPU_] |580| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |580| 
        ADDL      ACC,P                 ; [CPU_] |580| 
        MOVL      XAR4,ACC              ; [CPU_] |580| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |580| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 582,column 17,is_stmt
        B         $C$L60,UNC            ; [CPU_] |582| 
        ; branch occurs ; [] |582| 
$C$L59:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 585,column 19,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |585| 
        MOV       T,#14                 ; [CPU_] |585| 
        MPYXU     P,T,*-SP[14]          ; [CPU_] |585| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |585| 
        ADDL      ACC,P                 ; [CPU_] |585| 
        MOVL      XAR4,ACC              ; [CPU_] |585| 
        OR        *+XAR4[0],#0x0001     ; [CPU_] |585| 
$C$L60:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 588,column 15,is_stmt
        MOVB      *-SP[13],#11,UNC      ; [CPU_] |588| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 589,column 15,is_stmt
        B         $C$L75,UNC            ; [CPU_] |589| 
        ; branch occurs ; [] |589| 
$C$L61:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 593,column 13,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |593| 
        BF        $C$L62,EQ             ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |593| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |593| 
        BF        $C$L64,EQ             ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
$C$L62:    
        MOV       AL,*-SP[9]            ; [CPU_] |593| 
        BF        $C$L68,NEQ            ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |593| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |593| 
        CMPB      AL,#255               ; [CPU_] |593| 
        BF        $C$L63,EQ             ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
        MOVL      XAR4,*-SP[12]         ; [CPU_] |593| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |593| 
        CMPB      AL,#254               ; [CPU_] |593| 
        BF        $C$L68,NEQ            ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
$C$L63:    
        MOVL      XAR4,*-SP[8]          ; [CPU_] |593| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |593| 
        MOV       T,#14                 ; [CPU_] |593| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |593| 
        ADDL      XAR4,ACC              ; [CPU_] |593| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |593| 
        BF        $C$L68,TC             ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
$C$L64:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 602,column 15,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |602| 
        SUBB      XAR4,#28              ; [CPU_U] |602| 
        RPT       #10
||     MOV       *XAR4++,#0            ; [CPU_] |602| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 603,column 15,is_stmt
        MOV       AL,*-SP[14]           ; [CPU_] |603| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |603| 
        MOVZ      AR5,SP                ; [CPU_U] |603| 
        SUBB      XAR5,#28              ; [CPU_U] |603| 
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_name("_buildPDO")
	.dwattr $C$DW$173, DW_AT_TI_call
        LCR       #_buildPDO            ; [CPU_] |603| 
        ; call occurs [#_buildPDO] ; [] |603| 
        CMPB      AL,#0                 ; [CPU_] |603| 
        BF        $C$L65,EQ             ; [CPU_] |603| 
        ; branchcc occurs ; [] |603| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 607,column 19,is_stmt
        MOVB      *-SP[13],#11,UNC      ; [CPU_] |607| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 608,column 19,is_stmt
        B         $C$L75,UNC            ; [CPU_] |608| 
        ; branch occurs ; [] |608| 
$C$L65:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 612,column 15,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |612| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |612| 
        MOV       T,#14                 ; [CPU_] |612| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |612| 
        ADDL      XAR4,ACC              ; [CPU_] |612| 
        MOVZ      AR6,*+XAR4[3]         ; [CPU_] |612| 
        MOVU      ACC,*-SP[28]          ; [CPU_] |612| 
        CMPL      ACC,XAR6              ; [CPU_] |612| 
        BF        $C$L66,NEQ            ; [CPU_] |612| 
        ; branchcc occurs ; [] |612| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |612| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |612| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |612| 
        ADDL      XAR4,ACC              ; [CPU_] |612| 
        MOVZ      AR6,*+XAR4[5]         ; [CPU_] |612| 
        MOVU      ACC,*-SP[26]          ; [CPU_] |612| 
        CMPL      ACC,XAR6              ; [CPU_] |612| 
        BF        $C$L66,NEQ            ; [CPU_] |612| 
        ; branchcc occurs ; [] |612| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |612| 
        MOVZ      AR5,SP                ; [CPU_U] |612| 
        MPYXU     P,T,*-SP[14]          ; [CPU_] |612| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |612| 
        SUBB      XAR5,#25              ; [CPU_U] |612| 
        ADDL      ACC,P                 ; [CPU_] |612| 
        ADDB      ACC,#6                ; [CPU_] |612| 
        MOVL      XAR4,ACC              ; [CPU_] |612| 
        MOVB      ACC,#8                ; [CPU_] |612| 
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_name("_memcmp")
	.dwattr $C$DW$174, DW_AT_TI_call
        LCR       #_memcmp              ; [CPU_] |612| 
        ; call occurs [#_memcmp] ; [] |612| 
        CMPB      AL,#0                 ; [CPU_] |612| 
        BF        $C$L66,NEQ            ; [CPU_] |612| 
        ; branchcc occurs ; [] |612| 
        MOV       AL,*-SP[10]           ; [CPU_] |612| 
        CMPB      AL,#255               ; [CPU_] |612| 
        BF        $C$L66,NEQ            ; [CPU_] |612| 
        ; branchcc occurs ; [] |612| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 618,column 17,is_stmt
        MOVB      *-SP[13],#11,UNC      ; [CPU_] |618| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 619,column 15,is_stmt
        B         $C$L75,UNC            ; [CPU_] |619| 
        ; branch occurs ; [] |619| 
$C$L66:    

$C$DW$175	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$176	.dwtag  DW_TAG_variable, DW_AT_name("EventTimerDuration")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_EventTimerDuration")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_breg20 -32]
$C$DW$177	.dwtag  DW_TAG_variable, DW_AT_name("InhibitTimerDuration")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_InhibitTimerDuration")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_breg20 -36]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 626,column 17,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |626| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |626| 
        MOVU      ACC,*-SP[15]          ; [CPU_] |626| 
        LSL       ACC,2                 ; [CPU_] |626| 
        ADDL      XAR4,ACC              ; [CPU_] |626| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |626| 
        MOVB      XAR0,#44              ; [CPU_] |626| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |626| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |626| 
        MOVL      *-SP[32],ACC          ; [CPU_] |626| 
        MOV       *-SP[30],#0           ; [CPU_] |626| 
        MOV       *-SP[29],#0           ; [CPU_] |626| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 629,column 17,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |629| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |629| 
        MOVU      ACC,*-SP[15]          ; [CPU_] |629| 
        LSL       ACC,2                 ; [CPU_] |629| 
        ADDL      XAR4,ACC              ; [CPU_] |629| 
        MOVL      XAR4,*+XAR4[0]        ; [CPU_] |629| 
        MOVB      XAR0,#28              ; [CPU_] |629| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |629| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |629| 
        MOVL      *-SP[36],ACC          ; [CPU_] |629| 
        MOV       *-SP[34],#0           ; [CPU_] |629| 
        MOV       *-SP[33],#0           ; [CPU_] |629| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 632,column 17,is_stmt
        MOVB      *-SP[13],#5,UNC       ; [CPU_] |632| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 634,column 17,is_stmt
        MOVB      ACC,#0                ; [CPU_] |634| 
        MOVL      *-SP[4],ACC           ; [CPU_] |634| 
        MOV       *-SP[2],#0            ; [CPU_] |634| 
        MOV       *-SP[1],#0            ; [CPU_] |634| 
        MOVL      P,*-SP[32]            ; [CPU_] |634| 
        MOVL      ACC,*-SP[30]          ; [CPU_] |634| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$178, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |634| 
        ; call occurs [#ULL$$CMP] ; [] |634| 
        CMPB      AL,#0                 ; [CPU_] |634| 
        BF        $C$L67,EQ             ; [CPU_] |634| 
        ; branchcc occurs ; [] |634| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 636,column 21,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |636| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |636| 
        MOV       T,#14                 ; [CPU_] |636| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |636| 
        ADDL      XAR4,ACC              ; [CPU_] |636| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |636| 
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$179, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |636| 
        ; call occurs [#_DelAlarm] ; [] |636| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 637,column 21,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |637| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |637| 
        MOV       *-SP[2],#0            ; [CPU_] |637| 
        MOV       *-SP[1],#0            ; [CPU_] |637| 
        MOVL      P,*-SP[32]            ; [CPU_] |637| 
        MOVL      ACC,*-SP[30]          ; [CPU_] |637| 
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$180, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |637| 
        ; call occurs [#LL$$MPY] ; [] |637| 
        MOVZ      AR7,*-SP[14]          ; [CPU_] |637| 
        MOVB      XAR6,#0               ; [CPU_] |637| 
        MOVL      *-SP[2],XAR7          ; [CPU_] |637| 
        MOVL      *-SP[6],XAR6          ; [CPU_] |637| 
        MOV       *-SP[4],#0            ; [CPU_] |637| 
        MOV       *-SP[3],#0            ; [CPU_] |637| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |637| 
        MOVL      XAR5,#_PDOEventTimerAlarm ; [CPU_U] |637| 
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_SetAlarm")
	.dwattr $C$DW$181, DW_AT_TI_call
        LCR       #_SetAlarm            ; [CPU_] |637| 
        ; call occurs [#_SetAlarm] ; [] |637| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |637| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |637| 
        MOV       T,#14                 ; [CPU_] |637| 
        MOVZ      AR6,AL                ; [CPU_] |637| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |637| 
        ADDL      XAR4,ACC              ; [CPU_] |637| 
        MOV       *+XAR4[1],AR6         ; [CPU_] |637| 
$C$L67:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 642,column 17,is_stmt
        MOVB      ACC,#0                ; [CPU_] |642| 
        MOVL      P,*-SP[36]            ; [CPU_] |642| 
        MOVL      *-SP[4],ACC           ; [CPU_] |642| 
        MOVL      ACC,*-SP[34]          ; [CPU_] |642| 
        MOV       *-SP[2],#0            ; [CPU_] |642| 
        MOV       *-SP[1],#0            ; [CPU_] |642| 
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_name("ULL$$CMP")
	.dwattr $C$DW$182, DW_AT_TI_call
        FFC       XAR7,#ULL$$CMP        ; [CPU_] |642| 
        ; call occurs [#ULL$$CMP] ; [] |642| 
        CMPB      AL,#0                 ; [CPU_] |642| 
        BF        $C$L75,EQ             ; [CPU_] |642| 
        ; branchcc occurs ; [] |642| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 644,column 21,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |644| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |644| 
        MOV       T,#14                 ; [CPU_] |644| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |644| 
        ADDL      XAR4,ACC              ; [CPU_] |644| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |644| 
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$183, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |644| 
        ; call occurs [#_DelAlarm] ; [] |644| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 645,column 21,is_stmt
        MOVB      ACC,#100              ; [CPU_] |645| 
        MOVL      P,*-SP[36]            ; [CPU_] |645| 
        MOVL      *-SP[4],ACC           ; [CPU_] |645| 
        MOVL      ACC,*-SP[34]          ; [CPU_] |645| 
        MOV       *-SP[2],#0            ; [CPU_] |645| 
        MOV       *-SP[1],#0            ; [CPU_] |645| 
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("LL$$MPY")
	.dwattr $C$DW$184, DW_AT_TI_call
        FFC       XAR7,#LL$$MPY         ; [CPU_] |645| 
        ; call occurs [#LL$$MPY] ; [] |645| 
        MOVZ      AR7,*-SP[14]          ; [CPU_] |645| 
        MOVB      XAR6,#0               ; [CPU_] |645| 
        MOVL      *-SP[2],XAR7          ; [CPU_] |645| 
        MOVL      *-SP[6],XAR6          ; [CPU_] |645| 
        MOV       *-SP[4],#0            ; [CPU_] |645| 
        MOV       *-SP[3],#0            ; [CPU_] |645| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |645| 
        MOVL      XAR5,#_PDOInhibitTimerAlarm ; [CPU_U] |645| 
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_SetAlarm")
	.dwattr $C$DW$185, DW_AT_TI_call
        LCR       #_SetAlarm            ; [CPU_] |645| 
        ; call occurs [#_SetAlarm] ; [] |645| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |645| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |645| 
        MOV       T,#14                 ; [CPU_] |645| 
        MOVZ      AR6,AL                ; [CPU_] |645| 
        MPYXU     ACC,T,*-SP[14]        ; [CPU_] |645| 
        ADDL      XAR4,ACC              ; [CPU_] |645| 
        MOV       *+XAR4[2],AR6         ; [CPU_] |645| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 650,column 21,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |650| 
        MPYXU     P,T,*-SP[14]          ; [CPU_] |650| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |650| 
        ADDL      ACC,P                 ; [CPU_] |650| 
        MOVL      XAR4,ACC              ; [CPU_] |650| 
        OR        *+XAR4[0],#0x0001     ; [CPU_] |650| 
	.dwendtag $C$DW$175

	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 655,column 13,is_stmt
        B         $C$L75,UNC            ; [CPU_] |655| 
        ; branch occurs ; [] |655| 
$C$L68:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 661,column 15,is_stmt
        MOVB      *-SP[13],#11,UNC      ; [CPU_] |661| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 663,column 11,is_stmt
        B         $C$L75,UNC            ; [CPU_] |663| 
        ; branch occurs ; [] |663| 
$C$L69:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 666,column 11,is_stmt
        MOV       T,#14                 ; [CPU_] |666| 
        MOVL      XAR4,*-SP[8]          ; [CPU_] |666| 
        MOVZ      AR7,SP                ; [CPU_U] |666| 
        MPYXU     P,T,*-SP[14]          ; [CPU_] |666| 
        SUBB      XAR7,#28              ; [CPU_U] |666| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |666| 
        ADDL      ACC,P                 ; [CPU_] |666| 
        ADDB      ACC,#3                ; [CPU_] |666| 
        MOVL      XAR4,ACC              ; [CPU_] |666| 
        RPT       #10
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |666| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 670,column 11,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |670| 
        MOVB      XAR0,#249             ; [CPU_] |670| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |670| 
        BF        $C$L70,NEQ            ; [CPU_] |670| 
        ; branchcc occurs ; [] |670| 
        MOVZ      AR5,SP                ; [CPU_U] |670| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |670| 
        MOVB      AL,#0                 ; [CPU_] |670| 
        SUBB      XAR5,#28              ; [CPU_U] |670| 
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("_MBX_post")
	.dwattr $C$DW$186, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |670| 
        ; call occurs [#_MBX_post] ; [] |670| 
        B         $C$L71,UNC            ; [CPU_] |670| 
        ; branch occurs ; [] |670| 
$C$L70:    
        MOVZ      AR5,SP                ; [CPU_U] |670| 
        MOVL      XAR4,#_usb_tx_mbox    ; [CPU_U] |670| 
        MOVB      AL,#0                 ; [CPU_] |670| 
        SUBB      XAR5,#28              ; [CPU_U] |670| 
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("_MBX_post")
	.dwattr $C$DW$187, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |670| 
        ; call occurs [#_MBX_post] ; [] |670| 
$C$L71:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 671,column 11,is_stmt
        MOVB      *-SP[13],#11,UNC      ; [CPU_] |671| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 672,column 11,is_stmt
        B         $C$L75,UNC            ; [CPU_] |672| 
        ; branch occurs ; [] |672| 
$C$L72:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 674,column 11,is_stmt
        INC       *-SP[14]              ; [CPU_] |674| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 675,column 11,is_stmt
        INC       *-SP[15]              ; [CPU_] |675| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 676,column 11,is_stmt
        INC       *-SP[16]              ; [CPU_] |676| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 678,column 11,is_stmt
        MOVB      *-SP[13],#3,UNC       ; [CPU_] |678| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 679,column 11,is_stmt
        B         $C$L75,UNC            ; [CPU_] |679| 
        ; branch occurs ; [] |679| 
$C$L73:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 683,column 11,is_stmt
        MOVB      AL,#255               ; [CPU_] |683| 
        B         $C$L77,UNC            ; [CPU_] |683| 
        ; branch occurs ; [] |683| 
$C$L74:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 527,column 7,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |527| 
        CMPB      AL,#3                 ; [CPU_] |527| 
        BF        $C$L55,EQ             ; [CPU_] |527| 
        ; branchcc occurs ; [] |527| 
        CMPB      AL,#5                 ; [CPU_] |527| 
        BF        $C$L69,EQ             ; [CPU_] |527| 
        ; branchcc occurs ; [] |527| 
        CMPB      AL,#11                ; [CPU_] |527| 
        BF        $C$L72,EQ             ; [CPU_] |527| 
        ; branchcc occurs ; [] |527| 
        B         $C$L73,UNC            ; [CPU_] |527| 
        ; branch occurs ; [] |527| 
$C$L75:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 525,column 12,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |525| 
        CMP       AL,*-SP[15]           ; [CPU_] |525| 
        B         $C$L74,HIS            ; [CPU_] |525| 
        ; branchcc occurs ; [] |525| 
	.dwendtag $C$DW$169

$C$L76:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 688,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |688| 
$C$L77:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 689,column 1,is_stmt
        SUBB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$156, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$156, DW_AT_TI_end_line(0x2b1)
	.dwattr $C$DW$156, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$156

	.sect	".text"
	.clink
	.global	_TPDO_Communication_Parameter_Callback

$C$DW$189	.dwtag  DW_TAG_subprogram, DW_AT_name("TPDO_Communication_Parameter_Callback")
	.dwattr $C$DW$189, DW_AT_low_pc(_TPDO_Communication_Parameter_Callback)
	.dwattr $C$DW$189, DW_AT_high_pc(0x00)
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_TPDO_Communication_Parameter_Callback")
	.dwattr $C$DW$189, DW_AT_external
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$189, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$189, DW_AT_TI_begin_line(0x2bc)
	.dwattr $C$DW$189, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$189, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 702,column 1,is_stmt,address _TPDO_Communication_Parameter_Callback

	.dwfde $C$DW$CIE, _TPDO_Communication_Parameter_Callback
$C$DW$190	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_reg12]
$C$DW$191	.dwtag  DW_TAG_formal_parameter, DW_AT_name("OD_entry")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_OD_entry")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_reg14]
$C$DW$192	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$192, DW_AT_location[DW_OP_reg0]
$C$DW$193	.dwtag  DW_TAG_formal_parameter, DW_AT_name("unused_access")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_unused_access")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$193, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _TPDO_Communication_Parameter_Callback FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  9 Auto,  0 SOE     *
;***************************************************************

_TPDO_Communication_Parameter_Callback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$194	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_breg20 -2]
$C$DW$195	.dwtag  DW_TAG_variable, DW_AT_name("OD_entry")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_OD_entry")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_breg20 -4]
$C$DW$196	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$196, DW_AT_location[DW_OP_breg20 -5]
$C$DW$197	.dwtag  DW_TAG_variable, DW_AT_name("unused_access")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_unused_access")
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$197, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |702| 
        MOV       *-SP[5],AL            ; [CPU_] |702| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |702| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |702| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 704,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |704| 
        MOVB      XAR0,#82              ; [CPU_] |704| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |704| 
        BF        $C$L80,EQ             ; [CPU_] |704| 
        ; branchcc occurs ; [] |704| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 705,column 5,is_stmt
        B         $C$L79,UNC            ; [CPU_] |705| 
        ; branch occurs ; [] |705| 
$C$L78:    

$C$DW$198	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$199	.dwtag  DW_TAG_variable, DW_AT_name("TPDO_com")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_TPDO_com")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_breg20 -8]
$C$DW$200	.dwtag  DW_TAG_variable, DW_AT_name("numPdo")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_numPdo")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$200, DW_AT_location[DW_OP_breg20 -9]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 711,column 36,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |711| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |711| 
        MOVU      ACC,*+XAR4[4]         ; [CPU_] |711| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |711| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |711| 
        LSL       ACC,2                 ; [CPU_] |711| 
        ADDL      XAR6,ACC              ; [CPU_] |711| 
        MOVL      *-SP[8],XAR6          ; [CPU_] |711| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 712,column 21,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      ACC,*-SP[4]           ; [CPU_] |712| 
        SUBL      ACC,*-SP[8]           ; [CPU_] |712| 
        SFR       ACC,2                 ; [CPU_] |712| 
        MOV       *-SP[9],AL            ; [CPU_] |712| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 715,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |715| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |715| 
        MOV       T,#14                 ; [CPU_] |715| 
        MPYXU     ACC,T,*-SP[9]         ; [CPU_] |715| 
        ADDL      XAR4,ACC              ; [CPU_] |715| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |715| 
$C$DW$201	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$201, DW_AT_low_pc(0x00)
	.dwattr $C$DW$201, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$201, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |715| 
        ; call occurs [#_DelAlarm] ; [] |715| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |715| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |715| 
        MOV       T,#14                 ; [CPU_] |715| 
        MOVZ      AR6,AL                ; [CPU_] |715| 
        MPYXU     ACC,T,*-SP[9]         ; [CPU_] |715| 
        ADDL      XAR4,ACC              ; [CPU_] |715| 
        MOV       *+XAR4[1],AR6         ; [CPU_] |715| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 717,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |717| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |717| 
        MPYXU     ACC,T,*-SP[9]         ; [CPU_] |717| 
        ADDL      XAR4,ACC              ; [CPU_] |717| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |717| 
$C$DW$202	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$202, DW_AT_low_pc(0x00)
	.dwattr $C$DW$202, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$202, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |717| 
        ; call occurs [#_DelAlarm] ; [] |717| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |717| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |717| 
        MOV       T,#14                 ; [CPU_] |717| 
        MOVZ      AR6,AL                ; [CPU_] |717| 
        MPYXU     ACC,T,*-SP[9]         ; [CPU_] |717| 
        ADDL      XAR4,ACC              ; [CPU_] |717| 
        MOV       *+XAR4[2],AR6         ; [CPU_] |717| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 719,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |719| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |719| 
        MPYXU     ACC,T,*-SP[9]         ; [CPU_] |719| 
        ADDL      XAR4,ACC              ; [CPU_] |719| 
        MOV       *+XAR4[0],#0          ; [CPU_] |719| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 721,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |721| 
        MOVU      ACC,*-SP[9]           ; [CPU_] |721| 
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("_PDOEventTimerAlarm")
	.dwattr $C$DW$203, DW_AT_TI_call
        LCR       #_PDOEventTimerAlarm  ; [CPU_] |721| 
        ; call occurs [#_PDOEventTimerAlarm] ; [] |721| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 722,column 9,is_stmt
        MOVB      ACC,#0                ; [CPU_] |722| 
        B         $C$L81,UNC            ; [CPU_] |722| 
        ; branch occurs ; [] |722| 
	.dwendtag $C$DW$198

$C$L79:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 705,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |705| 
        CMPB      AL,#2                 ; [CPU_] |705| 
        BF        $C$L78,EQ             ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
        CMPB      AL,#3                 ; [CPU_] |705| 
        BF        $C$L78,EQ             ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
        CMPB      AL,#5                 ; [CPU_] |705| 
        BF        $C$L78,EQ             ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
$C$L80:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 729,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |729| 
$C$L81:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 730,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$189, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$189, DW_AT_TI_end_line(0x2da)
	.dwattr $C$DW$189, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$189

	.sect	".text"
	.clink
	.global	_PDOInit

$C$DW$205	.dwtag  DW_TAG_subprogram, DW_AT_name("PDOInit")
	.dwattr $C$DW$205, DW_AT_low_pc(_PDOInit)
	.dwattr $C$DW$205, DW_AT_high_pc(0x00)
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_PDOInit")
	.dwattr $C$DW$205, DW_AT_external
	.dwattr $C$DW$205, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$205, DW_AT_TI_begin_line(0x2dd)
	.dwattr $C$DW$205, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$205, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 734,column 1,is_stmt,address _PDOInit

	.dwfde $C$DW$CIE, _PDOInit
$C$DW$206	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PDOInit                      FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_PDOInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$207	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_breg20 -4]
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("pdoIndex")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_pdoIndex")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_breg20 -5]
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("offsetObjdict")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_offsetObjdict")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_breg20 -6]
$C$DW$210	.dwtag  DW_TAG_variable, DW_AT_name("lastIndex")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$210, DW_AT_location[DW_OP_breg20 -7]
        MOVL      *-SP[4],XAR4          ; [CPU_] |734| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 736,column 18,is_stmt
        MOV       *-SP[5],#6144         ; [CPU_] |736| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 738,column 23,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |738| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |738| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |738| 
        MOV       *-SP[6],AL            ; [CPU_] |738| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 739,column 19,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |739| 
        MOVB      XAR0,#8               ; [CPU_] |739| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |739| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |739| 
        MOV       *-SP[7],AL            ; [CPU_] |739| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 740,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |740| 
        BF        $C$L85,EQ             ; [CPU_] |740| 
        ; branchcc occurs ; [] |740| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 741,column 5,is_stmt
        B         $C$L84,UNC            ; [CPU_] |741| 
        ; branch occurs ; [] |741| 
$C$L82:    

$C$DW$211	.dwtag  DW_TAG_lexical_block, DW_AT_low_pc(0x00), DW_AT_high_pc(0x00)
$C$DW$212	.dwtag  DW_TAG_variable, DW_AT_name("errorCode")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_errorCode")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_breg20 -10]
$C$DW$213	.dwtag  DW_TAG_variable, DW_AT_name("CallbackList")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_CallbackList")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 747,column 9,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |747| 
        SUBB      XAR4,#12              ; [CPU_U] |747| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |747| 
        MOV       AL,*-SP[5]            ; [CPU_] |747| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |747| 
        MOVZ      AR5,SP                ; [CPU_U] |747| 
        SUBB      XAR5,#10              ; [CPU_U] |747| 
$C$DW$214	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$214, DW_AT_low_pc(0x00)
	.dwattr $C$DW$214, DW_AT_name("_scanIndexOD")
	.dwattr $C$DW$214, DW_AT_TI_call
        LCR       #_scanIndexOD         ; [CPU_] |747| 
        ; call occurs [#_scanIndexOD] ; [] |747| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 748,column 9,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |748| 
        BF        $C$L83,NEQ            ; [CPU_] |748| 
        ; branchcc occurs ; [] |748| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |748| 
        BF        $C$L83,EQ             ; [CPU_] |748| 
        ; branchcc occurs ; [] |748| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 752,column 13,is_stmt
        MOVL      XAR5,*-SP[12]         ; [CPU_] |752| 
        MOVL      XAR4,#_TPDO_Communication_Parameter_Callback ; [CPU_U] |752| 
        MOVL      *+XAR5[4],XAR4        ; [CPU_] |752| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 754,column 13,is_stmt
        MOVL      XAR5,*-SP[12]         ; [CPU_] |754| 
        MOVL      *+XAR5[6],XAR4        ; [CPU_] |754| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 756,column 13,is_stmt
        MOVL      XAR5,*-SP[12]         ; [CPU_] |756| 
        MOVB      XAR0,#10              ; [CPU_] |756| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |756| 
$C$L83:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 758,column 9,is_stmt
        INC       *-SP[5]               ; [CPU_] |758| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 759,column 9,is_stmt
        INC       *-SP[6]               ; [CPU_] |759| 
	.dwendtag $C$DW$211

$C$L84:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 741,column 12,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |741| 
        CMP       AL,*-SP[6]            ; [CPU_] |741| 
        B         $C$L82,HIS            ; [CPU_] |741| 
        ; branchcc occurs ; [] |741| 
$C$L85:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 763,column 3,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |763| 
        MOVB      AL,#0                 ; [CPU_] |763| 
        MOVB      AH,#255               ; [CPU_] |763| 
$C$DW$215	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$215, DW_AT_low_pc(0x00)
	.dwattr $C$DW$215, DW_AT_name("__sendPDOevent")
	.dwattr $C$DW$215, DW_AT_TI_call
        LCR       #__sendPDOevent       ; [CPU_] |763| 
        ; call occurs [#__sendPDOevent] ; [] |763| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 764,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$205, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$205, DW_AT_TI_end_line(0x2fc)
	.dwattr $C$DW$205, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$205

	.sect	".text"
	.clink
	.global	_PDOStop

$C$DW$217	.dwtag  DW_TAG_subprogram, DW_AT_name("PDOStop")
	.dwattr $C$DW$217, DW_AT_low_pc(_PDOStop)
	.dwattr $C$DW$217, DW_AT_high_pc(0x00)
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_PDOStop")
	.dwattr $C$DW$217, DW_AT_external
	.dwattr $C$DW$217, DW_AT_TI_begin_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$217, DW_AT_TI_begin_line(0x2ff)
	.dwattr $C$DW$217, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$217, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 768,column 1,is_stmt,address _PDOStop

	.dwfde $C$DW$CIE, _PDOStop
$C$DW$218	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PDOStop                      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_PDOStop:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$219	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$219, DW_AT_location[DW_OP_breg20 -2]
$C$DW$220	.dwtag  DW_TAG_variable, DW_AT_name("pdoNum")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_pdoNum")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_breg20 -3]
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("offsetObjdict")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_offsetObjdict")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_breg20 -4]
$C$DW$222	.dwtag  DW_TAG_variable, DW_AT_name("lastIndex")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_breg20 -5]
        MOVL      *-SP[2],XAR4          ; [CPU_] |768| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 770,column 15,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |770| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 771,column 23,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |771| 
        MOVL      XAR4,*+XAR4[6]        ; [CPU_] |771| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |771| 
        MOV       *-SP[4],AL            ; [CPU_] |771| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 772,column 19,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |772| 
        MOVB      XAR0,#8               ; [CPU_] |772| 
        MOVL      XAR4,*+XAR4[AR0]      ; [CPU_] |772| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |772| 
        MOV       *-SP[5],AL            ; [CPU_] |772| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 773,column 3,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |773| 
        BF        $C$L88,EQ             ; [CPU_] |773| 
        ; branchcc occurs ; [] |773| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 774,column 5,is_stmt
        B         $C$L87,UNC            ; [CPU_] |774| 
        ; branch occurs ; [] |774| 
$C$L86:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 777,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |777| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |777| 
        MOV       T,#14                 ; [CPU_] |777| 
        MPYXU     ACC,T,*-SP[3]         ; [CPU_] |777| 
        ADDL      XAR4,ACC              ; [CPU_] |777| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |777| 
$C$DW$223	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$223, DW_AT_low_pc(0x00)
	.dwattr $C$DW$223, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$223, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |777| 
        ; call occurs [#_DelAlarm] ; [] |777| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |777| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |777| 
        MOV       T,#14                 ; [CPU_] |777| 
        MOVZ      AR6,AL                ; [CPU_] |777| 
        MPYXU     ACC,T,*-SP[3]         ; [CPU_] |777| 
        ADDL      XAR4,ACC              ; [CPU_] |777| 
        MOV       *+XAR4[1],AR6         ; [CPU_] |777| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 779,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |779| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |779| 
        MPYXU     ACC,T,*-SP[3]         ; [CPU_] |779| 
        ADDL      XAR4,ACC              ; [CPU_] |779| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |779| 
$C$DW$224	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$224, DW_AT_low_pc(0x00)
	.dwattr $C$DW$224, DW_AT_name("_DelAlarm")
	.dwattr $C$DW$224, DW_AT_TI_call
        LCR       #_DelAlarm            ; [CPU_] |779| 
        ; call occurs [#_DelAlarm] ; [] |779| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |779| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |779| 
        MOV       T,#14                 ; [CPU_] |779| 
        MOVZ      AR6,AL                ; [CPU_] |779| 
        MPYXU     ACC,T,*-SP[3]         ; [CPU_] |779| 
        ADDL      XAR4,ACC              ; [CPU_] |779| 
        MOV       *+XAR4[2],AR6         ; [CPU_] |779| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 782,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |782| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |782| 
        MPYXU     ACC,T,*-SP[3]         ; [CPU_] |782| 
        ADDL      XAR4,ACC              ; [CPU_] |782| 
        MOV       *+XAR4[0],#0          ; [CPU_] |782| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 783,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |783| 
        MOVL      XAR4,*+XAR4[4]        ; [CPU_] |783| 
        MPYXU     ACC,T,*-SP[3]         ; [CPU_] |783| 
        ADDL      XAR4,ACC              ; [CPU_] |783| 
        MOV       *+XAR4[3],#0          ; [CPU_] |783| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 784,column 9,is_stmt
        INC       *-SP[3]               ; [CPU_] |784| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 785,column 9,is_stmt
        INC       *-SP[4]               ; [CPU_] |785| 
$C$L87:    
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 774,column 12,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |774| 
        CMP       AL,*-SP[4]            ; [CPU_] |774| 
        B         $C$L86,HIS            ; [CPU_] |774| 
        ; branchcc occurs ; [] |774| 
	.dwpsn	file "C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c",line 787,column 1,is_stmt
$C$L88:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$225	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$225, DW_AT_low_pc(0x00)
	.dwattr $C$DW$225, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$217, DW_AT_TI_end_file("C:/Users/aen17/Desktop/Working/SVN/Repository/Emosys_Controller/common/Festival/pdo.c")
	.dwattr $C$DW$217, DW_AT_TI_end_line(0x313)
	.dwattr $C$DW$217, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$217

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_MBX_post
	.global	_memcmp
	.global	_SetAlarm
	.global	_DelAlarm
	.global	_memset
	.global	__setODentry
	.global	_scanIndexOD
	.global	__getODentry
	.global	_usb_tx_mbox
	.global	_can_tx_mbox
	.global	LL$$MPY
	.global	ULL$$CMP

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$226, DW_AT_name("cob_id")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$227, DW_AT_name("rtr")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$228, DW_AT_name("len")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$229, DW_AT_name("data")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)
$C$DW$T$116	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$116, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$230, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$231, DW_AT_name("csSDO")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$232, DW_AT_name("csEmergency")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$233, DW_AT_name("csSYNC")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$234, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$235, DW_AT_name("csPDO")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$236, DW_AT_name("csLSS")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$237, DW_AT_name("errCode")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$238, DW_AT_name("errRegMask")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$239, DW_AT_name("active")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$99	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x18)
$C$DW$240	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$240, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$99


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$241, DW_AT_name("index")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$242, DW_AT_name("subindex")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$243, DW_AT_name("size")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$244, DW_AT_name("address")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x08)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$245, DW_AT_name("wListElem")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$246, DW_AT_name("wCount")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$247, DW_AT_name("fxn")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$26	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x16)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)

$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x30)
$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$248, DW_AT_name("dataQue")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$249, DW_AT_name("freeQue")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$250, DW_AT_name("dataSem")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$251, DW_AT_name("freeSem")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$252, DW_AT_name("segid")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$253, DW_AT_name("size")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$254, DW_AT_name("length")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$255, DW_AT_name("name")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38

$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)
$C$DW$T$120	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$120, DW_AT_language(DW_LANG_C)

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x04)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$256, DW_AT_name("next")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$257, DW_AT_name("prev")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)

$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x10)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$258, DW_AT_name("job")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$259, DW_AT_name("count")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$260, DW_AT_name("pendQ")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$261, DW_AT_name("name")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$121	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$262	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$27)
	.dwendtag $C$DW$T$28

$C$DW$T$29	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x16)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$67	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$263	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$T$67

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)

$C$DW$T$76	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$264	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$66)
$C$DW$265	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$76

$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)
$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$266	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$66)
$C$DW$267	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$6)
$C$DW$268	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$9)
$C$DW$269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x16)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$270	.dwtag  DW_TAG_TI_far_type
$C$DW$T$125	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$270)
$C$DW$T$126	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$126, DW_AT_address_class(0x16)

$C$DW$T$127	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$127, DW_AT_language(DW_LANG_C)
$C$DW$271	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$66)
$C$DW$272	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$T$127

$C$DW$T$128	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$128, DW_AT_address_class(0x16)
$C$DW$T$129	.dwtag  DW_TAG_typedef, DW_AT_name("TimerCallback_t")
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$273	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$273, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$274	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$6)
$C$DW$T$55	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$274)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$144	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$144, DW_AT_language(DW_LANG_C)
$C$DW$275	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$9)
$C$DW$T$53	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$275)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$276	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$6)
$C$DW$277	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$278	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$66)
$C$DW$279	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$47)
$C$DW$280	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$6)
$C$DW$281	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$84

$C$DW$T$85	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_address_class(0x16)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)
$C$DW$282	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$86)
$C$DW$T$87	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$282)
$C$DW$T$88	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x16)
$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)
$C$DW$T$149	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$149, DW_AT_address_class(0x16)

$C$DW$T$93	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$283	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$66)
$C$DW$284	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$9)
$C$DW$285	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$93

$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$96	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$96, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x01)
$C$DW$286	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$287	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$96

$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)

$C$DW$T$62	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$62, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$288	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$289	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$290	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$291	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$292	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$293	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$294	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$295	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$62

$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$79	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x80)
$C$DW$296	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$296, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$79


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x06)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$297, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$298, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$299, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$300, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$301, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$302, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43

$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$303	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$50)
$C$DW$T$51	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$303)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)

$C$DW$T$106	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$106, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x132)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$304, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$305, DW_AT_name("objdict")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$306, DW_AT_name("PDO_status")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$307, DW_AT_name("firstIndex")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$308, DW_AT_name("lastIndex")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$309, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$310, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$311, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$312, DW_AT_name("transfers")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$313, DW_AT_name("nodeState")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$314, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$315, DW_AT_name("initialisation")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$316, DW_AT_name("preOperational")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$317, DW_AT_name("operational")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$318, DW_AT_name("stopped")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$319, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$320, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$321, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$322, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$323, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$324, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$325, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$326, DW_AT_name("heartbeatError")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$327, DW_AT_name("NMTable")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$328, DW_AT_name("syncTimer")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$329, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$330, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$331, DW_AT_name("pre_sync")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$332, DW_AT_name("post_TPDO")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$333, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$334, DW_AT_name("toggle")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$335, DW_AT_name("canHandle")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$336, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$337, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$338, DW_AT_name("globalCallback")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$339, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$340, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$341, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$342, DW_AT_name("dcf_request")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$343, DW_AT_name("error_state")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$344, DW_AT_name("error_history_size")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$345, DW_AT_name("error_number")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$346, DW_AT_name("error_first_element")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$347, DW_AT_name("error_register")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$348, DW_AT_name("error_cobid")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$349, DW_AT_name("error_data")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$350, DW_AT_name("post_emcy")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$351, DW_AT_name("lss_transfer")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$352, DW_AT_name("eeprom_index")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$353, DW_AT_name("eeprom_size")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x0e)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$354, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$355, DW_AT_name("event_timer")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$356, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$357, DW_AT_name("last_message")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108

$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)

$C$DW$T$110	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$110, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x14)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$358, DW_AT_name("nodeId")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$359, DW_AT_name("whoami")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$360, DW_AT_name("state")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$361, DW_AT_name("toggle")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$362, DW_AT_name("abortCode")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$363, DW_AT_name("index")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$364, DW_AT_name("subIndex")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$365, DW_AT_name("port")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$366, DW_AT_name("count")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$367, DW_AT_name("offset")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$368, DW_AT_name("datap")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$369, DW_AT_name("dataType")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$370, DW_AT_name("timer")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$371, DW_AT_name("Callback")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110

$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)

$C$DW$T$61	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x3c)
$C$DW$372	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$372, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$61


$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x04)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$373, DW_AT_name("pSubindex")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$374, DW_AT_name("bSubCount")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$375, DW_AT_name("index")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$376	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$45)
$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$376)
$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x16)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$377	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$9)
$C$DW$378	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$73)
$C$DW$379	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$89)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)

$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x08)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$380, DW_AT_name("bAccessType")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$381, DW_AT_name("bDataType")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$382, DW_AT_name("size")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$383, DW_AT_name("pObject")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$384, DW_AT_name("bProcessor")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115

$C$DW$385	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$115)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$385)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$386	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$386, DW_AT_location[DW_OP_reg0]
$C$DW$387	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$387, DW_AT_location[DW_OP_reg1]
$C$DW$388	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$388, DW_AT_location[DW_OP_reg2]
$C$DW$389	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$389, DW_AT_location[DW_OP_reg3]
$C$DW$390	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$390, DW_AT_location[DW_OP_reg20]
$C$DW$391	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$391, DW_AT_location[DW_OP_reg21]
$C$DW$392	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$392, DW_AT_location[DW_OP_reg22]
$C$DW$393	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$393, DW_AT_location[DW_OP_reg23]
$C$DW$394	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$394, DW_AT_location[DW_OP_reg24]
$C$DW$395	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$395, DW_AT_location[DW_OP_reg25]
$C$DW$396	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$396, DW_AT_location[DW_OP_reg26]
$C$DW$397	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$397, DW_AT_location[DW_OP_reg28]
$C$DW$398	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$398, DW_AT_location[DW_OP_reg29]
$C$DW$399	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$399, DW_AT_location[DW_OP_reg30]
$C$DW$400	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$400, DW_AT_location[DW_OP_reg31]
$C$DW$401	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$401, DW_AT_location[DW_OP_regx 0x20]
$C$DW$402	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$402, DW_AT_location[DW_OP_regx 0x21]
$C$DW$403	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$403, DW_AT_location[DW_OP_regx 0x22]
$C$DW$404	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$404, DW_AT_location[DW_OP_regx 0x23]
$C$DW$405	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$405, DW_AT_location[DW_OP_regx 0x24]
$C$DW$406	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$406, DW_AT_location[DW_OP_regx 0x25]
$C$DW$407	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$407, DW_AT_location[DW_OP_regx 0x26]
$C$DW$408	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$408, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$409	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$409, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$410	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$410, DW_AT_location[DW_OP_reg4]
$C$DW$411	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$411, DW_AT_location[DW_OP_reg6]
$C$DW$412	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$412, DW_AT_location[DW_OP_reg8]
$C$DW$413	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$413, DW_AT_location[DW_OP_reg10]
$C$DW$414	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$414, DW_AT_location[DW_OP_reg12]
$C$DW$415	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$415, DW_AT_location[DW_OP_reg14]
$C$DW$416	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$416, DW_AT_location[DW_OP_reg16]
$C$DW$417	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$417, DW_AT_location[DW_OP_reg17]
$C$DW$418	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$418, DW_AT_location[DW_OP_reg18]
$C$DW$419	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$419, DW_AT_location[DW_OP_reg19]
$C$DW$420	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$420, DW_AT_location[DW_OP_reg5]
$C$DW$421	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg7]
$C$DW$422	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg9]
$C$DW$423	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg11]
$C$DW$424	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$424, DW_AT_location[DW_OP_reg13]
$C$DW$425	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$425, DW_AT_location[DW_OP_reg15]
$C$DW$426	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$426, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$427	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$427, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$428	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$428, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$429	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$429, DW_AT_location[DW_OP_regx 0x30]
$C$DW$430	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$430, DW_AT_location[DW_OP_regx 0x33]
$C$DW$431	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$431, DW_AT_location[DW_OP_regx 0x34]
$C$DW$432	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$432, DW_AT_location[DW_OP_regx 0x37]
$C$DW$433	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$433, DW_AT_location[DW_OP_regx 0x38]
$C$DW$434	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$434, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$435	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$435, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$436	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$436, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$437	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$437, DW_AT_location[DW_OP_regx 0x40]
$C$DW$438	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$438, DW_AT_location[DW_OP_regx 0x43]
$C$DW$439	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$439, DW_AT_location[DW_OP_regx 0x44]
$C$DW$440	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$440, DW_AT_location[DW_OP_regx 0x47]
$C$DW$441	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$441, DW_AT_location[DW_OP_regx 0x48]
$C$DW$442	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$442, DW_AT_location[DW_OP_regx 0x49]
$C$DW$443	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$443, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$444	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$444, DW_AT_location[DW_OP_regx 0x27]
$C$DW$445	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$445, DW_AT_location[DW_OP_regx 0x28]
$C$DW$446	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$446, DW_AT_location[DW_OP_reg27]
$C$DW$447	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$447, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

