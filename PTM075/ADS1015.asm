;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Mon Jul 25 10:11:40 2022                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ADS1015.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Emosys_Controller\PTM075")
	.global	_m_i2cAddress
_m_i2cAddress:	.usect	".ebss",1,1,0
$C$DW$1	.dwtag  DW_TAG_variable, DW_AT_name("m_i2cAddress")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_m_i2cAddress")
	.dwattr $C$DW$1, DW_AT_location[DW_OP_addr _m_i2cAddress]
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1, DW_AT_external
	.global	_m_gain
_m_gain:	.usect	".ebss",1,1,0
$C$DW$2	.dwtag  DW_TAG_variable, DW_AT_name("m_gain")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_m_gain")
	.dwattr $C$DW$2, DW_AT_location[DW_OP_addr _m_gain]
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$2, DW_AT_external
	.global	_m_conversionDelay
_m_conversionDelay:	.usect	".ebss",1,1,0
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("m_conversionDelay")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_m_conversionDelay")
	.dwattr $C$DW$3, DW_AT_location[DW_OP_addr _m_conversionDelay]
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_SwapWord")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_CNV_SwapWord")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$4


$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$9)
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$25)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$9)
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$6

	.global	_m_bitShift
_m_bitShift:	.usect	".ebss",1,1,0
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("m_bitShift")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_m_bitShift")
	.dwattr $C$DW$11, DW_AT_location[DW_OP_addr _m_bitShift]
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$11, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1271212 
	.sect	".text"
	.clink
	.global	_ADS_Init

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_Init")
	.dwattr $C$DW$12, DW_AT_low_pc(_ADS_Init)
	.dwattr $C$DW$12, DW_AT_high_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ADS_Init")
	.dwattr $C$DW$12, DW_AT_external
	.dwattr $C$DW$12, DW_AT_TI_begin_file("../ADS1015.c")
	.dwattr $C$DW$12, DW_AT_TI_begin_line(0x2b)
	.dwattr $C$DW$12, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$12, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../ADS1015.c",line 44,column 1,is_stmt,address _ADS_Init

	.dwfde $C$DW$CIE, _ADS_Init

;***************************************************************
;* FNAME: _ADS_Init                     FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ADS_Init:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../ADS1015.c",line 45,column 4,is_stmt
        MOVW      DP,#_m_conversionDelay ; [CPU_U] 
        MOVB      @_m_conversionDelay,#1,UNC ; [CPU_] |45| 
	.dwpsn	file "../ADS1015.c",line 46,column 4,is_stmt
        MOVB      @_m_bitShift,#4,UNC   ; [CPU_] |46| 
	.dwpsn	file "../ADS1015.c",line 47,column 4,is_stmt
        MOV       @_m_gain,#0           ; [CPU_] |47| 
	.dwpsn	file "../ADS1015.c",line 48,column 1,is_stmt
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$12, DW_AT_TI_end_file("../ADS1015.c")
	.dwattr $C$DW$12, DW_AT_TI_end_line(0x30)
	.dwattr $C$DW$12, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$12

	.sect	".text"
	.clink
	.global	_ADS_SetGain

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_SetGain")
	.dwattr $C$DW$14, DW_AT_low_pc(_ADS_SetGain)
	.dwattr $C$DW$14, DW_AT_high_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_ADS_SetGain")
	.dwattr $C$DW$14, DW_AT_external
	.dwattr $C$DW$14, DW_AT_TI_begin_file("../ADS1015.c")
	.dwattr $C$DW$14, DW_AT_TI_begin_line(0x39)
	.dwattr $C$DW$14, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$14, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ADS1015.c",line 58,column 1,is_stmt,address _ADS_SetGain

	.dwfde $C$DW$CIE, _ADS_SetGain
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("gain")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_gain")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _ADS_SetGain                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_ADS_SetGain:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("gain")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_gain")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |58| 
	.dwpsn	file "../ADS1015.c",line 59,column 3,is_stmt
        MOVW      DP,#_m_gain           ; [CPU_U] 
        MOV       @_m_gain,AL           ; [CPU_] |59| 
	.dwpsn	file "../ADS1015.c",line 60,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$14, DW_AT_TI_end_file("../ADS1015.c")
	.dwattr $C$DW$14, DW_AT_TI_end_line(0x3c)
	.dwattr $C$DW$14, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$14

	.sect	".text"
	.clink
	.global	_ADS_GetGain

$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_GetGain")
	.dwattr $C$DW$18, DW_AT_low_pc(_ADS_GetGain)
	.dwattr $C$DW$18, DW_AT_high_pc(0x00)
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_ADS_GetGain")
	.dwattr $C$DW$18, DW_AT_external
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$18, DW_AT_TI_begin_file("../ADS1015.c")
	.dwattr $C$DW$18, DW_AT_TI_begin_line(0x43)
	.dwattr $C$DW$18, DW_AT_TI_begin_column(0x0b)
	.dwattr $C$DW$18, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../ADS1015.c",line 68,column 1,is_stmt,address _ADS_GetGain

	.dwfde $C$DW$CIE, _ADS_GetGain

;***************************************************************
;* FNAME: _ADS_GetGain                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ADS_GetGain:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../ADS1015.c",line 69,column 3,is_stmt
        MOVW      DP,#_m_gain           ; [CPU_U] 
        MOV       AL,@_m_gain           ; [CPU_] |69| 
	.dwpsn	file "../ADS1015.c",line 70,column 1,is_stmt
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$18, DW_AT_TI_end_file("../ADS1015.c")
	.dwattr $C$DW$18, DW_AT_TI_end_line(0x46)
	.dwattr $C$DW$18, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$18

	.sect	".text"
	.clink
	.global	_ADS_StartReadADC_SingleEnded

$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_StartReadADC_SingleEnded")
	.dwattr $C$DW$20, DW_AT_low_pc(_ADS_StartReadADC_SingleEnded)
	.dwattr $C$DW$20, DW_AT_high_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_ADS_StartReadADC_SingleEnded")
	.dwattr $C$DW$20, DW_AT_external
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$20, DW_AT_TI_begin_file("../ADS1015.c")
	.dwattr $C$DW$20, DW_AT_TI_begin_line(0x4d)
	.dwattr $C$DW$20, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$20, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ADS1015.c",line 77,column 51,is_stmt,address _ADS_StartReadADC_SingleEnded

	.dwfde $C$DW$CIE, _ADS_StartReadADC_SingleEnded
$C$DW$21	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _ADS_StartReadADC_SingleEnded FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_ADS_StartReadADC_SingleEnded:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_breg20 -1]
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -2]
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("config")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[1],AL            ; [CPU_] |77| 
	.dwpsn	file "../ADS1015.c",line 80,column 3,is_stmt
        CMPB      AL,#4                 ; [CPU_] |80| 
        B         $C$L1,LO              ; [CPU_] |80| 
        ; branchcc occurs ; [] |80| 
	.dwpsn	file "../ADS1015.c",line 82,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |82| 
        B         $C$L8,UNC             ; [CPU_] |82| 
        ; branch occurs ; [] |82| 
$C$L1:    
	.dwpsn	file "../ADS1015.c",line 86,column 17,is_stmt
        MOV       *-SP[3],#387          ; [CPU_] |86| 
	.dwpsn	file "../ADS1015.c",line 94,column 3,is_stmt
        MOVW      DP,#_m_gain           ; [CPU_U] 
        MOV       AL,@_m_gain           ; [CPU_] |94| 
        OR        *-SP[3],AL            ; [CPU_] |94| 
	.dwpsn	file "../ADS1015.c",line 97,column 3,is_stmt
        B         $C$L6,UNC             ; [CPU_] |97| 
        ; branch occurs ; [] |97| 
$C$L2:    
	.dwpsn	file "../ADS1015.c",line 100,column 7,is_stmt
        OR        *-SP[3],#0x4000       ; [CPU_] |100| 
	.dwpsn	file "../ADS1015.c",line 101,column 7,is_stmt
        B         $C$L7,UNC             ; [CPU_] |101| 
        ; branch occurs ; [] |101| 
$C$L3:    
	.dwpsn	file "../ADS1015.c",line 103,column 7,is_stmt
        OR        *-SP[3],#0x5000       ; [CPU_] |103| 
	.dwpsn	file "../ADS1015.c",line 104,column 7,is_stmt
        B         $C$L7,UNC             ; [CPU_] |104| 
        ; branch occurs ; [] |104| 
$C$L4:    
	.dwpsn	file "../ADS1015.c",line 106,column 7,is_stmt
        OR        *-SP[3],#0x6000       ; [CPU_] |106| 
	.dwpsn	file "../ADS1015.c",line 107,column 7,is_stmt
        B         $C$L7,UNC             ; [CPU_] |107| 
        ; branch occurs ; [] |107| 
$C$L5:    
	.dwpsn	file "../ADS1015.c",line 109,column 7,is_stmt
        OR        *-SP[3],#0x7000       ; [CPU_] |109| 
	.dwpsn	file "../ADS1015.c",line 110,column 7,is_stmt
        B         $C$L7,UNC             ; [CPU_] |110| 
        ; branch occurs ; [] |110| 
$C$L6:    
	.dwpsn	file "../ADS1015.c",line 97,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |97| 
        BF        $C$L2,EQ              ; [CPU_] |97| 
        ; branchcc occurs ; [] |97| 
        CMPB      AL,#1                 ; [CPU_] |97| 
        BF        $C$L3,EQ              ; [CPU_] |97| 
        ; branchcc occurs ; [] |97| 
        CMPB      AL,#2                 ; [CPU_] |97| 
        BF        $C$L4,EQ              ; [CPU_] |97| 
        ; branchcc occurs ; [] |97| 
        CMPB      AL,#3                 ; [CPU_] |97| 
        BF        $C$L5,EQ              ; [CPU_] |97| 
        ; branchcc occurs ; [] |97| 
$C$L7:    
	.dwpsn	file "../ADS1015.c",line 114,column 3,is_stmt
        OR        *-SP[3],#0x8000       ; [CPU_] |114| 
	.dwpsn	file "../ADS1015.c",line 116,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |116| 
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_CNV_SwapWord")
	.dwattr $C$DW$25, DW_AT_TI_call
        LCR       #_CNV_SwapWord        ; [CPU_] |116| 
        ; call occurs [#_CNV_SwapWord] ; [] |116| 
        MOV       *-SP[3],AL            ; [CPU_] |116| 
	.dwpsn	file "../ADS1015.c",line 119,column 3,is_stmt
        MOVB      XAR5,#1               ; [CPU_] |119| 
        MOVB      AH,#2                 ; [CPU_] |119| 
        MOVZ      AR4,SP                ; [CPU_U] |119| 
        MOVB      AL,#5                 ; [CPU_] |119| 
        SUBB      XAR4,#3               ; [CPU_U] |119| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$26, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |119| 
        ; call occurs [#_I2C_Command] ; [] |119| 
        MOV       *-SP[2],AL            ; [CPU_] |119| 
	.dwpsn	file "../ADS1015.c",line 126,column 3,is_stmt
$C$L8:    
	.dwpsn	file "../ADS1015.c",line 127,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$20, DW_AT_TI_end_file("../ADS1015.c")
	.dwattr $C$DW$20, DW_AT_TI_end_line(0x7f)
	.dwattr $C$DW$20, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$20

	.sect	".text"
	.clink
	.global	_ADS_GetLastConversionResults

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_GetLastConversionResults")
	.dwattr $C$DW$28, DW_AT_low_pc(_ADS_GetLastConversionResults)
	.dwattr $C$DW$28, DW_AT_high_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ADS_GetLastConversionResults")
	.dwattr $C$DW$28, DW_AT_external
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$28, DW_AT_TI_begin_file("../ADS1015.c")
	.dwattr $C$DW$28, DW_AT_TI_begin_line(0x89)
	.dwattr $C$DW$28, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$28, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ADS1015.c",line 138,column 1,is_stmt,address _ADS_GetLastConversionResults

	.dwfde $C$DW$CIE, _ADS_GetLastConversionResults
$C$DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_name("measure")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_measure")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _ADS_GetLastConversionResults FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_ADS_GetLastConversionResults:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("measure")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_measure")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_breg20 -2]
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("command_res")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_command_res")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -3]
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("val")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_val")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],XAR4          ; [CPU_] |138| 
	.dwpsn	file "../ADS1015.c",line 147,column 3,is_stmt
        MOVB      AL,#6                 ; [CPU_] |147| 
        MOVB      AH,#2                 ; [CPU_] |147| 
        MOVB      XAR5,#0               ; [CPU_] |147| 
        MOVZ      AR4,SP                ; [CPU_U] |147| 
        SUBB      XAR4,#4               ; [CPU_U] |147| 
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$33, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |147| 
        ; call occurs [#_I2C_Command] ; [] |147| 
        MOV       *-SP[3],AL            ; [CPU_] |147| 
	.dwpsn	file "../ADS1015.c",line 149,column 3,is_stmt
        CMPB      AL,#0                 ; [CPU_] |149| 
        BF        $C$L11,EQ             ; [CPU_] |149| 
        ; branchcc occurs ; [] |149| 
	.dwpsn	file "../ADS1015.c",line 150,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |150| 
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_CNV_SwapWord")
	.dwattr $C$DW$34, DW_AT_TI_call
        LCR       #_CNV_SwapWord        ; [CPU_] |150| 
        ; call occurs [#_CNV_SwapWord] ; [] |150| 
        MOV       *-SP[4],AL            ; [CPU_] |150| 
	.dwpsn	file "../ADS1015.c",line 151,column 5,is_stmt
        MOVW      DP,#_m_bitShift       ; [CPU_U] 
        MOV       T,@_m_bitShift        ; [CPU_] |151| 
        LSR       AL,T                  ; [CPU_] |151| 
        MOV       *-SP[4],AL            ; [CPU_] |151| 
	.dwpsn	file "../ADS1015.c",line 152,column 5,is_stmt
        MOV       AL,@_m_bitShift       ; [CPU_] |152| 
        BF        $C$L9,NEQ             ; [CPU_] |152| 
        ; branchcc occurs ; [] |152| 
	.dwpsn	file "../ADS1015.c",line 154,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |154| 
        MOV       AL,*-SP[4]            ; [CPU_] |154| 
        MOV       *+XAR4[0],AL          ; [CPU_] |154| 
	.dwpsn	file "../ADS1015.c",line 155,column 5,is_stmt
        B         $C$L12,UNC            ; [CPU_] |155| 
        ; branch occurs ; [] |155| 
$C$L9:    
	.dwpsn	file "../ADS1015.c",line 160,column 7,is_stmt
        CMP       *-SP[4],#2047         ; [CPU_] |160| 
        B         $C$L10,LOS            ; [CPU_] |160| 
        ; branchcc occurs ; [] |160| 
	.dwpsn	file "../ADS1015.c",line 163,column 9,is_stmt
        OR        *-SP[4],#0xf000       ; [CPU_] |163| 
$C$L10:    
	.dwpsn	file "../ADS1015.c",line 165,column 7,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |165| 
        MOV       AL,*-SP[4]            ; [CPU_] |165| 
        MOV       *+XAR4[0],AL          ; [CPU_] |165| 
	.dwpsn	file "../ADS1015.c",line 167,column 3,is_stmt
        B         $C$L12,UNC            ; [CPU_] |167| 
        ; branch occurs ; [] |167| 
$C$L11:    
	.dwpsn	file "../ADS1015.c",line 169,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |169| 
        MOV       *+XAR4[0],#0          ; [CPU_] |169| 
$C$L12:    
	.dwpsn	file "../ADS1015.c",line 171,column 3,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |171| 
	.dwpsn	file "../ADS1015.c",line 172,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$28, DW_AT_TI_end_file("../ADS1015.c")
	.dwattr $C$DW$28, DW_AT_TI_end_line(0xac)
	.dwattr $C$DW$28, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$28

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_CNV_SwapWord
	.global	_I2C_Command

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$36	.dwtag  DW_TAG_enumerator, DW_AT_name("GAIN_TWOTHIRDS"), DW_AT_const_value(0x00)
$C$DW$37	.dwtag  DW_TAG_enumerator, DW_AT_name("GAIN_ONE"), DW_AT_const_value(0x200)
$C$DW$38	.dwtag  DW_TAG_enumerator, DW_AT_name("GAIN_TWO"), DW_AT_const_value(0x400)
$C$DW$39	.dwtag  DW_TAG_enumerator, DW_AT_name("GAIN_FOUR"), DW_AT_const_value(0x600)
$C$DW$40	.dwtag  DW_TAG_enumerator, DW_AT_name("GAIN_EIGHT"), DW_AT_const_value(0x800)
$C$DW$41	.dwtag  DW_TAG_enumerator, DW_AT_name("GAIN_SIXTEEN"), DW_AT_const_value(0xa00)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("adsGain_t")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$29	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$25	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$25, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$42	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg0]
$C$DW$43	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg1]
$C$DW$44	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg2]
$C$DW$45	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg3]
$C$DW$46	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg20]
$C$DW$47	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg21]
$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg22]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg23]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg24]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg25]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg26]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg28]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg29]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg30]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg31]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_regx 0x20]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_regx 0x21]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_regx 0x22]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_regx 0x23]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_regx 0x24]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x25]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x26]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg4]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg6]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg8]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg10]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg12]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg14]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg16]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg17]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg18]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg19]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg5]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg7]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg9]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg11]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg13]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg15]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_regx 0x30]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_regx 0x33]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x34]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x37]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x38]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x40]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x43]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x44]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x47]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x48]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x49]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x27]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x28]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg27]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

