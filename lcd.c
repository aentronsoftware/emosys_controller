/*
 * lcd.c
 *
 *  Created on: 23 nov. 2015
 *      Author: remip
 */

#include "gatewaycfg.h"

#include "uc.h"
#include "dspspecs.h"
#include "gateway_dict.h"   //for ODP_VersionParameters, also #include <includes.h> (festival)
#include "F2806x_I2c_defines.h"
#include "i2c.h"
#include "convert.h"
#include "param.h"
#include "ERROR.H"


void LCD_Start(void){
  uint16 buffer[2];
  buffer[0] = 0x0C3E;//0x8+4(display on)<<8; 0x20+0x10(8bits)+0xC(3lignes 12 char)+2(voltage generator)
  buffer[1] = 0x0206;//2(set home)<<8; 4+2(set cursor right shift)
  I2C_Command(LCD_WRITE, (char*)&buffer, 4, 0);
}

void LCD_WriteText(uint16 pos,char* text,uint16 lenght){
  uint16 res, temp, i;
  uint16 buffer[7];
  //char text[12] = "MODE:INIT(R)";
  buffer[0] = 0x4080 + pos;
  //res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0);
  if (lenght < 13){
    for (i=2;i<lenght+2;i++){
      res = text[i-2];
      if(res >= (int)' ' && res <= (int)'/'){
        temp = 0xA0 + res - (int)' ';
      }
      else if(res >= (int)'0' && res <= (int)'?'){
        temp = 0xB0 + res - (int)'0';
      }
      else if(res >= (int)'A' && res <= (int)'O'){
        temp = 0xC1 + res - (int)'A';
      }
      else if(res >= (int)'P' && res <= (int)'Z'){
        temp = 0xD0 + res - (int)'P';
      }
      else if(res >= (int)'a' && res <= (int)'o'){
        temp = 0xE1 + res - (int)'a';
      }
      else if(res >= (int)'p' && res <= (int)'z'){
        temp = 0xF0 + res - (int)'p';
      }
      else temp = 0xA0; //space
      if ((i&1) == 0) buffer[i/2] = temp;
      else buffer[i/2] += (temp<<8);
    }
    res = I2C_Command(LCD_WRITE, (char*)&buffer, lenght+2, 0x80);
  }
}


void LCD_ReadText(uint16 pos,char* text,uint16 lenght){
  uint16 res, temp, i;
  uint16 buffer[6];
  //char text[12] = "MODE:INIT(R)";
  buffer[0] = 0x0080 + pos;
  res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0); //set cursor adress
  if (lenght < 13){
    res = I2C_Command(LCD_READ, (char*)&buffer, lenght, 0x60);
    for (i=0;i<lenght;i++){
      if ((i&1) == 0) res = buffer[i/2] & 0xFF;
      else res = buffer[i/2] >> 8;
      if(res >= 0xA0 && res <= 0xAF){
        temp = (int)' ' + res - 0xA0;
      }
      else if(res >= 0xB0 && res <= 0xBF){
        temp = (int)'0' + res - 0xB0;
      }
      else if(res >= 0xC1 && res <= 0xCF){
        temp = (int)'A' + res - 0xC1;
      }
      else if(res >= 0xD0 && res <= 0xDF){
        temp = (int)'P' + res - 0xD0;
      }
      else if(res >= 0xE1 && res <= 0xEF){
        temp = (int)'a' + res - 0xE1;
      }
      else if(res >= 0xF0 && res <= 0xFF){
        temp = (int)'p' + res - 0xF0;
      }
      else temp = (int)' '; //space
      text[i] = (char)temp;
    }
  }
}

const uint64 DIVISOR[12] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000,100000000000};

void LCD_WriteInteger(uint16 pos,uint64* text,uint16 lenght,bool1 signe){
  uint16 res, temp, i = 0;
  uint16 buffer[7];
  int64 itemp;
  uint64 temp64 = *text;
  if (signe){
    itemp = *(int64*)text;
    if (itemp < 0){
      itemp = -itemp;
      i = 1;
      buffer[1] = 0xAD;
    }
    temp64 = itemp;
  }
  buffer[0] = 0x4080 + pos;
  //res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0);
  if (lenght < 13){
    while (i<lenght){
      res = temp64/DIVISOR[lenght-i-1];//partie entiere
      temp64 = temp64 - res * DIVISOR[lenght-i-1];//reste
      temp = 0xB0 + res;
      if ((i&1) == 0) buffer[(i+2)/2] = temp;
      else buffer[(i+2)/2] += (temp<<8);
      i++;
    }
    res = I2C_Command(LCD_WRITE, (char*)&buffer, lenght+2, 0x80);
  }
}

void LCD_ReadInteger(uint16 pos,uint64* text,uint16 lenght){
  bool1 res = FALSE;
  uint16 i, retry = 3;
  uint16 buffer[6];
  uint64 temp64 = *text;
  while (retry > 0 && !res){
    buffer[0] = 0x0080 + pos;
    res = I2C_Command(LCD_WRITE, (char*)&buffer, 1, 0); //set cursor adress
    if (lenght < 13 && res){
      TSK_sleep(2);
      res = I2C_Command(LCD_READ, (char*)&buffer, lenght, 0x60);
    }
    TSK_sleep(2);
    --retry;
  }
  if (lenght < 13 && res){
    for (i=0;i<lenght;i++){
      temp64 *= 10;
      if ((i&1) == 0) res = buffer[i/2] & 0xFF;
      else res = buffer[i/2] >> 8;
      res = res - 0xB0;
      temp64 += res;
    }
    *text = temp64;
  }
}

void LCD_SetPosition(uint16 pos, bool1 blink){
  uint16 buffer;
  buffer = 0x0080 + pos;
  if (blink) buffer += (0xF<<8); //0x8+4(display on)+2(cursor on)+1(blink on)
  else buffer += (0xC<<8);       //0x8+4(display on)
  I2C_Command(LCD_WRITE, (char*)&buffer, 2, 0); //set cursor adress
}


