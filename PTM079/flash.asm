;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/flash.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0638812 
	.sect	".TI.ramfunc"
	.clink

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_setWaitstates")
	.dwattr $C$DW$3, DW_AT_low_pc(_Flash_setWaitstates)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_Flash_setWaitstates")
	.dwattr $C$DW$3, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0xfa)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 251,column 1,is_stmt,address _Flash_setWaitstates

	.dwfde $C$DW$CIE, _Flash_setWaitstates
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("waitstates")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_waitstates")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _Flash_setWaitstates          FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_Flash_setWaitstates:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -2]
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("waitstates")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_waitstates")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |251| 
        MOVL      *-SP[2],ACC           ; [CPU_] |251| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 262,column 5,is_stmt
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_name("___eallow")
	.dwattr $C$DW$8, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |262| 
        ; call occurs [#___eallow] ; [] |262| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 266,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |266| 
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3] << 8      ; [CPU_] |266| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |266| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |266| 
        AND       PL,#61695             ; [CPU_] |266| 
        OR        AH,PH                 ; [CPU_] |266| 
        OR        AL,PL                 ; [CPU_] |266| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |266| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 270,column 5,is_stmt
$C$DW$9	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$9, DW_AT_low_pc(0x00)
	.dwattr $C$DW$9, DW_AT_name("___edis")
	.dwattr $C$DW$9, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |270| 
        ; call occurs [#___edis] ; [] |270| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 271,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x10f)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".TI.ramfunc"
	.clink

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_setBankPowerMode")
	.dwattr $C$DW$11, DW_AT_low_pc(_Flash_setBankPowerMode)
	.dwattr $C$DW$11, DW_AT_high_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_Flash_setBankPowerMode")
	.dwattr $C$DW$11, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$11, DW_AT_TI_begin_line(0x12c)
	.dwattr $C$DW$11, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$11, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 302,column 1,is_stmt,address _Flash_setBankPowerMode

	.dwfde $C$DW$CIE, _Flash_setBankPowerMode
$C$DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg0]
$C$DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bank")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_bank")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg12]
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("powerMode")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_powerMode")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _Flash_setBankPowerMode       FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_Flash_setBankPowerMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -2]
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("bank")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_bank")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -3]
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("powerMode")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_powerMode")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |302| 
        MOV       *-SP[3],AR4           ; [CPU_] |302| 
        MOVL      *-SP[2],ACC           ; [CPU_] |302| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 308,column 5,is_stmt
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("___eallow")
	.dwattr $C$DW$18, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |308| 
        ; call occurs [#___eallow] ; [] |308| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 313,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[4]           ; [CPU_] |313| 
        MOVL      P,ACC                 ; [CPU_] |313| 
        MOV       ACC,*-SP[3] << #1     ; [CPU_] |313| 
        MOVP      T,AL                  ; [CPU_] 
        LSLL      ACC,T                 ; [CPU_] |313| 
        MOVL      P,ACC                 ; [CPU_] |313| 
        MOV       ACC,*-SP[3] << #1     ; [CPU_] |313| 
        MOV       T,AL                  ; [CPU_] |313| 
        MOVB      AL,#3                 ; [CPU_] |313| 
        LSL       AL,T                  ; [CPU_] |313| 
        NOT       AL                    ; [CPU_] |313| 
        MOVZ      AR6,AL                ; [CPU_] |313| 
        MOVB      ACC,#32               ; [CPU_] |313| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |313| 
        MOVL      XAR4,ACC              ; [CPU_] |313| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |313| 
        MOVL      *-SP[6],ACC           ; [CPU_] |313| 
        AND       ACC,AR6               ; [CPU_] |313| 
        MOVL      *-SP[6],ACC           ; [CPU_] |313| 
        MOV       AL,*-SP[6]            ; [CPU_] |313| 
        OR        AL,PL                 ; [CPU_] |313| 
        MOV       *-SP[6],AL            ; [CPU_] |313| 
        MOV       AH,*-SP[5]            ; [CPU_] |313| 
        OR        AH,PH                 ; [CPU_] |313| 
        MOV       *-SP[5],AH            ; [CPU_] |313| 
        MOVB      ACC,#32               ; [CPU_] |313| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |313| 
        MOVL      XAR4,ACC              ; [CPU_] |313| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |313| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |313| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 317,column 5,is_stmt
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("___edis")
	.dwattr $C$DW$19, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |317| 
        ; call occurs [#___edis] ; [] |317| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 318,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$11, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$11, DW_AT_TI_end_line(0x13e)
	.dwattr $C$DW$11, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$11

	.sect	".TI.ramfunc"
	.clink

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_setPumpPowerMode")
	.dwattr $C$DW$21, DW_AT_low_pc(_Flash_setPumpPowerMode)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_Flash_setPumpPowerMode")
	.dwattr $C$DW$21, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0x153)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 340,column 1,is_stmt,address _Flash_setPumpPowerMode

	.dwfde $C$DW$CIE, _Flash_setPumpPowerMode
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg0]
$C$DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_name("powerMode")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_powerMode")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _Flash_setPumpPowerMode       FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_Flash_setPumpPowerMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -2]
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("powerMode")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_powerMode")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |340| 
        MOVL      *-SP[2],ACC           ; [CPU_] |340| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 346,column 5,is_stmt
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("___eallow")
	.dwattr $C$DW$26, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |346| 
        ; call occurs [#___eallow] ; [] |346| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 351,column 5,is_stmt
        MOVB      ACC,#36               ; [CPU_] |351| 
        SETC      SXM                   ; [CPU_] 
        ADDL      ACC,*-SP[2]           ; [CPU_] |351| 
        MOVL      XAR4,ACC              ; [CPU_] |351| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |351| 
        AND       PL,#65534             ; [CPU_] |351| 
        MOV       ACC,*-SP[3]           ; [CPU_] |351| 
        MOVL      *-SP[6],ACC           ; [CPU_] |351| 
        MOV       AL,*-SP[6]            ; [CPU_] |351| 
        OR        AL,PL                 ; [CPU_] |351| 
        MOV       *-SP[6],AL            ; [CPU_] |351| 
        MOV       AL,*-SP[5]            ; [CPU_] |351| 
        OR        AL,PH                 ; [CPU_] |351| 
        MOV       *-SP[5],AL            ; [CPU_] |351| 
        MOVB      ACC,#36               ; [CPU_] |351| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |351| 
        MOVL      XAR4,ACC              ; [CPU_] |351| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |351| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |351| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 354,column 5,is_stmt
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("___edis")
	.dwattr $C$DW$27, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |354| 
        ; call occurs [#___edis] ; [] |354| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 355,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$21, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x163)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$21

	.sect	".TI.ramfunc"
	.clink

$C$DW$29	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_enablePrefetch")
	.dwattr $C$DW$29, DW_AT_low_pc(_Flash_enablePrefetch)
	.dwattr $C$DW$29, DW_AT_high_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_Flash_enablePrefetch")
	.dwattr $C$DW$29, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$29, DW_AT_TI_begin_line(0x172)
	.dwattr $C$DW$29, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$29, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 371,column 1,is_stmt,address _Flash_enablePrefetch

	.dwfde $C$DW$CIE, _Flash_enablePrefetch
$C$DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Flash_enablePrefetch         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Flash_enablePrefetch:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |371| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 377,column 5,is_stmt
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("___eallow")
	.dwattr $C$DW$32, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |377| 
        ; call occurs [#___eallow] ; [] |377| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 382,column 5,is_stmt
        MOV       ACC,#384              ; [CPU_] |382| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |382| 
        MOVL      XAR4,ACC              ; [CPU_] |382| 
        OR        *+XAR4[0],#1          ; [CPU_] |382| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 384,column 5,is_stmt
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("___edis")
	.dwattr $C$DW$33, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |384| 
        ; call occurs [#___edis] ; [] |384| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 385,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$29, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$29, DW_AT_TI_end_line(0x181)
	.dwattr $C$DW$29, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$29

	.sect	".TI.ramfunc"
	.clink

$C$DW$35	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_disablePrefetch")
	.dwattr $C$DW$35, DW_AT_low_pc(_Flash_disablePrefetch)
	.dwattr $C$DW$35, DW_AT_high_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_Flash_disablePrefetch")
	.dwattr $C$DW$35, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$35, DW_AT_TI_begin_line(0x190)
	.dwattr $C$DW$35, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$35, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 401,column 1,is_stmt,address _Flash_disablePrefetch

	.dwfde $C$DW$CIE, _Flash_disablePrefetch
$C$DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Flash_disablePrefetch        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Flash_disablePrefetch:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |401| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 407,column 5,is_stmt
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("___eallow")
	.dwattr $C$DW$38, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |407| 
        ; call occurs [#___eallow] ; [] |407| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 412,column 5,is_stmt
        MOV       ACC,#384              ; [CPU_] |412| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |412| 
        MOVL      XAR4,ACC              ; [CPU_] |412| 
        AND       *+XAR4[0],#65534      ; [CPU_] |412| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 414,column 5,is_stmt
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("___edis")
	.dwattr $C$DW$39, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |414| 
        ; call occurs [#___edis] ; [] |414| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 415,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$35, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$35, DW_AT_TI_end_line(0x19f)
	.dwattr $C$DW$35, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$35

	.sect	".TI.ramfunc"
	.clink

$C$DW$41	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_enableCache")
	.dwattr $C$DW$41, DW_AT_low_pc(_Flash_enableCache)
	.dwattr $C$DW$41, DW_AT_high_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_Flash_enableCache")
	.dwattr $C$DW$41, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$41, DW_AT_TI_begin_line(0x1ae)
	.dwattr $C$DW$41, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$41, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 431,column 1,is_stmt,address _Flash_enableCache

	.dwfde $C$DW$CIE, _Flash_enableCache
$C$DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Flash_enableCache            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Flash_enableCache:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |431| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 437,column 5,is_stmt
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("___eallow")
	.dwattr $C$DW$44, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |437| 
        ; call occurs [#___eallow] ; [] |437| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 442,column 5,is_stmt
        MOV       ACC,#384              ; [CPU_] |442| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |442| 
        MOVL      XAR4,ACC              ; [CPU_] |442| 
        OR        *+XAR4[0],#2          ; [CPU_] |442| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 444,column 5,is_stmt
$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_name("___edis")
	.dwattr $C$DW$45, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |444| 
        ; call occurs [#___edis] ; [] |444| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 445,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$41, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$41, DW_AT_TI_end_line(0x1bd)
	.dwattr $C$DW$41, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$41

	.sect	".TI.ramfunc"
	.clink

$C$DW$47	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_disableCache")
	.dwattr $C$DW$47, DW_AT_low_pc(_Flash_disableCache)
	.dwattr $C$DW$47, DW_AT_high_pc(0x00)
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_Flash_disableCache")
	.dwattr $C$DW$47, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$47, DW_AT_TI_begin_line(0x1cc)
	.dwattr $C$DW$47, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$47, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 461,column 1,is_stmt,address _Flash_disableCache

	.dwfde $C$DW$CIE, _Flash_disableCache
$C$DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Flash_disableCache           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Flash_disableCache:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |461| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 467,column 5,is_stmt
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_name("___eallow")
	.dwattr $C$DW$50, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |467| 
        ; call occurs [#___eallow] ; [] |467| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 472,column 5,is_stmt
        MOV       ACC,#384              ; [CPU_] |472| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |472| 
        MOVL      XAR4,ACC              ; [CPU_] |472| 
        AND       *+XAR4[0],#65533      ; [CPU_] |472| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 474,column 5,is_stmt
$C$DW$51	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$51, DW_AT_low_pc(0x00)
	.dwattr $C$DW$51, DW_AT_name("___edis")
	.dwattr $C$DW$51, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |474| 
        ; call occurs [#___edis] ; [] |474| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 475,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x00)
	.dwattr $C$DW$52, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$47, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$47, DW_AT_TI_end_line(0x1db)
	.dwattr $C$DW$47, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$47

	.sect	".TI.ramfunc"
	.clink

$C$DW$53	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_enableECC")
	.dwattr $C$DW$53, DW_AT_low_pc(_Flash_enableECC)
	.dwattr $C$DW$53, DW_AT_high_pc(0x00)
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_Flash_enableECC")
	.dwattr $C$DW$53, DW_AT_TI_begin_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$53, DW_AT_TI_begin_line(0x1ea)
	.dwattr $C$DW$53, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$53, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 491,column 1,is_stmt,address _Flash_enableECC

	.dwfde $C$DW$CIE, _Flash_enableECC
$C$DW$54	.dwtag  DW_TAG_formal_parameter, DW_AT_name("eccBase")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_eccBase")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Flash_enableECC              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Flash_enableECC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("eccBase")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_eccBase")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |491| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 497,column 5,is_stmt
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_name("___eallow")
	.dwattr $C$DW$56, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |497| 
        ; call occurs [#___eallow] ; [] |497| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 502,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |502| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |502| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |502| 
        AND       AL,#65520             ; [CPU_] |502| 
        ORB       AL,#0x0a              ; [CPU_] |502| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |502| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 505,column 5,is_stmt
$C$DW$57	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$57, DW_AT_low_pc(0x00)
	.dwattr $C$DW$57, DW_AT_name("___edis")
	.dwattr $C$DW$57, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |505| 
        ; call occurs [#___edis] ; [] |505| 
	.dwpsn	file "..\ExtraData\driverlib2\flash.h",line 506,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$53, DW_AT_TI_end_file("..\ExtraData\driverlib2\flash.h")
	.dwattr $C$DW$53, DW_AT_TI_end_line(0x1fa)
	.dwattr $C$DW$53, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$53

	.sect	".TI.ramfunc"
	.clink
	.global	_Flash_initModule

$C$DW$59	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_initModule")
	.dwattr $C$DW$59, DW_AT_low_pc(_Flash_initModule)
	.dwattr $C$DW$59, DW_AT_high_pc(0x00)
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_Flash_initModule")
	.dwattr $C$DW$59, DW_AT_external
	.dwattr $C$DW$59, DW_AT_TI_begin_file("../ExtraData/driverlib2/flash.c")
	.dwattr $C$DW$59, DW_AT_TI_begin_line(0x3b)
	.dwattr $C$DW$59, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$59, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 60,column 1,is_stmt,address _Flash_initModule

	.dwfde $C$DW$CIE, _Flash_initModule
$C$DW$60	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg0]
$C$DW$61	.dwtag  DW_TAG_formal_parameter, DW_AT_name("eccBase")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_eccBase")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -8]
$C$DW$62	.dwtag  DW_TAG_formal_parameter, DW_AT_name("waitstates")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_waitstates")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _Flash_initModule             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_Flash_initModule:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -2]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("waitstates")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_waitstates")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |60| 
        MOVL      *-SP[2],ACC           ; [CPU_] |60| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 71,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |71| 
        MOVB      XAR5,#3               ; [CPU_] |71| 
        MOVB      XAR4,#0               ; [CPU_] |71| 
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_name("_Flash_setBankPowerMode")
	.dwattr $C$DW$65, DW_AT_TI_call
        LCR       #_Flash_setBankPowerMode ; [CPU_] |71| 
        ; call occurs [#_Flash_setBankPowerMode] ; [] |71| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 77,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |77| 
        MOVB      XAR4,#1               ; [CPU_] |77| 
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_name("_Flash_setPumpPowerMode")
	.dwattr $C$DW$66, DW_AT_TI_call
        LCR       #_Flash_setPumpPowerMode ; [CPU_] |77| 
        ; call occurs [#_Flash_setPumpPowerMode] ; [] |77| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 82,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |82| 
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("_Flash_disableCache")
	.dwattr $C$DW$67, DW_AT_TI_call
        LCR       #_Flash_disableCache  ; [CPU_] |82| 
        ; call occurs [#_Flash_disableCache] ; [] |82| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 83,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |83| 
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("_Flash_disablePrefetch")
	.dwattr $C$DW$68, DW_AT_TI_call
        LCR       #_Flash_disablePrefetch ; [CPU_] |83| 
        ; call occurs [#_Flash_disablePrefetch] ; [] |83| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 88,column 5,is_stmt
        MOVZ      AR4,*-SP[3]           ; [CPU_] |88| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |88| 
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_name("_Flash_setWaitstates")
	.dwattr $C$DW$69, DW_AT_TI_call
        LCR       #_Flash_setWaitstates ; [CPU_] |88| 
        ; call occurs [#_Flash_setWaitstates] ; [] |88| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 94,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |94| 
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_name("_Flash_enableCache")
	.dwattr $C$DW$70, DW_AT_TI_call
        LCR       #_Flash_enableCache   ; [CPU_] |94| 
        ; call occurs [#_Flash_enableCache] ; [] |94| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 95,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |95| 
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_name("_Flash_enablePrefetch")
	.dwattr $C$DW$71, DW_AT_TI_call
        LCR       #_Flash_enablePrefetch ; [CPU_] |95| 
        ; call occurs [#_Flash_enablePrefetch] ; [] |95| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 101,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |101| 
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_name("_Flash_enableECC")
	.dwattr $C$DW$72, DW_AT_TI_call
        LCR       #_Flash_enableECC     ; [CPU_] |101| 
        ; call occurs [#_Flash_enableECC] ; [] |101| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 107,column 5,is_stmt
 RPT #7 || NOP
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 108,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$59, DW_AT_TI_end_file("../ExtraData/driverlib2/flash.c")
	.dwattr $C$DW$59, DW_AT_TI_end_line(0x6c)
	.dwattr $C$DW$59, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$59

	.sect	".TI.ramfunc"
	.clink
	.global	_Flash_powerDown

$C$DW$74	.dwtag  DW_TAG_subprogram, DW_AT_name("Flash_powerDown")
	.dwattr $C$DW$74, DW_AT_low_pc(_Flash_powerDown)
	.dwattr $C$DW$74, DW_AT_high_pc(0x00)
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_Flash_powerDown")
	.dwattr $C$DW$74, DW_AT_external
	.dwattr $C$DW$74, DW_AT_TI_begin_file("../ExtraData/driverlib2/flash.c")
	.dwattr $C$DW$74, DW_AT_TI_begin_line(0x77)
	.dwattr $C$DW$74, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$74, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 120,column 1,is_stmt,address _Flash_powerDown

	.dwfde $C$DW$CIE, _Flash_powerDown
$C$DW$75	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ctrlBase")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Flash_powerDown              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Flash_powerDown:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("ctrlBase")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_ctrlBase")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |120| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 129,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |129| 
        MOVB      XAR4,#0               ; [CPU_] |129| 
        MOVB      XAR5,#0               ; [CPU_] |129| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_Flash_setBankPowerMode")
	.dwattr $C$DW$77, DW_AT_TI_call
        LCR       #_Flash_setBankPowerMode ; [CPU_] |129| 
        ; call occurs [#_Flash_setBankPowerMode] ; [] |129| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 135,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |135| 
        MOVB      XAR4,#0               ; [CPU_] |135| 
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("_Flash_setPumpPowerMode")
	.dwattr $C$DW$78, DW_AT_TI_call
        LCR       #_Flash_setPumpPowerMode ; [CPU_] |135| 
        ; call occurs [#_Flash_setPumpPowerMode] ; [] |135| 
	.dwpsn	file "../ExtraData/driverlib2/flash.c",line 136,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$74, DW_AT_TI_end_file("../ExtraData/driverlib2/flash.c")
	.dwattr $C$DW$74, DW_AT_TI_end_line(0x88)
	.dwattr $C$DW$74, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$74

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$80	.dwtag  DW_TAG_enumerator, DW_AT_name("FLASH_BANK"), DW_AT_const_value(0x00)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("Flash_BankNumber")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$81	.dwtag  DW_TAG_enumerator, DW_AT_name("FLASH_BANK_PWR_SLEEP"), DW_AT_const_value(0x00)
$C$DW$82	.dwtag  DW_TAG_enumerator, DW_AT_name("FLASH_BANK_PWR_STANDBY"), DW_AT_const_value(0x01)
$C$DW$83	.dwtag  DW_TAG_enumerator, DW_AT_name("FLASH_BANK_PWR_ACTIVE"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("Flash_BankPowerMode")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$84	.dwtag  DW_TAG_enumerator, DW_AT_name("FLASH_PUMP_PWR_SLEEP"), DW_AT_const_value(0x00)
$C$DW$85	.dwtag  DW_TAG_enumerator, DW_AT_name("FLASH_PUMP_PWR_ACTIVE"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("Flash_PumpPowerMode")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg0]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg1]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg2]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg3]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg20]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg21]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg22]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg23]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_reg24]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_reg25]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg26]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg28]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg29]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg30]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg31]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x20]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x21]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x22]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x23]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x24]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x25]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x26]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg4]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg6]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg8]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg10]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg12]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg14]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg16]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg17]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg18]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg19]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg5]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg7]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg9]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg11]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg13]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_reg15]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x30]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x33]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x34]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x37]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x38]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x40]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x43]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x44]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x47]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x48]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x49]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x27]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x28]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg27]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

