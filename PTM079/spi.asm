;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:04 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0475612 
	.sect	".text"
	.clink
	.global	_SPI_setConfig

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("SPI_setConfig")
	.dwattr $C$DW$1, DW_AT_low_pc(_SPI_setConfig)
	.dwattr $C$DW$1, DW_AT_high_pc(0x00)
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_SPI_setConfig")
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_TI_begin_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$1, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$1, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$1, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 53,column 1,is_stmt,address _SPI_setConfig

	.dwfde $C$DW$CIE, _SPI_setConfig
$C$DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$2, DW_AT_location[DW_OP_reg0]
$C$DW$3	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lspclkHz")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_lspclkHz")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$3, DW_AT_location[DW_OP_breg20 -12]
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("protocol")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_protocol")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg12]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mode")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg14]
$C$DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bitRate")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_bitRate")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -14]
$C$DW$7	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataWidth")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_dataWidth")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -15]

;***************************************************************
;* FNAME: _SPI_setConfig                FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_SPI_setConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_breg20 -2]
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("protocol")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_protocol")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_breg20 -3]
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("mode")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_breg20 -4]
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("regValue")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_regValue")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -5]
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("baud")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_baud")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[4],AR5           ; [CPU_] |53| 
        MOV       *-SP[3],AR4           ; [CPU_] |53| 
        MOVL      *-SP[2],ACC           ; [CPU_] |53| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 69,column 5,is_stmt
        MOV       ACC,*-SP[3] << #6     ; [CPU_] |69| 
        MOV       AH,*-SP[15]           ; [CPU_] |69| 
        ANDB      AL,#0x40              ; [CPU_] |69| 
        ADDB      AH,#-1                ; [CPU_] |69| 
        OR        AH,AL                 ; [CPU_] |69| 
        MOV       *-SP[5],AH            ; [CPU_] |69| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 72,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |72| 
        AND       AL,*+XAR4[0],#0xffb0  ; [CPU_] |72| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |72| 
        OR        AL,*-SP[5]            ; [CPU_] |72| 
        MOV       *+XAR4[0],AL          ; [CPU_] |72| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 79,column 5,is_stmt
        MOV       ACC,*-SP[3] << #2     ; [CPU_] |79| 
        ANDB      AL,#0x08              ; [CPU_] |79| 
        OR        AL,*-SP[4]            ; [CPU_] |79| 
        MOV       *-SP[5],AL            ; [CPU_] |79| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 82,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |82| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |82| 
        MOVL      XAR4,ACC              ; [CPU_] |82| 
        AND       AL,*+XAR4[0],#0xfff1  ; [CPU_] |82| 
        OR        AL,*-SP[5]            ; [CPU_] |82| 
        MOVZ      AR6,AL                ; [CPU_] |82| 
        MOVB      ACC,#1                ; [CPU_] |82| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |82| 
        MOVL      XAR4,ACC              ; [CPU_] |82| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |82| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 89,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |89| 
        MOVL      P,*-SP[12]            ; [CPU_] |89| 
        MOVB      ACC,#0                ; [CPU_] |89| 
        SUBB      XAR4,#14              ; [CPU_U] |89| 
        RPT       #31
||     SUBCUL    ACC,*+XAR4[0]         ; [CPU_] |89| 
        MOVL      ACC,P                 ; [CPU_] |89| 
        SUBB      ACC,#1                ; [CPU_] |89| 
        MOVL      *-SP[8],ACC           ; [CPU_] |89| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 90,column 5,is_stmt
        MOVZ      AR6,*-SP[8]           ; [CPU_] |90| 
        MOVB      ACC,#4                ; [CPU_] |90| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |90| 
        MOVL      XAR4,ACC              ; [CPU_] |90| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |90| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 91,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$1, DW_AT_TI_end_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$1, DW_AT_TI_end_line(0x5b)
	.dwattr $C$DW$1, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$1

	.sect	".text"
	.clink
	.global	_SPI_setBaudRate

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("SPI_setBaudRate")
	.dwattr $C$DW$14, DW_AT_low_pc(_SPI_setBaudRate)
	.dwattr $C$DW$14, DW_AT_high_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_SPI_setBaudRate")
	.dwattr $C$DW$14, DW_AT_external
	.dwattr $C$DW$14, DW_AT_TI_begin_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$14, DW_AT_TI_begin_line(0x63)
	.dwattr $C$DW$14, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$14, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 100,column 1,is_stmt,address _SPI_setBaudRate

	.dwfde $C$DW$CIE, _SPI_setBaudRate
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg0]
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lspclkHz")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_lspclkHz")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -8]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bitRate")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_bitRate")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -10]

;***************************************************************
;* FNAME: _SPI_setBaudRate              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_SPI_setBaudRate:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -2]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("baud")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_baud")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |100| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 113,column 5,is_stmt
        MOVL      P,*-SP[8]             ; [CPU_] |113| 
        MOVZ      AR4,SP                ; [CPU_U] |113| 
        MOVB      ACC,#0                ; [CPU_] |113| 
        SUBB      XAR4,#10              ; [CPU_U] |113| 
        RPT       #31
||     SUBCUL    ACC,*+XAR4[0]         ; [CPU_] |113| 
        MOVL      ACC,P                 ; [CPU_] |113| 
        SUBB      ACC,#1                ; [CPU_] |113| 
        MOVL      *-SP[4],ACC           ; [CPU_] |113| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 114,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |114| 
        MOVB      ACC,#4                ; [CPU_] |114| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |114| 
        MOVL      XAR4,ACC              ; [CPU_] |114| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |114| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 115,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$14, DW_AT_TI_end_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$14, DW_AT_TI_end_line(0x73)
	.dwattr $C$DW$14, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$14

	.sect	".text"
	.clink
	.global	_SPI_enableInterrupt

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("SPI_enableInterrupt")
	.dwattr $C$DW$21, DW_AT_low_pc(_SPI_enableInterrupt)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_SPI_enableInterrupt")
	.dwattr $C$DW$21, DW_AT_external
	.dwattr $C$DW$21, DW_AT_TI_begin_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0x7b)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 124,column 1,is_stmt,address _SPI_enableInterrupt

	.dwfde $C$DW$CIE, _SPI_enableInterrupt
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg0]
$C$DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_name("intFlags")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_intFlags")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _SPI_enableInterrupt          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SPI_enableInterrupt:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |124| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 133,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |133| 
        SUBB      XAR4,#6               ; [CPU_U] |133| 
        TBIT      *+XAR4[0],#1          ; [CPU_] |133| 
        BF        $C$L1,NTC             ; [CPU_] |133| 
        ; branchcc occurs ; [] |133| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 135,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |135| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |135| 
        MOVL      XAR4,ACC              ; [CPU_] |135| 
        OR        *+XAR4[0],#0x0001     ; [CPU_] |135| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 138,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |138| 
        SUBB      XAR4,#6               ; [CPU_U] |138| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |138| 
        BF        $C$L2,NTC             ; [CPU_] |138| 
        ; branchcc occurs ; [] |138| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 140,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |140| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |140| 
        MOVL      XAR4,ACC              ; [CPU_] |140| 
        OR        *+XAR4[0],#0x0010     ; [CPU_] |140| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 146,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |146| 
        SUBB      XAR4,#6               ; [CPU_U] |146| 
        TBIT      *+XAR4[0],#3          ; [CPU_] |146| 
        BF        $C$L3,NTC             ; [CPU_] |146| 
        ; branchcc occurs ; [] |146| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 148,column 9,is_stmt
        MOVB      ACC,#10               ; [CPU_] |148| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |148| 
        MOVL      XAR4,ACC              ; [CPU_] |148| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |148| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 151,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |151| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |151| 
        MOVB      AH,#0                 ; [CPU_] |151| 
        ANDB      AL,#0x14              ; [CPU_] |151| 
        CMPL      ACC,XAR6              ; [CPU_] |151| 
        BF        $C$L4,EQ              ; [CPU_] |151| 
        ; branchcc occurs ; [] |151| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 153,column 9,is_stmt
        MOVB      ACC,#11               ; [CPU_] |153| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |153| 
        MOVL      XAR4,ACC              ; [CPU_] |153| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |153| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 155,column 1,is_stmt
$C$L4:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$21, DW_AT_TI_end_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x9b)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$21

	.sect	".text"
	.clink
	.global	_SPI_disableInterrupt

$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("SPI_disableInterrupt")
	.dwattr $C$DW$26, DW_AT_low_pc(_SPI_disableInterrupt)
	.dwattr $C$DW$26, DW_AT_high_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_SPI_disableInterrupt")
	.dwattr $C$DW$26, DW_AT_external
	.dwattr $C$DW$26, DW_AT_TI_begin_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$26, DW_AT_TI_begin_line(0xa3)
	.dwattr $C$DW$26, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$26, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 164,column 1,is_stmt,address _SPI_disableInterrupt

	.dwfde $C$DW$CIE, _SPI_disableInterrupt
$C$DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg0]
$C$DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("intFlags")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_intFlags")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _SPI_disableInterrupt         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SPI_disableInterrupt:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |164| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 173,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |173| 
        SUBB      XAR4,#6               ; [CPU_U] |173| 
        TBIT      *+XAR4[0],#1          ; [CPU_] |173| 
        BF        $C$L5,NTC             ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 175,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |175| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |175| 
        MOVL      XAR4,ACC              ; [CPU_] |175| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |175| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 178,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |178| 
        SUBB      XAR4,#6               ; [CPU_U] |178| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |178| 
        BF        $C$L6,NTC             ; [CPU_] |178| 
        ; branchcc occurs ; [] |178| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 180,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |180| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |180| 
        MOVL      XAR4,ACC              ; [CPU_] |180| 
        AND       *+XAR4[0],#0xffef     ; [CPU_] |180| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 186,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |186| 
        SUBB      XAR4,#6               ; [CPU_U] |186| 
        TBIT      *+XAR4[0],#3          ; [CPU_] |186| 
        BF        $C$L7,NTC             ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 188,column 9,is_stmt
        MOVB      ACC,#10               ; [CPU_] |188| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |188| 
        MOVL      XAR4,ACC              ; [CPU_] |188| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |188| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 191,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |191| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |191| 
        MOVB      AH,#0                 ; [CPU_] |191| 
        ANDB      AL,#0x14              ; [CPU_] |191| 
        CMPL      ACC,XAR6              ; [CPU_] |191| 
        BF        $C$L8,EQ              ; [CPU_] |191| 
        ; branchcc occurs ; [] |191| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 193,column 9,is_stmt
        MOVB      ACC,#11               ; [CPU_] |193| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |193| 
        MOVL      XAR4,ACC              ; [CPU_] |193| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |193| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 195,column 1,is_stmt
$C$L8:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$26, DW_AT_TI_end_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$26, DW_AT_TI_end_line(0xc3)
	.dwattr $C$DW$26, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$26

	.sect	".text"
	.clink
	.global	_SPI_getInterruptStatus

$C$DW$31	.dwtag  DW_TAG_subprogram, DW_AT_name("SPI_getInterruptStatus")
	.dwattr $C$DW$31, DW_AT_low_pc(_SPI_getInterruptStatus)
	.dwattr $C$DW$31, DW_AT_high_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_SPI_getInterruptStatus")
	.dwattr $C$DW$31, DW_AT_external
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$31, DW_AT_TI_begin_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$31, DW_AT_TI_begin_line(0xcb)
	.dwattr $C$DW$31, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$31, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 204,column 1,is_stmt,address _SPI_getInterruptStatus

	.dwfde $C$DW$CIE, _SPI_getInterruptStatus
$C$DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SPI_getInterruptStatus       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_SPI_getInterruptStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -2]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |204| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 205,column 19,is_stmt
        MOVB      ACC,#0                ; [CPU_] |205| 
        MOVL      *-SP[4],ACC           ; [CPU_] |205| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 212,column 5,is_stmt
        MOVB      ACC,#2                ; [CPU_] |212| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |212| 
        MOVL      XAR4,ACC              ; [CPU_] |212| 
        TBIT      *+XAR4[0],#6          ; [CPU_] |212| 
        BF        $C$L9,NTC             ; [CPU_] |212| 
        ; branchcc occurs ; [] |212| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 214,column 9,is_stmt
        OR        *-SP[4],#2            ; [CPU_] |214| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 217,column 5,is_stmt
        MOVB      ACC,#2                ; [CPU_] |217| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |217| 
        MOVL      XAR4,ACC              ; [CPU_] |217| 
        TBIT      *+XAR4[0],#7          ; [CPU_] |217| 
        BF        $C$L10,NTC            ; [CPU_] |217| 
        ; branchcc occurs ; [] |217| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 219,column 9,is_stmt
        OR        *-SP[4],#1            ; [CPU_] |219| 
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 222,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |222| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |222| 
        MOVL      XAR4,ACC              ; [CPU_] |222| 
        TBIT      *+XAR4[0],#7          ; [CPU_] |222| 
        BF        $C$L11,NTC            ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 224,column 9,is_stmt
        OR        *-SP[4],#8            ; [CPU_] |224| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 227,column 5,is_stmt
        MOVB      ACC,#11               ; [CPU_] |227| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |227| 
        MOVL      XAR4,ACC              ; [CPU_] |227| 
        TBIT      *+XAR4[0],#7          ; [CPU_] |227| 
        BF        $C$L12,NTC            ; [CPU_] |227| 
        ; branchcc occurs ; [] |227| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 229,column 9,is_stmt
        OR        *-SP[4],#4            ; [CPU_] |229| 
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 232,column 5,is_stmt
        MOVB      ACC,#11               ; [CPU_] |232| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |232| 
        MOVL      XAR4,ACC              ; [CPU_] |232| 
        TBIT      *+XAR4[0],#15         ; [CPU_] |232| 
        BF        $C$L13,NTC            ; [CPU_] |232| 
        ; branchcc occurs ; [] |232| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 234,column 9,is_stmt
        OR        *-SP[4],#16           ; [CPU_] |234| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 237,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |237| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 238,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$31, DW_AT_TI_end_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$31, DW_AT_TI_end_line(0xee)
	.dwattr $C$DW$31, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$31

	.sect	".text"
	.clink
	.global	_SPI_clearInterruptStatus

$C$DW$36	.dwtag  DW_TAG_subprogram, DW_AT_name("SPI_clearInterruptStatus")
	.dwattr $C$DW$36, DW_AT_low_pc(_SPI_clearInterruptStatus)
	.dwattr $C$DW$36, DW_AT_high_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_SPI_clearInterruptStatus")
	.dwattr $C$DW$36, DW_AT_external
	.dwattr $C$DW$36, DW_AT_TI_begin_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$36, DW_AT_TI_begin_line(0xf6)
	.dwattr $C$DW$36, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$36, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 247,column 1,is_stmt,address _SPI_clearInterruptStatus

	.dwfde $C$DW$CIE, _SPI_clearInterruptStatus
$C$DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg0]
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("intFlags")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_intFlags")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _SPI_clearInterruptStatus     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SPI_clearInterruptStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |247| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 256,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |256| 
        SUBB      XAR4,#6               ; [CPU_U] |256| 
        TBIT      *+XAR4[0],#1          ; [CPU_] |256| 
        BF        $C$L14,NTC            ; [CPU_] |256| 
        ; branchcc occurs ; [] |256| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 258,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |258| 
        AND       *+XAR4[0],#0xff7f     ; [CPU_] |258| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 259,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |259| 
        OR        *+XAR4[0],#0x0080     ; [CPU_] |259| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 262,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |262| 
        SUBB      XAR4,#6               ; [CPU_U] |262| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |262| 
        BF        $C$L15,NTC            ; [CPU_] |262| 
        ; branchcc occurs ; [] |262| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 264,column 9,is_stmt
        MOVB      ACC,#2                ; [CPU_] |264| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |264| 
        MOVL      XAR4,ACC              ; [CPU_] |264| 
        OR        *+XAR4[0],#0x0080     ; [CPU_] |264| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 270,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |270| 
        SUBB      XAR4,#6               ; [CPU_U] |270| 
        TBIT      *+XAR4[0],#3          ; [CPU_] |270| 
        BF        $C$L16,NTC            ; [CPU_] |270| 
        ; branchcc occurs ; [] |270| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 272,column 9,is_stmt
        MOVB      ACC,#10               ; [CPU_] |272| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |272| 
        MOVL      XAR4,ACC              ; [CPU_] |272| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |272| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 275,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |275| 
        SUBB      XAR4,#6               ; [CPU_U] |275| 
        TBIT      *+XAR4[0],#2          ; [CPU_] |275| 
        BF        $C$L17,NTC            ; [CPU_] |275| 
        ; branchcc occurs ; [] |275| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 277,column 9,is_stmt
        MOVB      ACC,#11               ; [CPU_] |277| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |277| 
        MOVL      XAR4,ACC              ; [CPU_] |277| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |277| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 280,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |280| 
        SUBB      XAR4,#6               ; [CPU_U] |280| 
        TBIT      *+XAR4[0],#4          ; [CPU_] |280| 
        BF        $C$L18,NTC            ; [CPU_] |280| 
        ; branchcc occurs ; [] |280| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 282,column 9,is_stmt
        MOVB      ACC,#11               ; [CPU_] |282| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |282| 
        MOVL      XAR4,ACC              ; [CPU_] |282| 
        OR        *+XAR4[0],#0x4000     ; [CPU_] |282| 
	.dwpsn	file "../ExtraData/driverlib2/spi.c",line 284,column 1,is_stmt
$C$L18:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$36, DW_AT_TI_end_file("../ExtraData/driverlib2/spi.c")
	.dwattr $C$DW$36, DW_AT_TI_end_line(0x11c)
	.dwattr $C$DW$36, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$36


;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$41	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_PROT_POL0PHA0"), DW_AT_const_value(0x00)
$C$DW$42	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_PROT_POL0PHA1"), DW_AT_const_value(0x02)
$C$DW$43	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_PROT_POL1PHA0"), DW_AT_const_value(0x01)
$C$DW$44	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_PROT_POL1PHA1"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("SPI_TransferProtocol")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$45	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_MODE_SLAVE"), DW_AT_const_value(0x02)
$C$DW$46	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_MODE_MASTER"), DW_AT_const_value(0x06)
$C$DW$47	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_MODE_SLAVE_OD"), DW_AT_const_value(0x00)
$C$DW$48	.dwtag  DW_TAG_enumerator, DW_AT_name("SPI_MODE_MASTER_OD"), DW_AT_const_value(0x04)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("SPI_Mode")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg0]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg1]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg2]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg3]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg20]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg21]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg22]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg23]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg24]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg25]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg26]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg28]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg29]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg30]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg31]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x20]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x21]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x22]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x23]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x24]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x25]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x26]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg4]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg6]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg8]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg10]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg12]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg14]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg16]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg17]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg18]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg19]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg5]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg7]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg9]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg11]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg13]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg15]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x30]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x33]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x34]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x37]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x38]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x40]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x43]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x44]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x47]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x48]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x49]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x27]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x28]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg27]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

