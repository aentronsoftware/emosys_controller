;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/eqep.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0530812 
	.sect	".text"
	.clink
	.global	_EQEP_setCompareConfig

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("EQEP_setCompareConfig")
	.dwattr $C$DW$1, DW_AT_low_pc(_EQEP_setCompareConfig)
	.dwattr $C$DW$1, DW_AT_high_pc(0x00)
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_EQEP_setCompareConfig")
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_TI_begin_file("../ExtraData/driverlib2/eqep.c")
	.dwattr $C$DW$1, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$1, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$1, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 53,column 1,is_stmt,address _EQEP_setCompareConfig

	.dwfde $C$DW$CIE, _EQEP_setCompareConfig
$C$DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$2, DW_AT_location[DW_OP_reg0]
$C$DW$3	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$3, DW_AT_location[DW_OP_reg12]
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("compareValue")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_compareValue")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_breg20 -10]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("cycles")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_cycles")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _EQEP_setCompareConfig        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_EQEP_setCompareConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -2]
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("config")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -3]
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("cycles")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_cycles")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_breg20 -4]
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("regValue")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_regValue")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[4],AR5           ; [CPU_] |53| 
        MOV       *-SP[3],AR4           ; [CPU_] |53| 
        MOVL      *-SP[2],ACC           ; [CPU_] |53| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 65,column 5,is_stmt
        MOVL      XAR6,*-SP[10]         ; [CPU_] |65| 
        MOVB      ACC,#6                ; [CPU_] |65| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |65| 
        MOVL      XAR4,ACC              ; [CPU_] |65| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |65| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 70,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |70| 
        AND       AH,*-SP[3],#0xc000    ; [CPU_] |70| 
        ADDB      AL,#-1                ; [CPU_] |70| 
        OR        AL,AH                 ; [CPU_] |70| 
        MOV       *-SP[5],AL            ; [CPU_] |70| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 73,column 5,is_stmt
        MOVB      ACC,#23               ; [CPU_] |73| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |73| 
        MOVL      XAR4,ACC              ; [CPU_] |73| 
        AND       AL,*+XAR4[0],#0x3000  ; [CPU_] |73| 
        OR        AL,*-SP[5]            ; [CPU_] |73| 
        MOVZ      AR6,AL                ; [CPU_] |73| 
        MOVB      ACC,#23               ; [CPU_] |73| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |73| 
        MOVL      XAR4,ACC              ; [CPU_] |73| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |73| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 81,column 5,is_stmt
        AND       AL,*-SP[3],#0x3000    ; [CPU_] |81| 
        MOV       *-SP[5],AL            ; [CPU_] |81| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 83,column 5,is_stmt
        MOVB      ACC,#20               ; [CPU_] |83| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |83| 
        MOVL      XAR4,ACC              ; [CPU_] |83| 
        AND       AL,*+XAR4[0],#0xcfff  ; [CPU_] |83| 
        OR        AL,*-SP[5]            ; [CPU_] |83| 
        MOVZ      AR6,AL                ; [CPU_] |83| 
        MOVB      ACC,#20               ; [CPU_] |83| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |83| 
        MOVL      XAR4,ACC              ; [CPU_] |83| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |83| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 86,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$1, DW_AT_TI_end_file("../ExtraData/driverlib2/eqep.c")
	.dwattr $C$DW$1, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$1, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$1

	.sect	".text"
	.clink
	.global	_EQEP_setInputPolarity

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("EQEP_setInputPolarity")
	.dwattr $C$DW$11, DW_AT_low_pc(_EQEP_setInputPolarity)
	.dwattr $C$DW$11, DW_AT_high_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_EQEP_setInputPolarity")
	.dwattr $C$DW$11, DW_AT_external
	.dwattr $C$DW$11, DW_AT_TI_begin_file("../ExtraData/driverlib2/eqep.c")
	.dwattr $C$DW$11, DW_AT_TI_begin_line(0x5e)
	.dwattr $C$DW$11, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$11, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 96,column 1,is_stmt,address _EQEP_setInputPolarity

	.dwfde $C$DW$CIE, _EQEP_setInputPolarity
$C$DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg0]
$C$DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("invertQEPA")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_invertQEPA")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg12]
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("invertQEPB")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_invertQEPB")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg14]
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("invertIndex")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_invertIndex")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -7]
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("invertStrobe")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_invertStrobe")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -8]

;***************************************************************
;* FNAME: _EQEP_setInputPolarity        FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_EQEP_setInputPolarity:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -2]
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("invertQEPA")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_invertQEPA")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -3]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("invertQEPB")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_invertQEPB")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |96| 
        MOV       *-SP[3],AR4           ; [CPU_] |96| 
        MOVL      *-SP[2],ACC           ; [CPU_] |96| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 105,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |105| 
        BF        $C$L1,EQ              ; [CPU_] |105| 
        ; branchcc occurs ; [] |105| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 107,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |107| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |107| 
        MOVL      XAR4,ACC              ; [CPU_] |107| 
        OR        *+XAR4[0],#0x0100     ; [CPU_] |107| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 108,column 5,is_stmt
        B         $C$L2,UNC             ; [CPU_] |108| 
        ; branch occurs ; [] |108| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 111,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |111| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |111| 
        MOVL      XAR4,ACC              ; [CPU_] |111| 
        AND       *+XAR4[0],#0xfeff     ; [CPU_] |111| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 117,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |117| 
        BF        $C$L3,EQ              ; [CPU_] |117| 
        ; branchcc occurs ; [] |117| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 119,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |119| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |119| 
        MOVL      XAR4,ACC              ; [CPU_] |119| 
        OR        *+XAR4[0],#0x0080     ; [CPU_] |119| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 120,column 5,is_stmt
        B         $C$L4,UNC             ; [CPU_] |120| 
        ; branch occurs ; [] |120| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 123,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |123| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |123| 
        MOVL      XAR4,ACC              ; [CPU_] |123| 
        AND       *+XAR4[0],#0xff7f     ; [CPU_] |123| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 129,column 5,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |129| 
        BF        $C$L5,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 131,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |131| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |131| 
        MOVL      XAR4,ACC              ; [CPU_] |131| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |131| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 132,column 5,is_stmt
        B         $C$L6,UNC             ; [CPU_] |132| 
        ; branch occurs ; [] |132| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 135,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |135| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |135| 
        MOVL      XAR4,ACC              ; [CPU_] |135| 
        AND       *+XAR4[0],#0xffbf     ; [CPU_] |135| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 141,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |141| 
        BF        $C$L7,EQ              ; [CPU_] |141| 
        ; branchcc occurs ; [] |141| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 143,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |143| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |143| 
        MOVL      XAR4,ACC              ; [CPU_] |143| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |143| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 144,column 5,is_stmt
        B         $C$L8,UNC             ; [CPU_] |144| 
        ; branch occurs ; [] |144| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 147,column 9,is_stmt
        MOVB      ACC,#20               ; [CPU_] |147| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |147| 
        MOVL      XAR4,ACC              ; [CPU_] |147| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |147| 
	.dwpsn	file "../ExtraData/driverlib2/eqep.c",line 149,column 1,is_stmt
$C$L8:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$11, DW_AT_TI_end_file("../ExtraData/driverlib2/eqep.c")
	.dwattr $C$DW$11, DW_AT_TI_end_line(0x95)
	.dwattr $C$DW$11, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$11


;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$21	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg0]
$C$DW$22	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg1]
$C$DW$23	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$23, DW_AT_location[DW_OP_reg2]
$C$DW$24	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg3]
$C$DW$25	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg20]
$C$DW$26	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg21]
$C$DW$27	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg22]
$C$DW$28	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg23]
$C$DW$29	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg24]
$C$DW$30	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg25]
$C$DW$31	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg26]
$C$DW$32	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg28]
$C$DW$33	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg29]
$C$DW$34	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg30]
$C$DW$35	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg31]
$C$DW$36	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_regx 0x20]
$C$DW$37	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_regx 0x21]
$C$DW$38	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_regx 0x22]
$C$DW$39	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_regx 0x23]
$C$DW$40	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_regx 0x24]
$C$DW$41	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_regx 0x25]
$C$DW$42	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_regx 0x26]
$C$DW$43	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$44	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$45	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg4]
$C$DW$46	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg6]
$C$DW$47	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg8]
$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg10]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg12]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg14]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg16]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg17]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg18]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg19]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg5]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg7]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg9]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg11]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg13]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg15]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x30]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x33]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x34]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x37]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x38]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x40]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x43]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x44]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x47]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x48]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x49]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x27]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x28]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg27]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

