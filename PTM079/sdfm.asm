;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/sdfm.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2266412 
	.sect	".text"
	.clink

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_enableFilter")
	.dwattr $C$DW$3, DW_AT_low_pc(_SDFM_enableFilter)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_SDFM_enableFilter")
	.dwattr $C$DW$3, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x160)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 353,column 1,is_stmt,address _SDFM_enableFilter

	.dwfde $C$DW$CIE, _SDFM_enableFilter
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _SDFM_enableFilter            FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_SDFM_enableFilter:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -2]
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |353| 
        MOVL      *-SP[2],ACC           ; [CPU_] |353| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 359,column 5,is_stmt
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_name("___eallow")
	.dwattr $C$DW$8, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |359| 
        ; call occurs [#___eallow] ; [] |359| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 360,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |360| 
        LSL       ACC,4                 ; [CPU_] |360| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |360| 
        ADDB      ACC,#17               ; [CPU_] |360| 
        MOVL      XAR4,ACC              ; [CPU_] |360| 
        OR        *+XAR4[0],#0x0100     ; [CPU_] |360| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 362,column 5,is_stmt
$C$DW$9	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$9, DW_AT_low_pc(0x00)
	.dwattr $C$DW$9, DW_AT_name("___edis")
	.dwattr $C$DW$9, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |362| 
        ; call occurs [#___edis] ; [] |362| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 363,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x16b)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_disableFilter")
	.dwattr $C$DW$11, DW_AT_low_pc(_SDFM_disableFilter)
	.dwattr $C$DW$11, DW_AT_high_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_SDFM_disableFilter")
	.dwattr $C$DW$11, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$11, DW_AT_TI_begin_line(0x17a)
	.dwattr $C$DW$11, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$11, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 379,column 1,is_stmt,address _SDFM_disableFilter

	.dwfde $C$DW$CIE, _SDFM_disableFilter
$C$DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg0]
$C$DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _SDFM_disableFilter           FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_SDFM_disableFilter:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -2]
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |379| 
        MOVL      *-SP[2],ACC           ; [CPU_] |379| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 385,column 5,is_stmt
$C$DW$16	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$16, DW_AT_low_pc(0x00)
	.dwattr $C$DW$16, DW_AT_name("___eallow")
	.dwattr $C$DW$16, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |385| 
        ; call occurs [#___eallow] ; [] |385| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 386,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |386| 
        LSL       ACC,4                 ; [CPU_] |386| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |386| 
        ADDB      ACC,#17               ; [CPU_] |386| 
        MOVL      XAR4,ACC              ; [CPU_] |386| 
        AND       *+XAR4[0],#0xfeff     ; [CPU_] |386| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 388,column 5,is_stmt
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_name("___edis")
	.dwattr $C$DW$17, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |388| 
        ; call occurs [#___edis] ; [] |388| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 389,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$11, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$11, DW_AT_TI_end_line(0x185)
	.dwattr $C$DW$11, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$11

	.sect	".text"
	.clink

$C$DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setFilterType")
	.dwattr $C$DW$19, DW_AT_low_pc(_SDFM_setFilterType)
	.dwattr $C$DW$19, DW_AT_high_pc(0x00)
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_SDFM_setFilterType")
	.dwattr $C$DW$19, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$19, DW_AT_TI_begin_line(0x195)
	.dwattr $C$DW$19, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$19, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 407,column 1,is_stmt,address _SDFM_setFilterType

	.dwfde $C$DW$CIE, _SDFM_setFilterType
$C$DW$20	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_reg0]
$C$DW$21	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg12]
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterType")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_filterType")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setFilterType           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setFilterType:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -2]
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -3]
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("filterType")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_filterType")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -4]
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |407| 
        MOV       *-SP[3],AR4           ; [CPU_] |407| 
        MOVL      *-SP[2],ACC           ; [CPU_] |407| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 412,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |412| 
        LSL       ACC,4                 ; [CPU_] |412| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |412| 
        ADDB      ACC,#17               ; [CPU_] |412| 
        MOVL      *-SP[6],ACC           ; [CPU_] |412| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 417,column 5,is_stmt
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("___eallow")
	.dwattr $C$DW$27, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |417| 
        ; call occurs [#___eallow] ; [] |417| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 418,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |418| 
        AND       AL,*+XAR4[0],#0xf3ff  ; [CPU_] |418| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |418| 
        MOVZ      AR6,AL                ; [CPU_] |418| 
        MOV       ACC,*-SP[4] << #6     ; [CPU_] |418| 
        OR        AL,AR6                ; [CPU_] |418| 
        MOV       *+XAR4[0],AL          ; [CPU_] |418| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 420,column 5,is_stmt
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("___edis")
	.dwattr $C$DW$28, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |420| 
        ; call occurs [#___edis] ; [] |420| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 421,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$19, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$19, DW_AT_TI_end_line(0x1a5)
	.dwattr $C$DW$19, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$19

	.sect	".text"
	.clink

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setFilterOverSamplingRatio")
	.dwattr $C$DW$30, DW_AT_low_pc(_SDFM_setFilterOverSamplingRatio)
	.dwattr $C$DW$30, DW_AT_high_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_SDFM_setFilterOverSamplingRatio")
	.dwattr $C$DW$30, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$30, DW_AT_TI_begin_line(0x1b7)
	.dwattr $C$DW$30, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$30, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 441,column 1,is_stmt,address _SDFM_setFilterOverSamplingRatio

	.dwfde $C$DW$CIE, _SDFM_setFilterOverSamplingRatio
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg0]
$C$DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg12]
$C$DW$33	.dwtag  DW_TAG_formal_parameter, DW_AT_name("overSamplingRatio")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_overSamplingRatio")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setFilterOverSamplingRatio FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setFilterOverSamplingRatio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -2]
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -3]
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("overSamplingRatio")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_overSamplingRatio")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -4]
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |441| 
        MOV       *-SP[3],AR4           ; [CPU_] |441| 
        MOVL      *-SP[2],ACC           ; [CPU_] |441| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 447,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |447| 
        LSL       ACC,4                 ; [CPU_] |447| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |447| 
        ADDB      ACC,#17               ; [CPU_] |447| 
        MOVL      *-SP[6],ACC           ; [CPU_] |447| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 452,column 5,is_stmt
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("___eallow")
	.dwattr $C$DW$38, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |452| 
        ; call occurs [#___eallow] ; [] |452| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 453,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |453| 
        AND       AL,*+XAR4[0],#0xff00  ; [CPU_] |453| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |453| 
        OR        AL,*-SP[4]            ; [CPU_] |453| 
        MOV       *+XAR4[0],AL          ; [CPU_] |453| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 455,column 5,is_stmt
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("___edis")
	.dwattr $C$DW$39, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |455| 
        ; call occurs [#___edis] ; [] |455| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 456,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$30, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$30, DW_AT_TI_end_line(0x1c8)
	.dwattr $C$DW$30, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$30

	.sect	".text"
	.clink

$C$DW$41	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setOutputDataFormat")
	.dwattr $C$DW$41, DW_AT_low_pc(_SDFM_setOutputDataFormat)
	.dwattr $C$DW$41, DW_AT_high_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_SDFM_setOutputDataFormat")
	.dwattr $C$DW$41, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$41, DW_AT_TI_begin_line(0x1fb)
	.dwattr $C$DW$41, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$41, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 509,column 1,is_stmt,address _SDFM_setOutputDataFormat

	.dwfde $C$DW$CIE, _SDFM_setOutputDataFormat
$C$DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg0]
$C$DW$43	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg12]
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataFormat")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_dataFormat")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setOutputDataFormat     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setOutputDataFormat:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -2]
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -3]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("dataFormat")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_dataFormat")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -4]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |509| 
        MOV       *-SP[3],AR4           ; [CPU_] |509| 
        MOVL      *-SP[2],ACC           ; [CPU_] |509| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 514,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |514| 
        LSL       ACC,4                 ; [CPU_] |514| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |514| 
        ADDB      ACC,#18               ; [CPU_] |514| 
        MOVL      *-SP[6],ACC           ; [CPU_] |514| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 519,column 5,is_stmt
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_name("___eallow")
	.dwattr $C$DW$49, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |519| 
        ; call occurs [#___eallow] ; [] |519| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 520,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |520| 
        AND       AL,*+XAR4[0],#0xfbff  ; [CPU_] |520| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |520| 
        MOVZ      AR6,AL                ; [CPU_] |520| 
        MOV       ACC,*-SP[4] << #10    ; [CPU_] |520| 
        OR        AL,AR6                ; [CPU_] |520| 
        MOV       *+XAR4[0],AL          ; [CPU_] |520| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 522,column 5,is_stmt
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_name("___edis")
	.dwattr $C$DW$50, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |522| 
        ; call occurs [#___edis] ; [] |522| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 523,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$51	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$51, DW_AT_low_pc(0x00)
	.dwattr $C$DW$51, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$41, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$41, DW_AT_TI_end_line(0x20b)
	.dwattr $C$DW$41, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$41

	.sect	".text"
	.clink

$C$DW$52	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setDataShiftValue")
	.dwattr $C$DW$52, DW_AT_low_pc(_SDFM_setDataShiftValue)
	.dwattr $C$DW$52, DW_AT_high_pc(0x00)
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_SDFM_setDataShiftValue")
	.dwattr $C$DW$52, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$52, DW_AT_TI_begin_line(0x21e)
	.dwattr $C$DW$52, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$52, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 544,column 1,is_stmt,address _SDFM_setDataShiftValue

	.dwfde $C$DW$CIE, _SDFM_setDataShiftValue
$C$DW$53	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg0]
$C$DW$54	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg12]
$C$DW$55	.dwtag  DW_TAG_formal_parameter, DW_AT_name("shiftValue")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_shiftValue")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setDataShiftValue       FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setDataShiftValue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -2]
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg20 -3]
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("shiftValue")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_shiftValue")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg20 -4]
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |544| 
        MOV       *-SP[3],AR4           ; [CPU_] |544| 
        MOVL      *-SP[2],ACC           ; [CPU_] |544| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 550,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |550| 
        LSL       ACC,4                 ; [CPU_] |550| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |550| 
        ADDB      ACC,#18               ; [CPU_] |550| 
        MOVL      *-SP[6],ACC           ; [CPU_] |550| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 555,column 5,is_stmt
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_name("___eallow")
	.dwattr $C$DW$60, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |555| 
        ; call occurs [#___eallow] ; [] |555| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 556,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |556| 
        AND       AL,*+XAR4[0],#0x07ff  ; [CPU_] |556| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |556| 
        MOVZ      AR6,AL                ; [CPU_] |556| 
        MOV       ACC,*-SP[4] << #11    ; [CPU_] |556| 
        OR        AL,AR6                ; [CPU_] |556| 
        MOV       *+XAR4[0],AL          ; [CPU_] |556| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 558,column 5,is_stmt
$C$DW$61	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$61, DW_AT_low_pc(0x00)
	.dwattr $C$DW$61, DW_AT_name("___edis")
	.dwattr $C$DW$61, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |558| 
        ; call occurs [#___edis] ; [] |558| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 559,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$62	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$62, DW_AT_low_pc(0x00)
	.dwattr $C$DW$62, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$52, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$52, DW_AT_TI_end_line(0x22f)
	.dwattr $C$DW$52, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$52

	.sect	".text"
	.clink

$C$DW$63	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setCompFilterHighThreshold")
	.dwattr $C$DW$63, DW_AT_low_pc(_SDFM_setCompFilterHighThreshold)
	.dwattr $C$DW$63, DW_AT_high_pc(0x00)
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_SDFM_setCompFilterHighThreshold")
	.dwattr $C$DW$63, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$63, DW_AT_TI_begin_line(0x242)
	.dwattr $C$DW$63, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$63, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 580,column 1,is_stmt,address _SDFM_setCompFilterHighThreshold

	.dwfde $C$DW$CIE, _SDFM_setCompFilterHighThreshold
$C$DW$64	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg0]
$C$DW$65	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg12]
$C$DW$66	.dwtag  DW_TAG_formal_parameter, DW_AT_name("highThreshold")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_highThreshold")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setCompFilterHighThreshold FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setCompFilterHighThreshold:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -2]
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -3]
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("highThreshold")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_highThreshold")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -4]
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |580| 
        MOV       *-SP[3],AR4           ; [CPU_] |580| 
        MOVL      *-SP[2],ACC           ; [CPU_] |580| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 586,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |586| 
        LSL       ACC,4                 ; [CPU_] |586| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |586| 
        ADDB      ACC,#19               ; [CPU_] |586| 
        MOVL      *-SP[6],ACC           ; [CPU_] |586| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 591,column 5,is_stmt
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_name("___eallow")
	.dwattr $C$DW$71, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |591| 
        ; call occurs [#___eallow] ; [] |591| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 592,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |592| 
        AND       AL,*+XAR4[0],#0x8000  ; [CPU_] |592| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |592| 
        OR        AL,*-SP[4]            ; [CPU_] |592| 
        MOV       *+XAR4[0],AL          ; [CPU_] |592| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 593,column 5,is_stmt
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_name("___edis")
	.dwattr $C$DW$72, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |593| 
        ; call occurs [#___edis] ; [] |593| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 594,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$63, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$63, DW_AT_TI_end_line(0x252)
	.dwattr $C$DW$63, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$63

	.sect	".text"
	.clink

$C$DW$74	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setCompFilterLowThreshold")
	.dwattr $C$DW$74, DW_AT_low_pc(_SDFM_setCompFilterLowThreshold)
	.dwattr $C$DW$74, DW_AT_high_pc(0x00)
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_SDFM_setCompFilterLowThreshold")
	.dwattr $C$DW$74, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$74, DW_AT_TI_begin_line(0x265)
	.dwattr $C$DW$74, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$74, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 615,column 1,is_stmt,address _SDFM_setCompFilterLowThreshold

	.dwfde $C$DW$CIE, _SDFM_setCompFilterLowThreshold
$C$DW$75	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg0]
$C$DW$76	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg12]
$C$DW$77	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lowThreshold")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_lowThreshold")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setCompFilterLowThreshold FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setCompFilterLowThreshold:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_breg20 -2]
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_breg20 -3]
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("lowThreshold")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_lowThreshold")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_breg20 -4]
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |615| 
        MOV       *-SP[3],AR4           ; [CPU_] |615| 
        MOVL      *-SP[2],ACC           ; [CPU_] |615| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 621,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |621| 
        LSL       ACC,4                 ; [CPU_] |621| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |621| 
        ADDB      ACC,#20               ; [CPU_] |621| 
        MOVL      *-SP[6],ACC           ; [CPU_] |621| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 626,column 5,is_stmt
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_name("___eallow")
	.dwattr $C$DW$82, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |626| 
        ; call occurs [#___eallow] ; [] |626| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 627,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |627| 
        AND       AL,*+XAR4[0],#0x8000  ; [CPU_] |627| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |627| 
        OR        AL,*-SP[4]            ; [CPU_] |627| 
        MOV       *+XAR4[0],AL          ; [CPU_] |627| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 628,column 5,is_stmt
$C$DW$83	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$83, DW_AT_low_pc(0x00)
	.dwattr $C$DW$83, DW_AT_name("___edis")
	.dwattr $C$DW$83, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |628| 
        ; call occurs [#___edis] ; [] |628| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 629,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$84	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$84, DW_AT_low_pc(0x00)
	.dwattr $C$DW$84, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$74, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$74, DW_AT_TI_end_line(0x275)
	.dwattr $C$DW$74, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$74

	.sect	".text"
	.clink

$C$DW$85	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setComparatorFilterType")
	.dwattr $C$DW$85, DW_AT_low_pc(_SDFM_setComparatorFilterType)
	.dwattr $C$DW$85, DW_AT_high_pc(0x00)
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_SDFM_setComparatorFilterType")
	.dwattr $C$DW$85, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$85, DW_AT_TI_begin_line(0x2f6)
	.dwattr $C$DW$85, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$85, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 760,column 1,is_stmt,address _SDFM_setComparatorFilterType

	.dwfde $C$DW$CIE, _SDFM_setComparatorFilterType
$C$DW$86	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg0]
$C$DW$87	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg12]
$C$DW$88	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterType")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_filterType")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setComparatorFilterType FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setComparatorFilterType:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -2]
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -3]
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("filterType")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_filterType")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg20 -4]
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |760| 
        MOV       *-SP[3],AR4           ; [CPU_] |760| 
        MOVL      *-SP[2],ACC           ; [CPU_] |760| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 765,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |765| 
        LSL       ACC,4                 ; [CPU_] |765| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |765| 
        ADDB      ACC,#21               ; [CPU_] |765| 
        MOVL      *-SP[6],ACC           ; [CPU_] |765| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 770,column 5,is_stmt
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("___eallow")
	.dwattr $C$DW$93, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |770| 
        ; call occurs [#___eallow] ; [] |770| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 771,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |771| 
        AND       AL,*+XAR4[0],#0xfe7f  ; [CPU_] |771| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |771| 
        MOVZ      AR6,AL                ; [CPU_] |771| 
        MOV       ACC,*-SP[4] << #3     ; [CPU_] |771| 
        OR        AL,AR6                ; [CPU_] |771| 
        MOV       *+XAR4[0],AL          ; [CPU_] |771| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 773,column 5,is_stmt
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("___edis")
	.dwattr $C$DW$94, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |773| 
        ; call occurs [#___edis] ; [] |773| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 774,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$85, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$85, DW_AT_TI_end_line(0x306)
	.dwattr $C$DW$85, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$85

	.sect	".text"
	.clink

$C$DW$96	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_setCompFilterOverSamplingRatio")
	.dwattr $C$DW$96, DW_AT_low_pc(_SDFM_setCompFilterOverSamplingRatio)
	.dwattr $C$DW$96, DW_AT_high_pc(0x00)
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_SDFM_setCompFilterOverSamplingRatio")
	.dwattr $C$DW$96, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$96, DW_AT_TI_begin_line(0x318)
	.dwattr $C$DW$96, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$96, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 795,column 1,is_stmt,address _SDFM_setCompFilterOverSamplingRatio

	.dwfde $C$DW$CIE, _SDFM_setCompFilterOverSamplingRatio
$C$DW$97	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg0]
$C$DW$98	.dwtag  DW_TAG_formal_parameter, DW_AT_name("filterNumber")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg12]
$C$DW$99	.dwtag  DW_TAG_formal_parameter, DW_AT_name("overSamplingRatio")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_overSamplingRatio")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_setCompFilterOverSamplingRatio FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_setCompFilterOverSamplingRatio:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_breg20 -2]
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("filterNumber")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_filterNumber")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_breg20 -3]
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("overSamplingRatio")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_overSamplingRatio")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_breg20 -4]
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |795| 
        MOV       *-SP[3],AR4           ; [CPU_] |795| 
        MOVL      *-SP[2],ACC           ; [CPU_] |795| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 801,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |801| 
        LSL       ACC,4                 ; [CPU_] |801| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |801| 
        ADDB      ACC,#21               ; [CPU_] |801| 
        MOVL      *-SP[6],ACC           ; [CPU_] |801| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 806,column 5,is_stmt
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_name("___eallow")
	.dwattr $C$DW$104, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |806| 
        ; call occurs [#___eallow] ; [] |806| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 807,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |807| 
        AND       AL,*+XAR4[0],#0xffe0  ; [CPU_] |807| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |807| 
        OR        AL,*-SP[4]            ; [CPU_] |807| 
        MOV       *+XAR4[0],AL          ; [CPU_] |807| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 809,column 5,is_stmt
$C$DW$105	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$105, DW_AT_low_pc(0x00)
	.dwattr $C$DW$105, DW_AT_name("___edis")
	.dwattr $C$DW$105, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |809| 
        ; call occurs [#___edis] ; [] |809| 
	.dwpsn	file "..\ExtraData\driverlib2\sdfm.h",line 810,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$96, DW_AT_TI_end_file("..\ExtraData\driverlib2\sdfm.h")
	.dwattr $C$DW$96, DW_AT_TI_end_line(0x32a)
	.dwattr $C$DW$96, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$96

	.sect	".text"
	.clink
	.global	_SDFM_configComparator

$C$DW$107	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_configComparator")
	.dwattr $C$DW$107, DW_AT_low_pc(_SDFM_configComparator)
	.dwattr $C$DW$107, DW_AT_high_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_SDFM_configComparator")
	.dwattr $C$DW$107, DW_AT_external
	.dwattr $C$DW$107, DW_AT_TI_begin_file("../ExtraData/driverlib2/sdfm.c")
	.dwattr $C$DW$107, DW_AT_TI_begin_line(0x56)
	.dwattr $C$DW$107, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$107, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 87,column 1,is_stmt,address _SDFM_configComparator

	.dwfde $C$DW$CIE, _SDFM_configComparator
$C$DW$108	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg0]
$C$DW$109	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config1")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_config1")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg12]
$C$DW$110	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config2")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_config2")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_breg20 -10]

;***************************************************************
;* FNAME: _SDFM_configComparator        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SDFM_configComparator:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$111	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_breg20 -2]
$C$DW$112	.dwtag  DW_TAG_variable, DW_AT_name("config1")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_config1")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_breg20 -3]
$C$DW$113	.dwtag  DW_TAG_variable, DW_AT_name("filter")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_filter")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_breg20 -4]
$C$DW$114	.dwtag  DW_TAG_variable, DW_AT_name("ratio")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_ratio")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_breg20 -5]
$C$DW$115	.dwtag  DW_TAG_variable, DW_AT_name("filterType")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_filterType")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[3],AR4           ; [CPU_] |87| 
        MOVL      *-SP[2],ACC           ; [CPU_] |87| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 92,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |92| 
        ANDB      AL,#0x03              ; [CPU_] |92| 
        MOV       *-SP[4],AL            ; [CPU_] |92| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 93,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |93| 
        LSR       AL,8                  ; [CPU_] |93| 
        MOV       *-SP[5],AL            ; [CPU_] |93| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 94,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |94| 
        ANDB      AL,#0x30              ; [CPU_] |94| 
        MOV       *-SP[6],AL            ; [CPU_] |94| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 99,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |99| 
        CMPB      AL,#31                ; [CPU_] |99| 
        B         $C$L1,LOS             ; [CPU_] |99| 
        ; branchcc occurs ; [] |99| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 101,column 9,is_stmt
        MOVB      *-SP[5],#31,UNC       ; [CPU_] |101| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 107,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |107| 
        MOVZ      AR4,*-SP[4]           ; [CPU_] |107| 
        MOVZ      AR5,*-SP[6]           ; [CPU_] |107| 
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("_SDFM_setComparatorFilterType")
	.dwattr $C$DW$116, DW_AT_TI_call
        LCR       #_SDFM_setComparatorFilterType ; [CPU_] |107| 
        ; call occurs [#_SDFM_setComparatorFilterType] ; [] |107| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 112,column 5,is_stmt
        MOVZ      AR4,*-SP[4]           ; [CPU_] |112| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |112| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |112| 
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_name("_SDFM_setCompFilterOverSamplingRatio")
	.dwattr $C$DW$117, DW_AT_TI_call
        LCR       #_SDFM_setCompFilterOverSamplingRatio ; [CPU_] |112| 
        ; call occurs [#_SDFM_setCompFilterOverSamplingRatio] ; [] |112| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 117,column 5,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |117| 
        MOVZ      AR4,*-SP[4]           ; [CPU_] |117| 
        MOVU      ACC,AH                ; [CPU_] |117| 
        MOVZ      AR5,AL                ; [CPU_] |117| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |117| 
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("_SDFM_setCompFilterHighThreshold")
	.dwattr $C$DW$118, DW_AT_TI_call
        LCR       #_SDFM_setCompFilterHighThreshold ; [CPU_] |117| 
        ; call occurs [#_SDFM_setCompFilterHighThreshold] ; [] |117| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 123,column 5,is_stmt
        MOVZ      AR5,*-SP[10]          ; [CPU_] |123| 
        MOVZ      AR4,*-SP[4]           ; [CPU_] |123| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |123| 
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_name("_SDFM_setCompFilterLowThreshold")
	.dwattr $C$DW$119, DW_AT_TI_call
        LCR       #_SDFM_setCompFilterLowThreshold ; [CPU_] |123| 
        ; call occurs [#_SDFM_setCompFilterLowThreshold] ; [] |123| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 126,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$107, DW_AT_TI_end_file("../ExtraData/driverlib2/sdfm.c")
	.dwattr $C$DW$107, DW_AT_TI_end_line(0x7e)
	.dwattr $C$DW$107, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$107

	.sect	".text"
	.clink
	.global	_SDFM_configDataFilter

$C$DW$121	.dwtag  DW_TAG_subprogram, DW_AT_name("SDFM_configDataFilter")
	.dwattr $C$DW$121, DW_AT_low_pc(_SDFM_configDataFilter)
	.dwattr $C$DW$121, DW_AT_high_pc(0x00)
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_SDFM_configDataFilter")
	.dwattr $C$DW$121, DW_AT_external
	.dwattr $C$DW$121, DW_AT_TI_begin_file("../ExtraData/driverlib2/sdfm.c")
	.dwattr $C$DW$121, DW_AT_TI_begin_line(0x85)
	.dwattr $C$DW$121, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$121, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 134,column 1,is_stmt,address _SDFM_configDataFilter

	.dwfde $C$DW$CIE, _SDFM_configDataFilter
$C$DW$122	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg0]
$C$DW$123	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config1")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_config1")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg12]
$C$DW$124	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config2")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_config2")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SDFM_configDataFilter        FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  7 Auto,  0 SOE     *
;***************************************************************

_SDFM_configDataFilter:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_breg20 -2]
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("config1")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_config1")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_breg20 -3]
$C$DW$127	.dwtag  DW_TAG_variable, DW_AT_name("config2")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_config2")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_breg20 -4]
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("filter")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_filter")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -5]
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("ratio")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_ratio")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_breg20 -6]
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("filterType")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_filterType")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_breg20 -7]
        MOV       *-SP[4],AR5           ; [CPU_] |134| 
        MOV       *-SP[3],AR4           ; [CPU_] |134| 
        MOVL      *-SP[2],ACC           ; [CPU_] |134| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 139,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |139| 
        ANDB      AL,#0x03              ; [CPU_] |139| 
        MOV       *-SP[5],AL            ; [CPU_] |139| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 140,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |140| 
        LSR       AL,8                  ; [CPU_] |140| 
        MOV       *-SP[6],AL            ; [CPU_] |140| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 141,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |141| 
        ANDB      AL,#0x30              ; [CPU_] |141| 
        MOV       *-SP[7],AL            ; [CPU_] |141| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 146,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |146| 
        CMPB      AL,#255               ; [CPU_] |146| 
        B         $C$L2,LOS             ; [CPU_] |146| 
        ; branchcc occurs ; [] |146| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 148,column 9,is_stmt
        MOVB      *-SP[6],#255,UNC      ; [CPU_] |148| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 154,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |154| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |154| 
        MOVZ      AR5,*-SP[7]           ; [CPU_] |154| 
$C$DW$131	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$131, DW_AT_low_pc(0x00)
	.dwattr $C$DW$131, DW_AT_name("_SDFM_setFilterType")
	.dwattr $C$DW$131, DW_AT_TI_call
        LCR       #_SDFM_setFilterType  ; [CPU_] |154| 
        ; call occurs [#_SDFM_setFilterType] ; [] |154| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 159,column 5,is_stmt
        MOVZ      AR4,*-SP[5]           ; [CPU_] |159| 
        MOVZ      AR5,*-SP[6]           ; [CPU_] |159| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |159| 
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_name("_SDFM_setFilterOverSamplingRatio")
	.dwattr $C$DW$132, DW_AT_TI_call
        LCR       #_SDFM_setFilterOverSamplingRatio ; [CPU_] |159| 
        ; call occurs [#_SDFM_setFilterOverSamplingRatio] ; [] |159| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 164,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |164| 
        ANDB      AL,#0x02              ; [CPU_] |164| 
        CMPB      AL,#2                 ; [CPU_] |164| 
        BF        $C$L3,NEQ             ; [CPU_] |164| 
        ; branchcc occurs ; [] |164| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 166,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |166| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |166| 
$C$DW$133	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$133, DW_AT_low_pc(0x00)
	.dwattr $C$DW$133, DW_AT_name("_SDFM_enableFilter")
	.dwattr $C$DW$133, DW_AT_TI_call
        LCR       #_SDFM_enableFilter   ; [CPU_] |166| 
        ; call occurs [#_SDFM_enableFilter] ; [] |166| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 167,column 5,is_stmt
        B         $C$L4,UNC             ; [CPU_] |167| 
        ; branch occurs ; [] |167| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 170,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |170| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |170| 
$C$DW$134	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$134, DW_AT_low_pc(0x00)
	.dwattr $C$DW$134, DW_AT_name("_SDFM_disableFilter")
	.dwattr $C$DW$134, DW_AT_TI_call
        LCR       #_SDFM_disableFilter  ; [CPU_] |170| 
        ; call occurs [#_SDFM_disableFilter] ; [] |170| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 176,column 5,is_stmt
        AND       AL,*-SP[4],#0x0001    ; [CPU_] |176| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |176| 
        MOVZ      AR5,AL                ; [CPU_] |176| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |176| 
$C$DW$135	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$135, DW_AT_low_pc(0x00)
	.dwattr $C$DW$135, DW_AT_name("_SDFM_setOutputDataFormat")
	.dwattr $C$DW$135, DW_AT_TI_call
        LCR       #_SDFM_setOutputDataFormat ; [CPU_] |176| 
        ; call occurs [#_SDFM_setOutputDataFormat] ; [] |176| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 182,column 5,is_stmt
        TBIT      *-SP[4],#0            ; [CPU_] |182| 
        BF        $C$L5,TC              ; [CPU_] |182| 
        ; branchcc occurs ; [] |182| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 184,column 9,is_stmt
        AND       AL,*-SP[4],#0x007c    ; [CPU_] |184| 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |184| 
        LSR       AL,2                  ; [CPU_] |184| 
        MOVZ      AR5,AL                ; [CPU_] |184| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |184| 
$C$DW$136	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$136, DW_AT_low_pc(0x00)
	.dwattr $C$DW$136, DW_AT_name("_SDFM_setDataShiftValue")
	.dwattr $C$DW$136, DW_AT_TI_call
        LCR       #_SDFM_setDataShiftValue ; [CPU_] |184| 
        ; call occurs [#_SDFM_setDataShiftValue] ; [] |184| 
	.dwpsn	file "../ExtraData/driverlib2/sdfm.c",line 186,column 1,is_stmt
$C$L5:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$137	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$137, DW_AT_low_pc(0x00)
	.dwattr $C$DW$137, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$121, DW_AT_TI_end_file("../ExtraData/driverlib2/sdfm.c")
	.dwattr $C$DW$121, DW_AT_TI_end_line(0xba)
	.dwattr $C$DW$121, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$121

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___eallow
	.global	___edis

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$138	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_1"), DW_AT_const_value(0x00)
$C$DW$139	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_2"), DW_AT_const_value(0x01)
$C$DW$140	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_3"), DW_AT_const_value(0x02)
$C$DW$141	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_4"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("SDFM_FilterNumber")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$142	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_SINC_FAST"), DW_AT_const_value(0x00)
$C$DW$143	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_SINC_1"), DW_AT_const_value(0x10)
$C$DW$144	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_SINC_2"), DW_AT_const_value(0x20)
$C$DW$145	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_FILTER_SINC_3"), DW_AT_const_value(0x30)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("SDFM_FilterType")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$146	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_DATA_FORMAT_16_BIT"), DW_AT_const_value(0x00)
$C$DW$147	.dwtag  DW_TAG_enumerator, DW_AT_name("SDFM_DATA_FORMAT_32_BIT"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("SDFM_OutputDataFormat")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg0]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_reg1]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg2]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg3]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg20]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg21]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg22]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_reg23]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_reg24]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_reg25]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_reg26]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_reg28]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_reg29]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_reg30]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_reg31]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x20]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x21]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x22]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_regx 0x23]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x24]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x25]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x26]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg4]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg6]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_reg8]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_reg10]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_reg12]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_reg14]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_reg16]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_reg17]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_reg18]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_reg19]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_reg5]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_reg7]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_reg9]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_reg11]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_reg13]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_reg15]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$190	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_regx 0x30]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_regx 0x33]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_regx 0x34]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_regx 0x37]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_regx 0x38]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_regx 0x40]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_regx 0x43]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_regx 0x44]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_regx 0x47]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_regx 0x48]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_regx 0x49]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_regx 0x27]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_regx 0x28]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_reg27]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

