;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:05 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1976412 
	.sect	".text"
	.clink
	.global	_XBAR_setOutputMuxConfig

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("XBAR_setOutputMuxConfig")
	.dwattr $C$DW$3, DW_AT_low_pc(_XBAR_setOutputMuxConfig)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_XBAR_setOutputMuxConfig")
	.dwattr $C$DW$3, DW_AT_external
	.dwattr $C$DW$3, DW_AT_TI_begin_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 52,column 1,is_stmt,address _XBAR_setOutputMuxConfig

	.dwfde $C$DW$CIE, _XBAR_setOutputMuxConfig
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("output")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_output")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("muxConfig")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_muxConfig")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _XBAR_setOutputMuxConfig      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_XBAR_setOutputMuxConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("output")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_output")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -1]
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("muxConfig")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_muxConfig")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -2]
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("shift")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_shift")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_breg20 -4]
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[2],AH            ; [CPU_] |52| 
        MOV       *-SP[1],AL            ; [CPU_] |52| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 60,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[2]           ; [CPU_] |60| 
        TBIT      AL,#13                ; [CPU_] |60| 
        BF        $C$L1,NTC             ; [CPU_] |60| 
        ; branchcc occurs ; [] |60| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 62,column 9,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |62| 
        ADDB      AL,#2                 ; [CPU_] |62| 
        MOV       *-SP[5],AL            ; [CPU_] |62| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 63,column 5,is_stmt
        B         $C$L2,UNC             ; [CPU_] |63| 
        ; branch occurs ; [] |63| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 66,column 9,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |66| 
        MOV       *-SP[5],AL            ; [CPU_] |66| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 72,column 5,is_stmt
        MOV       ACC,*-SP[2]           ; [CPU_] |72| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,8                 ; [CPU_] |72| 
        MOVB      AH,#0                 ; [CPU_] |72| 
        ANDB      AL,#0x1f              ; [CPU_] |72| 
        MOVL      *-SP[4],ACC           ; [CPU_] |72| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 77,column 5,is_stmt
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_name("___eallow")
	.dwattr $C$DW$10, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |77| 
        ; call occurs [#___eallow] ; [] |77| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 79,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |79| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |79| 
        MOV       ACC,*-SP[2]           ; [CPU_] |79| 
        MOVL      P,ACC                 ; [CPU_] |79| 
        MOV       PH,#0                 ; [CPU_] |79| 
        ADD       AR4,#31360            ; [CPU_] |79| 
        MOV       AL,PL                 ; [CPU_] |79| 
        ADD       AR5,#31360            ; [CPU_] |79| 
        ANDB      AL,#0x03              ; [CPU_] |79| 
        MOV       PL,AL                 ; [CPU_] |79| 
        MOVP      T,*-SP[4]             ; [CPU_] 
        LSLL      ACC,T                 ; [CPU_] |79| 
        MOVL      P,ACC                 ; [CPU_] |79| 
        MOVB      ACC,#3                ; [CPU_] |79| 
        LSLL      ACC,T                 ; [CPU_] |79| 
        NOT       ACC                   ; [CPU_] |79| 
        AND       AL,*+XAR4[0]          ; [CPU_] |79| 
        OR        AL,PL                 ; [CPU_] |79| 
        AND       AH,*+XAR4[1]          ; [CPU_] |79| 
        OR        AH,PH                 ; [CPU_] |79| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |79| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 84,column 5,is_stmt
$C$DW$11	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$11, DW_AT_low_pc(0x00)
	.dwattr $C$DW$11, DW_AT_name("___edis")
	.dwattr $C$DW$11, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |84| 
        ; call occurs [#___edis] ; [] |84| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 85,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$12	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$12, DW_AT_low_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x55)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink
	.global	_XBAR_setEPWMMuxConfig

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("XBAR_setEPWMMuxConfig")
	.dwattr $C$DW$13, DW_AT_low_pc(_XBAR_setEPWMMuxConfig)
	.dwattr $C$DW$13, DW_AT_high_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_XBAR_setEPWMMuxConfig")
	.dwattr $C$DW$13, DW_AT_external
	.dwattr $C$DW$13, DW_AT_TI_begin_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$13, DW_AT_TI_begin_line(0x5d)
	.dwattr $C$DW$13, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$13, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 94,column 1,is_stmt,address _XBAR_setEPWMMuxConfig

	.dwfde $C$DW$CIE, _XBAR_setEPWMMuxConfig
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("trip")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_trip")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg0]
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("muxConfig")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_muxConfig")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _XBAR_setEPWMMuxConfig        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_XBAR_setEPWMMuxConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("trip")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_trip")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -1]
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("muxConfig")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_muxConfig")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -2]
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("shift")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_shift")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -4]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[2],AH            ; [CPU_] |94| 
        MOV       *-SP[1],AL            ; [CPU_] |94| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 102,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[2]           ; [CPU_] |102| 
        TBIT      AL,#13                ; [CPU_] |102| 
        BF        $C$L3,NTC             ; [CPU_] |102| 
        ; branchcc occurs ; [] |102| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 104,column 9,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |104| 
        ADDB      AL,#2                 ; [CPU_] |104| 
        MOV       *-SP[5],AL            ; [CPU_] |104| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 105,column 5,is_stmt
        B         $C$L4,UNC             ; [CPU_] |105| 
        ; branch occurs ; [] |105| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 108,column 9,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |108| 
        MOV       *-SP[5],AL            ; [CPU_] |108| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 114,column 5,is_stmt
        MOV       ACC,*-SP[2]           ; [CPU_] |114| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,8                 ; [CPU_] |114| 
        MOVB      AH,#0                 ; [CPU_] |114| 
        ANDB      AL,#0x1f              ; [CPU_] |114| 
        MOVL      *-SP[4],ACC           ; [CPU_] |114| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 119,column 5,is_stmt
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_name("___eallow")
	.dwattr $C$DW$20, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |119| 
        ; call occurs [#___eallow] ; [] |119| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 121,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |121| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |121| 
        MOV       ACC,*-SP[2]           ; [CPU_] |121| 
        MOVL      P,ACC                 ; [CPU_] |121| 
        MOV       PH,#0                 ; [CPU_] |121| 
        ADD       AR4,#31232            ; [CPU_] |121| 
        MOV       AL,PL                 ; [CPU_] |121| 
        ADD       AR5,#31232            ; [CPU_] |121| 
        ANDB      AL,#0x03              ; [CPU_] |121| 
        MOV       PL,AL                 ; [CPU_] |121| 
        MOVP      T,*-SP[4]             ; [CPU_] 
        LSLL      ACC,T                 ; [CPU_] |121| 
        MOVL      P,ACC                 ; [CPU_] |121| 
        MOVB      ACC,#3                ; [CPU_] |121| 
        LSLL      ACC,T                 ; [CPU_] |121| 
        NOT       ACC                   ; [CPU_] |121| 
        AND       AL,*+XAR4[0]          ; [CPU_] |121| 
        OR        AL,PL                 ; [CPU_] |121| 
        AND       AH,*+XAR4[1]          ; [CPU_] |121| 
        OR        AH,PH                 ; [CPU_] |121| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |121| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 125,column 5,is_stmt
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("___edis")
	.dwattr $C$DW$21, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |125| 
        ; call occurs [#___edis] ; [] |125| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 126,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$13, DW_AT_TI_end_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$13, DW_AT_TI_end_line(0x7e)
	.dwattr $C$DW$13, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$13

	.sect	".text"
	.clink
	.global	_XBAR_setCLBMuxConfig

$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("XBAR_setCLBMuxConfig")
	.dwattr $C$DW$23, DW_AT_low_pc(_XBAR_setCLBMuxConfig)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_XBAR_setCLBMuxConfig")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_TI_begin_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x86)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 135,column 1,is_stmt,address _XBAR_setCLBMuxConfig

	.dwfde $C$DW$CIE, _XBAR_setCLBMuxConfig
$C$DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("auxSignal")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_auxSignal")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg0]
$C$DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("muxConfig")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_muxConfig")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _XBAR_setCLBMuxConfig         FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_XBAR_setCLBMuxConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("auxSignal")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_auxSignal")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -1]
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("muxConfig")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_muxConfig")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -2]
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("shift")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_shift")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -4]
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[2],AH            ; [CPU_] |135| 
        MOV       *-SP[1],AL            ; [CPU_] |135| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 143,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[2]           ; [CPU_] |143| 
        TBIT      AL,#13                ; [CPU_] |143| 
        BF        $C$L5,NTC             ; [CPU_] |143| 
        ; branchcc occurs ; [] |143| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 145,column 9,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |145| 
        ADDB      AL,#2                 ; [CPU_] |145| 
        MOV       *-SP[5],AL            ; [CPU_] |145| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 146,column 5,is_stmt
        B         $C$L6,UNC             ; [CPU_] |146| 
        ; branch occurs ; [] |146| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 149,column 9,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |149| 
        MOV       *-SP[5],AL            ; [CPU_] |149| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 155,column 5,is_stmt
        MOV       ACC,*-SP[2]           ; [CPU_] |155| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,8                 ; [CPU_] |155| 
        MOVB      AH,#0                 ; [CPU_] |155| 
        ANDB      AL,#0x1f              ; [CPU_] |155| 
        MOVL      *-SP[4],ACC           ; [CPU_] |155| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 160,column 5,is_stmt
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("___eallow")
	.dwattr $C$DW$30, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |160| 
        ; call occurs [#___eallow] ; [] |160| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 163,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,*-SP[5]           ; [CPU_] |163| 
        MOVZ      AR5,*-SP[5]           ; [CPU_] |163| 
        MOV       ACC,*-SP[2]           ; [CPU_] |163| 
        MOVL      P,ACC                 ; [CPU_] |163| 
        MOV       PH,#0                 ; [CPU_] |163| 
        ADD       AR4,#31296            ; [CPU_] |163| 
        MOV       AL,PL                 ; [CPU_] |163| 
        ADD       AR5,#31296            ; [CPU_] |163| 
        ANDB      AL,#0x03              ; [CPU_] |163| 
        MOV       PL,AL                 ; [CPU_] |163| 
        MOVP      T,*-SP[4]             ; [CPU_] 
        LSLL      ACC,T                 ; [CPU_] |163| 
        MOVL      P,ACC                 ; [CPU_] |163| 
        MOVB      ACC,#3                ; [CPU_] |163| 
        LSLL      ACC,T                 ; [CPU_] |163| 
        NOT       ACC                   ; [CPU_] |163| 
        AND       AL,*+XAR4[0]          ; [CPU_] |163| 
        OR        AL,PL                 ; [CPU_] |163| 
        AND       AH,*+XAR4[1]          ; [CPU_] |163| 
        OR        AH,PH                 ; [CPU_] |163| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |163| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 167,column 5,is_stmt
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_name("___edis")
	.dwattr $C$DW$31, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |167| 
        ; call occurs [#___edis] ; [] |167| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 168,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0xa8)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

	.sect	".text"
	.clink
	.global	_XBAR_getInputFlagStatus

$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("XBAR_getInputFlagStatus")
	.dwattr $C$DW$33, DW_AT_low_pc(_XBAR_getInputFlagStatus)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_XBAR_getInputFlagStatus")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$33, DW_AT_TI_begin_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0xb0)
	.dwattr $C$DW$33, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 177,column 1,is_stmt,address _XBAR_getInputFlagStatus

	.dwfde $C$DW$CIE, _XBAR_getInputFlagStatus
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("inputFlag")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_inputFlag")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _XBAR_getInputFlagStatus      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_XBAR_getInputFlagStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("inputFlag")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_inputFlag")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -1]
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -4]
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("inputMask")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_inputMask")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[1],AL            ; [CPU_] |177| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 184,column 5,is_stmt
        B         $C$L11,UNC            ; [CPU_] |184| 
        ; branch occurs ; [] |184| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 187,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |187| 
        MOVL      *-SP[4],ACC           ; [CPU_] |187| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 188,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |188| 
        ; branch occurs ; [] |188| 
$C$L8:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 191,column 13,is_stmt
        MOVB      ACC,#2                ; [CPU_] |191| 
        MOVL      *-SP[4],ACC           ; [CPU_] |191| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 192,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |192| 
        ; branch occurs ; [] |192| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 195,column 13,is_stmt
        MOVB      ACC,#4                ; [CPU_] |195| 
        MOVL      *-SP[4],ACC           ; [CPU_] |195| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 196,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |196| 
        ; branch occurs ; [] |196| 
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 202,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |202| 
        MOVL      *-SP[4],ACC           ; [CPU_] |202| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 203,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |203| 
        ; branch occurs ; [] |203| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 184,column 5,is_stmt
        AND       AL,*-SP[1],#0xff00    ; [CPU_] |184| 
        MOVZ      AR6,AL                ; [CPU_] |184| 
        BF        $C$L7,EQ              ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
        MOVZ      AR7,AR6               ; [CPU_] |184| 
        MOV       ACC,#256              ; [CPU_] |184| 
        CMPL      ACC,XAR7              ; [CPU_] |184| 
        BF        $C$L8,EQ              ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
        MOVZ      AR6,AR6               ; [CPU_] |184| 
        MOV       ACC,#512              ; [CPU_] |184| 
        CMPL      ACC,XAR6              ; [CPU_] |184| 
        BF        $C$L9,EQ              ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
        B         $C$L10,UNC            ; [CPU_] |184| 
        ; branch occurs ; [] |184| 
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 209,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |209| 
        MOVL      XAR6,ACC              ; [CPU_] |209| 
        AND       AL,*-SP[1],#0x00ff    ; [CPU_] |209| 
        MOV       T,AL                  ; [CPU_] |209| 
        MOVL      ACC,XAR6              ; [CPU_] |209| 
        LSLL      ACC,T                 ; [CPU_] |209| 
        MOVL      *-SP[6],ACC           ; [CPU_] |209| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 211,column 5,is_stmt
        MOVB      XAR7,#0               ; [CPU_] |211| 
        MOV       ACC,#31008            ; [CPU_] |211| 
        MOVB      XAR6,#0               ; [CPU_] |211| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |211| 
        MOVL      XAR4,ACC              ; [CPU_] |211| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |211| 
        AND       AL,*-SP[6]            ; [CPU_] |211| 
        AND       AH,*-SP[5]            ; [CPU_] |211| 
        CMPL      ACC,XAR7              ; [CPU_] |211| 
        BF        $C$L13,EQ             ; [CPU_] |211| 
        ; branchcc occurs ; [] |211| 
        MOVB      XAR6,#1               ; [CPU_] |211| 
$C$L13:    
        MOV       AL,AR6                ; [CPU_] |211| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 212,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$33, DW_AT_TI_end_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0xd4)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text"
	.clink
	.global	_XBAR_clearInputFlag

$C$DW$39	.dwtag  DW_TAG_subprogram, DW_AT_name("XBAR_clearInputFlag")
	.dwattr $C$DW$39, DW_AT_low_pc(_XBAR_clearInputFlag)
	.dwattr $C$DW$39, DW_AT_high_pc(0x00)
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_XBAR_clearInputFlag")
	.dwattr $C$DW$39, DW_AT_external
	.dwattr $C$DW$39, DW_AT_TI_begin_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$39, DW_AT_TI_begin_line(0xdc)
	.dwattr $C$DW$39, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$39, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 221,column 1,is_stmt,address _XBAR_clearInputFlag

	.dwfde $C$DW$CIE, _XBAR_clearInputFlag
$C$DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("inputFlag")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_inputFlag")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _XBAR_clearInputFlag          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_XBAR_clearInputFlag:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("inputFlag")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_inputFlag")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -1]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("offset")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -4]
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("inputMask")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_inputMask")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[1],AL            ; [CPU_] |221| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 228,column 5,is_stmt
        B         $C$L18,UNC            ; [CPU_] |228| 
        ; branch occurs ; [] |228| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 231,column 13,is_stmt
        MOVB      ACC,#8                ; [CPU_] |231| 
        MOVL      *-SP[4],ACC           ; [CPU_] |231| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 232,column 13,is_stmt
        B         $C$L19,UNC            ; [CPU_] |232| 
        ; branch occurs ; [] |232| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 235,column 13,is_stmt
        MOVB      ACC,#10               ; [CPU_] |235| 
        MOVL      *-SP[4],ACC           ; [CPU_] |235| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 236,column 13,is_stmt
        B         $C$L19,UNC            ; [CPU_] |236| 
        ; branch occurs ; [] |236| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 239,column 13,is_stmt
        MOVB      ACC,#12               ; [CPU_] |239| 
        MOVL      *-SP[4],ACC           ; [CPU_] |239| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 240,column 13,is_stmt
        B         $C$L19,UNC            ; [CPU_] |240| 
        ; branch occurs ; [] |240| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 246,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |246| 
        MOVL      *-SP[4],ACC           ; [CPU_] |246| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 247,column 13,is_stmt
        B         $C$L19,UNC            ; [CPU_] |247| 
        ; branch occurs ; [] |247| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 228,column 5,is_stmt
        AND       AL,*-SP[1],#0xff00    ; [CPU_] |228| 
        MOVZ      AR6,AL                ; [CPU_] |228| 
        BF        $C$L14,EQ             ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
        MOVZ      AR7,AR6               ; [CPU_] |228| 
        MOV       ACC,#256              ; [CPU_] |228| 
        CMPL      ACC,XAR7              ; [CPU_] |228| 
        BF        $C$L15,EQ             ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
        MOVZ      AR6,AR6               ; [CPU_] |228| 
        MOV       ACC,#512              ; [CPU_] |228| 
        CMPL      ACC,XAR6              ; [CPU_] |228| 
        BF        $C$L16,EQ             ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
        B         $C$L17,UNC            ; [CPU_] |228| 
        ; branch occurs ; [] |228| 
$C$L19:    
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 253,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |253| 
        MOVL      XAR6,ACC              ; [CPU_] |253| 
        AND       AL,*-SP[1],#0x00ff    ; [CPU_] |253| 
        MOV       T,AL                  ; [CPU_] |253| 
        MOVL      ACC,XAR6              ; [CPU_] |253| 
        LSLL      ACC,T                 ; [CPU_] |253| 
        MOVL      *-SP[6],ACC           ; [CPU_] |253| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 254,column 5,is_stmt
        MOVL      XAR6,*-SP[6]          ; [CPU_] |254| 
        MOV       ACC,#31008            ; [CPU_] |254| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |254| 
        MOVL      XAR4,ACC              ; [CPU_] |254| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |254| 
	.dwpsn	file "../ExtraData/driverlib2/xbar.c",line 255,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$39, DW_AT_TI_end_file("../ExtraData/driverlib2/xbar.c")
	.dwattr $C$DW$39, DW_AT_TI_end_line(0xff)
	.dwattr $C$DW$39, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$39

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$45	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT1"), DW_AT_const_value(0x00)
$C$DW$46	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT2"), DW_AT_const_value(0x02)
$C$DW$47	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT3"), DW_AT_const_value(0x04)
$C$DW$48	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT4"), DW_AT_const_value(0x06)
$C$DW$49	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT5"), DW_AT_const_value(0x08)
$C$DW$50	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT6"), DW_AT_const_value(0x0a)
$C$DW$51	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT7"), DW_AT_const_value(0x0c)
$C$DW$52	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUTPUT8"), DW_AT_const_value(0x0e)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_OutputNum")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$53	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX00_CMPSS1_CTRIPOUTH"), DW_AT_const_value(0x00)
$C$DW$54	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX00_CMPSS1_CTRIPOUTH_OR_L"), DW_AT_const_value(0x01)
$C$DW$55	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX00_ADCAEVT1"), DW_AT_const_value(0x02)
$C$DW$56	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX00_ECAP1_OUT"), DW_AT_const_value(0x03)
$C$DW$57	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX01_CMPSS1_CTRIPOUTL"), DW_AT_const_value(0x200)
$C$DW$58	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX01_INPUTXBAR1"), DW_AT_const_value(0x201)
$C$DW$59	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX01_CLB1_OUT4"), DW_AT_const_value(0x202)
$C$DW$60	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX01_ADCCEVT1"), DW_AT_const_value(0x203)
$C$DW$61	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX02_CMPSS2_CTRIPOUTH"), DW_AT_const_value(0x400)
$C$DW$62	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX02_CMPSS2_CTRIPOUTH_OR_L"), DW_AT_const_value(0x401)
$C$DW$63	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX02_ADCAEVT2"), DW_AT_const_value(0x402)
$C$DW$64	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX02_ECAP2_OUT"), DW_AT_const_value(0x403)
$C$DW$65	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX03_CMPSS2_CTRIPOUTL"), DW_AT_const_value(0x600)
$C$DW$66	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX03_INPUTXBAR2"), DW_AT_const_value(0x601)
$C$DW$67	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX03_CLB1_OUT5"), DW_AT_const_value(0x602)
$C$DW$68	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX03_ADCCEVT2"), DW_AT_const_value(0x603)
$C$DW$69	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX04_CMPSS3_CTRIPOUTH"), DW_AT_const_value(0x800)
$C$DW$70	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX04_CMPSS3_CTRIPOUTH_OR_L"), DW_AT_const_value(0x801)
$C$DW$71	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX04_ADCAEVT3"), DW_AT_const_value(0x802)
$C$DW$72	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX04_ECAP3_OUT"), DW_AT_const_value(0x803)
$C$DW$73	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX05_CMPSS3_CTRIPOUTL"), DW_AT_const_value(0xa00)
$C$DW$74	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX05_INPUTXBAR3"), DW_AT_const_value(0xa01)
$C$DW$75	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX05_CLB2_OUT4"), DW_AT_const_value(0xa02)
$C$DW$76	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX05_ADCCEVT3"), DW_AT_const_value(0xa03)
$C$DW$77	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX06_CMPSS4_CTRIPOUTH"), DW_AT_const_value(0xc00)
$C$DW$78	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX06_CMPSS4_CTRIPOUTH_OR_L"), DW_AT_const_value(0xc01)
$C$DW$79	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX06_ADCAEVT4"), DW_AT_const_value(0xc02)
$C$DW$80	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX06_ECAP4_OUT"), DW_AT_const_value(0xc03)
$C$DW$81	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX07_CMPSS4_CTRIPOUTL"), DW_AT_const_value(0xe00)
$C$DW$82	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX07_INPUTXBAR4"), DW_AT_const_value(0xe01)
$C$DW$83	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX07_CLB2_OUT5"), DW_AT_const_value(0xe02)
$C$DW$84	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX07_ADCCEVT4"), DW_AT_const_value(0xe03)
$C$DW$85	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX08_CMPSS5_CTRIPOUTH"), DW_AT_const_value(0x1000)
$C$DW$86	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX08_CMPSS5_CTRIPOUTH_OR_L"), DW_AT_const_value(0x1001)
$C$DW$87	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX08_ADCBEVT1"), DW_AT_const_value(0x1002)
$C$DW$88	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX08_ECAP5_OUT"), DW_AT_const_value(0x1003)
$C$DW$89	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX09_CMPSS5_CTRIPOUTL"), DW_AT_const_value(0x1200)
$C$DW$90	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX09_INPUTXBAR5"), DW_AT_const_value(0x1201)
$C$DW$91	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX09_CLB3_OUT4"), DW_AT_const_value(0x1202)
$C$DW$92	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX09_ADCDEVT1"), DW_AT_const_value(0x1203)
$C$DW$93	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX10_CMPSS6_CTRIPOUTH"), DW_AT_const_value(0x1400)
$C$DW$94	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX10_CMPSS6_CTRIPOUTH_OR_L"), DW_AT_const_value(0x1401)
$C$DW$95	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX10_ADCBEVT2"), DW_AT_const_value(0x1402)
$C$DW$96	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX10_ECAP6_OUT"), DW_AT_const_value(0x1403)
$C$DW$97	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX11_CMPSS6_CTRIPOUTL"), DW_AT_const_value(0x1600)
$C$DW$98	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX11_INPUTXBAR6"), DW_AT_const_value(0x1601)
$C$DW$99	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX11_CLB3_OUT5"), DW_AT_const_value(0x1602)
$C$DW$100	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX11_ADCDEVT2"), DW_AT_const_value(0x1603)
$C$DW$101	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX12_CMPSS7_CTRIPOUTH"), DW_AT_const_value(0x1800)
$C$DW$102	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX12_CMPSS7_CTRIPOUTH_OR_L"), DW_AT_const_value(0x1801)
$C$DW$103	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX12_ADCBEVT3"), DW_AT_const_value(0x1802)
$C$DW$104	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX13_CMPSS7_CTRIPOUTL"), DW_AT_const_value(0x1a00)
$C$DW$105	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX13_ADCSOCA"), DW_AT_const_value(0x1a01)
$C$DW$106	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX13_CLB4_OUT4"), DW_AT_const_value(0x1a02)
$C$DW$107	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX13_ADCDEVT3"), DW_AT_const_value(0x1a03)
$C$DW$108	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX14_CMPSS8_CTRIPOUTH"), DW_AT_const_value(0x1c00)
$C$DW$109	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX14_CMPSS8_CTRIPOUTH_OR_L"), DW_AT_const_value(0x1c01)
$C$DW$110	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX14_ADCBEVT4"), DW_AT_const_value(0x1c02)
$C$DW$111	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX14_EXTSYNCOUT"), DW_AT_const_value(0x1c03)
$C$DW$112	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX15_CMPSS8_CTRIPOUTL"), DW_AT_const_value(0x1e00)
$C$DW$113	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX15_ADCSOCB"), DW_AT_const_value(0x1e01)
$C$DW$114	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX15_CLB4_OUT5"), DW_AT_const_value(0x1e02)
$C$DW$115	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX15_ADCDEVT4"), DW_AT_const_value(0x1e03)
$C$DW$116	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX16_SD1FLT1_COMPH"), DW_AT_const_value(0x2000)
$C$DW$117	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX16_SD1FLT1_COMPH_OR_COMPL"), DW_AT_const_value(0x2001)
$C$DW$118	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX17_SD1FLT1_COMPL"), DW_AT_const_value(0x2200)
$C$DW$119	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX18_SD1FLT2_COMPH"), DW_AT_const_value(0x2400)
$C$DW$120	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX18_SD1FLT2_COMPH_OR_COMPL"), DW_AT_const_value(0x2401)
$C$DW$121	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX19_SD1FLT2_COMPL"), DW_AT_const_value(0x2600)
$C$DW$122	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX20_SD1FLT3_COMPH"), DW_AT_const_value(0x2800)
$C$DW$123	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX20_SD1FLT3_COMPH_OR_COMPL"), DW_AT_const_value(0x2801)
$C$DW$124	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX21_SD1FLT3_COMPL"), DW_AT_const_value(0x2a00)
$C$DW$125	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX22_SD1FLT4_COMPH"), DW_AT_const_value(0x2c00)
$C$DW$126	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX22_SD1FLT4_COMPH_OR_COMPL"), DW_AT_const_value(0x2c01)
$C$DW$127	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX23_SD1FLT4_COMPL"), DW_AT_const_value(0x2e00)
$C$DW$128	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX24_SD2FLT1_COMPH"), DW_AT_const_value(0x3000)
$C$DW$129	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX24_SD2FLT1_COMPH_OR_COMPL"), DW_AT_const_value(0x3001)
$C$DW$130	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX25_SD2FLT1_COMPL"), DW_AT_const_value(0x3200)
$C$DW$131	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX26_SD2FLT2_COMPH"), DW_AT_const_value(0x3400)
$C$DW$132	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX26_SD2FLT2_COMPH_OR_COMPL"), DW_AT_const_value(0x3401)
$C$DW$133	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX27_SD2FLT2_COMPL"), DW_AT_const_value(0x3600)
$C$DW$134	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX28_SD2FLT3_COMPH"), DW_AT_const_value(0x3800)
$C$DW$135	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX28_SD2FLT3_COMPH_OR_COMPL"), DW_AT_const_value(0x3801)
$C$DW$136	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX29_SD2FLT3_COMPL"), DW_AT_const_value(0x3a00)
$C$DW$137	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX30_SD2FLT4_COMPH"), DW_AT_const_value(0x3c00)
$C$DW$138	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX30_SD2FLT4_COMPH_OR_COMPL"), DW_AT_const_value(0x3c01)
$C$DW$139	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_OUT_MUX31_SD2FLT4_COMPL"), DW_AT_const_value(0x3e00)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_OutputMuxConfig")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$140	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP4"), DW_AT_const_value(0x00)
$C$DW$141	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP5"), DW_AT_const_value(0x02)
$C$DW$142	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP7"), DW_AT_const_value(0x04)
$C$DW$143	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP8"), DW_AT_const_value(0x06)
$C$DW$144	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP9"), DW_AT_const_value(0x08)
$C$DW$145	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP10"), DW_AT_const_value(0x0a)
$C$DW$146	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP11"), DW_AT_const_value(0x0c)
$C$DW$147	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_TRIP12"), DW_AT_const_value(0x0e)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_TripNum")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)

$C$DW$T$25	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$148	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX00_CMPSS1_CTRIPH"), DW_AT_const_value(0x00)
$C$DW$149	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX00_CMPSS1_CTRIPH_OR_L"), DW_AT_const_value(0x01)
$C$DW$150	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX00_ADCAEVT1"), DW_AT_const_value(0x02)
$C$DW$151	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX00_ECAP1_OUT"), DW_AT_const_value(0x03)
$C$DW$152	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX01_CMPSS1_CTRIPL"), DW_AT_const_value(0x200)
$C$DW$153	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX01_INPUTXBAR1"), DW_AT_const_value(0x201)
$C$DW$154	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX01_CLB1_OUT4"), DW_AT_const_value(0x202)
$C$DW$155	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX01_ADCCEVT1"), DW_AT_const_value(0x203)
$C$DW$156	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX02_CMPSS2_CTRIPH"), DW_AT_const_value(0x400)
$C$DW$157	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX02_CMPSS2_CTRIPH_OR_L"), DW_AT_const_value(0x401)
$C$DW$158	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX02_ADCAEVT2"), DW_AT_const_value(0x402)
$C$DW$159	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX02_ECAP2_OUT"), DW_AT_const_value(0x403)
$C$DW$160	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX03_CMPSS2_CTRIPL"), DW_AT_const_value(0x600)
$C$DW$161	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX03_INPUTXBAR2"), DW_AT_const_value(0x601)
$C$DW$162	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX03_CLB1_OUT5"), DW_AT_const_value(0x602)
$C$DW$163	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX03_ADCCEVT2"), DW_AT_const_value(0x603)
$C$DW$164	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX04_CMPSS3_CTRIPH"), DW_AT_const_value(0x800)
$C$DW$165	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX04_CMPSS3_CTRIPH_OR_L"), DW_AT_const_value(0x801)
$C$DW$166	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX04_ADCAEVT3"), DW_AT_const_value(0x802)
$C$DW$167	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX04_ECAP3_OUT"), DW_AT_const_value(0x803)
$C$DW$168	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX05_CMPSS3_CTRIPL"), DW_AT_const_value(0xa00)
$C$DW$169	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX05_INPUTXBAR3"), DW_AT_const_value(0xa01)
$C$DW$170	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX05_CLB2_OUT4"), DW_AT_const_value(0xa02)
$C$DW$171	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX05_ADCCEVT3"), DW_AT_const_value(0xa03)
$C$DW$172	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX06_CMPSS4_CTRIPH"), DW_AT_const_value(0xc00)
$C$DW$173	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX06_CMPSS4_CTRIPH_OR_L"), DW_AT_const_value(0xc01)
$C$DW$174	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX06_ADCAEVT4"), DW_AT_const_value(0xc02)
$C$DW$175	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX06_ECAP4_OUT"), DW_AT_const_value(0xc03)
$C$DW$176	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX07_CMPSS4_CTRIPL"), DW_AT_const_value(0xe00)
$C$DW$177	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX07_INPUTXBAR4"), DW_AT_const_value(0xe01)
$C$DW$178	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX07_CLB2_OUT5"), DW_AT_const_value(0xe02)
$C$DW$179	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX07_ADCCEVT4"), DW_AT_const_value(0xe03)
$C$DW$180	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX08_CMPSS5_CTRIPH"), DW_AT_const_value(0x1000)
$C$DW$181	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX08_CMPSS5_CTRIPH_OR_L"), DW_AT_const_value(0x1001)
$C$DW$182	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX08_ADCBEVT1"), DW_AT_const_value(0x1002)
$C$DW$183	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX08_ECAP5_OUT"), DW_AT_const_value(0x1003)
$C$DW$184	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX09_CMPSS5_CTRIPL"), DW_AT_const_value(0x1200)
$C$DW$185	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX09_INPUTXBAR5"), DW_AT_const_value(0x1201)
$C$DW$186	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX09_CLB3_OUT4"), DW_AT_const_value(0x1202)
$C$DW$187	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX09_ADCDEVT1"), DW_AT_const_value(0x1203)
$C$DW$188	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX10_CMPSS6_CTRIPH"), DW_AT_const_value(0x1400)
$C$DW$189	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX10_CMPSS6_CTRIPH_OR_L"), DW_AT_const_value(0x1401)
$C$DW$190	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX10_ADCBEVT2"), DW_AT_const_value(0x1402)
$C$DW$191	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX10_ECAP6_OUT"), DW_AT_const_value(0x1403)
$C$DW$192	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX11_CMPSS6_CTRIPL"), DW_AT_const_value(0x1600)
$C$DW$193	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX11_INPUTXBAR6"), DW_AT_const_value(0x1601)
$C$DW$194	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX11_CLB3_OUT5"), DW_AT_const_value(0x1602)
$C$DW$195	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX11_ADCDEVT2"), DW_AT_const_value(0x1603)
$C$DW$196	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX12_CMPSS7_CTRIPH"), DW_AT_const_value(0x1800)
$C$DW$197	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX12_CMPSS7_CTRIPH_OR_L"), DW_AT_const_value(0x1801)
$C$DW$198	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX12_ADCBEVT3"), DW_AT_const_value(0x1802)
$C$DW$199	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX13_CMPSS7_CTRIPL"), DW_AT_const_value(0x1a00)
$C$DW$200	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX13_ADCSOCA"), DW_AT_const_value(0x1a01)
$C$DW$201	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX13_CLB4_OUT4"), DW_AT_const_value(0x1a02)
$C$DW$202	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX13_ADCDEVT3"), DW_AT_const_value(0x1a03)
$C$DW$203	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX14_CMPSS8_CTRIPH"), DW_AT_const_value(0x1c00)
$C$DW$204	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX14_CMPSS8_CTRIPH_OR_L"), DW_AT_const_value(0x1c01)
$C$DW$205	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX14_ADCBEVT4"), DW_AT_const_value(0x1c02)
$C$DW$206	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX14_EXTSYNCOUT"), DW_AT_const_value(0x1c03)
$C$DW$207	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX15_CMPSS8_CTRIPL"), DW_AT_const_value(0x1e00)
$C$DW$208	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX15_ADCSOCB"), DW_AT_const_value(0x1e01)
$C$DW$209	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX15_CLB4_OUT5"), DW_AT_const_value(0x1e02)
$C$DW$210	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX15_ADCDEVT4"), DW_AT_const_value(0x1e03)
$C$DW$211	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX16_SD1FLT1_COMPH"), DW_AT_const_value(0x2000)
$C$DW$212	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX16_SD1FLT1_COMPH_OR_COMPL"), DW_AT_const_value(0x2001)
$C$DW$213	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX17_SD1FLT1_COMPL"), DW_AT_const_value(0x2200)
$C$DW$214	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX18_SD1FLT2_COMPH"), DW_AT_const_value(0x2400)
$C$DW$215	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX18_SD1FLT2_COMPH_OR_COMPL"), DW_AT_const_value(0x2401)
$C$DW$216	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX19_SD1FLT2_COMPL"), DW_AT_const_value(0x2600)
$C$DW$217	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX20_SD1FLT3_COMPH"), DW_AT_const_value(0x2800)
$C$DW$218	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX20_SD1FLT3_COMPH_OR_COMPL"), DW_AT_const_value(0x2801)
$C$DW$219	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX21_SD1FLT3_COMPL"), DW_AT_const_value(0x2a00)
$C$DW$220	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX22_SD1FLT4_COMPH"), DW_AT_const_value(0x2c00)
$C$DW$221	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX22_SD1FLT4_COMPH_OR_COMPL"), DW_AT_const_value(0x2c01)
$C$DW$222	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX23_SD1FLT4_COMPL"), DW_AT_const_value(0x2e00)
$C$DW$223	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX24_SD2FLT1_COMPH"), DW_AT_const_value(0x3000)
$C$DW$224	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX24_SD2FLT1_COMPH_OR_COMPL"), DW_AT_const_value(0x3001)
$C$DW$225	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX25_SD2FLT1_COMPL"), DW_AT_const_value(0x3200)
$C$DW$226	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX26_SD2FLT2_COMPH"), DW_AT_const_value(0x3400)
$C$DW$227	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX26_SD2FLT2_COMPH_OR_COMPL"), DW_AT_const_value(0x3401)
$C$DW$228	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX27_SD2FLT2_COMPL"), DW_AT_const_value(0x3600)
$C$DW$229	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX28_SD2FLT3_COMPH"), DW_AT_const_value(0x3800)
$C$DW$230	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX28_SD2FLT3_COMPH_OR_COMPL"), DW_AT_const_value(0x3801)
$C$DW$231	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX29_SD2FLT3_COMPL"), DW_AT_const_value(0x3a00)
$C$DW$232	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX30_SD2FLT4_COMPH"), DW_AT_const_value(0x3c00)
$C$DW$233	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX30_SD2FLT4_COMPH_OR_COMPL"), DW_AT_const_value(0x3c01)
$C$DW$234	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_EPWM_MUX31_SD2FLT4_COMPL"), DW_AT_const_value(0x3e00)
	.dwendtag $C$DW$T$25

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_EPWMMuxConfig")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$235	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG0"), DW_AT_const_value(0x00)
$C$DW$236	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG1"), DW_AT_const_value(0x02)
$C$DW$237	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG2"), DW_AT_const_value(0x04)
$C$DW$238	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG3"), DW_AT_const_value(0x06)
$C$DW$239	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG4"), DW_AT_const_value(0x08)
$C$DW$240	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG5"), DW_AT_const_value(0x0a)
$C$DW$241	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG6"), DW_AT_const_value(0x0c)
$C$DW$242	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_AUXSIG7"), DW_AT_const_value(0x0e)
	.dwendtag $C$DW$T$27

$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_AuxSigNum")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$T$29	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$243	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX00_CMPSS1_CTRIPH"), DW_AT_const_value(0x00)
$C$DW$244	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX00_CMPSS1_CTRIPH_OR_L"), DW_AT_const_value(0x01)
$C$DW$245	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX00_ADCAEVT1"), DW_AT_const_value(0x02)
$C$DW$246	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX00_ECAP1_OUT"), DW_AT_const_value(0x03)
$C$DW$247	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX01_CMPSS1_CTRIPL"), DW_AT_const_value(0x200)
$C$DW$248	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX01_INPUTXBAR1"), DW_AT_const_value(0x201)
$C$DW$249	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX01_CLB1_OUT4"), DW_AT_const_value(0x202)
$C$DW$250	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX01_ADCCEVT1"), DW_AT_const_value(0x203)
$C$DW$251	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX02_CMPSS2_CTRIPH"), DW_AT_const_value(0x400)
$C$DW$252	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX02_CMPSS2_CTRIPH_OR_L"), DW_AT_const_value(0x401)
$C$DW$253	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX02_ADCAEVT2"), DW_AT_const_value(0x402)
$C$DW$254	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX02_ECAP2_OUT"), DW_AT_const_value(0x403)
$C$DW$255	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX03_CMPSS2_CTRIPL"), DW_AT_const_value(0x600)
$C$DW$256	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX03_INPUTXBAR2"), DW_AT_const_value(0x601)
$C$DW$257	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX03_CLB1_OUT5"), DW_AT_const_value(0x602)
$C$DW$258	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX03_ADCCEVT2"), DW_AT_const_value(0x603)
$C$DW$259	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX04_CMPSS3_CTRIPH"), DW_AT_const_value(0x800)
$C$DW$260	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX04_CMPSS3_CTRIPH_OR_L"), DW_AT_const_value(0x801)
$C$DW$261	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX04_ADCAEVT3"), DW_AT_const_value(0x802)
$C$DW$262	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX04_ECAP3_OUT"), DW_AT_const_value(0x803)
$C$DW$263	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX05_CMPSS3_CTRIPL"), DW_AT_const_value(0xa00)
$C$DW$264	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX05_INPUTXBAR3"), DW_AT_const_value(0xa01)
$C$DW$265	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX05_CLB2_OUT4"), DW_AT_const_value(0xa02)
$C$DW$266	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX05_ADCCEVT3"), DW_AT_const_value(0xa03)
$C$DW$267	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX06_CMPSS4_CTRIPH"), DW_AT_const_value(0xc00)
$C$DW$268	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX06_CMPSS4_CTRIPH_OR_L"), DW_AT_const_value(0xc01)
$C$DW$269	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX06_ADCAEVT4"), DW_AT_const_value(0xc02)
$C$DW$270	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX06_ECAP4_OUT"), DW_AT_const_value(0xc03)
$C$DW$271	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX07_CMPSS4_CTRIPL"), DW_AT_const_value(0xe00)
$C$DW$272	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX07_INPUTXBAR4"), DW_AT_const_value(0xe01)
$C$DW$273	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX07_CLB2_OUT5"), DW_AT_const_value(0xe02)
$C$DW$274	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX07_ADCCEVT4"), DW_AT_const_value(0xe03)
$C$DW$275	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX08_CMPSS5_CTRIPH"), DW_AT_const_value(0x1000)
$C$DW$276	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX08_CMPSS5_CTRIPH_OR_L"), DW_AT_const_value(0x1001)
$C$DW$277	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX08_ADCBEVT1"), DW_AT_const_value(0x1002)
$C$DW$278	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX08_ECAP5_OUT"), DW_AT_const_value(0x1003)
$C$DW$279	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX09_CMPSS5_CTRIPL"), DW_AT_const_value(0x1200)
$C$DW$280	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX09_INPUTXBAR5"), DW_AT_const_value(0x1201)
$C$DW$281	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX09_CLB3_OUT4"), DW_AT_const_value(0x1202)
$C$DW$282	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX09_ADCDEVT1"), DW_AT_const_value(0x1203)
$C$DW$283	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX10_CMPSS6_CTRIPH"), DW_AT_const_value(0x1400)
$C$DW$284	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX10_CMPSS6_CTRIPH_OR_L"), DW_AT_const_value(0x1401)
$C$DW$285	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX10_ADCBEVT2"), DW_AT_const_value(0x1402)
$C$DW$286	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX10_ECAP6_OUT"), DW_AT_const_value(0x1403)
$C$DW$287	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX11_CMPSS6_CTRIPL"), DW_AT_const_value(0x1600)
$C$DW$288	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX11_INPUTXBAR6"), DW_AT_const_value(0x1601)
$C$DW$289	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX11_CLB3_OUT5"), DW_AT_const_value(0x1602)
$C$DW$290	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX11_ADCDEVT2"), DW_AT_const_value(0x1603)
$C$DW$291	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX12_CMPSS7_CTRIPH"), DW_AT_const_value(0x1800)
$C$DW$292	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX12_CMPSS7_CTRIPH_OR_L"), DW_AT_const_value(0x1801)
$C$DW$293	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX12_ADCBEVT3"), DW_AT_const_value(0x1802)
$C$DW$294	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX13_CMPSS7_CTRIPL"), DW_AT_const_value(0x1a00)
$C$DW$295	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX13_ADCSOCA"), DW_AT_const_value(0x1a01)
$C$DW$296	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX13_CLB4_OUT4"), DW_AT_const_value(0x1a02)
$C$DW$297	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX13_ADCDEVT3"), DW_AT_const_value(0x1a03)
$C$DW$298	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX14_CMPSS8_CTRIPH"), DW_AT_const_value(0x1c00)
$C$DW$299	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX14_CMPSS8_CTRIPH_OR_L"), DW_AT_const_value(0x1c01)
$C$DW$300	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX14_ADCBEVT4"), DW_AT_const_value(0x1c02)
$C$DW$301	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX14_EXTSYNCOUT"), DW_AT_const_value(0x1c03)
$C$DW$302	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX15_CMPSS8_CTRIPL"), DW_AT_const_value(0x1e00)
$C$DW$303	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX15_ADCSOCB"), DW_AT_const_value(0x1e01)
$C$DW$304	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX15_CLB4_OUT5"), DW_AT_const_value(0x1e02)
$C$DW$305	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX15_ADCDEVT4"), DW_AT_const_value(0x1e03)
$C$DW$306	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX16_SD1FLT1_COMPH"), DW_AT_const_value(0x2000)
$C$DW$307	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX16_SD1FLT1_COMPH_OR_COMPL"), DW_AT_const_value(0x2001)
$C$DW$308	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX17_SD1FLT1_COMPL"), DW_AT_const_value(0x2200)
$C$DW$309	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX18_SD1FLT2_COMPH"), DW_AT_const_value(0x2400)
$C$DW$310	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX18_SD1FLT2_COMPH_OR_COMPL"), DW_AT_const_value(0x2401)
$C$DW$311	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX19_SD1FLT2_COMPL"), DW_AT_const_value(0x2600)
$C$DW$312	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX20_SD1FLT3_COMPH"), DW_AT_const_value(0x2800)
$C$DW$313	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX20_SD1FLT3_COMPH_OR_COMPL"), DW_AT_const_value(0x2801)
$C$DW$314	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX21_SD1FLT3_COMPL"), DW_AT_const_value(0x2a00)
$C$DW$315	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX22_SD1FLT4_COMPH"), DW_AT_const_value(0x2c00)
$C$DW$316	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX22_SD1FLT4_COMPH_OR_COMPL"), DW_AT_const_value(0x2c01)
$C$DW$317	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX23_SD1FLT4_COMPL"), DW_AT_const_value(0x2e00)
$C$DW$318	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX24_SD2FLT1_COMPH"), DW_AT_const_value(0x3000)
$C$DW$319	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX24_SD2FLT1_COMPH_OR_COMPL"), DW_AT_const_value(0x3001)
$C$DW$320	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX25_SD2FLT1_COMPL"), DW_AT_const_value(0x3200)
$C$DW$321	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX26_SD2FLT2_COMPH"), DW_AT_const_value(0x3400)
$C$DW$322	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX26_SD2FLT2_COMPH_OR_COMPL"), DW_AT_const_value(0x3401)
$C$DW$323	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX27_SD2FLT2_COMPL"), DW_AT_const_value(0x3600)
$C$DW$324	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX28_SD2FLT3_COMPH"), DW_AT_const_value(0x3800)
$C$DW$325	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX28_SD2FLT3_COMPH_OR_COMPL"), DW_AT_const_value(0x3801)
$C$DW$326	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX29_SD2FLT3_COMPL"), DW_AT_const_value(0x3a00)
$C$DW$327	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX30_SD2FLT4_COMPH"), DW_AT_const_value(0x3c00)
$C$DW$328	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX30_SD2FLT4_COMPH_OR_COMPL"), DW_AT_const_value(0x3c01)
$C$DW$329	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_CLB_MUX31_SD2FLT4_COMPL"), DW_AT_const_value(0x3e00)
	.dwendtag $C$DW$T$29

$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_CLBMuxConfig")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$31	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$330	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS1_CTRIPL"), DW_AT_const_value(0x00)
$C$DW$331	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS1_CTRIPH"), DW_AT_const_value(0x01)
$C$DW$332	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS2_CTRIPL"), DW_AT_const_value(0x02)
$C$DW$333	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS2_CTRIPH"), DW_AT_const_value(0x03)
$C$DW$334	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS3_CTRIPL"), DW_AT_const_value(0x04)
$C$DW$335	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS3_CTRIPH"), DW_AT_const_value(0x05)
$C$DW$336	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS4_CTRIPL"), DW_AT_const_value(0x06)
$C$DW$337	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS4_CTRIPH"), DW_AT_const_value(0x07)
$C$DW$338	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS5_CTRIPL"), DW_AT_const_value(0x08)
$C$DW$339	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS5_CTRIPH"), DW_AT_const_value(0x09)
$C$DW$340	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS6_CTRIPL"), DW_AT_const_value(0x0a)
$C$DW$341	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS6_CTRIPH"), DW_AT_const_value(0x0b)
$C$DW$342	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS7_CTRIPL"), DW_AT_const_value(0x0c)
$C$DW$343	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS7_CTRIPH"), DW_AT_const_value(0x0d)
$C$DW$344	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS8_CTRIPL"), DW_AT_const_value(0x0e)
$C$DW$345	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS8_CTRIPH"), DW_AT_const_value(0x0f)
$C$DW$346	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS1_CTRIPOUTL"), DW_AT_const_value(0x10)
$C$DW$347	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS1_CTRIPOUTH"), DW_AT_const_value(0x11)
$C$DW$348	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS2_CTRIPOUTL"), DW_AT_const_value(0x12)
$C$DW$349	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS2_CTRIPOUTH"), DW_AT_const_value(0x13)
$C$DW$350	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS3_CTRIPOUTL"), DW_AT_const_value(0x14)
$C$DW$351	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS3_CTRIPOUTH"), DW_AT_const_value(0x15)
$C$DW$352	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS4_CTRIPOUTL"), DW_AT_const_value(0x16)
$C$DW$353	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS4_CTRIPOUTH"), DW_AT_const_value(0x17)
$C$DW$354	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS5_CTRIPOUTL"), DW_AT_const_value(0x18)
$C$DW$355	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS5_CTRIPOUTH"), DW_AT_const_value(0x19)
$C$DW$356	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS6_CTRIPOUTL"), DW_AT_const_value(0x1a)
$C$DW$357	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS6_CTRIPOUTH"), DW_AT_const_value(0x1b)
$C$DW$358	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS7_CTRIPOUTL"), DW_AT_const_value(0x1c)
$C$DW$359	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS7_CTRIPOUTH"), DW_AT_const_value(0x1d)
$C$DW$360	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS8_CTRIPOUTL"), DW_AT_const_value(0x1e)
$C$DW$361	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_CMPSS8_CTRIPOUTH"), DW_AT_const_value(0x1f)
$C$DW$362	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_INPUT1"), DW_AT_const_value(0x100)
$C$DW$363	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_INPUT2"), DW_AT_const_value(0x101)
$C$DW$364	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_INPUT3"), DW_AT_const_value(0x102)
$C$DW$365	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_INPUT4"), DW_AT_const_value(0x103)
$C$DW$366	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_INPUT5"), DW_AT_const_value(0x104)
$C$DW$367	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_INPUT6"), DW_AT_const_value(0x105)
$C$DW$368	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCSOCA"), DW_AT_const_value(0x106)
$C$DW$369	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCSOCB"), DW_AT_const_value(0x107)
$C$DW$370	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ECAP1_OUT"), DW_AT_const_value(0x110)
$C$DW$371	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ECAP2_OUT"), DW_AT_const_value(0x111)
$C$DW$372	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ECAP3_OUT"), DW_AT_const_value(0x112)
$C$DW$373	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ECAP4_OUT"), DW_AT_const_value(0x113)
$C$DW$374	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ECAP5_OUT"), DW_AT_const_value(0x114)
$C$DW$375	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ECAP6_OUT"), DW_AT_const_value(0x115)
$C$DW$376	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_EXTSYNCOUT"), DW_AT_const_value(0x116)
$C$DW$377	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCAEVT1"), DW_AT_const_value(0x117)
$C$DW$378	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCAEVT2"), DW_AT_const_value(0x118)
$C$DW$379	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCAEVT3"), DW_AT_const_value(0x119)
$C$DW$380	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCAEVT4"), DW_AT_const_value(0x11a)
$C$DW$381	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCBEVT1"), DW_AT_const_value(0x11b)
$C$DW$382	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCBEVT2"), DW_AT_const_value(0x11c)
$C$DW$383	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCBEVT3"), DW_AT_const_value(0x11d)
$C$DW$384	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCBEVT4"), DW_AT_const_value(0x11e)
$C$DW$385	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCCEVT1"), DW_AT_const_value(0x11f)
$C$DW$386	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCCEVT2"), DW_AT_const_value(0x200)
$C$DW$387	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCCEVT3"), DW_AT_const_value(0x201)
$C$DW$388	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCCEVT4"), DW_AT_const_value(0x202)
$C$DW$389	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCDEVT1"), DW_AT_const_value(0x203)
$C$DW$390	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCDEVT2"), DW_AT_const_value(0x204)
$C$DW$391	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCDEVT3"), DW_AT_const_value(0x205)
$C$DW$392	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_ADCDEVT4"), DW_AT_const_value(0x206)
$C$DW$393	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT1_COMPL"), DW_AT_const_value(0x207)
$C$DW$394	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT1_COMPH"), DW_AT_const_value(0x208)
$C$DW$395	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT2_COMPL"), DW_AT_const_value(0x209)
$C$DW$396	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT2_COMPH"), DW_AT_const_value(0x20a)
$C$DW$397	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT3_COMPL"), DW_AT_const_value(0x20b)
$C$DW$398	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT3_COMPH"), DW_AT_const_value(0x20c)
$C$DW$399	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT4_COMPL"), DW_AT_const_value(0x20d)
$C$DW$400	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD1FLT4_COMPH"), DW_AT_const_value(0x20e)
$C$DW$401	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT1_COMPL"), DW_AT_const_value(0x20f)
$C$DW$402	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT1_COMPH"), DW_AT_const_value(0x210)
$C$DW$403	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT2_COMPL"), DW_AT_const_value(0x211)
$C$DW$404	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT2_COMPH"), DW_AT_const_value(0x212)
$C$DW$405	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT3_COMPL"), DW_AT_const_value(0x213)
$C$DW$406	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT3_COMPH"), DW_AT_const_value(0x214)
$C$DW$407	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT4_COMPL"), DW_AT_const_value(0x215)
$C$DW$408	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT_FLG_SD2FLT4_COMPH"), DW_AT_const_value(0x216)
	.dwendtag $C$DW$T$31

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_InputFlag")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("uintptr_t")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$409	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$409, DW_AT_location[DW_OP_reg0]
$C$DW$410	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$410, DW_AT_location[DW_OP_reg1]
$C$DW$411	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$411, DW_AT_location[DW_OP_reg2]
$C$DW$412	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$412, DW_AT_location[DW_OP_reg3]
$C$DW$413	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$413, DW_AT_location[DW_OP_reg20]
$C$DW$414	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$414, DW_AT_location[DW_OP_reg21]
$C$DW$415	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$415, DW_AT_location[DW_OP_reg22]
$C$DW$416	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$416, DW_AT_location[DW_OP_reg23]
$C$DW$417	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$417, DW_AT_location[DW_OP_reg24]
$C$DW$418	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$418, DW_AT_location[DW_OP_reg25]
$C$DW$419	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$419, DW_AT_location[DW_OP_reg26]
$C$DW$420	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$420, DW_AT_location[DW_OP_reg28]
$C$DW$421	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg29]
$C$DW$422	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg30]
$C$DW$423	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg31]
$C$DW$424	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$424, DW_AT_location[DW_OP_regx 0x20]
$C$DW$425	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$425, DW_AT_location[DW_OP_regx 0x21]
$C$DW$426	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$426, DW_AT_location[DW_OP_regx 0x22]
$C$DW$427	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$427, DW_AT_location[DW_OP_regx 0x23]
$C$DW$428	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$428, DW_AT_location[DW_OP_regx 0x24]
$C$DW$429	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$429, DW_AT_location[DW_OP_regx 0x25]
$C$DW$430	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$430, DW_AT_location[DW_OP_regx 0x26]
$C$DW$431	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$431, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$432	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$432, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$433	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$433, DW_AT_location[DW_OP_reg4]
$C$DW$434	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$434, DW_AT_location[DW_OP_reg6]
$C$DW$435	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$435, DW_AT_location[DW_OP_reg8]
$C$DW$436	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$436, DW_AT_location[DW_OP_reg10]
$C$DW$437	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$437, DW_AT_location[DW_OP_reg12]
$C$DW$438	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$438, DW_AT_location[DW_OP_reg14]
$C$DW$439	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$439, DW_AT_location[DW_OP_reg16]
$C$DW$440	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$440, DW_AT_location[DW_OP_reg17]
$C$DW$441	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg18]
$C$DW$442	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$442, DW_AT_location[DW_OP_reg19]
$C$DW$443	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$443, DW_AT_location[DW_OP_reg5]
$C$DW$444	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$444, DW_AT_location[DW_OP_reg7]
$C$DW$445	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$445, DW_AT_location[DW_OP_reg9]
$C$DW$446	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$446, DW_AT_location[DW_OP_reg11]
$C$DW$447	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$447, DW_AT_location[DW_OP_reg13]
$C$DW$448	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$448, DW_AT_location[DW_OP_reg15]
$C$DW$449	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$449, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$450	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$450, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$451	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$451, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$452	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$452, DW_AT_location[DW_OP_regx 0x30]
$C$DW$453	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$453, DW_AT_location[DW_OP_regx 0x33]
$C$DW$454	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$454, DW_AT_location[DW_OP_regx 0x34]
$C$DW$455	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$455, DW_AT_location[DW_OP_regx 0x37]
$C$DW$456	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$456, DW_AT_location[DW_OP_regx 0x38]
$C$DW$457	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$457, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$458	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$458, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$459	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$459, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$460	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$460, DW_AT_location[DW_OP_regx 0x40]
$C$DW$461	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$461, DW_AT_location[DW_OP_regx 0x43]
$C$DW$462	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$462, DW_AT_location[DW_OP_regx 0x44]
$C$DW$463	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$463, DW_AT_location[DW_OP_regx 0x47]
$C$DW$464	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$464, DW_AT_location[DW_OP_regx 0x48]
$C$DW$465	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$465, DW_AT_location[DW_OP_regx 0x49]
$C$DW$466	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$466, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$467	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$467, DW_AT_location[DW_OP_regx 0x27]
$C$DW$468	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$468, DW_AT_location[DW_OP_regx 0x28]
$C$DW$469	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$469, DW_AT_location[DW_OP_reg27]
$C$DW$470	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$470, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

