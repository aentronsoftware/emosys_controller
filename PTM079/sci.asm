;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2124812 
	.sect	".text"
	.clink

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_enableModule")
	.dwattr $C$DW$1, DW_AT_low_pc(_SCI_enableModule)
	.dwattr $C$DW$1, DW_AT_high_pc(0x00)
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_SCI_enableModule")
	.dwattr $C$DW$1, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$1, DW_AT_TI_begin_line(0x1d3)
	.dwattr $C$DW$1, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$1, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 468,column 1,is_stmt,address _SCI_enableModule

	.dwfde $C$DW$CIE, _SCI_enableModule
$C$DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$2, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_enableModule             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_enableModule:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$3, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |468| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 477,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |477| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |477| 
        MOVL      XAR4,ACC              ; [CPU_] |477| 
        OR        *+XAR4[0],#0x0023     ; [CPU_] |477| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 479,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$4	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$4, DW_AT_low_pc(0x00)
	.dwattr $C$DW$4, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$1, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$1, DW_AT_TI_end_line(0x1df)
	.dwattr $C$DW$1, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$1

	.sect	".text"
	.clink

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_disableModule")
	.dwattr $C$DW$5, DW_AT_low_pc(_SCI_disableModule)
	.dwattr $C$DW$5, DW_AT_high_pc(0x00)
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_SCI_disableModule")
	.dwattr $C$DW$5, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$5, DW_AT_TI_begin_line(0x1ee)
	.dwattr $C$DW$5, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$5, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 495,column 1,is_stmt,address _SCI_disableModule

	.dwfde $C$DW$CIE, _SCI_disableModule
$C$DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_disableModule            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_disableModule:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |495| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 504,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |504| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |504| 
        MOVL      XAR4,ACC              ; [CPU_] |504| 
        AND       *+XAR4[0],#0xbfff     ; [CPU_] |504| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 509,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |509| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |509| 
        MOVL      XAR4,ACC              ; [CPU_] |509| 
        AND       *+XAR4[0],#0xfffc     ; [CPU_] |509| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 510,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$5, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$5, DW_AT_TI_end_line(0x1fe)
	.dwattr $C$DW$5, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$5

	.sect	".text"
	.clink

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_isFIFOEnabled")
	.dwattr $C$DW$9, DW_AT_low_pc(_SCI_isFIFOEnabled)
	.dwattr $C$DW$9, DW_AT_high_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_SCI_isFIFOEnabled")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$9, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$9, DW_AT_TI_begin_line(0x242)
	.dwattr $C$DW$9, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$9, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 579,column 1,is_stmt,address _SCI_isFIFOEnabled

	.dwfde $C$DW$CIE, _SCI_isFIFOEnabled
$C$DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_isFIFOEnabled            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_isFIFOEnabled:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |579| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 588,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |588| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |588| 
        MOVL      XAR5,ACC              ; [CPU_] |588| 
        AND       AL,*+XAR5[0],#0x4000  ; [CPU_] |588| 
        MOVZ      AR6,AL                ; [CPU_] |588| 
        MOV       ACC,#16384            ; [CPU_] |588| 
        CMPL      ACC,XAR6              ; [CPU_] |588| 
        BF        $C$L1,NEQ             ; [CPU_] |588| 
        ; branchcc occurs ; [] |588| 
        MOVB      AL,#1                 ; [CPU_] |588| 
        B         $C$L2,UNC             ; [CPU_] |588| 
        ; branch occurs ; [] |588| 
$C$L1:    
        MOVB      AL,#0                 ; [CPU_] |588| 
$C$L2:    
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 590,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$12	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$12, DW_AT_low_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$9, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$9, DW_AT_TI_end_line(0x24e)
	.dwattr $C$DW$9, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$9

	.sect	".text"
	.clink

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_isDataAvailableNonFIFO")
	.dwattr $C$DW$13, DW_AT_low_pc(_SCI_isDataAvailableNonFIFO)
	.dwattr $C$DW$13, DW_AT_high_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_SCI_isDataAvailableNonFIFO")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$13, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$13, DW_AT_TI_begin_line(0x2ad)
	.dwattr $C$DW$13, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$13, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 686,column 1,is_stmt,address _SCI_isDataAvailableNonFIFO

	.dwfde $C$DW$CIE, _SCI_isDataAvailableNonFIFO
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_isDataAvailableNonFIFO   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_isDataAvailableNonFIFO:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |686| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 695,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |695| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |695| 
        MOVL      XAR4,ACC              ; [CPU_] |695| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |695| 
        ANDB      AL,#0x40              ; [CPU_] |695| 
        CMPB      AL,#64                ; [CPU_] |695| 
        BF        $C$L3,NEQ             ; [CPU_] |695| 
        ; branchcc occurs ; [] |695| 
        MOVB      AL,#1                 ; [CPU_] |695| 
        B         $C$L4,UNC             ; [CPU_] |695| 
        ; branch occurs ; [] |695| 
$C$L3:    
        MOVB      AL,#0                 ; [CPU_] |695| 
$C$L4:    
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 697,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$16	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$16, DW_AT_low_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$13, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$13, DW_AT_TI_end_line(0x2b9)
	.dwattr $C$DW$13, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$13

	.sect	".text"
	.clink

$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_isSpaceAvailableNonFIFO")
	.dwattr $C$DW$17, DW_AT_low_pc(_SCI_isSpaceAvailableNonFIFO)
	.dwattr $C$DW$17, DW_AT_high_pc(0x00)
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_SCI_isSpaceAvailableNonFIFO")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$17, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$17, DW_AT_TI_begin_line(0x2ca)
	.dwattr $C$DW$17, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$17, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 715,column 1,is_stmt,address _SCI_isSpaceAvailableNonFIFO

	.dwfde $C$DW$CIE, _SCI_isSpaceAvailableNonFIFO
$C$DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_isSpaceAvailableNonFIFO  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_isSpaceAvailableNonFIFO:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |715| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 724,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |724| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |724| 
        MOVL      XAR4,ACC              ; [CPU_] |724| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |724| 
        ANDB      AL,#0x80              ; [CPU_] |724| 
        CMPB      AL,#128               ; [CPU_] |724| 
        BF        $C$L5,NEQ             ; [CPU_] |724| 
        ; branchcc occurs ; [] |724| 
        MOVB      AL,#1                 ; [CPU_] |724| 
        B         $C$L6,UNC             ; [CPU_] |724| 
        ; branch occurs ; [] |724| 
$C$L5:    
        MOVB      AL,#0                 ; [CPU_] |724| 
$C$L6:    
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 726,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$17, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$17, DW_AT_TI_end_line(0x2d6)
	.dwattr $C$DW$17, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$17

	.sect	".text"
	.clink

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_getTxFIFOStatus")
	.dwattr $C$DW$21, DW_AT_low_pc(_SCI_getTxFIFOStatus)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_SCI_getTxFIFOStatus")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$21, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0x2e7)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 744,column 1,is_stmt,address _SCI_getTxFIFOStatus

	.dwfde $C$DW$CIE, _SCI_getTxFIFOStatus
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_getTxFIFOStatus          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_getTxFIFOStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |744| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 753,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |753| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |753| 
        MOVL      XAR4,ACC              ; [CPU_] |753| 
        AND       AL,*+XAR4[0],#0x1f00  ; [CPU_] |753| 
        LSR       AL,8                  ; [CPU_] |753| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 755,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$21, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x2f3)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$21

	.sect	".text"
	.clink

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_getRxFIFOStatus")
	.dwattr $C$DW$25, DW_AT_low_pc(_SCI_getRxFIFOStatus)
	.dwattr $C$DW$25, DW_AT_high_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_SCI_getRxFIFOStatus")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$25, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$25, DW_AT_TI_begin_line(0x304)
	.dwattr $C$DW$25, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$25, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 773,column 1,is_stmt,address _SCI_getRxFIFOStatus

	.dwfde $C$DW$CIE, _SCI_getRxFIFOStatus
$C$DW$26	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_getRxFIFOStatus          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_getRxFIFOStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |773| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 782,column 5,is_stmt
        MOVB      ACC,#11               ; [CPU_] |782| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |782| 
        MOVL      XAR4,ACC              ; [CPU_] |782| 
        AND       AL,*+XAR4[0],#0x1f00  ; [CPU_] |782| 
        LSR       AL,8                  ; [CPU_] |782| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 784,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$25, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$25, DW_AT_TI_end_line(0x310)
	.dwattr $C$DW$25, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$25

	.sect	".text"
	.clink

$C$DW$29	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_performSoftwareReset")
	.dwattr $C$DW$29, DW_AT_low_pc(_SCI_performSoftwareReset)
	.dwattr $C$DW$29, DW_AT_high_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_SCI_performSoftwareReset")
	.dwattr $C$DW$29, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$29, DW_AT_TI_begin_line(0x453)
	.dwattr $C$DW$29, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$29, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 1108,column 1,is_stmt,address _SCI_performSoftwareReset

	.dwfde $C$DW$CIE, _SCI_performSoftwareReset
$C$DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_performSoftwareReset     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_performSoftwareReset:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1108| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 1117,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |1117| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1117| 
        MOVL      XAR4,ACC              ; [CPU_] |1117| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |1117| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 1118,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |1118| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1118| 
        MOVL      XAR4,ACC              ; [CPU_] |1118| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |1118| 
	.dwpsn	file "..\ExtraData\driverlib2\sci.h",line 1119,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$29, DW_AT_TI_end_file("..\ExtraData\driverlib2\sci.h")
	.dwattr $C$DW$29, DW_AT_TI_end_line(0x45f)
	.dwattr $C$DW$29, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$29

	.sect	".text"
	.clink
	.global	_SCI_setConfig

$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_setConfig")
	.dwattr $C$DW$33, DW_AT_low_pc(_SCI_setConfig)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_SCI_setConfig")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$33, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 52,column 1,is_stmt,address _SCI_setConfig

	.dwfde $C$DW$CIE, _SCI_setConfig
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg0]
$C$DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lspclkHz")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_lspclkHz")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -10]
$C$DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_name("baud")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_baud")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -12]
$C$DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -14]

;***************************************************************
;* FNAME: _SCI_setConfig                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  2 SOE     *
;***************************************************************

_SCI_setConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -2]
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("divider")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_divider")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |52| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 66,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |66| 
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("_SCI_disableModule")
	.dwattr $C$DW$40, DW_AT_TI_call
        LCR       #_SCI_disableModule   ; [CPU_] |66| 
        ; call occurs [#_SCI_disableModule] ; [] |66| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 71,column 5,is_stmt
        MOVL      P,*-SP[10]            ; [CPU_] |71| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |71| 
        LSL       ACC,3                 ; [CPU_] |71| 
        MOVL      XAR6,ACC              ; [CPU_] |71| 
        MOVB      ACC,#0                ; [CPU_] |71| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |71| 
        MOVL      ACC,P                 ; [CPU_] |71| 
        SUBB      ACC,#1                ; [CPU_] |71| 
        MOVL      *-SP[4],ACC           ; [CPU_] |71| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 76,column 5,is_stmt
        MOVL      XAR4,#65280           ; [CPU_U] |76| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      P,XAR4                ; [CPU_] |76| 
        MOV       AL,PL                 ; [CPU_] |76| 
        MOV       AH,PH                 ; [CPU_] |76| 
        AND       AL,*-SP[4]            ; [CPU_] |76| 
        AND       AH,*-SP[3]            ; [CPU_] |76| 
        MOV       PL,AL                 ; [CPU_] |76| 
        MOV       PH,AH                 ; [CPU_] |76| 
        MOVL      ACC,P                 ; [CPU_] |76| 
        SFR       ACC,8                 ; [CPU_] |76| 
        MOVL      P,ACC                 ; [CPU_] |76| 
        MOVB      ACC,#2                ; [CPU_] |76| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |76| 
        MOVL      XAR4,ACC              ; [CPU_] |76| 
        MOV       *+XAR4[0],P           ; [CPU_] |76| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 77,column 5,is_stmt
        MOVB      ACC,#3                ; [CPU_] |77| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |77| 
        MOVL      XAR4,ACC              ; [CPU_] |77| 
        MOV       AL,*-SP[4]            ; [CPU_] |77| 
        ANDB      AL,#0xff              ; [CPU_] |77| 
        MOV       *+XAR4[0],AL          ; [CPU_] |77| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 82,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |82| 
        AND       AL,*+XAR4[0],#0xff18  ; [CPU_] |82| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |82| 
        OR        AL,*-SP[14]           ; [CPU_] |82| 
        MOV       *+XAR4[0],AL          ; [CPU_] |82| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 90,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |90| 
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("_SCI_enableModule")
	.dwattr $C$DW$41, DW_AT_TI_call
        LCR       #_SCI_enableModule    ; [CPU_] |90| 
        ; call occurs [#_SCI_enableModule] ; [] |90| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 91,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$33, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x5b)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text"
	.clink
	.global	_SCI_writeCharArray

$C$DW$43	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_writeCharArray")
	.dwattr $C$DW$43, DW_AT_low_pc(_SCI_writeCharArray)
	.dwattr $C$DW$43, DW_AT_high_pc(0x00)
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_SCI_writeCharArray")
	.dwattr $C$DW$43, DW_AT_external
	.dwattr $C$DW$43, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$43, DW_AT_TI_begin_line(0x63)
	.dwattr $C$DW$43, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$43, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 101,column 1,is_stmt,address _SCI_writeCharArray

	.dwfde $C$DW$CIE, _SCI_writeCharArray
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg0]
$C$DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_array")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg12]
$C$DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_name("length")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SCI_writeCharArray           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SCI_writeCharArray:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -2]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("array")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_array")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -4]
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("length")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -5]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[5],AR5           ; [CPU_] |101| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |101| 
        MOVL      *-SP[2],ACC           ; [CPU_] |101| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 111,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |111| 
$C$DW$51	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$51, DW_AT_low_pc(0x00)
	.dwattr $C$DW$51, DW_AT_name("_SCI_isFIFOEnabled")
	.dwattr $C$DW$51, DW_AT_TI_call
        LCR       #_SCI_isFIFOEnabled   ; [CPU_] |111| 
        ; call occurs [#_SCI_isFIFOEnabled] ; [] |111| 
        CMPB      AL,#0                 ; [CPU_] |111| 
        BF        $C$L9,EQ              ; [CPU_] |111| 
        ; branchcc occurs ; [] |111| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 117,column 13,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |117| 
        B         $C$L8,UNC             ; [CPU_] |117| 
        ; branch occurs ; [] |117| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 122,column 19,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |122| 
$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x00)
	.dwattr $C$DW$52, DW_AT_name("_SCI_getTxFIFOStatus")
	.dwattr $C$DW$52, DW_AT_TI_call
        LCR       #_SCI_getTxFIFOStatus ; [CPU_] |122| 
        ; call occurs [#_SCI_getTxFIFOStatus] ; [] |122| 
        CMPB      AL,#15                ; [CPU_] |122| 
        BF        $C$L7,EQ              ; [CPU_] |122| 
        ; branchcc occurs ; [] |122| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 129,column 13,is_stmt
        MOVL      XAR5,*-SP[4]          ; [CPU_] |129| 
        MOVZ      AR0,*-SP[6]           ; [CPU_] |129| 
        MOVZ      AR6,*+XAR5[AR0]       ; [CPU_] |129| 
        MOVB      ACC,#9                ; [CPU_] |129| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |129| 
        MOVL      XAR4,ACC              ; [CPU_] |129| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |129| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 117,column 33,is_stmt
        INC       *-SP[6]               ; [CPU_] |117| 
$C$L8:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 117,column 21,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |117| 
        CMP       AL,*-SP[6]            ; [CPU_] |117| 
        B         $C$L7,HI              ; [CPU_] |117| 
        ; branchcc occurs ; [] |117| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 131,column 5,is_stmt
        B         $C$L12,UNC            ; [CPU_] |131| 
        ; branch occurs ; [] |131| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 138,column 13,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |138| 
        B         $C$L11,UNC            ; [CPU_] |138| 
        ; branch occurs ; [] |138| 
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 143,column 19,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |143| 
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_SCI_isSpaceAvailableNonFIFO")
	.dwattr $C$DW$53, DW_AT_TI_call
        LCR       #_SCI_isSpaceAvailableNonFIFO ; [CPU_] |143| 
        ; call occurs [#_SCI_isSpaceAvailableNonFIFO] ; [] |143| 
        CMPB      AL,#0                 ; [CPU_] |143| 
        BF        $C$L10,EQ             ; [CPU_] |143| 
        ; branchcc occurs ; [] |143| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 150,column 13,is_stmt
        MOVL      XAR5,*-SP[4]          ; [CPU_] |150| 
        MOVZ      AR0,*-SP[6]           ; [CPU_] |150| 
        MOVZ      AR6,*+XAR5[AR0]       ; [CPU_] |150| 
        MOVB      ACC,#9                ; [CPU_] |150| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |150| 
        MOVL      XAR4,ACC              ; [CPU_] |150| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |150| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 138,column 33,is_stmt
        INC       *-SP[6]               ; [CPU_] |138| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 138,column 21,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |138| 
        CMP       AL,*-SP[6]            ; [CPU_] |138| 
        B         $C$L10,HI             ; [CPU_] |138| 
        ; branchcc occurs ; [] |138| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 153,column 1,is_stmt
$C$L12:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$43, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$43, DW_AT_TI_end_line(0x99)
	.dwattr $C$DW$43, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$43

	.sect	".text"
	.clink
	.global	_SCI_readCharArray

$C$DW$55	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_readCharArray")
	.dwattr $C$DW$55, DW_AT_low_pc(_SCI_readCharArray)
	.dwattr $C$DW$55, DW_AT_high_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_SCI_readCharArray")
	.dwattr $C$DW$55, DW_AT_external
	.dwattr $C$DW$55, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$55, DW_AT_TI_begin_line(0xa1)
	.dwattr $C$DW$55, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$55, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 162,column 1,is_stmt,address _SCI_readCharArray

	.dwfde $C$DW$CIE, _SCI_readCharArray
$C$DW$56	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg0]
$C$DW$57	.dwtag  DW_TAG_formal_parameter, DW_AT_name("array")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_array")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg12]
$C$DW$58	.dwtag  DW_TAG_formal_parameter, DW_AT_name("length")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _SCI_readCharArray            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SCI_readCharArray:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg20 -2]
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("array")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_array")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg20 -4]
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("length")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -5]
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[5],AR5           ; [CPU_] |162| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |162| 
        MOVL      *-SP[2],ACC           ; [CPU_] |162| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 172,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |172| 
$C$DW$63	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$63, DW_AT_low_pc(0x00)
	.dwattr $C$DW$63, DW_AT_name("_SCI_isFIFOEnabled")
	.dwattr $C$DW$63, DW_AT_TI_call
        LCR       #_SCI_isFIFOEnabled   ; [CPU_] |172| 
        ; call occurs [#_SCI_isFIFOEnabled] ; [] |172| 
        CMPB      AL,#0                 ; [CPU_] |172| 
        BF        $C$L15,EQ             ; [CPU_] |172| 
        ; branchcc occurs ; [] |172| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 178,column 13,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |178| 
        B         $C$L14,UNC            ; [CPU_] |178| 
        ; branch occurs ; [] |178| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 183,column 19,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |183| 
$C$DW$64	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$64, DW_AT_low_pc(0x00)
	.dwattr $C$DW$64, DW_AT_name("_SCI_getRxFIFOStatus")
	.dwattr $C$DW$64, DW_AT_TI_call
        LCR       #_SCI_getRxFIFOStatus ; [CPU_] |183| 
        ; call occurs [#_SCI_getRxFIFOStatus] ; [] |183| 
        CMPB      AL,#0                 ; [CPU_] |183| 
        BF        $C$L13,EQ             ; [CPU_] |183| 
        ; branchcc occurs ; [] |183| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 190,column 13,is_stmt
        MOVB      ACC,#7                ; [CPU_] |190| 
        MOVZ      AR0,*-SP[6]           ; [CPU_] |190| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |190| 
        MOVL      XAR4,ACC              ; [CPU_] |190| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |190| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |190| 
        ANDB      AL,#0xff              ; [CPU_] |190| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |190| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 178,column 33,is_stmt
        INC       *-SP[6]               ; [CPU_] |178| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 178,column 21,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |178| 
        CMP       AL,*-SP[6]            ; [CPU_] |178| 
        B         $C$L13,HI             ; [CPU_] |178| 
        ; branchcc occurs ; [] |178| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 193,column 5,is_stmt
        B         $C$L18,UNC            ; [CPU_] |193| 
        ; branch occurs ; [] |193| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 200,column 13,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |200| 
        B         $C$L17,UNC            ; [CPU_] |200| 
        ; branch occurs ; [] |200| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 205,column 19,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |205| 
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_name("_SCI_isDataAvailableNonFIFO")
	.dwattr $C$DW$65, DW_AT_TI_call
        LCR       #_SCI_isDataAvailableNonFIFO ; [CPU_] |205| 
        ; call occurs [#_SCI_isDataAvailableNonFIFO] ; [] |205| 
        CMPB      AL,#0                 ; [CPU_] |205| 
        BF        $C$L16,EQ             ; [CPU_] |205| 
        ; branchcc occurs ; [] |205| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 212,column 13,is_stmt
        MOVB      ACC,#7                ; [CPU_] |212| 
        MOVZ      AR0,*-SP[6]           ; [CPU_] |212| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |212| 
        MOVL      XAR4,ACC              ; [CPU_] |212| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |212| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |212| 
        ANDB      AL,#0xff              ; [CPU_] |212| 
        MOV       *+XAR4[AR0],AL        ; [CPU_] |212| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 200,column 33,is_stmt
        INC       *-SP[6]               ; [CPU_] |200| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 200,column 21,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |200| 
        CMP       AL,*-SP[6]            ; [CPU_] |200| 
        B         $C$L16,HI             ; [CPU_] |200| 
        ; branchcc occurs ; [] |200| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 216,column 1,is_stmt
$C$L18:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$55, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$55, DW_AT_TI_end_line(0xd8)
	.dwattr $C$DW$55, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$55

	.sect	".text"
	.clink
	.global	_SCI_enableInterrupt

$C$DW$67	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_enableInterrupt")
	.dwattr $C$DW$67, DW_AT_low_pc(_SCI_enableInterrupt)
	.dwattr $C$DW$67, DW_AT_high_pc(0x00)
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_SCI_enableInterrupt")
	.dwattr $C$DW$67, DW_AT_external
	.dwattr $C$DW$67, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$67, DW_AT_TI_begin_line(0xe0)
	.dwattr $C$DW$67, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$67, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 225,column 1,is_stmt,address _SCI_enableInterrupt

	.dwfde $C$DW$CIE, _SCI_enableInterrupt
$C$DW$68	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg0]
$C$DW$69	.dwtag  DW_TAG_formal_parameter, DW_AT_name("intFlags")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_intFlags")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _SCI_enableInterrupt          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_enableInterrupt:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |225| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 234,column 5,is_stmt
        MOVB      XAR6,#1               ; [CPU_] |234| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |234| 
        MOVB      AH,#0                 ; [CPU_] |234| 
        ANDB      AL,#0x01              ; [CPU_] |234| 
        CMPL      ACC,XAR6              ; [CPU_] |234| 
        BF        $C$L19,NEQ            ; [CPU_] |234| 
        ; branchcc occurs ; [] |234| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 236,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |236| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |236| 
        MOVL      XAR4,ACC              ; [CPU_] |236| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |236| 
$C$L19:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 238,column 5,is_stmt
        MOVB      XAR6,#2               ; [CPU_] |238| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |238| 
        MOVB      AH,#0                 ; [CPU_] |238| 
        ANDB      AL,#0x02              ; [CPU_] |238| 
        CMPL      ACC,XAR6              ; [CPU_] |238| 
        BF        $C$L20,NEQ            ; [CPU_] |238| 
        ; branchcc occurs ; [] |238| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 240,column 9,is_stmt
        MOVB      ACC,#4                ; [CPU_] |240| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |240| 
        MOVL      XAR4,ACC              ; [CPU_] |240| 
        OR        *+XAR4[0],#0x0002     ; [CPU_] |240| 
$C$L20:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 242,column 5,is_stmt
        MOVB      XAR6,#4               ; [CPU_] |242| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |242| 
        MOVB      AH,#0                 ; [CPU_] |242| 
        ANDB      AL,#0x04              ; [CPU_] |242| 
        CMPL      ACC,XAR6              ; [CPU_] |242| 
        BF        $C$L21,NEQ            ; [CPU_] |242| 
        ; branchcc occurs ; [] |242| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 244,column 9,is_stmt
        MOVB      ACC,#4                ; [CPU_] |244| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |244| 
        MOVL      XAR4,ACC              ; [CPU_] |244| 
        OR        *+XAR4[0],#0x0001     ; [CPU_] |244| 
$C$L21:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 246,column 5,is_stmt
        MOVB      XAR6,#8               ; [CPU_] |246| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |246| 
        MOVB      AH,#0                 ; [CPU_] |246| 
        ANDB      AL,#0x08              ; [CPU_] |246| 
        CMPL      ACC,XAR6              ; [CPU_] |246| 
        BF        $C$L22,NEQ            ; [CPU_] |246| 
        ; branchcc occurs ; [] |246| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 248,column 9,is_stmt
        MOVB      ACC,#10               ; [CPU_] |248| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |248| 
        MOVL      XAR4,ACC              ; [CPU_] |248| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |248| 
$C$L22:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 250,column 5,is_stmt
        MOVB      XAR6,#16              ; [CPU_] |250| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |250| 
        MOVB      AH,#0                 ; [CPU_] |250| 
        ANDB      AL,#0x10              ; [CPU_] |250| 
        CMPL      ACC,XAR6              ; [CPU_] |250| 
        BF        $C$L23,NEQ            ; [CPU_] |250| 
        ; branchcc occurs ; [] |250| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 252,column 9,is_stmt
        MOVB      ACC,#11               ; [CPU_] |252| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |252| 
        MOVL      XAR4,ACC              ; [CPU_] |252| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |252| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 254,column 1,is_stmt
$C$L23:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$67, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$67, DW_AT_TI_end_line(0xfe)
	.dwattr $C$DW$67, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$67

	.sect	".text"
	.clink
	.global	_SCI_disableInterrupt

$C$DW$72	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_disableInterrupt")
	.dwattr $C$DW$72, DW_AT_low_pc(_SCI_disableInterrupt)
	.dwattr $C$DW$72, DW_AT_high_pc(0x00)
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_SCI_disableInterrupt")
	.dwattr $C$DW$72, DW_AT_external
	.dwattr $C$DW$72, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$72, DW_AT_TI_begin_line(0x106)
	.dwattr $C$DW$72, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$72, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 263,column 1,is_stmt,address _SCI_disableInterrupt

	.dwfde $C$DW$CIE, _SCI_disableInterrupt
$C$DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg0]
$C$DW$74	.dwtag  DW_TAG_formal_parameter, DW_AT_name("intFlags")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_intFlags")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _SCI_disableInterrupt         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_disableInterrupt:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |263| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 272,column 5,is_stmt
        MOVB      XAR6,#1               ; [CPU_] |272| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |272| 
        MOVB      AH,#0                 ; [CPU_] |272| 
        ANDB      AL,#0x01              ; [CPU_] |272| 
        CMPL      ACC,XAR6              ; [CPU_] |272| 
        BF        $C$L24,NEQ            ; [CPU_] |272| 
        ; branchcc occurs ; [] |272| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 274,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |274| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |274| 
        MOVL      XAR4,ACC              ; [CPU_] |274| 
        AND       *+XAR4[0],#0xffbf     ; [CPU_] |274| 
$C$L24:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 276,column 5,is_stmt
        MOVB      XAR6,#2               ; [CPU_] |276| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |276| 
        MOVB      AH,#0                 ; [CPU_] |276| 
        ANDB      AL,#0x02              ; [CPU_] |276| 
        CMPL      ACC,XAR6              ; [CPU_] |276| 
        BF        $C$L25,NEQ            ; [CPU_] |276| 
        ; branchcc occurs ; [] |276| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 278,column 9,is_stmt
        MOVB      ACC,#4                ; [CPU_] |278| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |278| 
        MOVL      XAR4,ACC              ; [CPU_] |278| 
        AND       *+XAR4[0],#0xfffd     ; [CPU_] |278| 
$C$L25:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 280,column 5,is_stmt
        MOVB      XAR6,#4               ; [CPU_] |280| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |280| 
        MOVB      AH,#0                 ; [CPU_] |280| 
        ANDB      AL,#0x04              ; [CPU_] |280| 
        CMPL      ACC,XAR6              ; [CPU_] |280| 
        BF        $C$L26,NEQ            ; [CPU_] |280| 
        ; branchcc occurs ; [] |280| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 282,column 9,is_stmt
        MOVB      ACC,#4                ; [CPU_] |282| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |282| 
        MOVL      XAR4,ACC              ; [CPU_] |282| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |282| 
$C$L26:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 284,column 5,is_stmt
        MOVB      XAR6,#8               ; [CPU_] |284| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |284| 
        MOVB      AH,#0                 ; [CPU_] |284| 
        ANDB      AL,#0x08              ; [CPU_] |284| 
        CMPL      ACC,XAR6              ; [CPU_] |284| 
        BF        $C$L27,NEQ            ; [CPU_] |284| 
        ; branchcc occurs ; [] |284| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 286,column 9,is_stmt
        MOVB      ACC,#10               ; [CPU_] |286| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |286| 
        MOVL      XAR4,ACC              ; [CPU_] |286| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |286| 
$C$L27:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 288,column 5,is_stmt
        MOVB      XAR6,#16              ; [CPU_] |288| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |288| 
        MOVB      AH,#0                 ; [CPU_] |288| 
        ANDB      AL,#0x10              ; [CPU_] |288| 
        CMPL      ACC,XAR6              ; [CPU_] |288| 
        BF        $C$L28,NEQ            ; [CPU_] |288| 
        ; branchcc occurs ; [] |288| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 290,column 9,is_stmt
        MOVB      ACC,#11               ; [CPU_] |290| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |290| 
        MOVL      XAR4,ACC              ; [CPU_] |290| 
        AND       *+XAR4[0],#0xffdf     ; [CPU_] |290| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 292,column 1,is_stmt
$C$L28:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$72, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$72, DW_AT_TI_end_line(0x124)
	.dwattr $C$DW$72, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$72

	.sect	".text"
	.clink
	.global	_SCI_getInterruptStatus

$C$DW$77	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_getInterruptStatus")
	.dwattr $C$DW$77, DW_AT_low_pc(_SCI_getInterruptStatus)
	.dwattr $C$DW$77, DW_AT_high_pc(0x00)
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_SCI_getInterruptStatus")
	.dwattr $C$DW$77, DW_AT_external
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$77, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$77, DW_AT_TI_begin_line(0x12c)
	.dwattr $C$DW$77, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$77, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 301,column 1,is_stmt,address _SCI_getInterruptStatus

	.dwfde $C$DW$CIE, _SCI_getInterruptStatus
$C$DW$78	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SCI_getInterruptStatus       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_SCI_getInterruptStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_breg20 -2]
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("interruptStatus")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_interruptStatus")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |301| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 302,column 30,is_stmt
        MOVB      ACC,#0                ; [CPU_] |302| 
        MOVL      *-SP[4],ACC           ; [CPU_] |302| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 312,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |312| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |312| 
        MOVL      XAR4,ACC              ; [CPU_] |312| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |312| 
        ANDB      AL,#0x80              ; [CPU_] |312| 
        CMPB      AL,#128               ; [CPU_] |312| 
        BF        $C$L29,NEQ            ; [CPU_] |312| 
        ; branchcc occurs ; [] |312| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 314,column 9,is_stmt
        OR        *-SP[4],#4            ; [CPU_] |314| 
$C$L29:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 316,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |316| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |316| 
        MOVL      XAR4,ACC              ; [CPU_] |316| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |316| 
        ANDB      AL,#0x80              ; [CPU_] |316| 
        CMPB      AL,#128               ; [CPU_] |316| 
        BF        $C$L30,NEQ            ; [CPU_] |316| 
        ; branchcc occurs ; [] |316| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 318,column 9,is_stmt
        OR        *-SP[4],#1            ; [CPU_] |318| 
$C$L30:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 320,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |320| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |320| 
        MOVL      XAR4,ACC              ; [CPU_] |320| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |320| 
        ANDB      AL,#0x40              ; [CPU_] |320| 
        CMPB      AL,#64                ; [CPU_] |320| 
        BF        $C$L31,EQ             ; [CPU_] |320| 
        ; branchcc occurs ; [] |320| 
        MOVB      ACC,#5                ; [CPU_] |320| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |320| 
        MOVL      XAR4,ACC              ; [CPU_] |320| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |320| 
        ANDB      AL,#0x20              ; [CPU_] |320| 
        CMPB      AL,#32                ; [CPU_] |320| 
        BF        $C$L32,NEQ            ; [CPU_] |320| 
        ; branchcc occurs ; [] |320| 
$C$L31:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 323,column 9,is_stmt
        OR        *-SP[4],#2            ; [CPU_] |323| 
$C$L32:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 325,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |325| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |325| 
        MOVL      XAR4,ACC              ; [CPU_] |325| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |325| 
        ANDB      AL,#0x80              ; [CPU_] |325| 
        CMPB      AL,#128               ; [CPU_] |325| 
        BF        $C$L33,NEQ            ; [CPU_] |325| 
        ; branchcc occurs ; [] |325| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 327,column 9,is_stmt
        OR        *-SP[4],#8            ; [CPU_] |327| 
$C$L33:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 329,column 5,is_stmt
        MOVB      ACC,#11               ; [CPU_] |329| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |329| 
        MOVL      XAR4,ACC              ; [CPU_] |329| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |329| 
        ANDB      AL,#0x80              ; [CPU_] |329| 
        CMPB      AL,#128               ; [CPU_] |329| 
        BF        $C$L34,NEQ            ; [CPU_] |329| 
        ; branchcc occurs ; [] |329| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 331,column 9,is_stmt
        OR        *-SP[4],#16           ; [CPU_] |331| 
$C$L34:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 333,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |333| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |333| 
        MOVL      XAR4,ACC              ; [CPU_] |333| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |333| 
        ANDB      AL,#0x10              ; [CPU_] |333| 
        CMPB      AL,#16                ; [CPU_] |333| 
        BF        $C$L35,NEQ            ; [CPU_] |333| 
        ; branchcc occurs ; [] |333| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 335,column 9,is_stmt
        OR        *-SP[4],#32           ; [CPU_] |335| 
$C$L35:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 337,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |337| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |337| 
        MOVL      XAR4,ACC              ; [CPU_] |337| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |337| 
        ANDB      AL,#0x08              ; [CPU_] |337| 
        CMPB      AL,#8                 ; [CPU_] |337| 
        BF        $C$L36,NEQ            ; [CPU_] |337| 
        ; branchcc occurs ; [] |337| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 339,column 9,is_stmt
        OR        *-SP[4],#64           ; [CPU_] |339| 
$C$L36:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 341,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |341| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |341| 
        MOVL      XAR4,ACC              ; [CPU_] |341| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |341| 
        ANDB      AL,#0x04              ; [CPU_] |341| 
        CMPB      AL,#4                 ; [CPU_] |341| 
        BF        $C$L37,NEQ            ; [CPU_] |341| 
        ; branchcc occurs ; [] |341| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 343,column 9,is_stmt
        OR        *-SP[4],#128          ; [CPU_] |343| 
$C$L37:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 346,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |346| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 347,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$77, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$77, DW_AT_TI_end_line(0x15b)
	.dwattr $C$DW$77, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$77

	.sect	".text"
	.clink
	.global	_SCI_clearInterruptStatus

$C$DW$82	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_clearInterruptStatus")
	.dwattr $C$DW$82, DW_AT_low_pc(_SCI_clearInterruptStatus)
	.dwattr $C$DW$82, DW_AT_high_pc(0x00)
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_SCI_clearInterruptStatus")
	.dwattr $C$DW$82, DW_AT_external
	.dwattr $C$DW$82, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$82, DW_AT_TI_begin_line(0x163)
	.dwattr $C$DW$82, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$82, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 356,column 1,is_stmt,address _SCI_clearInterruptStatus

	.dwfde $C$DW$CIE, _SCI_clearInterruptStatus
$C$DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg0]
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("intFlags")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_intFlags")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _SCI_clearInterruptStatus     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SCI_clearInterruptStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |356| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 365,column 5,is_stmt
        MOVB      XAR6,#1               ; [CPU_] |365| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |365| 
        MOVB      AH,#0                 ; [CPU_] |365| 
        ANDB      AL,#0x01              ; [CPU_] |365| 
        CMPL      ACC,XAR6              ; [CPU_] |365| 
        BF        $C$L38,EQ             ; [CPU_] |365| 
        ; branchcc occurs ; [] |365| 
        MOVB      XAR6,#2               ; [CPU_] |365| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |365| 
        MOVB      AH,#0                 ; [CPU_] |365| 
        ANDB      AL,#0x02              ; [CPU_] |365| 
        CMPL      ACC,XAR6              ; [CPU_] |365| 
        BF        $C$L38,EQ             ; [CPU_] |365| 
        ; branchcc occurs ; [] |365| 
        MOVB      XAR6,#32              ; [CPU_] |365| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |365| 
        MOVB      AH,#0                 ; [CPU_] |365| 
        ANDB      AL,#0x20              ; [CPU_] |365| 
        CMPL      ACC,XAR6              ; [CPU_] |365| 
        BF        $C$L38,EQ             ; [CPU_] |365| 
        ; branchcc occurs ; [] |365| 
        MOVB      XAR6,#64              ; [CPU_] |365| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |365| 
        MOVB      AH,#0                 ; [CPU_] |365| 
        ANDB      AL,#0x40              ; [CPU_] |365| 
        CMPL      ACC,XAR6              ; [CPU_] |365| 
        BF        $C$L38,EQ             ; [CPU_] |365| 
        ; branchcc occurs ; [] |365| 
        MOVB      XAR6,#128             ; [CPU_] |365| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |365| 
        MOVB      AH,#0                 ; [CPU_] |365| 
        ANDB      AL,#0x80              ; [CPU_] |365| 
        CMPL      ACC,XAR6              ; [CPU_] |365| 
        BF        $C$L39,NEQ            ; [CPU_] |365| 
        ; branchcc occurs ; [] |365| 
$C$L38:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 371,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |371| 
$C$DW$86	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$86, DW_AT_low_pc(0x00)
	.dwattr $C$DW$86, DW_AT_name("_SCI_performSoftwareReset")
	.dwattr $C$DW$86, DW_AT_TI_call
        LCR       #_SCI_performSoftwareReset ; [CPU_] |371| 
        ; call occurs [#_SCI_performSoftwareReset] ; [] |371| 
$C$L39:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 373,column 5,is_stmt
        MOVB      XAR6,#8               ; [CPU_] |373| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |373| 
        MOVB      AH,#0                 ; [CPU_] |373| 
        ANDB      AL,#0x08              ; [CPU_] |373| 
        CMPL      ACC,XAR6              ; [CPU_] |373| 
        BF        $C$L40,NEQ            ; [CPU_] |373| 
        ; branchcc occurs ; [] |373| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 375,column 9,is_stmt
        MOVB      ACC,#10               ; [CPU_] |375| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |375| 
        MOVL      XAR4,ACC              ; [CPU_] |375| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |375| 
$C$L40:    
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 377,column 5,is_stmt
        MOVB      XAR6,#16              ; [CPU_] |377| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |377| 
        MOVB      AH,#0                 ; [CPU_] |377| 
        ANDB      AL,#0x10              ; [CPU_] |377| 
        CMPL      ACC,XAR6              ; [CPU_] |377| 
        BF        $C$L41,NEQ            ; [CPU_] |377| 
        ; branchcc occurs ; [] |377| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 379,column 10,is_stmt
        MOVB      ACC,#11               ; [CPU_] |379| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |379| 
        MOVL      XAR4,ACC              ; [CPU_] |379| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |379| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 381,column 1,is_stmt
$C$L41:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$87	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$87, DW_AT_low_pc(0x00)
	.dwattr $C$DW$87, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$82, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$82, DW_AT_TI_end_line(0x17d)
	.dwattr $C$DW$82, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$82

	.sect	".text"
	.clink
	.global	_SCI_setBaud

$C$DW$88	.dwtag  DW_TAG_subprogram, DW_AT_name("SCI_setBaud")
	.dwattr $C$DW$88, DW_AT_low_pc(_SCI_setBaud)
	.dwattr $C$DW$88, DW_AT_high_pc(0x00)
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_SCI_setBaud")
	.dwattr $C$DW$88, DW_AT_external
	.dwattr $C$DW$88, DW_AT_TI_begin_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$88, DW_AT_TI_begin_line(0x184)
	.dwattr $C$DW$88, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$88, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 389,column 1,is_stmt,address _SCI_setBaud

	.dwfde $C$DW$CIE, _SCI_setBaud
$C$DW$89	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg0]
$C$DW$90	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lspclkHz")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_lspclkHz")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -10]
$C$DW$91	.dwtag  DW_TAG_formal_parameter, DW_AT_name("baud")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_baud")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg20 -12]

;***************************************************************
;* FNAME: _SCI_setBaud                  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  2 SOE     *
;***************************************************************

_SCI_setBaud:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg20 -2]
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("divider")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_divider")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |389| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 395,column 5,is_stmt
        UI32TOF32 R1H,*-SP[12]          ; [CPU_] |395| 
        UI32TOF32 R0H,*-SP[10]          ; [CPU_] |395| 
        MPYF32    R1H,R1H,#16640        ; [CPU_] |395| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$94, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |395| 
        ; call occurs [#FS$$DIV] ; [] |395| 
        ADDF32    R0H,R0H,#49024        ; [CPU_] |395| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,#16128        ; [CPU_] |395| 
        NOP       ; [CPU_] 
        F32TOUI32 R0H,R0H               ; [CPU_] |395| 
        NOP       ; [CPU_] 
        MOV32     *-SP[4],R0H           ; [CPU_] |395| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 400,column 5,is_stmt
        MOVL      XAR4,#65280           ; [CPU_U] |400| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      P,XAR4                ; [CPU_] |400| 
        MOV       AL,PL                 ; [CPU_] |400| 
        MOV       AH,PH                 ; [CPU_] |400| 
        AND       AL,*-SP[4]            ; [CPU_] |400| 
        AND       AH,*-SP[3]            ; [CPU_] |400| 
        MOV       PL,AL                 ; [CPU_] |400| 
        MOV       PH,AH                 ; [CPU_] |400| 
        MOVL      ACC,P                 ; [CPU_] |400| 
        SFR       ACC,8                 ; [CPU_] |400| 
        MOVL      P,ACC                 ; [CPU_] |400| 
        MOVB      ACC,#2                ; [CPU_] |400| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |400| 
        MOVL      XAR4,ACC              ; [CPU_] |400| 
        MOV       *+XAR4[0],P           ; [CPU_] |400| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 401,column 5,is_stmt
        MOVB      ACC,#3                ; [CPU_] |401| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |401| 
        MOVL      XAR4,ACC              ; [CPU_] |401| 
        MOV       AL,*-SP[4]            ; [CPU_] |401| 
        ANDB      AL,#0xff              ; [CPU_] |401| 
        MOV       *+XAR4[0],AL          ; [CPU_] |401| 
	.dwpsn	file "../ExtraData/driverlib2/sci.c",line 402,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$88, DW_AT_TI_end_file("../ExtraData/driverlib2/sci.c")
	.dwattr $C$DW$88, DW_AT_TI_end_line(0x192)
	.dwattr $C$DW$88, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$88

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$96	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX0"), DW_AT_const_value(0x00)
$C$DW$97	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX1"), DW_AT_const_value(0x01)
$C$DW$98	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX2"), DW_AT_const_value(0x02)
$C$DW$99	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX3"), DW_AT_const_value(0x03)
$C$DW$100	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX4"), DW_AT_const_value(0x04)
$C$DW$101	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX5"), DW_AT_const_value(0x05)
$C$DW$102	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX6"), DW_AT_const_value(0x06)
$C$DW$103	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX7"), DW_AT_const_value(0x07)
$C$DW$104	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX8"), DW_AT_const_value(0x08)
$C$DW$105	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX9"), DW_AT_const_value(0x09)
$C$DW$106	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX10"), DW_AT_const_value(0x0a)
$C$DW$107	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX11"), DW_AT_const_value(0x0b)
$C$DW$108	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX12"), DW_AT_const_value(0x0c)
$C$DW$109	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX13"), DW_AT_const_value(0x0d)
$C$DW$110	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX14"), DW_AT_const_value(0x0e)
$C$DW$111	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX15"), DW_AT_const_value(0x0f)
$C$DW$112	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_TX16"), DW_AT_const_value(0x10)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("SCI_TxFIFOLevel")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$24	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$113	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX0"), DW_AT_const_value(0x00)
$C$DW$114	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX1"), DW_AT_const_value(0x01)
$C$DW$115	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX2"), DW_AT_const_value(0x02)
$C$DW$116	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX3"), DW_AT_const_value(0x03)
$C$DW$117	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX4"), DW_AT_const_value(0x04)
$C$DW$118	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX5"), DW_AT_const_value(0x05)
$C$DW$119	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX6"), DW_AT_const_value(0x06)
$C$DW$120	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX7"), DW_AT_const_value(0x07)
$C$DW$121	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX8"), DW_AT_const_value(0x08)
$C$DW$122	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX9"), DW_AT_const_value(0x09)
$C$DW$123	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX10"), DW_AT_const_value(0x0a)
$C$DW$124	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX11"), DW_AT_const_value(0x0b)
$C$DW$125	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX12"), DW_AT_const_value(0x0c)
$C$DW$126	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX13"), DW_AT_const_value(0x0d)
$C$DW$127	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX14"), DW_AT_const_value(0x0e)
$C$DW$128	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX15"), DW_AT_const_value(0x0f)
$C$DW$129	.dwtag  DW_TAG_enumerator, DW_AT_name("SCI_FIFO_RX16"), DW_AT_const_value(0x10)
	.dwendtag $C$DW$T$24

$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("SCI_RxFIFOLevel")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$T$36	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$36, DW_AT_address_class(0x16)
$C$DW$130	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$36)
$C$DW$T$37	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$130)
$C$DW$131	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$31)
$C$DW$T$32	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$131)
$C$DW$T$33	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_address_class(0x16)
$C$DW$132	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$33)
$C$DW$T$34	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$132)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg0]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg1]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg2]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg3]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_reg20]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg21]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg22]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg23]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg24]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_reg25]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg26]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg28]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg29]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg30]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg31]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x20]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x21]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x22]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x23]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x24]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x25]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x26]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_reg4]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_reg6]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_reg8]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_reg10]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_reg12]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_reg14]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_reg16]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_reg17]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_reg18]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_reg19]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_reg5]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_reg7]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg9]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg11]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg13]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg15]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x30]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x33]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x34]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x37]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x38]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x40]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x43]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x44]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x47]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x48]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0x49]
$C$DW$190	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_regx 0x27]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_regx 0x28]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_reg27]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

