;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Mon Jul 25 10:48:12 2022                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../error.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Emosys_Controller\PTM079")
$C$DW$1	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ChargerData_ChargerError")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_ODV_ChargerData_ChargerError")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorLevel")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorLevel")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ChargerData_MasterErrorByte")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ODV_ChargerData_MasterErrorByte")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Statusword")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_ODV_Statusword")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Controlword")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ODV_Controlword")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ChargerData_SlaveErrorByte")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_ODV_ChargerData_SlaveErrorByte")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_WarningNumber")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_ODV_ErrorDsp_WarningNumber")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("GpioDataRegs")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_GpioDataRegs")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2908813 
	.sect	".text"
	.clink
	.global	_ERR_ErrorDigOvld

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorDigOvld")
	.dwattr $C$DW$13, DW_AT_low_pc(_ERR_ErrorDigOvld)
	.dwattr $C$DW$13, DW_AT_high_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_ERR_ErrorDigOvld")
	.dwattr $C$DW$13, DW_AT_external
	.dwattr $C$DW$13, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$13, DW_AT_TI_begin_line(0x34)
	.dwattr $C$DW$13, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$13, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 53,column 1,is_stmt,address _ERR_ErrorDigOvld

	.dwfde $C$DW$CIE, _ERR_ErrorDigOvld

;***************************************************************
;* FNAME: _ERR_ErrorDigOvld             FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ErrorDigOvld:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 54,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |54| 
        CMPB      AL,#2                 ; [CPU_] |54| 
        B         $C$L1,HIS             ; [CPU_] |54| 
        ; branchcc occurs ; [] |54| 
	.dwpsn	file "../error.c",line 56,column 6,is_stmt
        MOVB      ACC,#16               ; [CPU_] |56| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |56| 
	.dwpsn	file "../error.c",line 57,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |57| 
$C$L1:    
	.dwpsn	file "../error.c",line 59,column 3,is_stmt
        MOVB      ACC,#16               ; [CPU_] |59| 
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$14, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |59| 
        ; call occurs [#_ERR_SetError] ; [] |59| 
	.dwpsn	file "../error.c",line 60,column 1,is_stmt
$C$DW$15	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$15, DW_AT_low_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$13, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$13, DW_AT_TI_end_line(0x3c)
	.dwattr $C$DW$13, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$13

	.sect	".text"
	.clink
	.global	_ERR_ISOLATION

$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ISOLATION")
	.dwattr $C$DW$16, DW_AT_low_pc(_ERR_ISOLATION)
	.dwattr $C$DW$16, DW_AT_high_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_ERR_ISOLATION")
	.dwattr $C$DW$16, DW_AT_external
	.dwattr $C$DW$16, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$16, DW_AT_TI_begin_line(0x3f)
	.dwattr $C$DW$16, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$16, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 64,column 1,is_stmt,address _ERR_ISOLATION

	.dwfde $C$DW$CIE, _ERR_ISOLATION

;***************************************************************
;* FNAME: _ERR_ISOLATION                FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ISOLATION:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 65,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |65| 
        CMPB      AL,#2                 ; [CPU_] |65| 
        B         $C$L2,HIS             ; [CPU_] |65| 
        ; branchcc occurs ; [] |65| 
	.dwpsn	file "../error.c",line 67,column 6,is_stmt
        MOVL      XAR4,#256             ; [CPU_U] |67| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,XAR4 ; [CPU_] |67| 
	.dwpsn	file "../error.c",line 68,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |68| 
$C$L2:    
	.dwpsn	file "../error.c",line 70,column 3,is_stmt
        MOV       ACC,#256              ; [CPU_] |70| 
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$17, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |70| 
        ; call occurs [#_ERR_SetError] ; [] |70| 
	.dwpsn	file "../error.c",line 71,column 1,is_stmt
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$16, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$16, DW_AT_TI_end_line(0x47)
	.dwattr $C$DW$16, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$16

	.sect	".text"
	.clink
	.global	_ERR_CONTACTOR_PLUS

$C$DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_CONTACTOR_PLUS")
	.dwattr $C$DW$19, DW_AT_low_pc(_ERR_CONTACTOR_PLUS)
	.dwattr $C$DW$19, DW_AT_high_pc(0x00)
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_ERR_CONTACTOR_PLUS")
	.dwattr $C$DW$19, DW_AT_external
	.dwattr $C$DW$19, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$19, DW_AT_TI_begin_line(0x4a)
	.dwattr $C$DW$19, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$19, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 75,column 1,is_stmt,address _ERR_CONTACTOR_PLUS

	.dwfde $C$DW$CIE, _ERR_CONTACTOR_PLUS

;***************************************************************
;* FNAME: _ERR_CONTACTOR_PLUS           FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_CONTACTOR_PLUS:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 76,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |76| 
        CMPB      AL,#2                 ; [CPU_] |76| 
        B         $C$L3,HIS             ; [CPU_] |76| 
        ; branchcc occurs ; [] |76| 
	.dwpsn	file "../error.c",line 78,column 6,is_stmt
        MOVL      XAR4,#512             ; [CPU_U] |78| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,XAR4 ; [CPU_] |78| 
	.dwpsn	file "../error.c",line 79,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |79| 
$C$L3:    
	.dwpsn	file "../error.c",line 81,column 3,is_stmt
        MOV       ACC,#512              ; [CPU_] |81| 
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$20, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |81| 
        ; call occurs [#_ERR_SetError] ; [] |81| 
	.dwpsn	file "../error.c",line 82,column 1,is_stmt
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$19, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$19, DW_AT_TI_end_line(0x52)
	.dwattr $C$DW$19, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$19

	.sect	".text"
	.clink
	.global	_ERR_CONTACTOR_NEG

$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_CONTACTOR_NEG")
	.dwattr $C$DW$22, DW_AT_low_pc(_ERR_CONTACTOR_NEG)
	.dwattr $C$DW$22, DW_AT_high_pc(0x00)
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_ERR_CONTACTOR_NEG")
	.dwattr $C$DW$22, DW_AT_external
	.dwattr $C$DW$22, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$22, DW_AT_TI_begin_line(0x54)
	.dwattr $C$DW$22, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$22, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 85,column 1,is_stmt,address _ERR_CONTACTOR_NEG

	.dwfde $C$DW$CIE, _ERR_CONTACTOR_NEG

;***************************************************************
;* FNAME: _ERR_CONTACTOR_NEG            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_CONTACTOR_NEG:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 86,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |86| 
        CMPB      AL,#2                 ; [CPU_] |86| 
        B         $C$L4,HIS             ; [CPU_] |86| 
        ; branchcc occurs ; [] |86| 
	.dwpsn	file "../error.c",line 88,column 6,is_stmt
        MOVL      XAR4,#1024            ; [CPU_U] |88| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,XAR4 ; [CPU_] |88| 
	.dwpsn	file "../error.c",line 89,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |89| 
$C$L4:    
	.dwpsn	file "../error.c",line 91,column 3,is_stmt
        MOV       ACC,#1024             ; [CPU_] |91| 
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$23, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |91| 
        ; call occurs [#_ERR_SetError] ; [] |91| 
	.dwpsn	file "../error.c",line 92,column 1,is_stmt
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$22, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$22, DW_AT_TI_end_line(0x5c)
	.dwattr $C$DW$22, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$22

	.sect	".text"
	.clink
	.global	_ERR_CONTACTOR_PRE

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_CONTACTOR_PRE")
	.dwattr $C$DW$25, DW_AT_low_pc(_ERR_CONTACTOR_PRE)
	.dwattr $C$DW$25, DW_AT_high_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_ERR_CONTACTOR_PRE")
	.dwattr $C$DW$25, DW_AT_external
	.dwattr $C$DW$25, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$25, DW_AT_TI_begin_line(0x5e)
	.dwattr $C$DW$25, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$25, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 95,column 1,is_stmt,address _ERR_CONTACTOR_PRE

	.dwfde $C$DW$CIE, _ERR_CONTACTOR_PRE

;***************************************************************
;* FNAME: _ERR_CONTACTOR_PRE            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_CONTACTOR_PRE:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 96,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |96| 
        CMPB      AL,#2                 ; [CPU_] |96| 
        B         $C$L5,HIS             ; [CPU_] |96| 
        ; branchcc occurs ; [] |96| 
	.dwpsn	file "../error.c",line 98,column 6,is_stmt
        MOVL      XAR4,#2048            ; [CPU_U] |98| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,XAR4 ; [CPU_] |98| 
	.dwpsn	file "../error.c",line 99,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |99| 
$C$L5:    
	.dwpsn	file "../error.c",line 101,column 3,is_stmt
        MOV       ACC,#2048             ; [CPU_] |101| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$26, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |101| 
        ; call occurs [#_ERR_SetError] ; [] |101| 
	.dwpsn	file "../error.c",line 102,column 1,is_stmt
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$25, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$25, DW_AT_TI_end_line(0x66)
	.dwattr $C$DW$25, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$25

	.sect	".text"
	.clink
	.global	_ERR_ErrorParam

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorParam")
	.dwattr $C$DW$28, DW_AT_low_pc(_ERR_ErrorParam)
	.dwattr $C$DW$28, DW_AT_high_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ERR_ErrorParam")
	.dwattr $C$DW$28, DW_AT_external
	.dwattr $C$DW$28, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$28, DW_AT_TI_begin_line(0x68)
	.dwattr $C$DW$28, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$28, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 105,column 1,is_stmt,address _ERR_ErrorParam

	.dwfde $C$DW$CIE, _ERR_ErrorParam

;***************************************************************
;* FNAME: _ERR_ErrorParam               FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ErrorParam:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 106,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |106| 
        CMPB      AL,#2                 ; [CPU_] |106| 
        B         $C$L6,HIS             ; [CPU_] |106| 
        ; branchcc occurs ; [] |106| 
	.dwpsn	file "../error.c",line 108,column 6,is_stmt
        MOVB      ACC,#64               ; [CPU_] |108| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |108| 
	.dwpsn	file "../error.c",line 109,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |109| 
$C$L6:    
	.dwpsn	file "../error.c",line 111,column 3,is_stmt
        MOVB      ACC,#64               ; [CPU_] |111| 
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$29, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |111| 
        ; call occurs [#_ERR_SetError] ; [] |111| 
	.dwpsn	file "../error.c",line 112,column 1,is_stmt
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$28, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$28, DW_AT_TI_end_line(0x70)
	.dwattr $C$DW$28, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$28

	.sect	".text"
	.clink
	.global	_ERR_ErrorOverCurrent

$C$DW$31	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverCurrent")
	.dwattr $C$DW$31, DW_AT_low_pc(_ERR_ErrorOverCurrent)
	.dwattr $C$DW$31, DW_AT_high_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$31, DW_AT_external
	.dwattr $C$DW$31, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$31, DW_AT_TI_begin_line(0x77)
	.dwattr $C$DW$31, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$31, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 120,column 1,is_stmt,address _ERR_ErrorOverCurrent

	.dwfde $C$DW$CIE, _ERR_ErrorOverCurrent

;***************************************************************
;* FNAME: _ERR_ErrorOverCurrent         FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ErrorOverCurrent:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 121,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |121| 
        CMPB      AL,#2                 ; [CPU_] |121| 
        B         $C$L7,HIS             ; [CPU_] |121| 
        ; branchcc occurs ; [] |121| 
	.dwpsn	file "../error.c",line 123,column 6,is_stmt
        MOVB      ACC,#2                ; [CPU_] |123| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |123| 
	.dwpsn	file "../error.c",line 124,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |124| 
$C$L7:    
	.dwpsn	file "../error.c",line 126,column 3,is_stmt
        MOVB      ACC,#2                ; [CPU_] |126| 
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$32, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |126| 
        ; call occurs [#_ERR_SetError] ; [] |126| 
	.dwpsn	file "../error.c",line 127,column 1,is_stmt
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$31, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$31, DW_AT_TI_end_line(0x7f)
	.dwattr $C$DW$31, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$31

	.sect	".text"
	.clink
	.global	_ERR_ErrorOverTemp

$C$DW$34	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverTemp")
	.dwattr $C$DW$34, DW_AT_low_pc(_ERR_ErrorOverTemp)
	.dwattr $C$DW$34, DW_AT_high_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$34, DW_AT_external
	.dwattr $C$DW$34, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$34, DW_AT_TI_begin_line(0x84)
	.dwattr $C$DW$34, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$34, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 133,column 1,is_stmt,address _ERR_ErrorOverTemp

	.dwfde $C$DW$CIE, _ERR_ErrorOverTemp

;***************************************************************
;* FNAME: _ERR_ErrorOverTemp            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ErrorOverTemp:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 134,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |134| 
        CMPB      AL,#2                 ; [CPU_] |134| 
        B         $C$L8,HIS             ; [CPU_] |134| 
        ; branchcc occurs ; [] |134| 
	.dwpsn	file "../error.c",line 136,column 6,is_stmt
        MOVB      ACC,#32               ; [CPU_] |136| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |136| 
	.dwpsn	file "../error.c",line 137,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |137| 
$C$L8:    
	.dwpsn	file "../error.c",line 139,column 3,is_stmt
        MOVB      ACC,#32               ; [CPU_] |139| 
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$35, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |139| 
        ; call occurs [#_ERR_SetError] ; [] |139| 
	.dwpsn	file "../error.c",line 140,column 1,is_stmt
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$34, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$34, DW_AT_TI_end_line(0x8c)
	.dwattr $C$DW$34, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$34

	.sect	".text"
	.clink
	.global	_ERR_ErrorComm

$C$DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorComm")
	.dwattr $C$DW$37, DW_AT_low_pc(_ERR_ErrorComm)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ERR_ErrorComm")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0x8f)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 144,column 1,is_stmt,address _ERR_ErrorComm

	.dwfde $C$DW$CIE, _ERR_ErrorComm

;***************************************************************
;* FNAME: _ERR_ErrorComm                FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ErrorComm:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 145,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |145| 
        CMPB      AL,#2                 ; [CPU_] |145| 
        B         $C$L9,HIS             ; [CPU_] |145| 
        ; branchcc occurs ; [] |145| 
	.dwpsn	file "../error.c",line 147,column 6,is_stmt
        MOVB      ACC,#128              ; [CPU_] |147| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |147| 
	.dwpsn	file "../error.c",line 148,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |148| 
$C$L9:    
	.dwpsn	file "../error.c",line 150,column 3,is_stmt
        MOVB      ACC,#128              ; [CPU_] |150| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$38, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |150| 
        ; call occurs [#_ERR_SetError] ; [] |150| 
	.dwpsn	file "../error.c",line 151,column 1,is_stmt
$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$37, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0x97)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

	.sect	".text"
	.clink
	.global	_WARN_Insulation

$C$DW$40	.dwtag  DW_TAG_subprogram, DW_AT_name("WARN_Insulation")
	.dwattr $C$DW$40, DW_AT_low_pc(_WARN_Insulation)
	.dwattr $C$DW$40, DW_AT_high_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_WARN_Insulation")
	.dwattr $C$DW$40, DW_AT_external
	.dwattr $C$DW$40, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$40, DW_AT_TI_begin_line(0x99)
	.dwattr $C$DW$40, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$40, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 153,column 27,is_stmt,address _WARN_Insulation

	.dwfde $C$DW$CIE, _WARN_Insulation

;***************************************************************
;* FNAME: _WARN_Insulation              FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_WARN_Insulation:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 160,column 1,is_stmt
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$40, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$40, DW_AT_TI_end_line(0xa0)
	.dwattr $C$DW$40, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$40

	.sect	".text"
	.clink
	.global	_ERR_ErrorOverVoltage

$C$DW$42	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverVoltage")
	.dwattr $C$DW$42, DW_AT_low_pc(_ERR_ErrorOverVoltage)
	.dwattr $C$DW$42, DW_AT_high_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$42, DW_AT_external
	.dwattr $C$DW$42, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$42, DW_AT_TI_begin_line(0xa2)
	.dwattr $C$DW$42, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$42, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 163,column 1,is_stmt,address _ERR_ErrorOverVoltage

	.dwfde $C$DW$CIE, _ERR_ErrorOverVoltage

;***************************************************************
;* FNAME: _ERR_ErrorOverVoltage         FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ErrorOverVoltage:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 164,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |164| 
        CMPB      AL,#2                 ; [CPU_] |164| 
        B         $C$L10,HIS            ; [CPU_] |164| 
        ; branchcc occurs ; [] |164| 
	.dwpsn	file "../error.c",line 166,column 6,is_stmt
        MOVB      ACC,#4                ; [CPU_] |166| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |166| 
	.dwpsn	file "../error.c",line 167,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |167| 
$C$L10:    
	.dwpsn	file "../error.c",line 169,column 3,is_stmt
        MOVB      ACC,#4                ; [CPU_] |169| 
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$43, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |169| 
        ; call occurs [#_ERR_SetError] ; [] |169| 
	.dwpsn	file "../error.c",line 170,column 1,is_stmt
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$42, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$42, DW_AT_TI_end_line(0xaa)
	.dwattr $C$DW$42, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$42

	.sect	".text"
	.clink
	.global	_ERR_ErrorUnderVoltage

$C$DW$45	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorUnderVoltage")
	.dwattr $C$DW$45, DW_AT_low_pc(_ERR_ErrorUnderVoltage)
	.dwattr $C$DW$45, DW_AT_high_pc(0x00)
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$45, DW_AT_external
	.dwattr $C$DW$45, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$45, DW_AT_TI_begin_line(0xac)
	.dwattr $C$DW$45, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$45, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 173,column 1,is_stmt,address _ERR_ErrorUnderVoltage

	.dwfde $C$DW$CIE, _ERR_ErrorUnderVoltage

;***************************************************************
;* FNAME: _ERR_ErrorUnderVoltage        FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ErrorUnderVoltage:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 174,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |174| 
        CMPB      AL,#2                 ; [CPU_] |174| 
        B         $C$L11,HIS            ; [CPU_] |174| 
        ; branchcc occurs ; [] |174| 
	.dwpsn	file "../error.c",line 176,column 6,is_stmt
        MOVB      ACC,#8                ; [CPU_] |176| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |176| 
	.dwpsn	file "../error.c",line 177,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |177| 
$C$L11:    
	.dwpsn	file "../error.c",line 179,column 3,is_stmt
        MOVB      ACC,#8                ; [CPU_] |179| 
$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$46, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |179| 
        ; call occurs [#_ERR_SetError] ; [] |179| 
	.dwpsn	file "../error.c",line 180,column 1,is_stmt
$C$DW$47	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$47, DW_AT_low_pc(0x00)
	.dwattr $C$DW$47, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$45, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$45, DW_AT_TI_end_line(0xb4)
	.dwattr $C$DW$45, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$45

	.sect	".text"
	.clink
	.global	_ERR_DELTA_Voltage

$C$DW$48	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_DELTA_Voltage")
	.dwattr $C$DW$48, DW_AT_low_pc(_ERR_DELTA_Voltage)
	.dwattr $C$DW$48, DW_AT_high_pc(0x00)
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ERR_DELTA_Voltage")
	.dwattr $C$DW$48, DW_AT_external
	.dwattr $C$DW$48, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$48, DW_AT_TI_begin_line(0xb6)
	.dwattr $C$DW$48, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$48, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 183,column 1,is_stmt,address _ERR_DELTA_Voltage

	.dwfde $C$DW$CIE, _ERR_DELTA_Voltage

;***************************************************************
;* FNAME: _ERR_DELTA_Voltage            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_DELTA_Voltage:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 184,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       AL,@_ODV_ErrorDsp_ErrorLevel ; [CPU_] |184| 
        CMPB      AL,#2                 ; [CPU_] |184| 
        B         $C$L12,HIS            ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
	.dwpsn	file "../error.c",line 186,column 6,is_stmt
        MOVL      XAR4,#4096            ; [CPU_U] |186| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,XAR4 ; [CPU_] |186| 
	.dwpsn	file "../error.c",line 187,column 6,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOVB      @_ODV_ErrorDsp_ErrorLevel,#1,UNC ; [CPU_] |187| 
$C$L12:    
	.dwpsn	file "../error.c",line 189,column 3,is_stmt
        MOV       ACC,#4096             ; [CPU_] |189| 
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$49, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |189| 
        ; call occurs [#_ERR_SetError] ; [] |189| 
	.dwpsn	file "../error.c",line 190,column 1,is_stmt
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$48, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$48, DW_AT_TI_end_line(0xbe)
	.dwattr $C$DW$48, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$48

	.sect	".text"
	.clink
	.global	_ERR_SetError

$C$DW$51	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_SetError")
	.dwattr $C$DW$51, DW_AT_low_pc(_ERR_SetError)
	.dwattr $C$DW$51, DW_AT_high_pc(0x00)
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_ERR_SetError")
	.dwattr $C$DW$51, DW_AT_external
	.dwattr $C$DW$51, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$51, DW_AT_TI_begin_line(0xc0)
	.dwattr $C$DW$51, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$51, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../error.c",line 192,column 37,is_stmt,address _ERR_SetError

	.dwfde $C$DW$CIE, _ERR_SetError
$C$DW$52	.dwtag  DW_TAG_formal_parameter, DW_AT_name("error_code")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _ERR_SetError                 FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_ERR_SetError:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("error_code")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_error_code")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |192| 
	.dwpsn	file "../error.c",line 193,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      ACC,*-SP[2]           ; [CPU_] |193| 
        OR        @_ODV_Gateway_Errorcode,AL ; [CPU_] |193| 
        OR        @_ODV_Gateway_Errorcode+1,AH ; [CPU_] |193| 
	.dwpsn	file "../error.c",line 194,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        OR        @_ODV_Gateway_State,#0x0080 ; [CPU_] |194| 
	.dwpsn	file "../error.c",line 195,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#64,UNC ; [CPU_] |195| 
	.dwpsn	file "../error.c",line 196,column 3,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       @_GpioDataRegs+1,#0xfdff ; [CPU_] |196| 
	.dwpsn	file "../error.c",line 197,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$51, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$51, DW_AT_TI_end_line(0xc5)
	.dwattr $C$DW$51, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$51

	.sect	".text"
	.clink
	.global	_ChargerError

$C$DW$55	.dwtag  DW_TAG_subprogram, DW_AT_name("ChargerError")
	.dwattr $C$DW$55, DW_AT_low_pc(_ChargerError)
	.dwattr $C$DW$55, DW_AT_high_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ChargerError")
	.dwattr $C$DW$55, DW_AT_external
	.dwattr $C$DW$55, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$55, DW_AT_TI_begin_line(0xd0)
	.dwattr $C$DW$55, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$55, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 208,column 25,is_stmt,address _ChargerError

	.dwfde $C$DW$CIE, _ChargerError

;***************************************************************
;* FNAME: _ChargerError                 FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ChargerError:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 210,column 5,is_stmt
        MOVW      DP,#_ODV_ChargerData_MasterErrorByte ; [CPU_U] 
        MOV       AH,@_ODV_ChargerData_MasterErrorByte ; [CPU_] |210| 
        MOVW      DP,#_ODV_ChargerData_SlaveErrorByte ; [CPU_U] 
        ANDB      AH,#0x01              ; [CPU_] |210| 
        MOV       AL,@_ODV_ChargerData_SlaveErrorByte ; [CPU_] |210| 
        ANDB      AL,#0x01              ; [CPU_] |210| 
        OR        AL,AH                 ; [CPU_] |210| 
        BF        $C$L13,EQ             ; [CPU_] |210| 
        ; branchcc occurs ; [] |210| 
	.dwpsn	file "../error.c",line 211,column 6,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        OR        @_ODV_ChargerData_ChargerError,#0x0001 ; [CPU_] |211| 
	.dwpsn	file "../error.c",line 212,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |212| 
        ; branch occurs ; [] |212| 
$C$L13:    
	.dwpsn	file "../error.c",line 212,column 12,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        AND       @_ODV_ChargerData_ChargerError,#0xfffe ; [CPU_] |212| 
$C$L14:    
	.dwpsn	file "../error.c",line 214,column 5,is_stmt
        MOVW      DP,#_ODV_ChargerData_MasterErrorByte ; [CPU_U] 
        MOV       AH,@_ODV_ChargerData_MasterErrorByte ; [CPU_] |214| 
        MOVW      DP,#_ODV_ChargerData_SlaveErrorByte ; [CPU_U] 
        ANDB      AH,#0x02              ; [CPU_] |214| 
        MOV       AL,@_ODV_ChargerData_SlaveErrorByte ; [CPU_] |214| 
        ANDB      AL,#0x02              ; [CPU_] |214| 
        OR        AL,AH                 ; [CPU_] |214| 
        BF        $C$L15,EQ             ; [CPU_] |214| 
        ; branchcc occurs ; [] |214| 
	.dwpsn	file "../error.c",line 215,column 6,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        OR        @_ODV_ChargerData_ChargerError,#0x0002 ; [CPU_] |215| 
	.dwpsn	file "../error.c",line 216,column 5,is_stmt
        B         $C$L16,UNC            ; [CPU_] |216| 
        ; branch occurs ; [] |216| 
$C$L15:    
	.dwpsn	file "../error.c",line 216,column 12,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        AND       @_ODV_ChargerData_ChargerError,#0xfffd ; [CPU_] |216| 
$C$L16:    
	.dwpsn	file "../error.c",line 218,column 5,is_stmt
        MOVW      DP,#_ODV_ChargerData_MasterErrorByte ; [CPU_U] 
        MOV       AH,@_ODV_ChargerData_MasterErrorByte ; [CPU_] |218| 
        MOVW      DP,#_ODV_ChargerData_SlaveErrorByte ; [CPU_U] 
        ANDB      AH,#0x08              ; [CPU_] |218| 
        MOV       AL,@_ODV_ChargerData_SlaveErrorByte ; [CPU_] |218| 
        ANDB      AL,#0x08              ; [CPU_] |218| 
        OR        AL,AH                 ; [CPU_] |218| 
        BF        $C$L17,EQ             ; [CPU_] |218| 
        ; branchcc occurs ; [] |218| 
	.dwpsn	file "../error.c",line 219,column 6,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        OR        @_ODV_ChargerData_ChargerError,#0x0008 ; [CPU_] |219| 
	.dwpsn	file "../error.c",line 220,column 5,is_stmt
        B         $C$L18,UNC            ; [CPU_] |220| 
        ; branch occurs ; [] |220| 
$C$L17:    
	.dwpsn	file "../error.c",line 220,column 12,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        AND       @_ODV_ChargerData_ChargerError,#0xfff7 ; [CPU_] |220| 
$C$L18:    
	.dwpsn	file "../error.c",line 222,column 5,is_stmt
        MOVW      DP,#_ODV_ChargerData_MasterErrorByte ; [CPU_U] 
        MOV       AH,@_ODV_ChargerData_MasterErrorByte ; [CPU_] |222| 
        MOVW      DP,#_ODV_ChargerData_SlaveErrorByte ; [CPU_U] 
        ANDB      AH,#0x20              ; [CPU_] |222| 
        MOV       AL,@_ODV_ChargerData_SlaveErrorByte ; [CPU_] |222| 
        ANDB      AL,#0x20              ; [CPU_] |222| 
        OR        AL,AH                 ; [CPU_] |222| 
        BF        $C$L19,EQ             ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
	.dwpsn	file "../error.c",line 223,column 6,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        OR        @_ODV_ChargerData_ChargerError,#0x0020 ; [CPU_] |223| 
	.dwpsn	file "../error.c",line 224,column 5,is_stmt
        B         $C$L20,UNC            ; [CPU_] |224| 
        ; branch occurs ; [] |224| 
$C$L19:    
	.dwpsn	file "../error.c",line 224,column 12,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        AND       @_ODV_ChargerData_ChargerError,#0xffdf ; [CPU_] |224| 
$C$L20:    
	.dwpsn	file "../error.c",line 226,column 5,is_stmt
        MOVW      DP,#_ODV_ChargerData_MasterErrorByte ; [CPU_U] 
        MOV       AH,@_ODV_ChargerData_MasterErrorByte ; [CPU_] |226| 
        MOVW      DP,#_ODV_ChargerData_SlaveErrorByte ; [CPU_U] 
        ANDB      AH,#0x40              ; [CPU_] |226| 
        MOV       AL,@_ODV_ChargerData_SlaveErrorByte ; [CPU_] |226| 
        ANDB      AL,#0x40              ; [CPU_] |226| 
        OR        AL,AH                 ; [CPU_] |226| 
        BF        $C$L21,EQ             ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
	.dwpsn	file "../error.c",line 227,column 6,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        OR        @_ODV_ChargerData_ChargerError,#0x0040 ; [CPU_] |227| 
	.dwpsn	file "../error.c",line 228,column 5,is_stmt
        B         $C$L22,UNC            ; [CPU_] |228| 
        ; branch occurs ; [] |228| 
$C$L21:    
	.dwpsn	file "../error.c",line 228,column 12,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        AND       @_ODV_ChargerData_ChargerError,#0xffbf ; [CPU_] |228| 
	.dwpsn	file "../error.c",line 230,column 1,is_stmt
$C$L22:    
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$55, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$55, DW_AT_TI_end_line(0xe6)
	.dwattr $C$DW$55, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$55

	.sect	".text"
	.clink
	.global	_ERR_ClearError

$C$DW$57	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearError")
	.dwattr $C$DW$57, DW_AT_low_pc(_ERR_ClearError)
	.dwattr $C$DW$57, DW_AT_high_pc(0x00)
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ERR_ClearError")
	.dwattr $C$DW$57, DW_AT_external
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$57, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$57, DW_AT_TI_begin_line(0xe9)
	.dwattr $C$DW$57, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$57, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../error.c",line 233,column 28,is_stmt,address _ERR_ClearError

	.dwfde $C$DW$CIE, _ERR_ClearError

;***************************************************************
;* FNAME: _ERR_ClearError               FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_ERR_ClearError:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("res")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_res")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg20 -1]
	.dwpsn	file "../error.c",line 236,column 13,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |236| 
	.dwpsn	file "../error.c",line 237,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      ACC,@_ODV_Gateway_Errorcode ; [CPU_] |237| 
        BF        $C$L23,NEQ            ; [CPU_] |237| 
        ; branchcc occurs ; [] |237| 
	.dwpsn	file "../error.c",line 239,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        AND       @_ODV_Gateway_State,#0xff7f ; [CPU_] |239| 
	.dwpsn	file "../error.c",line 240,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |240| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,ACC ; [CPU_] |240| 
	.dwpsn	file "../error.c",line 241,column 5,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorLevel ; [CPU_U] 
        MOV       @_ODV_ErrorDsp_ErrorLevel,#0 ; [CPU_] |241| 
	.dwpsn	file "../error.c",line 242,column 5,is_stmt
        MOVW      DP,#_ODV_Statusword   ; [CPU_U] 
        AND       @_ODV_Statusword,#0xfff7 ; [CPU_] |242| 
	.dwpsn	file "../error.c",line 243,column 5,is_stmt
        OR        @_ODV_Statusword,#0x0040 ; [CPU_] |243| 
	.dwpsn	file "../error.c",line 244,column 5,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        AND       @_ODV_Controlword,#0xff7f ; [CPU_] |244| 
	.dwpsn	file "../error.c",line 245,column 5,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0200 ; [CPU_] |245| 
	.dwpsn	file "../error.c",line 247,column 5,is_stmt
        MOVB      *-SP[1],#1,UNC        ; [CPU_] |247| 
$C$L23:    
	.dwpsn	file "../error.c",line 249,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |249| 
	.dwpsn	file "../error.c",line 250,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$57, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$57, DW_AT_TI_end_line(0xfa)
	.dwattr $C$DW$57, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$57

	.sect	".text"
	.clink
	.global	_ClearErrorCallback

$C$DW$60	.dwtag  DW_TAG_subprogram, DW_AT_name("ClearErrorCallback")
	.dwattr $C$DW$60, DW_AT_low_pc(_ClearErrorCallback)
	.dwattr $C$DW$60, DW_AT_high_pc(0x00)
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ClearErrorCallback")
	.dwattr $C$DW$60, DW_AT_external
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$60, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$60, DW_AT_TI_begin_line(0xfc)
	.dwattr $C$DW$60, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$60, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../error.c",line 252,column 97,is_stmt,address _ClearErrorCallback

	.dwfde $C$DW$CIE, _ClearErrorCallback
$C$DW$61	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg12]
$C$DW$62	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg14]
$C$DW$63	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg0]
$C$DW$64	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ClearErrorCallback           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ClearErrorCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -2]
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_breg20 -4]
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -5]
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |252| 
        MOV       *-SP[5],AL            ; [CPU_] |252| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |252| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |252| 
	.dwpsn	file "../error.c",line 253,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |253| 
        CMPB      AL,#1                 ; [CPU_] |253| 
        BF        $C$L25,NEQ            ; [CPU_] |253| 
        ; branchcc occurs ; [] |253| 
	.dwpsn	file "../error.c",line 254,column 5,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |254| 
        BF        $C$L24,NEQ            ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
	.dwpsn	file "../error.c",line 255,column 7,is_stmt
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_name("_ERR_ClearError")
	.dwattr $C$DW$69, DW_AT_TI_call
        LCR       #_ERR_ClearError      ; [CPU_] |255| 
        ; call occurs [#_ERR_ClearError] ; [] |255| 
$C$L24:    
	.dwpsn	file "../error.c",line 256,column 5,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_WarningNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_WarningNumber ; [CPU_] |256| 
        BF        $C$L25,NEQ            ; [CPU_] |256| 
        ; branchcc occurs ; [] |256| 
	.dwpsn	file "../error.c",line 257,column 7,is_stmt
$C$DW$70	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$70, DW_AT_low_pc(0x00)
	.dwattr $C$DW$70, DW_AT_name("_ERR_ClearWarnings")
	.dwattr $C$DW$70, DW_AT_TI_call
        LCR       #_ERR_ClearWarnings   ; [CPU_] |257| 
        ; call occurs [#_ERR_ClearWarnings] ; [] |257| 
$C$L25:    
	.dwpsn	file "../error.c",line 259,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |259| 
	.dwpsn	file "../error.c",line 260,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$60, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$60, DW_AT_TI_end_line(0x104)
	.dwattr $C$DW$60, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$60

	.sect	".text"
	.clink
	.global	_ERR_HandleWarning

$C$DW$72	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$72, DW_AT_low_pc(_ERR_HandleWarning)
	.dwattr $C$DW$72, DW_AT_high_pc(0x00)
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$72, DW_AT_external
	.dwattr $C$DW$72, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$72, DW_AT_TI_begin_line(0x10a)
	.dwattr $C$DW$72, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$72, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../error.c",line 267,column 1,is_stmt,address _ERR_HandleWarning

	.dwfde $C$DW$CIE, _ERR_HandleWarning
$C$DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_name("warningno")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_warningno")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _ERR_HandleWarning            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_ERR_HandleWarning:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("warningno")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_warningno")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |267| 
	.dwpsn	file "../error.c",line 269,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_WarningNumber ; [CPU_U] 
        MOVL      ACC,*-SP[2]           ; [CPU_] |269| 
        MOVL      @_ODV_ErrorDsp_WarningNumber,ACC ; [CPU_] |269| 
	.dwpsn	file "../error.c",line 270,column 3,is_stmt
        MOVW      DP,#_ODV_Statusword   ; [CPU_U] 
        OR        @_ODV_Statusword,#0x0080 ; [CPU_] |270| 
	.dwpsn	file "../error.c",line 271,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      ACC,*-SP[2]           ; [CPU_] |271| 
        OR        @_ODV_Gateway_Errorcode,AL ; [CPU_] |271| 
        OR        @_ODV_Gateway_Errorcode+1,AH ; [CPU_] |271| 
	.dwpsn	file "../error.c",line 272,column 3,is_stmt
	.dwpsn	file "../error.c",line 273,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$72, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$72, DW_AT_TI_end_line(0x111)
	.dwattr $C$DW$72, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$72

	.sect	".text"
	.clink
	.global	_ERR_ClearWarnings

$C$DW$76	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarnings")
	.dwattr $C$DW$76, DW_AT_low_pc(_ERR_ClearWarnings)
	.dwattr $C$DW$76, DW_AT_high_pc(0x00)
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_ERR_ClearWarnings")
	.dwattr $C$DW$76, DW_AT_external
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$76, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$76, DW_AT_TI_begin_line(0x113)
	.dwattr $C$DW$76, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$76, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../error.c",line 276,column 1,is_stmt,address _ERR_ClearWarnings

	.dwfde $C$DW$CIE, _ERR_ClearWarnings

;***************************************************************
;* FNAME: _ERR_ClearWarnings            FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ERR_ClearWarnings:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../error.c",line 277,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |277| 
        MOVW      DP,#_ODV_ErrorDsp_WarningNumber ; [CPU_U] 
        MOVL      @_ODV_ErrorDsp_WarningNumber,ACC ; [CPU_] |277| 
	.dwpsn	file "../error.c",line 278,column 3,is_stmt
        MOVW      DP,#_ODV_Statusword   ; [CPU_U] 
        AND       @_ODV_Statusword,#0xff7f ; [CPU_] |278| 
	.dwpsn	file "../error.c",line 279,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode+1 ; [CPU_U] 
        AND       @_ODV_Gateway_Errorcode+1,#65280 ; [CPU_] |279| 
	.dwpsn	file "../error.c",line 280,column 3,is_stmt
	.dwpsn	file "../error.c",line 281,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |281| 
	.dwpsn	file "../error.c",line 282,column 1,is_stmt
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$76, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$76, DW_AT_TI_end_line(0x11a)
	.dwattr $C$DW$76, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$76

	.sect	".text"
	.clink
	.global	_ERR_ClearWarning

$C$DW$78	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarning")
	.dwattr $C$DW$78, DW_AT_low_pc(_ERR_ClearWarning)
	.dwattr $C$DW$78, DW_AT_high_pc(0x00)
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_ERR_ClearWarning")
	.dwattr $C$DW$78, DW_AT_external
	.dwattr $C$DW$78, DW_AT_TI_begin_file("../error.c")
	.dwattr $C$DW$78, DW_AT_TI_begin_line(0x11c)
	.dwattr $C$DW$78, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$78, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../error.c",line 284,column 40,is_stmt,address _ERR_ClearWarning

	.dwfde $C$DW$CIE, _ERR_ClearWarning
$C$DW$79	.dwtag  DW_TAG_formal_parameter, DW_AT_name("warningno")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_warningno")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _ERR_ClearWarning             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_ERR_ClearWarning:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("warningno")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_warningno")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |284| 
	.dwpsn	file "../error.c",line 285,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_WarningNumber ; [CPU_U] 
        MOVL      ACC,*-SP[2]           ; [CPU_] |285| 
        NOT       ACC                   ; [CPU_] |285| 
        AND       @_ODV_ErrorDsp_WarningNumber,AL ; [CPU_] |285| 
        AND       @_ODV_ErrorDsp_WarningNumber+1,AH ; [CPU_] |285| 
	.dwpsn	file "../error.c",line 286,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVL      ACC,*-SP[2]           ; [CPU_] |286| 
        NOT       ACC                   ; [CPU_] |286| 
        AND       @_ODV_Gateway_Errorcode,AL ; [CPU_] |286| 
        AND       @_ODV_Gateway_Errorcode+1,AH ; [CPU_] |286| 
	.dwpsn	file "../error.c",line 287,column 3,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_WarningNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_WarningNumber ; [CPU_] |287| 
        BF        $C$L26,NEQ            ; [CPU_] |287| 
        ; branchcc occurs ; [] |287| 
	.dwpsn	file "../error.c",line 287,column 40,is_stmt
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_name("_ERR_ClearWarnings")
	.dwattr $C$DW$81, DW_AT_TI_call
        LCR       #_ERR_ClearWarnings   ; [CPU_] |287| 
        ; call occurs [#_ERR_ClearWarnings] ; [] |287| 
	.dwpsn	file "../error.c",line 288,column 1,is_stmt
$C$L26:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$78, DW_AT_TI_end_file("../error.c")
	.dwattr $C$DW$78, DW_AT_TI_end_line(0x120)
	.dwattr $C$DW$78, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$78

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_ODV_ChargerData_ChargerError
	.global	_ODV_ErrorDsp_ErrorLevel
	.global	_ODV_ChargerData_MasterErrorByte
	.global	_ODV_Gateway_State
	.global	_ODV_MachineMode
	.global	_ODV_Statusword
	.global	_ODV_Controlword
	.global	_ODV_ChargerData_SlaveErrorByte
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_ODV_Gateway_Errorcode
	.global	_ODV_ErrorDsp_WarningNumber
	.global	_GpioDataRegs

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$83, DW_AT_name("cob_id")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$84, DW_AT_name("rtr")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$85, DW_AT_name("len")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$86	.dwtag  DW_TAG_member
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$86, DW_AT_name("data")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$87	.dwtag  DW_TAG_member
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$87, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$88, DW_AT_name("csSDO")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$88, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$89, DW_AT_name("csEmergency")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$90, DW_AT_name("csSYNC")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$91, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$92, DW_AT_name("csPDO")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$93, DW_AT_name("csLSS")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$94	.dwtag  DW_TAG_member
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$94, DW_AT_name("errCode")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$94, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$94, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$95, DW_AT_name("errRegMask")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_name("active")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$96, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)

$C$DW$T$99	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x18)
$C$DW$97	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$97, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$99


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$98, DW_AT_name("index")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$98, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$99, DW_AT_name("subindex")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$100, DW_AT_name("size")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$101, DW_AT_name("address")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x01)
$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$102, DW_AT_name("SwitchOn")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$102, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$103, DW_AT_name("EnableVolt")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$103, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$104, DW_AT_name("QuickStop")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$104, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$105	.dwtag  DW_TAG_member
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$105, DW_AT_name("EnableOperation")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$105, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$105, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_member
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$106, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$106, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$106, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$106, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$107, DW_AT_name("ResetFault")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$107, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$107, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$108, DW_AT_name("Halt")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$108, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$109, DW_AT_name("Oms")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$109, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$110, DW_AT_name("Rsvd")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$110, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$111, DW_AT_name("Manufacturer")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$111, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24


$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$112, DW_AT_name("SwitchOn")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$112, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$113, DW_AT_name("EnableVolt")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$113, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$114, DW_AT_name("QuickStop")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$114, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$115, DW_AT_name("EnableOperation")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$115, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$116, DW_AT_name("Rsvd0")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_Rsvd0")
	.dwattr $C$DW$116, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$117, DW_AT_name("ResetFault")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$117, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$118, DW_AT_name("Halt")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$118, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$119, DW_AT_name("Oms")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$119, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$120, DW_AT_name("Rsvd")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$120, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$121, DW_AT_name("Manufacturer")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$121, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$122, DW_AT_name("SwitchOn")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$122, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$123, DW_AT_name("EnableVolt")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$123, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$124, DW_AT_name("QuickStop")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$124, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$125, DW_AT_name("EnableOperation")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$125, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$126, DW_AT_name("NewSetPoint")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_NewSetPoint")
	.dwattr $C$DW$126, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$127, DW_AT_name("ChangeSetImm")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_ChangeSetImm")
	.dwattr $C$DW$127, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$128, DW_AT_name("Abs_Rel")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_Abs_Rel")
	.dwattr $C$DW$128, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$129, DW_AT_name("ResetFault")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$129, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$130, DW_AT_name("Halt")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$130, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$131, DW_AT_name("ChangeSetPos")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_ChangeSetPos")
	.dwattr $C$DW$131, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$132, DW_AT_name("Rsvd")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$132, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$133, DW_AT_name("Manufacturer")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$133, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$134, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$134, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$135, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$135, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$136, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$136, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$137, DW_AT_name("Fault")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$137, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$138, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$138, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$139, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$139, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$140, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$140, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$141, DW_AT_name("Warning")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$141, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$142, DW_AT_name("Manufacturer")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$142, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$143, DW_AT_name("Remote")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$143, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$144, DW_AT_name("TargetReached")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$144, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$145, DW_AT_name("InternalLimit")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$145, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$146, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$146, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x02)
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$147, DW_AT_name("Manufacturers")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$147, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x01)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$148, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$148, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$149, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$149, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$150, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$150, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$151, DW_AT_name("Fault")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$151, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$152, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$152, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$153, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$153, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$154, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$154, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$155, DW_AT_name("Warning")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$155, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$156, DW_AT_name("Manufacturer")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$156, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$157, DW_AT_name("Remote")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$157, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$158, DW_AT_name("TargetReached")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$158, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$159, DW_AT_name("InternalLimit")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$159, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$160, DW_AT_name("Speed")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_Speed")
	.dwattr $C$DW$160, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$161, DW_AT_name("MaxSlipError")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_MaxSlipError")
	.dwattr $C$DW$161, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$162, DW_AT_name("Manufacturers")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$162, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28


$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$163, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$163, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$164, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$164, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$165, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$165, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$166, DW_AT_name("Fault")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$166, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$167, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$167, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$168, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$168, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$169, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$169, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$170, DW_AT_name("Warning")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$170, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$171, DW_AT_name("Manufacturer")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$171, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$172, DW_AT_name("Remote")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$172, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$173, DW_AT_name("TargetReached")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$173, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$174, DW_AT_name("InternalLimit")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$174, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$175, DW_AT_name("SetPointAck")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_SetPointAck")
	.dwattr $C$DW$175, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$176, DW_AT_name("FollowError")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_FollowError")
	.dwattr $C$DW$176, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$177, DW_AT_name("Manufacturers")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$177, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x01)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$178, DW_AT_name("ReadyToSwitchOn")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_ReadyToSwitchOn")
	.dwattr $C$DW$178, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$179, DW_AT_name("SwitchedOn")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_SwitchedOn")
	.dwattr $C$DW$179, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$180, DW_AT_name("OperationEnabled")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_OperationEnabled")
	.dwattr $C$DW$180, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$181, DW_AT_name("Fault")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_Fault")
	.dwattr $C$DW$181, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$182, DW_AT_name("VoltageDisabled")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_VoltageDisabled")
	.dwattr $C$DW$182, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$183, DW_AT_name("NotQuickStop")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_NotQuickStop")
	.dwattr $C$DW$183, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$184, DW_AT_name("SwitchOnDisabled")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_SwitchOnDisabled")
	.dwattr $C$DW$184, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$185, DW_AT_name("Warning")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_Warning")
	.dwattr $C$DW$185, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$186, DW_AT_name("Manufacturer")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$186, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$187, DW_AT_name("Remote")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_Remote")
	.dwattr $C$DW$187, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$188, DW_AT_name("TargetReached")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_TargetReached")
	.dwattr $C$DW$188, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$189, DW_AT_name("InternalLimit")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_InternalLimit")
	.dwattr $C$DW$189, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$190, DW_AT_name("HomingAttained")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_HomingAttained")
	.dwattr $C$DW$190, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$191, DW_AT_name("HomingError")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_HomingError")
	.dwattr $C$DW$191, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$192, DW_AT_name("Manufacturers")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_Manufacturers")
	.dwattr $C$DW$192, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x02)
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$193, DW_AT_name("ControlWord")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_ControlWord")
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$194, DW_AT_name("AnyMode")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$195, DW_AT_name("VelocityMode")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$196, DW_AT_name("PositionMode")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31

$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("TControlword")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)

$C$DW$T$32	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x01)
$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$197, DW_AT_name("StatusWord")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_StatusWord")
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$198, DW_AT_name("AnyMode")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$199, DW_AT_name("VelocityMode")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$200, DW_AT_name("PositionMode")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$201, DW_AT_name("HomingMode")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_HomingMode")
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32

$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("TStatusword")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)

$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_name("AIODAT_BITS")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$202, DW_AT_name("rsvd1")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$202, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$203, DW_AT_name("rsvd2")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$203, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$204, DW_AT_name("AIO2")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$204, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$205	.dwtag  DW_TAG_member
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$205, DW_AT_name("rsvd3")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$205, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$205, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$205, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$206, DW_AT_name("AIO4")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$206, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$207, DW_AT_name("rsvd4")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$207, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$208, DW_AT_name("AIO6")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$208, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$209, DW_AT_name("rsvd5")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$209, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$210, DW_AT_name("rsvd6")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$210, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$211, DW_AT_name("rsvd7")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$211, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$212, DW_AT_name("AIO10")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$212, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$213, DW_AT_name("rsvd8")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$213, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$214, DW_AT_name("AIO12")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$214, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$215, DW_AT_name("rsvd9")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$215, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$216, DW_AT_name("AIO14")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$216, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$217, DW_AT_name("rsvd10")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$217, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$218, DW_AT_name("rsvd11")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$218, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$36	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$36, DW_AT_name("AIODAT_REG")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$219, DW_AT_name("all")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$220, DW_AT_name("bit")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$37, DW_AT_name("GPADAT_BITS")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x02)
$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$221, DW_AT_name("GPIO0")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$221, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$222, DW_AT_name("GPIO1")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$222, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$223, DW_AT_name("GPIO2")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$223, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$224	.dwtag  DW_TAG_member
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$224, DW_AT_name("GPIO3")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$224, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$224, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$225	.dwtag  DW_TAG_member
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$225, DW_AT_name("GPIO4")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$225, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$225, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$225, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$226, DW_AT_name("GPIO5")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$226, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$227, DW_AT_name("GPIO6")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$227, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$228, DW_AT_name("GPIO7")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$228, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$229, DW_AT_name("GPIO8")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$229, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$230, DW_AT_name("GPIO9")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$230, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$231, DW_AT_name("GPIO10")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$231, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$232, DW_AT_name("GPIO11")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$232, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$233, DW_AT_name("GPIO12")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$233, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$234, DW_AT_name("GPIO13")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$234, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$235, DW_AT_name("GPIO14")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$235, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$236, DW_AT_name("GPIO15")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$236, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$237, DW_AT_name("GPIO16")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$237, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$238, DW_AT_name("GPIO17")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$238, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$239, DW_AT_name("GPIO18")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$239, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$240, DW_AT_name("GPIO19")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$240, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$241, DW_AT_name("GPIO20")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$241, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$242, DW_AT_name("GPIO21")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$242, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$243, DW_AT_name("GPIO22")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$243, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$244, DW_AT_name("GPIO23")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$244, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$245, DW_AT_name("GPIO24")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$245, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$246, DW_AT_name("GPIO25")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$246, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$247, DW_AT_name("GPIO26")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$247, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$248, DW_AT_name("GPIO27")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$248, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$249, DW_AT_name("GPIO28")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$249, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$250, DW_AT_name("GPIO29")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$250, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$251, DW_AT_name("GPIO30")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$251, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$252, DW_AT_name("GPIO31")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$252, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37


$C$DW$T$38	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$38, DW_AT_name("GPADAT_REG")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x02)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$253, DW_AT_name("all")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$254, DW_AT_name("bit")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38


$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("GPBDAT_BITS")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x02)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$255, DW_AT_name("GPIO32")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$255, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$256, DW_AT_name("GPIO33")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$256, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$257, DW_AT_name("GPIO34")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$257, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$258, DW_AT_name("GPIO35")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$258, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$259, DW_AT_name("GPIO36")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$259, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$260, DW_AT_name("GPIO37")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$260, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$261, DW_AT_name("GPIO38")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$261, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$262, DW_AT_name("GPIO39")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$262, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$263, DW_AT_name("GPIO40")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$263, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$264, DW_AT_name("GPIO41")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$264, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$265, DW_AT_name("GPIO42")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$265, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$266, DW_AT_name("GPIO43")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$266, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$267, DW_AT_name("GPIO44")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$267, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$268, DW_AT_name("rsvd1")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$268, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$269, DW_AT_name("rsvd2")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$269, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$269, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$270, DW_AT_name("GPIO50")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$270, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$271, DW_AT_name("GPIO51")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$271, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$272	.dwtag  DW_TAG_member
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$272, DW_AT_name("GPIO52")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$272, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$272, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$272, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$273, DW_AT_name("GPIO53")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$273, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$274, DW_AT_name("GPIO54")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$274, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$275	.dwtag  DW_TAG_member
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$275, DW_AT_name("GPIO55")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$275, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$275, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$275, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$276	.dwtag  DW_TAG_member
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$276, DW_AT_name("GPIO56")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$276, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$276, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$276, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$277, DW_AT_name("GPIO57")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$277, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$277, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$278, DW_AT_name("GPIO58")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$278, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$279, DW_AT_name("rsvd3")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$279, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39


$C$DW$T$40	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$40, DW_AT_name("GPBDAT_REG")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x02)
$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$280, DW_AT_name("all")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$281, DW_AT_name("bit")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40


$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("GPIO_DATA_REGS")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x20)
$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$282, DW_AT_name("GPADAT")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_GPADAT")
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$283, DW_AT_name("GPASET")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_GPASET")
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$284, DW_AT_name("GPACLEAR")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_GPACLEAR")
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$285, DW_AT_name("GPATOGGLE")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_GPATOGGLE")
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$286, DW_AT_name("GPBDAT")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_GPBDAT")
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$287, DW_AT_name("GPBSET")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_GPBSET")
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$288, DW_AT_name("GPBCLEAR")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_GPBCLEAR")
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$289, DW_AT_name("GPBTOGGLE")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_GPBTOGGLE")
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$290, DW_AT_name("rsvd1")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$291, DW_AT_name("AIODAT")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_AIODAT")
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$292, DW_AT_name("AIOSET")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_AIOSET")
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$293, DW_AT_name("AIOCLEAR")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_AIOCLEAR")
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$294, DW_AT_name("AIOTOGGLE")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_AIOTOGGLE")
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42

$C$DW$295	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$42)
$C$DW$T$120	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$295)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)

$C$DW$T$67	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$67, DW_AT_language(DW_LANG_C)
$C$DW$296	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$66)
	.dwendtag $C$DW$T$67

$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x16)
$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$T$70	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$70, DW_AT_language(DW_LANG_C)
$C$DW$T$80	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$80, DW_AT_language(DW_LANG_C)
$C$DW$T$72	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)

$C$DW$T$76	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$297	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$66)
$C$DW$298	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$76

$C$DW$T$77	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x16)
$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$78	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_language(DW_LANG_C)
$C$DW$T$82	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$82, DW_AT_language(DW_LANG_C)

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$299	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$66)
$C$DW$300	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$6)
$C$DW$301	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$9)
$C$DW$302	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$100

$C$DW$T$101	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x16)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$83	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$83, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$303	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$303, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x16)
$C$DW$304	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$6)
$C$DW$T$55	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$304)
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x16)
$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$305	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$9)
$C$DW$T$53	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$305)
$C$DW$T$54	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x16)
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)

$C$DW$T$41	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x08)
$C$DW$306	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$306, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$41

$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$307	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$6)
$C$DW$308	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x16)
$C$DW$T$59	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_language(DW_LANG_C)
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x16)

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$84, DW_AT_language(DW_LANG_C)
$C$DW$309	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$66)
$C$DW$310	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$47)
$C$DW$311	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$6)
$C$DW$312	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$84

$C$DW$T$85	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_address_class(0x16)
$C$DW$T$86	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_language(DW_LANG_C)
$C$DW$313	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$86)
$C$DW$T$87	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$313)
$C$DW$T$88	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x16)
$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)

$C$DW$T$93	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)
$C$DW$314	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$66)
$C$DW$315	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$9)
$C$DW$316	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$93

$C$DW$T$94	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_address_class(0x16)
$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)

$C$DW$T$96	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$96, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x01)
$C$DW$317	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$318	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$96

$C$DW$T$97	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_language(DW_LANG_C)

$C$DW$T$62	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$62, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$319	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$320	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$321	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$322	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$323	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$324	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$325	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$326	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$62

$C$DW$T$63	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_language(DW_LANG_C)

$C$DW$T$79	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x80)
$C$DW$327	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$327, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$79


$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x06)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$328, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$329, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$330, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$331, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$332, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$333, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43

$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$334	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$50)
$C$DW$T$51	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$334)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)

$C$DW$T$106	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$106, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x132)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$335, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$336, DW_AT_name("objdict")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$337, DW_AT_name("PDO_status")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$338, DW_AT_name("firstIndex")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$339, DW_AT_name("lastIndex")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$340, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$341, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$342, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$343, DW_AT_name("transfers")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$344, DW_AT_name("nodeState")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$345, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$346, DW_AT_name("initialisation")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$347, DW_AT_name("preOperational")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$348, DW_AT_name("operational")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$349, DW_AT_name("stopped")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$350, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$351, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$352, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$353, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$354, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$355, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$356, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$357, DW_AT_name("heartbeatError")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$358, DW_AT_name("NMTable")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$359, DW_AT_name("syncTimer")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$360, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$361, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$362, DW_AT_name("pre_sync")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$363, DW_AT_name("post_TPDO")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$364, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$365, DW_AT_name("toggle")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$366, DW_AT_name("canHandle")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$367, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$368, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$369, DW_AT_name("globalCallback")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$370, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$371, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$372, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$373, DW_AT_name("dcf_request")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$374, DW_AT_name("error_state")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$375, DW_AT_name("error_history_size")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$376, DW_AT_name("error_number")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$377, DW_AT_name("error_first_element")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$378, DW_AT_name("error_register")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$379, DW_AT_name("error_cobid")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$380, DW_AT_name("error_data")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$381, DW_AT_name("post_emcy")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$382, DW_AT_name("lss_transfer")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$383, DW_AT_name("eeprom_index")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$384, DW_AT_name("eeprom_size")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)
$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x16)

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x0e)
$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$385, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$386, DW_AT_name("event_timer")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$387, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$388, DW_AT_name("last_message")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108

$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$49	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x16)

$C$DW$T$110	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$110, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x14)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$389, DW_AT_name("nodeId")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$390, DW_AT_name("whoami")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$391, DW_AT_name("state")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$392, DW_AT_name("toggle")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$393, DW_AT_name("abortCode")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$394, DW_AT_name("index")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$395, DW_AT_name("subIndex")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$396, DW_AT_name("port")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$397, DW_AT_name("count")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$398, DW_AT_name("offset")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$399, DW_AT_name("datap")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$400, DW_AT_name("dataType")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$401, DW_AT_name("timer")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$402, DW_AT_name("Callback")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110

$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)

$C$DW$T$61	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x3c)
$C$DW$403	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$403, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$61


$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x04)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$404, DW_AT_name("pSubindex")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$405, DW_AT_name("bSubCount")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$406, DW_AT_name("index")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$407	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$45)
$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$407)
$C$DW$T$47	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x16)

$C$DW$T$90	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$408	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$9)
$C$DW$409	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$73)
$C$DW$410	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$89)
	.dwendtag $C$DW$T$90

$C$DW$T$91	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x16)
$C$DW$T$92	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)

$C$DW$T$115	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$115, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x08)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$411, DW_AT_name("bAccessType")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$412, DW_AT_name("bDataType")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$413, DW_AT_name("size")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$414, DW_AT_name("pObject")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$415, DW_AT_name("bProcessor")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115

$C$DW$416	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$115)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$416)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)
$C$DW$T$113	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$417	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$417, DW_AT_location[DW_OP_reg0]
$C$DW$418	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$418, DW_AT_location[DW_OP_reg1]
$C$DW$419	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$419, DW_AT_location[DW_OP_reg2]
$C$DW$420	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$420, DW_AT_location[DW_OP_reg3]
$C$DW$421	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$421, DW_AT_location[DW_OP_reg20]
$C$DW$422	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg21]
$C$DW$423	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg22]
$C$DW$424	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$424, DW_AT_location[DW_OP_reg23]
$C$DW$425	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$425, DW_AT_location[DW_OP_reg24]
$C$DW$426	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$426, DW_AT_location[DW_OP_reg25]
$C$DW$427	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$427, DW_AT_location[DW_OP_reg26]
$C$DW$428	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$428, DW_AT_location[DW_OP_reg28]
$C$DW$429	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$429, DW_AT_location[DW_OP_reg29]
$C$DW$430	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$430, DW_AT_location[DW_OP_reg30]
$C$DW$431	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$431, DW_AT_location[DW_OP_reg31]
$C$DW$432	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$432, DW_AT_location[DW_OP_regx 0x20]
$C$DW$433	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$433, DW_AT_location[DW_OP_regx 0x21]
$C$DW$434	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$434, DW_AT_location[DW_OP_regx 0x22]
$C$DW$435	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$435, DW_AT_location[DW_OP_regx 0x23]
$C$DW$436	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$436, DW_AT_location[DW_OP_regx 0x24]
$C$DW$437	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$437, DW_AT_location[DW_OP_regx 0x25]
$C$DW$438	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$438, DW_AT_location[DW_OP_regx 0x26]
$C$DW$439	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$439, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$440	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$440, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$441	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg4]
$C$DW$442	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$442, DW_AT_location[DW_OP_reg6]
$C$DW$443	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$443, DW_AT_location[DW_OP_reg8]
$C$DW$444	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$444, DW_AT_location[DW_OP_reg10]
$C$DW$445	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$445, DW_AT_location[DW_OP_reg12]
$C$DW$446	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$446, DW_AT_location[DW_OP_reg14]
$C$DW$447	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$447, DW_AT_location[DW_OP_reg16]
$C$DW$448	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$448, DW_AT_location[DW_OP_reg17]
$C$DW$449	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$449, DW_AT_location[DW_OP_reg18]
$C$DW$450	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$450, DW_AT_location[DW_OP_reg19]
$C$DW$451	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$451, DW_AT_location[DW_OP_reg5]
$C$DW$452	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$452, DW_AT_location[DW_OP_reg7]
$C$DW$453	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$453, DW_AT_location[DW_OP_reg9]
$C$DW$454	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$454, DW_AT_location[DW_OP_reg11]
$C$DW$455	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$455, DW_AT_location[DW_OP_reg13]
$C$DW$456	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$456, DW_AT_location[DW_OP_reg15]
$C$DW$457	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$457, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$458	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$458, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$459	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$459, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$460	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$460, DW_AT_location[DW_OP_regx 0x30]
$C$DW$461	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$461, DW_AT_location[DW_OP_regx 0x33]
$C$DW$462	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$462, DW_AT_location[DW_OP_regx 0x34]
$C$DW$463	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$463, DW_AT_location[DW_OP_regx 0x37]
$C$DW$464	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$464, DW_AT_location[DW_OP_regx 0x38]
$C$DW$465	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$465, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$466	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$466, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$467	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$467, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$468	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$468, DW_AT_location[DW_OP_regx 0x40]
$C$DW$469	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$469, DW_AT_location[DW_OP_regx 0x43]
$C$DW$470	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$470, DW_AT_location[DW_OP_regx 0x44]
$C$DW$471	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$471, DW_AT_location[DW_OP_regx 0x47]
$C$DW$472	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$472, DW_AT_location[DW_OP_regx 0x48]
$C$DW$473	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$473, DW_AT_location[DW_OP_regx 0x49]
$C$DW$474	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$474, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$475	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$475, DW_AT_location[DW_OP_regx 0x27]
$C$DW$476	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$476, DW_AT_location[DW_OP_regx 0x28]
$C$DW$477	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$477, DW_AT_location[DW_OP_reg27]
$C$DW$478	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$478, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

