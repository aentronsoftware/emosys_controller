;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0842812 
	.sect	".text"
	.clink
	.global	_MemCfg_lockConfig

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_lockConfig")
	.dwattr $C$DW$3, DW_AT_low_pc(_MemCfg_lockConfig)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_MemCfg_lockConfig")
	.dwattr $C$DW$3, DW_AT_external
	.dwattr $C$DW$3, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x34)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 53,column 1,is_stmt,address _MemCfg_lockConfig

	.dwfde $C$DW$CIE, _MemCfg_lockConfig
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("memSections")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_memSections")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_lockConfig            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_MemCfg_lockConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("memSections")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_memSections")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |53| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 65,column 5,is_stmt
$C$DW$6	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$6, DW_AT_low_pc(0x00)
	.dwattr $C$DW$6, DW_AT_name("___eallow")
	.dwattr $C$DW$6, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |65| 
        ; call occurs [#___eallow] ; [] |65| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 67,column 5,is_stmt
        B         $C$L5,UNC             ; [CPU_] |67| 
        ; branch occurs ; [] |67| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 70,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |70| 
        MOVL      XAR4,#390144          ; [CPU_U] |70| 
        ANDB      AH,#255               ; [CPU_] |70| 
        OR        *+XAR4[0],AL          ; [CPU_] |70| 
        OR        *+XAR4[1],AH          ; [CPU_] |70| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 72,column 13,is_stmt
        B         $C$L6,UNC             ; [CPU_] |72| 
        ; branch occurs ; [] |72| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 75,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |75| 
        MOVL      XAR4,#390176          ; [CPU_U] |75| 
        ANDB      AH,#255               ; [CPU_] |75| 
        OR        *+XAR4[0],AL          ; [CPU_] |75| 
        OR        *+XAR4[1],AH          ; [CPU_] |75| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 77,column 13,is_stmt
        B         $C$L6,UNC             ; [CPU_] |77| 
        ; branch occurs ; [] |77| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 80,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |80| 
        MOVL      XAR4,#390208          ; [CPU_U] |80| 
        ANDB      AH,#255               ; [CPU_] |80| 
        OR        *+XAR4[0],AL          ; [CPU_] |80| 
        OR        *+XAR4[1],AH          ; [CPU_] |80| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 82,column 13,is_stmt
        B         $C$L6,UNC             ; [CPU_] |82| 
        ; branch occurs ; [] |82| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 88,column 13,is_stmt
        MOVL      XAR4,#390144          ; [CPU_U] |88| 
        OR        *+XAR4[0],#15         ; [CPU_] |88| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 90,column 13,is_stmt
        MOVL      XAR4,#390176          ; [CPU_U] |90| 
        OR        *+XAR4[0],#63         ; [CPU_] |90| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 92,column 13,is_stmt
        MOVL      XAR4,#390208          ; [CPU_U] |92| 
        OR        *+XAR4[0],#65535      ; [CPU_] |92| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 94,column 13,is_stmt
        B         $C$L6,UNC             ; [CPU_] |94| 
        ; branch occurs ; [] |94| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 67,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |67| 
        SETC      SXM                   ; [CPU_] 
        AND       ACC,#65280 << 16      ; [CPU_] |67| 
        MOVL      XAR6,ACC              ; [CPU_] |67| 
        MOV       ACC,#-512 << 15       ; [CPU_] |67| 
        CMPL      ACC,XAR6              ; [CPU_] |67| 
        BF        $C$L4,EQ              ; [CPU_] |67| 
        ; branchcc occurs ; [] |67| 
        MOVB      ACC,#0                ; [CPU_] |67| 
        CMPL      ACC,XAR6              ; [CPU_] |67| 
        BF        $C$L1,EQ              ; [CPU_] |67| 
        ; branchcc occurs ; [] |67| 
        MOV       ACC,#512 << 15        ; [CPU_] |67| 
        CMPL      ACC,XAR6              ; [CPU_] |67| 
        BF        $C$L2,EQ              ; [CPU_] |67| 
        ; branchcc occurs ; [] |67| 
        MOV       ACC,#1024 << 15       ; [CPU_] |67| 
        CMPL      ACC,XAR6              ; [CPU_] |67| 
        BF        $C$L3,EQ              ; [CPU_] |67| 
        ; branchcc occurs ; [] |67| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 104,column 5,is_stmt
$C$DW$7	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$7, DW_AT_low_pc(0x00)
	.dwattr $C$DW$7, DW_AT_name("___edis")
	.dwattr $C$DW$7, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |104| 
        ; call occurs [#___edis] ; [] |104| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 105,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x69)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink
	.global	_MemCfg_unlockConfig

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_unlockConfig")
	.dwattr $C$DW$9, DW_AT_low_pc(_MemCfg_unlockConfig)
	.dwattr $C$DW$9, DW_AT_high_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_MemCfg_unlockConfig")
	.dwattr $C$DW$9, DW_AT_external
	.dwattr $C$DW$9, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$9, DW_AT_TI_begin_line(0x71)
	.dwattr $C$DW$9, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$9, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 114,column 1,is_stmt,address _MemCfg_unlockConfig

	.dwfde $C$DW$CIE, _MemCfg_unlockConfig
$C$DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("memSections")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_memSections")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_unlockConfig          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_MemCfg_unlockConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("memSections")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_memSections")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |114| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 127,column 5,is_stmt
$C$DW$12	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$12, DW_AT_low_pc(0x00)
	.dwattr $C$DW$12, DW_AT_name("___eallow")
	.dwattr $C$DW$12, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |127| 
        ; call occurs [#___eallow] ; [] |127| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 129,column 5,is_stmt
        B         $C$L11,UNC            ; [CPU_] |129| 
        ; branch occurs ; [] |129| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 132,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |132| 
        MOVL      XAR4,#390144          ; [CPU_U] |132| 
        ANDB      AH,#255               ; [CPU_] |132| 
        NOT       ACC                   ; [CPU_] |132| 
        AND       *+XAR4[0],AL          ; [CPU_] |132| 
        AND       *+XAR4[1],AH          ; [CPU_] |132| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 134,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |134| 
        ; branch occurs ; [] |134| 
$C$L8:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 137,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |137| 
        MOVL      XAR4,#390176          ; [CPU_U] |137| 
        ANDB      AH,#255               ; [CPU_] |137| 
        NOT       ACC                   ; [CPU_] |137| 
        AND       *+XAR4[0],AL          ; [CPU_] |137| 
        AND       *+XAR4[1],AH          ; [CPU_] |137| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 139,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |139| 
        ; branch occurs ; [] |139| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 142,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |142| 
        MOVL      XAR4,#390208          ; [CPU_U] |142| 
        ANDB      AH,#255               ; [CPU_] |142| 
        NOT       ACC                   ; [CPU_] |142| 
        AND       *+XAR4[0],AL          ; [CPU_] |142| 
        AND       *+XAR4[1],AH          ; [CPU_] |142| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 144,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |144| 
        ; branch occurs ; [] |144| 
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 151,column 13,is_stmt
        MOVL      XAR4,#390144          ; [CPU_U] |151| 
        AND       *+XAR4[0],#65520      ; [CPU_] |151| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 153,column 13,is_stmt
        MOVL      XAR4,#390176          ; [CPU_U] |153| 
        AND       *+XAR4[0],#65472      ; [CPU_] |153| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 155,column 13,is_stmt
        MOVL      XAR4,#390208          ; [CPU_U] |155| 
        AND       *+XAR4[0],#0          ; [CPU_] |155| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 157,column 13,is_stmt
        B         $C$L12,UNC            ; [CPU_] |157| 
        ; branch occurs ; [] |157| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 129,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |129| 
        SETC      SXM                   ; [CPU_] 
        AND       ACC,#65280 << 16      ; [CPU_] |129| 
        MOVL      XAR6,ACC              ; [CPU_] |129| 
        MOV       ACC,#-512 << 15       ; [CPU_] |129| 
        CMPL      ACC,XAR6              ; [CPU_] |129| 
        BF        $C$L10,EQ             ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        MOVB      ACC,#0                ; [CPU_] |129| 
        CMPL      ACC,XAR6              ; [CPU_] |129| 
        BF        $C$L7,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        MOV       ACC,#512 << 15        ; [CPU_] |129| 
        CMPL      ACC,XAR6              ; [CPU_] |129| 
        BF        $C$L8,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        MOV       ACC,#1024 << 15       ; [CPU_] |129| 
        CMPL      ACC,XAR6              ; [CPU_] |129| 
        BF        $C$L9,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 167,column 5,is_stmt
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_name("___edis")
	.dwattr $C$DW$13, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |167| 
        ; call occurs [#___edis] ; [] |167| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 168,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$9, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$9, DW_AT_TI_end_line(0xa8)
	.dwattr $C$DW$9, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$9

	.sect	".text"
	.clink
	.global	_MemCfg_commitConfig

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_commitConfig")
	.dwattr $C$DW$15, DW_AT_low_pc(_MemCfg_commitConfig)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_MemCfg_commitConfig")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0xb0)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 177,column 1,is_stmt,address _MemCfg_commitConfig

	.dwfde $C$DW$CIE, _MemCfg_commitConfig
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("memSections")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_memSections")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_commitConfig          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_MemCfg_commitConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("memSections")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_memSections")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |177| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 190,column 5,is_stmt
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("___eallow")
	.dwattr $C$DW$18, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |190| 
        ; call occurs [#___eallow] ; [] |190| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 192,column 5,is_stmt
        B         $C$L17,UNC            ; [CPU_] |192| 
        ; branch occurs ; [] |192| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 195,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |195| 
        MOVL      XAR4,#390146          ; [CPU_U] |195| 
        ANDB      AH,#255               ; [CPU_] |195| 
        OR        *+XAR4[0],AL          ; [CPU_] |195| 
        OR        *+XAR4[1],AH          ; [CPU_] |195| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 197,column 13,is_stmt
        B         $C$L18,UNC            ; [CPU_] |197| 
        ; branch occurs ; [] |197| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 200,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |200| 
        MOVL      XAR4,#390178          ; [CPU_U] |200| 
        ANDB      AH,#255               ; [CPU_] |200| 
        OR        *+XAR4[0],AL          ; [CPU_] |200| 
        OR        *+XAR4[1],AH          ; [CPU_] |200| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 202,column 13,is_stmt
        B         $C$L18,UNC            ; [CPU_] |202| 
        ; branch occurs ; [] |202| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 205,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |205| 
        MOVL      XAR4,#390210          ; [CPU_U] |205| 
        ANDB      AH,#255               ; [CPU_] |205| 
        OR        *+XAR4[0],AL          ; [CPU_] |205| 
        OR        *+XAR4[1],AH          ; [CPU_] |205| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 207,column 13,is_stmt
        B         $C$L18,UNC            ; [CPU_] |207| 
        ; branch occurs ; [] |207| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 214,column 13,is_stmt
        MOVL      XAR4,#390146          ; [CPU_U] |214| 
        OR        *+XAR4[0],#15         ; [CPU_] |214| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 216,column 13,is_stmt
        MOVL      XAR4,#390178          ; [CPU_U] |216| 
        OR        *+XAR4[0],#63         ; [CPU_] |216| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 218,column 13,is_stmt
        MOVL      XAR4,#390210          ; [CPU_U] |218| 
        OR        *+XAR4[0],#65535      ; [CPU_] |218| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 220,column 13,is_stmt
        B         $C$L18,UNC            ; [CPU_] |220| 
        ; branch occurs ; [] |220| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 192,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |192| 
        SETC      SXM                   ; [CPU_] 
        AND       ACC,#65280 << 16      ; [CPU_] |192| 
        MOVL      XAR6,ACC              ; [CPU_] |192| 
        MOV       ACC,#-512 << 15       ; [CPU_] |192| 
        CMPL      ACC,XAR6              ; [CPU_] |192| 
        BF        $C$L16,EQ             ; [CPU_] |192| 
        ; branchcc occurs ; [] |192| 
        MOVB      ACC,#0                ; [CPU_] |192| 
        CMPL      ACC,XAR6              ; [CPU_] |192| 
        BF        $C$L13,EQ             ; [CPU_] |192| 
        ; branchcc occurs ; [] |192| 
        MOV       ACC,#512 << 15        ; [CPU_] |192| 
        CMPL      ACC,XAR6              ; [CPU_] |192| 
        BF        $C$L14,EQ             ; [CPU_] |192| 
        ; branchcc occurs ; [] |192| 
        MOV       ACC,#1024 << 15       ; [CPU_] |192| 
        CMPL      ACC,XAR6              ; [CPU_] |192| 
        BF        $C$L15,EQ             ; [CPU_] |192| 
        ; branchcc occurs ; [] |192| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 230,column 5,is_stmt
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("___edis")
	.dwattr $C$DW$19, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |230| 
        ; call occurs [#___edis] ; [] |230| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 231,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$15, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0xe7)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

	.sect	".text"
	.clink
	.global	_MemCfg_setProtection

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_setProtection")
	.dwattr $C$DW$21, DW_AT_low_pc(_MemCfg_setProtection)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_MemCfg_setProtection")
	.dwattr $C$DW$21, DW_AT_external
	.dwattr $C$DW$21, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0xef)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 240,column 1,is_stmt,address _MemCfg_setProtection

	.dwfde $C$DW$CIE, _MemCfg_setProtection
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("memSection")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_memSection")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg0]
$C$DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_name("protectMode")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_protectMode")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -16]

;***************************************************************
;* FNAME: _MemCfg_setProtection         FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_MemCfg_setProtection:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("memSection")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_memSection")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -2]
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("shiftVal")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_shiftVal")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -4]
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("maskVal")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_maskVal")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -6]
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("regVal")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_regVal")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -8]
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("sectionNum")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_sectionNum")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -10]
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("regOffset")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_regOffset")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -12]
        MOVL      *-SP[2],ACC           ; [CPU_] |240| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 241,column 23,is_stmt
        MOVB      ACC,#0                ; [CPU_] |241| 
        MOVL      *-SP[4],ACC           ; [CPU_] |241| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 259,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |259| 
        ANDB      AH,#255               ; [CPU_] |259| 
        MOVL      *-SP[10],ACC          ; [CPU_] |259| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 261,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |261| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |261| 
        BF        $C$L20,EQ             ; [CPU_] |261| 
        ; branchcc occurs ; [] |261| 
$C$L19:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 263,column 9,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |263| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |263| 
        MOVL      *-SP[10],ACC          ; [CPU_] |263| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 264,column 9,is_stmt
        MOVB      ACC,#8                ; [CPU_] |264| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |264| 
        MOVL      *-SP[4],ACC           ; [CPU_] |264| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 261,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |261| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |261| 
        BF        $C$L19,NEQ            ; [CPU_] |261| 
        ; branchcc occurs ; [] |261| 
$C$L20:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 271,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#65504            ; [CPU_] |271| 
        AND       AL,*-SP[4]            ; [CPU_] |271| 
        AND       AH,*-SP[3]            ; [CPU_] |271| 
        SFR       ACC,4                 ; [CPU_] |271| 
        MOVL      *-SP[12],ACC          ; [CPU_] |271| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 272,column 5,is_stmt
        MOVB      ACC,#31               ; [CPU_] |272| 
        AND       *-SP[4],AL            ; [CPU_] |272| 
        AND       *-SP[3],AH            ; [CPU_] |272| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 273,column 5,is_stmt
        MOV       T,*-SP[4]             ; [CPU_] |273| 
        MOVB      ACC,#7                ; [CPU_] |273| 
        LSLL      ACC,T                 ; [CPU_] |273| 
        MOVL      *-SP[6],ACC           ; [CPU_] |273| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 274,column 5,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |274| 
        LSLL      ACC,T                 ; [CPU_] |274| 
        MOVL      *-SP[8],ACC           ; [CPU_] |274| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 279,column 5,is_stmt
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("___eallow")
	.dwattr $C$DW$30, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |279| 
        ; call occurs [#___eallow] ; [] |279| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 281,column 5,is_stmt
        B         $C$L24,UNC            ; [CPU_] |281| 
        ; branch occurs ; [] |281| 
$C$L21:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 284,column 13,is_stmt
        MOVL      XAR4,#390152          ; [CPU_U] |284| 
        MOVL      ACC,XAR4              ; [CPU_] |284| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |284| 
        MOVL      XAR4,ACC              ; [CPU_] |284| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |284| 
        NOT       ACC                   ; [CPU_] |284| 
        AND       *+XAR4[0],AL          ; [CPU_] |284| 
        AND       *+XAR4[1],AH          ; [CPU_] |284| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 285,column 13,is_stmt
        MOVL      XAR4,#390152          ; [CPU_U] |285| 
        MOVL      ACC,XAR4              ; [CPU_] |285| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |285| 
        MOVL      XAR4,ACC              ; [CPU_] |285| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |285| 
        OR        *+XAR4[0],AL          ; [CPU_] |285| 
        OR        *+XAR4[1],AH          ; [CPU_] |285| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 286,column 13,is_stmt
        B         $C$L25,UNC            ; [CPU_] |286| 
        ; branch occurs ; [] |286| 
$C$L22:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 289,column 13,is_stmt
        MOVL      XAR4,#390184          ; [CPU_U] |289| 
        MOVL      ACC,XAR4              ; [CPU_] |289| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |289| 
        MOVL      XAR4,ACC              ; [CPU_] |289| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |289| 
        NOT       ACC                   ; [CPU_] |289| 
        AND       *+XAR4[0],AL          ; [CPU_] |289| 
        AND       *+XAR4[1],AH          ; [CPU_] |289| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 290,column 13,is_stmt
        MOVL      XAR4,#390184          ; [CPU_U] |290| 
        MOVL      ACC,XAR4              ; [CPU_] |290| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |290| 
        MOVL      XAR4,ACC              ; [CPU_] |290| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |290| 
        OR        *+XAR4[0],AL          ; [CPU_] |290| 
        OR        *+XAR4[1],AH          ; [CPU_] |290| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 291,column 13,is_stmt
        B         $C$L25,UNC            ; [CPU_] |291| 
        ; branch occurs ; [] |291| 
$C$L23:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 294,column 13,is_stmt
        MOVL      XAR4,#390216          ; [CPU_U] |294| 
        MOVL      ACC,XAR4              ; [CPU_] |294| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |294| 
        MOVL      XAR4,ACC              ; [CPU_] |294| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |294| 
        NOT       ACC                   ; [CPU_] |294| 
        AND       *+XAR4[0],AL          ; [CPU_] |294| 
        AND       *+XAR4[1],AH          ; [CPU_] |294| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 295,column 13,is_stmt
        MOVL      XAR4,#390216          ; [CPU_U] |295| 
        MOVL      ACC,XAR4              ; [CPU_] |295| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |295| 
        MOVL      XAR4,ACC              ; [CPU_] |295| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |295| 
        OR        *+XAR4[0],AL          ; [CPU_] |295| 
        OR        *+XAR4[1],AH          ; [CPU_] |295| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 296,column 13,is_stmt
        B         $C$L25,UNC            ; [CPU_] |296| 
        ; branch occurs ; [] |296| 
$C$L24:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 281,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |281| 
        AND       ACC,#65280 << 16      ; [CPU_] |281| 
        MOVL      XAR6,ACC              ; [CPU_] |281| 
        MOVB      ACC,#0                ; [CPU_] |281| 
        CMPL      ACC,XAR6              ; [CPU_] |281| 
        BF        $C$L21,EQ             ; [CPU_] |281| 
        ; branchcc occurs ; [] |281| 
        MOV       ACC,#512 << 15        ; [CPU_] |281| 
        CMPL      ACC,XAR6              ; [CPU_] |281| 
        BF        $C$L22,EQ             ; [CPU_] |281| 
        ; branchcc occurs ; [] |281| 
        MOV       ACC,#1024 << 15       ; [CPU_] |281| 
        CMPL      ACC,XAR6              ; [CPU_] |281| 
        BF        $C$L23,EQ             ; [CPU_] |281| 
        ; branchcc occurs ; [] |281| 
$C$L25:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 306,column 5,is_stmt
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_name("___edis")
	.dwattr $C$DW$31, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |306| 
        ; call occurs [#___edis] ; [] |306| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 307,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$21, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x133)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$21

	.sect	".text"
	.clink
	.global	_MemCfg_setLSRAMMasterSel

$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_setLSRAMMasterSel")
	.dwattr $C$DW$33, DW_AT_low_pc(_MemCfg_setLSRAMMasterSel)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_MemCfg_setLSRAMMasterSel")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0x13b)
	.dwattr $C$DW$33, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 316,column 1,is_stmt,address _MemCfg_setLSRAMMasterSel

	.dwfde $C$DW$CIE, _MemCfg_setLSRAMMasterSel
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ramSection")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ramSection")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg0]
$C$DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_name("masterSel")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_masterSel")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _MemCfg_setLSRAMMasterSel     FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_MemCfg_setLSRAMMasterSel:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ramSection")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ramSection")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -2]
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("masterSel")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_masterSel")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -3]
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("shiftVal")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_shiftVal")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -6]
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[3],AR4           ; [CPU_] |316| 
        MOVL      *-SP[2],ACC           ; [CPU_] |316| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 330,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |330| 
        MOVL      *-SP[6],ACC           ; [CPU_] |330| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 331,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |331| 
        ANDB      AH,#255               ; [CPU_] |331| 
        MOVL      *-SP[8],ACC           ; [CPU_] |331| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 333,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |333| 
        CMPL      ACC,*-SP[8]           ; [CPU_] |333| 
        BF        $C$L27,EQ             ; [CPU_] |333| 
        ; branchcc occurs ; [] |333| 
$C$L26:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 335,column 9,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |335| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |335| 
        MOVL      *-SP[8],ACC           ; [CPU_] |335| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 336,column 9,is_stmt
        MOVB      ACC,#2                ; [CPU_] |336| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |336| 
        MOVL      *-SP[6],ACC           ; [CPU_] |336| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 333,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |333| 
        CMPL      ACC,*-SP[8]           ; [CPU_] |333| 
        BF        $C$L26,NEQ            ; [CPU_] |333| 
        ; branchcc occurs ; [] |333| 
$C$L27:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 342,column 5,is_stmt
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("___eallow")
	.dwattr $C$DW$40, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |342| 
        ; call occurs [#___eallow] ; [] |342| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 344,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       T,*-SP[6]             ; [CPU_] |344| 
        MOVL      XAR4,#390180          ; [CPU_U] |344| 
        MOV       ACC,*-SP[3]           ; [CPU_] |344| 
        LSLL      ACC,T                 ; [CPU_] |344| 
        MOVL      P,ACC                 ; [CPU_] |344| 
        MOVB      AL,#3                 ; [CPU_] |344| 
        LSL       AL,T                  ; [CPU_] |344| 
        NOT       AL                    ; [CPU_] |344| 
        MOVZ      AR6,AL                ; [CPU_] |344| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |344| 
        AND       ACC,AR6               ; [CPU_] |344| 
        OR        AL,PL                 ; [CPU_] |344| 
        OR        AH,PH                 ; [CPU_] |344| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |344| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 349,column 5,is_stmt
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("___edis")
	.dwattr $C$DW$41, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |349| 
        ; call occurs [#___edis] ; [] |349| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 350,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$33, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x15e)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text"
	.clink
	.global	_MemCfg_setTestMode

$C$DW$43	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_setTestMode")
	.dwattr $C$DW$43, DW_AT_low_pc(_MemCfg_setTestMode)
	.dwattr $C$DW$43, DW_AT_high_pc(0x00)
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_MemCfg_setTestMode")
	.dwattr $C$DW$43, DW_AT_external
	.dwattr $C$DW$43, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$43, DW_AT_TI_begin_line(0x166)
	.dwattr $C$DW$43, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$43, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 359,column 1,is_stmt,address _MemCfg_setTestMode

	.dwfde $C$DW$CIE, _MemCfg_setTestMode
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("memSection")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_memSection")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg0]
$C$DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_name("testMode")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_testMode")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _MemCfg_setTestMode           FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_MemCfg_setTestMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("memSection")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_memSection")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -2]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("testMode")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_testMode")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -3]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("shiftVal")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_shiftVal")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -6]
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("maskVal")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_maskVal")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -8]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("regVal")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_regVal")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -10]
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("sectionNum")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_sectionNum")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -12]
        MOV       *-SP[3],AR4           ; [CPU_] |359| 
        MOVL      *-SP[2],ACC           ; [CPU_] |359| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 360,column 23,is_stmt
        MOVB      ACC,#0                ; [CPU_] |360| 
        MOVL      *-SP[6],ACC           ; [CPU_] |360| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 378,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |378| 
        ANDB      AH,#255               ; [CPU_] |378| 
        MOVL      *-SP[12],ACC          ; [CPU_] |378| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 380,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |380| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |380| 
        BF        $C$L29,EQ             ; [CPU_] |380| 
        ; branchcc occurs ; [] |380| 
$C$L28:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 382,column 9,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |382| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |382| 
        MOVL      *-SP[12],ACC          ; [CPU_] |382| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 383,column 9,is_stmt
        MOVB      ACC,#2                ; [CPU_] |383| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |383| 
        MOVL      *-SP[6],ACC           ; [CPU_] |383| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 380,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |380| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |380| 
        BF        $C$L28,NEQ            ; [CPU_] |380| 
        ; branchcc occurs ; [] |380| 
$C$L29:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 386,column 5,is_stmt
        MOVB      ACC,#3                ; [CPU_] |386| 
        MOV       T,*-SP[6]             ; [CPU_] |386| 
        LSLL      ACC,T                 ; [CPU_] |386| 
        MOVL      *-SP[8],ACC           ; [CPU_] |386| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 387,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |387| 
        LSLL      ACC,T                 ; [CPU_] |387| 
        MOVL      *-SP[10],ACC          ; [CPU_] |387| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 392,column 5,is_stmt
$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x00)
	.dwattr $C$DW$52, DW_AT_name("___eallow")
	.dwattr $C$DW$52, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |392| 
        ; call occurs [#___eallow] ; [] |392| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 394,column 5,is_stmt
        B         $C$L34,UNC            ; [CPU_] |394| 
        ; branch occurs ; [] |394| 
$C$L30:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 397,column 13,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |397| 
        MOVL      XAR4,#390160          ; [CPU_U] |397| 
        NOT       ACC                   ; [CPU_] |397| 
        AND       *+XAR4[0],AL          ; [CPU_] |397| 
        AND       *+XAR4[1],AH          ; [CPU_] |397| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 398,column 13,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |398| 
        OR        *+XAR4[0],AL          ; [CPU_] |398| 
        OR        *+XAR4[1],AH          ; [CPU_] |398| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 399,column 13,is_stmt
        B         $C$L35,UNC            ; [CPU_] |399| 
        ; branch occurs ; [] |399| 
$C$L31:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 402,column 13,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |402| 
        MOVL      XAR4,#390192          ; [CPU_U] |402| 
        NOT       ACC                   ; [CPU_] |402| 
        AND       *+XAR4[0],AL          ; [CPU_] |402| 
        AND       *+XAR4[1],AH          ; [CPU_] |402| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 403,column 13,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |403| 
        OR        *+XAR4[0],AL          ; [CPU_] |403| 
        OR        *+XAR4[1],AH          ; [CPU_] |403| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 404,column 13,is_stmt
        B         $C$L35,UNC            ; [CPU_] |404| 
        ; branch occurs ; [] |404| 
$C$L32:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 407,column 13,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |407| 
        MOVL      XAR4,#390224          ; [CPU_U] |407| 
        NOT       ACC                   ; [CPU_] |407| 
        AND       *+XAR4[0],AL          ; [CPU_] |407| 
        AND       *+XAR4[1],AH          ; [CPU_] |407| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 408,column 13,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |408| 
        OR        *+XAR4[0],AL          ; [CPU_] |408| 
        OR        *+XAR4[1],AH          ; [CPU_] |408| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 409,column 13,is_stmt
        B         $C$L35,UNC            ; [CPU_] |409| 
        ; branch occurs ; [] |409| 
$C$L33:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 412,column 13,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |412| 
        MOVL      XAR4,#390256          ; [CPU_U] |412| 
        NOT       ACC                   ; [CPU_] |412| 
        AND       *+XAR4[0],AL          ; [CPU_] |412| 
        AND       *+XAR4[1],AH          ; [CPU_] |412| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 413,column 13,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |413| 
        OR        *+XAR4[0],AL          ; [CPU_] |413| 
        OR        *+XAR4[1],AH          ; [CPU_] |413| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 414,column 13,is_stmt
        B         $C$L35,UNC            ; [CPU_] |414| 
        ; branch occurs ; [] |414| 
$C$L34:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 394,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |394| 
        AND       ACC,#65280 << 16      ; [CPU_] |394| 
        MOVL      XAR6,ACC              ; [CPU_] |394| 
        MOVB      ACC,#0                ; [CPU_] |394| 
        CMPL      ACC,XAR6              ; [CPU_] |394| 
        BF        $C$L30,EQ             ; [CPU_] |394| 
        ; branchcc occurs ; [] |394| 
        MOV       ACC,#512 << 15        ; [CPU_] |394| 
        CMPL      ACC,XAR6              ; [CPU_] |394| 
        BF        $C$L31,EQ             ; [CPU_] |394| 
        ; branchcc occurs ; [] |394| 
        MOV       ACC,#1024 << 15       ; [CPU_] |394| 
        CMPL      ACC,XAR6              ; [CPU_] |394| 
        BF        $C$L32,EQ             ; [CPU_] |394| 
        ; branchcc occurs ; [] |394| 
        MOV       ACC,#1536 << 15       ; [CPU_] |394| 
        CMPL      ACC,XAR6              ; [CPU_] |394| 
        BF        $C$L33,EQ             ; [CPU_] |394| 
        ; branchcc occurs ; [] |394| 
$C$L35:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 423,column 5,is_stmt
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("___edis")
	.dwattr $C$DW$53, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |423| 
        ; call occurs [#___edis] ; [] |423| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 424,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$43, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$43, DW_AT_TI_end_line(0x1a8)
	.dwattr $C$DW$43, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$43

	.sect	".text"
	.clink
	.global	_MemCfg_initSections

$C$DW$55	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_initSections")
	.dwattr $C$DW$55, DW_AT_low_pc(_MemCfg_initSections)
	.dwattr $C$DW$55, DW_AT_high_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_MemCfg_initSections")
	.dwattr $C$DW$55, DW_AT_external
	.dwattr $C$DW$55, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$55, DW_AT_TI_begin_line(0x1b0)
	.dwattr $C$DW$55, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$55, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 433,column 1,is_stmt,address _MemCfg_initSections

	.dwfde $C$DW$CIE, _MemCfg_initSections
$C$DW$56	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ramSections")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ramSections")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_initSections          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_MemCfg_initSections:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("ramSections")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ramSections")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |433| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 447,column 5,is_stmt
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_name("___eallow")
	.dwattr $C$DW$58, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |447| 
        ; call occurs [#___eallow] ; [] |447| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 449,column 5,is_stmt
        B         $C$L41,UNC            ; [CPU_] |449| 
        ; branch occurs ; [] |449| 
$C$L36:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 452,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |452| 
        MOVL      XAR4,#390162          ; [CPU_U] |452| 
        ANDB      AH,#255               ; [CPU_] |452| 
        OR        *+XAR4[0],AL          ; [CPU_] |452| 
        OR        *+XAR4[1],AH          ; [CPU_] |452| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 454,column 13,is_stmt
        B         $C$L43,UNC            ; [CPU_] |454| 
        ; branch occurs ; [] |454| 
$C$L37:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 457,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |457| 
        MOVL      XAR4,#390194          ; [CPU_U] |457| 
        ANDB      AH,#255               ; [CPU_] |457| 
        OR        *+XAR4[0],AL          ; [CPU_] |457| 
        OR        *+XAR4[1],AH          ; [CPU_] |457| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 459,column 13,is_stmt
        B         $C$L43,UNC            ; [CPU_] |459| 
        ; branch occurs ; [] |459| 
$C$L38:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 462,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |462| 
        MOVL      XAR4,#390226          ; [CPU_U] |462| 
        ANDB      AH,#255               ; [CPU_] |462| 
        OR        *+XAR4[0],AL          ; [CPU_] |462| 
        OR        *+XAR4[1],AH          ; [CPU_] |462| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 464,column 13,is_stmt
        B         $C$L43,UNC            ; [CPU_] |464| 
        ; branch occurs ; [] |464| 
$C$L39:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 467,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |467| 
        MOVL      XAR4,#390258          ; [CPU_U] |467| 
        ANDB      AH,#255               ; [CPU_] |467| 
        OR        *+XAR4[0],AL          ; [CPU_] |467| 
        OR        *+XAR4[1],AH          ; [CPU_] |467| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 469,column 13,is_stmt
        B         $C$L43,UNC            ; [CPU_] |469| 
        ; branch occurs ; [] |469| 
$C$L40:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 475,column 13,is_stmt
        MOVL      XAR4,#390162          ; [CPU_U] |475| 
        OR        *+XAR4[0],#15         ; [CPU_] |475| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 477,column 13,is_stmt
        MOVL      XAR4,#390194          ; [CPU_U] |477| 
        OR        *+XAR4[0],#63         ; [CPU_] |477| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 479,column 13,is_stmt
        MOVL      XAR4,#390226          ; [CPU_U] |479| 
        OR        *+XAR4[0],#65535      ; [CPU_] |479| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 481,column 13,is_stmt
        MOVL      XAR4,#390258          ; [CPU_U] |481| 
        OR        *+XAR4[0],#6          ; [CPU_] |481| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 483,column 13,is_stmt
        B         $C$L43,UNC            ; [CPU_] |483| 
        ; branch occurs ; [] |483| 
$C$L41:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 449,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |449| 
        AND       ACC,#65280 << 16      ; [CPU_] |449| 
        MOVL      XAR6,ACC              ; [CPU_] |449| 
        MOV       ACC,#512 << 15        ; [CPU_] |449| 
        CMPL      ACC,XAR6              ; [CPU_] |449| 
        B         $C$L42,LT             ; [CPU_] |449| 
        ; branchcc occurs ; [] |449| 
        CMPL      ACC,XAR6              ; [CPU_] |449| 
        BF        $C$L37,EQ             ; [CPU_] |449| 
        ; branchcc occurs ; [] |449| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,#-512 << 15       ; [CPU_] |449| 
        CMPL      ACC,XAR6              ; [CPU_] |449| 
        BF        $C$L40,EQ             ; [CPU_] |449| 
        ; branchcc occurs ; [] |449| 
        MOVB      ACC,#0                ; [CPU_] |449| 
        CMPL      ACC,XAR6              ; [CPU_] |449| 
        BF        $C$L36,EQ             ; [CPU_] |449| 
        ; branchcc occurs ; [] |449| 
        B         $C$L43,UNC            ; [CPU_] |449| 
        ; branch occurs ; [] |449| 
$C$L42:    
        MOV       ACC,#1024 << 15       ; [CPU_] |449| 
        CMPL      ACC,XAR6              ; [CPU_] |449| 
        BF        $C$L38,EQ             ; [CPU_] |449| 
        ; branchcc occurs ; [] |449| 
        MOV       ACC,#1536 << 15       ; [CPU_] |449| 
        CMPL      ACC,XAR6              ; [CPU_] |449| 
        BF        $C$L39,EQ             ; [CPU_] |449| 
        ; branchcc occurs ; [] |449| 
$C$L43:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 493,column 5,is_stmt
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_name("___edis")
	.dwattr $C$DW$59, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |493| 
        ; call occurs [#___edis] ; [] |493| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 494,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$55, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$55, DW_AT_TI_end_line(0x1ee)
	.dwattr $C$DW$55, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$55

	.sect	".text"
	.clink
	.global	_MemCfg_getInitStatus

$C$DW$61	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_getInitStatus")
	.dwattr $C$DW$61, DW_AT_low_pc(_MemCfg_getInitStatus)
	.dwattr $C$DW$61, DW_AT_high_pc(0x00)
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_MemCfg_getInitStatus")
	.dwattr $C$DW$61, DW_AT_external
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$61, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$61, DW_AT_TI_begin_line(0x1f6)
	.dwattr $C$DW$61, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$61, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 503,column 1,is_stmt,address _MemCfg_getInitStatus

	.dwfde $C$DW$CIE, _MemCfg_getInitStatus
$C$DW$62	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ramSections")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ramSections")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_getInitStatus         FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_MemCfg_getInitStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ramSections")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ramSections")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -2]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |503| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 518,column 5,is_stmt
        B         $C$L51,UNC            ; [CPU_] |518| 
        ; branch occurs ; [] |518| 
$C$L44:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 521,column 13,is_stmt
        MOVL      XAR4,#390164          ; [CPU_U] |521| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |521| 
        MOVL      *-SP[4],ACC           ; [CPU_] |521| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 522,column 13,is_stmt
        B         $C$L53,UNC            ; [CPU_] |522| 
        ; branch occurs ; [] |522| 
$C$L45:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 525,column 13,is_stmt
        MOVL      XAR4,#390196          ; [CPU_U] |525| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |525| 
        MOVL      *-SP[4],ACC           ; [CPU_] |525| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 526,column 13,is_stmt
        B         $C$L53,UNC            ; [CPU_] |526| 
        ; branch occurs ; [] |526| 
$C$L46:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 529,column 13,is_stmt
        MOVL      XAR4,#390228          ; [CPU_U] |529| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |529| 
        MOVL      *-SP[4],ACC           ; [CPU_] |529| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 530,column 13,is_stmt
        B         $C$L53,UNC            ; [CPU_] |530| 
        ; branch occurs ; [] |530| 
$C$L47:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 533,column 13,is_stmt
        MOVL      XAR4,#390260          ; [CPU_U] |533| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |533| 
        MOVL      *-SP[4],ACC           ; [CPU_] |533| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 534,column 13,is_stmt
        B         $C$L53,UNC            ; [CPU_] |534| 
        ; branch occurs ; [] |534| 
$C$L48:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 540,column 13,is_stmt
        MOVB      ACC,#15               ; [CPU_] |540| 
        MOVL      XAR4,#390164          ; [CPU_U] |540| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |540| 
        BF        $C$L49,NEQ            ; [CPU_] |540| 
        ; branchcc occurs ; [] |540| 
        MOV       AL,#63                ; [CPU_] |540| 
        MOV       AH,#256               ; [CPU_] |540| 
        MOVL      XAR4,#390196          ; [CPU_U] |540| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |540| 
        BF        $C$L49,NEQ            ; [CPU_] |540| 
        ; branchcc occurs ; [] |540| 
        MOV       AL,#65535             ; [CPU_] |540| 
        MOV       AH,#512               ; [CPU_] |540| 
        MOVL      XAR4,#390228          ; [CPU_U] |540| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |540| 
        BF        $C$L49,NEQ            ; [CPU_] |540| 
        ; branchcc occurs ; [] |540| 
        MOV       AL,#6                 ; [CPU_] |540| 
        MOV       AH,#768               ; [CPU_] |540| 
        MOVL      XAR4,#390260          ; [CPU_U] |540| 
        CMPL      ACC,*+XAR4[0]         ; [CPU_] |540| 
        BF        $C$L49,NEQ            ; [CPU_] |540| 
        ; branchcc occurs ; [] |540| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 549,column 17,is_stmt
        MOV       AL,#65535             ; [CPU_] |549| 
        MOV       AH,#255               ; [CPU_] |549| 
        MOVL      *-SP[4],ACC           ; [CPU_] |549| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 550,column 13,is_stmt
        B         $C$L53,UNC            ; [CPU_] |550| 
        ; branch occurs ; [] |550| 
$C$L49:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 553,column 17,is_stmt
        MOVB      ACC,#0                ; [CPU_] |553| 
        MOVL      *-SP[4],ACC           ; [CPU_] |553| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 555,column 13,is_stmt
        B         $C$L53,UNC            ; [CPU_] |555| 
        ; branch occurs ; [] |555| 
$C$L50:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 562,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |562| 
        MOVL      *-SP[4],ACC           ; [CPU_] |562| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 563,column 13,is_stmt
        B         $C$L53,UNC            ; [CPU_] |563| 
        ; branch occurs ; [] |563| 
$C$L51:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 518,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |518| 
        AND       ACC,#65280 << 16      ; [CPU_] |518| 
        MOVL      XAR6,ACC              ; [CPU_] |518| 
        MOV       ACC,#512 << 15        ; [CPU_] |518| 
        CMPL      ACC,XAR6              ; [CPU_] |518| 
        B         $C$L52,LT             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
        CMPL      ACC,XAR6              ; [CPU_] |518| 
        BF        $C$L45,EQ             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,#-512 << 15       ; [CPU_] |518| 
        CMPL      ACC,XAR6              ; [CPU_] |518| 
        BF        $C$L48,EQ             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
        MOVB      ACC,#0                ; [CPU_] |518| 
        CMPL      ACC,XAR6              ; [CPU_] |518| 
        BF        $C$L44,EQ             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
        B         $C$L50,UNC            ; [CPU_] |518| 
        ; branch occurs ; [] |518| 
$C$L52:    
        MOV       ACC,#1024 << 15       ; [CPU_] |518| 
        CMPL      ACC,XAR6              ; [CPU_] |518| 
        BF        $C$L46,EQ             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
        MOV       ACC,#1536 << 15       ; [CPU_] |518| 
        CMPL      ACC,XAR6              ; [CPU_] |518| 
        BF        $C$L47,EQ             ; [CPU_] |518| 
        ; branchcc occurs ; [] |518| 
        B         $C$L50,UNC            ; [CPU_] |518| 
        ; branch occurs ; [] |518| 
$C$L53:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 566,column 5,is_stmt
        MOVL      P,*-SP[2]             ; [CPU_] |566| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |566| 
        AND       PH,#255               ; [CPU_] |566| 
        AND       AL,*-SP[2]            ; [CPU_] |566| 
        AND       AH,*-SP[1]            ; [CPU_] |566| 
        MOVB      XAR6,#0               ; [CPU_] |566| 
        CMPL      ACC,P                 ; [CPU_] |566| 
        BF        $C$L54,NEQ            ; [CPU_] |566| 
        ; branchcc occurs ; [] |566| 
        MOVB      XAR6,#1               ; [CPU_] |566| 
$C$L54:    
        MOV       AL,AR6                ; [CPU_] |566| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 567,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$61, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$61, DW_AT_TI_end_line(0x237)
	.dwattr $C$DW$61, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$61

	.sect	".text"
	.clink
	.global	_MemCfg_getViolationAddress

$C$DW$66	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_getViolationAddress")
	.dwattr $C$DW$66, DW_AT_low_pc(_MemCfg_getViolationAddress)
	.dwattr $C$DW$66, DW_AT_high_pc(0x00)
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_MemCfg_getViolationAddress")
	.dwattr $C$DW$66, DW_AT_external
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$66, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$66, DW_AT_TI_begin_line(0x23f)
	.dwattr $C$DW$66, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$66, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 576,column 1,is_stmt,address _MemCfg_getViolationAddress

	.dwfde $C$DW$CIE, _MemCfg_getViolationAddress
$C$DW$67	.dwtag  DW_TAG_formal_parameter, DW_AT_name("intFlag")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_intFlag")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_getViolationAddress   FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MemCfg_getViolationAddress:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("intFlag")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_intFlag")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -2]
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -4]
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("stsNumber")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_stsNumber")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],ACC           ; [CPU_] |576| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 583,column 5,is_stmt
        MOVL      XAR4,#983040          ; [CPU_U] |583| 
        MOVB      XAR6,#0               ; [CPU_] |583| 
        MOVL      ACC,XAR4              ; [CPU_] |583| 
        AND       AL,*-SP[2]            ; [CPU_] |583| 
        AND       AH,*-SP[1]            ; [CPU_] |583| 
        CMPL      ACC,XAR6              ; [CPU_] |583| 
        BF        $C$L55,EQ             ; [CPU_] |583| 
        ; branchcc occurs ; [] |583| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 585,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |585| 
        MOVU      ACC,AH                ; [CPU_] |585| 
        MOVL      *-SP[6],ACC           ; [CPU_] |585| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 586,column 9,is_stmt
        MOVL      XAR4,#390376          ; [CPU_U] |586| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |586| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 587,column 5,is_stmt
        B         $C$L56,UNC            ; [CPU_] |587| 
        ; branch occurs ; [] |587| 
$C$L55:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 590,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |590| 
        MOVL      *-SP[6],ACC           ; [CPU_] |590| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 591,column 9,is_stmt
        MOVL      XAR4,#390344          ; [CPU_U] |591| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |591| 
$C$L56:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 594,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |594| 
        CMPL      ACC,*-SP[6]           ; [CPU_] |594| 
        B         $C$L58,HIS            ; [CPU_] |594| 
        ; branchcc occurs ; [] |594| 
$C$L57:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 596,column 9,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |596| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |596| 
        MOVL      *-SP[6],ACC           ; [CPU_] |596| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 597,column 9,is_stmt
        MOVB      ACC,#2                ; [CPU_] |597| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |597| 
        MOVL      *-SP[4],ACC           ; [CPU_] |597| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 594,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |594| 
        CMPL      ACC,*-SP[6]           ; [CPU_] |594| 
        B         $C$L57,LO             ; [CPU_] |594| 
        ; branchcc occurs ; [] |594| 
$C$L58:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 603,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |603| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |603| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 604,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$66, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$66, DW_AT_TI_end_line(0x25c)
	.dwattr $C$DW$66, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$66

	.sect	".text"
	.clink
	.global	_MemCfg_getCorrErrorAddress

$C$DW$72	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_getCorrErrorAddress")
	.dwattr $C$DW$72, DW_AT_low_pc(_MemCfg_getCorrErrorAddress)
	.dwattr $C$DW$72, DW_AT_high_pc(0x00)
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_MemCfg_getCorrErrorAddress")
	.dwattr $C$DW$72, DW_AT_external
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$72, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$72, DW_AT_TI_begin_line(0x264)
	.dwattr $C$DW$72, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$72, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 613,column 1,is_stmt,address _MemCfg_getCorrErrorAddress

	.dwfde $C$DW$CIE, _MemCfg_getCorrErrorAddress
$C$DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_name("stsFlag")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_stsFlag")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_getCorrErrorAddress   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_MemCfg_getCorrErrorAddress:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("stsFlag")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_stsFlag")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |613| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 617,column 5,is_stmt
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 631,column 5,is_stmt
        MOVL      XAR4,#390438          ; [CPU_U] |631| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |631| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 632,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$72, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$72, DW_AT_TI_end_line(0x278)
	.dwattr $C$DW$72, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$72

	.sect	".text"
	.clink
	.global	_MemCfg_getUncorrErrorAddress

$C$DW$76	.dwtag  DW_TAG_subprogram, DW_AT_name("MemCfg_getUncorrErrorAddress")
	.dwattr $C$DW$76, DW_AT_low_pc(_MemCfg_getUncorrErrorAddress)
	.dwattr $C$DW$76, DW_AT_high_pc(0x00)
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_MemCfg_getUncorrErrorAddress")
	.dwattr $C$DW$76, DW_AT_external
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$76, DW_AT_TI_begin_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$76, DW_AT_TI_begin_line(0x280)
	.dwattr $C$DW$76, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$76, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 641,column 1,is_stmt,address _MemCfg_getUncorrErrorAddress

	.dwfde $C$DW$CIE, _MemCfg_getUncorrErrorAddress
$C$DW$77	.dwtag  DW_TAG_formal_parameter, DW_AT_name("stsFlag")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_stsFlag")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _MemCfg_getUncorrErrorAddress FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MemCfg_getUncorrErrorAddress:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("stsFlag")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_stsFlag")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_breg20 -2]
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("address")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_breg20 -4]
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],ACC           ; [CPU_] |641| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 648,column 5,is_stmt
        MOVL      XAR4,#390406          ; [CPU_U] |648| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |648| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 650,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |650| 
        MOVL      *-SP[6],ACC           ; [CPU_] |650| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 652,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |652| 
        CMPL      ACC,*-SP[6]           ; [CPU_] |652| 
        B         $C$L60,HIS            ; [CPU_] |652| 
        ; branchcc occurs ; [] |652| 
$C$L59:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 654,column 9,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |654| 
        CLRC      SXM                   ; [CPU_] 
        SFR       ACC,1                 ; [CPU_] |654| 
        MOVL      *-SP[6],ACC           ; [CPU_] |654| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 655,column 9,is_stmt
        MOVB      ACC,#2                ; [CPU_] |655| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |655| 
        MOVL      *-SP[4],ACC           ; [CPU_] |655| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 652,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |652| 
        CMPL      ACC,*-SP[6]           ; [CPU_] |652| 
        B         $C$L59,LO             ; [CPU_] |652| 
        ; branchcc occurs ; [] |652| 
$C$L60:    
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 661,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |661| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |661| 
	.dwpsn	file "../ExtraData/driverlib2/memcfg.c",line 662,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$81	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$81, DW_AT_low_pc(0x00)
	.dwattr $C$DW$81, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$76, DW_AT_TI_end_file("../ExtraData/driverlib2/memcfg.c")
	.dwattr $C$DW$76, DW_AT_TI_end_line(0x296)
	.dwattr $C$DW$76, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$76

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$82	.dwtag  DW_TAG_enumerator, DW_AT_name("MEMCFG_LSRAMMASTER_CPU_ONLY"), DW_AT_const_value(0x00)
$C$DW$83	.dwtag  DW_TAG_enumerator, DW_AT_name("MEMCFG_LSRAMMASTER_CPU_CLA1"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("MemCfg_LSRAMMasterSel")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$84	.dwtag  DW_TAG_enumerator, DW_AT_name("MEMCFG_TEST_FUNCTIONAL"), DW_AT_const_value(0x00)
$C$DW$85	.dwtag  DW_TAG_enumerator, DW_AT_name("MEMCFG_TEST_WRITE_DATA"), DW_AT_const_value(0x01)
$C$DW$86	.dwtag  DW_TAG_enumerator, DW_AT_name("MEMCFG_TEST_WRITE_ECC"), DW_AT_const_value(0x02)
$C$DW$87	.dwtag  DW_TAG_enumerator, DW_AT_name("MEMCFG_TEST_WRITE_PARITY"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("MemCfg_TestMode")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg0]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg1]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg2]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg3]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg20]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg21]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_reg22]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_reg23]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg24]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg25]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_reg26]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_reg28]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_reg29]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_reg30]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg31]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x20]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x21]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x22]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x23]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x24]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x25]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x26]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg4]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg6]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg8]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg10]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg12]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg14]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg16]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg17]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg18]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg19]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg5]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg7]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg9]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_reg11]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_reg13]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg15]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x30]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x33]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x34]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x37]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x38]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x40]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x43]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x44]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x47]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x48]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x49]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x27]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x28]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg27]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

