;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/epwm.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2152012 
	.sect	".text"
	.clink

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_setEPWMClockDivider")
	.dwattr $C$DW$3, DW_AT_low_pc(_SysCtl_setEPWMClockDivider)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_SysCtl_setEPWMClockDivider")
	.dwattr $C$DW$3, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x410)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1041,column 1,is_stmt,address _SysCtl_setEPWMClockDivider

	.dwfde $C$DW$CIE, _SysCtl_setEPWMClockDivider
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("divider")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_divider")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_setEPWMClockDivider   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_SysCtl_setEPWMClockDivider:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("divider")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_divider")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |1041| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1045,column 5,is_stmt
$C$DW$6	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$6, DW_AT_low_pc(0x00)
	.dwattr $C$DW$6, DW_AT_name("___eallow")
	.dwattr $C$DW$6, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1045| 
        ; call occurs [#___eallow] ; [] |1045| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1046,column 5,is_stmt
        MOVL      XAR4,#381478          ; [CPU_U] |1046| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1046| 
        OR        AL,*-SP[1]            ; [CPU_] |1046| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1046| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1049,column 5,is_stmt
$C$DW$7	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$7, DW_AT_low_pc(0x00)
	.dwattr $C$DW$7, DW_AT_name("___edis")
	.dwattr $C$DW$7, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1049| 
        ; call occurs [#___edis] ; [] |1049| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1050,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x41a)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setTimeBaseCounter")
	.dwattr $C$DW$9, DW_AT_low_pc(_EPWM_setTimeBaseCounter)
	.dwattr $C$DW$9, DW_AT_high_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_EPWM_setTimeBaseCounter")
	.dwattr $C$DW$9, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$9, DW_AT_TI_begin_line(0x5f3)
	.dwattr $C$DW$9, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$9, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1524,column 1,is_stmt,address _EPWM_setTimeBaseCounter

	.dwfde $C$DW$CIE, _EPWM_setTimeBaseCounter
$C$DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg0]
$C$DW$11	.dwtag  DW_TAG_formal_parameter, DW_AT_name("count")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _EPWM_setTimeBaseCounter      FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_EPWM_setTimeBaseCounter:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -2]
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("count")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1524| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1524| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1533,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |1533| 
        MOVB      ACC,#4                ; [CPU_] |1533| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1533| 
        MOVL      XAR4,ACC              ; [CPU_] |1533| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1533| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1534,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$9, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$9, DW_AT_TI_end_line(0x5fe)
	.dwattr $C$DW$9, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$9

	.sect	".text"
	.clink

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setClockPrescaler")
	.dwattr $C$DW$15, DW_AT_low_pc(_EPWM_setClockPrescaler)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_EPWM_setClockPrescaler")
	.dwattr $C$DW$15, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x640)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1602,column 1,is_stmt,address _EPWM_setClockPrescaler

	.dwfde $C$DW$CIE, _EPWM_setClockPrescaler
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg0]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("prescaler")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_prescaler")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg12]
$C$DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_name("highSpeedPrescaler")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_highSpeedPrescaler")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _EPWM_setClockPrescaler       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_EPWM_setClockPrescaler:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -2]
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("prescaler")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_prescaler")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -3]
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("highSpeedPrescaler")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_highSpeedPrescaler")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |1602| 
        MOV       *-SP[3],AR4           ; [CPU_] |1602| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1602| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1611,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |1611| 
        AND       AL,*+XAR4[0],#0xe07f  ; [CPU_] |1611| 
        MOVZ      AR7,AL                ; [CPU_] |1611| 
        MOV       ACC,*-SP[3] << #10    ; [CPU_] |1611| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |1611| 
        OR        AL,AR7                ; [CPU_] |1611| 
        MOVZ      AR6,AL                ; [CPU_] |1611| 
        MOV       ACC,*-SP[4] << #7     ; [CPU_] |1611| 
        OR        AL,AR6                ; [CPU_] |1611| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1611| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1616,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$15, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x650)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

	.sect	".text"
	.clink

$C$DW$23	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_disablePhaseShiftLoad")
	.dwattr $C$DW$23, DW_AT_low_pc(_EPWM_disablePhaseShiftLoad)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_EPWM_disablePhaseShiftLoad")
	.dwattr $C$DW$23, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$23, DW_AT_TI_begin_line(0x700)
	.dwattr $C$DW$23, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1793,column 1,is_stmt,address _EPWM_disablePhaseShiftLoad

	.dwfde $C$DW$CIE, _EPWM_disablePhaseShiftLoad
$C$DW$24	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _EPWM_disablePhaseShiftLoad   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_EPWM_disablePhaseShiftLoad:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1793| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1802,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |1802| 
        AND       *+XAR4[0],#0xfffb     ; [CPU_] |1802| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1803,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0x70b)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

	.sect	".text"
	.clink

$C$DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setTimeBaseCounterMode")
	.dwattr $C$DW$27, DW_AT_low_pc(_EPWM_setTimeBaseCounterMode)
	.dwattr $C$DW$27, DW_AT_high_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_EPWM_setTimeBaseCounterMode")
	.dwattr $C$DW$27, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$27, DW_AT_TI_begin_line(0x71f)
	.dwattr $C$DW$27, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$27, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1824,column 1,is_stmt,address _EPWM_setTimeBaseCounterMode

	.dwfde $C$DW$CIE, _EPWM_setTimeBaseCounterMode
$C$DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg0]
$C$DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_name("counterMode")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_counterMode")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _EPWM_setTimeBaseCounterMode  FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_EPWM_setTimeBaseCounterMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_breg20 -2]
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("counterMode")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_counterMode")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1824| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1824| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1833,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |1833| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1833| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |1833| 
        OR        AL,*-SP[3]            ; [CPU_] |1833| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1833| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 1836,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$27, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$27, DW_AT_TI_end_line(0x72c)
	.dwattr $C$DW$27, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$27

	.sect	".text"
	.clink

$C$DW$33	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setPhaseShift")
	.dwattr $C$DW$33, DW_AT_low_pc(_EPWM_setPhaseShift)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_EPWM_setPhaseShift")
	.dwattr $C$DW$33, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$33, DW_AT_TI_begin_line(0x84a)
	.dwattr $C$DW$33, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2123,column 1,is_stmt,address _EPWM_setPhaseShift

	.dwfde $C$DW$CIE, _EPWM_setPhaseShift
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg0]
$C$DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_name("phaseCount")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_phaseCount")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _EPWM_setPhaseShift           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_EPWM_setPhaseShift:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -2]
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("phaseCount")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_phaseCount")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2123| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2123| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2132,column 5,is_stmt
        MOVB      ACC,#96               ; [CPU_] |2132| 
        CLRC      SXM                   ; [CPU_] 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2132| 
        MOVL      XAR4,ACC              ; [CPU_] |2132| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |2132| 
        MOV       ACC,*-SP[3] << 16     ; [CPU_] |2132| 
        AND       PH,#0                 ; [CPU_] |2132| 
        MOVL      *-SP[6],ACC           ; [CPU_] |2132| 
        MOV       AL,*-SP[6]            ; [CPU_] |2132| 
        OR        AL,PL                 ; [CPU_] |2132| 
        MOV       *-SP[6],AL            ; [CPU_] |2132| 
        MOV       AL,*-SP[5]            ; [CPU_] |2132| 
        OR        AL,PH                 ; [CPU_] |2132| 
        MOV       *-SP[5],AL            ; [CPU_] |2132| 
        MOVB      ACC,#96               ; [CPU_] |2132| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2132| 
        MOVL      XAR4,ACC              ; [CPU_] |2132| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |2132| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |2132| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2136,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$33, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x858)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text"
	.clink

$C$DW$39	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setTimeBasePeriod")
	.dwattr $C$DW$39, DW_AT_low_pc(_EPWM_setTimeBasePeriod)
	.dwattr $C$DW$39, DW_AT_high_pc(0x00)
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_EPWM_setTimeBasePeriod")
	.dwattr $C$DW$39, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$39, DW_AT_TI_begin_line(0x86c)
	.dwattr $C$DW$39, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$39, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2157,column 1,is_stmt,address _EPWM_setTimeBasePeriod

	.dwfde $C$DW$CIE, _EPWM_setTimeBasePeriod
$C$DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg0]
$C$DW$41	.dwtag  DW_TAG_formal_parameter, DW_AT_name("periodCount")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_periodCount")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _EPWM_setTimeBasePeriod       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_EPWM_setTimeBasePeriod:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -2]
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("periodCount")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_periodCount")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2157| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2157| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2166,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |2166| 
        MOVB      ACC,#99               ; [CPU_] |2166| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2166| 
        MOVL      XAR4,ACC              ; [CPU_] |2166| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2166| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2167,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$39, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$39, DW_AT_TI_end_line(0x877)
	.dwattr $C$DW$39, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$39

	.sect	".text"
	.clink

$C$DW$45	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setCounterCompareShadowLoadMode")
	.dwattr $C$DW$45, DW_AT_low_pc(_EPWM_setCounterCompareShadowLoadMode)
	.dwattr $C$DW$45, DW_AT_high_pc(0x00)
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_EPWM_setCounterCompareShadowLoadMode")
	.dwattr $C$DW$45, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$45, DW_AT_TI_begin_line(0x8eb)
	.dwattr $C$DW$45, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$45, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2286,column 1,is_stmt,address _EPWM_setCounterCompareShadowLoadMode

	.dwfde $C$DW$CIE, _EPWM_setCounterCompareShadowLoadMode
$C$DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg0]
$C$DW$47	.dwtag  DW_TAG_formal_parameter, DW_AT_name("compModule")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_compModule")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg12]
$C$DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_name("loadMode")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_loadMode")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _EPWM_setCounterCompareShadowLoadMode FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_EPWM_setCounterCompareShadowLoadMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -2]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("compModule")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_compModule")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -3]
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("loadMode")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_loadMode")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -4]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("syncModeOffset")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_syncModeOffset")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -5]
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("loadModeOffset")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_loadModeOffset")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_breg20 -6]
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("shadowModeOffset")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_shadowModeOffset")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_breg20 -7]
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("registerOffset")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_registerOffset")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[4],AR5           ; [CPU_] |2286| 
        MOV       *-SP[3],AR4           ; [CPU_] |2286| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2286| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2297,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |2297| 
        BF        $C$L1,EQ              ; [CPU_] |2297| 
        ; branchcc occurs ; [] |2297| 
        CMPB      AL,#5                 ; [CPU_] |2297| 
        BF        $C$L2,NEQ             ; [CPU_] |2297| 
        ; branchcc occurs ; [] |2297| 
$C$L1:    
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2300,column 9,is_stmt
        MOVB      *-SP[5],#10,UNC       ; [CPU_] |2300| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2301,column 9,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |2301| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2302,column 9,is_stmt
        MOVB      *-SP[7],#4,UNC        ; [CPU_] |2302| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2303,column 5,is_stmt
        B         $C$L3,UNC             ; [CPU_] |2303| 
        ; branch occurs ; [] |2303| 
$C$L2:    
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2306,column 9,is_stmt
        MOVB      *-SP[5],#12,UNC       ; [CPU_] |2306| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2307,column 9,is_stmt
        MOVB      *-SP[6],#2,UNC        ; [CPU_] |2307| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2308,column 9,is_stmt
        MOVB      *-SP[7],#6,UNC        ; [CPU_] |2308| 
$C$L3:    
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2315,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[3]           ; [CPU_] |2315| 
        MOVB      AH,#0                 ; [CPU_] |2315| 
        ANDB      AL,#0x01              ; [CPU_] |2315| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2315| 
        ADDB      ACC,#8                ; [CPU_] |2315| 
        MOVL      *-SP[10],ACC          ; [CPU_] |2315| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2321,column 5,is_stmt
        MOV       T,*-SP[5]             ; [CPU_] |2321| 
        MOVB      AL,#3                 ; [CPU_] |2321| 
        MOVB      AH,#3                 ; [CPU_] |2321| 
        LSL       AL,T                  ; [CPU_] |2321| 
        MOV       T,*-SP[6]             ; [CPU_] |2321| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |2321| 
        LSL       AH,T                  ; [CPU_] |2321| 
        MOV       T,*-SP[7]             ; [CPU_] |2321| 
        OR        AH,AL                 ; [CPU_] |2321| 
        MOVB      AL,#1                 ; [CPU_] |2321| 
        LSL       AL,T                  ; [CPU_] |2321| 
        MOV       T,*-SP[5]             ; [CPU_] |2321| 
        OR        AL,AH                 ; [CPU_] |2321| 
        MOV       AH,*-SP[4]            ; [CPU_] |2321| 
        NOT       AL                    ; [CPU_] |2321| 
        LSR       AH,2                  ; [CPU_] |2321| 
        LSL       AH,T                  ; [CPU_] |2321| 
        MOV       T,*-SP[6]             ; [CPU_] |2321| 
        AND       AL,*+XAR4[0]          ; [CPU_] |2321| 
        OR        AH,AL                 ; [CPU_] |2321| 
        MOV       AL,*-SP[4]            ; [CPU_] |2321| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |2321| 
        ANDB      AL,#0x03              ; [CPU_] |2321| 
        LSL       AL,T                  ; [CPU_] |2321| 
        OR        AL,AH                 ; [CPU_] |2321| 
        MOV       *+XAR4[0],AL          ; [CPU_] |2321| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2327,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$45, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$45, DW_AT_TI_end_line(0x917)
	.dwattr $C$DW$45, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$45

	.sect	".text"
	.clink

$C$DW$57	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setCounterCompareValue")
	.dwattr $C$DW$57, DW_AT_low_pc(_EPWM_setCounterCompareValue)
	.dwattr $C$DW$57, DW_AT_high_pc(0x00)
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_EPWM_setCounterCompareValue")
	.dwattr $C$DW$57, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$57, DW_AT_TI_begin_line(0x962)
	.dwattr $C$DW$57, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$57, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2404,column 1,is_stmt,address _EPWM_setCounterCompareValue

	.dwfde $C$DW$CIE, _EPWM_setCounterCompareValue
$C$DW$58	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg0]
$C$DW$59	.dwtag  DW_TAG_formal_parameter, DW_AT_name("compModule")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_compModule")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg12]
$C$DW$60	.dwtag  DW_TAG_formal_parameter, DW_AT_name("compCount")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_compCount")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _EPWM_setCounterCompareValue  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_EPWM_setCounterCompareValue:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -2]
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("compModule")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_compModule")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -3]
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("compCount")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_compCount")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -4]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("registerOffset")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_registerOffset")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[4],AR5           ; [CPU_] |2404| 
        MOV       *-SP[3],AR4           ; [CPU_] |2404| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2404| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2415,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |2415| 
        ADDB      AL,#106               ; [CPU_] |2415| 
        MOVU      ACC,AL                ; [CPU_] |2415| 
        MOVL      *-SP[6],ACC           ; [CPU_] |2415| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2420,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |2420| 
        BF        $C$L4,EQ              ; [CPU_] |2420| 
        ; branchcc occurs ; [] |2420| 
        CMPB      AL,#2                 ; [CPU_] |2420| 
        BF        $C$L5,NEQ             ; [CPU_] |2420| 
        ; branchcc occurs ; [] |2420| 
$C$L4:    
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2426,column 9,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |2426| 
        MOVZ      AR6,*-SP[4]           ; [CPU_] |2426| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2426| 
        ADDB      ACC,#1                ; [CPU_] |2426| 
        MOVL      XAR4,ACC              ; [CPU_] |2426| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2426| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2427,column 5,is_stmt
        B         $C$L6,UNC             ; [CPU_] |2427| 
        ; branch occurs ; [] |2427| 
$C$L5:    
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2433,column 9,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |2433| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |2433| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2433| 
        MOVL      XAR4,ACC              ; [CPU_] |2433| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2433| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2435,column 1,is_stmt
$C$L6:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$57, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$57, DW_AT_TI_end_line(0x983)
	.dwattr $C$DW$57, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$57

	.sect	".text"
	.clink

$C$DW$66	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setActionQualifierAction")
	.dwattr $C$DW$66, DW_AT_low_pc(_EPWM_setActionQualifierAction)
	.dwattr $C$DW$66, DW_AT_high_pc(0x00)
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$66, DW_AT_TI_begin_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$66, DW_AT_TI_begin_line(0xabd)
	.dwattr $C$DW$66, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$66, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2753,column 1,is_stmt,address _EPWM_setActionQualifierAction

	.dwfde $C$DW$CIE, _EPWM_setActionQualifierAction
$C$DW$67	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg0]
$C$DW$68	.dwtag  DW_TAG_formal_parameter, DW_AT_name("epwmOutput")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_epwmOutput")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg12]
$C$DW$69	.dwtag  DW_TAG_formal_parameter, DW_AT_name("output")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_output")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg14]
$C$DW$70	.dwtag  DW_TAG_formal_parameter, DW_AT_name("event")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_event")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -13]

;***************************************************************
;* FNAME: _EPWM_setActionQualifierAction FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  2 SOE     *
;***************************************************************

_EPWM_setActionQualifierAction:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOVL      *SP++,XAR1            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 7, 2
	.dwcfi	cfa_offset, -4
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -2]
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("epwmOutput")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_epwmOutput")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -3]
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("output")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_output")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_breg20 -4]
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("registerOffset")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_registerOffset")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -6]
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("registerTOffset")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_registerTOffset")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[4],AR5           ; [CPU_] |2753| 
        MOV       *-SP[3],AR4           ; [CPU_] |2753| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2753| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2765,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |2765| 
        ADDB      AL,#64                ; [CPU_] |2765| 
        MOVU      ACC,AL                ; [CPU_] |2765| 
        MOVL      *-SP[6],ACC           ; [CPU_] |2765| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2766,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |2766| 
        ADDB      AL,#65                ; [CPU_] |2766| 
        MOVU      ACC,AL                ; [CPU_] |2766| 
        MOVL      *-SP[8],ACC           ; [CPU_] |2766| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2771,column 5,is_stmt
        MOV       AL,*-SP[13]           ; [CPU_] |2771| 
        ANDB      AL,#0x01              ; [CPU_] |2771| 
        CMPB      AL,#1                 ; [CPU_] |2771| 
        BF        $C$L7,NEQ             ; [CPU_] |2771| 
        ; branchcc occurs ; [] |2771| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2776,column 9,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |2776| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2776| 
        MOVL      XAR4,ACC              ; [CPU_] |2776| 
        MOV       AL,*-SP[13]           ; [CPU_] |2776| 
        MOVB      AH,#3                 ; [CPU_] |2776| 
        ADDB      AL,#-1                ; [CPU_] |2776| 
        MOV       T,AL                  ; [CPU_] |2776| 
        MOV       AL,*-SP[13]           ; [CPU_] |2776| 
        LSL       AH,T                  ; [CPU_] |2776| 
        ADDB      AL,#-1                ; [CPU_] |2776| 
        NOT       AH                    ; [CPU_] |2776| 
        MOV       T,AL                  ; [CPU_] |2776| 
        MOV       AL,*-SP[4]            ; [CPU_] |2776| 
        AND       AH,*+XAR4[0]          ; [CPU_] |2776| 
        LSL       AL,T                  ; [CPU_] |2776| 
        MOVZ      AR6,AL                ; [CPU_] |2776| 
        OR        AR6,AH                ; [CPU_] |2776| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |2776| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2776| 
        MOVL      XAR4,ACC              ; [CPU_] |2776| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2776| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2779,column 5,is_stmt
        B         $C$L8,UNC             ; [CPU_] |2779| 
        ; branch occurs ; [] |2779| 
$C$L7:    
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2785,column 9,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |2785| 
        MOV       T,*-SP[13]            ; [CPU_] |2785| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2785| 
        MOVL      XAR4,ACC              ; [CPU_] |2785| 
        MOV       AL,*-SP[4]            ; [CPU_] |2785| 
        MOVB      AH,#3                 ; [CPU_] |2785| 
        LSL       AL,T                  ; [CPU_] |2785| 
        LSL       AH,T                  ; [CPU_] |2785| 
        MOVZ      AR6,AL                ; [CPU_] |2785| 
        NOT       AH                    ; [CPU_] |2785| 
        AND       AH,*+XAR4[0]          ; [CPU_] |2785| 
        OR        AR6,AH                ; [CPU_] |2785| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |2785| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2785| 
        MOVL      XAR4,ACC              ; [CPU_] |2785| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2785| 
	.dwpsn	file "..\ExtraData\driverlib2\epwm.h",line 2789,column 1,is_stmt
$C$L8:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOVL      XAR1,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 7
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$66, DW_AT_TI_end_file("..\ExtraData\driverlib2\epwm.h")
	.dwattr $C$DW$66, DW_AT_TI_end_line(0xae5)
	.dwattr $C$DW$66, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$66

	.sect	".text"
	.clink
	.global	_EPWM_setEmulationMode

$C$DW$77	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_setEmulationMode")
	.dwattr $C$DW$77, DW_AT_low_pc(_EPWM_setEmulationMode)
	.dwattr $C$DW$77, DW_AT_high_pc(0x00)
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_EPWM_setEmulationMode")
	.dwattr $C$DW$77, DW_AT_external
	.dwattr $C$DW$77, DW_AT_TI_begin_file("../ExtraData/driverlib2/epwm.c")
	.dwattr $C$DW$77, DW_AT_TI_begin_line(0x32)
	.dwattr $C$DW$77, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$77, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 51,column 1,is_stmt,address _EPWM_setEmulationMode

	.dwfde $C$DW$CIE, _EPWM_setEmulationMode
$C$DW$78	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg0]
$C$DW$79	.dwtag  DW_TAG_formal_parameter, DW_AT_name("emulationMode")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_emulationMode")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _EPWM_setEmulationMode        FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_EPWM_setEmulationMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_breg20 -2]
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("emulationMode")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_emulationMode")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |51| 
        MOVL      *-SP[2],ACC           ; [CPU_] |51| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 60,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |60| 
        AND       AL,*+XAR4[0],#0x3fff  ; [CPU_] |60| 
        MOVL      XAR4,*-SP[2]          ; [CPU_] |60| 
        MOVZ      AR6,AL                ; [CPU_] |60| 
        MOV       ACC,*-SP[3] << #14    ; [CPU_] |60| 
        OR        AL,AR6                ; [CPU_] |60| 
        MOV       *+XAR4[0],AL          ; [CPU_] |60| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 63,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$82	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$82, DW_AT_low_pc(0x00)
	.dwattr $C$DW$82, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$77, DW_AT_TI_end_file("../ExtraData/driverlib2/epwm.c")
	.dwattr $C$DW$77, DW_AT_TI_end_line(0x3f)
	.dwattr $C$DW$77, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$77

	.sect	".text"
	.clink
	.global	_EPWM_configureSignal

$C$DW$83	.dwtag  DW_TAG_subprogram, DW_AT_name("EPWM_configureSignal")
	.dwattr $C$DW$83, DW_AT_low_pc(_EPWM_configureSignal)
	.dwattr $C$DW$83, DW_AT_high_pc(0x00)
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_EPWM_configureSignal")
	.dwattr $C$DW$83, DW_AT_external
	.dwattr $C$DW$83, DW_AT_TI_begin_file("../ExtraData/driverlib2/epwm.c")
	.dwattr $C$DW$83, DW_AT_TI_begin_line(0x46)
	.dwattr $C$DW$83, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$83, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 71,column 1,is_stmt,address _EPWM_configureSignal

	.dwfde $C$DW$CIE, _EPWM_configureSignal
$C$DW$84	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg0]
$C$DW$85	.dwtag  DW_TAG_formal_parameter, DW_AT_name("signalParams")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_signalParams")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _EPWM_configureSignal         FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_EPWM_configureSignal:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -4]
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("signalParams")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_signalParams")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -6]
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("tbClkInHz")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_tbClkInHz")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -8]
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("tbPrdVal")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_tbPrdVal")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -9]
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("cmpAVal")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_cmpAVal")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -10]
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("cmpBVal")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_cmpBVal")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg20 -11]
        MOVL      *-SP[6],XAR4          ; [CPU_] |71| 
        MOVL      *-SP[4],ACC           ; [CPU_] |71| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 72,column 25,is_stmt
        ZERO      R0H                   ; [CPU_] |72| 
        MOV32     *-SP[8],R0H           ; [CPU_] |72| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 73,column 23,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |73| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 73,column 37,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |73| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 73,column 51,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |73| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 83,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |83| 
        MOVB      XAR0,#10              ; [CPU_] |83| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |83| 
$C$DW$92	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$92, DW_AT_low_pc(0x00)
	.dwattr $C$DW$92, DW_AT_name("_SysCtl_setEPWMClockDivider")
	.dwattr $C$DW$92, DW_AT_TI_call
        LCR       #_SysCtl_setEPWMClockDivider ; [CPU_] |83| 
        ; call occurs [#_SysCtl_setEPWMClockDivider] ; [] |83| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 88,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |88| 
        MOVB      XAR0,#12              ; [CPU_] |88| 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |88| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |88| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |88| 
        MOVB      XAR0,#13              ; [CPU_] |88| 
        MOVZ      AR5,*+XAR5[AR0]       ; [CPU_] |88| 
$C$DW$93	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$93, DW_AT_low_pc(0x00)
	.dwattr $C$DW$93, DW_AT_name("_EPWM_setClockPrescaler")
	.dwattr $C$DW$93, DW_AT_TI_call
        LCR       #_EPWM_setClockPrescaler ; [CPU_] |88| 
        ; call occurs [#_EPWM_setClockPrescaler] ; [] |88| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 94,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |94| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |94| 
        MOVB      XAR0,#11              ; [CPU_] |94| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |94| 
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_name("_EPWM_setTimeBaseCounterMode")
	.dwattr $C$DW$94, DW_AT_TI_call
        LCR       #_EPWM_setTimeBaseCounterMode ; [CPU_] |94| 
        ; call occurs [#_EPWM_setTimeBaseCounterMode] ; [] |94| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 100,column 5,is_stmt
        MOVL      XAR5,*-SP[6]          ; [CPU_] |100| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |100| 
        MOVB      XAR0,#12              ; [CPU_] |100| 
        MOV       AL,*+XAR5[AR0]        ; [CPU_] |100| 
        MOVB      XAR0,#10              ; [CPU_] |100| 
        ADD       AL,*+XAR4[AR0]        ; [CPU_] |100| 
        MOV       T,AL                  ; [CPU_] |100| 
        MOVB      AL,#1                 ; [CPU_] |100| 
        LSL       AL,T                  ; [CPU_] |100| 
        MOVU      ACC,AL                ; [CPU_] |100| 
        MOV32     R1H,ACC               ; [CPU_] |100| 
        NOP       ; [CPU_] 
        MOVL      XAR5,*-SP[6]          ; [CPU_] |100| 
        MOVB      XAR0,#8               ; [CPU_] |100| 
        MOV32     R0H,*+XAR5[AR0]       ; [CPU_] |100| 
        UI32TOF32 R1H,R1H               ; [CPU_] |100| 
$C$DW$95	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$95, DW_AT_low_pc(0x00)
	.dwattr $C$DW$95, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$95, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |100| 
        ; call occurs [#FS$$DIV] ; [] |100| 
        MOV32     *-SP[8],R0H           ; [CPU_] |100| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 104,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |104| 
        MOVB      XAR0,#13              ; [CPU_] |104| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |104| 
        CMPB      AL,#2                 ; [CPU_] |104| 
        B         $C$L9,GT              ; [CPU_] |104| 
        ; branchcc occurs ; [] |104| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 106,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |106| 
        MOVB      XAR0,#13              ; [CPU_] |106| 
        MOVB      AL,#1                 ; [CPU_] |106| 
        MOV       T,*+XAR4[AR0]         ; [CPU_] |106| 
        LSL       AL,T                  ; [CPU_] |106| 
        MOVU      ACC,AL                ; [CPU_] |106| 
        MOV32     R0H,ACC               ; [CPU_] |106| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        UI32TOF32 R1H,R0H               ; [CPU_] |106| 
        MOV32     R0H,*-SP[8]           ; [CPU_] |106| 
$C$DW$96	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$96, DW_AT_low_pc(0x00)
	.dwattr $C$DW$96, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$96, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |106| 
        ; call occurs [#FS$$DIV] ; [] |106| 
        MOV32     *-SP[8],R0H           ; [CPU_] |106| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 107,column 5,is_stmt
        B         $C$L10,UNC            ; [CPU_] |107| 
        ; branch occurs ; [] |107| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 110,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |110| 
        MOVB      XAR0,#13              ; [CPU_] |110| 
        MOV       ACC,*+XAR4[AR0] << #1 ; [CPU_] |110| 
        MOVU      ACC,AL                ; [CPU_] |110| 
        MOV32     R0H,ACC               ; [CPU_] |110| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        UI32TOF32 R1H,R0H               ; [CPU_] |110| 
        MOV32     R0H,*-SP[8]           ; [CPU_] |110| 
$C$DW$97	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$97, DW_AT_low_pc(0x00)
	.dwattr $C$DW$97, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$97, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |110| 
        ; call occurs [#FS$$DIV] ; [] |110| 
        MOV32     *-SP[8],R0H           ; [CPU_] |110| 
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 113,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |113| 
        MOVB      XAR0,#11              ; [CPU_] |113| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |113| 
        BF        $C$L11,NEQ            ; [CPU_] |113| 
        ; branchcc occurs ; [] |113| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 115,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |115| 
        MOV32     R1H,*+XAR4[0]         ; [CPU_] |115| 
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$98, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |115| 
        ; call occurs [#FS$$DIV] ; [] |115| 
        ADDF32    R0H,R0H,#49024        ; [CPU_] |115| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |115| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |115| 
        MOV       *-SP[9],AL            ; [CPU_] |115| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 116,column 9,is_stmt
        ADDB      AL,#1                 ; [CPU_] |116| 
        MOVU      ACC,AL                ; [CPU_] |116| 
        MOV32     R0H,ACC               ; [CPU_] |116| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |116| 
        UI32TOF32 R1H,R0H               ; [CPU_] |116| 
        MOV32     R3H,*+XAR4[2]         ; [CPU_] |116| 
        MPYF32    R0H,R1H,R3H           ; [CPU_] |116| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |116| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |116| 
        MOV       *-SP[10],AL           ; [CPU_] |116| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 118,column 9,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |118| 
        ADDB      AL,#1                 ; [CPU_] |118| 
        MOVU      ACC,AL                ; [CPU_] |118| 
        MOV32     R0H,ACC               ; [CPU_] |118| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |118| 
        UI32TOF32 R1H,R0H               ; [CPU_] |118| 
        MOV32     R2H,*+XAR4[4]         ; [CPU_] |118| 
        MPYF32    R0H,R1H,R2H           ; [CPU_] |118| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |118| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |118| 
        MOV       *-SP[11],AL           ; [CPU_] |118| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 120,column 5,is_stmt
        B         $C$L13,UNC            ; [CPU_] |120| 
        ; branch occurs ; [] |120| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 121,column 10,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |121| 
        MOVB      XAR0,#11              ; [CPU_] |121| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |121| 
        CMPB      AL,#1                 ; [CPU_] |121| 
        BF        $C$L12,NEQ            ; [CPU_] |121| 
        ; branchcc occurs ; [] |121| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 123,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |123| 
        MOV32     R1H,*+XAR4[0]         ; [CPU_] |123| 
$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$99, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |123| 
        ; call occurs [#FS$$DIV] ; [] |123| 
        ADDF32    R0H,R0H,#49024        ; [CPU_] |123| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |123| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |123| 
        MOV       *-SP[9],AL            ; [CPU_] |123| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 124,column 9,is_stmt
        ADDB      AL,#1                 ; [CPU_] |124| 
        MOVU      ACC,AL                ; [CPU_] |124| 
        MOV32     R0H,ACC               ; [CPU_] |124| 
        NOP       ; [CPU_] 
        MOVZ      AR6,*-SP[9]           ; [CPU_] |124| 
        ADDB      XAR6,#1               ; [CPU_] |124| 
        MOVZ      AR6,AR6               ; [CPU_] |124| 
        UI32TOF32 R2H,R0H               ; [CPU_] |124| 
        MOV32     R0H,XAR6              ; [CPU_] |124| 
        NOP       ; [CPU_] 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |124| 
        MOV32     R1H,*+XAR4[2]         ; [CPU_] |124| 
        MPYF32    R1H,R2H,R1H           ; [CPU_] |124| 
        UI32TOF32 R0H,R0H               ; [CPU_] |124| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R0H,R1H           ; [CPU_] |124| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |124| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |124| 
        MOV       *-SP[10],AL           ; [CPU_] |124| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 126,column 9,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |126| 
        ADDB      AL,#1                 ; [CPU_] |126| 
        MOVU      ACC,AL                ; [CPU_] |126| 
        MOV32     R0H,ACC               ; [CPU_] |126| 
        NOP       ; [CPU_] 
        MOVZ      AR6,*-SP[9]           ; [CPU_] |126| 
        ADDB      XAR6,#1               ; [CPU_] |126| 
        MOVZ      AR6,AR6               ; [CPU_] |126| 
        UI32TOF32 R2H,R0H               ; [CPU_] |126| 
        MOV32     R0H,XAR6              ; [CPU_] |126| 
        NOP       ; [CPU_] 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |126| 
        MOV32     R1H,*+XAR4[4]         ; [CPU_] |126| 
        MPYF32    R1H,R2H,R1H           ; [CPU_] |126| 
        UI32TOF32 R0H,R0H               ; [CPU_] |126| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R0H,R1H           ; [CPU_] |126| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |126| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |126| 
        MOV       *-SP[11],AL           ; [CPU_] |126| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 128,column 5,is_stmt
        B         $C$L13,UNC            ; [CPU_] |128| 
        ; branch occurs ; [] |128| 
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 129,column 10,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |129| 
        MOVB      XAR0,#11              ; [CPU_] |129| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |129| 
        CMPB      AL,#2                 ; [CPU_] |129| 
        BF        $C$L13,NEQ            ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 131,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |131| 
        MOV32     R0H,*+XAR4[0]         ; [CPU_] |131| 
        MPYF32    R1H,R0H,#16384        ; [CPU_] |131| 
        MOV32     R0H,*-SP[8]           ; [CPU_] |131| 
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$100, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |131| 
        ; call occurs [#FS$$DIV] ; [] |131| 
        F32TOUI16 R0H,R0H               ; [CPU_] |131| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |131| 
        MOV       *-SP[9],AL            ; [CPU_] |131| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 132,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |132| 
        UI16TOF32 R1H,*-SP[9]           ; [CPU_] |132| 
        UI16TOF32 R2H,*-SP[9]           ; [CPU_] |132| 
        MOV32     R0H,*+XAR4[2]         ; [CPU_] |132| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |132| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R2H,R0H           ; [CPU_] |132| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,#16128        ; [CPU_] |132| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |132| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |132| 
        MOV       *-SP[10],AL           ; [CPU_] |132| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 135,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |135| 
        UI16TOF32 R1H,*-SP[9]           ; [CPU_] |135| 
        UI16TOF32 R2H,*-SP[9]           ; [CPU_] |135| 
        MOV32     R0H,*+XAR4[4]         ; [CPU_] |135| 
        MPYF32    R0H,R1H,R0H           ; [CPU_] |135| 
        NOP       ; [CPU_] 
        SUBF32    R0H,R2H,R0H           ; [CPU_] |135| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,#16128        ; [CPU_] |135| 
        NOP       ; [CPU_] 
        F32TOUI16 R0H,R0H               ; [CPU_] |135| 
        NOP       ; [CPU_] 
        NOP       ; [CPU_] 
        MOV32     ACC,R0H               ; [CPU_] |135| 
        MOV       *-SP[11],AL           ; [CPU_] |135| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 143,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |143| 
        MOVZ      AR4,*-SP[9]           ; [CPU_] |143| 
$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_name("_EPWM_setTimeBasePeriod")
	.dwattr $C$DW$101, DW_AT_TI_call
        LCR       #_EPWM_setTimeBasePeriod ; [CPU_] |143| 
        ; call occurs [#_EPWM_setTimeBasePeriod] ; [] |143| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 148,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |148| 
$C$DW$102	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$102, DW_AT_low_pc(0x00)
	.dwattr $C$DW$102, DW_AT_name("_EPWM_disablePhaseShiftLoad")
	.dwattr $C$DW$102, DW_AT_TI_call
        LCR       #_EPWM_disablePhaseShiftLoad ; [CPU_] |148| 
        ; call occurs [#_EPWM_disablePhaseShiftLoad] ; [] |148| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 149,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |149| 
        MOVB      XAR4,#0               ; [CPU_] |149| 
$C$DW$103	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$103, DW_AT_low_pc(0x00)
	.dwattr $C$DW$103, DW_AT_name("_EPWM_setPhaseShift")
	.dwattr $C$DW$103, DW_AT_TI_call
        LCR       #_EPWM_setPhaseShift  ; [CPU_] |149| 
        ; call occurs [#_EPWM_setPhaseShift] ; [] |149| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 150,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |150| 
        MOVB      XAR4,#0               ; [CPU_] |150| 
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_name("_EPWM_setTimeBaseCounter")
	.dwattr $C$DW$104, DW_AT_TI_call
        LCR       #_EPWM_setTimeBaseCounter ; [CPU_] |150| 
        ; call occurs [#_EPWM_setTimeBaseCounter] ; [] |150| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 155,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |155| 
        MOVB      XAR5,#0               ; [CPU_] |155| 
        MOVB      XAR4,#0               ; [CPU_] |155| 
$C$DW$105	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$105, DW_AT_low_pc(0x00)
	.dwattr $C$DW$105, DW_AT_name("_EPWM_setCounterCompareShadowLoadMode")
	.dwattr $C$DW$105, DW_AT_TI_call
        LCR       #_EPWM_setCounterCompareShadowLoadMode ; [CPU_] |155| 
        ; call occurs [#_EPWM_setCounterCompareShadowLoadMode] ; [] |155| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 158,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |158| 
        MOVB      XAR4,#2               ; [CPU_] |158| 
        MOVB      XAR5,#0               ; [CPU_] |158| 
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("_EPWM_setCounterCompareShadowLoadMode")
	.dwattr $C$DW$106, DW_AT_TI_call
        LCR       #_EPWM_setCounterCompareShadowLoadMode ; [CPU_] |158| 
        ; call occurs [#_EPWM_setCounterCompareShadowLoadMode] ; [] |158| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 164,column 5,is_stmt
        MOVZ      AR5,*-SP[10]          ; [CPU_] |164| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |164| 
        MOVB      XAR4,#0               ; [CPU_] |164| 
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("_EPWM_setCounterCompareValue")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #_EPWM_setCounterCompareValue ; [CPU_] |164| 
        ; call occurs [#_EPWM_setCounterCompareValue] ; [] |164| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 166,column 5,is_stmt
        MOVZ      AR5,*-SP[11]          ; [CPU_] |166| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |166| 
        MOVB      XAR4,#2               ; [CPU_] |166| 
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_EPWM_setCounterCompareValue")
	.dwattr $C$DW$108, DW_AT_TI_call
        LCR       #_EPWM_setCounterCompareValue ; [CPU_] |166| 
        ; call occurs [#_EPWM_setCounterCompareValue] ; [] |166| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 172,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |172| 
        MOVB      XAR0,#11              ; [CPU_] |172| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |172| 
        BF        $C$L15,NEQ            ; [CPU_] |172| 
        ; branchcc occurs ; [] |172| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 177,column 9,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |177| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |177| 
        MOVB      XAR5,#2               ; [CPU_] |177| 
        MOVB      XAR4,#0               ; [CPU_] |177| 
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$109, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |177| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |177| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 185,column 9,is_stmt
        MOVB      *-SP[1],#4,UNC        ; [CPU_] |185| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |185| 
        MOVB      XAR4,#0               ; [CPU_] |185| 
        MOVB      XAR5,#1               ; [CPU_] |185| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$110, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |185| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |185| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 190,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |190| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |190| 
        CMPB      AL,#1                 ; [CPU_] |190| 
        BF        $C$L14,NEQ            ; [CPU_] |190| 
        ; branchcc occurs ; [] |190| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 195,column 13,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |195| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |195| 
        MOVB      XAR4,#2               ; [CPU_] |195| 
        MOVB      XAR5,#1               ; [CPU_] |195| 
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$111, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |195| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |195| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 202,column 13,is_stmt
        MOVB      *-SP[1],#8,UNC        ; [CPU_] |202| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |202| 
        MOVB      XAR4,#2               ; [CPU_] |202| 
        MOVB      XAR5,#2               ; [CPU_] |202| 
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$112, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |202| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |202| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 206,column 9,is_stmt
        B         $C$L19,UNC            ; [CPU_] |206| 
        ; branch occurs ; [] |206| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 212,column 13,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |212| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |212| 
        MOVB      XAR4,#2               ; [CPU_] |212| 
        MOVB      XAR5,#2               ; [CPU_] |212| 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$113, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |212| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |212| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 219,column 13,is_stmt
        MOVB      *-SP[1],#8,UNC        ; [CPU_] |219| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |219| 
        MOVB      XAR4,#2               ; [CPU_] |219| 
        MOVB      XAR5,#1               ; [CPU_] |219| 
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$114, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |219| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |219| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 225,column 5,is_stmt
        B         $C$L19,UNC            ; [CPU_] |225| 
        ; branch occurs ; [] |225| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 226,column 10,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |226| 
        MOVB      XAR0,#11              ; [CPU_] |226| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |226| 
        CMPB      AL,#1                 ; [CPU_] |226| 
        BF        $C$L17,NEQ            ; [CPU_] |226| 
        ; branchcc occurs ; [] |226| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 231,column 9,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |231| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |231| 
        MOVB      XAR5,#2               ; [CPU_] |231| 
        MOVB      XAR4,#0               ; [CPU_] |231| 
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$115, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |231| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |231| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 239,column 9,is_stmt
        MOVB      *-SP[1],#6,UNC        ; [CPU_] |239| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |239| 
        MOVB      XAR4,#0               ; [CPU_] |239| 
        MOVB      XAR5,#1               ; [CPU_] |239| 
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$116, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |239| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |239| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 244,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |244| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |244| 
        CMPB      AL,#1                 ; [CPU_] |244| 
        BF        $C$L16,NEQ            ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 249,column 13,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |249| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |249| 
        MOVB      XAR4,#2               ; [CPU_] |249| 
        MOVB      XAR5,#1               ; [CPU_] |249| 
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$117, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |249| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |249| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 256,column 13,is_stmt
        MOVB      *-SP[1],#10,UNC       ; [CPU_] |256| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |256| 
        MOVB      XAR4,#2               ; [CPU_] |256| 
        MOVB      XAR5,#2               ; [CPU_] |256| 
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$118, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |256| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |256| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 260,column 9,is_stmt
        B         $C$L19,UNC            ; [CPU_] |260| 
        ; branch occurs ; [] |260| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 266,column 13,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |266| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |266| 
        MOVB      XAR4,#2               ; [CPU_] |266| 
        MOVB      XAR5,#2               ; [CPU_] |266| 
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$119, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |266| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |266| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 273,column 13,is_stmt
        MOVB      *-SP[1],#10,UNC       ; [CPU_] |273| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |273| 
        MOVB      XAR4,#2               ; [CPU_] |273| 
        MOVB      XAR5,#1               ; [CPU_] |273| 
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$120, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |273| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |273| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 278,column 5,is_stmt
        B         $C$L19,UNC            ; [CPU_] |278| 
        ; branch occurs ; [] |278| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 279,column 10,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |279| 
        MOVB      XAR0,#11              ; [CPU_] |279| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |279| 
        CMPB      AL,#2                 ; [CPU_] |279| 
        BF        $C$L19,NEQ            ; [CPU_] |279| 
        ; branchcc occurs ; [] |279| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 284,column 9,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |284| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |284| 
        MOVB      XAR5,#1               ; [CPU_] |284| 
        MOVB      XAR4,#0               ; [CPU_] |284| 
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$121, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |284| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |284| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 292,column 9,is_stmt
        MOVB      *-SP[1],#4,UNC        ; [CPU_] |292| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |292| 
        MOVB      XAR4,#0               ; [CPU_] |292| 
        MOVB      XAR5,#2               ; [CPU_] |292| 
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$122, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |292| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |292| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 300,column 9,is_stmt
        MOVB      *-SP[1],#6,UNC        ; [CPU_] |300| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |300| 
        MOVB      XAR4,#0               ; [CPU_] |300| 
        MOVB      XAR5,#1               ; [CPU_] |300| 
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$123, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |300| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |300| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 305,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |305| 
        MOV       AL,*+XAR4[6]          ; [CPU_] |305| 
        CMPB      AL,#1                 ; [CPU_] |305| 
        BF        $C$L18,NEQ            ; [CPU_] |305| 
        ; branchcc occurs ; [] |305| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 310,column 13,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |310| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |310| 
        MOVB      XAR4,#2               ; [CPU_] |310| 
        MOVB      XAR5,#2               ; [CPU_] |310| 
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$124, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |310| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |310| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 318,column 13,is_stmt
        MOVB      *-SP[1],#8,UNC        ; [CPU_] |318| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |318| 
        MOVB      XAR4,#2               ; [CPU_] |318| 
        MOVB      XAR5,#1               ; [CPU_] |318| 
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$125, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |318| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |318| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 325,column 13,is_stmt
        MOVB      *-SP[1],#10,UNC       ; [CPU_] |325| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |325| 
        MOVB      XAR4,#2               ; [CPU_] |325| 
        MOVB      XAR5,#2               ; [CPU_] |325| 
$C$DW$126	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$126, DW_AT_low_pc(0x00)
	.dwattr $C$DW$126, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$126, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |325| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |325| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 329,column 9,is_stmt
        B         $C$L19,UNC            ; [CPU_] |329| 
        ; branch occurs ; [] |329| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 335,column 13,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |335| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |335| 
        MOVB      XAR4,#2               ; [CPU_] |335| 
        MOVB      XAR5,#1               ; [CPU_] |335| 
$C$DW$127	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$127, DW_AT_low_pc(0x00)
	.dwattr $C$DW$127, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$127, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |335| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |335| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 343,column 13,is_stmt
        MOVB      *-SP[1],#8,UNC        ; [CPU_] |343| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |343| 
        MOVB      XAR4,#2               ; [CPU_] |343| 
        MOVB      XAR5,#2               ; [CPU_] |343| 
$C$DW$128	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$128, DW_AT_low_pc(0x00)
	.dwattr $C$DW$128, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$128, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |343| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |343| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 350,column 13,is_stmt
        MOVB      *-SP[1],#10,UNC       ; [CPU_] |350| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |350| 
        MOVB      XAR4,#2               ; [CPU_] |350| 
        MOVB      XAR5,#1               ; [CPU_] |350| 
$C$DW$129	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$129, DW_AT_low_pc(0x00)
	.dwattr $C$DW$129, DW_AT_name("_EPWM_setActionQualifierAction")
	.dwattr $C$DW$129, DW_AT_TI_call
        LCR       #_EPWM_setActionQualifierAction ; [CPU_] |350| 
        ; call occurs [#_EPWM_setActionQualifierAction] ; [] |350| 
	.dwpsn	file "../ExtraData/driverlib2/epwm.c",line 356,column 1,is_stmt
$C$L19:    
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$83, DW_AT_TI_end_file("../ExtraData/driverlib2/epwm.c")
	.dwattr $C$DW$83, DW_AT_TI_end_line(0x164)
	.dwattr $C$DW$83, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$83

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___eallow
	.global	___edis
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$131	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_EPWMCLK_DIV_1"), DW_AT_const_value(0x00)
$C$DW$132	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_EPWMCLK_DIV_2"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("SysCtl_EPWMCLKDivider")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$133	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_MODE_UP"), DW_AT_const_value(0x00)
$C$DW$134	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_MODE_DOWN"), DW_AT_const_value(0x01)
$C$DW$135	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_MODE_UP_DOWN"), DW_AT_const_value(0x02)
$C$DW$136	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_MODE_STOP_FREEZE"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_TimeBaseCountMode")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)

$C$DW$T$25	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$137	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_1"), DW_AT_const_value(0x00)
$C$DW$138	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_2"), DW_AT_const_value(0x01)
$C$DW$139	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_4"), DW_AT_const_value(0x02)
$C$DW$140	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_8"), DW_AT_const_value(0x03)
$C$DW$141	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_16"), DW_AT_const_value(0x04)
$C$DW$142	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_32"), DW_AT_const_value(0x05)
$C$DW$143	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_64"), DW_AT_const_value(0x06)
$C$DW$144	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_CLOCK_DIVIDER_128"), DW_AT_const_value(0x07)
	.dwendtag $C$DW$T$25

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_ClockDivider")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$145	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_1"), DW_AT_const_value(0x00)
$C$DW$146	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_2"), DW_AT_const_value(0x01)
$C$DW$147	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_4"), DW_AT_const_value(0x02)
$C$DW$148	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_6"), DW_AT_const_value(0x03)
$C$DW$149	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_8"), DW_AT_const_value(0x04)
$C$DW$150	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_10"), DW_AT_const_value(0x05)
$C$DW$151	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_12"), DW_AT_const_value(0x06)
$C$DW$152	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_HSCLOCK_DIVIDER_14"), DW_AT_const_value(0x07)
	.dwendtag $C$DW$T$27

$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_HSClockDivider")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$T$34	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x01)
$C$DW$153	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_COMPARE_A"), DW_AT_const_value(0x00)
$C$DW$154	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_COMPARE_B"), DW_AT_const_value(0x02)
$C$DW$155	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_COMPARE_C"), DW_AT_const_value(0x05)
$C$DW$156	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COUNTER_COMPARE_D"), DW_AT_const_value(0x07)
	.dwendtag $C$DW$T$34

$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_CounterCompareModule")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)

$C$DW$T$36	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x01)
$C$DW$157	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_ON_CNTR_ZERO"), DW_AT_const_value(0x00)
$C$DW$158	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_ON_CNTR_PERIOD"), DW_AT_const_value(0x01)
$C$DW$159	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_ON_CNTR_ZERO_PERIOD"), DW_AT_const_value(0x02)
$C$DW$160	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_FREEZE"), DW_AT_const_value(0x03)
$C$DW$161	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_ON_SYNC_CNTR_ZERO"), DW_AT_const_value(0x04)
$C$DW$162	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_ON_SYNC_CNTR_PERIOD"), DW_AT_const_value(0x05)
$C$DW$163	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_ON_SYNC_CNTR_ZERO_PERIOD"), DW_AT_const_value(0x06)
$C$DW$164	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_COMP_LOAD_ON_SYNC_ONLY"), DW_AT_const_value(0x08)
	.dwendtag $C$DW$T$36

$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_CounterCompareLoadMode")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)

$C$DW$T$38	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x01)
$C$DW$165	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_A"), DW_AT_const_value(0x00)
$C$DW$166	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_B"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$38

$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_ActionQualifierOutputModule")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)

$C$DW$T$40	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x01)
$C$DW$167	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_NO_CHANGE"), DW_AT_const_value(0x00)
$C$DW$168	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_LOW"), DW_AT_const_value(0x01)
$C$DW$169	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_HIGH"), DW_AT_const_value(0x02)
$C$DW$170	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_TOGGLE"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$40

$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_ActionQualifierOutput")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)

$C$DW$T$42	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x01)
$C$DW$171	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_TIMEBASE_ZERO"), DW_AT_const_value(0x00)
$C$DW$172	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_TIMEBASE_PERIOD"), DW_AT_const_value(0x02)
$C$DW$173	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPA"), DW_AT_const_value(0x04)
$C$DW$174	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPA"), DW_AT_const_value(0x06)
$C$DW$175	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_TIMEBASE_UP_CMPB"), DW_AT_const_value(0x08)
$C$DW$176	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_TIMEBASE_DOWN_CMPB"), DW_AT_const_value(0x0a)
$C$DW$177	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_T1_COUNT_UP"), DW_AT_const_value(0x01)
$C$DW$178	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_T1_COUNT_DOWN"), DW_AT_const_value(0x03)
$C$DW$179	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_T2_COUNT_UP"), DW_AT_const_value(0x05)
$C$DW$180	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_AQ_OUTPUT_ON_T2_COUNT_DOWN"), DW_AT_const_value(0x07)
	.dwendtag $C$DW$T$42

$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_ActionQualifierOutputEvent")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)

$C$DW$T$44	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x01)
$C$DW$181	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_EMULATION_STOP_AFTER_NEXT_TB"), DW_AT_const_value(0x00)
$C$DW$182	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_EMULATION_STOP_AFTER_FULL_CYCLE"), DW_AT_const_value(0x01)
$C$DW$183	.dwtag  DW_TAG_enumerator, DW_AT_name("EPWM_EMULATION_FREE_RUN"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$44

$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_EmulationMode")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)

$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x0e)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$184, DW_AT_name("freqInHz")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_freqInHz")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$185, DW_AT_name("dutyValA")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_dutyValA")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$186, DW_AT_name("dutyValB")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_dutyValB")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$187, DW_AT_name("invertSignalB")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_invertSignalB")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$188, DW_AT_name("sysClkInHz")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_sysClkInHz")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$189, DW_AT_name("epwmClkDiv")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_epwmClkDiv")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$190, DW_AT_name("tbCtrMode")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_tbCtrMode")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$191, DW_AT_name("tbClkDiv")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_tbClkDiv")
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$192, DW_AT_name("tbHSClkDiv")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_tbHSClkDiv")
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29

$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("EPWM_SignalParams")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$193	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$46)
$C$DW$T$47	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$193)
$C$DW$T$48	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$48, DW_AT_address_class(0x16)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("float32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg0]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg1]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_reg2]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_reg3]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg20]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg21]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg22]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg23]
$C$DW$202	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg24]
$C$DW$203	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_reg25]
$C$DW$204	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg26]
$C$DW$205	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_reg28]
$C$DW$206	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg29]
$C$DW$207	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg30]
$C$DW$208	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_reg31]
$C$DW$209	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_regx 0x20]
$C$DW$210	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_regx 0x21]
$C$DW$211	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_regx 0x22]
$C$DW$212	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$212, DW_AT_location[DW_OP_regx 0x23]
$C$DW$213	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$213, DW_AT_location[DW_OP_regx 0x24]
$C$DW$214	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$214, DW_AT_location[DW_OP_regx 0x25]
$C$DW$215	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$215, DW_AT_location[DW_OP_regx 0x26]
$C$DW$216	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$216, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$217	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$217, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$218	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg4]
$C$DW$219	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_reg6]
$C$DW$220	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg8]
$C$DW$221	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg10]
$C$DW$222	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg12]
$C$DW$223	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg14]
$C$DW$224	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg16]
$C$DW$225	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg17]
$C$DW$226	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$226, DW_AT_location[DW_OP_reg18]
$C$DW$227	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg19]
$C$DW$228	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg5]
$C$DW$229	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_reg7]
$C$DW$230	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_reg9]
$C$DW$231	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_reg11]
$C$DW$232	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_reg13]
$C$DW$233	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_reg15]
$C$DW$234	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_regx 0x30]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_regx 0x33]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_regx 0x34]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_regx 0x37]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_regx 0x38]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_regx 0x40]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x43]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x44]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_regx 0x47]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_regx 0x48]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x49]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0x27]
$C$DW$253	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$253, DW_AT_location[DW_OP_regx 0x28]
$C$DW$254	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$254, DW_AT_location[DW_OP_reg27]
$C$DW$255	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$255, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

