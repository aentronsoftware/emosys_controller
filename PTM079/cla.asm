;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:02 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/cla.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0398412 
	.sect	".text"
	.clink
	.global	_CLA_setTriggerSource

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("CLA_setTriggerSource")
	.dwattr $C$DW$3, DW_AT_low_pc(_CLA_setTriggerSource)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_CLA_setTriggerSource")
	.dwattr $C$DW$3, DW_AT_external
	.dwattr $C$DW$3, DW_AT_TI_begin_file("../ExtraData/driverlib2/cla.c")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 52,column 1,is_stmt,address _CLA_setTriggerSource

	.dwfde $C$DW$CIE, _CLA_setTriggerSource
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("taskNumber")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_taskNumber")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("trigger")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_trigger")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _CLA_setTriggerSource         FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_CLA_setTriggerSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("taskNumber")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_taskNumber")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -1]
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("trigger")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_trigger")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -2]
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("srcSelReg")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_srcSelReg")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_breg20 -4]
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("shiftVal")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_shiftVal")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[2],AH            ; [CPU_] |52| 
        MOV       *-SP[1],AL            ; [CPU_] |52| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 59,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[1]           ; [CPU_] |59| 
        LSL       ACC,3                 ; [CPU_] |59| 
        MOVB      AH,#0                 ; [CPU_] |59| 
        ANDB      AL,#0x1f              ; [CPU_] |59| 
        MOVL      *-SP[6],ACC           ; [CPU_] |59| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 64,column 5,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |64| 
        CMPB      AL,#3                 ; [CPU_] |64| 
        B         $C$L1,GT              ; [CPU_] |64| 
        ; branchcc occurs ; [] |64| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 69,column 9,is_stmt
        MOVL      XAR4,#31110           ; [CPU_U] |69| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |69| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 70,column 5,is_stmt
        B         $C$L2,UNC             ; [CPU_] |70| 
        ; branch occurs ; [] |70| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 76,column 9,is_stmt
        MOVL      XAR4,#31112           ; [CPU_U] |76| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |76| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 79,column 5,is_stmt
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_name("___eallow")
	.dwattr $C$DW$10, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |79| 
        ; call occurs [#___eallow] ; [] |79| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 84,column 5,is_stmt
        MOVB      ACC,#255              ; [CPU_] |84| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |84| 
        MOV       T,*-SP[6]             ; [CPU_] |84| 
        LSLL      ACC,T                 ; [CPU_] |84| 
        NOT       ACC                   ; [CPU_] |84| 
        AND       *+XAR4[0],AL          ; [CPU_] |84| 
        AND       *+XAR4[1],AH          ; [CPU_] |84| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 86,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       T,*-SP[6]             ; [CPU_] |86| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |86| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |86| 
        MOV       ACC,*-SP[2]           ; [CPU_] |86| 
        LSLL      ACC,T                 ; [CPU_] |86| 
        OR        AL,*+XAR4[0]          ; [CPU_] |86| 
        OR        AH,*+XAR4[1]          ; [CPU_] |86| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |86| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 88,column 5,is_stmt
$C$DW$11	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$11, DW_AT_low_pc(0x00)
	.dwattr $C$DW$11, DW_AT_name("___edis")
	.dwattr $C$DW$11, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |88| 
        ; call occurs [#___edis] ; [] |88| 
	.dwpsn	file "../ExtraData/driverlib2/cla.c",line 89,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$12	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$12, DW_AT_low_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("../ExtraData/driverlib2/cla.c")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x59)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$13	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_1"), DW_AT_const_value(0x00)
$C$DW$14	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_2"), DW_AT_const_value(0x01)
$C$DW$15	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_3"), DW_AT_const_value(0x02)
$C$DW$16	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_4"), DW_AT_const_value(0x03)
$C$DW$17	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_5"), DW_AT_const_value(0x04)
$C$DW$18	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_6"), DW_AT_const_value(0x05)
$C$DW$19	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_7"), DW_AT_const_value(0x06)
$C$DW$20	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TASK_8"), DW_AT_const_value(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("CLA_TaskNumber")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$21	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SOFTWARE"), DW_AT_const_value(0x00)
$C$DW$22	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCA1"), DW_AT_const_value(0x01)
$C$DW$23	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCA2"), DW_AT_const_value(0x02)
$C$DW$24	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCA3"), DW_AT_const_value(0x03)
$C$DW$25	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCA4"), DW_AT_const_value(0x04)
$C$DW$26	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCAEVT"), DW_AT_const_value(0x05)
$C$DW$27	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCB1"), DW_AT_const_value(0x06)
$C$DW$28	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCB2"), DW_AT_const_value(0x07)
$C$DW$29	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCB3"), DW_AT_const_value(0x08)
$C$DW$30	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCB4"), DW_AT_const_value(0x09)
$C$DW$31	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCBEVT"), DW_AT_const_value(0x0a)
$C$DW$32	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCD1"), DW_AT_const_value(0x10)
$C$DW$33	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCD2"), DW_AT_const_value(0x11)
$C$DW$34	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCD3"), DW_AT_const_value(0x12)
$C$DW$35	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCD4"), DW_AT_const_value(0x13)
$C$DW$36	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ADCDEVT"), DW_AT_const_value(0x14)
$C$DW$37	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_XINT1"), DW_AT_const_value(0x1d)
$C$DW$38	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_XINT2"), DW_AT_const_value(0x1e)
$C$DW$39	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_XINT3"), DW_AT_const_value(0x1f)
$C$DW$40	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_XINT4"), DW_AT_const_value(0x20)
$C$DW$41	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_XINT5"), DW_AT_const_value(0x21)
$C$DW$42	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM1INT"), DW_AT_const_value(0x24)
$C$DW$43	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM2INT"), DW_AT_const_value(0x25)
$C$DW$44	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM3INT"), DW_AT_const_value(0x26)
$C$DW$45	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM4INT"), DW_AT_const_value(0x27)
$C$DW$46	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM5INT"), DW_AT_const_value(0x28)
$C$DW$47	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM6INT"), DW_AT_const_value(0x29)
$C$DW$48	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM7INT"), DW_AT_const_value(0x2a)
$C$DW$49	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM8INT"), DW_AT_const_value(0x2b)
$C$DW$50	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM9INT"), DW_AT_const_value(0x2c)
$C$DW$51	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM10INT"), DW_AT_const_value(0x2d)
$C$DW$52	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM11INT"), DW_AT_const_value(0x2e)
$C$DW$53	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EPWM12INT"), DW_AT_const_value(0x2f)
$C$DW$54	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_TINT0"), DW_AT_const_value(0x44)
$C$DW$55	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_TINT1"), DW_AT_const_value(0x45)
$C$DW$56	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_TINT2"), DW_AT_const_value(0x46)
$C$DW$57	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_MXINTA"), DW_AT_const_value(0x47)
$C$DW$58	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_MRINTA"), DW_AT_const_value(0x48)
$C$DW$59	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_MXINTB"), DW_AT_const_value(0x49)
$C$DW$60	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_MRINTB"), DW_AT_const_value(0x4a)
$C$DW$61	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ECAP1INT"), DW_AT_const_value(0x4b)
$C$DW$62	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ECAP2INT"), DW_AT_const_value(0x4c)
$C$DW$63	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ECAP3INT"), DW_AT_const_value(0x4d)
$C$DW$64	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ECAP4INT"), DW_AT_const_value(0x4e)
$C$DW$65	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ECAP5INT"), DW_AT_const_value(0x4f)
$C$DW$66	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_ECAP6INT"), DW_AT_const_value(0x50)
$C$DW$67	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EQEP1INT"), DW_AT_const_value(0x53)
$C$DW$68	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EQEP2INT"), DW_AT_const_value(0x54)
$C$DW$69	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_EQEP3INT"), DW_AT_const_value(0x55)
$C$DW$70	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SDFM1INT"), DW_AT_const_value(0x5f)
$C$DW$71	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SDFM2INT"), DW_AT_const_value(0x60)
$C$DW$72	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_UPP1INT"), DW_AT_const_value(0x6b)
$C$DW$73	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SPITXAINT"), DW_AT_const_value(0x6d)
$C$DW$74	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SPIRXAINT"), DW_AT_const_value(0x6e)
$C$DW$75	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SPITXBINT"), DW_AT_const_value(0x6f)
$C$DW$76	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SPIRXBINT"), DW_AT_const_value(0x70)
$C$DW$77	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SPITXCINT"), DW_AT_const_value(0x71)
$C$DW$78	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_SPIRXCINT"), DW_AT_const_value(0x72)
$C$DW$79	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_CLB1INT"), DW_AT_const_value(0x7f)
$C$DW$80	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_CLB2INT"), DW_AT_const_value(0x80)
$C$DW$81	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_CLB3INT"), DW_AT_const_value(0x81)
$C$DW$82	.dwtag  DW_TAG_enumerator, DW_AT_name("CLA_TRIGGER_CLB4INT"), DW_AT_const_value(0x82)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("CLA_Trigger")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg0]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg1]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg2]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg3]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg20]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg21]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg22]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg23]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg24]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg25]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg26]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_reg28]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_reg29]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg30]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg31]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x20]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x21]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x22]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x23]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x24]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x25]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x26]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg4]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg6]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg8]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg10]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg12]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg14]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg16]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg17]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg18]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg19]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg5]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg7]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg9]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg11]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg13]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg15]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x30]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x33]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x34]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x37]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x38]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x40]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x43]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x44]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x47]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x48]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x49]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x27]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x28]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg27]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

