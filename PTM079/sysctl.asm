;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:04 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_delay")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_SysCtl_delay")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$27)
	.dwendtag $C$DW$3


$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("__disable_interrupts")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("___disable_interrupts")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$5, DW_AT_declaration
	.dwattr $C$DW$5, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1778812 
	.sect	".text"
 .if __TI_EABI__
 .asg    SysCtl_delay    , _SysCtl_delay
 .endif
 .def _SysCtl_delay
 .sect ".TI.ramfunc"
 .global  _SysCtl_delay
_SysCtl_delay:
 SUB    ACC,#1
 BF     _SysCtl_delay, GEQ
 LRETR
	.sect	".text"
	.clink

$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_serviceWatchdog")
	.dwattr $C$DW$6, DW_AT_low_pc(_SysCtl_serviceWatchdog)
	.dwattr $C$DW$6, DW_AT_high_pc(0x00)
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_SysCtl_serviceWatchdog")
	.dwattr $C$DW$6, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$6, DW_AT_TI_begin_line(0x6fa)
	.dwattr $C$DW$6, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$6, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1787,column 1,is_stmt,address _SysCtl_serviceWatchdog

	.dwfde $C$DW$CIE, _SysCtl_serviceWatchdog

;***************************************************************
;* FNAME: _SysCtl_serviceWatchdog       FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SysCtl_serviceWatchdog:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1788,column 5,is_stmt
$C$DW$7	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$7, DW_AT_low_pc(0x00)
	.dwattr $C$DW$7, DW_AT_name("___eallow")
	.dwattr $C$DW$7, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1788| 
        ; call occurs [#___eallow] ; [] |1788| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1793,column 5,is_stmt
        MOVB      AL,#85                ; [CPU_] |1793| 
        MOV       *(0:0x7025),AL        ; [CPU_] |1793| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1794,column 5,is_stmt
        MOVB      AL,#170               ; [CPU_] |1794| 
        MOV       *(0:0x7025),AL        ; [CPU_] |1794| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1796,column 5,is_stmt
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_name("___edis")
	.dwattr $C$DW$8, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1796| 
        ; call occurs [#___edis] ; [] |1796| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 1797,column 1,is_stmt
$C$DW$9	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$9, DW_AT_low_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$6, DW_AT_TI_end_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$6, DW_AT_TI_end_line(0x705)
	.dwattr $C$DW$6, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$6

	.sect	".text"
	.clink

$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_isMCDClockFailureDetected")
	.dwattr $C$DW$10, DW_AT_low_pc(_SysCtl_isMCDClockFailureDetected)
	.dwattr $C$DW$10, DW_AT_high_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_SysCtl_isMCDClockFailureDetected")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$10, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$10, DW_AT_TI_begin_line(0x954)
	.dwattr $C$DW$10, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$10, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2389,column 1,is_stmt,address _SysCtl_isMCDClockFailureDetected

	.dwfde $C$DW$CIE, _SysCtl_isMCDClockFailureDetected

;***************************************************************
;* FNAME: _SysCtl_isMCDClockFailureDetected FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SysCtl_isMCDClockFailureDetected:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2393,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |2393| 
        MOVL      XAR4,#381486          ; [CPU_U] |2393| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |2393| 
        BF        $C$L1,NTC             ; [CPU_] |2393| 
        ; branchcc occurs ; [] |2393| 
        MOVB      AL,#1                 ; [CPU_] |2393| 
$C$L1:    
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2394,column 1,is_stmt
$C$DW$11	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$11, DW_AT_low_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$10, DW_AT_TI_end_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$10, DW_AT_TI_end_line(0x95a)
	.dwattr $C$DW$10, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$10

	.sect	".text"
	.clink

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_resetMCD")
	.dwattr $C$DW$12, DW_AT_low_pc(_SysCtl_resetMCD)
	.dwattr $C$DW$12, DW_AT_high_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_SysCtl_resetMCD")
	.dwattr $C$DW$12, DW_AT_TI_begin_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$12, DW_AT_TI_begin_line(0x964)
	.dwattr $C$DW$12, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$12, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2405,column 1,is_stmt,address _SysCtl_resetMCD

	.dwfde $C$DW$CIE, _SysCtl_resetMCD

;***************************************************************
;* FNAME: _SysCtl_resetMCD              FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_SysCtl_resetMCD:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2406,column 5,is_stmt
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_name("___eallow")
	.dwattr $C$DW$13, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |2406| 
        ; call occurs [#___eallow] ; [] |2406| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2408,column 5,is_stmt
        MOVL      XAR4,#381486          ; [CPU_U] |2408| 
        OR        *+XAR4[0],#0x0002     ; [CPU_] |2408| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2410,column 5,is_stmt
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_name("___edis")
	.dwattr $C$DW$14, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |2410| 
        ; call occurs [#___edis] ; [] |2410| 
	.dwpsn	file "..\ExtraData\driverlib2\sysctl.h",line 2411,column 1,is_stmt
$C$DW$15	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$15, DW_AT_low_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$12, DW_AT_TI_end_file("..\ExtraData\driverlib2\sysctl.h")
	.dwattr $C$DW$12, DW_AT_TI_end_line(0x96b)
	.dwattr $C$DW$12, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$12

	.sect	".text"
	.clink

$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("CPUTimer_clearOverflowFlag")
	.dwattr $C$DW$16, DW_AT_low_pc(_CPUTimer_clearOverflowFlag)
	.dwattr $C$DW$16, DW_AT_high_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_CPUTimer_clearOverflowFlag")
	.dwattr $C$DW$16, DW_AT_TI_begin_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$16, DW_AT_TI_begin_line(0xa2)
	.dwattr $C$DW$16, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$16, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 163,column 1,is_stmt,address _CPUTimer_clearOverflowFlag

	.dwfde $C$DW$CIE, _CPUTimer_clearOverflowFlag
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _CPUTimer_clearOverflowFlag   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPUTimer_clearOverflowFlag:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |163| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 169,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |169| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |169| 
        MOVL      XAR4,ACC              ; [CPU_] |169| 
        OR        *+XAR4[0],#0x8000     ; [CPU_] |169| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 170,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$16, DW_AT_TI_end_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$16, DW_AT_TI_end_line(0xaa)
	.dwattr $C$DW$16, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$16

	.sect	".text"
	.clink

$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("CPUTimer_disableInterrupt")
	.dwattr $C$DW$20, DW_AT_low_pc(_CPUTimer_disableInterrupt)
	.dwattr $C$DW$20, DW_AT_high_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_CPUTimer_disableInterrupt")
	.dwattr $C$DW$20, DW_AT_TI_begin_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$20, DW_AT_TI_begin_line(0xb7)
	.dwattr $C$DW$20, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$20, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 184,column 1,is_stmt,address _CPUTimer_disableInterrupt

	.dwfde $C$DW$CIE, _CPUTimer_disableInterrupt
$C$DW$21	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _CPUTimer_disableInterrupt    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPUTimer_disableInterrupt:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |184| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 190,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |190| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |190| 
        MOVL      XAR4,ACC              ; [CPU_] |190| 
        AND       *+XAR4[0],#0xbfff     ; [CPU_] |190| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 191,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$20, DW_AT_TI_end_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$20, DW_AT_TI_end_line(0xbf)
	.dwattr $C$DW$20, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$20

	.sect	".text"
	.clink

$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("CPUTimer_stopTimer")
	.dwattr $C$DW$24, DW_AT_low_pc(_CPUTimer_stopTimer)
	.dwattr $C$DW$24, DW_AT_high_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_CPUTimer_stopTimer")
	.dwattr $C$DW$24, DW_AT_TI_begin_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$24, DW_AT_TI_begin_line(0xf7)
	.dwattr $C$DW$24, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$24, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 248,column 1,is_stmt,address _CPUTimer_stopTimer

	.dwfde $C$DW$CIE, _CPUTimer_stopTimer
$C$DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _CPUTimer_stopTimer           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPUTimer_stopTimer:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |248| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 254,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |254| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |254| 
        MOVL      XAR4,ACC              ; [CPU_] |254| 
        OR        *+XAR4[0],#0x0010     ; [CPU_] |254| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 255,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$24, DW_AT_TI_end_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$24, DW_AT_TI_end_line(0xff)
	.dwattr $C$DW$24, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$24

	.sect	".text"
	.clink

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("CPUTimer_startTimer")
	.dwattr $C$DW$28, DW_AT_low_pc(_CPUTimer_startTimer)
	.dwattr $C$DW$28, DW_AT_high_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_CPUTimer_startTimer")
	.dwattr $C$DW$28, DW_AT_TI_begin_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$28, DW_AT_TI_begin_line(0x125)
	.dwattr $C$DW$28, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$28, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 294,column 1,is_stmt,address _CPUTimer_startTimer

	.dwfde $C$DW$CIE, _CPUTimer_startTimer
$C$DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _CPUTimer_startTimer          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPUTimer_startTimer:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |294| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 300,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |300| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |300| 
        MOVL      XAR4,ACC              ; [CPU_] |300| 
        OR        *+XAR4[0],#0x0020     ; [CPU_] |300| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 305,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |305| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |305| 
        MOVL      XAR4,ACC              ; [CPU_] |305| 
        AND       *+XAR4[0],#0xffef     ; [CPU_] |305| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 306,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$28, DW_AT_TI_end_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$28, DW_AT_TI_end_line(0x132)
	.dwattr $C$DW$28, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$28

	.sect	".text"
	.clink

$C$DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("CPUTimer_setPeriod")
	.dwattr $C$DW$32, DW_AT_low_pc(_CPUTimer_setPeriod)
	.dwattr $C$DW$32, DW_AT_high_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_CPUTimer_setPeriod")
	.dwattr $C$DW$32, DW_AT_TI_begin_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$32, DW_AT_TI_begin_line(0x140)
	.dwattr $C$DW$32, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$32, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 321,column 1,is_stmt,address _CPUTimer_setPeriod

	.dwfde $C$DW$CIE, _CPUTimer_setPeriod
$C$DW$33	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg0]
$C$DW$34	.dwtag  DW_TAG_formal_parameter, DW_AT_name("periodCount")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_periodCount")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _CPUTimer_setPeriod           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPUTimer_setPeriod:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |321| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 327,column 5,is_stmt
        MOVL      XAR6,*-SP[6]          ; [CPU_] |327| 
        MOVB      ACC,#2                ; [CPU_] |327| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |327| 
        MOVL      XAR4,ACC              ; [CPU_] |327| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |327| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 328,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$32, DW_AT_TI_end_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$32, DW_AT_TI_end_line(0x148)
	.dwattr $C$DW$32, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$32

	.sect	".text"
	.clink

$C$DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("CPUTimer_getTimerOverflowStatus")
	.dwattr $C$DW$37, DW_AT_low_pc(_CPUTimer_getTimerOverflowStatus)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_CPUTimer_getTimerOverflowStatus")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$37, DW_AT_TI_begin_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0x182)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 387,column 1,is_stmt,address _CPUTimer_getTimerOverflowStatus

	.dwfde $C$DW$CIE, _CPUTimer_getTimerOverflowStatus
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _CPUTimer_getTimerOverflowStatus FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CPUTimer_getTimerOverflowStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |387| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 393,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |393| 
        CLRC      SXM                   ; [CPU_] 
        ADDL      ACC,*-SP[2]           ; [CPU_] |393| 
        MOVL      XAR5,ACC              ; [CPU_] |393| 
        AND       AL,*+XAR5[0],#0x8000  ; [CPU_] |393| 
        MOVZ      AR6,AL                ; [CPU_] |393| 
        MOV       ACC,#32768            ; [CPU_] |393| 
        CMPL      ACC,XAR6              ; [CPU_] |393| 
        BF        $C$L2,NEQ             ; [CPU_] |393| 
        ; branchcc occurs ; [] |393| 
        MOVB      AL,#1                 ; [CPU_] |393| 
        B         $C$L3,UNC             ; [CPU_] |393| 
        ; branch occurs ; [] |393| 
$C$L2:    
        MOVB      AL,#0                 ; [CPU_] |393| 
$C$L3:    
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 395,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$37, DW_AT_TI_end_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0x18b)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

	.sect	".text"
	.clink

$C$DW$41	.dwtag  DW_TAG_subprogram, DW_AT_name("CPUTimer_selectClockSource")
	.dwattr $C$DW$41, DW_AT_low_pc(_CPUTimer_selectClockSource)
	.dwattr $C$DW$41, DW_AT_high_pc(0x00)
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_CPUTimer_selectClockSource")
	.dwattr $C$DW$41, DW_AT_TI_begin_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$41, DW_AT_TI_begin_line(0x1aa)
	.dwattr $C$DW$41, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$41, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 429,column 1,is_stmt,address _CPUTimer_selectClockSource

	.dwfde $C$DW$CIE, _CPUTimer_selectClockSource
$C$DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg0]
$C$DW$43	.dwtag  DW_TAG_formal_parameter, DW_AT_name("source")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_source")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg12]
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("prescaler")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_prescaler")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _CPUTimer_selectClockSource   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CPUTimer_selectClockSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -2]
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("source")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_source")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -3]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("prescaler")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_prescaler")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |429| 
        MOV       *-SP[3],AR4           ; [CPU_] |429| 
        MOVL      *-SP[2],ACC           ; [CPU_] |429| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 435,column 5,is_stmt
        MOV       ACC,#3088             ; [CPU_] |435| 
        CMPL      ACC,*-SP[2]           ; [CPU_] |435| 
        BF        $C$L4,NEQ             ; [CPU_] |435| 
        ; branchcc occurs ; [] |435| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 437,column 9,is_stmt
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_name("___eallow")
	.dwattr $C$DW$48, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |437| 
        ; call occurs [#___eallow] ; [] |437| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 442,column 9,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |442| 
        AND       *+XAR4[0],#0xfff8     ; [CPU_] |442| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 445,column 9,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |445| 
        OR        *+XAR4[0],AL          ; [CPU_] |445| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 450,column 9,is_stmt
        AND       *+XAR4[0],#0xffc7     ; [CPU_] |450| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 453,column 9,is_stmt
        MOV       ACC,*-SP[4] << #3     ; [CPU_] |453| 
        OR        *+XAR4[0],AL          ; [CPU_] |453| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 456,column 9,is_stmt
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_name("___edis")
	.dwattr $C$DW$49, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |456| 
        ; call occurs [#___edis] ; [] |456| 
	.dwpsn	file "..\ExtraData\driverlib2\cputimer.h",line 458,column 1,is_stmt
$C$L4:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$41, DW_AT_TI_end_file("..\ExtraData\driverlib2\cputimer.h")
	.dwattr $C$DW$41, DW_AT_TI_end_line(0x1ca)
	.dwattr $C$DW$41, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$41

	.sect	".text"
	.clink

$C$DW$51	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_pollCpuTimer")
	.dwattr $C$DW$51, DW_AT_low_pc(_SysCtl_pollCpuTimer)
	.dwattr $C$DW$51, DW_AT_high_pc(0x00)
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_SysCtl_pollCpuTimer")
	.dwattr $C$DW$51, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$51, DW_AT_TI_begin_line(0x56)
	.dwattr $C$DW$51, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$51, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 87,column 1,is_stmt,address _SysCtl_pollCpuTimer

	.dwfde $C$DW$CIE, _SysCtl_pollCpuTimer

;***************************************************************
;* FNAME: _SysCtl_pollCpuTimer          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_SysCtl_pollCpuTimer:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("loopCount")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_loopCount")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -1]
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 88,column 24,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |88| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 95,column 5,is_stmt
        MOV       ACC,#2000             ; [CPU_] |95| 
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$53, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |95| 
        ; call occurs [#_SysCtl_delay] ; [] |95| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 105,column 9,is_stmt
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 105,column 15,is_stmt
        MOVL      XAR4,#3088            ; [CPU_U] |105| 
        MOVL      ACC,XAR4              ; [CPU_] |105| 
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_name("_CPUTimer_getTimerOverflowStatus")
	.dwattr $C$DW$54, DW_AT_TI_call
        LCR       #_CPUTimer_getTimerOverflowStatus ; [CPU_] |105| 
        ; call occurs [#_CPUTimer_getTimerOverflowStatus] ; [] |105| 
        CMPB      AL,#0                 ; [CPU_] |105| 
        BF        $C$L5,EQ              ; [CPU_] |105| 
        ; branchcc occurs ; [] |105| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 116,column 9,is_stmt
        MOVL      ACC,XAR4              ; [CPU_] |116| 
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_name("_CPUTimer_clearOverflowFlag")
	.dwattr $C$DW$55, DW_AT_TI_call
        LCR       #_CPUTimer_clearOverflowFlag ; [CPU_] |116| 
        ; call occurs [#_CPUTimer_clearOverflowFlag] ; [] |116| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 121,column 9,is_stmt
        INC       *-SP[1]               ; [CPU_] |121| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 123,column 12,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |123| 
        CMPB      AL,#4                 ; [CPU_] |123| 
        B         $C$L5,LO              ; [CPU_] |123| 
        ; branchcc occurs ; [] |123| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 124,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$51, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$51, DW_AT_TI_end_line(0x7c)
	.dwattr $C$DW$51, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$51

	.sect	".text"
	.clink
	.global	_SysCtl_getClock

$C$DW$57	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_getClock")
	.dwattr $C$DW$57, DW_AT_low_pc(_SysCtl_getClock)
	.dwattr $C$DW$57, DW_AT_high_pc(0x00)
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_SysCtl_getClock")
	.dwattr $C$DW$57, DW_AT_external
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$57, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$57, DW_AT_TI_begin_line(0x84)
	.dwattr $C$DW$57, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$57, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 133,column 1,is_stmt,address _SysCtl_getClock

	.dwfde $C$DW$CIE, _SysCtl_getClock
$C$DW$58	.dwtag  DW_TAG_formal_parameter, DW_AT_name("clockInHz")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_clockInHz")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_getClock              FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_SysCtl_getClock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("clockInHz")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_clockInHz")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg20 -2]
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg20 -4]
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("oscSource")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_oscSource")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -6]
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("clockOut")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_clockOut")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[2],ACC           ; [CPU_] |133| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 141,column 5,is_stmt
$C$DW$63	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$63, DW_AT_low_pc(0x00)
	.dwattr $C$DW$63, DW_AT_name("_SysCtl_isMCDClockFailureDetected")
	.dwattr $C$DW$63, DW_AT_TI_call
        LCR       #_SysCtl_isMCDClockFailureDetected ; [CPU_] |141| 
        ; call occurs [#_SysCtl_isMCDClockFailureDetected] ; [] |141| 
        CMPB      AL,#0                 ; [CPU_] |141| 
        BF        $C$L6,EQ              ; [CPU_] |141| 
        ; branchcc occurs ; [] |141| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 147,column 9,is_stmt
        MOV       AL,#38528             ; [CPU_] |147| 
        MOV       AH,#152               ; [CPU_] |147| 
        MOVL      *-SP[8],ACC           ; [CPU_] |147| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 148,column 5,is_stmt
        B         $C$L11,UNC            ; [CPU_] |148| 
        ; branch occurs ; [] |148| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 155,column 9,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |155| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |155| 
        MOVB      AH,#0                 ; [CPU_] |155| 
        ANDB      AL,#0x03              ; [CPU_] |155| 
        MOVL      *-SP[6],ACC           ; [CPU_] |155| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 158,column 9,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |158| 
        BF        $C$L7,EQ              ; [CPU_] |158| 
        ; branchcc occurs ; [] |158| 
        MOVB      ACC,#2                ; [CPU_] |158| 
        CMPL      ACC,*-SP[6]           ; [CPU_] |158| 
        BF        $C$L8,NEQ             ; [CPU_] |158| 
        ; branchcc occurs ; [] |158| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 161,column 13,is_stmt
        MOV       AL,#38528             ; [CPU_] |161| 
        MOV       AH,#152               ; [CPU_] |161| 
        MOVL      *-SP[8],ACC           ; [CPU_] |161| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 162,column 9,is_stmt
        B         $C$L9,UNC             ; [CPU_] |162| 
        ; branch occurs ; [] |162| 
$C$L8:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 165,column 13,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |165| 
        MOVL      *-SP[8],ACC           ; [CPU_] |165| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 171,column 9,is_stmt
        MOVL      XAR4,#381454          ; [CPU_U] |171| 
        MOVB      XAR6,#3               ; [CPU_] |171| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |171| 
        MOVB      AH,#0                 ; [CPU_] |171| 
        ANDB      AL,#0x03              ; [CPU_] |171| 
        CMPL      ACC,XAR6              ; [CPU_] |171| 
        BF        $C$L10,NEQ            ; [CPU_] |171| 
        ; branchcc occurs ; [] |171| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 177,column 13,is_stmt
        MOVL      XAR4,#381460          ; [CPU_U] |177| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |177| 
        MOVB      AH,#0                 ; [CPU_] |177| 
        AND       AL,#0x0300            ; [CPU_] |177| 
        SFR       ACC,8                 ; [CPU_] |177| 
        MOVL      XT,ACC                ; [CPU_] |177| 
        IMPYL     ACC,XT,*-SP[2]        ; [CPU_] |177| 
        SFR       ACC,2                 ; [CPU_] |177| 
        MOVL      *-SP[4],ACC           ; [CPU_] |177| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 184,column 13,is_stmt
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |184| 
        MOVB      AH,#0                 ; [CPU_] |184| 
        ANDB      AL,#0x7f              ; [CPU_] |184| 
        MOVL      XT,ACC                ; [CPU_] |184| 
        IMPYXUL   P,XT,*-SP[8]          ; [CPU_] |184| 
        MOVL      *-SP[8],P             ; [CPU_] |184| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 191,column 13,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |191| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |191| 
        MOVL      *-SP[8],ACC           ; [CPU_] |191| 
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 194,column 9,is_stmt
        MOVL      XAR4,#381474          ; [CPU_U] |194| 
        MOVB      XAR6,#0               ; [CPU_] |194| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |194| 
        MOVB      AH,#0                 ; [CPU_] |194| 
        ANDB      AL,#0x3f              ; [CPU_] |194| 
        CMPL      ACC,XAR6              ; [CPU_] |194| 
        BF        $C$L11,EQ             ; [CPU_] |194| 
        ; branchcc occurs ; [] |194| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 197,column 13,is_stmt
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |197| 
        MOVB      AH,#0                 ; [CPU_] |197| 
        MOVL      P,*-SP[8]             ; [CPU_] |197| 
        ANDB      AL,#0x3f              ; [CPU_] |197| 
        LSL       ACC,1                 ; [CPU_] |197| 
        MOVL      XAR6,ACC              ; [CPU_] |197| 
        MOVB      ACC,#0                ; [CPU_] |197| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |197| 
        MOVL      *-SP[8],P             ; [CPU_] |197| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 202,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |202| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 203,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$64	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$64, DW_AT_low_pc(0x00)
	.dwattr $C$DW$64, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$57, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$57, DW_AT_TI_end_line(0xcb)
	.dwattr $C$DW$57, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$57

	.sect	".text"
	.clink
	.global	_SysCtl_getAuxClock

$C$DW$65	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_getAuxClock")
	.dwattr $C$DW$65, DW_AT_low_pc(_SysCtl_getAuxClock)
	.dwattr $C$DW$65, DW_AT_high_pc(0x00)
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_SysCtl_getAuxClock")
	.dwattr $C$DW$65, DW_AT_external
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$65, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$65, DW_AT_TI_begin_line(0xd2)
	.dwattr $C$DW$65, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$65, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 211,column 1,is_stmt,address _SysCtl_getAuxClock

	.dwfde $C$DW$CIE, _SysCtl_getAuxClock
$C$DW$66	.dwtag  DW_TAG_formal_parameter, DW_AT_name("clockInHz")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_clockInHz")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_getAuxClock           FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_SysCtl_getAuxClock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("clockInHz")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_clockInHz")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -2]
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_breg20 -4]
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("oscSource")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_oscSource")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_breg20 -6]
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("clockOut")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_clockOut")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[2],ACC           ; [CPU_] |211| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 216,column 5,is_stmt
        MOVL      XAR4,#381450          ; [CPU_U] |216| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |216| 
        MOVB      AH,#0                 ; [CPU_] |216| 
        ANDB      AL,#0x03              ; [CPU_] |216| 
        MOVL      *-SP[6],ACC           ; [CPU_] |216| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 223,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |223| 
        BF        $C$L12,NEQ            ; [CPU_] |223| 
        ; branchcc occurs ; [] |223| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 228,column 9,is_stmt
        MOV       AL,#38528             ; [CPU_] |228| 
        MOV       AH,#152               ; [CPU_] |228| 
        MOVL      *-SP[8],ACC           ; [CPU_] |228| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 229,column 5,is_stmt
        B         $C$L13,UNC            ; [CPU_] |229| 
        ; branch occurs ; [] |229| 
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 232,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |232| 
        MOVL      *-SP[8],ACC           ; [CPU_] |232| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 238,column 5,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |238| 
        MOVB      XAR6,#3               ; [CPU_] |238| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |238| 
        MOVB      AH,#0                 ; [CPU_] |238| 
        ANDB      AL,#0x03              ; [CPU_] |238| 
        CMPL      ACC,XAR6              ; [CPU_] |238| 
        BF        $C$L14,NEQ            ; [CPU_] |238| 
        ; branchcc occurs ; [] |238| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 244,column 9,is_stmt
        MOVL      XAR4,#381470          ; [CPU_U] |244| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |244| 
        MOVB      AH,#0                 ; [CPU_] |244| 
        AND       AL,#0x0300            ; [CPU_] |244| 
        SFR       ACC,8                 ; [CPU_] |244| 
        MOVL      XT,ACC                ; [CPU_] |244| 
        IMPYL     ACC,XT,*-SP[2]        ; [CPU_] |244| 
        SFR       ACC,2                 ; [CPU_] |244| 
        MOVL      *-SP[4],ACC           ; [CPU_] |244| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 251,column 9,is_stmt
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |251| 
        MOVB      AH,#0                 ; [CPU_] |251| 
        ANDB      AL,#0x7f              ; [CPU_] |251| 
        MOVL      XT,ACC                ; [CPU_] |251| 
        IMPYXUL   P,XT,*-SP[8]          ; [CPU_] |251| 
        MOVL      *-SP[8],P             ; [CPU_] |251| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 258,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |258| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |258| 
        MOVL      *-SP[8],ACC           ; [CPU_] |258| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 261,column 5,is_stmt
        MOVL      XAR4,#381476          ; [CPU_U] |261| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |261| 
        MOVL      P,*-SP[8]             ; [CPU_] |261| 
        ANDB      AL,#0x03              ; [CPU_] |261| 
        MOV       T,AL                  ; [CPU_] |261| 
        MOVB      AL,#1                 ; [CPU_] |261| 
        LSL       AL,T                  ; [CPU_] |261| 
        MOVZ      AR6,AL                ; [CPU_] |261| 
        MOVB      ACC,#0                ; [CPU_] |261| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |261| 
        MOVL      *-SP[8],P             ; [CPU_] |261| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 264,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |264| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 265,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$65, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$65, DW_AT_TI_end_line(0x109)
	.dwattr $C$DW$65, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$65

	.sect	".text"
	.clink
	.global	_SysCtl_setClock

$C$DW$72	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_setClock")
	.dwattr $C$DW$72, DW_AT_low_pc(_SysCtl_setClock)
	.dwattr $C$DW$72, DW_AT_high_pc(0x00)
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_SysCtl_setClock")
	.dwattr $C$DW$72, DW_AT_external
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$72, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$72, DW_AT_TI_begin_line(0x111)
	.dwattr $C$DW$72, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$72, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 274,column 1,is_stmt,address _SysCtl_setClock

	.dwfde $C$DW$CIE, _SysCtl_setClock
$C$DW$73	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_setClock              FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter, 32 Auto,  2 SOE     *
;***************************************************************

_SysCtl_setClock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        MOV32     *SP++,R4H             ; [CPU_] 
        ADDB      SP,#34                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("config")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -4]
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("divSel")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_divSel")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -5]
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("iMult")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_iMult")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -6]
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("fMult")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_fMult")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$77, DW_AT_location[DW_OP_breg20 -7]
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("pllMult")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_pllMult")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_breg20 -8]
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("div")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_div")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_breg20 -9]
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_breg20 -10]
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("sysclkInvalidFreq")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_sysclkInvalidFreq")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_breg20 -11]
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_breg20 -12]
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("tempSCSR")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_tempSCSR")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_breg20 -13]
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("tempWDCR")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_tempWDCR")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -14]
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("tempWDWCR")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_tempWDWCR")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -15]
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("intStatus")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_intStatus")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -16]
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("t1TCR")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_t1TCR")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -17]
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("t1TPR")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_t1TPR")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_breg20 -18]
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("t1TPRH")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_t1TPRH")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -19]
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("t2TCR")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_t2TCR")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_breg20 -20]
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("t2TPR")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_t2TPR")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_breg20 -21]
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("t2TPRH")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_t2TPRH")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_breg20 -22]
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("t2CLKCTL")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_t2CLKCTL")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg20 -23]
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("t1PRD")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_t1PRD")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$94, DW_AT_location[DW_OP_breg20 -26]
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("t2PRD")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_t2PRD")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_breg20 -28]
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("ctr1")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_ctr1")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_breg20 -30]
$C$DW$97	.dwtag  DW_TAG_variable, DW_AT_name("sysclkToInClkError")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_sysclkToInClkError")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_breg20 -32]
$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("mult")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_mult")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_breg20 -34]
        MOVL      *-SP[4],ACC           ; [CPU_] |274| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 276,column 20,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |276| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 276,column 32,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |276| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 276,column 46,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |276| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 277,column 36,is_stmt
        MOVB      *-SP[11],#1,UNC       ; [CPU_] |277| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 291,column 5,is_stmt
$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_name("_SysCtl_isMCDClockFailureDetected")
	.dwattr $C$DW$99, DW_AT_TI_call
        LCR       #_SysCtl_isMCDClockFailureDetected ; [CPU_] |291| 
        ; call occurs [#_SysCtl_isMCDClockFailureDetected] ; [] |291| 
        CMPB      AL,#0                 ; [CPU_] |291| 
        BF        $C$L15,EQ             ; [CPU_] |291| 
        ; branchcc occurs ; [] |291| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 297,column 9,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |297| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 298,column 5,is_stmt
        B         $C$L38,UNC            ; [CPU_] |298| 
        ; branch occurs ; [] |298| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 304,column 9,is_stmt
        MOVL      XAR4,#196608          ; [CPU_U] |304| 
        MOVL      ACC,XAR4              ; [CPU_] |304| 
        AND       AL,*-SP[4]            ; [CPU_] |304| 
        AND       AH,*-SP[3]            ; [CPU_] |304| 
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_name("_SysCtl_selectOscSource")
	.dwattr $C$DW$100, DW_AT_TI_call
        LCR       #_SysCtl_selectOscSource ; [CPU_] |304| 
        ; call occurs [#_SysCtl_selectOscSource] ; [] |304| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 309,column 9,is_stmt
$C$DW$101	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$101, DW_AT_low_pc(0x00)
	.dwattr $C$DW$101, DW_AT_name("___eallow")
	.dwattr $C$DW$101, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |309| 
        ; call occurs [#___eallow] ; [] |309| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 310,column 9,is_stmt
        MOVL      XAR4,#381454          ; [CPU_U] |310| 
        AND       *+XAR4[0],#0xfffd     ; [CPU_] |310| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 312,column 9,is_stmt
$C$DW$102	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$102, DW_AT_low_pc(0x00)
	.dwattr $C$DW$102, DW_AT_name("___edis")
	.dwattr $C$DW$102, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |312| 
        ; call occurs [#___edis] ; [] |312| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 317,column 9,is_stmt
        MOVB      ACC,#23               ; [CPU_] |317| 
$C$DW$103	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$103, DW_AT_low_pc(0x00)
	.dwattr $C$DW$103, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$103, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |317| 
        ; call occurs [#_SysCtl_delay] ; [] |317| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 322,column 9,is_stmt
$C$DW$104	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$104, DW_AT_low_pc(0x00)
	.dwattr $C$DW$104, DW_AT_name("___eallow")
	.dwattr $C$DW$104, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |322| 
        ; call occurs [#___eallow] ; [] |322| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 323,column 9,is_stmt
        MOV       PL,#0                 ; [CPU_] |323| 
        MOV       PH,#32768             ; [CPU_] |323| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |323| 
        AND       ACC,#32768 << 16      ; [CPU_] |323| 
        CMPL      ACC,P                 ; [CPU_] |323| 
        BF        $C$L18,NEQ            ; [CPU_] |323| 
        ; branchcc occurs ; [] |323| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 325,column 13,is_stmt
        MOVL      XAR4,#381228          ; [CPU_U] |325| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |325| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 346,column 13,is_stmt
        MOVL      XAR4,#381474          ; [CPU_U] |346| 
        MOV       *+XAR4[0],#0          ; [CPU_] |346| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 351,column 13,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |351| 
        ANDB      AL,#0x7f              ; [CPU_] |351| 
        OR        *-SP[6],AL            ; [CPU_] |351| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 352,column 13,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#24576            ; [CPU_] |352| 
        AND       AL,*-SP[4]            ; [CPU_] |352| 
        AND       AH,*-SP[3]            ; [CPU_] |352| 
        SFR       ACC,13                ; [CPU_] |352| 
        OR        *-SP[7],AL            ; [CPU_] |352| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 353,column 13,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |353| 
        OR        AL,*-SP[8]            ; [CPU_] |353| 
        MOVZ      AR6,AL                ; [CPU_] |353| 
        MOV       ACC,*-SP[7] << #8     ; [CPU_] |353| 
        OR        AL,AR6                ; [CPU_] |353| 
        MOV       *-SP[8],AL            ; [CPU_] |353| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 361,column 17,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |361| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 361,column 25,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |361| 
        CMPB      AL,#5                 ; [CPU_] |361| 
        B         $C$L18,HIS            ; [CPU_] |361| 
        ; branchcc occurs ; [] |361| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 366,column 17,is_stmt
        MOVL      XAR4,#381454          ; [CPU_U] |366| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |366| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 369,column 17,is_stmt
 RPT #60 || NOP
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 374,column 17,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |374| 
        MOVL      XAR4,#381460          ; [CPU_U] |374| 
        MOV       *+XAR4[0],AL          ; [CPU_] |374| 
        MOVL      XAR4,#381462          ; [CPU_U] |379| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 379,column 17,is_stmt
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 379,column 23,is_stmt
        TBIT      *+XAR4[0],#0          ; [CPU_] |379| 
        BF        $C$L17,NTC            ; [CPU_] |379| 
        ; branchcc occurs ; [] |379| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 361,column 33,is_stmt
        INC       *-SP[12]              ; [CPU_] |361| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 361,column 25,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |361| 
        CMPB      AL,#5                 ; [CPU_] |361| 
        B         $C$L16,LO             ; [CPU_] |361| 
        ; branchcc occurs ; [] |361| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 394,column 9,is_stmt
        AND       AL,*-SP[4],#0x1f80    ; [CPU_] |394| 
        LSR       AL,7                  ; [CPU_] |394| 
        MOV       *-SP[5],AL            ; [CPU_] |394| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 396,column 9,is_stmt
        CMPB      AL,#63                ; [CPU_] |396| 
        BF        $C$L19,EQ             ; [CPU_] |396| 
        ; branchcc occurs ; [] |396| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 398,column 13,is_stmt
        MOVL      XAR4,#381474          ; [CPU_U] |398| 
        MOV       AH,*-SP[5]            ; [CPU_] |398| 
        AND       AL,*+XAR4[0],#0xffc0  ; [CPU_] |398| 
        ADDB      AH,#1                 ; [CPU_] |398| 
        OR        AH,AL                 ; [CPU_] |398| 
        MOV       *+XAR4[0],AH          ; [CPU_] |398| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 401,column 9,is_stmt
        B         $C$L20,UNC            ; [CPU_] |401| 
        ; branch occurs ; [] |401| 
$C$L19:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 404,column 13,is_stmt
        MOVL      XAR4,#381474          ; [CPU_U] |404| 
        AND       AL,*+XAR4[0],#0xffc0  ; [CPU_] |404| 
        OR        AL,*-SP[5]            ; [CPU_] |404| 
        MOV       *+XAR4[0],AL          ; [CPU_] |404| 
$C$L20:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 420,column 9,is_stmt
        MOV       *-SP[13],*(0:0x7022)  ; [CPU_] |420| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 421,column 9,is_stmt
        MOV       *-SP[14],*(0:0x7029)  ; [CPU_] |421| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 422,column 9,is_stmt
        MOV       *-SP[15],*(0:0x702a)  ; [CPU_] |422| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 427,column 9,is_stmt
        MOVB      AL,#0                 ; [CPU_] |427| 
        MOV       *(0:0x702a),AL        ; [CPU_] |427| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 428,column 9,is_stmt
        SPM       #0                    ; [CPU_] 
$C$DW$105	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$105, DW_AT_low_pc(0x00)
	.dwattr $C$DW$105, DW_AT_name("_SysCtl_serviceWatchdog")
	.dwattr $C$DW$105, DW_AT_TI_call
        LCR       #_SysCtl_serviceWatchdog ; [CPU_] |428| 
        ; call occurs [#_SysCtl_serviceWatchdog] ; [] |428| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 433,column 9,is_stmt
        PUSH      ST1                   ; [CPU_] |433| 
        SETC      INTM, DBGM            ; [CPU_] |433| 
        MOV       AL,*--SP              ; [CPU_] |433| 
        MOV       *-SP[16],AL           ; [CPU_] |433| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 438,column 9,is_stmt
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_name("___eallow")
	.dwattr $C$DW$106, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |438| 
        ; call occurs [#___eallow] ; [] |438| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 439,column 9,is_stmt
        MOVB      AL,#0                 ; [CPU_] |439| 
        MOV       *(0:0x7022),AL        ; [CPU_] |439| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 440,column 9,is_stmt
        MOVB      AL,#40                ; [CPU_] |440| 
        MOV       *(0:0x7029),AL        ; [CPU_] |440| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 446,column 9,is_stmt
        MOVL      XAR4,#381228          ; [CPU_U] |446| 
        OR        *+XAR4[0],#0x0001     ; [CPU_] |446| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 451,column 9,is_stmt
        MOVL      XAR4,#381454          ; [CPU_U] |451| 
        OR        *+XAR4[0],#0x0002     ; [CPU_] |451| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 454,column 9,is_stmt
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("___edis")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |454| 
        ; call occurs [#___edis] ; [] |454| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 460,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |460| 
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$108, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |460| 
        ; call occurs [#_SysCtl_delay] ; [] |460| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 471,column 9,is_stmt
        B         $C$L34,UNC            ; [CPU_] |471| 
        ; branch occurs ; [] |471| 
$C$L21:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 474,column 13,is_stmt
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_name("___eallow")
	.dwattr $C$DW$109, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |474| 
        ; call occurs [#___eallow] ; [] |474| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 480,column 13,is_stmt
        MOVL      XAR4,#381462          ; [CPU_U] |480| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |480| 
        ANDB      AL,#0x02              ; [CPU_] |480| 
        CMPB      AL,#1                 ; [CPU_] |480| 
        BF        $C$L23,NEQ            ; [CPU_] |480| 
        ; branchcc occurs ; [] |480| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 486,column 17,is_stmt
        MOVL      XAR4,#381454          ; [CPU_U] |486| 
        AND       *+XAR4[0],#0xfffd     ; [CPU_] |486| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 492,column 17,is_stmt
        MOVB      ACC,#23               ; [CPU_] |492| 
$C$DW$110	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$110, DW_AT_low_pc(0x00)
	.dwattr $C$DW$110, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$110, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |492| 
        ; call occurs [#_SysCtl_delay] ; [] |492| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 497,column 17,is_stmt
        MOVL      XAR4,#381454          ; [CPU_U] |497| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |497| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 500,column 17,is_stmt
        MOVB      ACC,#3                ; [CPU_] |500| 
$C$DW$111	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$111, DW_AT_low_pc(0x00)
	.dwattr $C$DW$111, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$111, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |500| 
        ; call occurs [#_SysCtl_delay] ; [] |500| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 505,column 17,is_stmt
        MOVL      XAR4,#381460          ; [CPU_U] |505| 
        MOV       AL,*-SP[8]            ; [CPU_] |505| 
        OR        *+XAR4[0],AL          ; [CPU_] |505| 
        MOVL      XAR4,#381462          ; [CPU_U] |510| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 510,column 17,is_stmt
$C$L22:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 510,column 23,is_stmt
        TBIT      *+XAR4[0],#0          ; [CPU_] |510| 
        BF        $C$L22,NTC            ; [CPU_] |510| 
        ; branchcc occurs ; [] |510| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 519,column 17,is_stmt
        MOVL      XAR4,#381454          ; [CPU_U] |519| 
        OR        *+XAR4[0],#0x0002     ; [CPU_] |519| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 526,column 17,is_stmt
        MOVB      ACC,#3                ; [CPU_] |526| 
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$112, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |526| 
        ; call occurs [#_SysCtl_delay] ; [] |526| 
$C$L23:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 532,column 13,is_stmt
        MOV       *-SP[17],*(0:0x0c0c)  ; [CPU_] |532| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 533,column 13,is_stmt
        MOVL      XAR4,#3082            ; [CPU_] |533| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |533| 
        MOVL      *-SP[26],ACC          ; [CPU_] |533| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 534,column 13,is_stmt
        MOV       *-SP[18],*(0:0x0c0e)  ; [CPU_] |534| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 535,column 13,is_stmt
        MOV       *-SP[19],*(0:0x0c0f)  ; [CPU_] |535| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 536,column 13,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |536| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |536| 
        MOV       *-SP[23],AL           ; [CPU_] |536| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 537,column 13,is_stmt
        MOV       *-SP[20],*(0:0x0c14)  ; [CPU_] |537| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 538,column 13,is_stmt
        MOVL      XAR4,#3090            ; [CPU_] |538| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |538| 
        MOVL      *-SP[28],ACC          ; [CPU_] |538| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 539,column 13,is_stmt
        MOV       *-SP[21],*(0:0x0c16)  ; [CPU_] |539| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 540,column 13,is_stmt
        MOV       *-SP[22],*(0:0x0c17)  ; [CPU_] |540| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 555,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |555| 
        ORB       AL,#0x10              ; [CPU_] |555| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |555| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 556,column 13,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#3082            ; [CPU_] |556| 
        MOV       ACC,#-8192 << 15      ; [CPU_] |556| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |556| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 557,column 13,is_stmt
        MOVL      XAR4,#3086            ; [CPU_] |557| 
        MOVB      ACC,#0                ; [CPU_] |557| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |557| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 558,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |558| 
        ORB       AL,#0x20              ; [CPU_] |558| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |558| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 559,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |559| 
        OR        AL,#0x8000            ; [CPU_] |559| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |559| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 560,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |560| 
        OR        AL,#0x4000            ; [CPU_] |560| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |560| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 565,column 13,is_stmt
        B         $C$L27,UNC            ; [CPU_] |565| 
        ; branch occurs ; [] |565| 
$C$L24:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 571,column 21,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |571| 
        AND       *+XAR4[0],#0xfff8     ; [CPU_] |571| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 573,column 21,is_stmt
        OR        *+XAR4[0],#0x0001     ; [CPU_] |573| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 574,column 21,is_stmt
        B         $C$L28,UNC            ; [CPU_] |574| 
        ; branch occurs ; [] |574| 
$C$L25:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 580,column 21,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |580| 
        AND       *+XAR4[0],#0xfff8     ; [CPU_] |580| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 582,column 21,is_stmt
        OR        *+XAR4[0],#0x0002     ; [CPU_] |582| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 583,column 21,is_stmt
        B         $C$L28,UNC            ; [CPU_] |583| 
        ; branch occurs ; [] |583| 
$C$L26:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 589,column 21,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |589| 
        AND       *+XAR4[0],#0xfff8     ; [CPU_] |589| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 591,column 21,is_stmt
        OR        *+XAR4[0],#0x0003     ; [CPU_] |591| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 592,column 21,is_stmt
        B         $C$L28,UNC            ; [CPU_] |592| 
        ; branch occurs ; [] |592| 
$C$L27:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 565,column 13,is_stmt
        MOVL      XAR4,#196608          ; [CPU_U] |565| 
        MOVL      P,XAR4                ; [CPU_] |565| 
        MOV       AH,PL                 ; [CPU_] |565| 
        MOV       AL,PH                 ; [CPU_] |565| 
        AND       AH,*-SP[4]            ; [CPU_] |565| 
        AND       AL,*-SP[3]            ; [CPU_] |565| 
        MOV       PL,AH                 ; [CPU_] |565| 
        MOV       PH,AL                 ; [CPU_] |565| 
        MOVB      ACC,#0                ; [CPU_] |565| 
        CMPL      ACC,P                 ; [CPU_] |565| 
        BF        $C$L25,EQ             ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
        MOVL      XAR4,#65536           ; [CPU_U] |565| 
        MOVL      ACC,XAR4              ; [CPU_] |565| 
        CMPL      ACC,P                 ; [CPU_] |565| 
        BF        $C$L26,EQ             ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
        MOVL      XAR4,#131072          ; [CPU_U] |565| 
        MOVL      ACC,XAR4              ; [CPU_] |565| 
        CMPL      ACC,P                 ; [CPU_] |565| 
        BF        $C$L24,EQ             ; [CPU_] |565| 
        ; branchcc occurs ; [] |565| 
$C$L28:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 609,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |609| 
        OR        AL,#0x8000            ; [CPU_] |609| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |609| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 610,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |610| 
        OR        AL,#0x4000            ; [CPU_] |610| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |610| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 611,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |611| 
        ORB       AL,#0x10              ; [CPU_] |611| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |611| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 612,column 13,is_stmt
        MOVL      XAR5,#2048            ; [CPU_U] |612| 
        MOVL      XAR4,#3090            ; [CPU_] |612| 
        MOVL      *+XAR4[0],XAR5        ; [CPU_] |612| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 613,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |613| 
        MOVL      XAR4,#3094            ; [CPU_] |613| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |613| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 614,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |614| 
        ORB       AL,#0x20              ; [CPU_] |614| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |614| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 629,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |629| 
        ORB       AL,#0x10              ; [CPU_] |629| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |629| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 630,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |630| 
        ORB       AL,#0x10              ; [CPU_] |630| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |630| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 631,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |631| 
        ORB       AL,#0x20              ; [CPU_] |631| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |631| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 632,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |632| 
        ORB       AL,#0x20              ; [CPU_] |632| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |632| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 633,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |633| 
        OR        AL,#0x8000            ; [CPU_] |633| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |633| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 634,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |634| 
        AND       AL,#0xffef            ; [CPU_] |634| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |634| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 635,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |635| 
        AND       AL,#0xffef            ; [CPU_] |635| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |635| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 640,column 13,is_stmt
$C$L29:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 640,column 19,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |640| 
        TBIT      AL,#15                ; [CPU_] |640| 
        BF        $C$L30,TC             ; [CPU_] |640| 
        ; branchcc occurs ; [] |640| 
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |640| 
        TBIT      AL,#15                ; [CPU_] |640| 
        BF        $C$L29,NTC            ; [CPU_] |640| 
        ; branchcc occurs ; [] |640| 
$C$L30:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 651,column 13,is_stmt
        MOV       AL,*(0:0x0c0c)        ; [CPU_] |651| 
        ORB       AL,#0x10              ; [CPU_] |651| 
        MOV       *(0:0x0c0c),AL        ; [CPU_] |651| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 652,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |652| 
        ORB       AL,#0x10              ; [CPU_] |652| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |652| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 657,column 13,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#3080            ; [CPU_] |657| 
        MOV       ACC,#-8192 << 15      ; [CPU_] |657| 
        SUBL      ACC,*+XAR4[0]         ; [CPU_] |657| 
        MOVL      *-SP[30],ACC          ; [CPU_] |657| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 663,column 13,is_stmt
        MOV       *(0:0x0c0c),*-SP[17]  ; [CPU_] |663| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 664,column 13,is_stmt
        MOVL      XAR4,#3082            ; [CPU_] |664| 
        MOVL      ACC,*-SP[26]          ; [CPU_] |664| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |664| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 665,column 13,is_stmt
        MOV       *(0:0x0c0e),*-SP[18]  ; [CPU_] |665| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 666,column 13,is_stmt
        MOV       *(0:0x0c0f),*-SP[19]  ; [CPU_] |666| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 667,column 13,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |667| 
        MOV       AL,*-SP[23]           ; [CPU_] |667| 
        MOV       *+XAR4[0],AL          ; [CPU_] |667| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 668,column 13,is_stmt
        MOV       *(0:0x0c14),*-SP[20]  ; [CPU_] |668| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 669,column 13,is_stmt
        MOVL      XAR4,#3090            ; [CPU_] |669| 
        MOVL      ACC,*-SP[28]          ; [CPU_] |669| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |669| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 670,column 13,is_stmt
        MOV       *(0:0x0c16),*-SP[21]  ; [CPU_] |670| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 671,column 13,is_stmt
        MOV       *(0:0x0c17),*-SP[22]  ; [CPU_] |671| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 677,column 13,is_stmt
        UI16TOF32 R0H,*-SP[7]           ; [CPU_] |677| 
        MOVIZ     R1H,#16512            ; [CPU_] |677| 
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$113, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |677| 
        ; call occurs [#FS$$DIV] ; [] |677| 
        UI16TOF32 R1H,*-SP[6]           ; [CPU_] |677| 
        NOP       ; [CPU_] 
        ADDF32    R0H,R0H,R1H           ; [CPU_] |677| 
        NOP       ; [CPU_] 
        MOV32     *-SP[34],R0H          ; [CPU_] |677| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 679,column 13,is_stmt
        MOVL      XAR4,#381474          ; [CPU_U] |679| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |679| 
        ANDB      AL,#0x3f              ; [CPU_] |679| 
        BF        $C$L31,NEQ            ; [CPU_] |679| 
        ; branchcc occurs ; [] |679| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 681,column 17,is_stmt
        MOVB      *-SP[9],#1,UNC        ; [CPU_] |681| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 682,column 13,is_stmt
        B         $C$L32,UNC            ; [CPU_] |682| 
        ; branch occurs ; [] |682| 
$C$L31:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 685,column 17,is_stmt
        MOV       AL,*+XAR4[0]          ; [CPU_] |685| 
        ANDB      AL,#0x3f              ; [CPU_] |685| 
        LSL       AL,1                  ; [CPU_] |685| 
        MOV       *-SP[9],AL            ; [CPU_] |685| 
$C$L32:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 689,column 13,is_stmt
        UI32TOF32 R0H,*-SP[30]          ; [CPU_] |689| 
        MOVIZ     R1H,#17664            ; [CPU_] |689| 
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$114, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |689| 
        ; call occurs [#FS$$DIV] ; [] |689| 
        UI16TOF32 R1H,*-SP[9]           ; [CPU_] |689| 
        MOV32     R4H,R0H               ; [CPU_] |689| 
        MOV32     R0H,*-SP[34]          ; [CPU_] |689| 
$C$DW$115	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$115, DW_AT_low_pc(0x00)
	.dwattr $C$DW$115, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$115, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |689| 
        ; call occurs [#FS$$DIV] ; [] |689| 
        SUBF32    R0H,R0H,R4H           ; [CPU_] |689| 
        NOP       ; [CPU_] 
        MOV32     *-SP[32],R0H          ; [CPU_] |689| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 696,column 13,is_stmt
        MOVB      AL,#1                 ; [CPU_] |696| 
        MOVIZ     R0H,#15820            ; [CPU_] |696| 
        MOV32     R1H,*-SP[32]          ; [CPU_] |696| 
        MOVXI     R0H,#52429            ; [CPU_] |696| 
        CMPF32    R1H,R0H               ; [CPU_] |696| 
        MOVST0    ZF, NF                ; [CPU_] |696| 
        B         $C$L33,GT             ; [CPU_] |696| 
        ; branchcc occurs ; [] |696| 
        MOVIZ     R0H,#48588            ; [CPU_] |696| 
        MOVXI     R0H,#52429            ; [CPU_] |696| 
        CMPF32    R1H,R0H               ; [CPU_] |696| 
        MOVST0    ZF, NF                ; [CPU_] |696| 
        B         $C$L33,LT             ; [CPU_] |696| 
        ; branchcc occurs ; [] |696| 
        MOVB      AL,#0                 ; [CPU_] |696| 
$C$L33:    
        MOV       *-SP[11],AL           ; [CPU_] |696| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 699,column 13,is_stmt
$C$DW$116	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$116, DW_AT_low_pc(0x00)
	.dwattr $C$DW$116, DW_AT_name("___edis")
	.dwattr $C$DW$116, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |699| 
        ; call occurs [#___edis] ; [] |699| 
$C$L34:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 471,column 15,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |471| 
        MOV       PL,#0                 ; [CPU_] |471| 
        MOV       PH,#32768             ; [CPU_] |471| 
        AND       ACC,#32768 << 16      ; [CPU_] |471| 
        CMPL      ACC,P                 ; [CPU_] |471| 
        BF        $C$L35,NEQ            ; [CPU_] |471| 
        ; branchcc occurs ; [] |471| 
        MOV       AL,*-SP[11]           ; [CPU_] |471| 
        CMPB      AL,#1                 ; [CPU_] |471| 
        BF        $C$L21,EQ             ; [CPU_] |471| 
        ; branchcc occurs ; [] |471| 
$C$L35:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 705,column 9,is_stmt
$C$DW$117	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$117, DW_AT_low_pc(0x00)
	.dwattr $C$DW$117, DW_AT_name("___eallow")
	.dwattr $C$DW$117, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |705| 
        ; call occurs [#___eallow] ; [] |705| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 706,column 9,is_stmt
        MOVL      XAR4,#381228          ; [CPU_U] |706| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |706| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 707,column 9,is_stmt
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_name("___edis")
	.dwattr $C$DW$118, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |707| 
        ; call occurs [#___edis] ; [] |707| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 712,column 9,is_stmt
$C$DW$119	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$119, DW_AT_low_pc(0x00)
	.dwattr $C$DW$119, DW_AT_name("_SysCtl_serviceWatchdog")
	.dwattr $C$DW$119, DW_AT_TI_call
        LCR       #_SysCtl_serviceWatchdog ; [CPU_] |712| 
        ; call occurs [#_SysCtl_serviceWatchdog] ; [] |712| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 717,column 9,is_stmt
$C$DW$120	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$120, DW_AT_low_pc(0x00)
	.dwattr $C$DW$120, DW_AT_name("___eallow")
	.dwattr $C$DW$120, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |717| 
        ; call occurs [#___eallow] ; [] |717| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 718,column 9,is_stmt
        MOV       AL,*-SP[14]           ; [CPU_] |718| 
        ORB       AL,#0x28              ; [CPU_] |718| 
        MOV       *(0:0x7029),AL        ; [CPU_] |718| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 719,column 9,is_stmt
        MOV       *(0:0x702a),*-SP[15]  ; [CPU_] |719| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 720,column 9,is_stmt
        AND       AL,*-SP[13],#0xfffe   ; [CPU_] |720| 
        MOV       *(0:0x7022),AL        ; [CPU_] |720| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 721,column 9,is_stmt
$C$DW$121	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$121, DW_AT_low_pc(0x00)
	.dwattr $C$DW$121, DW_AT_name("___edis")
	.dwattr $C$DW$121, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |721| 
        ; call occurs [#___edis] ; [] |721| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 727,column 9,is_stmt
        TBIT      *-SP[16],#0           ; [CPU_] |727| 
        BF        $C$L36,TC             ; [CPU_] |727| 
        ; branchcc occurs ; [] |727| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 729,column 13,is_stmt
 clrc INTM
$C$L36:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 736,column 9,is_stmt
        TBIT      *-SP[16],#1           ; [CPU_] |736| 
        BF        $C$L37,TC             ; [CPU_] |736| 
        ; branchcc occurs ; [] |736| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 738,column 13,is_stmt
 CLRC DBGM
$C$L37:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 745,column 9,is_stmt
        SPM       #0                    ; [CPU_] 
        MOVB      ACC,#40               ; [CPU_] |745| 
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$122, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |745| 
        ; call occurs [#_SysCtl_delay] ; [] |745| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 750,column 9,is_stmt
$C$DW$123	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$123, DW_AT_low_pc(0x00)
	.dwattr $C$DW$123, DW_AT_name("___eallow")
	.dwattr $C$DW$123, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |750| 
        ; call occurs [#___eallow] ; [] |750| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 751,column 9,is_stmt
        MOVL      XAR4,#381474          ; [CPU_U] |751| 
        AND       AL,*+XAR4[0],#0xffc0  ; [CPU_] |751| 
        OR        AL,*-SP[5]            ; [CPU_] |751| 
        MOV       *+XAR4[0],AL          ; [CPU_] |751| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 754,column 9,is_stmt
$C$DW$124	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$124, DW_AT_low_pc(0x00)
	.dwattr $C$DW$124, DW_AT_name("___edis")
	.dwattr $C$DW$124, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |754| 
        ; call occurs [#___edis] ; [] |754| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 756,column 9,is_stmt
        MOVB      *-SP[10],#1,UNC       ; [CPU_] |756| 
$C$L38:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 759,column 5,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |759| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 760,column 1,is_stmt
        SUBB      SP,#34                ; [CPU_U] 
	.dwcfi	cfa_offset, -4
        MOV32     R4H,*--SP             ; [CPU_] 
$C$DW$125	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$125, DW_AT_low_pc(0x00)
	.dwattr $C$DW$125, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$72, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$72, DW_AT_TI_end_line(0x2f8)
	.dwattr $C$DW$72, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$72

	.sect	".text"
	.clink
	.global	_SysCtl_setAuxClock

$C$DW$126	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_setAuxClock")
	.dwattr $C$DW$126, DW_AT_low_pc(_SysCtl_setAuxClock)
	.dwattr $C$DW$126, DW_AT_high_pc(0x00)
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_SysCtl_setAuxClock")
	.dwattr $C$DW$126, DW_AT_external
	.dwattr $C$DW$126, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$126, DW_AT_TI_begin_line(0x2fe)
	.dwattr $C$DW$126, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$126, DW_AT_TI_max_frame_size(-16)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 767,column 1,is_stmt,address _SysCtl_setAuxClock

	.dwfde $C$DW$CIE, _SysCtl_setAuxClock
$C$DW$127	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_setAuxClock           FR SIZE:  14           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 14 Auto,  0 SOE     *
;***************************************************************

_SysCtl_setAuxClock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -16
$C$DW$128	.dwtag  DW_TAG_variable, DW_AT_name("config")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_breg20 -2]
$C$DW$129	.dwtag  DW_TAG_variable, DW_AT_name("pllMult")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_pllMult")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_breg20 -3]
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("counter")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_counter")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_breg20 -4]
$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("started")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_started")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$131, DW_AT_location[DW_OP_breg20 -5]
$C$DW$132	.dwtag  DW_TAG_variable, DW_AT_name("attempts")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_attempts")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$132, DW_AT_location[DW_OP_breg20 -6]
$C$DW$133	.dwtag  DW_TAG_variable, DW_AT_name("mult")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_mult")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_breg20 -7]
$C$DW$134	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_breg20 -8]
$C$DW$135	.dwtag  DW_TAG_variable, DW_AT_name("t2TCR")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_t2TCR")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_breg20 -9]
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("t2TPR")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_t2TPR")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_breg20 -10]
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("t2TPRH")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_t2TPRH")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -11]
$C$DW$138	.dwtag  DW_TAG_variable, DW_AT_name("t2CLKCTL")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_t2CLKCTL")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_breg20 -12]
$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("t2PRD")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_t2PRD")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_breg20 -14]
        MOVL      *-SP[2],ACC           ; [CPU_] |767| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 768,column 22,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |768| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 769,column 22,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |769| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 769,column 36,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |769| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 769,column 51,is_stmt
        MOV       *-SP[6],#0            ; [CPU_] |769| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 782,column 5,is_stmt
$C$DW$140	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$140, DW_AT_low_pc(0x00)
	.dwattr $C$DW$140, DW_AT_name("___eallow")
	.dwattr $C$DW$140, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |782| 
        ; call occurs [#___eallow] ; [] |782| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 783,column 5,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |783| 
        AND       *+XAR4[0],#0xfffd     ; [CPU_] |783| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 784,column 5,is_stmt
$C$DW$141	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$141, DW_AT_low_pc(0x00)
	.dwattr $C$DW$141, DW_AT_name("___edis")
	.dwattr $C$DW$141, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |784| 
        ; call occurs [#___edis] ; [] |784| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 789,column 5,is_stmt
        MOVB      ACC,#23               ; [CPU_] |789| 
$C$DW$142	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$142, DW_AT_low_pc(0x00)
	.dwattr $C$DW$142, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$142, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |789| 
        ; call occurs [#_SysCtl_delay] ; [] |789| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 794,column 5,is_stmt
        MOVL      XAR4,#196608          ; [CPU_U] |794| 
        MOVL      ACC,XAR4              ; [CPU_] |794| 
        AND       AL,*-SP[2]            ; [CPU_] |794| 
        AND       AH,*-SP[1]            ; [CPU_] |794| 
$C$DW$143	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$143, DW_AT_low_pc(0x00)
	.dwattr $C$DW$143, DW_AT_name("_SysCtl_selectOscSourceAuxPLL")
	.dwattr $C$DW$143, DW_AT_TI_call
        LCR       #_SysCtl_selectOscSourceAuxPLL ; [CPU_] |794| 
        ; call occurs [#_SysCtl_selectOscSourceAuxPLL] ; [] |794| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 799,column 5,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |799| 
        ANDB      AL,#0x7f              ; [CPU_] |799| 
        OR        *-SP[3],AL            ; [CPU_] |799| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 801,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#24576            ; [CPU_] |801| 
        AND       AL,*-SP[2]            ; [CPU_] |801| 
        AND       AH,*-SP[1]            ; [CPU_] |801| 
        SFR       ACC,13                ; [CPU_] |801| 
        MOV       ACC,AL << #8          ; [CPU_] |801| 
        OR        *-SP[3],AL            ; [CPU_] |801| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 807,column 5,is_stmt
        MOVL      XAR4,#381470          ; [CPU_U] |807| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |807| 
        ANDB      AL,#127               ; [CPU_] |807| 
        MOV       *-SP[7],AL            ; [CPU_] |807| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 810,column 5,is_stmt
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |810| 
        AND       AL,AL,#0x0300         ; [CPU_] |810| 
        OR        *-SP[7],AL            ; [CPU_] |810| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 816,column 5,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |816| 
        MOVU      ACC,*-SP[3]           ; [CPU_] |816| 
        CMPL      ACC,XAR6              ; [CPU_] |816| 
        BF        $C$L47,EQ             ; [CPU_] |816| 
        ; branchcc occurs ; [] |816| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 822,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |822| 
        MOV       PL,#0                 ; [CPU_] |822| 
        MOV       PH,#32768             ; [CPU_] |822| 
        AND       ACC,#32768 << 16      ; [CPU_] |822| 
        CMPL      ACC,P                 ; [CPU_] |822| 
        BF        $C$L50,NEQ            ; [CPU_] |822| 
        ; branchcc occurs ; [] |822| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 827,column 13,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |827| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |827| 
        MOV       *-SP[12],AL           ; [CPU_] |827| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 828,column 13,is_stmt
        MOV       *-SP[9],*(0:0x0c14)   ; [CPU_] |828| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 829,column 13,is_stmt
        MOVL      XAR4,#3090            ; [CPU_] |829| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |829| 
        MOVL      *-SP[14],ACC          ; [CPU_] |829| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 830,column 13,is_stmt
        MOV       *-SP[10],*(0:0x0c16)  ; [CPU_] |830| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 831,column 13,is_stmt
        MOV       *-SP[11],*(0:0x0c17)  ; [CPU_] |831| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 840,column 13,is_stmt
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_name("___eallow")
	.dwattr $C$DW$144, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |840| 
        ; call occurs [#___eallow] ; [] |840| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 841,column 13,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |841| 
        MOVB      *+XAR4[0],#6,UNC      ; [CPU_] |841| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 843,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |843| 
        ORB       AL,#0x10              ; [CPU_] |843| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |843| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 845,column 13,is_stmt
        MOVL      XAR4,#3090            ; [CPU_] |845| 
        MOVB      ACC,#10               ; [CPU_] |845| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |845| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 846,column 13,is_stmt
        MOVB      AL,#0                 ; [CPU_] |846| 
        MOV       *(0:0x0c16),AL        ; [CPU_] |846| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 847,column 13,is_stmt
        MOVB      AL,#0                 ; [CPU_] |847| 
        MOV       *(0:0x0c17),AL        ; [CPU_] |847| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 848,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |848| 
        AND       AL,#0xbfff            ; [CPU_] |848| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |848| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 854,column 13,is_stmt
        MOVL      XAR4,#381476          ; [CPU_U] |854| 
        MOVB      *+XAR4[0],#3,UNC      ; [CPU_] |854| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 855,column 13,is_stmt
$C$DW$145	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$145, DW_AT_low_pc(0x00)
	.dwattr $C$DW$145, DW_AT_name("___edis")
	.dwattr $C$DW$145, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |855| 
        ; call occurs [#___edis] ; [] |855| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 862,column 13,is_stmt
        B         $C$L44,UNC            ; [CPU_] |862| 
        ; branch occurs ; [] |862| 
$C$L39:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 864,column 17,is_stmt
$C$DW$146	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$146, DW_AT_low_pc(0x00)
	.dwattr $C$DW$146, DW_AT_name("___eallow")
	.dwattr $C$DW$146, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |864| 
        ; call occurs [#___eallow] ; [] |864| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 869,column 17,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |869| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |869| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 871,column 17,is_stmt
        MOVB      ACC,#3                ; [CPU_] |871| 
$C$DW$147	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$147, DW_AT_low_pc(0x00)
	.dwattr $C$DW$147, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$147, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |871| 
        ; call occurs [#_SysCtl_delay] ; [] |871| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 877,column 17,is_stmt
        MOVL      XAR4,#381470          ; [CPU_U] |877| 
        MOV       AL,*-SP[3]            ; [CPU_] |877| 
        OR        *+XAR4[0],AL          ; [CPU_] |877| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 882,column 17,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |882| 
        OR        *+XAR4[0],#0x0001     ; [CPU_] |882| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 884,column 17,is_stmt
$C$DW$148	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$148, DW_AT_low_pc(0x00)
	.dwattr $C$DW$148, DW_AT_name("___edis")
	.dwattr $C$DW$148, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |884| 
        ; call occurs [#___edis] ; [] |884| 
        MOVL      XAR4,#381472          ; [CPU_U] |890| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 890,column 17,is_stmt
$C$L40:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 890,column 23,is_stmt
        MOV       AL,*+XAR4[0]          ; [CPU_] |890| 
        ANDB      AL,#0x01              ; [CPU_] |890| 
        CMPB      AL,#1                 ; [CPU_] |890| 
        BF        $C$L40,NEQ            ; [CPU_] |890| 
        ; branchcc occurs ; [] |890| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 903,column 17,is_stmt
$C$DW$149	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$149, DW_AT_low_pc(0x00)
	.dwattr $C$DW$149, DW_AT_name("___eallow")
	.dwattr $C$DW$149, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |903| 
        ; call occurs [#___eallow] ; [] |903| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 904,column 17,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |904| 
        OR        *+XAR4[0],#0x0002     ; [CPU_] |904| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 906,column 17,is_stmt
        MOVB      ACC,#3                ; [CPU_] |906| 
$C$DW$150	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$150, DW_AT_low_pc(0x00)
	.dwattr $C$DW$150, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$150, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |906| 
        ; call occurs [#_SysCtl_delay] ; [] |906| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 914,column 17,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |914| 
        ORB       AL,#0x20              ; [CPU_] |914| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |914| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 915,column 17,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |915| 
        AND       AL,#0xffef            ; [CPU_] |915| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |915| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 920,column 21,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |920| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 920,column 29,is_stmt
        CMP       *-SP[8],#1000         ; [CPU_] |920| 
        B         $C$L43,HIS            ; [CPU_] |920| 
        ; branchcc occurs ; [] |920| 
$C$L41:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 925,column 21,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |925| 
        TBIT      AL,#15                ; [CPU_] |925| 
        BF        $C$L42,NTC            ; [CPU_] |925| 
        ; branchcc occurs ; [] |925| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 931,column 25,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |931| 
        OR        AL,#0x8000            ; [CPU_] |931| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |931| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 938,column 25,is_stmt
        MOVB      *-SP[5],#1,UNC        ; [CPU_] |938| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 939,column 25,is_stmt
        B         $C$L43,UNC            ; [CPU_] |939| 
        ; branch occurs ; [] |939| 
$C$L42:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 920,column 40,is_stmt
        INC       *-SP[8]               ; [CPU_] |920| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 920,column 29,is_stmt
        CMP       *-SP[8],#1000         ; [CPU_] |920| 
        B         $C$L41,LO             ; [CPU_] |920| 
        ; branchcc occurs ; [] |920| 
$C$L43:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 946,column 17,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |946| 
        ORB       AL,#0x10              ; [CPU_] |946| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |946| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 947,column 17,is_stmt
        INC       *-SP[4]               ; [CPU_] |947| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 948,column 17,is_stmt
$C$DW$151	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$151, DW_AT_low_pc(0x00)
	.dwattr $C$DW$151, DW_AT_name("___edis")
	.dwattr $C$DW$151, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |948| 
        ; call occurs [#___edis] ; [] |948| 
$C$L44:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 862,column 19,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |862| 
        CMPB      AL,#5                 ; [CPU_] |862| 
        B         $C$L45,HIS            ; [CPU_] |862| 
        ; branchcc occurs ; [] |862| 
        MOV       AL,*-SP[5]            ; [CPU_] |862| 
        BF        $C$L39,EQ             ; [CPU_] |862| 
        ; branchcc occurs ; [] |862| 
$C$L45:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 951,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |951| 
        BF        $C$L46,NEQ            ; [CPU_] |951| 
        ; branchcc occurs ; [] |951| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 957,column 17,is_stmt
$C$DW$152	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$152, DW_AT_low_pc(0x00)
	.dwattr $C$DW$152, DW_AT_name("___eallow")
	.dwattr $C$DW$152, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |957| 
        ; call occurs [#___eallow] ; [] |957| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 958,column 17,is_stmt
        MOVL      XAR4,#381470          ; [CPU_U] |958| 
        MOV       *+XAR4[0],#0          ; [CPU_] |958| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 959,column 17,is_stmt
$C$DW$153	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$153, DW_AT_low_pc(0x00)
	.dwattr $C$DW$153, DW_AT_name("___edis")
	.dwattr $C$DW$153, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |959| 
        ; call occurs [#___edis] ; [] |959| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 965,column 17,is_stmt
 ESTOP0
$C$L46:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 971,column 13,is_stmt
        SPM       #0                    ; [CPU_] 
$C$DW$154	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$154, DW_AT_low_pc(0x00)
	.dwattr $C$DW$154, DW_AT_name("___eallow")
	.dwattr $C$DW$154, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |971| 
        ; call occurs [#___eallow] ; [] |971| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 972,column 13,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |972| 
        MOV       AL,*-SP[12]           ; [CPU_] |972| 
        MOV       *+XAR4[0],AL          ; [CPU_] |972| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 973,column 13,is_stmt
        MOV       *(0:0x0c14),*-SP[9]   ; [CPU_] |973| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 974,column 13,is_stmt
        MOVL      XAR4,#3090            ; [CPU_] |974| 
        MOVL      ACC,*-SP[14]          ; [CPU_] |974| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |974| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 975,column 13,is_stmt
        MOV       *(0:0x0c16),*-SP[10]  ; [CPU_] |975| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 976,column 13,is_stmt
        MOV       *(0:0x0c17),*-SP[11]  ; [CPU_] |976| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 981,column 13,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |981| 
        ORB       AL,#0x20              ; [CPU_] |981| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |981| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 982,column 13,is_stmt
$C$DW$155	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$155, DW_AT_low_pc(0x00)
	.dwattr $C$DW$155, DW_AT_name("___edis")
	.dwattr $C$DW$155, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |982| 
        ; call occurs [#___edis] ; [] |982| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 984,column 5,is_stmt
        B         $C$L50,UNC            ; [CPU_] |984| 
        ; branch occurs ; [] |984| 
$C$L47:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 990,column 9,is_stmt
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_name("___eallow")
	.dwattr $C$DW$156, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |990| 
        ; call occurs [#___eallow] ; [] |990| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 991,column 9,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |991| 
        OR        *+XAR4[0],#0x0002     ; [CPU_] |991| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 992,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |992| 
$C$DW$157	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$157, DW_AT_low_pc(0x00)
	.dwattr $C$DW$157, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$157, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |992| 
        ; call occurs [#_SysCtl_delay] ; [] |992| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 993,column 9,is_stmt
$C$DW$158	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$158, DW_AT_low_pc(0x00)
	.dwattr $C$DW$158, DW_AT_name("___edis")
	.dwattr $C$DW$158, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |993| 
        ; call occurs [#___edis] ; [] |993| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1000,column 5,is_stmt
        B         $C$L50,UNC            ; [CPU_] |1000| 
        ; branch occurs ; [] |1000| 
$C$L48:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1004,column 9,is_stmt
$C$DW$159	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$159, DW_AT_low_pc(0x00)
	.dwattr $C$DW$159, DW_AT_name("___eallow")
	.dwattr $C$DW$159, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1004| 
        ; call occurs [#___eallow] ; [] |1004| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1009,column 9,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |1009| 
        AND       *+XAR4[0],#0xfffd     ; [CPU_] |1009| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1015,column 9,is_stmt
        MOVB      ACC,#23               ; [CPU_] |1015| 
$C$DW$160	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$160, DW_AT_low_pc(0x00)
	.dwattr $C$DW$160, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$160, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |1015| 
        ; call occurs [#_SysCtl_delay] ; [] |1015| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1020,column 9,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |1020| 
        AND       *+XAR4[0],#0xfffe     ; [CPU_] |1020| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1021,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |1021| 
$C$DW$161	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$161, DW_AT_low_pc(0x00)
	.dwattr $C$DW$161, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$161, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |1021| 
        ; call occurs [#_SysCtl_delay] ; [] |1021| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1027,column 9,is_stmt
        MOVL      XAR4,#381470          ; [CPU_U] |1027| 
        MOV       AL,*-SP[3]            ; [CPU_] |1027| 
        OR        *+XAR4[0],AL          ; [CPU_] |1027| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1032,column 9,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |1032| 
        OR        *+XAR4[0],#0x0001     ; [CPU_] |1032| 
        MOVL      XAR4,#381472          ; [CPU_U] |1037| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1037,column 9,is_stmt
$C$L49:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1037,column 15,is_stmt
        MOV       AL,*+XAR4[0]          ; [CPU_] |1037| 
        ANDB      AL,#0x01              ; [CPU_] |1037| 
        CMPB      AL,#1                 ; [CPU_] |1037| 
        BF        $C$L49,NEQ            ; [CPU_] |1037| 
        ; branchcc occurs ; [] |1037| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1049,column 9,is_stmt
        MOVL      XAR4,#381464          ; [CPU_U] |1049| 
        OR        *+XAR4[0],#0x0002     ; [CPU_] |1049| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1051,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |1051| 
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_name("_SysCtl_delay")
	.dwattr $C$DW$162, DW_AT_TI_call
        LCR       #_SysCtl_delay        ; [CPU_] |1051| 
        ; call occurs [#_SysCtl_delay] ; [] |1051| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1053,column 9,is_stmt
        INC       *-SP[6]               ; [CPU_] |1053| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1055,column 9,is_stmt
$C$DW$163	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$163, DW_AT_low_pc(0x00)
	.dwattr $C$DW$163, DW_AT_name("___edis")
	.dwattr $C$DW$163, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1055| 
        ; call occurs [#___edis] ; [] |1055| 
$C$L50:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1000,column 11,is_stmt
        MOVL      XAR4,#381472          ; [CPU_U] |1000| 
        TBIT      *+XAR4[0],#1          ; [CPU_] |1000| 
        BF        $C$L51,NTC            ; [CPU_] |1000| 
        ; branchcc occurs ; [] |1000| 
        MOV       AL,*-SP[6]            ; [CPU_] |1000| 
        CMPB      AL,#10                ; [CPU_] |1000| 
        B         $C$L51,HIS            ; [CPU_] |1000| 
        ; branchcc occurs ; [] |1000| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1000| 
        MOV       PL,#0                 ; [CPU_] |1000| 
        MOV       PH,#32768             ; [CPU_] |1000| 
        AND       ACC,#32768 << 16      ; [CPU_] |1000| 
        CMPL      ACC,P                 ; [CPU_] |1000| 
        BF        $C$L48,EQ             ; [CPU_] |1000| 
        ; branchcc occurs ; [] |1000| 
$C$L51:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1061,column 5,is_stmt
$C$DW$164	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$164, DW_AT_low_pc(0x00)
	.dwattr $C$DW$164, DW_AT_name("___eallow")
	.dwattr $C$DW$164, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1061| 
        ; call occurs [#___eallow] ; [] |1061| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1062,column 5,is_stmt
        AND       AL,*-SP[2],#0x1f80    ; [CPU_] |1062| 
        MOVL      XAR4,#381476          ; [CPU_U] |1062| 
        LSR       AL,7                  ; [CPU_] |1062| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1062| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1064,column 5,is_stmt
$C$DW$165	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$165, DW_AT_low_pc(0x00)
	.dwattr $C$DW$165, DW_AT_name("___edis")
	.dwattr $C$DW$165, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1064| 
        ; call occurs [#___edis] ; [] |1064| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1066,column 1,is_stmt
        SUBB      SP,#14                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$166	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$166, DW_AT_low_pc(0x00)
	.dwattr $C$DW$166, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$126, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$126, DW_AT_TI_end_line(0x42a)
	.dwattr $C$DW$126, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$126

	.sect	".text"
	.clink
	.global	_SysCtl_selectXTAL

$C$DW$167	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_selectXTAL")
	.dwattr $C$DW$167, DW_AT_low_pc(_SysCtl_selectXTAL)
	.dwattr $C$DW$167, DW_AT_high_pc(0x00)
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_SysCtl_selectXTAL")
	.dwattr $C$DW$167, DW_AT_external
	.dwattr $C$DW$167, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$167, DW_AT_TI_begin_line(0x433)
	.dwattr $C$DW$167, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$167, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1076,column 1,is_stmt,address _SysCtl_selectXTAL

	.dwfde $C$DW$CIE, _SysCtl_selectXTAL

;***************************************************************
;* FNAME: _SysCtl_selectXTAL            FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SysCtl_selectXTAL:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("t2TCR")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_t2TCR")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$168, DW_AT_location[DW_OP_breg20 -3]
$C$DW$169	.dwtag  DW_TAG_variable, DW_AT_name("t2TPR")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_t2TPR")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_breg20 -4]
$C$DW$170	.dwtag  DW_TAG_variable, DW_AT_name("t2TPRH")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_t2TPRH")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_breg20 -5]
$C$DW$171	.dwtag  DW_TAG_variable, DW_AT_name("t2CLKCTL")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_t2CLKCTL")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_breg20 -6]
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("t2PRD")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_t2PRD")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_breg20 -8]
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1083,column 5,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |1083| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1083| 
        MOV       *-SP[6],AL            ; [CPU_] |1083| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1084,column 5,is_stmt
        MOV       *-SP[3],*(0:0x0c14)   ; [CPU_] |1084| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1085,column 5,is_stmt
        MOVL      XAR4,#3090            ; [CPU_] |1085| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1085| 
        MOVL      *-SP[8],ACC           ; [CPU_] |1085| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1086,column 5,is_stmt
        MOV       *-SP[4],*(0:0x0c16)   ; [CPU_] |1086| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1087,column 5,is_stmt
        MOV       *-SP[5],*(0:0x0c17)   ; [CPU_] |1087| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1093,column 2,is_stmt
        MOV       ACC,#3088             ; [CPU_] |1093| 
$C$DW$173	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$173, DW_AT_low_pc(0x00)
	.dwattr $C$DW$173, DW_AT_name("_CPUTimer_disableInterrupt")
	.dwattr $C$DW$173, DW_AT_TI_call
        LCR       #_CPUTimer_disableInterrupt ; [CPU_] |1093| 
        ; call occurs [#_CPUTimer_disableInterrupt] ; [] |1093| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1098,column 5,is_stmt
        MOV       ACC,#3088             ; [CPU_] |1098| 
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_name("_CPUTimer_stopTimer")
	.dwattr $C$DW$174, DW_AT_TI_call
        LCR       #_CPUTimer_stopTimer  ; [CPU_] |1098| 
        ; call occurs [#_CPUTimer_stopTimer] ; [] |1098| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1103,column 5,is_stmt
        MOVL      XAR4,#1023            ; [CPU_U] |1103| 
        MOV       ACC,#3088             ; [CPU_] |1103| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1103| 
$C$DW$175	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$175, DW_AT_low_pc(0x00)
	.dwattr $C$DW$175, DW_AT_name("_CPUTimer_setPeriod")
	.dwattr $C$DW$175, DW_AT_TI_call
        LCR       #_CPUTimer_setPeriod  ; [CPU_] |1103| 
        ; call occurs [#_CPUTimer_setPeriod] ; [] |1103| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1108,column 5,is_stmt
        MOVB      XAR5,#0               ; [CPU_] |1108| 
        MOVB      XAR4,#3               ; [CPU_] |1108| 
        MOV       ACC,#3088             ; [CPU_] |1108| 
$C$DW$176	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$176, DW_AT_low_pc(0x00)
	.dwattr $C$DW$176, DW_AT_name("_CPUTimer_selectClockSource")
	.dwattr $C$DW$176, DW_AT_TI_call
        LCR       #_CPUTimer_selectClockSource ; [CPU_] |1108| 
        ; call occurs [#_CPUTimer_selectClockSource] ; [] |1108| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1114,column 5,is_stmt
        MOV       ACC,#3088             ; [CPU_] |1114| 
$C$DW$177	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$177, DW_AT_low_pc(0x00)
	.dwattr $C$DW$177, DW_AT_name("_CPUTimer_clearOverflowFlag")
	.dwattr $C$DW$177, DW_AT_TI_call
        LCR       #_CPUTimer_clearOverflowFlag ; [CPU_] |1114| 
        ; call occurs [#_CPUTimer_clearOverflowFlag] ; [] |1114| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1119,column 5,is_stmt
        MOV       ACC,#3088             ; [CPU_] |1119| 
$C$DW$178	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$178, DW_AT_low_pc(0x00)
	.dwattr $C$DW$178, DW_AT_name("_CPUTimer_startTimer")
	.dwattr $C$DW$178, DW_AT_TI_call
        LCR       #_CPUTimer_startTimer ; [CPU_] |1119| 
        ; call occurs [#_CPUTimer_startTimer] ; [] |1119| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1121,column 5,is_stmt
$C$DW$179	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$179, DW_AT_low_pc(0x00)
	.dwattr $C$DW$179, DW_AT_name("___eallow")
	.dwattr $C$DW$179, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1121| 
        ; call occurs [#___eallow] ; [] |1121| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1125,column 5,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |1125| 
        AND       *+XAR4[0],#0xffef     ; [CPU_] |1125| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1126,column 5,is_stmt
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_name("___edis")
	.dwattr $C$DW$180, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1126| 
        ; call occurs [#___edis] ; [] |1126| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1131,column 5,is_stmt
$C$DW$181	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$181, DW_AT_low_pc(0x00)
	.dwattr $C$DW$181, DW_AT_name("_SysCtl_pollCpuTimer")
	.dwattr $C$DW$181, DW_AT_TI_call
        LCR       #_SysCtl_pollCpuTimer ; [CPU_] |1131| 
        ; call occurs [#_SysCtl_pollCpuTimer] ; [] |1131| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1136,column 5,is_stmt
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_name("___eallow")
	.dwattr $C$DW$182, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1136| 
        ; call occurs [#___eallow] ; [] |1136| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1137,column 5,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |1137| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1137| 
        ORB       AL,#0x01              ; [CPU_] |1137| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1137| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1141,column 5,is_stmt
$C$DW$183	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$183, DW_AT_low_pc(0x00)
	.dwattr $C$DW$183, DW_AT_name("___edis")
	.dwattr $C$DW$183, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1141| 
        ; call occurs [#___edis] ; [] |1141| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1147,column 5,is_stmt
        B         $C$L53,UNC            ; [CPU_] |1147| 
        ; branch occurs ; [] |1147| 
$C$L52:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1152,column 9,is_stmt
$C$DW$184	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$184, DW_AT_low_pc(0x00)
	.dwattr $C$DW$184, DW_AT_name("_SysCtl_resetMCD")
	.dwattr $C$DW$184, DW_AT_TI_call
        LCR       #_SysCtl_resetMCD     ; [CPU_] |1152| 
        ; call occurs [#_SysCtl_resetMCD] ; [] |1152| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1157,column 9,is_stmt
$C$DW$185	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$185, DW_AT_low_pc(0x00)
	.dwattr $C$DW$185, DW_AT_name("_SysCtl_pollCpuTimer")
	.dwattr $C$DW$185, DW_AT_TI_call
        LCR       #_SysCtl_pollCpuTimer ; [CPU_] |1157| 
        ; call occurs [#_SysCtl_pollCpuTimer] ; [] |1157| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1162,column 9,is_stmt
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_name("___eallow")
	.dwattr $C$DW$186, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1162| 
        ; call occurs [#___eallow] ; [] |1162| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1163,column 9,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |1163| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1163| 
        ORB       AL,#0x01              ; [CPU_] |1163| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1163| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1167,column 9,is_stmt
$C$DW$187	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$187, DW_AT_low_pc(0x00)
	.dwattr $C$DW$187, DW_AT_name("___edis")
	.dwattr $C$DW$187, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1167| 
        ; call occurs [#___edis] ; [] |1167| 
$C$L53:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1168,column 5,is_stmt
$C$DW$188	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$188, DW_AT_low_pc(0x00)
	.dwattr $C$DW$188, DW_AT_name("_SysCtl_isMCDClockFailureDetected")
	.dwattr $C$DW$188, DW_AT_TI_call
        LCR       #_SysCtl_isMCDClockFailureDetected ; [CPU_] |1168| 
        ; call occurs [#_SysCtl_isMCDClockFailureDetected] ; [] |1168| 
        CMPB      AL,#0                 ; [CPU_] |1168| 
        BF        $C$L52,NEQ            ; [CPU_] |1168| 
        ; branchcc occurs ; [] |1168| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1173,column 5,is_stmt
        MOV       ACC,#3088             ; [CPU_] |1173| 
$C$DW$189	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$189, DW_AT_low_pc(0x00)
	.dwattr $C$DW$189, DW_AT_name("_CPUTimer_stopTimer")
	.dwattr $C$DW$189, DW_AT_TI_call
        LCR       #_CPUTimer_stopTimer  ; [CPU_] |1173| 
        ; call occurs [#_CPUTimer_stopTimer] ; [] |1173| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1178,column 5,is_stmt
$C$DW$190	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$190, DW_AT_low_pc(0x00)
	.dwattr $C$DW$190, DW_AT_name("___eallow")
	.dwattr $C$DW$190, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1178| 
        ; call occurs [#___eallow] ; [] |1178| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1179,column 5,is_stmt
        MOVL      XAR4,#381820          ; [CPU_U] |1179| 
        MOV       AL,*-SP[6]            ; [CPU_] |1179| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1179| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1180,column 5,is_stmt
        MOV       *(0:0x0c14),*-SP[3]   ; [CPU_] |1180| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1181,column 5,is_stmt
        MOVL      XAR4,#3090            ; [CPU_] |1181| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |1181| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |1181| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1182,column 5,is_stmt
        MOV       *(0:0x0c16),*-SP[4]   ; [CPU_] |1182| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1183,column 5,is_stmt
        MOV       *(0:0x0c17),*-SP[5]   ; [CPU_] |1183| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1184,column 5,is_stmt
        MOV       AL,*(0:0x0c14)        ; [CPU_] |1184| 
        ORB       AL,#0x20              ; [CPU_] |1184| 
        MOV       *(0:0x0c14),AL        ; [CPU_] |1184| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1185,column 5,is_stmt
$C$DW$191	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$191, DW_AT_low_pc(0x00)
	.dwattr $C$DW$191, DW_AT_name("___edis")
	.dwattr $C$DW$191, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1185| 
        ; call occurs [#___edis] ; [] |1185| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1186,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$167, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$167, DW_AT_TI_end_line(0x4a2)
	.dwattr $C$DW$167, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$167

	.sect	".text"
	.clink
	.global	_SysCtl_selectOscSource

$C$DW$193	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_selectOscSource")
	.dwattr $C$DW$193, DW_AT_low_pc(_SysCtl_selectOscSource)
	.dwattr $C$DW$193, DW_AT_high_pc(0x00)
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_SysCtl_selectOscSource")
	.dwattr $C$DW$193, DW_AT_external
	.dwattr $C$DW$193, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$193, DW_AT_TI_begin_line(0x4aa)
	.dwattr $C$DW$193, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$193, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1195,column 1,is_stmt,address _SysCtl_selectOscSource

	.dwfde $C$DW$CIE, _SysCtl_selectOscSource
$C$DW$194	.dwtag  DW_TAG_formal_parameter, DW_AT_name("oscSource")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_oscSource")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_selectOscSource       FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SysCtl_selectOscSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$195	.dwtag  DW_TAG_variable, DW_AT_name("oscSource")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_oscSource")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1195| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1203,column 5,is_stmt
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_name("___eallow")
	.dwattr $C$DW$196, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1203| 
        ; call occurs [#___eallow] ; [] |1203| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1204,column 5,is_stmt
        B         $C$L57,UNC            ; [CPU_] |1204| 
        ; branch occurs ; [] |1204| 
$C$L54:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1210,column 13,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |1210| 
        AND       *+XAR4[0],#0xfff7     ; [CPU_] |1210| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1213,column 13,is_stmt
 RPT #250 || NOP 
 RPT #50 || NOP
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1218,column 13,is_stmt
        AND       *+XAR4[0],#0xfffc     ; [CPU_] |1218| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1221,column 13,is_stmt
 RPT #250 || NOP 
 RPT #50 || NOP
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1226,column 13,is_stmt
        OR        *+XAR4[0],#0x0010     ; [CPU_] |1226| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1229,column 13,is_stmt
        B         $C$L58,UNC            ; [CPU_] |1229| 
        ; branch occurs ; [] |1229| 
$C$L55:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1235,column 13,is_stmt
$C$DW$197	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$197, DW_AT_low_pc(0x00)
	.dwattr $C$DW$197, DW_AT_name("_SysCtl_selectXTAL")
	.dwattr $C$DW$197, DW_AT_TI_call
        LCR       #_SysCtl_selectXTAL   ; [CPU_] |1235| 
        ; call occurs [#_SysCtl_selectXTAL] ; [] |1235| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1236,column 13,is_stmt
        B         $C$L58,UNC            ; [CPU_] |1236| 
        ; branch occurs ; [] |1236| 
$C$L56:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1242,column 13,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |1242| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1242| 
        ORB       AL,#0x02              ; [CPU_] |1242| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1242| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1247,column 13,is_stmt
 RPT #250 || NOP 
 RPT #50 || NOP
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1252,column 13,is_stmt
        OR        *+XAR4[0],#0x0010     ; [CPU_] |1252| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1255,column 13,is_stmt
        B         $C$L58,UNC            ; [CPU_] |1255| 
        ; branch occurs ; [] |1255| 
$C$L57:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1204,column 5,is_stmt
        MOVL      XAR6,*-SP[2]          ; [CPU_] |1204| 
        MOVB      ACC,#0                ; [CPU_] |1204| 
        CMPL      ACC,XAR6              ; [CPU_] |1204| 
        BF        $C$L54,EQ             ; [CPU_] |1204| 
        ; branchcc occurs ; [] |1204| 
        MOVL      XAR4,#65536           ; [CPU_U] |1204| 
        MOVL      ACC,XAR4              ; [CPU_] |1204| 
        CMPL      ACC,XAR6              ; [CPU_] |1204| 
        BF        $C$L55,EQ             ; [CPU_] |1204| 
        ; branchcc occurs ; [] |1204| 
        MOVL      XAR4,#131072          ; [CPU_U] |1204| 
        MOVL      ACC,XAR4              ; [CPU_] |1204| 
        CMPL      ACC,XAR6              ; [CPU_] |1204| 
        BF        $C$L56,EQ             ; [CPU_] |1204| 
        ; branchcc occurs ; [] |1204| 
$C$L58:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1263,column 5,is_stmt
        SPM       #0                    ; [CPU_] 
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_name("___edis")
	.dwattr $C$DW$198, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1263| 
        ; call occurs [#___edis] ; [] |1263| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1264,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$199	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$199, DW_AT_low_pc(0x00)
	.dwattr $C$DW$199, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$193, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$193, DW_AT_TI_end_line(0x4f0)
	.dwattr $C$DW$193, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$193

	.sect	".text"
	.clink
	.global	_SysCtl_selectOscSourceAuxPLL

$C$DW$200	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_selectOscSourceAuxPLL")
	.dwattr $C$DW$200, DW_AT_low_pc(_SysCtl_selectOscSourceAuxPLL)
	.dwattr $C$DW$200, DW_AT_high_pc(0x00)
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_SysCtl_selectOscSourceAuxPLL")
	.dwattr $C$DW$200, DW_AT_external
	.dwattr $C$DW$200, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$200, DW_AT_TI_begin_line(0x4f8)
	.dwattr $C$DW$200, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$200, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1273,column 1,is_stmt,address _SysCtl_selectOscSourceAuxPLL

	.dwfde $C$DW$CIE, _SysCtl_selectOscSourceAuxPLL
$C$DW$201	.dwtag  DW_TAG_formal_parameter, DW_AT_name("oscSource")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_oscSource")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_selectOscSourceAuxPLL FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_SysCtl_selectOscSourceAuxPLL:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$202	.dwtag  DW_TAG_variable, DW_AT_name("oscSource")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_oscSource")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1273| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1274,column 5,is_stmt
$C$DW$203	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$203, DW_AT_low_pc(0x00)
	.dwattr $C$DW$203, DW_AT_name("___eallow")
	.dwattr $C$DW$203, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |1274| 
        ; call occurs [#___eallow] ; [] |1274| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1276,column 5,is_stmt
        B         $C$L62,UNC            ; [CPU_] |1276| 
        ; branch occurs ; [] |1276| 
$C$L59:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1282,column 13,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |1282| 
        AND       *+XAR4[0],#0xfff7     ; [CPU_] |1282| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1288,column 13,is_stmt
        MOVL      XAR4,#381450          ; [CPU_U] |1288| 
        AND       *+XAR4[0],#0xfffc     ; [CPU_] |1288| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1290,column 13,is_stmt
        B         $C$L63,UNC            ; [CPU_] |1290| 
        ; branch occurs ; [] |1290| 
$C$L60:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1296,column 13,is_stmt
        MOVL      XAR4,#381448          ; [CPU_U] |1296| 
        AND       *+XAR4[0],#0xffef     ; [CPU_] |1296| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1302,column 13,is_stmt
        MOVL      XAR4,#381450          ; [CPU_U] |1302| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1302| 
        ORB       AL,#0x01              ; [CPU_] |1302| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1302| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1306,column 13,is_stmt
        B         $C$L63,UNC            ; [CPU_] |1306| 
        ; branch occurs ; [] |1306| 
$C$L61:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1312,column 13,is_stmt
        MOVL      XAR4,#381450          ; [CPU_U] |1312| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1312| 
        ORB       AL,#0x02              ; [CPU_] |1312| 
        MOV       *+XAR4[0],AL          ; [CPU_] |1312| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1316,column 13,is_stmt
        B         $C$L63,UNC            ; [CPU_] |1316| 
        ; branch occurs ; [] |1316| 
$C$L62:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1276,column 5,is_stmt
        MOVL      XAR6,*-SP[2]          ; [CPU_] |1276| 
        MOVB      ACC,#0                ; [CPU_] |1276| 
        CMPL      ACC,XAR6              ; [CPU_] |1276| 
        BF        $C$L59,EQ             ; [CPU_] |1276| 
        ; branchcc occurs ; [] |1276| 
        MOVL      XAR4,#65536           ; [CPU_U] |1276| 
        MOVL      ACC,XAR4              ; [CPU_] |1276| 
        CMPL      ACC,XAR6              ; [CPU_] |1276| 
        BF        $C$L60,EQ             ; [CPU_] |1276| 
        ; branchcc occurs ; [] |1276| 
        MOVL      XAR4,#131072          ; [CPU_U] |1276| 
        MOVL      ACC,XAR4              ; [CPU_] |1276| 
        CMPL      ACC,XAR6              ; [CPU_] |1276| 
        BF        $C$L61,EQ             ; [CPU_] |1276| 
        ; branchcc occurs ; [] |1276| 
$C$L63:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1324,column 5,is_stmt
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_name("___edis")
	.dwattr $C$DW$204, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |1324| 
        ; call occurs [#___edis] ; [] |1324| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1325,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$205	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$205, DW_AT_low_pc(0x00)
	.dwattr $C$DW$205, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$200, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$200, DW_AT_TI_end_line(0x52d)
	.dwattr $C$DW$200, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$200

	.sect	".text"
	.clink
	.global	_SysCtl_getLowSpeedClock

$C$DW$206	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_getLowSpeedClock")
	.dwattr $C$DW$206, DW_AT_low_pc(_SysCtl_getLowSpeedClock)
	.dwattr $C$DW$206, DW_AT_high_pc(0x00)
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_SysCtl_getLowSpeedClock")
	.dwattr $C$DW$206, DW_AT_external
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$206, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$206, DW_AT_TI_begin_line(0x535)
	.dwattr $C$DW$206, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$206, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1334,column 1,is_stmt,address _SysCtl_getLowSpeedClock

	.dwfde $C$DW$CIE, _SysCtl_getLowSpeedClock
$C$DW$207	.dwtag  DW_TAG_formal_parameter, DW_AT_name("clockInHz")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_clockInHz")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_getLowSpeedClock      FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_SysCtl_getLowSpeedClock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("clockInHz")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_clockInHz")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_breg20 -2]
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("clockOut")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_clockOut")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |1334| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1340,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |1340| 
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_name("_SysCtl_getClock")
	.dwattr $C$DW$210, DW_AT_TI_call
        LCR       #_SysCtl_getClock     ; [CPU_] |1340| 
        ; call occurs [#_SysCtl_getClock] ; [] |1340| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1340| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1345,column 5,is_stmt
        MOVL      XAR4,#381484          ; [CPU_U] |1345| 
        MOVB      XAR6,#0               ; [CPU_] |1345| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1345| 
        MOVB      AH,#0                 ; [CPU_] |1345| 
        ANDB      AL,#0x07              ; [CPU_] |1345| 
        CMPL      ACC,XAR6              ; [CPU_] |1345| 
        BF        $C$L64,EQ             ; [CPU_] |1345| 
        ; branchcc occurs ; [] |1345| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1348,column 9,is_stmt
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1348| 
        MOVB      AH,#0                 ; [CPU_] |1348| 
        MOVL      P,*-SP[4]             ; [CPU_] |1348| 
        ANDB      AL,#0x07              ; [CPU_] |1348| 
        LSL       ACC,1                 ; [CPU_] |1348| 
        MOVL      XAR6,ACC              ; [CPU_] |1348| 
        MOVB      ACC,#0                ; [CPU_] |1348| 
        RPT       #31
||     SUBCUL    ACC,XAR6              ; [CPU_] |1348| 
        MOVL      *-SP[4],P             ; [CPU_] |1348| 
$C$L64:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1352,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1352| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1353,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$211	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$211, DW_AT_low_pc(0x00)
	.dwattr $C$DW$211, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$206, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$206, DW_AT_TI_end_line(0x549)
	.dwattr $C$DW$206, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$206

	.sect	".text"
	.clink
	.global	_SysCtl_getDeviceParametric

$C$DW$212	.dwtag  DW_TAG_subprogram, DW_AT_name("SysCtl_getDeviceParametric")
	.dwattr $C$DW$212, DW_AT_low_pc(_SysCtl_getDeviceParametric)
	.dwattr $C$DW$212, DW_AT_high_pc(0x00)
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_SysCtl_getDeviceParametric")
	.dwattr $C$DW$212, DW_AT_external
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$212, DW_AT_TI_begin_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$212, DW_AT_TI_begin_line(0x551)
	.dwattr $C$DW$212, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$212, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1362,column 1,is_stmt,address _SysCtl_getDeviceParametric

	.dwfde $C$DW$CIE, _SysCtl_getDeviceParametric
$C$DW$213	.dwtag  DW_TAG_formal_parameter, DW_AT_name("parametric")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_parametric")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _SysCtl_getDeviceParametric   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_SysCtl_getDeviceParametric:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$214	.dwtag  DW_TAG_variable, DW_AT_name("parametric")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_parametric")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_breg20 -1]
$C$DW$215	.dwtag  DW_TAG_variable, DW_AT_name("value")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[1],AL            ; [CPU_] |1362| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1368,column 5,is_stmt
        B         $C$L74,UNC            ; [CPU_] |1368| 
        ; branch occurs ; [] |1368| 
$C$L65:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1374,column 13,is_stmt
        MOVL      XAR4,#380936          ; [CPU_U] |1374| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1374| 
        MOVB      AH,#0                 ; [CPU_] |1374| 
        ANDB      AL,#0xc0              ; [CPU_] |1374| 
        SFR       ACC,6                 ; [CPU_] |1374| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1374| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1376,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1376| 
        ; branch occurs ; [] |1376| 
$C$L66:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1382,column 13,is_stmt
        MOVL      XAR4,#380936          ; [CPU_U] |1382| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1382| 
        MOVB      AH,#0                 ; [CPU_] |1382| 
        AND       AL,#0x0700            ; [CPU_] |1382| 
        SFR       ACC,8                 ; [CPU_] |1382| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1382| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1385,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1385| 
        ; branch occurs ; [] |1385| 
$C$L67:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1391,column 13,is_stmt
        MOVL      XAR4,#380936          ; [CPU_U] |1391| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1391| 
        MOVB      AH,#0                 ; [CPU_] |1391| 
        AND       AL,#0x6000            ; [CPU_] |1391| 
        SFR       ACC,13                ; [CPU_] |1391| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1391| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1394,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1394| 
        ; branch occurs ; [] |1394| 
$C$L68:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1400,column 13,is_stmt
        MOVL      XAR4,#380936          ; [CPU_U] |1400| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1400| 
        ANDB      AH,#255               ; [CPU_] |1400| 
        MOVU      ACC,AH                ; [CPU_] |1400| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1400| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1403,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1403| 
        ; branch occurs ; [] |1403| 
$C$L69:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1409,column 13,is_stmt
        MOVL      XAR4,#380936          ; [CPU_U] |1409| 
        MOV       T,#28                 ; [CPU_] |1409| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1409| 
        AND       ACC,#61440 << 16      ; [CPU_] |1409| 
        LSRL      ACC,T                 ; [CPU_] |1409| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1409| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1412,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1412| 
        ; branch occurs ; [] |1412| 
$C$L70:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1418,column 13,is_stmt
        MOVL      XAR4,#380938          ; [CPU_U] |1418| 
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1418| 
        MOVB      AH,#0                 ; [CPU_] |1418| 
        AND       AL,#0xff00            ; [CPU_] |1418| 
        SFR       ACC,8                 ; [CPU_] |1418| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1418| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1420,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1420| 
        ; branch occurs ; [] |1420| 
$C$L71:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1426,column 13,is_stmt
        MOVL      XAR4,#380938          ; [CPU_U] |1426| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1426| 
        ANDB      AH,#255               ; [CPU_] |1426| 
        MOVU      ACC,AH                ; [CPU_] |1426| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1426| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1428,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1428| 
        ; branch occurs ; [] |1428| 
$C$L72:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1434,column 13,is_stmt
        MOVL      XAR4,#380938          ; [CPU_U] |1434| 
        MOV       T,#24                 ; [CPU_] |1434| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |1434| 
        AND       ACC,#65280 << 16      ; [CPU_] |1434| 
        LSRL      ACC,T                 ; [CPU_] |1434| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1434| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1437,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1437| 
        ; branch occurs ; [] |1437| 
$C$L73:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1443,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1443| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1443| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1444,column 13,is_stmt
        B         $C$L76,UNC            ; [CPU_] |1444| 
        ; branch occurs ; [] |1444| 
$C$L74:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1368,column 5,is_stmt
        CMPB      AL,#4                 ; [CPU_] |1368| 
        B         $C$L75,GT             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        CMPB      AL,#4                 ; [CPU_] |1368| 
        BF        $C$L69,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        CMPB      AL,#0                 ; [CPU_] |1368| 
        BF        $C$L65,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        CMPB      AL,#1                 ; [CPU_] |1368| 
        BF        $C$L66,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        CMPB      AL,#2                 ; [CPU_] |1368| 
        BF        $C$L67,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        CMPB      AL,#3                 ; [CPU_] |1368| 
        BF        $C$L68,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        B         $C$L73,UNC            ; [CPU_] |1368| 
        ; branch occurs ; [] |1368| 
$C$L75:    
        CMPB      AL,#5                 ; [CPU_] |1368| 
        BF        $C$L70,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        CMPB      AL,#6                 ; [CPU_] |1368| 
        BF        $C$L71,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        CMPB      AL,#7                 ; [CPU_] |1368| 
        BF        $C$L72,EQ             ; [CPU_] |1368| 
        ; branchcc occurs ; [] |1368| 
        B         $C$L73,UNC            ; [CPU_] |1368| 
        ; branch occurs ; [] |1368| 
$C$L76:    
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1447,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |1447| 
	.dwpsn	file "../ExtraData/driverlib2/sysctl.c",line 1448,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$212, DW_AT_TI_end_file("../ExtraData/driverlib2/sysctl.c")
	.dwattr $C$DW$212, DW_AT_TI_end_line(0x5a8)
	.dwattr $C$DW$212, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$212

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___eallow
	.global	___edis
	.global	_SysCtl_delay
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$217	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_SOURCE_SYS"), DW_AT_const_value(0x00)
$C$DW$218	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_SOURCE_INTOSC1"), DW_AT_const_value(0x01)
$C$DW$219	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_SOURCE_INTOSC2"), DW_AT_const_value(0x02)
$C$DW$220	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_SOURCE_XTAL"), DW_AT_const_value(0x03)
$C$DW$221	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_SOURCE_AUX"), DW_AT_const_value(0x06)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("CPUTimer_ClockSource")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$222	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_PRESCALER_1"), DW_AT_const_value(0x00)
$C$DW$223	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_PRESCALER_2"), DW_AT_const_value(0x01)
$C$DW$224	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_PRESCALER_4"), DW_AT_const_value(0x02)
$C$DW$225	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_PRESCALER_8"), DW_AT_const_value(0x03)
$C$DW$226	.dwtag  DW_TAG_enumerator, DW_AT_name("CPUTIMER_CLOCK_PRESCALER_16"), DW_AT_const_value(0x04)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("CPUTimer_Prescaler")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$227	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_QUAL"), DW_AT_const_value(0x00)
$C$DW$228	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_PINCOUNT"), DW_AT_const_value(0x01)
$C$DW$229	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_INSTASPIN"), DW_AT_const_value(0x02)
$C$DW$230	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_FLASH"), DW_AT_const_value(0x03)
$C$DW$231	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_PARTID"), DW_AT_const_value(0x04)
$C$DW$232	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_FAMILY"), DW_AT_const_value(0x05)
$C$DW$233	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_PARTNO"), DW_AT_const_value(0x06)
$C$DW$234	.dwtag  DW_TAG_enumerator, DW_AT_name("SYSCTL_DEVICE_CLASSID"), DW_AT_const_value(0x07)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("SysCtl_DeviceParametric")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("float32_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$235	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_reg0]
$C$DW$236	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_reg1]
$C$DW$237	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_reg2]
$C$DW$238	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_reg3]
$C$DW$239	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_reg20]
$C$DW$240	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_reg21]
$C$DW$241	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_reg22]
$C$DW$242	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$242, DW_AT_location[DW_OP_reg23]
$C$DW$243	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$243, DW_AT_location[DW_OP_reg24]
$C$DW$244	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$244, DW_AT_location[DW_OP_reg25]
$C$DW$245	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$245, DW_AT_location[DW_OP_reg26]
$C$DW$246	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$246, DW_AT_location[DW_OP_reg28]
$C$DW$247	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$247, DW_AT_location[DW_OP_reg29]
$C$DW$248	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$248, DW_AT_location[DW_OP_reg30]
$C$DW$249	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$249, DW_AT_location[DW_OP_reg31]
$C$DW$250	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$250, DW_AT_location[DW_OP_regx 0x20]
$C$DW$251	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_regx 0x21]
$C$DW$252	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$252, DW_AT_location[DW_OP_regx 0x22]
$C$DW$253	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$253, DW_AT_location[DW_OP_regx 0x23]
$C$DW$254	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$254, DW_AT_location[DW_OP_regx 0x24]
$C$DW$255	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$255, DW_AT_location[DW_OP_regx 0x25]
$C$DW$256	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$256, DW_AT_location[DW_OP_regx 0x26]
$C$DW$257	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$257, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$258	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$258, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$259	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$259, DW_AT_location[DW_OP_reg4]
$C$DW$260	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$260, DW_AT_location[DW_OP_reg6]
$C$DW$261	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$261, DW_AT_location[DW_OP_reg8]
$C$DW$262	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$262, DW_AT_location[DW_OP_reg10]
$C$DW$263	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$263, DW_AT_location[DW_OP_reg12]
$C$DW$264	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$264, DW_AT_location[DW_OP_reg14]
$C$DW$265	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$265, DW_AT_location[DW_OP_reg16]
$C$DW$266	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$266, DW_AT_location[DW_OP_reg17]
$C$DW$267	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$267, DW_AT_location[DW_OP_reg18]
$C$DW$268	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$268, DW_AT_location[DW_OP_reg19]
$C$DW$269	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_reg5]
$C$DW$270	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_reg7]
$C$DW$271	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_reg9]
$C$DW$272	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_reg11]
$C$DW$273	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$273, DW_AT_location[DW_OP_reg13]
$C$DW$274	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$274, DW_AT_location[DW_OP_reg15]
$C$DW$275	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$275, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$276	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$276, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$277	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$277, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$278	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$278, DW_AT_location[DW_OP_regx 0x30]
$C$DW$279	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$279, DW_AT_location[DW_OP_regx 0x33]
$C$DW$280	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$280, DW_AT_location[DW_OP_regx 0x34]
$C$DW$281	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$281, DW_AT_location[DW_OP_regx 0x37]
$C$DW$282	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$282, DW_AT_location[DW_OP_regx 0x38]
$C$DW$283	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$283, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$284	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$284, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$285	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$285, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$286	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_regx 0x40]
$C$DW$287	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_regx 0x43]
$C$DW$288	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_regx 0x44]
$C$DW$289	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$289, DW_AT_location[DW_OP_regx 0x47]
$C$DW$290	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$290, DW_AT_location[DW_OP_regx 0x48]
$C$DW$291	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_regx 0x49]
$C$DW$292	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$292, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$293	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$293, DW_AT_location[DW_OP_regx 0x27]
$C$DW$294	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$294, DW_AT_location[DW_OP_regx 0x28]
$C$DW$295	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$295, DW_AT_location[DW_OP_reg27]
$C$DW$296	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$296, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

