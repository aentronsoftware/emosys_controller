;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0611612 
	.sect	".text"
	.clink
	.global	_CMPSS_configFilterHigh

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("CMPSS_configFilterHigh")
	.dwattr $C$DW$3, DW_AT_low_pc(_CMPSS_configFilterHigh)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_CMPSS_configFilterHigh")
	.dwattr $C$DW$3, DW_AT_external
	.dwattr $C$DW$3, DW_AT_TI_begin_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 53,column 1,is_stmt,address _CMPSS_configFilterHigh

	.dwfde $C$DW$CIE, _CMPSS_configFilterHigh
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("samplePrescale")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_samplePrescale")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg12]
$C$DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("sampleWindow")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_sampleWindow")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg14]
$C$DW$7	.dwtag  DW_TAG_formal_parameter, DW_AT_name("threshold")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_threshold")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -9]

;***************************************************************
;* FNAME: _CMPSS_configFilterHigh       FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CMPSS_configFilterHigh:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_breg20 -2]
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("samplePrescale")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_samplePrescale")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_breg20 -3]
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("sampleWindow")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_sampleWindow")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_breg20 -4]
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("regValue")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_regValue")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[4],AR5           ; [CPU_] |53| 
        MOV       *-SP[3],AR4           ; [CPU_] |53| 
        MOVL      *-SP[2],ACC           ; [CPU_] |53| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 68,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |68| 
        MOV       AH,*-SP[4]            ; [CPU_] |68| 
        ADDB      AL,#-1                ; [CPU_] |68| 
        ADDB      AH,#-1                ; [CPU_] |68| 
        LSL       AL,9                  ; [CPU_] |68| 
        LSL       AH,4                  ; [CPU_] |68| 
        OR        AL,AH                 ; [CPU_] |68| 
        MOV       *-SP[5],AL            ; [CPU_] |68| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 71,column 5,is_stmt
$C$DW$12	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$12, DW_AT_low_pc(0x00)
	.dwattr $C$DW$12, DW_AT_name("___eallow")
	.dwattr $C$DW$12, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |71| 
        ; call occurs [#___eallow] ; [] |71| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 73,column 5,is_stmt
        MOVB      ACC,#24               ; [CPU_] |73| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |73| 
        MOVL      XAR4,ACC              ; [CPU_] |73| 
        AND       AL,*+XAR4[0],#0xc00f  ; [CPU_] |73| 
        OR        AL,*-SP[5]            ; [CPU_] |73| 
        MOVZ      AR6,AL                ; [CPU_] |73| 
        MOVB      ACC,#24               ; [CPU_] |73| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |73| 
        MOVL      XAR4,ACC              ; [CPU_] |73| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |73| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 81,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |81| 
        MOVB      ACC,#25               ; [CPU_] |81| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |81| 
        MOVL      XAR4,ACC              ; [CPU_] |81| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |81| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 83,column 5,is_stmt
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_name("___edis")
	.dwattr $C$DW$13, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |83| 
        ; call occurs [#___edis] ; [] |83| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 84,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x54)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink
	.global	_CMPSS_configFilterLow

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("CMPSS_configFilterLow")
	.dwattr $C$DW$15, DW_AT_low_pc(_CMPSS_configFilterLow)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_CMPSS_configFilterLow")
	.dwattr $C$DW$15, DW_AT_external
	.dwattr $C$DW$15, DW_AT_TI_begin_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x5c)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 94,column 1,is_stmt,address _CMPSS_configFilterLow

	.dwfde $C$DW$CIE, _CMPSS_configFilterLow
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg0]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("samplePrescale")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_samplePrescale")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg12]
$C$DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_name("sampleWindow")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_sampleWindow")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg14]
$C$DW$19	.dwtag  DW_TAG_formal_parameter, DW_AT_name("threshold")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_threshold")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -9]

;***************************************************************
;* FNAME: _CMPSS_configFilterLow        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_CMPSS_configFilterLow:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -2]
$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("samplePrescale")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_samplePrescale")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_breg20 -3]
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("sampleWindow")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_sampleWindow")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_breg20 -4]
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("regValue")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_regValue")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[4],AR5           ; [CPU_] |94| 
        MOV       *-SP[3],AR4           ; [CPU_] |94| 
        MOVL      *-SP[2],ACC           ; [CPU_] |94| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 109,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |109| 
        MOV       AH,*-SP[4]            ; [CPU_] |109| 
        ADDB      AL,#-1                ; [CPU_] |109| 
        ADDB      AH,#-1                ; [CPU_] |109| 
        LSL       AL,9                  ; [CPU_] |109| 
        LSL       AH,4                  ; [CPU_] |109| 
        OR        AL,AH                 ; [CPU_] |109| 
        MOV       *-SP[5],AL            ; [CPU_] |109| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 112,column 5,is_stmt
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("___eallow")
	.dwattr $C$DW$24, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |112| 
        ; call occurs [#___eallow] ; [] |112| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 114,column 5,is_stmt
        MOVB      ACC,#22               ; [CPU_] |114| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |114| 
        MOVL      XAR4,ACC              ; [CPU_] |114| 
        AND       AL,*+XAR4[0],#0xc00f  ; [CPU_] |114| 
        OR        AL,*-SP[5]            ; [CPU_] |114| 
        MOVZ      AR6,AL                ; [CPU_] |114| 
        MOVB      ACC,#22               ; [CPU_] |114| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |114| 
        MOVL      XAR4,ACC              ; [CPU_] |114| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |114| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 122,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |122| 
        MOVB      ACC,#23               ; [CPU_] |122| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |122| 
        MOVL      XAR4,ACC              ; [CPU_] |122| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |122| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 124,column 5,is_stmt
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("___edis")
	.dwattr $C$DW$25, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |124| 
        ; call occurs [#___edis] ; [] |124| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 125,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$15, DW_AT_TI_end_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x7d)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

	.sect	".text"
	.clink
	.global	_CMPSS_configLatchOnPWMSYNC

$C$DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("CMPSS_configLatchOnPWMSYNC")
	.dwattr $C$DW$27, DW_AT_low_pc(_CMPSS_configLatchOnPWMSYNC)
	.dwattr $C$DW$27, DW_AT_high_pc(0x00)
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_CMPSS_configLatchOnPWMSYNC")
	.dwattr $C$DW$27, DW_AT_external
	.dwattr $C$DW$27, DW_AT_TI_begin_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$27, DW_AT_TI_begin_line(0x85)
	.dwattr $C$DW$27, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$27, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 134,column 1,is_stmt,address _CMPSS_configLatchOnPWMSYNC

	.dwfde $C$DW$CIE, _CMPSS_configLatchOnPWMSYNC
$C$DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg0]
$C$DW$29	.dwtag  DW_TAG_formal_parameter, DW_AT_name("highEnable")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_highEnable")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg12]
$C$DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_name("lowEnable")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_lowEnable")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _CMPSS_configLatchOnPWMSYNC   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CMPSS_configLatchOnPWMSYNC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -2]
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("highEnable")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_highEnable")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -3]
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("lowEnable")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_lowEnable")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |134| 
        MOV       *-SP[3],AR4           ; [CPU_] |134| 
        MOVL      *-SP[2],ACC           ; [CPU_] |134| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 144,column 5,is_stmt
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("___eallow")
	.dwattr $C$DW$34, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |144| 
        ; call occurs [#___eallow] ; [] |144| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 146,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |146| 
        BF        $C$L1,EQ              ; [CPU_] |146| 
        ; branchcc occurs ; [] |146| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 148,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |148| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |148| 
        MOVL      XAR4,ACC              ; [CPU_] |148| 
        OR        *+XAR4[0],#0x0004     ; [CPU_] |148| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 149,column 5,is_stmt
        B         $C$L2,UNC             ; [CPU_] |149| 
        ; branch occurs ; [] |149| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 152,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |152| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |152| 
        MOVL      XAR4,ACC              ; [CPU_] |152| 
        AND       *+XAR4[0],#0xfffb     ; [CPU_] |152| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 159,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |159| 
        BF        $C$L3,EQ              ; [CPU_] |159| 
        ; branchcc occurs ; [] |159| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 161,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |161| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |161| 
        MOVL      XAR4,ACC              ; [CPU_] |161| 
        OR        *+XAR4[0],#0x0400     ; [CPU_] |161| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 162,column 5,is_stmt
        B         $C$L4,UNC             ; [CPU_] |162| 
        ; branch occurs ; [] |162| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 165,column 9,is_stmt
        MOVB      ACC,#3                ; [CPU_] |165| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |165| 
        MOVL      XAR4,ACC              ; [CPU_] |165| 
        AND       *+XAR4[0],#0xfbff     ; [CPU_] |165| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 168,column 5,is_stmt
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("___edis")
	.dwattr $C$DW$35, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |168| 
        ; call occurs [#___edis] ; [] |168| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 169,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$27, DW_AT_TI_end_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$27, DW_AT_TI_end_line(0xa9)
	.dwattr $C$DW$27, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$27

	.sect	".text"
	.clink
	.global	_CMPSS_configRamp

$C$DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("CMPSS_configRamp")
	.dwattr $C$DW$37, DW_AT_low_pc(_CMPSS_configRamp)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_CMPSS_configRamp")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_TI_begin_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0xb1)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 179,column 1,is_stmt,address _CMPSS_configRamp

	.dwfde $C$DW$CIE, _CMPSS_configRamp
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg0]
$C$DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("maxRampVal")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_maxRampVal")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg12]
$C$DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("decrementVal")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_decrementVal")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg14]
$C$DW$41	.dwtag  DW_TAG_formal_parameter, DW_AT_name("delayVal")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_delayVal")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -7]
$C$DW$42	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pwmSyncSrc")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_pwmSyncSrc")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -8]
$C$DW$43	.dwtag  DW_TAG_formal_parameter, DW_AT_name("useRampValShdw")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_useRampValShdw")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -9]

;***************************************************************
;* FNAME: _CMPSS_configRamp             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CMPSS_configRamp:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_breg20 -2]
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("maxRampVal")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_maxRampVal")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -3]
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("decrementVal")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_decrementVal")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |179| 
        MOV       *-SP[3],AR4           ; [CPU_] |179| 
        MOVL      *-SP[2],ACC           ; [CPU_] |179| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 187,column 5,is_stmt
$C$DW$47	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$47, DW_AT_low_pc(0x00)
	.dwattr $C$DW$47, DW_AT_name("___eallow")
	.dwattr $C$DW$47, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |187| 
        ; call occurs [#___eallow] ; [] |187| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 192,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |192| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |192| 
        MOVL      XAR4,ACC              ; [CPU_] |192| 
        MOV       AL,*-SP[8]            ; [CPU_] |192| 
        ADDB      AL,#-1                ; [CPU_] |192| 
        AND       AH,*+XAR4[0],#0xffe1  ; [CPU_] |192| 
        LSL       AL,1                  ; [CPU_] |192| 
        MOVZ      AR6,AL                ; [CPU_] |192| 
        OR        AR6,AH                ; [CPU_] |192| 
        MOVB      ACC,#4                ; [CPU_] |192| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |192| 
        MOVL      XAR4,ACC              ; [CPU_] |192| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |192| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 201,column 5,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |201| 
        BF        $C$L5,EQ              ; [CPU_] |201| 
        ; branchcc occurs ; [] |201| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 203,column 9,is_stmt
        MOVB      ACC,#4                ; [CPU_] |203| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |203| 
        MOVL      XAR4,ACC              ; [CPU_] |203| 
        OR        *+XAR4[0],#0x0040     ; [CPU_] |203| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 204,column 5,is_stmt
        B         $C$L6,UNC             ; [CPU_] |204| 
        ; branch occurs ; [] |204| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 207,column 9,is_stmt
        MOVB      ACC,#4                ; [CPU_] |207| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |207| 
        MOVL      XAR4,ACC              ; [CPU_] |207| 
        AND       *+XAR4[0],#0xffbf     ; [CPU_] |207| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 210,column 5,is_stmt
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_name("___edis")
	.dwattr $C$DW$48, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |210| 
        ; call occurs [#___edis] ; [] |210| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 215,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |215| 
        MOVB      ACC,#10               ; [CPU_] |215| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |215| 
        MOVL      XAR4,ACC              ; [CPU_] |215| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |215| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 220,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |220| 
        MOVB      ACC,#14               ; [CPU_] |220| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |220| 
        MOVL      XAR4,ACC              ; [CPU_] |220| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |220| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 225,column 5,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |225| 
        MOVB      ACC,#21               ; [CPU_] |225| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |225| 
        MOVL      XAR4,ACC              ; [CPU_] |225| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |225| 
	.dwpsn	file "../ExtraData/driverlib2/cmpss.c",line 226,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$37, DW_AT_TI_end_file("../ExtraData/driverlib2/cmpss.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0xe2)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg0]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg1]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg2]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg3]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg20]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg21]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg22]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg23]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg24]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg25]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg26]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg28]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg29]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg30]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg31]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x20]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x21]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x22]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x23]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x24]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x25]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x26]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg4]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg6]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg8]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg10]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg12]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg14]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg16]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg17]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg18]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg19]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg5]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg7]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg9]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg11]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg13]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg15]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x30]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x33]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x34]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x37]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x38]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x40]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x43]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x44]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x47]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x48]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x49]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x27]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x28]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg27]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

