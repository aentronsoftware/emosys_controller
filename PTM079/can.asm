;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Mon Jul 25 10:48:11 2022                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../can.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Emosys_Controller\PTM079")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_i_blink$1+0,32
	.bits	0,16			; _i_blink$1 @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("canDispatch")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_canDispatch")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$144)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$194)
$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$1

	.global	_can_bus_state
_can_bus_state:	.usect	".ebss",1,1,0
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("can_bus_state")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_can_bus_state")
	.dwattr $C$DW$5, DW_AT_location[DW_OP_addr _can_bus_state]
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$5, DW_AT_external
_i_blink$1:	.usect	".ebss",1,1,0
	.global	_dummy
_dummy:	.usect	".ebss",1,1,0
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("dummy")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_dummy")
	.dwattr $C$DW$6, DW_AT_location[DW_OP_addr _dummy]
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("ATM_seti")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_ATM_seti")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external
$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$255)
$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$91)
	.dwendtag $C$DW$7


$C$DW$10	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$249)
	.dwattr $C$DW$10, DW_AT_declaration
	.dwattr $C$DW$10, DW_AT_external
$C$DW$11	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$233)
$C$DW$12	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$243)
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$102)
	.dwendtag $C$DW$10


$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$249)
	.dwattr $C$DW$14, DW_AT_declaration
	.dwattr $C$DW$14, DW_AT_external
$C$DW$15	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$242)
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$102)
	.dwendtag $C$DW$14


$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$249)
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$233)
$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$243)
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$102)
	.dwendtag $C$DW$17

$C$DW$21	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$239)
	.dwattr $C$DW$23, DW_AT_declaration
	.dwattr $C$DW$23, DW_AT_external
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$230)
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("can_rx_mbox")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_can_rx_mbox")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$230)
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("ECanaRegs")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_ECanaRegs")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$226)
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("ECanaLAMRegs")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_ECanaLAMRegs")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("ECanaMOTORegs")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ECanaMOTORegs")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$234)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("ECanaMboxes")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_ECanaMboxes")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$225)
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2328412 
	.sect	".text"
	.clink
	.global	_calculateBRPREG2

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("calculateBRPREG2")
	.dwattr $C$DW$30, DW_AT_low_pc(_calculateBRPREG2)
	.dwattr $C$DW$30, DW_AT_high_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_calculateBRPREG2")
	.dwattr $C$DW$30, DW_AT_external
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$30, DW_AT_TI_begin_file("../can.c")
	.dwattr $C$DW$30, DW_AT_TI_begin_line(0x23)
	.dwattr $C$DW$30, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$30, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../can.c",line 36,column 1,is_stmt,address _calculateBRPREG2

	.dwfde $C$DW$CIE, _calculateBRPREG2
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("baudrate")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_baudrate")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _calculateBRPREG2             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_calculateBRPREG2:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("baudrate")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_baudrate")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |36| 
	.dwpsn	file "../can.c",line 37,column 3,is_stmt
        B         $C$L6,UNC             ; [CPU_] |37| 
        ; branch occurs ; [] |37| 
$C$L1:    
	.dwpsn	file "../can.c",line 40,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |40| 
        B         $C$L7,UNC             ; [CPU_] |40| 
        ; branch occurs ; [] |40| 
$C$L2:    
	.dwpsn	file "../can.c",line 42,column 7,is_stmt
        MOVB      AL,#3                 ; [CPU_] |42| 
        B         $C$L7,UNC             ; [CPU_] |42| 
        ; branch occurs ; [] |42| 
$C$L3:    
	.dwpsn	file "../can.c",line 44,column 7,is_stmt
        MOVB      AL,#7                 ; [CPU_] |44| 
        B         $C$L7,UNC             ; [CPU_] |44| 
        ; branch occurs ; [] |44| 
$C$L4:    
	.dwpsn	file "../can.c",line 46,column 7,is_stmt
        MOVB      AL,#19                ; [CPU_] |46| 
        B         $C$L7,UNC             ; [CPU_] |46| 
        ; branch occurs ; [] |46| 
$C$L5:    
	.dwpsn	file "../can.c",line 49,column 7,is_stmt
        MOVB      AL,#1                 ; [CPU_] |49| 
        B         $C$L7,UNC             ; [CPU_] |49| 
        ; branch occurs ; [] |49| 
$C$L6:    
	.dwpsn	file "../can.c",line 37,column 3,is_stmt
        CMPB      AL,#100               ; [CPU_] |37| 
        BF        $C$L4,EQ              ; [CPU_] |37| 
        ; branchcc occurs ; [] |37| 
        CMPB      AL,#250               ; [CPU_] |37| 
        BF        $C$L3,EQ              ; [CPU_] |37| 
        ; branchcc occurs ; [] |37| 
        CMP       AL,#500               ; [CPU_] |37| 
        BF        $C$L2,EQ              ; [CPU_] |37| 
        ; branchcc occurs ; [] |37| 
        CMP       AL,#1000              ; [CPU_] |37| 
        BF        $C$L1,EQ              ; [CPU_] |37| 
        ; branchcc occurs ; [] |37| 
        B         $C$L5,UNC             ; [CPU_] |37| 
        ; branch occurs ; [] |37| 
$C$L7:    
	.dwpsn	file "../can.c",line 51,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$30, DW_AT_TI_end_file("../can.c")
	.dwattr $C$DW$30, DW_AT_TI_end_line(0x33)
	.dwattr $C$DW$30, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$30

	.sect	".text"
	.clink
	.global	_canInit

$C$DW$34	.dwtag  DW_TAG_subprogram, DW_AT_name("canInit")
	.dwattr $C$DW$34, DW_AT_low_pc(_canInit)
	.dwattr $C$DW$34, DW_AT_high_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_canInit")
	.dwattr $C$DW$34, DW_AT_external
	.dwattr $C$DW$34, DW_AT_TI_begin_file("../can.c")
	.dwattr $C$DW$34, DW_AT_TI_begin_line(0x35)
	.dwattr $C$DW$34, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$34, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../can.c",line 54,column 1,is_stmt,address _canInit

	.dwfde $C$DW$CIE, _canInit
$C$DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_name("baudrate")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_baudrate")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg0]
$C$DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_name("nodeID")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_nodeID")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _canInit                      FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_canInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("baudrate")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_baudrate")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -1]
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("nodeID")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_nodeID")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -2]
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("p_mbox")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_p_mbox")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$229)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -4]
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("p_lam_reg")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_p_lam_reg")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$202)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg20 -6]
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANBTC")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ECanaShadow_CANBTC")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -8]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -9]
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("aux32")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_aux32")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -12]
        MOV       *-SP[2],AH            ; [CPU_] |54| 
        MOV       *-SP[1],AL            ; [CPU_] |54| 
	.dwpsn	file "../can.c",line 61,column 2,is_stmt
  EALLOW
	.dwpsn	file "../can.c",line 63,column 2,is_stmt
        MOVW      DP,#_ECanaRegs+42     ; [CPU_U] 
        OR        @_ECanaRegs+42,#0x0008 ; [CPU_] |63| 
	.dwpsn	file "../can.c",line 64,column 2,is_stmt
        OR        @_ECanaRegs+44,#0x0008 ; [CPU_] |64| 
	.dwpsn	file "../can.c",line 67,column 2,is_stmt
        OR        @_ECanaRegs+20,#0x2000 ; [CPU_] |67| 
	.dwpsn	file "../can.c",line 70,column 2,is_stmt
        OR        @_ECanaRegs+20,#0x0080 ; [CPU_] |70| 
	.dwpsn	file "../can.c",line 76,column 2,is_stmt
        OR        @_ECanaRegs+20,#0x1000 ; [CPU_] |76| 
	.dwpsn	file "../can.c",line 77,column 2,is_stmt
$C$L8:    
	.dwpsn	file "../can.c",line 77,column 8,is_stmt
        AND       AL,@_ECanaRegs+24,#0x0010 ; [CPU_] |77| 
        LSR       AL,4                  ; [CPU_] |77| 
        CMPB      AL,#1                 ; [CPU_] |77| 
        BF        $C$L8,NEQ             ; [CPU_] |77| 
        ; branchcc occurs ; [] |77| 
	.dwpsn	file "../can.c",line 79,column 2,is_stmt
        MOVB      ACC,#0                ; [CPU_] |79| 
        MOVL      *-SP[8],ACC           ; [CPU_] |79| 
	.dwpsn	file "../can.c",line 80,column 2,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |80| 
        SPM       #0                    ; [CPU_] 
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("_calculateBRPREG2")
	.dwattr $C$DW$44, DW_AT_TI_call
        LCR       #_calculateBRPREG2    ; [CPU_] |80| 
        ; call occurs [#_calculateBRPREG2] ; [] |80| 
        MOV       AH,*-SP[7]            ; [CPU_] |80| 
        MOVB      AH,AL.LSB             ; [CPU_] |80| 
        MOV       *-SP[7],AH            ; [CPU_] |80| 
	.dwpsn	file "../can.c",line 81,column 2,is_stmt
        AND       AL,*-SP[8],#0xfff8    ; [CPU_] |81| 
        ORB       AL,#0x01              ; [CPU_] |81| 
        MOV       *-SP[8],AL            ; [CPU_] |81| 
	.dwpsn	file "../can.c",line 82,column 2,is_stmt
        AND       AL,*-SP[8],#0xff87    ; [CPU_] |82| 
        ORB       AL,#0x30              ; [CPU_] |82| 
        MOV       *-SP[8],AL            ; [CPU_] |82| 
	.dwpsn	file "../can.c",line 83,column 2,is_stmt
        AND       AL,*-SP[8],#0xfcff    ; [CPU_] |83| 
        OR        AL,#0x0100            ; [CPU_] |83| 
        MOV       *-SP[8],AL            ; [CPU_] |83| 
	.dwpsn	file "../can.c",line 84,column 2,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |84| 
        MOVL      @_ECanaRegs+22,ACC    ; [CPU_] |84| 
	.dwpsn	file "../can.c",line 86,column 2,is_stmt
        AND       @_ECanaRegs+20,#0xefff ; [CPU_] |86| 
	.dwpsn	file "../can.c",line 87,column 2,is_stmt
$C$L9:    
	.dwpsn	file "../can.c",line 87,column 8,is_stmt
        AND       AL,@_ECanaRegs+24,#0x0010 ; [CPU_] |87| 
        LSR       AL,4                  ; [CPU_] |87| 
        CMPB      AL,#1                 ; [CPU_] |87| 
        BF        $C$L9,EQ              ; [CPU_] |87| 
        ; branchcc occurs ; [] |87| 
	.dwpsn	file "../can.c",line 89,column 2,is_stmt
        MOVW      DP,#_can_bus_state    ; [CPU_U] 
        MOVB      @_can_bus_state,#1,UNC ; [CPU_] |89| 
	.dwpsn	file "../can.c",line 92,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |92| 
        MOVW      DP,#_ECanaRegs        ; [CPU_U] 
        MOVL      @_ECanaRegs,ACC       ; [CPU_] |92| 
	.dwpsn	file "../can.c",line 93,column 2,is_stmt
  EDIS
	.dwpsn	file "../can.c",line 98,column 6,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |98| 
	.dwpsn	file "../can.c",line 98,column 10,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |98| 
        CMPB      AL,#32                ; [CPU_] |98| 
        B         $C$L15,GEQ            ; [CPU_] |98| 
        ; branchcc occurs ; [] |98| 
$C$L10:    
	.dwpsn	file "../can.c",line 100,column 3,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ECanaMboxes    ; [CPU_U] |100| 
        MOV       ACC,*-SP[9] << 3      ; [CPU_] |100| 
        ADDL      XAR4,ACC              ; [CPU_] |100| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |100| 
	.dwpsn	file "../can.c",line 101,column 3,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |101| 
        AND       *+XAR4[1],#0x7fff     ; [CPU_] |101| 
	.dwpsn	file "../can.c",line 102,column 3,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |102| 
        OR        *+XAR4[1],#0x4000     ; [CPU_] |102| 
	.dwpsn	file "../can.c",line 103,column 3,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |103| 
        AND       *+XAR4[1],#0xdfff     ; [CPU_] |103| 
	.dwpsn	file "../can.c",line 105,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |105| 
        CMPB      AL,#16                ; [CPU_] |105| 
        B         $C$L11,LT             ; [CPU_] |105| 
        ; branchcc occurs ; [] |105| 
	.dwpsn	file "../can.c",line 106,column 4,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |106| 
        AND       *+XAR4[1],#0xe003     ; [CPU_] |106| 
        B         $C$L14,UNC            ; [CPU_] |106| 
        ; branch occurs ; [] |106| 
$C$L11:    
	.dwpsn	file "../can.c",line 107,column 8,is_stmt
        CMPB      AL,#10                ; [CPU_] |107| 
        B         $C$L12,GEQ            ; [CPU_] |107| 
        ; branchcc occurs ; [] |107| 
	.dwpsn	file "../can.c",line 108,column 4,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |108| 
        ADDB      XAR4,#1               ; [CPU_] |108| 
        AND       AL,*+XAR4[0],#0xe003  ; [CPU_] |108| 
        ORB       AL,#0x04              ; [CPU_] |108| 
        MOV       *+XAR4[0],AL          ; [CPU_] |108| 
        B         $C$L14,UNC            ; [CPU_] |108| 
        ; branch occurs ; [] |108| 
$C$L12:    
	.dwpsn	file "../can.c",line 109,column 8,is_stmt
        CMPB      AL,#13                ; [CPU_] |109| 
        B         $C$L13,GEQ            ; [CPU_] |109| 
        ; branchcc occurs ; [] |109| 
	.dwpsn	file "../can.c",line 110,column 7,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |110| 
        AND       AL,*-SP[2],#0x07ff    ; [CPU_] |110| 
        LSL       AL,2                  ; [CPU_] |110| 
        ADDB      XAR4,#1               ; [CPU_] |110| 
        AND       AH,*+XAR4[0],#0xe003  ; [CPU_] |110| 
        OR        AL,AH                 ; [CPU_] |110| 
        MOV       *+XAR4[0],AL          ; [CPU_] |110| 
        B         $C$L14,UNC            ; [CPU_] |110| 
        ; branch occurs ; [] |110| 
$C$L13:    
	.dwpsn	file "../can.c",line 112,column 4,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |112| 
        ADDB      XAR4,#1               ; [CPU_] |112| 
        AND       AL,*+XAR4[0],#0xe003  ; [CPU_] |112| 
        OR        AL,#0x1600            ; [CPU_] |112| 
        MOV       *+XAR4[0],AL          ; [CPU_] |112| 
$C$L14:    
	.dwpsn	file "../can.c",line 98,column 46,is_stmt
        INC       *-SP[9]               ; [CPU_] |98| 
	.dwpsn	file "../can.c",line 98,column 10,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |98| 
        CMPB      AL,#32                ; [CPU_] |98| 
        B         $C$L10,LT             ; [CPU_] |98| 
        ; branchcc occurs ; [] |98| 
$C$L15:    
	.dwpsn	file "../can.c",line 116,column 2,is_stmt
        MOVL      XAR4,#65535           ; [CPU_U] |116| 
        MOVW      DP,#_ECanaRegs+2      ; [CPU_U] 
        MOVL      @_ECanaRegs+2,XAR4    ; [CPU_] |116| 
	.dwpsn	file "../can.c",line 119,column 2,is_stmt
        MOVL      *-SP[12],XAR4         ; [CPU_] |119| 
	.dwpsn	file "../can.c",line 120,column 6,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |120| 
	.dwpsn	file "../can.c",line 120,column 10,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |120| 
        CMPB      AL,#16                ; [CPU_] |120| 
        B         $C$L17,GEQ            ; [CPU_] |120| 
        ; branchcc occurs ; [] |120| 
$C$L16:    
	.dwpsn	file "../can.c",line 120,column 34,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |120| 
        LSL       ACC,1                 ; [CPU_] |120| 
        ADDB      ACC,#1                ; [CPU_] |120| 
        MOVL      *-SP[12],ACC          ; [CPU_] |120| 
	.dwpsn	file "../can.c",line 120,column 29,is_stmt
        INC       *-SP[9]               ; [CPU_] |120| 
	.dwpsn	file "../can.c",line 120,column 10,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |120| 
        CMPB      AL,#16                ; [CPU_] |120| 
        B         $C$L16,LT             ; [CPU_] |120| 
        ; branchcc occurs ; [] |120| 
$C$L17:    
	.dwpsn	file "../can.c",line 121,column 2,is_stmt
        MOVL      ACC,*-SP[12]          ; [CPU_] |121| 
        MOVW      DP,#_ECanaRegs        ; [CPU_U] 
        MOVL      @_ECanaRegs,ACC       ; [CPU_] |121| 
	.dwpsn	file "../can.c",line 124,column 6,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |124| 
	.dwpsn	file "../can.c",line 124,column 10,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |124| 
        CMPB      AL,#16                ; [CPU_] |124| 
        B         $C$L22,GEQ            ; [CPU_] |124| 
        ; branchcc occurs ; [] |124| 
$C$L18:    
	.dwpsn	file "../can.c",line 126,column 3,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ECanaLAMRegs   ; [CPU_U] |126| 
        MOV       ACC,*-SP[9] << 1      ; [CPU_] |126| 
        ADDL      XAR4,ACC              ; [CPU_] |126| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |126| 
	.dwpsn	file "../can.c",line 128,column 3,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |128| 
        AND       *+XAR4[1],#0x7fff     ; [CPU_] |128| 
	.dwpsn	file "../can.c",line 129,column 3,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |129| 
        MOV       *+XAR4[0],#0          ; [CPU_] |129| 
	.dwpsn	file "../can.c",line 130,column 3,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |130| 
        CMPB      AL,#12                ; [CPU_] |130| 
        B         $C$L19,LEQ            ; [CPU_] |130| 
        ; branchcc occurs ; [] |130| 
	.dwpsn	file "../can.c",line 131,column 4,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |131| 
        ADDB      XAR4,#1               ; [CPU_] |131| 
        AND       AL,*+XAR4[0],#0xe000  ; [CPU_] |131| 
        OR        AL,#0x03fc            ; [CPU_] |131| 
        MOV       *+XAR4[0],AL          ; [CPU_] |131| 
        B         $C$L21,UNC            ; [CPU_] |131| 
        ; branch occurs ; [] |131| 
$C$L19:    
	.dwpsn	file "../can.c",line 133,column 8,is_stmt
        CMPB      AL,#9                 ; [CPU_] |133| 
        B         $C$L20,LEQ            ; [CPU_] |133| 
        ; branchcc occurs ; [] |133| 
	.dwpsn	file "../can.c",line 134,column 7,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |134| 
        ADDB      XAR4,#1               ; [CPU_] |134| 
        AND       AL,*+XAR4[0],#0xe000  ; [CPU_] |134| 
        OR        AL,#0x1e00            ; [CPU_] |134| 
        MOV       *+XAR4[0],AL          ; [CPU_] |134| 
        B         $C$L21,UNC            ; [CPU_] |134| 
        ; branch occurs ; [] |134| 
$C$L20:    
	.dwpsn	file "../can.c",line 136,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |136| 
        ADDB      XAR4,#1               ; [CPU_] |136| 
        AND       AL,*+XAR4[0],#0xe000  ; [CPU_] |136| 
        OR        AL,#0x0ffc            ; [CPU_] |136| 
        MOV       *+XAR4[0],AL          ; [CPU_] |136| 
$C$L21:    
	.dwpsn	file "../can.c",line 124,column 29,is_stmt
        INC       *-SP[9]               ; [CPU_] |124| 
	.dwpsn	file "../can.c",line 124,column 10,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |124| 
        CMPB      AL,#16                ; [CPU_] |124| 
        B         $C$L18,LT             ; [CPU_] |124| 
        ; branchcc occurs ; [] |124| 
$C$L22:    
	.dwpsn	file "../can.c",line 141,column 2,is_stmt
        MOVL      XAR4,#65470           ; [CPU_U] |141| 
        MOVW      DP,#_ECanaRegs+40     ; [CPU_U] 
        MOVL      @_ECanaRegs+40,XAR4   ; [CPU_] |141| 
	.dwpsn	file "../can.c",line 144,column 2,is_stmt
        MOVB      ACC,#0                ; [CPU_] |144| 
        SUBB      ACC,#1                ; [CPU_] |144| 
        MOVL      @_ECanaRegs+38,ACC    ; [CPU_] |144| 
	.dwpsn	file "../can.c",line 146,column 2,is_stmt
 EALLOW
	.dwpsn	file "../can.c",line 148,column 2,is_stmt
        MOVL      XAR4,#65535           ; [CPU_U] |148| 
        MOVL      @_ECanaRegs+36,XAR4   ; [CPU_] |148| 
	.dwpsn	file "../can.c",line 151,column 2,is_stmt
        MOVL      XAR4,#143107          ; [CPU_U] |151| 
        MOVL      @_ECanaRegs+32,XAR4   ; [CPU_] |151| 
	.dwpsn	file "../can.c",line 152,column 2,is_stmt
 EDIS
	.dwpsn	file "../can.c",line 154,column 2,is_stmt
        MOVW      DP,#_PieCtrlRegs+18   ; [CPU_U] 
        OR        @_PieCtrlRegs+18,#0x0010 ; [CPU_] |154| 
	.dwpsn	file "../can.c",line 155,column 2,is_stmt
        OR        @_PieCtrlRegs+18,#0x0020 ; [CPU_] |155| 
	.dwpsn	file "../can.c",line 156,column 2,is_stmt
        OR        IER,#0x0100           ; [CPU_] |156| 
	.dwpsn	file "../can.c",line 157,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$34, DW_AT_TI_end_file("../can.c")
	.dwattr $C$DW$34, DW_AT_TI_end_line(0x9d)
	.dwattr $C$DW$34, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$34

	.sect	"ramfuncs:retain"
	.retain
	.retainrefs
	.global	_hwiFnCan0

$C$DW$46	.dwtag  DW_TAG_subprogram, DW_AT_name("hwiFnCan0")
	.dwattr $C$DW$46, DW_AT_low_pc(_hwiFnCan0)
	.dwattr $C$DW$46, DW_AT_high_pc(0x00)
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_hwiFnCan0")
	.dwattr $C$DW$46, DW_AT_external
	.dwattr $C$DW$46, DW_AT_TI_begin_file("../can.c")
	.dwattr $C$DW$46, DW_AT_TI_begin_line(0xa6)
	.dwattr $C$DW$46, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$46, DW_AT_TI_interrupt
	.dwattr $C$DW$46, DW_AT_TI_max_frame_size(-48)
	.dwpsn	file "../can.c",line 167,column 1,is_stmt,address _hwiFnCan0

	.dwfde $C$DW$CIE, _hwiFnCan0

;***************************************************************
;* FNAME: _hwiFnCan0                    FR SIZE:  46           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 21 Auto, 24 SOE     *
;***************************************************************

_hwiFnCan0:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        PUSH      AR1H:AR0H             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 5, 4
	.dwcfi	save_reg_to_mem, 7, 5
	.dwcfi	cfa_offset, -6
        MOVL      *SP++,XT              ; [CPU_] 
	.dwcfi	save_reg_to_mem, 21, 6
	.dwcfi	save_reg_to_mem, 22, 7
	.dwcfi	cfa_offset, -8
        MOVL      *SP++,XAR4            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 12, 8
	.dwcfi	save_reg_to_mem, 13, 9
	.dwcfi	cfa_offset, -10
        MOVL      *SP++,XAR5            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 14, 10
	.dwcfi	save_reg_to_mem, 15, 11
	.dwcfi	cfa_offset, -12
        MOVL      *SP++,XAR6            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 16, 12
	.dwcfi	save_reg_to_mem, 17, 13
	.dwcfi	cfa_offset, -14
        MOVL      *SP++,XAR7            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 18, 14
	.dwcfi	save_reg_to_mem, 19, 15
	.dwcfi	cfa_offset, -16
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 16
	.dwcfi	save_reg_to_mem, 40, 17
	.dwcfi	cfa_offset, -18
        MOV32     *SP++,R0H             ; [CPU_] 
        MOV32     *SP++,R1H             ; [CPU_] 
        MOV32     *SP++,R2H             ; [CPU_] 
        MOV32     *SP++,R3H             ; [CPU_] 
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        ADDB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -48
        SPM       0                     ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANGIF1")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ECanaShadow_CANGIF1")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -2]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANTOS")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ECanaShadow_CANTOS")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -4]
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANTOC")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ECanaShadow_CANTOC")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_breg20 -6]
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("p_mbox")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_p_mbox")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$229)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -8]
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("tmp_u32")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_tmp_u32")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -10]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -21]
	.dwpsn	file "../can.c",line 174,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        OR        @_PieCtrlRegs+1,#0x0100 ; [CPU_] |174| 
	.dwpsn	file "../can.c",line 175,column 3,is_stmt
 clrc INTM
	.dwpsn	file "../can.c",line 176,column 3,is_stmt
        MOV       AL,#1033              ; [CPU_] |176| 
        MOV       IER,AL                ; [CPU_] |176| 
 clrc INTM
	.dwpsn	file "../can.c",line 177,column 3,is_stmt
        MOVW      DP,#_ECanaRegs+34     ; [CPU_U] 
        MOVL      ACC,@_ECanaRegs+34    ; [CPU_] |177| 
        MOVL      *-SP[2],ACC           ; [CPU_] |177| 
	.dwpsn	file "../can.c",line 178,column 3,is_stmt
        AND       AL,*-SP[2],#0x8000    ; [CPU_] |178| 
        LSR       AL,15                 ; [CPU_] |178| 
        CMPB      AL,#1                 ; [CPU_] |178| 
        BF        $C$L28,NEQ            ; [CPU_] |178| 
        ; branchcc occurs ; [] |178| 
	.dwpsn	file "../can.c",line 180,column 5,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |180| 
        ANDB      AL,#0x1f              ; [CPU_] |180| 
        CMPB      AL,#16                ; [CPU_] |180| 
        B         $C$L27,GEQ            ; [CPU_] |180| 
        ; branchcc occurs ; [] |180| 
	.dwpsn	file "../can.c",line 182,column 7,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |182| 
        MOVL      XAR4,#_ECanaMboxes    ; [CPU_U] |182| 
        ANDB      AL,#0x1f              ; [CPU_] |182| 
        MOVU      ACC,AL                ; [CPU_] |182| 
        LSL       ACC,3                 ; [CPU_] |182| 
        ADDL      XAR4,ACC              ; [CPU_] |182| 
        MOVL      *-SP[8],XAR4          ; [CPU_] |182| 
	.dwpsn	file "../can.c",line 185,column 7,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |185| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |185| 
        MOVL      *-SP[10],ACC          ; [CPU_] |185| 
	.dwpsn	file "../can.c",line 186,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |186| 
        BF        $C$L23,NEQ            ; [CPU_] |186| 
        ; branchcc occurs ; [] |186| 
	.dwpsn	file "../can.c",line 186,column 26,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |186| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |186| 
        MOVL      *-SP[10],ACC          ; [CPU_] |186| 
$C$L23:    
	.dwpsn	file "../can.c",line 188,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |188| 
        MOV       T,#18                 ; [CPU_] |188| 
        LSRL      ACC,T                 ; [CPU_] |188| 
        AND       AL,#0x07ff            ; [CPU_] |188| 
        MOV       *-SP[21],AL           ; [CPU_] |188| 
	.dwpsn	file "../can.c",line 194,column 7,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |194| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |194| 
        MOVL      *-SP[10],ACC          ; [CPU_] |194| 
	.dwpsn	file "../can.c",line 195,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |195| 
        BF        $C$L24,NEQ            ; [CPU_] |195| 
        ; branchcc occurs ; [] |195| 
	.dwpsn	file "../can.c",line 195,column 26,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |195| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |195| 
        MOVL      *-SP[10],ACC          ; [CPU_] |195| 
$C$L24:    
	.dwpsn	file "../can.c",line 197,column 7,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |197| 
        ANDB      AL,#0x0f              ; [CPU_] |197| 
        MOV       *-SP[19],AL           ; [CPU_] |197| 
	.dwpsn	file "../can.c",line 198,column 7,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*-SP[10]          ; [CPU_] |198| 
        SFR       ACC,4                 ; [CPU_] |198| 
        ANDB      AL,#0x01              ; [CPU_] |198| 
        MOV       *-SP[20],AL           ; [CPU_] |198| 
	.dwpsn	file "../can.c",line 201,column 7,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |201| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |201| 
        MOVL      *-SP[10],ACC          ; [CPU_] |201| 
	.dwpsn	file "../can.c",line 202,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |202| 
        BF        $C$L25,NEQ            ; [CPU_] |202| 
        ; branchcc occurs ; [] |202| 
	.dwpsn	file "../can.c",line 202,column 26,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |202| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |202| 
        MOVL      *-SP[10],ACC          ; [CPU_] |202| 
$C$L25:    
	.dwpsn	file "../can.c",line 204,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |204| 
        MOV       T,#24                 ; [CPU_] |204| 
        LSRL      ACC,T                 ; [CPU_] |204| 
        ANDB      AL,#0xff              ; [CPU_] |204| 
        MOV       *-SP[18],AL           ; [CPU_] |204| 
	.dwpsn	file "../can.c",line 205,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |205| 
        ANDB      AH,#0xff              ; [CPU_] |205| 
        MOV       *-SP[17],AH           ; [CPU_] |205| 
	.dwpsn	file "../can.c",line 206,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |206| 
        SFR       ACC,8                 ; [CPU_] |206| 
        ANDB      AL,#0xff              ; [CPU_] |206| 
        MOV       *-SP[16],AL           ; [CPU_] |206| 
	.dwpsn	file "../can.c",line 207,column 7,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |207| 
        ANDB      AL,#0xff              ; [CPU_] |207| 
        MOV       *-SP[15],AL           ; [CPU_] |207| 
	.dwpsn	file "../can.c",line 210,column 7,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |210| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |210| 
        MOVL      *-SP[10],ACC          ; [CPU_] |210| 
	.dwpsn	file "../can.c",line 211,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |211| 
        BF        $C$L26,NEQ            ; [CPU_] |211| 
        ; branchcc occurs ; [] |211| 
	.dwpsn	file "../can.c",line 211,column 26,is_stmt
        MOVL      XAR4,*-SP[8]          ; [CPU_] |211| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |211| 
        MOVL      *-SP[10],ACC          ; [CPU_] |211| 
$C$L26:    
	.dwpsn	file "../can.c",line 213,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |213| 
        LSRL      ACC,T                 ; [CPU_] |213| 
        ANDB      AL,#0xff              ; [CPU_] |213| 
        MOV       *-SP[14],AL           ; [CPU_] |213| 
	.dwpsn	file "../can.c",line 214,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |214| 
        ANDB      AH,#0xff              ; [CPU_] |214| 
        MOV       *-SP[13],AH           ; [CPU_] |214| 
	.dwpsn	file "../can.c",line 215,column 7,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |215| 
        SFR       ACC,8                 ; [CPU_] |215| 
        ANDB      AL,#0xff              ; [CPU_] |215| 
        MOV       *-SP[12],AL           ; [CPU_] |215| 
	.dwpsn	file "../can.c",line 216,column 7,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |216| 
        ANDB      AL,#0xff              ; [CPU_] |216| 
        MOV       *-SP[11],AL           ; [CPU_] |216| 
	.dwpsn	file "../can.c",line 218,column 7,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |218| 
        MOVL      XAR4,#_can_rx_mbox    ; [CPU_U] |218| 
        SPM       #0                    ; [CPU_] 
        MOVB      AL,#0                 ; [CPU_] |218| 
        SUBB      XAR5,#21              ; [CPU_U] |218| 
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("_MBX_post")
	.dwattr $C$DW$53, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |218| 
        ; call occurs [#_MBX_post] ; [] |218| 
	.dwpsn	file "../can.c",line 222,column 7,is_stmt
        MOVW      DP,#_dummy            ; [CPU_U] 
        MOVB      @_dummy,#1,UNC        ; [CPU_] |222| 
	.dwpsn	file "../can.c",line 223,column 7,is_stmt
        MOVB      ACC,#1                ; [CPU_] |223| 
        MOVW      DP,#_ECanaRegs+12     ; [CPU_U] 
        MOVL      XAR6,ACC              ; [CPU_] |223| 
        MOV       AL,*-SP[2]            ; [CPU_] |223| 
        ANDB      AL,#0x1f              ; [CPU_] |223| 
        MOV       T,AL                  ; [CPU_] |223| 
        MOVL      ACC,XAR6              ; [CPU_] |223| 
        LSLL      ACC,T                 ; [CPU_] |223| 
        MOVL      @_ECanaRegs+12,ACC    ; [CPU_] |223| 
	.dwpsn	file "../can.c",line 224,column 5,is_stmt
        B         $C$L29,UNC            ; [CPU_] |224| 
        ; branch occurs ; [] |224| 
$C$L27:    
	.dwpsn	file "../can.c",line 227,column 7,is_stmt
        MOVB      ACC,#1                ; [CPU_] |227| 
        MOVL      XAR6,ACC              ; [CPU_] |227| 
        MOV       AL,*-SP[2]            ; [CPU_] |227| 
        ANDB      AL,#0x1f              ; [CPU_] |227| 
        MOV       T,AL                  ; [CPU_] |227| 
        MOVL      ACC,XAR6              ; [CPU_] |227| 
        LSLL      ACC,T                 ; [CPU_] |227| 
        MOVL      @_ECanaRegs+8,ACC     ; [CPU_] |227| 
	.dwpsn	file "../can.c",line 229,column 3,is_stmt
        B         $C$L29,UNC            ; [CPU_] |229| 
        ; branch occurs ; [] |229| 
$C$L28:    
	.dwpsn	file "../can.c",line 230,column 8,is_stmt
        AND       AL,*-SP[1],#0x0002    ; [CPU_] |230| 
        LSR       AL,1                  ; [CPU_] |230| 
        CMPB      AL,#1                 ; [CPU_] |230| 
        BF        $C$L29,NEQ            ; [CPU_] |230| 
        ; branchcc occurs ; [] |230| 
	.dwpsn	file "../can.c",line 231,column 5,is_stmt
        MOVL      ACC,@_ECanaRegs+50    ; [CPU_] |231| 
        MOVL      *-SP[4],ACC           ; [CPU_] |231| 
	.dwpsn	file "../can.c",line 234,column 5,is_stmt
        MOVL      ACC,@_ECanaRegs+48    ; [CPU_] |234| 
        MOVL      *-SP[6],ACC           ; [CPU_] |234| 
	.dwpsn	file "../can.c",line 235,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |235| 
        NOT       ACC                   ; [CPU_] |235| 
        AND       *-SP[6],AL            ; [CPU_] |235| 
        AND       *-SP[5],AH            ; [CPU_] |235| 
	.dwpsn	file "../can.c",line 236,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |236| 
        MOVL      @_ECanaRegs+48,ACC    ; [CPU_] |236| 
	.dwpsn	file "../can.c",line 238,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |238| 
        MOVL      @_ECanaRegs+6,ACC     ; [CPU_] |238| 
	.dwpsn	file "../can.c",line 239,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |239| 
        MOVL      @_ECanaRegs+50,ACC    ; [CPU_] |239| 
	.dwpsn	file "../can.c",line 242,column 3,is_stmt
$C$L29:    
	.dwpsn	file "../can.c",line 246,column 3,is_stmt
        MOV       AL,#1023              ; [CPU_] |246| 
        MOV       IER,AL                ; [CPU_] |246| 
 clrc INTM
	.dwpsn	file "../can.c",line 247,column 1,is_stmt
        SUBB      SP,#22                ; [CPU_U] 
	.dwcfi	cfa_offset, -26
        MOV32     R3H,*--SP             ; [CPU_] 
        MOV32     R2H,*--SP             ; [CPU_] 
        MOV32     R1H,*--SP             ; [CPU_] 
        MOV32     R0H,*--SP             ; [CPU_] 
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -16
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        MOVL      XAR7,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -14
	.dwcfi	restore_reg, 18
        MOVL      XAR6,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -12
	.dwcfi	restore_reg, 16
        MOVL      XAR5,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -10
	.dwcfi	restore_reg, 14
        MOVL      XAR4,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -8
	.dwcfi	restore_reg, 12
        MOVL      XT,*--SP              ; [CPU_] 
	.dwcfi	cfa_offset, -6
	.dwcfi	restore_reg, 22
	.dwcfi	restore_reg, 21
        POP       AR1H:AR0H             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 5
	.dwcfi	restore_reg, 7
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$46, DW_AT_TI_end_file("../can.c")
	.dwattr $C$DW$46, DW_AT_TI_end_line(0xf7)
	.dwattr $C$DW$46, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$46

	.sect	".text"
	.clink
	.global	_TaskCanSendLoop

$C$DW$55	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskCanSendLoop")
	.dwattr $C$DW$55, DW_AT_low_pc(_TaskCanSendLoop)
	.dwattr $C$DW$55, DW_AT_high_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_TaskCanSendLoop")
	.dwattr $C$DW$55, DW_AT_external
	.dwattr $C$DW$55, DW_AT_TI_begin_file("../can.c")
	.dwattr $C$DW$55, DW_AT_TI_begin_line(0xfb)
	.dwattr $C$DW$55, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$55, DW_AT_TI_max_frame_size(-30)
	.dwpsn	file "../can.c",line 252,column 1,is_stmt,address _TaskCanSendLoop

	.dwfde $C$DW$CIE, _TaskCanSendLoop

;***************************************************************
;* FNAME: _TaskCanSendLoop              FR SIZE:  28           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 28 Auto,  0 SOE     *
;***************************************************************

_TaskCanSendLoop:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#28                ; [CPU_U] 
	.dwcfi	cfa_offset, -30
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -11]
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANME")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ECanaShadow_CANME")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg20 -14]
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANTOC")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ECanaShadow_CANTOC")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg20 -16]
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANMDL")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ECanaShadow_CANMDL")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg20 -18]
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANMDH")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ECanaShadow_CANMDH")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg20 -20]
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -21]
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("found")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_found")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_breg20 -22]
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("aux32")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_aux32")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -24]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("p_mbox")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_p_mbox")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$229)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -26]
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("p_moto_reg")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_p_moto_reg")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$263)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -28]
	.dwpsn	file "../can.c",line 264,column 9,is_stmt
$C$L30:    
	.dwpsn	file "../can.c",line 266,column 3,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |266| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |266| 
        SPM       #0                    ; [CPU_] 
        MOV       AL,#65535             ; [CPU_] |266| 
        SUBB      XAR5,#11              ; [CPU_U] |266| 
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$66, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |266| 
        ; call occurs [#_MBX_pend] ; [] |266| 
	.dwpsn	file "../can.c",line 267,column 3,is_stmt
        MOV       *-SP[22],#0           ; [CPU_] |267| 
	.dwpsn	file "../can.c",line 268,column 3,is_stmt
        MOVB      *-SP[21],#16,UNC      ; [CPU_] |268| 
	.dwpsn	file "../can.c",line 268,column 23,is_stmt
        MOVL      XAR4,#65536           ; [CPU_U] |268| 
        MOVL      *-SP[24],XAR4         ; [CPU_] |268| 
	.dwpsn	file "../can.c",line 269,column 3,is_stmt
        B         $C$L33,UNC            ; [CPU_] |269| 
        ; branch occurs ; [] |269| 
$C$L31:    
	.dwpsn	file "../can.c",line 270,column 5,is_stmt
        MOVW      DP,#_ECanaRegs+4      ; [CPU_U] 
        MOVB      XAR6,#0               ; [CPU_] |270| 
        MOVL      ACC,@_ECanaRegs+4     ; [CPU_] |270| 
        AND       AL,*-SP[24]           ; [CPU_] |270| 
        AND       AH,*-SP[23]           ; [CPU_] |270| 
        CMPL      ACC,XAR6              ; [CPU_] |270| 
        BF        $C$L32,NEQ            ; [CPU_] |270| 
        ; branchcc occurs ; [] |270| 
	.dwpsn	file "../can.c",line 271,column 7,is_stmt
        MOVB      *-SP[22],#1,UNC       ; [CPU_] |271| 
	.dwpsn	file "../can.c",line 273,column 5,is_stmt
        B         $C$L33,UNC            ; [CPU_] |273| 
        ; branch occurs ; [] |273| 
$C$L32:    
	.dwpsn	file "../can.c",line 275,column 7,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |275| 
        LSL       ACC,1                 ; [CPU_] |275| 
        MOVL      *-SP[24],ACC          ; [CPU_] |275| 
	.dwpsn	file "../can.c",line 276,column 7,is_stmt
        INC       *-SP[21]              ; [CPU_] |276| 
        MOV       AL,*-SP[21]           ; [CPU_] |276| 
        CMPB      AL,#32                ; [CPU_] |276| 
        BF        $C$L33,NEQ            ; [CPU_] |276| 
        ; branchcc occurs ; [] |276| 
	.dwpsn	file "../can.c",line 277,column 11,is_stmt
        MOVB      *-SP[21],#16,UNC      ; [CPU_] |277| 
	.dwpsn	file "../can.c",line 277,column 31,is_stmt
        MOVL      XAR4,#65536           ; [CPU_U] |277| 
        MOVL      *-SP[24],XAR4         ; [CPU_] |277| 
	.dwpsn	file "../can.c",line 278,column 11,is_stmt
        MOVB      AL,#1                 ; [CPU_] |278| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |278| 
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$67, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |278| 
        ; call occurs [#_SEM_pend] ; [] |278| 
$C$L33:    
	.dwpsn	file "../can.c",line 269,column 10,is_stmt
        MOV       AL,*-SP[22]           ; [CPU_] |269| 
        BF        $C$L31,EQ             ; [CPU_] |269| 
        ; branchcc occurs ; [] |269| 
	.dwpsn	file "../can.c",line 283,column 3,is_stmt
        MOV       AL,*-SP[21]           ; [CPU_] |283| 
        CMPB      AL,#32                ; [CPU_] |283| 
        B         $C$L37,GEQ            ; [CPU_] |283| 
        ; branchcc occurs ; [] |283| 
	.dwpsn	file "../can.c",line 285,column 4,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ECanaMboxes    ; [CPU_U] |285| 
        MOV       ACC,*-SP[21] << 3     ; [CPU_] |285| 
        ADDL      XAR4,ACC              ; [CPU_] |285| 
        MOVL      *-SP[26],XAR4         ; [CPU_] |285| 
	.dwpsn	file "../can.c",line 288,column 4,is_stmt
        MOVW      DP,#_ECanaRegs+8      ; [CPU_U] 
        MOVB      XAR6,#0               ; [CPU_] |288| 
        MOVL      ACC,@_ECanaRegs+8     ; [CPU_] |288| 
        AND       AL,*-SP[24]           ; [CPU_] |288| 
        AND       AH,*-SP[23]           ; [CPU_] |288| 
        CMPL      ACC,XAR6              ; [CPU_] |288| 
        BF        $C$L35,EQ             ; [CPU_] |288| 
        ; branchcc occurs ; [] |288| 
	.dwpsn	file "../can.c",line 289,column 5,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |289| 
        MOVL      @_ECanaRegs+8,ACC     ; [CPU_] |289| 
	.dwpsn	file "../can.c",line 290,column 5,is_stmt
$C$L34:    
	.dwpsn	file "../can.c",line 290,column 45,is_stmt
        MOVL      ACC,@_ECanaRegs+8     ; [CPU_] |290| 
        AND       AL,*-SP[24]           ; [CPU_] |290| 
        AND       AH,*-SP[23]           ; [CPU_] |290| 
        CMPL      ACC,XAR6              ; [CPU_] |290| 
        BF        $C$L34,NEQ            ; [CPU_] |290| 
        ; branchcc occurs ; [] |290| 
$C$L35:    
	.dwpsn	file "../can.c",line 294,column 4,is_stmt
        MOVL      ACC,@_ECanaRegs       ; [CPU_] |294| 
        MOVL      *-SP[14],ACC          ; [CPU_] |294| 
	.dwpsn	file "../can.c",line 295,column 4,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |295| 
        NOT       ACC                   ; [CPU_] |295| 
        AND       *-SP[14],AL           ; [CPU_] |295| 
        AND       *-SP[13],AH           ; [CPU_] |295| 
	.dwpsn	file "../can.c",line 296,column 4,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |296| 
        MOVL      @_ECanaRegs,ACC       ; [CPU_] |296| 
	.dwpsn	file "../can.c",line 300,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |300| 
        MOV       T,#18                 ; [CPU_] |300| 
        MOVU      ACC,*-SP[11]          ; [CPU_] |300| 
        LSLL      ACC,T                 ; [CPU_] |300| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |300| 
	.dwpsn	file "../can.c",line 301,column 4,is_stmt
	RPT #3 || NOP
	.dwpsn	file "../can.c",line 302,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |302| 
        MOVU      ACC,*-SP[11]          ; [CPU_] |302| 
        LSLL      ACC,T                 ; [CPU_] |302| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |302| 
	.dwpsn	file "../can.c",line 308,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |308| 
        MOV       ACC,*-SP[10] << #4    ; [CPU_] |308| 
        ADD       AL,*-SP[9]            ; [CPU_] |308| 
        MOVU      ACC,AL                ; [CPU_] |308| 
        MOVL      *+XAR4[2],ACC         ; [CPU_] |308| 
	.dwpsn	file "../can.c",line 309,column 4,is_stmt
	RPT #3 || NOP
	.dwpsn	file "../can.c",line 310,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |310| 
        MOV       ACC,*-SP[10] << #4    ; [CPU_] |310| 
        ADD       AL,*-SP[9]            ; [CPU_] |310| 
        MOVU      ACC,AL                ; [CPU_] |310| 
        MOVL      *+XAR4[2],ACC         ; [CPU_] |310| 
	.dwpsn	file "../can.c",line 314,column 4,is_stmt
        MOVL      ACC,@_ECanaRegs       ; [CPU_] |314| 
        MOVL      *-SP[14],ACC          ; [CPU_] |314| 
	.dwpsn	file "../can.c",line 315,column 4,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |315| 
        OR        *-SP[14],AL           ; [CPU_] |315| 
        OR        *-SP[13],AH           ; [CPU_] |315| 
	.dwpsn	file "../can.c",line 316,column 4,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |316| 
        MOVL      @_ECanaRegs,ACC       ; [CPU_] |316| 
	.dwpsn	file "../can.c",line 318,column 4,is_stmt
        MOV       AL,*-SP[17]           ; [CPU_] |318| 
        MOV       AH,*-SP[8]            ; [CPU_] |318| 
        MOVB      AL.MSB,AH             ; [CPU_] |318| 
        MOV       *-SP[17],AL           ; [CPU_] |318| 
	.dwpsn	file "../can.c",line 319,column 4,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |319| 
        MOV       AH,*-SP[17]           ; [CPU_] |319| 
        MOVB      AH,AL.LSB             ; [CPU_] |319| 
        MOV       *-SP[17],AH           ; [CPU_] |319| 
	.dwpsn	file "../can.c",line 320,column 4,is_stmt
        MOV       AH,*-SP[6]            ; [CPU_] |320| 
        MOV       AL,*-SP[18]           ; [CPU_] |320| 
        MOVB      AL.MSB,AH             ; [CPU_] |320| 
        MOV       *-SP[18],AL           ; [CPU_] |320| 
	.dwpsn	file "../can.c",line 321,column 4,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |321| 
        MOV       AH,*-SP[18]           ; [CPU_] |321| 
        MOVB      AH,AL.LSB             ; [CPU_] |321| 
        MOV       *-SP[18],AH           ; [CPU_] |321| 
	.dwpsn	file "../can.c",line 323,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |323| 
        MOVL      ACC,*-SP[18]          ; [CPU_] |323| 
        MOVL      *+XAR4[4],ACC         ; [CPU_] |323| 
	.dwpsn	file "../can.c",line 324,column 4,is_stmt
	RPT #3 || NOP
	.dwpsn	file "../can.c",line 325,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |325| 
        MOVL      ACC,*-SP[18]          ; [CPU_] |325| 
        MOVL      *+XAR4[4],ACC         ; [CPU_] |325| 
	.dwpsn	file "../can.c",line 328,column 4,is_stmt
        MOV       AL,*-SP[19]           ; [CPU_] |328| 
        MOV       AH,*-SP[4]            ; [CPU_] |328| 
        MOVB      AL.MSB,AH             ; [CPU_] |328| 
        MOV       *-SP[19],AL           ; [CPU_] |328| 
	.dwpsn	file "../can.c",line 329,column 4,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |329| 
        MOV       AH,*-SP[19]           ; [CPU_] |329| 
        MOVB      AH,AL.LSB             ; [CPU_] |329| 
        MOV       *-SP[19],AH           ; [CPU_] |329| 
	.dwpsn	file "../can.c",line 330,column 4,is_stmt
        MOV       AH,*-SP[2]            ; [CPU_] |330| 
        MOV       AL,*-SP[20]           ; [CPU_] |330| 
        MOVB      AL.MSB,AH             ; [CPU_] |330| 
        MOV       *-SP[20],AL           ; [CPU_] |330| 
	.dwpsn	file "../can.c",line 331,column 4,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |331| 
        MOV       AH,*-SP[20]           ; [CPU_] |331| 
        MOVB      AH,AL.LSB             ; [CPU_] |331| 
        MOV       *-SP[20],AH           ; [CPU_] |331| 
	.dwpsn	file "../can.c",line 333,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |333| 
        MOVL      ACC,*-SP[20]          ; [CPU_] |333| 
        MOVL      *+XAR4[6],ACC         ; [CPU_] |333| 
	.dwpsn	file "../can.c",line 334,column 4,is_stmt
	RPT #3 || NOP
	.dwpsn	file "../can.c",line 335,column 4,is_stmt
        MOVL      XAR4,*-SP[26]         ; [CPU_] |335| 
        MOVL      ACC,*-SP[20]          ; [CPU_] |335| 
        MOVL      *+XAR4[6],ACC         ; [CPU_] |335| 
	.dwpsn	file "../can.c",line 339,column 4,is_stmt
        MOVL      ACC,@_ECanaRegs+48    ; [CPU_] |339| 
        MOVL      *-SP[16],ACC          ; [CPU_] |339| 
	.dwpsn	file "../can.c",line 340,column 4,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |340| 
        NOT       ACC                   ; [CPU_] |340| 
        AND       *-SP[16],AL           ; [CPU_] |340| 
        AND       *-SP[15],AH           ; [CPU_] |340| 
	.dwpsn	file "../can.c",line 341,column 4,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |341| 
        MOVL      @_ECanaRegs+48,ACC    ; [CPU_] |341| 
	.dwpsn	file "../can.c",line 342,column 4,is_stmt
$C$L36:    
        MOVL      ACC,@_ECanaRegs+48    ; [CPU_] |342| 
        AND       AL,*-SP[24]           ; [CPU_] |342| 
        AND       AH,*-SP[23]           ; [CPU_] |342| 
        CMPL      ACC,XAR6              ; [CPU_] |342| 
        BF        $C$L36,NEQ            ; [CPU_] |342| 
        ; branchcc occurs ; [] |342| 
	.dwpsn	file "../can.c",line 344,column 4,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ECanaMOTORegs  ; [CPU_U] |344| 
        MOV       ACC,*-SP[21] << 1     ; [CPU_] |344| 
        ADDL      XAR4,ACC              ; [CPU_] |344| 
        MOVL      *-SP[28],XAR4         ; [CPU_] |344| 
	.dwpsn	file "../can.c",line 346,column 4,is_stmt
        MOVL      XAR5,*-SP[28]         ; [CPU_] |346| 
        MOVL      XAR4,#1000            ; [CPU_U] |346| 
        MOVL      ACC,XAR4              ; [CPU_] |346| 
        ADDL      ACC,@_ECanaRegs+46    ; [CPU_] |346| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |346| 
	.dwpsn	file "../can.c",line 347,column 4,is_stmt
	RPT #3 || NOP
	.dwpsn	file "../can.c",line 348,column 4,is_stmt
        MOVL      XAR5,*-SP[28]         ; [CPU_] |348| 
        MOVL      ACC,XAR4              ; [CPU_] |348| 
        ADDL      ACC,@_ECanaRegs+46    ; [CPU_] |348| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |348| 
	.dwpsn	file "../can.c",line 352,column 4,is_stmt
        MOVL      ACC,@_ECanaRegs+48    ; [CPU_] |352| 
        MOVL      *-SP[16],ACC          ; [CPU_] |352| 
	.dwpsn	file "../can.c",line 353,column 4,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |353| 
        OR        *-SP[16],AL           ; [CPU_] |353| 
        OR        *-SP[15],AH           ; [CPU_] |353| 
	.dwpsn	file "../can.c",line 354,column 4,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |354| 
        MOVL      @_ECanaRegs+48,ACC    ; [CPU_] |354| 
	.dwpsn	file "../can.c",line 357,column 4,is_stmt
        MOVL      ACC,*-SP[24]          ; [CPU_] |357| 
        MOVL      @_ECanaRegs+4,ACC     ; [CPU_] |357| 
	.dwpsn	file "../can.c",line 359,column 4,is_stmt
        MOVW      DP,#_dummy            ; [CPU_U] 
        MOVB      @_dummy,#1,UNC        ; [CPU_] |359| 
	.dwpsn	file "../can.c",line 360,column 3,is_stmt
        B         $C$L30,UNC            ; [CPU_] |360| 
        ; branch occurs ; [] |360| 
$C$L37:    
	.dwpsn	file "../can.c",line 364,column 12,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |364| 
	.dwpsn	file "../can.c",line 264,column 9,is_stmt
        B         $C$L30,UNC            ; [CPU_] |264| 
        ; branch occurs ; [] |264| 
	.dwattr $C$DW$55, DW_AT_TI_end_file("../can.c")
	.dwattr $C$DW$55, DW_AT_TI_end_line(0x16f)
	.dwattr $C$DW$55, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$55

	.sect	".text:retain"
	.retain
	.retainrefs
	.global	_manage_can_bus

$C$DW$68	.dwtag  DW_TAG_subprogram, DW_AT_name("manage_can_bus")
	.dwattr $C$DW$68, DW_AT_low_pc(_manage_can_bus)
	.dwattr $C$DW$68, DW_AT_high_pc(0x00)
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_manage_can_bus")
	.dwattr $C$DW$68, DW_AT_external
	.dwattr $C$DW$68, DW_AT_TI_begin_file("../can.c")
	.dwattr $C$DW$68, DW_AT_TI_begin_line(0x174)
	.dwattr $C$DW$68, DW_AT_TI_begin_column(0x10)
	.dwattr $C$DW$68, DW_AT_TI_interrupt
	.dwattr $C$DW$68, DW_AT_TI_max_frame_size(-28)
	.dwpsn	file "../can.c",line 373,column 1,is_stmt,address _manage_can_bus

	.dwfde $C$DW$CIE, _manage_can_bus
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("i_blink")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_i_blink$1")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_addr _i_blink$1]

;***************************************************************
;* FNAME: _manage_can_bus               FR SIZE:  26           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto, 24 SOE     *
;***************************************************************

_manage_can_bus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ASP       ; [CPU_] 
        PUSH      RB                    ; [CPU_] 
	.dwcfi	save_reg_to_mem, 73, 2
	.dwcfi	save_reg_to_mem, 74, 3
	.dwcfi	cfa_offset, -4
        PUSH      AR1H:AR0H             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 5, 4
	.dwcfi	save_reg_to_mem, 7, 5
	.dwcfi	cfa_offset, -6
        MOVL      *SP++,XT              ; [CPU_] 
	.dwcfi	save_reg_to_mem, 21, 6
	.dwcfi	save_reg_to_mem, 22, 7
	.dwcfi	cfa_offset, -8
        MOVL      *SP++,XAR4            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 12, 8
	.dwcfi	save_reg_to_mem, 13, 9
	.dwcfi	cfa_offset, -10
        MOVL      *SP++,XAR5            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 14, 10
	.dwcfi	save_reg_to_mem, 15, 11
	.dwcfi	cfa_offset, -12
        MOVL      *SP++,XAR6            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 16, 12
	.dwcfi	save_reg_to_mem, 17, 13
	.dwcfi	cfa_offset, -14
        MOVL      *SP++,XAR7            ; [CPU_] 
	.dwcfi	save_reg_to_mem, 18, 14
	.dwcfi	save_reg_to_mem, 19, 15
	.dwcfi	cfa_offset, -16
        MOV32     *SP++,STF             ; [CPU_] 
	.dwcfi	save_reg_to_mem, 39, 16
	.dwcfi	save_reg_to_mem, 40, 17
	.dwcfi	cfa_offset, -18
        MOV32     *SP++,R0H             ; [CPU_] 
        MOV32     *SP++,R1H             ; [CPU_] 
        MOV32     *SP++,R2H             ; [CPU_] 
        MOV32     *SP++,R3H             ; [CPU_] 
        SETFLG    RNDF32=1, RNDF64=1    ; [CPU_] 
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -28
        SPM       0                     ; [CPU_] 
        CLRC      PAGE0,OVM             ; [CPU_] 
        CLRC      AMODE                 ; [CPU_] 
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ECanaShadow_CANES")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ECanaShadow_CANES")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "../can.c",line 377,column 2,is_stmt
        MOVW      DP,#_ECanaRegs+24     ; [CPU_U] 
        MOVL      ACC,@_ECanaRegs+24    ; [CPU_] |377| 
        MOVL      *-SP[2],ACC           ; [CPU_] |377| 
	.dwpsn	file "../can.c",line 378,column 2,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |378| 
        MOVL      @_ECanaRegs+24,ACC    ; [CPU_] |378| 
	.dwpsn	file "../can.c",line 381,column 2,is_stmt
        TBIT      *-SP[1],#2            ; [CPU_] |381| 
        BF        $C$L44,TC             ; [CPU_] |381| 
        ; branchcc occurs ; [] |381| 
        TBIT      *-SP[1],#1            ; [CPU_] |381| 
        BF        $C$L44,TC             ; [CPU_] |381| 
        ; branchcc occurs ; [] |381| 
	.dwpsn	file "../can.c",line 382,column 3,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |382| 
        ANDB      AL,#0x01              ; [CPU_] |382| 
        CMPB      AL,#1                 ; [CPU_] |382| 
        BF        $C$L38,NEQ            ; [CPU_] |382| 
        ; branchcc occurs ; [] |382| 
	.dwpsn	file "../can.c",line 383,column 4,is_stmt
        MOVL      XAR4,#_can_bus_state  ; [CPU_U] |383| 
        MOVB      AL,#2                 ; [CPU_] |383| 
$C$DW$71	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$71, DW_AT_low_pc(0x00)
	.dwattr $C$DW$71, DW_AT_name("_ATM_seti")
	.dwattr $C$DW$71, DW_AT_TI_call
        LCR       #_ATM_seti            ; [CPU_] |383| 
        ; call occurs [#_ATM_seti] ; [] |383| 
        CMPB      AL,#2                 ; [CPU_] |383| 
        BF        $C$L44,EQ             ; [CPU_] |383| 
        ; branchcc occurs ; [] |383| 
	.dwpsn	file "../can.c",line 386,column 3,is_stmt
        B         $C$L44,UNC            ; [CPU_] |386| 
        ; branch occurs ; [] |386| 
$C$L38:    
	.dwpsn	file "../can.c",line 387,column 4,is_stmt
        MOVL      XAR4,#_can_bus_state  ; [CPU_U] |387| 
        MOVB      AL,#1                 ; [CPU_] |387| 
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_name("_ATM_seti")
	.dwattr $C$DW$72, DW_AT_TI_call
        LCR       #_ATM_seti            ; [CPU_] |387| 
        ; call occurs [#_ATM_seti] ; [] |387| 
        CMPB      AL,#1                 ; [CPU_] |387| 
        BF        $C$L44,EQ             ; [CPU_] |387| 
        ; branchcc occurs ; [] |387| 
	.dwpsn	file "../can.c",line 393,column 2,is_stmt
        B         $C$L44,UNC            ; [CPU_] |393| 
        ; branch occurs ; [] |393| 
$C$L39:    
	.dwpsn	file "../can.c",line 395,column 4,is_stmt
        MOV       @_dummy,#0            ; [CPU_] |395| 
	.dwpsn	file "../can.c",line 396,column 4,is_stmt
        B         $C$L45,UNC            ; [CPU_] |396| 
        ; branch occurs ; [] |396| 
$C$L40:    
	.dwpsn	file "../can.c",line 398,column 4,is_stmt
        INC       @_i_blink$1           ; [CPU_] |398| 
        MOV       AL,@_i_blink$1        ; [CPU_] |398| 
        CMPB      AL,#50                ; [CPU_] |398| 
        B         $C$L45,LO             ; [CPU_] |398| 
        ; branchcc occurs ; [] |398| 
	.dwpsn	file "../can.c",line 399,column 5,is_stmt
        MOV       AH,@_dummy            ; [CPU_] |399| 
        MOVB      AL,#0                 ; [CPU_] |399| 
        MOVB      XAR6,#0               ; [CPU_] |399| 
        CMPB      AH,#0                 ; [CPU_] |399| 
        BF        $C$L41,EQ             ; [CPU_] |399| 
        ; branchcc occurs ; [] |399| 
        MOVB      AL,#1                 ; [CPU_] |399| 
$C$L41:    
        CMPB      AL,#0                 ; [CPU_] |399| 
        BF        $C$L42,NEQ            ; [CPU_] |399| 
        ; branchcc occurs ; [] |399| 
        MOVB      XAR6,#1               ; [CPU_] |399| 
$C$L42:    
        MOV       @_dummy,AR6           ; [CPU_] |399| 
	.dwpsn	file "../can.c",line 400,column 5,is_stmt
        MOV       @_i_blink$1,#0        ; [CPU_] |400| 
	.dwpsn	file "../can.c",line 402,column 4,is_stmt
        B         $C$L45,UNC            ; [CPU_] |402| 
        ; branch occurs ; [] |402| 
$C$L43:    
	.dwpsn	file "../can.c",line 405,column 4,is_stmt
        MOVB      @_dummy,#1,UNC        ; [CPU_] |405| 
	.dwpsn	file "../can.c",line 406,column 4,is_stmt
        B         $C$L45,UNC            ; [CPU_] |406| 
        ; branch occurs ; [] |406| 
$C$L44:    
	.dwpsn	file "../can.c",line 393,column 2,is_stmt
        MOVW      DP,#_can_bus_state    ; [CPU_U] 
        MOV       AL,@_can_bus_state    ; [CPU_] |393| 
        CMPB      AL,#1                 ; [CPU_] |393| 
        BF        $C$L39,EQ             ; [CPU_] |393| 
        ; branchcc occurs ; [] |393| 
        CMPB      AL,#2                 ; [CPU_] |393| 
        BF        $C$L40,EQ             ; [CPU_] |393| 
        ; branchcc occurs ; [] |393| 
        CMPB      AL,#3                 ; [CPU_] |393| 
        BF        $C$L43,EQ             ; [CPU_] |393| 
        ; branchcc occurs ; [] |393| 
        CMPB      AL,#4                 ; [CPU_] |393| 
        BF        $C$L43,EQ             ; [CPU_] |393| 
        ; branchcc occurs ; [] |393| 
$C$L45:    
	.dwpsn	file "../can.c",line 408,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+1    ; [CPU_U] 
        OR        @_PieCtrlRegs+1,#0x0100 ; [CPU_] |408| 
	.dwpsn	file "../can.c",line 409,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -26
        MOV32     R3H,*--SP             ; [CPU_] 
        MOV32     R2H,*--SP             ; [CPU_] 
        MOV32     R1H,*--SP             ; [CPU_] 
        MOV32     R0H,*--SP             ; [CPU_] 
        MOV32     STF,*--SP             ; [CPU_] 
	.dwcfi	cfa_offset, -16
	.dwcfi	restore_reg, 39
	.dwcfi	restore_reg, 40
        MOVL      XAR7,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -14
	.dwcfi	restore_reg, 18
        MOVL      XAR6,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -12
	.dwcfi	restore_reg, 16
        MOVL      XAR5,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -10
	.dwcfi	restore_reg, 14
        MOVL      XAR4,*--SP            ; [CPU_] 
	.dwcfi	cfa_offset, -8
	.dwcfi	restore_reg, 12
        MOVL      XT,*--SP              ; [CPU_] 
	.dwcfi	cfa_offset, -6
	.dwcfi	restore_reg, 22
	.dwcfi	restore_reg, 21
        POP       AR1H:AR0H             ; [CPU_] 
	.dwcfi	cfa_offset, -4
	.dwcfi	restore_reg, 5
	.dwcfi	restore_reg, 7
        POP       RB                    ; [CPU_] 
	.dwcfi	cfa_offset, -2
	.dwcfi	restore_reg, 73
	.dwcfi	restore_reg, 74
        NASP      ; [CPU_] 
$C$DW$73	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$73, DW_AT_low_pc(0x00)
	.dwattr $C$DW$73, DW_AT_TI_return
        IRET      ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$68, DW_AT_TI_end_file("../can.c")
	.dwattr $C$DW$68, DW_AT_TI_end_line(0x199)
	.dwattr $C$DW$68, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$68

	.sect	".text"
	.clink
	.global	_TaskCanWaitMessage

$C$DW$74	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskCanWaitMessage")
	.dwattr $C$DW$74, DW_AT_low_pc(_TaskCanWaitMessage)
	.dwattr $C$DW$74, DW_AT_high_pc(0x00)
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_TaskCanWaitMessage")
	.dwattr $C$DW$74, DW_AT_external
	.dwattr $C$DW$74, DW_AT_TI_begin_file("../can.c")
	.dwattr $C$DW$74, DW_AT_TI_begin_line(0x19b)
	.dwattr $C$DW$74, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$74, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../can.c",line 411,column 30,is_stmt,address _TaskCanWaitMessage

	.dwfde $C$DW$CIE, _TaskCanWaitMessage

;***************************************************************
;* FNAME: _TaskCanWaitMessage           FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 12 Auto,  0 SOE     *
;***************************************************************

_TaskCanWaitMessage:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("m")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_m")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -11]
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "../can.c",line 415,column 3,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |415| 
	.dwpsn	file "../can.c",line 416,column 3,is_stmt
        MOVB      *-SP[9],#8,UNC        ; [CPU_] |416| 
	.dwpsn	file "../can.c",line 417,column 3,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |417| 
	.dwpsn	file "../can.c",line 418,column 8,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |418| 
	.dwpsn	file "../can.c",line 418,column 12,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |418| 
        CMPB      AL,#8                 ; [CPU_] |418| 
        B         $C$L47,GEQ            ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
$C$L46:    
	.dwpsn	file "../can.c",line 419,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOV       ACC,*-SP[12]          ; [CPU_] |419| 
        MOVZ      AR6,*-SP[12]          ; [CPU_] |419| 
        MOVZ      AR4,SP                ; [CPU_U] |419| 
        SUBB      XAR4,#8               ; [CPU_U] |419| 
        ADDL      XAR4,ACC              ; [CPU_] |419| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |419| 
	.dwpsn	file "../can.c",line 418,column 17,is_stmt
        INC       *-SP[12]              ; [CPU_] |418| 
	.dwpsn	file "../can.c",line 418,column 12,is_stmt
        MOV       AL,*-SP[12]           ; [CPU_] |418| 
        CMPB      AL,#8                 ; [CPU_] |418| 
        B         $C$L46,LT             ; [CPU_] |418| 
        ; branchcc occurs ; [] |418| 
$C$L47:    
	.dwpsn	file "../can.c",line 422,column 10,is_stmt
$C$L48:    
	.dwpsn	file "../can.c",line 423,column 5,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |423| 
        MOVL      XAR4,#_can_rx_mbox    ; [CPU_U] |423| 
        MOVB      AL,#0                 ; [CPU_] |423| 
        SUBB      XAR5,#11              ; [CPU_U] |423| 
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$77, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |423| 
        ; call occurs [#_MBX_pend] ; [] |423| 
        CMPB      AL,#0                 ; [CPU_] |423| 
        BF        $C$L49,EQ             ; [CPU_] |423| 
        ; branchcc occurs ; [] |423| 
	.dwpsn	file "../can.c",line 424,column 7,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVZ      AR5,SP                ; [CPU_U] |424| 
        MOVB      AL,#0                 ; [CPU_] |424| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |424| 
        SUBB      XAR5,#11              ; [CPU_U] |424| 
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("_canDispatch")
	.dwattr $C$DW$78, DW_AT_TI_call
        LCR       #_canDispatch         ; [CPU_] |424| 
        ; call occurs [#_canDispatch] ; [] |424| 
$C$L49:    
	.dwpsn	file "../can.c",line 426,column 5,is_stmt
        MOVW      DP,#_can_rx_mbox+16   ; [CPU_U] 
        MOV       AL,@_can_rx_mbox+16   ; [CPU_] |426| 
        BF        $C$L48,NEQ            ; [CPU_] |426| 
        ; branchcc occurs ; [] |426| 
	.dwpsn	file "../can.c",line 426,column 41,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |426| 
        MOVB      AL,#1                 ; [CPU_] |426| 
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$79, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |426| 
        ; call occurs [#_SEM_pend] ; [] |426| 
	.dwpsn	file "../can.c",line 422,column 10,is_stmt
        B         $C$L48,UNC            ; [CPU_] |422| 
        ; branch occurs ; [] |422| 
	.dwattr $C$DW$74, DW_AT_TI_end_file("../can.c")
	.dwattr $C$DW$74, DW_AT_TI_end_line(0x1ac)
	.dwattr $C$DW$74, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$74

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_canDispatch
	.global	_ATM_seti
	.global	_MBX_post
	.global	_SEM_pend
	.global	_MBX_pend
	.global	_BoardODdata
	.global	_TSK_timerSem
	.global	_PieCtrlRegs
	.global	_can_tx_mbox
	.global	_can_rx_mbox
	.global	_ECanaRegs
	.global	_ECanaLAMRegs
	.global	_ECanaMOTORegs
	.global	_ECanaMboxes

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$80	.dwtag  DW_TAG_member
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$80, DW_AT_name("cob_id")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$80, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$80, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$81	.dwtag  DW_TAG_member
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$81, DW_AT_name("rtr")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$81, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$81, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$82	.dwtag  DW_TAG_member
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$82, DW_AT_name("len")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$82, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$82, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$83, DW_AT_name("data")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$185	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$185, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$185, DW_AT_language(DW_LANG_C)
$C$DW$T$194	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$194, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$T$194, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$84, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$85, DW_AT_name("csSDO")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$86	.dwtag  DW_TAG_member
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$86, DW_AT_name("csEmergency")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$87	.dwtag  DW_TAG_member
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$87, DW_AT_name("csSYNC")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$88, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$88, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$89, DW_AT_name("csPDO")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$90, DW_AT_name("csLSS")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$142	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$142, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$91, DW_AT_name("errCode")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_name("errRegMask")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$93, DW_AT_name("active")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$176	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$176, DW_AT_language(DW_LANG_C)

$C$DW$T$177	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$T$177, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$177, DW_AT_byte_size(0x18)
$C$DW$94	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$94, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$177


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$95, DW_AT_name("index")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$96, DW_AT_name("subindex")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$96, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$97	.dwtag  DW_TAG_member
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$97, DW_AT_name("size")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$97, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$98, DW_AT_name("address")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$98, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$182	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$182, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$182, DW_AT_language(DW_LANG_C)
$C$DW$T$183	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$183, DW_AT_type(*$C$DW$T$182)
	.dwattr $C$DW$T$183, DW_AT_address_class(0x16)

$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_name("CANAA_BITS")
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x02)
$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$99, DW_AT_name("AA0")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_AA0")
	.dwattr $C$DW$99, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$100, DW_AT_name("AA1")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_AA1")
	.dwattr $C$DW$100, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$101, DW_AT_name("AA2")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_AA2")
	.dwattr $C$DW$101, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$102, DW_AT_name("AA3")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_AA3")
	.dwattr $C$DW$102, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$103, DW_AT_name("AA4")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_AA4")
	.dwattr $C$DW$103, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$104, DW_AT_name("AA5")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_AA5")
	.dwattr $C$DW$104, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$105	.dwtag  DW_TAG_member
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$105, DW_AT_name("AA6")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_AA6")
	.dwattr $C$DW$105, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$105, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_member
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$106, DW_AT_name("AA7")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_AA7")
	.dwattr $C$DW$106, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$106, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$106, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$107, DW_AT_name("AA8")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_AA8")
	.dwattr $C$DW$107, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$107, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$108, DW_AT_name("AA9")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_AA9")
	.dwattr $C$DW$108, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$109, DW_AT_name("AA10")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_AA10")
	.dwattr $C$DW$109, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$110, DW_AT_name("AA11")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_AA11")
	.dwattr $C$DW$110, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$111, DW_AT_name("AA12")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_AA12")
	.dwattr $C$DW$111, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$112, DW_AT_name("AA13")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_AA13")
	.dwattr $C$DW$112, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$113, DW_AT_name("AA14")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_AA14")
	.dwattr $C$DW$113, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$114, DW_AT_name("AA15")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_AA15")
	.dwattr $C$DW$114, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$115, DW_AT_name("AA16")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_AA16")
	.dwattr $C$DW$115, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$116, DW_AT_name("AA17")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_AA17")
	.dwattr $C$DW$116, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$117, DW_AT_name("AA18")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_AA18")
	.dwattr $C$DW$117, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$118, DW_AT_name("AA19")
	.dwattr $C$DW$118, DW_AT_TI_symbol_name("_AA19")
	.dwattr $C$DW$118, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$119, DW_AT_name("AA20")
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_AA20")
	.dwattr $C$DW$119, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$120, DW_AT_name("AA21")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_AA21")
	.dwattr $C$DW$120, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$121, DW_AT_name("AA22")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_AA22")
	.dwattr $C$DW$121, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$122, DW_AT_name("AA23")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_AA23")
	.dwattr $C$DW$122, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$123, DW_AT_name("AA24")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_AA24")
	.dwattr $C$DW$123, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$124, DW_AT_name("AA25")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_AA25")
	.dwattr $C$DW$124, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$125, DW_AT_name("AA26")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_AA26")
	.dwattr $C$DW$125, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$126, DW_AT_name("AA27")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_AA27")
	.dwattr $C$DW$126, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$127, DW_AT_name("AA28")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_AA28")
	.dwattr $C$DW$127, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$128, DW_AT_name("AA29")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_AA29")
	.dwattr $C$DW$128, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$129, DW_AT_name("AA30")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_AA30")
	.dwattr $C$DW$129, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$130, DW_AT_name("AA31")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_AA31")
	.dwattr $C$DW$130, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$27	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$27, DW_AT_name("CANAA_REG")
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x02)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$131, DW_AT_name("all")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$132, DW_AT_name("bit")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_name("CANBTC_BITS")
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x02)
$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$133, DW_AT_name("TSEG2REG")
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_TSEG2REG")
	.dwattr $C$DW$133, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$134, DW_AT_name("TSEG1REG")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_TSEG1REG")
	.dwattr $C$DW$134, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x04)
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$135, DW_AT_name("SAM")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_SAM")
	.dwattr $C$DW$135, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$136, DW_AT_name("SJWREG")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_SJWREG")
	.dwattr $C$DW$136, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x02)
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$137, DW_AT_name("rsvd1")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$137, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x06)
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$138, DW_AT_name("BRPREG")
	.dwattr $C$DW$138, DW_AT_TI_symbol_name("_BRPREG")
	.dwattr $C$DW$138, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$139, DW_AT_name("rsvd2")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$139, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28


$C$DW$T$29	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$29, DW_AT_name("CANBTC_REG")
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x02)
$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$140, DW_AT_name("all")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$141, DW_AT_name("bit")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29


$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("CANES_BITS")
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x02)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$142, DW_AT_name("TM")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_TM")
	.dwattr $C$DW$142, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$143, DW_AT_name("RM")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_RM")
	.dwattr $C$DW$143, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$144, DW_AT_name("rsvd1")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$144, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$145, DW_AT_name("PDA")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_PDA")
	.dwattr $C$DW$145, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$146, DW_AT_name("CCE")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_CCE")
	.dwattr $C$DW$146, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$147, DW_AT_name("SMA")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_SMA")
	.dwattr $C$DW$147, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$148, DW_AT_name("rsvd2")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$148, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0a)
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$149, DW_AT_name("EW")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_EW")
	.dwattr $C$DW$149, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$150, DW_AT_name("EP")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_EP")
	.dwattr $C$DW$150, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$151, DW_AT_name("BO")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_BO")
	.dwattr $C$DW$151, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$152, DW_AT_name("ACKE")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_ACKE")
	.dwattr $C$DW$152, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$153, DW_AT_name("SE")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_SE")
	.dwattr $C$DW$153, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$154, DW_AT_name("CRCE")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_CRCE")
	.dwattr $C$DW$154, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$155, DW_AT_name("SA1")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_SA1")
	.dwattr $C$DW$155, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$156, DW_AT_name("BE")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_BE")
	.dwattr $C$DW$156, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$157, DW_AT_name("FE")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_FE")
	.dwattr $C$DW$157, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$158, DW_AT_name("rsvd3")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$158, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x07)
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$30


$C$DW$T$31	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$31, DW_AT_name("CANES_REG")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x02)
$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$159, DW_AT_name("all")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$160, DW_AT_name("bit")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31


$C$DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$32, DW_AT_name("CANGAM_BITS")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x02)
$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$161, DW_AT_name("GAM150")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_GAM150")
	.dwattr $C$DW$161, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$162, DW_AT_name("GAM2816")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_GAM2816")
	.dwattr $C$DW$162, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x0d)
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$163, DW_AT_name("rsvd")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_rsvd")
	.dwattr $C$DW$163, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x02)
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$164, DW_AT_name("AMI")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_AMI")
	.dwattr $C$DW$164, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$32


$C$DW$T$33	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$33, DW_AT_name("CANGAM_REG")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$165, DW_AT_name("all")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$166, DW_AT_name("bit")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_name("CANGIF0_BITS")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$167, DW_AT_name("MIV0")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_MIV0")
	.dwattr $C$DW$167, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$168, DW_AT_name("rsvd1")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$168, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x03)
	.dwattr $C$DW$168, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$169, DW_AT_name("WLIF0")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_WLIF0")
	.dwattr $C$DW$169, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$170, DW_AT_name("EPIF0")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_EPIF0")
	.dwattr $C$DW$170, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$171, DW_AT_name("BOIF0")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_BOIF0")
	.dwattr $C$DW$171, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$172, DW_AT_name("RMLIF0")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_RMLIF0")
	.dwattr $C$DW$172, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$173, DW_AT_name("WUIF0")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_WUIF0")
	.dwattr $C$DW$173, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$174, DW_AT_name("WDIF0")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_WDIF0")
	.dwattr $C$DW$174, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$175, DW_AT_name("AAIF0")
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_AAIF0")
	.dwattr $C$DW$175, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$176, DW_AT_name("GMIF0")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_GMIF0")
	.dwattr $C$DW$176, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$177, DW_AT_name("TCOF0")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_TCOF0")
	.dwattr $C$DW$177, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$178, DW_AT_name("MTOF0")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_MTOF0")
	.dwattr $C$DW$178, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$179, DW_AT_name("rsvd2")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$179, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0e)
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$35, DW_AT_name("CANGIF0_REG")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$180, DW_AT_name("all")
	.dwattr $C$DW$180, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$181, DW_AT_name("bit")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$36, DW_AT_name("CANGIF1_BITS")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$182, DW_AT_name("MIV1")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_MIV1")
	.dwattr $C$DW$182, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$183, DW_AT_name("rsvd1")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$183, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x03)
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$184, DW_AT_name("WLIF1")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_WLIF1")
	.dwattr $C$DW$184, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$185, DW_AT_name("EPIF1")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_EPIF1")
	.dwattr $C$DW$185, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$186, DW_AT_name("BOIF1")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_BOIF1")
	.dwattr $C$DW$186, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$187, DW_AT_name("RMLIF1")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_RMLIF1")
	.dwattr $C$DW$187, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$188, DW_AT_name("WUIF1")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_WUIF1")
	.dwattr $C$DW$188, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$189, DW_AT_name("WDIF1")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_WDIF1")
	.dwattr $C$DW$189, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$190, DW_AT_name("AAIF1")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_AAIF1")
	.dwattr $C$DW$190, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$191, DW_AT_name("GMIF1")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_GMIF1")
	.dwattr $C$DW$191, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$192	.dwtag  DW_TAG_member
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$192, DW_AT_name("TCOF1")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_TCOF1")
	.dwattr $C$DW$192, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$192, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_member
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$193, DW_AT_name("MTOF1")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_MTOF1")
	.dwattr $C$DW$193, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$193, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$193, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$194, DW_AT_name("rsvd2")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$194, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0e)
	.dwattr $C$DW$194, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$37, DW_AT_name("CANGIF1_REG")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x02)
$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$195, DW_AT_name("all")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$196, DW_AT_name("bit")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37


$C$DW$T$38	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$38, DW_AT_name("CANGIM_BITS")
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x02)
$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$197, DW_AT_name("I0EN")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_I0EN")
	.dwattr $C$DW$197, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$198, DW_AT_name("I1EN")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_I1EN")
	.dwattr $C$DW$198, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$199, DW_AT_name("GIL")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_GIL")
	.dwattr $C$DW$199, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$200	.dwtag  DW_TAG_member
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$200, DW_AT_name("rsvd1")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$200, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x05)
	.dwattr $C$DW$200, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_member
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$201, DW_AT_name("WLIM")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_WLIM")
	.dwattr $C$DW$201, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$201, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$201, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$202	.dwtag  DW_TAG_member
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$202, DW_AT_name("EPIM")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_EPIM")
	.dwattr $C$DW$202, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$202, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_member
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$203, DW_AT_name("BOIM")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_BOIM")
	.dwattr $C$DW$203, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$203, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$204	.dwtag  DW_TAG_member
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$204, DW_AT_name("RMLIM")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_RMLIM")
	.dwattr $C$DW$204, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$204, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$205	.dwtag  DW_TAG_member
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$205, DW_AT_name("WUIM")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_WUIM")
	.dwattr $C$DW$205, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$205, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$205, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$206, DW_AT_name("WDIM")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_WDIM")
	.dwattr $C$DW$206, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$206, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$207, DW_AT_name("AAIM")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_AAIM")
	.dwattr $C$DW$207, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$208, DW_AT_name("rsvd2")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$208, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$209, DW_AT_name("TCOM")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_TCOM")
	.dwattr $C$DW$209, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$210, DW_AT_name("MTOM")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_MTOM")
	.dwattr $C$DW$210, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$211, DW_AT_name("rsvd3")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$211, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0e)
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$38


$C$DW$T$39	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$39, DW_AT_name("CANGIM_REG")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x02)
$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$212, DW_AT_name("all")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$213, DW_AT_name("bit")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39


$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("CANLAM_BITS")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x02)
$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$214, DW_AT_name("LAM_L")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_LAM_L")
	.dwattr $C$DW$214, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$215, DW_AT_name("LAM_H")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_LAM_H")
	.dwattr $C$DW$215, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x0d)
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$216, DW_AT_name("rsvd1")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$216, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x02)
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$217, DW_AT_name("LAMI")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_LAMI")
	.dwattr $C$DW$217, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40


$C$DW$T$41	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$41, DW_AT_name("CANLAM_REG")
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x02)
$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$218, DW_AT_name("all")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$219, DW_AT_name("bit")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$41

$C$DW$220	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$41)
$C$DW$T$201	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$201, DW_AT_type(*$C$DW$220)
$C$DW$T$202	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$202, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$T$202, DW_AT_address_class(0x16)

$C$DW$T$42	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$42, DW_AT_name("CANMC_BITS")
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x02)
$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$221, DW_AT_name("MBNR")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_MBNR")
	.dwattr $C$DW$221, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x05)
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$222, DW_AT_name("SRES")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_SRES")
	.dwattr $C$DW$222, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$223, DW_AT_name("STM")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_STM")
	.dwattr $C$DW$223, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$224	.dwtag  DW_TAG_member
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$224, DW_AT_name("ABO")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_ABO")
	.dwattr $C$DW$224, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$224, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$225	.dwtag  DW_TAG_member
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$225, DW_AT_name("CDR")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_CDR")
	.dwattr $C$DW$225, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$225, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$225, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$226	.dwtag  DW_TAG_member
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$226, DW_AT_name("WUBA")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_WUBA")
	.dwattr $C$DW$226, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$226, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_member
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$227, DW_AT_name("DBO")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_DBO")
	.dwattr $C$DW$227, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$227, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$227, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$228	.dwtag  DW_TAG_member
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$228, DW_AT_name("PDR")
	.dwattr $C$DW$228, DW_AT_TI_symbol_name("_PDR")
	.dwattr $C$DW$228, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$228, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$228, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$229	.dwtag  DW_TAG_member
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$229, DW_AT_name("CCR")
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_CCR")
	.dwattr $C$DW$229, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$229, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$229, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$230	.dwtag  DW_TAG_member
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$230, DW_AT_name("SCB")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_SCB")
	.dwattr $C$DW$230, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$230, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$231, DW_AT_name("TCC")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_TCC")
	.dwattr $C$DW$231, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$231, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$232	.dwtag  DW_TAG_member
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$232, DW_AT_name("MBCC")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_MBCC")
	.dwattr $C$DW$232, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$232, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_member
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$233, DW_AT_name("SUSP")
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_SUSP")
	.dwattr $C$DW$233, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$233, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$233, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$234	.dwtag  DW_TAG_member
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$234, DW_AT_name("rsvd")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_rsvd")
	.dwattr $C$DW$234, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$234, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$42


$C$DW$T$43	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$43, DW_AT_name("CANMC_REG")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x02)
$C$DW$235	.dwtag  DW_TAG_member
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$235, DW_AT_name("all")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$235, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$235, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$236	.dwtag  DW_TAG_member
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$236, DW_AT_name("bit")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$236, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$236, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$43


$C$DW$T$44	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$44, DW_AT_name("CANMDH_BYTES")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x02)
$C$DW$237	.dwtag  DW_TAG_member
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$237, DW_AT_name("BYTE7")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_BYTE7")
	.dwattr $C$DW$237, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$237, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$238, DW_AT_name("BYTE6")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_BYTE6")
	.dwattr $C$DW$238, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$238, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$239, DW_AT_name("BYTE5")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_BYTE5")
	.dwattr $C$DW$239, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$240, DW_AT_name("BYTE4")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_BYTE4")
	.dwattr $C$DW$240, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$44


$C$DW$T$45	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$45, DW_AT_name("CANMDH_REG")
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x02)
$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$241, DW_AT_name("all")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$242, DW_AT_name("word")
	.dwattr $C$DW$242, DW_AT_TI_symbol_name("_word")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$243, DW_AT_name("byte")
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_byte")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$45


$C$DW$T$46	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$46, DW_AT_name("CANMDH_WORDS")
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x02)
$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$244, DW_AT_name("LOW_WORD")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_LOW_WORD")
	.dwattr $C$DW$244, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$245, DW_AT_name("HI_WORD")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_HI_WORD")
	.dwattr $C$DW$245, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$46


$C$DW$T$47	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$47, DW_AT_name("CANMDL_BYTES")
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x02)
$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$246, DW_AT_name("BYTE3")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_BYTE3")
	.dwattr $C$DW$246, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$247, DW_AT_name("BYTE2")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_BYTE2")
	.dwattr $C$DW$247, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$248, DW_AT_name("BYTE1")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_BYTE1")
	.dwattr $C$DW$248, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$249, DW_AT_name("BYTE0")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_BYTE0")
	.dwattr $C$DW$249, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$47


$C$DW$T$48	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$48, DW_AT_name("CANMDL_REG")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x02)
$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$250, DW_AT_name("all")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$251, DW_AT_name("word")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_word")
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$252, DW_AT_name("byte")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_byte")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$48


$C$DW$T$49	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$49, DW_AT_name("CANMDL_WORDS")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x02)
$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$253, DW_AT_name("LOW_WORD")
	.dwattr $C$DW$253, DW_AT_TI_symbol_name("_LOW_WORD")
	.dwattr $C$DW$253, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$254, DW_AT_name("HI_WORD")
	.dwattr $C$DW$254, DW_AT_TI_symbol_name("_HI_WORD")
	.dwattr $C$DW$254, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$49


$C$DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$50, DW_AT_name("CANMD_BITS")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x02)
$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$255, DW_AT_name("MD0")
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_MD0")
	.dwattr $C$DW$255, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$256, DW_AT_name("MD1")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_MD1")
	.dwattr $C$DW$256, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$257, DW_AT_name("MD2")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_MD2")
	.dwattr $C$DW$257, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$258, DW_AT_name("MD3")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_MD3")
	.dwattr $C$DW$258, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$259, DW_AT_name("MD4")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_MD4")
	.dwattr $C$DW$259, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$260, DW_AT_name("MD5")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_MD5")
	.dwattr $C$DW$260, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$261, DW_AT_name("MD6")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_MD6")
	.dwattr $C$DW$261, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$262, DW_AT_name("MD7")
	.dwattr $C$DW$262, DW_AT_TI_symbol_name("_MD7")
	.dwattr $C$DW$262, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$263, DW_AT_name("MD8")
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_MD8")
	.dwattr $C$DW$263, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$264, DW_AT_name("MD9")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_MD9")
	.dwattr $C$DW$264, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$265, DW_AT_name("MD10")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_MD10")
	.dwattr $C$DW$265, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$266, DW_AT_name("MD11")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_MD11")
	.dwattr $C$DW$266, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$267, DW_AT_name("MD12")
	.dwattr $C$DW$267, DW_AT_TI_symbol_name("_MD12")
	.dwattr $C$DW$267, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$268, DW_AT_name("MD13")
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_MD13")
	.dwattr $C$DW$268, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$269, DW_AT_name("MD14")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_MD14")
	.dwattr $C$DW$269, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$269, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$270, DW_AT_name("MD15")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_MD15")
	.dwattr $C$DW$270, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$271, DW_AT_name("MD16")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_MD16")
	.dwattr $C$DW$271, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$272	.dwtag  DW_TAG_member
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$272, DW_AT_name("MD17")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_MD17")
	.dwattr $C$DW$272, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$272, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$272, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$273, DW_AT_name("MD18")
	.dwattr $C$DW$273, DW_AT_TI_symbol_name("_MD18")
	.dwattr $C$DW$273, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$274, DW_AT_name("MD19")
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_MD19")
	.dwattr $C$DW$274, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$275	.dwtag  DW_TAG_member
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$275, DW_AT_name("MD20")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_MD20")
	.dwattr $C$DW$275, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$275, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$275, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$276	.dwtag  DW_TAG_member
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$276, DW_AT_name("MD21")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_MD21")
	.dwattr $C$DW$276, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$276, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$276, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$277, DW_AT_name("MD22")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_MD22")
	.dwattr $C$DW$277, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$277, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$278, DW_AT_name("MD23")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_MD23")
	.dwattr $C$DW$278, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$279, DW_AT_name("MD24")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_MD24")
	.dwattr $C$DW$279, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$280, DW_AT_name("MD25")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_MD25")
	.dwattr $C$DW$280, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$281, DW_AT_name("MD26")
	.dwattr $C$DW$281, DW_AT_TI_symbol_name("_MD26")
	.dwattr $C$DW$281, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$282, DW_AT_name("MD27")
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_MD27")
	.dwattr $C$DW$282, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$283, DW_AT_name("MD28")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_MD28")
	.dwattr $C$DW$283, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$284, DW_AT_name("MD29")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_MD29")
	.dwattr $C$DW$284, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$285, DW_AT_name("MD30")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_MD30")
	.dwattr $C$DW$285, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$286, DW_AT_name("MD31")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_MD31")
	.dwattr $C$DW$286, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$50


$C$DW$T$51	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$51, DW_AT_name("CANMD_REG")
	.dwattr $C$DW$T$51, DW_AT_byte_size(0x02)
$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$287, DW_AT_name("all")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$288, DW_AT_name("bit")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$51


$C$DW$T$52	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$52, DW_AT_name("CANME_BITS")
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x02)
$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$289, DW_AT_name("ME0")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_ME0")
	.dwattr $C$DW$289, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$290, DW_AT_name("ME1")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_ME1")
	.dwattr $C$DW$290, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$291, DW_AT_name("ME2")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_ME2")
	.dwattr $C$DW$291, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$292, DW_AT_name("ME3")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_ME3")
	.dwattr $C$DW$292, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$293, DW_AT_name("ME4")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_ME4")
	.dwattr $C$DW$293, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$294, DW_AT_name("ME5")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_ME5")
	.dwattr $C$DW$294, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$295, DW_AT_name("ME6")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_ME6")
	.dwattr $C$DW$295, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$296, DW_AT_name("ME7")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_ME7")
	.dwattr $C$DW$296, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$297, DW_AT_name("ME8")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_ME8")
	.dwattr $C$DW$297, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$298, DW_AT_name("ME9")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_ME9")
	.dwattr $C$DW$298, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$298, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$299, DW_AT_name("ME10")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_ME10")
	.dwattr $C$DW$299, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$299, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$300, DW_AT_name("ME11")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_ME11")
	.dwattr $C$DW$300, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$300, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$301, DW_AT_name("ME12")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_ME12")
	.dwattr $C$DW$301, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$301, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$302, DW_AT_name("ME13")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_ME13")
	.dwattr $C$DW$302, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$302, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$303, DW_AT_name("ME14")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_ME14")
	.dwattr $C$DW$303, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$303, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$304, DW_AT_name("ME15")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_ME15")
	.dwattr $C$DW$304, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$305, DW_AT_name("ME16")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_ME16")
	.dwattr $C$DW$305, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$306, DW_AT_name("ME17")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_ME17")
	.dwattr $C$DW$306, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$307, DW_AT_name("ME18")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_ME18")
	.dwattr $C$DW$307, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$308, DW_AT_name("ME19")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_ME19")
	.dwattr $C$DW$308, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$309, DW_AT_name("ME20")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_ME20")
	.dwattr $C$DW$309, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$310, DW_AT_name("ME21")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_ME21")
	.dwattr $C$DW$310, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$310, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$311, DW_AT_name("ME22")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_ME22")
	.dwattr $C$DW$311, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$311, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$312, DW_AT_name("ME23")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_ME23")
	.dwattr $C$DW$312, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$312, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$313, DW_AT_name("ME24")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_ME24")
	.dwattr $C$DW$313, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$313, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$314, DW_AT_name("ME25")
	.dwattr $C$DW$314, DW_AT_TI_symbol_name("_ME25")
	.dwattr $C$DW$314, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$315, DW_AT_name("ME26")
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_ME26")
	.dwattr $C$DW$315, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$316, DW_AT_name("ME27")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_ME27")
	.dwattr $C$DW$316, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$317, DW_AT_name("ME28")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_ME28")
	.dwattr $C$DW$317, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$318, DW_AT_name("ME29")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_ME29")
	.dwattr $C$DW$318, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$319, DW_AT_name("ME30")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_ME30")
	.dwattr $C$DW$319, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$320, DW_AT_name("ME31")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_ME31")
	.dwattr $C$DW$320, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$52


$C$DW$T$53	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$53, DW_AT_name("CANME_REG")
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x02)
$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$321, DW_AT_name("all")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$322	.dwtag  DW_TAG_member
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$322, DW_AT_name("bit")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$322, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$53


$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("CANMIL_BITS")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x02)
$C$DW$323	.dwtag  DW_TAG_member
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$323, DW_AT_name("MIL0")
	.dwattr $C$DW$323, DW_AT_TI_symbol_name("_MIL0")
	.dwattr $C$DW$323, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$323, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$323, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$324, DW_AT_name("MIL1")
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_MIL1")
	.dwattr $C$DW$324, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$324, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$325, DW_AT_name("MIL2")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_MIL2")
	.dwattr $C$DW$325, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$326, DW_AT_name("MIL3")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_MIL3")
	.dwattr $C$DW$326, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$327, DW_AT_name("MIL4")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_MIL4")
	.dwattr $C$DW$327, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$328, DW_AT_name("MIL5")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_MIL5")
	.dwattr $C$DW$328, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$329, DW_AT_name("MIL6")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_MIL6")
	.dwattr $C$DW$329, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$330, DW_AT_name("MIL7")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_MIL7")
	.dwattr $C$DW$330, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$331, DW_AT_name("MIL8")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_MIL8")
	.dwattr $C$DW$331, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$332, DW_AT_name("MIL9")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_MIL9")
	.dwattr $C$DW$332, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$333, DW_AT_name("MIL10")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_MIL10")
	.dwattr $C$DW$333, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$334, DW_AT_name("MIL11")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_MIL11")
	.dwattr $C$DW$334, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$335, DW_AT_name("MIL12")
	.dwattr $C$DW$335, DW_AT_TI_symbol_name("_MIL12")
	.dwattr $C$DW$335, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$336, DW_AT_name("MIL13")
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_MIL13")
	.dwattr $C$DW$336, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$337, DW_AT_name("MIL14")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_MIL14")
	.dwattr $C$DW$337, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_member
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$338, DW_AT_name("MIL15")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_MIL15")
	.dwattr $C$DW$338, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$338, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_member
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$339, DW_AT_name("MIL16")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_MIL16")
	.dwattr $C$DW$339, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$339, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_member
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$340, DW_AT_name("MIL17")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_MIL17")
	.dwattr $C$DW$340, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$340, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$341, DW_AT_name("MIL18")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_MIL18")
	.dwattr $C$DW$341, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$341, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$342, DW_AT_name("MIL19")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_MIL19")
	.dwattr $C$DW$342, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$343, DW_AT_name("MIL20")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_MIL20")
	.dwattr $C$DW$343, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$344, DW_AT_name("MIL21")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_MIL21")
	.dwattr $C$DW$344, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$345, DW_AT_name("MIL22")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_MIL22")
	.dwattr $C$DW$345, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$346, DW_AT_name("MIL23")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_MIL23")
	.dwattr $C$DW$346, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$347, DW_AT_name("MIL24")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_MIL24")
	.dwattr $C$DW$347, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_member
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$348, DW_AT_name("MIL25")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_MIL25")
	.dwattr $C$DW$348, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$348, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_member
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$349, DW_AT_name("MIL26")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_MIL26")
	.dwattr $C$DW$349, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$349, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_member
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$350, DW_AT_name("MIL27")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_MIL27")
	.dwattr $C$DW$350, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$350, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_member
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$351, DW_AT_name("MIL28")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_MIL28")
	.dwattr $C$DW$351, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$351, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$351, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$352	.dwtag  DW_TAG_member
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$352, DW_AT_name("MIL29")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_MIL29")
	.dwattr $C$DW$352, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$352, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$352, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$353, DW_AT_name("MIL30")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_MIL30")
	.dwattr $C$DW$353, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$353, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$354, DW_AT_name("MIL31")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_MIL31")
	.dwattr $C$DW$354, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54


$C$DW$T$55	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$55, DW_AT_name("CANMIL_REG")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x02)
$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$355, DW_AT_name("all")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$356, DW_AT_name("bit")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_name("CANMIM_BITS")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x02)
$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$357, DW_AT_name("MIM0")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_MIM0")
	.dwattr $C$DW$357, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$358, DW_AT_name("MIM1")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_MIM1")
	.dwattr $C$DW$358, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$359, DW_AT_name("MIM2")
	.dwattr $C$DW$359, DW_AT_TI_symbol_name("_MIM2")
	.dwattr $C$DW$359, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$360, DW_AT_name("MIM3")
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_MIM3")
	.dwattr $C$DW$360, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$361, DW_AT_name("MIM4")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_MIM4")
	.dwattr $C$DW$361, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$362, DW_AT_name("MIM5")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_MIM5")
	.dwattr $C$DW$362, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$363, DW_AT_name("MIM6")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_MIM6")
	.dwattr $C$DW$363, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$364, DW_AT_name("MIM7")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_MIM7")
	.dwattr $C$DW$364, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_member
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$365, DW_AT_name("MIM8")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_MIM8")
	.dwattr $C$DW$365, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$365, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_member
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$366, DW_AT_name("MIM9")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_MIM9")
	.dwattr $C$DW$366, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$366, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_member
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$367, DW_AT_name("MIM10")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_MIM10")
	.dwattr $C$DW$367, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$367, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_member
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$368, DW_AT_name("MIM11")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_MIM11")
	.dwattr $C$DW$368, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$368, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$368, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$369, DW_AT_name("MIM12")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_MIM12")
	.dwattr $C$DW$369, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$369, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$370, DW_AT_name("MIM13")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_MIM13")
	.dwattr $C$DW$370, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$371, DW_AT_name("MIM14")
	.dwattr $C$DW$371, DW_AT_TI_symbol_name("_MIM14")
	.dwattr $C$DW$371, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$372, DW_AT_name("MIM15")
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_MIM15")
	.dwattr $C$DW$372, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$373, DW_AT_name("MIM16")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_MIM16")
	.dwattr $C$DW$373, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_member
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$374, DW_AT_name("MIM17")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_MIM17")
	.dwattr $C$DW$374, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$374, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_member
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$375, DW_AT_name("MIM18")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_MIM18")
	.dwattr $C$DW$375, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$375, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_member
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$376, DW_AT_name("MIM19")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_MIM19")
	.dwattr $C$DW$376, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$376, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$377, DW_AT_name("MIM20")
	.dwattr $C$DW$377, DW_AT_TI_symbol_name("_MIM20")
	.dwattr $C$DW$377, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$377, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$378, DW_AT_name("MIM21")
	.dwattr $C$DW$378, DW_AT_TI_symbol_name("_MIM21")
	.dwattr $C$DW$378, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$379, DW_AT_name("MIM22")
	.dwattr $C$DW$379, DW_AT_TI_symbol_name("_MIM22")
	.dwattr $C$DW$379, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$380, DW_AT_name("MIM23")
	.dwattr $C$DW$380, DW_AT_TI_symbol_name("_MIM23")
	.dwattr $C$DW$380, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$381, DW_AT_name("MIM24")
	.dwattr $C$DW$381, DW_AT_TI_symbol_name("_MIM24")
	.dwattr $C$DW$381, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$382	.dwtag  DW_TAG_member
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$382, DW_AT_name("MIM25")
	.dwattr $C$DW$382, DW_AT_TI_symbol_name("_MIM25")
	.dwattr $C$DW$382, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$382, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_member
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$383, DW_AT_name("MIM26")
	.dwattr $C$DW$383, DW_AT_TI_symbol_name("_MIM26")
	.dwattr $C$DW$383, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$383, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$383, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$384	.dwtag  DW_TAG_member
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$384, DW_AT_name("MIM27")
	.dwattr $C$DW$384, DW_AT_TI_symbol_name("_MIM27")
	.dwattr $C$DW$384, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$384, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_member
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$385, DW_AT_name("MIM28")
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_MIM28")
	.dwattr $C$DW$385, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$385, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$385, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$386	.dwtag  DW_TAG_member
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$386, DW_AT_name("MIM29")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_MIM29")
	.dwattr $C$DW$386, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$386, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$386, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$387	.dwtag  DW_TAG_member
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$387, DW_AT_name("MIM30")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_MIM30")
	.dwattr $C$DW$387, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$387, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$387, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$388, DW_AT_name("MIM31")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_MIM31")
	.dwattr $C$DW$388, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$388, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56


$C$DW$T$57	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$57, DW_AT_name("CANMIM_REG")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x02)
$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$389, DW_AT_name("all")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$390, DW_AT_name("bit")
	.dwattr $C$DW$390, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$58, DW_AT_name("CANMSGCTRL_BITS")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x02)
$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$391, DW_AT_name("DLC")
	.dwattr $C$DW$391, DW_AT_TI_symbol_name("_DLC")
	.dwattr $C$DW$391, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x04)
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$392, DW_AT_name("RTR")
	.dwattr $C$DW$392, DW_AT_TI_symbol_name("_RTR")
	.dwattr $C$DW$392, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$393, DW_AT_name("rsvd1")
	.dwattr $C$DW$393, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$393, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x03)
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$394, DW_AT_name("TPL")
	.dwattr $C$DW$394, DW_AT_TI_symbol_name("_TPL")
	.dwattr $C$DW$394, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x05)
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$395, DW_AT_name("rsvd2")
	.dwattr $C$DW$395, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$395, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$396, DW_AT_name("rsvd3")
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$396, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$59, DW_AT_name("CANMSGCTRL_REG")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x02)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$397, DW_AT_name("all")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$398, DW_AT_name("bit")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$60, DW_AT_name("CANMSGID_BITS")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x02)
$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$399, DW_AT_name("EXTMSGID_L")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_EXTMSGID_L")
	.dwattr $C$DW$399, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$400, DW_AT_name("EXTMSGID_H")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_EXTMSGID_H")
	.dwattr $C$DW$400, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$401, DW_AT_name("STDMSGID")
	.dwattr $C$DW$401, DW_AT_TI_symbol_name("_STDMSGID")
	.dwattr $C$DW$401, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x0b)
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$402, DW_AT_name("AAM")
	.dwattr $C$DW$402, DW_AT_TI_symbol_name("_AAM")
	.dwattr $C$DW$402, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$403, DW_AT_name("AME")
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_AME")
	.dwattr $C$DW$403, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$404, DW_AT_name("IDE")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_IDE")
	.dwattr $C$DW$404, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$61, DW_AT_name("CANMSGID_REG")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x02)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$405, DW_AT_name("all")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$406, DW_AT_name("bit")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$62, DW_AT_name("CANOPC_BITS")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x02)
$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$407, DW_AT_name("OPC0")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_OPC0")
	.dwattr $C$DW$407, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$408, DW_AT_name("OPC1")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_OPC1")
	.dwattr $C$DW$408, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$409, DW_AT_name("OPC2")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_OPC2")
	.dwattr $C$DW$409, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$410, DW_AT_name("OPC3")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_OPC3")
	.dwattr $C$DW$410, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$411, DW_AT_name("OPC4")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_OPC4")
	.dwattr $C$DW$411, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$412, DW_AT_name("OPC5")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_OPC5")
	.dwattr $C$DW$412, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$413, DW_AT_name("OPC6")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_OPC6")
	.dwattr $C$DW$413, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$414, DW_AT_name("OPC7")
	.dwattr $C$DW$414, DW_AT_TI_symbol_name("_OPC7")
	.dwattr $C$DW$414, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$415, DW_AT_name("OPC8")
	.dwattr $C$DW$415, DW_AT_TI_symbol_name("_OPC8")
	.dwattr $C$DW$415, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$416, DW_AT_name("OPC9")
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_OPC9")
	.dwattr $C$DW$416, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$417, DW_AT_name("OPC10")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_OPC10")
	.dwattr $C$DW$417, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$418, DW_AT_name("OPC11")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_OPC11")
	.dwattr $C$DW$418, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$419, DW_AT_name("OPC12")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_OPC12")
	.dwattr $C$DW$419, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_member
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$420, DW_AT_name("OPC13")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_OPC13")
	.dwattr $C$DW$420, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$420, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$421, DW_AT_name("OPC14")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_OPC14")
	.dwattr $C$DW$421, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$421, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$422, DW_AT_name("OPC15")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_OPC15")
	.dwattr $C$DW$422, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$423, DW_AT_name("OPC16")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_OPC16")
	.dwattr $C$DW$423, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$423, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$424, DW_AT_name("OPC17")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_OPC17")
	.dwattr $C$DW$424, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$425, DW_AT_name("OPC18")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_OPC18")
	.dwattr $C$DW$425, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$426, DW_AT_name("OPC19")
	.dwattr $C$DW$426, DW_AT_TI_symbol_name("_OPC19")
	.dwattr $C$DW$426, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$426, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$427, DW_AT_name("OPC20")
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_OPC20")
	.dwattr $C$DW$427, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$428, DW_AT_name("OPC21")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_OPC21")
	.dwattr $C$DW$428, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$429, DW_AT_name("OPC22")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_OPC22")
	.dwattr $C$DW$429, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$430, DW_AT_name("OPC23")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_OPC23")
	.dwattr $C$DW$430, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_member
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$431, DW_AT_name("OPC24")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_OPC24")
	.dwattr $C$DW$431, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$431, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$432, DW_AT_name("OPC25")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_OPC25")
	.dwattr $C$DW$432, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$432, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$433, DW_AT_name("OPC26")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_OPC26")
	.dwattr $C$DW$433, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$434, DW_AT_name("OPC27")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_OPC27")
	.dwattr $C$DW$434, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$435	.dwtag  DW_TAG_member
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$435, DW_AT_name("OPC28")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_OPC28")
	.dwattr $C$DW$435, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$435, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$436, DW_AT_name("OPC29")
	.dwattr $C$DW$436, DW_AT_TI_symbol_name("_OPC29")
	.dwattr $C$DW$436, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$436, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$437, DW_AT_name("OPC30")
	.dwattr $C$DW$437, DW_AT_TI_symbol_name("_OPC30")
	.dwattr $C$DW$437, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$438, DW_AT_name("OPC31")
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_OPC31")
	.dwattr $C$DW$438, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$62


$C$DW$T$63	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$63, DW_AT_name("CANOPC_REG")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x02)
$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$439, DW_AT_name("all")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$440, DW_AT_name("bit")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$63


$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("CANREC_BITS")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x02)
$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$441, DW_AT_name("REC")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_REC")
	.dwattr $C$DW$441, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$442, DW_AT_name("rsvd1")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$442, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$443, DW_AT_name("rsvd2")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$443, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$64


$C$DW$T$65	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$65, DW_AT_name("CANREC_REG")
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x02)
$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$444, DW_AT_name("all")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$445, DW_AT_name("bit")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$65


$C$DW$T$66	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$66, DW_AT_name("CANRFP_BITS")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x02)
$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$446, DW_AT_name("RFP0")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_RFP0")
	.dwattr $C$DW$446, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$447, DW_AT_name("RFP1")
	.dwattr $C$DW$447, DW_AT_TI_symbol_name("_RFP1")
	.dwattr $C$DW$447, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$448, DW_AT_name("RFP2")
	.dwattr $C$DW$448, DW_AT_TI_symbol_name("_RFP2")
	.dwattr $C$DW$448, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$449, DW_AT_name("RFP3")
	.dwattr $C$DW$449, DW_AT_TI_symbol_name("_RFP3")
	.dwattr $C$DW$449, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$450, DW_AT_name("RFP4")
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_RFP4")
	.dwattr $C$DW$450, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$451, DW_AT_name("RFP5")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_RFP5")
	.dwattr $C$DW$451, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$452, DW_AT_name("RFP6")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_RFP6")
	.dwattr $C$DW$452, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$453, DW_AT_name("RFP7")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_RFP7")
	.dwattr $C$DW$453, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$454, DW_AT_name("RFP8")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_RFP8")
	.dwattr $C$DW$454, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$455, DW_AT_name("RFP9")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_RFP9")
	.dwattr $C$DW$455, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$456, DW_AT_name("RFP10")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_RFP10")
	.dwattr $C$DW$456, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$457, DW_AT_name("RFP11")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_RFP11")
	.dwattr $C$DW$457, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$458, DW_AT_name("RFP12")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_RFP12")
	.dwattr $C$DW$458, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$459, DW_AT_name("RFP13")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_RFP13")
	.dwattr $C$DW$459, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$460, DW_AT_name("RFP14")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_RFP14")
	.dwattr $C$DW$460, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$461, DW_AT_name("RFP15")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_RFP15")
	.dwattr $C$DW$461, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$462, DW_AT_name("RFP16")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_RFP16")
	.dwattr $C$DW$462, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$463, DW_AT_name("RFP17")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_RFP17")
	.dwattr $C$DW$463, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$464, DW_AT_name("RFP18")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_RFP18")
	.dwattr $C$DW$464, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$465, DW_AT_name("RFP19")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_RFP19")
	.dwattr $C$DW$465, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$466, DW_AT_name("RFP20")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_RFP20")
	.dwattr $C$DW$466, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$467, DW_AT_name("RFP21")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_RFP21")
	.dwattr $C$DW$467, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$468, DW_AT_name("RFP22")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_RFP22")
	.dwattr $C$DW$468, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$469, DW_AT_name("RFP23")
	.dwattr $C$DW$469, DW_AT_TI_symbol_name("_RFP23")
	.dwattr $C$DW$469, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$470, DW_AT_name("RFP24")
	.dwattr $C$DW$470, DW_AT_TI_symbol_name("_RFP24")
	.dwattr $C$DW$470, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$471, DW_AT_name("RFP25")
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_RFP25")
	.dwattr $C$DW$471, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$472, DW_AT_name("RFP26")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_RFP26")
	.dwattr $C$DW$472, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$473, DW_AT_name("RFP27")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_RFP27")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$474, DW_AT_name("RFP28")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_RFP28")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$475, DW_AT_name("RFP29")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_RFP29")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$476, DW_AT_name("RFP30")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_RFP30")
	.dwattr $C$DW$476, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$477, DW_AT_name("RFP31")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_RFP31")
	.dwattr $C$DW$477, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$66


$C$DW$T$67	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$67, DW_AT_name("CANRFP_REG")
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x02)
$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$478, DW_AT_name("all")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$479, DW_AT_name("bit")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$67


$C$DW$T$68	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$68, DW_AT_name("CANRIOC_BITS")
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x02)
$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$480, DW_AT_name("rsvd1")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$480, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$481, DW_AT_name("RXFUNC")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_RXFUNC")
	.dwattr $C$DW$481, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$482, DW_AT_name("rsvd2")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$482, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$483, DW_AT_name("rsvd3")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$483, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$68


$C$DW$T$69	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$69, DW_AT_name("CANRIOC_REG")
	.dwattr $C$DW$T$69, DW_AT_byte_size(0x02)
$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$484, DW_AT_name("all")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$485, DW_AT_name("bit")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$69


$C$DW$T$70	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$70, DW_AT_name("CANRML_BITS")
	.dwattr $C$DW$T$70, DW_AT_byte_size(0x02)
$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$486, DW_AT_name("RML0")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_RML0")
	.dwattr $C$DW$486, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$487, DW_AT_name("RML1")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_RML1")
	.dwattr $C$DW$487, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$488, DW_AT_name("RML2")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_RML2")
	.dwattr $C$DW$488, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$489, DW_AT_name("RML3")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_RML3")
	.dwattr $C$DW$489, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$490, DW_AT_name("RML4")
	.dwattr $C$DW$490, DW_AT_TI_symbol_name("_RML4")
	.dwattr $C$DW$490, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$491, DW_AT_name("RML5")
	.dwattr $C$DW$491, DW_AT_TI_symbol_name("_RML5")
	.dwattr $C$DW$491, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$492, DW_AT_name("RML6")
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_RML6")
	.dwattr $C$DW$492, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$493, DW_AT_name("RML7")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_RML7")
	.dwattr $C$DW$493, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$494, DW_AT_name("RML8")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_RML8")
	.dwattr $C$DW$494, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$495, DW_AT_name("RML9")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_RML9")
	.dwattr $C$DW$495, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$496, DW_AT_name("RML10")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_RML10")
	.dwattr $C$DW$496, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$497, DW_AT_name("RML11")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_RML11")
	.dwattr $C$DW$497, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$498, DW_AT_name("RML12")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_RML12")
	.dwattr $C$DW$498, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$499, DW_AT_name("RML13")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_RML13")
	.dwattr $C$DW$499, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$500, DW_AT_name("RML14")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_RML14")
	.dwattr $C$DW$500, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$501, DW_AT_name("RML15")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_RML15")
	.dwattr $C$DW$501, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$502, DW_AT_name("RML16")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_RML16")
	.dwattr $C$DW$502, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$503, DW_AT_name("RML17")
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_RML17")
	.dwattr $C$DW$503, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$504, DW_AT_name("RML18")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_RML18")
	.dwattr $C$DW$504, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$505, DW_AT_name("RML19")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_RML19")
	.dwattr $C$DW$505, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$506, DW_AT_name("RML20")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_RML20")
	.dwattr $C$DW$506, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$507, DW_AT_name("RML21")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_RML21")
	.dwattr $C$DW$507, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$508, DW_AT_name("RML22")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_RML22")
	.dwattr $C$DW$508, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$509, DW_AT_name("RML23")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_RML23")
	.dwattr $C$DW$509, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$510, DW_AT_name("RML24")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_RML24")
	.dwattr $C$DW$510, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$511, DW_AT_name("RML25")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_RML25")
	.dwattr $C$DW$511, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$512, DW_AT_name("RML26")
	.dwattr $C$DW$512, DW_AT_TI_symbol_name("_RML26")
	.dwattr $C$DW$512, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$513, DW_AT_name("RML27")
	.dwattr $C$DW$513, DW_AT_TI_symbol_name("_RML27")
	.dwattr $C$DW$513, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$514, DW_AT_name("RML28")
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_RML28")
	.dwattr $C$DW$514, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$515, DW_AT_name("RML29")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_RML29")
	.dwattr $C$DW$515, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$516, DW_AT_name("RML30")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_RML30")
	.dwattr $C$DW$516, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$517, DW_AT_name("RML31")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_RML31")
	.dwattr $C$DW$517, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$70


$C$DW$T$71	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$71, DW_AT_name("CANRML_REG")
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x02)
$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$518, DW_AT_name("all")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$519, DW_AT_name("bit")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$71


$C$DW$T$72	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$72, DW_AT_name("CANRMP_BITS")
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x02)
$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$520, DW_AT_name("RMP0")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_RMP0")
	.dwattr $C$DW$520, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$521, DW_AT_name("RMP1")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_RMP1")
	.dwattr $C$DW$521, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$522, DW_AT_name("RMP2")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_RMP2")
	.dwattr $C$DW$522, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$523	.dwtag  DW_TAG_member
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$523, DW_AT_name("RMP3")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_RMP3")
	.dwattr $C$DW$523, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$523, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$524, DW_AT_name("RMP4")
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_RMP4")
	.dwattr $C$DW$524, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$524, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$525, DW_AT_name("RMP5")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_RMP5")
	.dwattr $C$DW$525, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$526, DW_AT_name("RMP6")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_RMP6")
	.dwattr $C$DW$526, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$527, DW_AT_name("RMP7")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_RMP7")
	.dwattr $C$DW$527, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$528, DW_AT_name("RMP8")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_RMP8")
	.dwattr $C$DW$528, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$529, DW_AT_name("RMP9")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_RMP9")
	.dwattr $C$DW$529, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$530, DW_AT_name("RMP10")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_RMP10")
	.dwattr $C$DW$530, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$531, DW_AT_name("RMP11")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_RMP11")
	.dwattr $C$DW$531, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$532, DW_AT_name("RMP12")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_RMP12")
	.dwattr $C$DW$532, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$533, DW_AT_name("RMP13")
	.dwattr $C$DW$533, DW_AT_TI_symbol_name("_RMP13")
	.dwattr $C$DW$533, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$534, DW_AT_name("RMP14")
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_RMP14")
	.dwattr $C$DW$534, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$535, DW_AT_name("RMP15")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_RMP15")
	.dwattr $C$DW$535, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$536, DW_AT_name("RMP16")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_RMP16")
	.dwattr $C$DW$536, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$537, DW_AT_name("RMP17")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_RMP17")
	.dwattr $C$DW$537, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$538, DW_AT_name("RMP18")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_RMP18")
	.dwattr $C$DW$538, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$539, DW_AT_name("RMP19")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_RMP19")
	.dwattr $C$DW$539, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$540, DW_AT_name("RMP20")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_RMP20")
	.dwattr $C$DW$540, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$541, DW_AT_name("RMP21")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_RMP21")
	.dwattr $C$DW$541, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$542, DW_AT_name("RMP22")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_RMP22")
	.dwattr $C$DW$542, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$543, DW_AT_name("RMP23")
	.dwattr $C$DW$543, DW_AT_TI_symbol_name("_RMP23")
	.dwattr $C$DW$543, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$544, DW_AT_name("RMP24")
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_RMP24")
	.dwattr $C$DW$544, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$545, DW_AT_name("RMP25")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_RMP25")
	.dwattr $C$DW$545, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$546, DW_AT_name("RMP26")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_RMP26")
	.dwattr $C$DW$546, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$547, DW_AT_name("RMP27")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_RMP27")
	.dwattr $C$DW$547, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$548, DW_AT_name("RMP28")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_RMP28")
	.dwattr $C$DW$548, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$549, DW_AT_name("RMP29")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_RMP29")
	.dwattr $C$DW$549, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$550, DW_AT_name("RMP30")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_RMP30")
	.dwattr $C$DW$550, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$551, DW_AT_name("RMP31")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_RMP31")
	.dwattr $C$DW$551, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$72


$C$DW$T$73	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$73, DW_AT_name("CANRMP_REG")
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x02)
$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$552, DW_AT_name("all")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$552, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$553, DW_AT_name("bit")
	.dwattr $C$DW$553, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$553, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$73


$C$DW$T$74	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$74, DW_AT_name("CANTA_BITS")
	.dwattr $C$DW$T$74, DW_AT_byte_size(0x02)
$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$554, DW_AT_name("TA0")
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_TA0")
	.dwattr $C$DW$554, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$554, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$555, DW_AT_name("TA1")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_TA1")
	.dwattr $C$DW$555, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$555, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$556, DW_AT_name("TA2")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_TA2")
	.dwattr $C$DW$556, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$556, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$557, DW_AT_name("TA3")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_TA3")
	.dwattr $C$DW$557, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$557, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$558, DW_AT_name("TA4")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_TA4")
	.dwattr $C$DW$558, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$558, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$559, DW_AT_name("TA5")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_TA5")
	.dwattr $C$DW$559, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$560, DW_AT_name("TA6")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_TA6")
	.dwattr $C$DW$560, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$561, DW_AT_name("TA7")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_TA7")
	.dwattr $C$DW$561, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$562, DW_AT_name("TA8")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_TA8")
	.dwattr $C$DW$562, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$563, DW_AT_name("TA9")
	.dwattr $C$DW$563, DW_AT_TI_symbol_name("_TA9")
	.dwattr $C$DW$563, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$564, DW_AT_name("TA10")
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_TA10")
	.dwattr $C$DW$564, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$565, DW_AT_name("TA11")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_TA11")
	.dwattr $C$DW$565, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$566, DW_AT_name("TA12")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_TA12")
	.dwattr $C$DW$566, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$567, DW_AT_name("TA13")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_TA13")
	.dwattr $C$DW$567, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$567, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$568, DW_AT_name("TA14")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_TA14")
	.dwattr $C$DW$568, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$568, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$569, DW_AT_name("TA15")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_TA15")
	.dwattr $C$DW$569, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$569, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_member
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$570, DW_AT_name("TA16")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_TA16")
	.dwattr $C$DW$570, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$570, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_member
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$571, DW_AT_name("TA17")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_TA17")
	.dwattr $C$DW$571, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$571, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$571, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$572	.dwtag  DW_TAG_member
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$572, DW_AT_name("TA18")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_TA18")
	.dwattr $C$DW$572, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$572, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$573	.dwtag  DW_TAG_member
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$573, DW_AT_name("TA19")
	.dwattr $C$DW$573, DW_AT_TI_symbol_name("_TA19")
	.dwattr $C$DW$573, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$573, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$573, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$574, DW_AT_name("TA20")
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_TA20")
	.dwattr $C$DW$574, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$574, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$575, DW_AT_name("TA21")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_TA21")
	.dwattr $C$DW$575, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$576, DW_AT_name("TA22")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_TA22")
	.dwattr $C$DW$576, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$577, DW_AT_name("TA23")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_TA23")
	.dwattr $C$DW$577, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$578, DW_AT_name("TA24")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_TA24")
	.dwattr $C$DW$578, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$579, DW_AT_name("TA25")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_TA25")
	.dwattr $C$DW$579, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$580, DW_AT_name("TA26")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_TA26")
	.dwattr $C$DW$580, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$581, DW_AT_name("TA27")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_TA27")
	.dwattr $C$DW$581, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$582, DW_AT_name("TA28")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_TA28")
	.dwattr $C$DW$582, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$583, DW_AT_name("TA29")
	.dwattr $C$DW$583, DW_AT_TI_symbol_name("_TA29")
	.dwattr $C$DW$583, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$584, DW_AT_name("TA30")
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_TA30")
	.dwattr $C$DW$584, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$585, DW_AT_name("TA31")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_TA31")
	.dwattr $C$DW$585, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$74


$C$DW$T$75	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$75, DW_AT_name("CANTA_REG")
	.dwattr $C$DW$T$75, DW_AT_byte_size(0x02)
$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$586, DW_AT_name("all")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$587, DW_AT_name("bit")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$587, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$75


$C$DW$T$76	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$76, DW_AT_name("CANTEC_BITS")
	.dwattr $C$DW$T$76, DW_AT_byte_size(0x02)
$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$588, DW_AT_name("TEC")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_TEC")
	.dwattr $C$DW$588, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x08)
	.dwattr $C$DW$588, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$589, DW_AT_name("rsvd1")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$589, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$590, DW_AT_name("rsvd2")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$590, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$76


$C$DW$T$77	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$77, DW_AT_name("CANTEC_REG")
	.dwattr $C$DW$T$77, DW_AT_byte_size(0x02)
$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$591, DW_AT_name("all")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$592, DW_AT_name("bit")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$77


$C$DW$T$78	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$78, DW_AT_name("CANTIOC_BITS")
	.dwattr $C$DW$T$78, DW_AT_byte_size(0x02)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$593, DW_AT_name("rsvd1")
	.dwattr $C$DW$593, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$593, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x03)
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$594, DW_AT_name("TXFUNC")
	.dwattr $C$DW$594, DW_AT_TI_symbol_name("_TXFUNC")
	.dwattr $C$DW$594, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$595, DW_AT_name("rsvd2")
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0c)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$596, DW_AT_name("rsvd3")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$596, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$78


$C$DW$T$79	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$79, DW_AT_name("CANTIOC_REG")
	.dwattr $C$DW$T$79, DW_AT_byte_size(0x02)
$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$597, DW_AT_name("all")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$598, DW_AT_name("bit")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$79


$C$DW$T$80	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$80, DW_AT_name("CANTOC_BITS")
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x02)
$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$599, DW_AT_name("TOC0")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_TOC0")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$600, DW_AT_name("TOC1")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_TOC1")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$601, DW_AT_name("TOC2")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_TOC2")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$602, DW_AT_name("TOC3")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_TOC3")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$603, DW_AT_name("TOC4")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_TOC4")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$604, DW_AT_name("TOC5")
	.dwattr $C$DW$604, DW_AT_TI_symbol_name("_TOC5")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$605, DW_AT_name("TOC6")
	.dwattr $C$DW$605, DW_AT_TI_symbol_name("_TOC6")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$606, DW_AT_name("TOC7")
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_TOC7")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$607, DW_AT_name("TOC8")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_TOC8")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$608, DW_AT_name("TOC9")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_TOC9")
	.dwattr $C$DW$608, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$609, DW_AT_name("TOC10")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_TOC10")
	.dwattr $C$DW$609, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$610, DW_AT_name("TOC11")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_TOC11")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$611, DW_AT_name("TOC12")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_TOC12")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$612, DW_AT_name("TOC13")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_TOC13")
	.dwattr $C$DW$612, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$613, DW_AT_name("TOC14")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_TOC14")
	.dwattr $C$DW$613, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$614, DW_AT_name("TOC15")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_TOC15")
	.dwattr $C$DW$614, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$615, DW_AT_name("TOC16")
	.dwattr $C$DW$615, DW_AT_TI_symbol_name("_TOC16")
	.dwattr $C$DW$615, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$616, DW_AT_name("TOC17")
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_TOC17")
	.dwattr $C$DW$616, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$617, DW_AT_name("TOC18")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_TOC18")
	.dwattr $C$DW$617, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$618, DW_AT_name("TOC19")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_TOC19")
	.dwattr $C$DW$618, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$619, DW_AT_name("TOC20")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_TOC20")
	.dwattr $C$DW$619, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$620, DW_AT_name("TOC21")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_TOC21")
	.dwattr $C$DW$620, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$621, DW_AT_name("TOC22")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_TOC22")
	.dwattr $C$DW$621, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$622, DW_AT_name("TOC23")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_TOC23")
	.dwattr $C$DW$622, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$623, DW_AT_name("TOC24")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_TOC24")
	.dwattr $C$DW$623, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$624, DW_AT_name("TOC25")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_TOC25")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$625, DW_AT_name("TOC26")
	.dwattr $C$DW$625, DW_AT_TI_symbol_name("_TOC26")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$626, DW_AT_name("TOC27")
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_TOC27")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$627, DW_AT_name("TOC28")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_TOC28")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$628, DW_AT_name("TOC29")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_TOC29")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$629, DW_AT_name("TOC30")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_TOC30")
	.dwattr $C$DW$629, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$630, DW_AT_name("TOC31")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_TOC31")
	.dwattr $C$DW$630, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$80


$C$DW$T$81	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$81, DW_AT_name("CANTOC_REG")
	.dwattr $C$DW$T$81, DW_AT_byte_size(0x02)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$631, DW_AT_name("all")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$632, DW_AT_name("bit")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$81


$C$DW$T$82	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$82, DW_AT_name("CANTOS_BITS")
	.dwattr $C$DW$T$82, DW_AT_byte_size(0x02)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$633, DW_AT_name("TOS0")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_TOS0")
	.dwattr $C$DW$633, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$634, DW_AT_name("TOS1")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_TOS1")
	.dwattr $C$DW$634, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$635, DW_AT_name("TOS2")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_TOS2")
	.dwattr $C$DW$635, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$636, DW_AT_name("TOS3")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_TOS3")
	.dwattr $C$DW$636, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$637, DW_AT_name("TOS4")
	.dwattr $C$DW$637, DW_AT_TI_symbol_name("_TOS4")
	.dwattr $C$DW$637, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$638, DW_AT_name("TOS5")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_TOS5")
	.dwattr $C$DW$638, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$639, DW_AT_name("TOS6")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_TOS6")
	.dwattr $C$DW$639, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$640, DW_AT_name("TOS7")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_TOS7")
	.dwattr $C$DW$640, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$641, DW_AT_name("TOS8")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_TOS8")
	.dwattr $C$DW$641, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$642, DW_AT_name("TOS9")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_TOS9")
	.dwattr $C$DW$642, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$643, DW_AT_name("TOS10")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_TOS10")
	.dwattr $C$DW$643, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$644, DW_AT_name("TOS11")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_TOS11")
	.dwattr $C$DW$644, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$645, DW_AT_name("TOS12")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_TOS12")
	.dwattr $C$DW$645, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$646, DW_AT_name("TOS13")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_TOS13")
	.dwattr $C$DW$646, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$647, DW_AT_name("TOS14")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_TOS14")
	.dwattr $C$DW$647, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$648, DW_AT_name("TOS15")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_TOS15")
	.dwattr $C$DW$648, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$649, DW_AT_name("TOS16")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_TOS16")
	.dwattr $C$DW$649, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$650, DW_AT_name("TOS17")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_TOS17")
	.dwattr $C$DW$650, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$651, DW_AT_name("TOS18")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_TOS18")
	.dwattr $C$DW$651, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$652, DW_AT_name("TOS19")
	.dwattr $C$DW$652, DW_AT_TI_symbol_name("_TOS19")
	.dwattr $C$DW$652, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$653, DW_AT_name("TOS20")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_TOS20")
	.dwattr $C$DW$653, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$654, DW_AT_name("TOS21")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_TOS21")
	.dwattr $C$DW$654, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$655, DW_AT_name("TOS22")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_TOS22")
	.dwattr $C$DW$655, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$656, DW_AT_name("TOS23")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_TOS23")
	.dwattr $C$DW$656, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$657, DW_AT_name("TOS24")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_TOS24")
	.dwattr $C$DW$657, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$658, DW_AT_name("TOS25")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_TOS25")
	.dwattr $C$DW$658, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$659, DW_AT_name("TOS26")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_TOS26")
	.dwattr $C$DW$659, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$660, DW_AT_name("TOS27")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_TOS27")
	.dwattr $C$DW$660, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$661, DW_AT_name("TOS28")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_TOS28")
	.dwattr $C$DW$661, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$662, DW_AT_name("TOS29")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_TOS29")
	.dwattr $C$DW$662, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$663, DW_AT_name("TOS30")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_TOS30")
	.dwattr $C$DW$663, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$664, DW_AT_name("TOS31")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_TOS31")
	.dwattr $C$DW$664, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$82


$C$DW$T$83	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$83, DW_AT_name("CANTOS_REG")
	.dwattr $C$DW$T$83, DW_AT_byte_size(0x02)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$665, DW_AT_name("all")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$666, DW_AT_name("bit")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$83


$C$DW$T$84	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$84, DW_AT_name("CANTRR_BITS")
	.dwattr $C$DW$T$84, DW_AT_byte_size(0x02)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$667, DW_AT_name("TRR0")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_TRR0")
	.dwattr $C$DW$667, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$668, DW_AT_name("TRR1")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_TRR1")
	.dwattr $C$DW$668, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$669, DW_AT_name("TRR2")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_TRR2")
	.dwattr $C$DW$669, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$670, DW_AT_name("TRR3")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_TRR3")
	.dwattr $C$DW$670, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$671, DW_AT_name("TRR4")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_TRR4")
	.dwattr $C$DW$671, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$672, DW_AT_name("TRR5")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_TRR5")
	.dwattr $C$DW$672, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$673, DW_AT_name("TRR6")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_TRR6")
	.dwattr $C$DW$673, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$674, DW_AT_name("TRR7")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_TRR7")
	.dwattr $C$DW$674, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$675, DW_AT_name("TRR8")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_TRR8")
	.dwattr $C$DW$675, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$676, DW_AT_name("TRR9")
	.dwattr $C$DW$676, DW_AT_TI_symbol_name("_TRR9")
	.dwattr $C$DW$676, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$677, DW_AT_name("TRR10")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_TRR10")
	.dwattr $C$DW$677, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$678, DW_AT_name("TRR11")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_TRR11")
	.dwattr $C$DW$678, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$679, DW_AT_name("TRR12")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_TRR12")
	.dwattr $C$DW$679, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$680, DW_AT_name("TRR13")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_TRR13")
	.dwattr $C$DW$680, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$681, DW_AT_name("TRR14")
	.dwattr $C$DW$681, DW_AT_TI_symbol_name("_TRR14")
	.dwattr $C$DW$681, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$682, DW_AT_name("TRR15")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_TRR15")
	.dwattr $C$DW$682, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$683, DW_AT_name("TRR16")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_TRR16")
	.dwattr $C$DW$683, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$684, DW_AT_name("TRR17")
	.dwattr $C$DW$684, DW_AT_TI_symbol_name("_TRR17")
	.dwattr $C$DW$684, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$685, DW_AT_name("TRR18")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_TRR18")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$686, DW_AT_name("TRR19")
	.dwattr $C$DW$686, DW_AT_TI_symbol_name("_TRR19")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$687, DW_AT_name("TRR20")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_TRR20")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$688, DW_AT_name("TRR21")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_TRR21")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$689, DW_AT_name("TRR22")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_TRR22")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$690, DW_AT_name("TRR23")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_TRR23")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$691, DW_AT_name("TRR24")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_TRR24")
	.dwattr $C$DW$691, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$692, DW_AT_name("TRR25")
	.dwattr $C$DW$692, DW_AT_TI_symbol_name("_TRR25")
	.dwattr $C$DW$692, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$693, DW_AT_name("TRR26")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_TRR26")
	.dwattr $C$DW$693, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$694, DW_AT_name("TRR27")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_TRR27")
	.dwattr $C$DW$694, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$695, DW_AT_name("TRR28")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_TRR28")
	.dwattr $C$DW$695, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$696, DW_AT_name("TRR29")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_TRR29")
	.dwattr $C$DW$696, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$697, DW_AT_name("TRR30")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_TRR30")
	.dwattr $C$DW$697, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$698, DW_AT_name("TRR31")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_TRR31")
	.dwattr $C$DW$698, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$84


$C$DW$T$85	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$85, DW_AT_name("CANTRR_REG")
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x02)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$699, DW_AT_name("all")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$700, DW_AT_name("bit")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$85


$C$DW$T$86	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$86, DW_AT_name("CANTRS_BITS")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x02)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$701, DW_AT_name("TRS0")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_TRS0")
	.dwattr $C$DW$701, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$702, DW_AT_name("TRS1")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_TRS1")
	.dwattr $C$DW$702, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$703, DW_AT_name("TRS2")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_TRS2")
	.dwattr $C$DW$703, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$704, DW_AT_name("TRS3")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_TRS3")
	.dwattr $C$DW$704, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$705, DW_AT_name("TRS4")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_TRS4")
	.dwattr $C$DW$705, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$706, DW_AT_name("TRS5")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_TRS5")
	.dwattr $C$DW$706, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$707, DW_AT_name("TRS6")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_TRS6")
	.dwattr $C$DW$707, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$708, DW_AT_name("TRS7")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_TRS7")
	.dwattr $C$DW$708, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$709, DW_AT_name("TRS8")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_TRS8")
	.dwattr $C$DW$709, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$710, DW_AT_name("TRS9")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_TRS9")
	.dwattr $C$DW$710, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$711, DW_AT_name("TRS10")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_TRS10")
	.dwattr $C$DW$711, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$712, DW_AT_name("TRS11")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_TRS11")
	.dwattr $C$DW$712, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$713, DW_AT_name("TRS12")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_TRS12")
	.dwattr $C$DW$713, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$714, DW_AT_name("TRS13")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_TRS13")
	.dwattr $C$DW$714, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$715, DW_AT_name("TRS14")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_TRS14")
	.dwattr $C$DW$715, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$716, DW_AT_name("TRS15")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_TRS15")
	.dwattr $C$DW$716, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$717, DW_AT_name("TRS16")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_TRS16")
	.dwattr $C$DW$717, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$718, DW_AT_name("TRS17")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_TRS17")
	.dwattr $C$DW$718, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$719, DW_AT_name("TRS18")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_TRS18")
	.dwattr $C$DW$719, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$720, DW_AT_name("TRS19")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_TRS19")
	.dwattr $C$DW$720, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$721, DW_AT_name("TRS20")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_TRS20")
	.dwattr $C$DW$721, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$722, DW_AT_name("TRS21")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_TRS21")
	.dwattr $C$DW$722, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$723, DW_AT_name("TRS22")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_TRS22")
	.dwattr $C$DW$723, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$724, DW_AT_name("TRS23")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_TRS23")
	.dwattr $C$DW$724, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$725, DW_AT_name("TRS24")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_TRS24")
	.dwattr $C$DW$725, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$726, DW_AT_name("TRS25")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_TRS25")
	.dwattr $C$DW$726, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$727, DW_AT_name("TRS26")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_TRS26")
	.dwattr $C$DW$727, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$728, DW_AT_name("TRS27")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_TRS27")
	.dwattr $C$DW$728, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$729, DW_AT_name("TRS28")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_TRS28")
	.dwattr $C$DW$729, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$730, DW_AT_name("TRS29")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_TRS29")
	.dwattr $C$DW$730, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$731, DW_AT_name("TRS30")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_TRS30")
	.dwattr $C$DW$731, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$732, DW_AT_name("TRS31")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_TRS31")
	.dwattr $C$DW$732, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$86


$C$DW$T$87	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$87, DW_AT_name("CANTRS_REG")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x02)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$733, DW_AT_name("all")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$734, DW_AT_name("bit")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$87


$C$DW$T$88	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$88, DW_AT_name("ECAN_MBOXES")
	.dwattr $C$DW$T$88, DW_AT_byte_size(0x100)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$735, DW_AT_name("MBOX0")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_MBOX0")
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$736, DW_AT_name("MBOX1")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_MBOX1")
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$737, DW_AT_name("MBOX2")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_MBOX2")
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$738, DW_AT_name("MBOX3")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_MBOX3")
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$739, DW_AT_name("MBOX4")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_MBOX4")
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$740, DW_AT_name("MBOX5")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_MBOX5")
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$741, DW_AT_name("MBOX6")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_MBOX6")
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$742, DW_AT_name("MBOX7")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_MBOX7")
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$743, DW_AT_name("MBOX8")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_MBOX8")
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$744, DW_AT_name("MBOX9")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_MBOX9")
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$745, DW_AT_name("MBOX10")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_MBOX10")
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$746, DW_AT_name("MBOX11")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_MBOX11")
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$747, DW_AT_name("MBOX12")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_MBOX12")
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$748, DW_AT_name("MBOX13")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_MBOX13")
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$749, DW_AT_name("MBOX14")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_MBOX14")
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$750, DW_AT_name("MBOX15")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_MBOX15")
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$751, DW_AT_name("MBOX16")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_MBOX16")
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$752, DW_AT_name("MBOX17")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_MBOX17")
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x88]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$753, DW_AT_name("MBOX18")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_MBOX18")
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x90]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$754, DW_AT_name("MBOX19")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_MBOX19")
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x98]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$755, DW_AT_name("MBOX20")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_MBOX20")
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0xa0]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$756, DW_AT_name("MBOX21")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_MBOX21")
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$757, DW_AT_name("MBOX22")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_MBOX22")
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$758, DW_AT_name("MBOX23")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_MBOX23")
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0xb8]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$759, DW_AT_name("MBOX24")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_MBOX24")
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$760, DW_AT_name("MBOX25")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_MBOX25")
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$761, DW_AT_name("MBOX26")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_MBOX26")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$762, DW_AT_name("MBOX27")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_MBOX27")
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0xd8]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$763, DW_AT_name("MBOX28")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_MBOX28")
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0xe0]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$764, DW_AT_name("MBOX29")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_MBOX29")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0xe8]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$765, DW_AT_name("MBOX30")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_MBOX30")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$766, DW_AT_name("MBOX31")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_MBOX31")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$88

$C$DW$767	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$88)
$C$DW$T$225	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$225, DW_AT_type(*$C$DW$767)

$C$DW$T$89	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$89, DW_AT_name("ECAN_REGS")
	.dwattr $C$DW$T$89, DW_AT_byte_size(0x34)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$768, DW_AT_name("CANME")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_CANME")
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$769, DW_AT_name("CANMD")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_CANMD")
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$770, DW_AT_name("CANTRS")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_CANTRS")
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$771, DW_AT_name("CANTRR")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_CANTRR")
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$772, DW_AT_name("CANTA")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_CANTA")
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$773, DW_AT_name("CANAA")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_CANAA")
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$774, DW_AT_name("CANRMP")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_CANRMP")
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$775, DW_AT_name("CANRML")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_CANRML")
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$776, DW_AT_name("CANRFP")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_CANRFP")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$777, DW_AT_name("CANGAM")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_CANGAM")
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$778, DW_AT_name("CANMC")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_CANMC")
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$779, DW_AT_name("CANBTC")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_CANBTC")
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$780, DW_AT_name("CANES")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_CANES")
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$781, DW_AT_name("CANTEC")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_CANTEC")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$782, DW_AT_name("CANREC")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_CANREC")
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$783, DW_AT_name("CANGIF0")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_CANGIF0")
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$784, DW_AT_name("CANGIM")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_CANGIM")
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$785, DW_AT_name("CANGIF1")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_CANGIF1")
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$786, DW_AT_name("CANMIM")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_CANMIM")
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$787, DW_AT_name("CANMIL")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_CANMIL")
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$788, DW_AT_name("CANOPC")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_CANOPC")
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$789, DW_AT_name("CANTIOC")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_CANTIOC")
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$790, DW_AT_name("CANRIOC")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_CANRIOC")
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$791, DW_AT_name("CANTSC")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_CANTSC")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$792, DW_AT_name("CANTOC")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_CANTOC")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$793, DW_AT_name("CANTOS")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_CANTOS")
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$89

$C$DW$794	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$89)
$C$DW$T$226	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$226, DW_AT_type(*$C$DW$794)

$C$DW$T$97	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$97, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$97, DW_AT_byte_size(0x08)
$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$795, DW_AT_name("wListElem")
	.dwattr $C$DW$795, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$796	.dwtag  DW_TAG_member
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$796, DW_AT_name("wCount")
	.dwattr $C$DW$796, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$796, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$796, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$797, DW_AT_name("fxn")
	.dwattr $C$DW$797, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$97

$C$DW$T$119	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)
$C$DW$T$92	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$92, DW_AT_address_class(0x16)
$C$DW$T$93	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_language(DW_LANG_C)

$C$DW$T$98	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$98, DW_AT_name("LAM_REGS")
	.dwattr $C$DW$T$98, DW_AT_byte_size(0x40)
$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$798, DW_AT_name("LAM0")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_LAM0")
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$799, DW_AT_name("LAM1")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_LAM1")
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$800, DW_AT_name("LAM2")
	.dwattr $C$DW$800, DW_AT_TI_symbol_name("_LAM2")
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$801, DW_AT_name("LAM3")
	.dwattr $C$DW$801, DW_AT_TI_symbol_name("_LAM3")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$802, DW_AT_name("LAM4")
	.dwattr $C$DW$802, DW_AT_TI_symbol_name("_LAM4")
	.dwattr $C$DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$803	.dwtag  DW_TAG_member
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$803, DW_AT_name("LAM5")
	.dwattr $C$DW$803, DW_AT_TI_symbol_name("_LAM5")
	.dwattr $C$DW$803, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$803, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$804	.dwtag  DW_TAG_member
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$804, DW_AT_name("LAM6")
	.dwattr $C$DW$804, DW_AT_TI_symbol_name("_LAM6")
	.dwattr $C$DW$804, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$804, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$805	.dwtag  DW_TAG_member
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$805, DW_AT_name("LAM7")
	.dwattr $C$DW$805, DW_AT_TI_symbol_name("_LAM7")
	.dwattr $C$DW$805, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$805, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$806	.dwtag  DW_TAG_member
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$806, DW_AT_name("LAM8")
	.dwattr $C$DW$806, DW_AT_TI_symbol_name("_LAM8")
	.dwattr $C$DW$806, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$806, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$807	.dwtag  DW_TAG_member
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$807, DW_AT_name("LAM9")
	.dwattr $C$DW$807, DW_AT_TI_symbol_name("_LAM9")
	.dwattr $C$DW$807, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$807, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$808	.dwtag  DW_TAG_member
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$808, DW_AT_name("LAM10")
	.dwattr $C$DW$808, DW_AT_TI_symbol_name("_LAM10")
	.dwattr $C$DW$808, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$808, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$809	.dwtag  DW_TAG_member
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$809, DW_AT_name("LAM11")
	.dwattr $C$DW$809, DW_AT_TI_symbol_name("_LAM11")
	.dwattr $C$DW$809, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$809, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$810	.dwtag  DW_TAG_member
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$810, DW_AT_name("LAM12")
	.dwattr $C$DW$810, DW_AT_TI_symbol_name("_LAM12")
	.dwattr $C$DW$810, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$810, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$811	.dwtag  DW_TAG_member
	.dwattr $C$DW$811, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$811, DW_AT_name("LAM13")
	.dwattr $C$DW$811, DW_AT_TI_symbol_name("_LAM13")
	.dwattr $C$DW$811, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$811, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$812	.dwtag  DW_TAG_member
	.dwattr $C$DW$812, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$812, DW_AT_name("LAM14")
	.dwattr $C$DW$812, DW_AT_TI_symbol_name("_LAM14")
	.dwattr $C$DW$812, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$812, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$813	.dwtag  DW_TAG_member
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$813, DW_AT_name("LAM15")
	.dwattr $C$DW$813, DW_AT_TI_symbol_name("_LAM15")
	.dwattr $C$DW$813, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$813, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$814	.dwtag  DW_TAG_member
	.dwattr $C$DW$814, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$814, DW_AT_name("LAM16")
	.dwattr $C$DW$814, DW_AT_TI_symbol_name("_LAM16")
	.dwattr $C$DW$814, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$814, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$815	.dwtag  DW_TAG_member
	.dwattr $C$DW$815, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$815, DW_AT_name("LAM17")
	.dwattr $C$DW$815, DW_AT_TI_symbol_name("_LAM17")
	.dwattr $C$DW$815, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$815, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$816	.dwtag  DW_TAG_member
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$816, DW_AT_name("LAM18")
	.dwattr $C$DW$816, DW_AT_TI_symbol_name("_LAM18")
	.dwattr $C$DW$816, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$816, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$817	.dwtag  DW_TAG_member
	.dwattr $C$DW$817, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$817, DW_AT_name("LAM19")
	.dwattr $C$DW$817, DW_AT_TI_symbol_name("_LAM19")
	.dwattr $C$DW$817, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$817, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$818	.dwtag  DW_TAG_member
	.dwattr $C$DW$818, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$818, DW_AT_name("LAM20")
	.dwattr $C$DW$818, DW_AT_TI_symbol_name("_LAM20")
	.dwattr $C$DW$818, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$818, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$819	.dwtag  DW_TAG_member
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$819, DW_AT_name("LAM21")
	.dwattr $C$DW$819, DW_AT_TI_symbol_name("_LAM21")
	.dwattr $C$DW$819, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$819, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$820	.dwtag  DW_TAG_member
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$820, DW_AT_name("LAM22")
	.dwattr $C$DW$820, DW_AT_TI_symbol_name("_LAM22")
	.dwattr $C$DW$820, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$820, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$821	.dwtag  DW_TAG_member
	.dwattr $C$DW$821, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$821, DW_AT_name("LAM23")
	.dwattr $C$DW$821, DW_AT_TI_symbol_name("_LAM23")
	.dwattr $C$DW$821, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$821, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$822	.dwtag  DW_TAG_member
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$822, DW_AT_name("LAM24")
	.dwattr $C$DW$822, DW_AT_TI_symbol_name("_LAM24")
	.dwattr $C$DW$822, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$822, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$823	.dwtag  DW_TAG_member
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$823, DW_AT_name("LAM25")
	.dwattr $C$DW$823, DW_AT_TI_symbol_name("_LAM25")
	.dwattr $C$DW$823, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$823, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$824	.dwtag  DW_TAG_member
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$824, DW_AT_name("LAM26")
	.dwattr $C$DW$824, DW_AT_TI_symbol_name("_LAM26")
	.dwattr $C$DW$824, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$824, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$825	.dwtag  DW_TAG_member
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$825, DW_AT_name("LAM27")
	.dwattr $C$DW$825, DW_AT_TI_symbol_name("_LAM27")
	.dwattr $C$DW$825, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$825, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$826	.dwtag  DW_TAG_member
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$826, DW_AT_name("LAM28")
	.dwattr $C$DW$826, DW_AT_TI_symbol_name("_LAM28")
	.dwattr $C$DW$826, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$826, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$827	.dwtag  DW_TAG_member
	.dwattr $C$DW$827, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$827, DW_AT_name("LAM29")
	.dwattr $C$DW$827, DW_AT_TI_symbol_name("_LAM29")
	.dwattr $C$DW$827, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$827, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$828	.dwtag  DW_TAG_member
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$828, DW_AT_name("LAM30")
	.dwattr $C$DW$828, DW_AT_TI_symbol_name("_LAM30")
	.dwattr $C$DW$828, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$828, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$829	.dwtag  DW_TAG_member
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$829, DW_AT_name("LAM31")
	.dwattr $C$DW$829, DW_AT_TI_symbol_name("_LAM31")
	.dwattr $C$DW$829, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$829, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$98

$C$DW$830	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$830, DW_AT_type(*$C$DW$T$98)
$C$DW$T$227	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$227, DW_AT_type(*$C$DW$830)

$C$DW$T$99	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$99, DW_AT_name("MBOX")
	.dwattr $C$DW$T$99, DW_AT_byte_size(0x08)
$C$DW$831	.dwtag  DW_TAG_member
	.dwattr $C$DW$831, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$831, DW_AT_name("MSGID")
	.dwattr $C$DW$831, DW_AT_TI_symbol_name("_MSGID")
	.dwattr $C$DW$831, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$831, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$832	.dwtag  DW_TAG_member
	.dwattr $C$DW$832, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$832, DW_AT_name("MSGCTRL")
	.dwattr $C$DW$832, DW_AT_TI_symbol_name("_MSGCTRL")
	.dwattr $C$DW$832, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$832, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$833	.dwtag  DW_TAG_member
	.dwattr $C$DW$833, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$833, DW_AT_name("MDL")
	.dwattr $C$DW$833, DW_AT_TI_symbol_name("_MDL")
	.dwattr $C$DW$833, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$833, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$834	.dwtag  DW_TAG_member
	.dwattr $C$DW$834, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$834, DW_AT_name("MDH")
	.dwattr $C$DW$834, DW_AT_TI_symbol_name("_MDH")
	.dwattr $C$DW$834, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$834, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$99

$C$DW$835	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$835, DW_AT_type(*$C$DW$T$99)
$C$DW$T$228	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$228, DW_AT_type(*$C$DW$835)
$C$DW$T$229	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$229, DW_AT_type(*$C$DW$T$228)
	.dwattr $C$DW$T$229, DW_AT_address_class(0x16)

$C$DW$T$106	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$106, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$106, DW_AT_byte_size(0x30)
$C$DW$836	.dwtag  DW_TAG_member
	.dwattr $C$DW$836, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$836, DW_AT_name("dataQue")
	.dwattr $C$DW$836, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$836, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$836, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$837	.dwtag  DW_TAG_member
	.dwattr $C$DW$837, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$837, DW_AT_name("freeQue")
	.dwattr $C$DW$837, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$837, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$837, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$838	.dwtag  DW_TAG_member
	.dwattr $C$DW$838, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$838, DW_AT_name("dataSem")
	.dwattr $C$DW$838, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$838, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$838, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$839	.dwtag  DW_TAG_member
	.dwattr $C$DW$839, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$839, DW_AT_name("freeSem")
	.dwattr $C$DW$839, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$839, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$839, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$840	.dwtag  DW_TAG_member
	.dwattr $C$DW$840, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$840, DW_AT_name("segid")
	.dwattr $C$DW$840, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$840, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$840, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$841	.dwtag  DW_TAG_member
	.dwattr $C$DW$841, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$841, DW_AT_name("size")
	.dwattr $C$DW$841, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$841, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$841, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$842	.dwtag  DW_TAG_member
	.dwattr $C$DW$842, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$842, DW_AT_name("length")
	.dwattr $C$DW$842, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$842, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$842, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$843	.dwtag  DW_TAG_member
	.dwattr $C$DW$843, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$843, DW_AT_name("name")
	.dwattr $C$DW$843, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$843, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$843, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$106

$C$DW$T$230	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$230, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$230, DW_AT_language(DW_LANG_C)
$C$DW$T$232	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$232, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$232, DW_AT_address_class(0x16)
$C$DW$T$233	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$233, DW_AT_type(*$C$DW$T$232)
	.dwattr $C$DW$T$233, DW_AT_language(DW_LANG_C)

$C$DW$T$107	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$107, DW_AT_name("MOTO_REGS")
	.dwattr $C$DW$T$107, DW_AT_byte_size(0x40)
$C$DW$844	.dwtag  DW_TAG_member
	.dwattr $C$DW$844, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$844, DW_AT_name("MOTO0")
	.dwattr $C$DW$844, DW_AT_TI_symbol_name("_MOTO0")
	.dwattr $C$DW$844, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$844, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$845	.dwtag  DW_TAG_member
	.dwattr $C$DW$845, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$845, DW_AT_name("MOTO1")
	.dwattr $C$DW$845, DW_AT_TI_symbol_name("_MOTO1")
	.dwattr $C$DW$845, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$845, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$846	.dwtag  DW_TAG_member
	.dwattr $C$DW$846, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$846, DW_AT_name("MOTO2")
	.dwattr $C$DW$846, DW_AT_TI_symbol_name("_MOTO2")
	.dwattr $C$DW$846, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$846, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$847	.dwtag  DW_TAG_member
	.dwattr $C$DW$847, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$847, DW_AT_name("MOTO3")
	.dwattr $C$DW$847, DW_AT_TI_symbol_name("_MOTO3")
	.dwattr $C$DW$847, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$847, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$848	.dwtag  DW_TAG_member
	.dwattr $C$DW$848, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$848, DW_AT_name("MOTO4")
	.dwattr $C$DW$848, DW_AT_TI_symbol_name("_MOTO4")
	.dwattr $C$DW$848, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$848, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$849	.dwtag  DW_TAG_member
	.dwattr $C$DW$849, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$849, DW_AT_name("MOTO5")
	.dwattr $C$DW$849, DW_AT_TI_symbol_name("_MOTO5")
	.dwattr $C$DW$849, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$849, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$850	.dwtag  DW_TAG_member
	.dwattr $C$DW$850, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$850, DW_AT_name("MOTO6")
	.dwattr $C$DW$850, DW_AT_TI_symbol_name("_MOTO6")
	.dwattr $C$DW$850, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$850, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$851, DW_AT_name("MOTO7")
	.dwattr $C$DW$851, DW_AT_TI_symbol_name("_MOTO7")
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$852, DW_AT_name("MOTO8")
	.dwattr $C$DW$852, DW_AT_TI_symbol_name("_MOTO8")
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$853, DW_AT_name("MOTO9")
	.dwattr $C$DW$853, DW_AT_TI_symbol_name("_MOTO9")
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$854, DW_AT_name("MOTO10")
	.dwattr $C$DW$854, DW_AT_TI_symbol_name("_MOTO10")
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$855, DW_AT_name("MOTO11")
	.dwattr $C$DW$855, DW_AT_TI_symbol_name("_MOTO11")
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$856, DW_AT_name("MOTO12")
	.dwattr $C$DW$856, DW_AT_TI_symbol_name("_MOTO12")
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$857	.dwtag  DW_TAG_member
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$857, DW_AT_name("MOTO13")
	.dwattr $C$DW$857, DW_AT_TI_symbol_name("_MOTO13")
	.dwattr $C$DW$857, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$857, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$858, DW_AT_name("MOTO14")
	.dwattr $C$DW$858, DW_AT_TI_symbol_name("_MOTO14")
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$859, DW_AT_name("MOTO15")
	.dwattr $C$DW$859, DW_AT_TI_symbol_name("_MOTO15")
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$860, DW_AT_name("MOTO16")
	.dwattr $C$DW$860, DW_AT_TI_symbol_name("_MOTO16")
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$861, DW_AT_name("MOTO17")
	.dwattr $C$DW$861, DW_AT_TI_symbol_name("_MOTO17")
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0x22]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$862, DW_AT_name("MOTO18")
	.dwattr $C$DW$862, DW_AT_TI_symbol_name("_MOTO18")
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$863, DW_AT_name("MOTO19")
	.dwattr $C$DW$863, DW_AT_TI_symbol_name("_MOTO19")
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0x26]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$864, DW_AT_name("MOTO20")
	.dwattr $C$DW$864, DW_AT_TI_symbol_name("_MOTO20")
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$865, DW_AT_name("MOTO21")
	.dwattr $C$DW$865, DW_AT_TI_symbol_name("_MOTO21")
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$866, DW_AT_name("MOTO22")
	.dwattr $C$DW$866, DW_AT_TI_symbol_name("_MOTO22")
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$867, DW_AT_name("MOTO23")
	.dwattr $C$DW$867, DW_AT_TI_symbol_name("_MOTO23")
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$868, DW_AT_name("MOTO24")
	.dwattr $C$DW$868, DW_AT_TI_symbol_name("_MOTO24")
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$869, DW_AT_name("MOTO25")
	.dwattr $C$DW$869, DW_AT_TI_symbol_name("_MOTO25")
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0x32]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$870, DW_AT_name("MOTO26")
	.dwattr $C$DW$870, DW_AT_TI_symbol_name("_MOTO26")
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$871, DW_AT_name("MOTO27")
	.dwattr $C$DW$871, DW_AT_TI_symbol_name("_MOTO27")
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$872, DW_AT_name("MOTO28")
	.dwattr $C$DW$872, DW_AT_TI_symbol_name("_MOTO28")
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$873, DW_AT_name("MOTO29")
	.dwattr $C$DW$873, DW_AT_TI_symbol_name("_MOTO29")
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$874, DW_AT_name("MOTO30")
	.dwattr $C$DW$874, DW_AT_TI_symbol_name("_MOTO30")
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$875, DW_AT_name("MOTO31")
	.dwattr $C$DW$875, DW_AT_TI_symbol_name("_MOTO31")
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$107

$C$DW$876	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$107)
$C$DW$T$234	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$234, DW_AT_type(*$C$DW$876)

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0x01)
$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$877, DW_AT_name("ACK1")
	.dwattr $C$DW$877, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$877, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$878, DW_AT_name("ACK2")
	.dwattr $C$DW$878, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$878, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$879, DW_AT_name("ACK3")
	.dwattr $C$DW$879, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$879, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$880, DW_AT_name("ACK4")
	.dwattr $C$DW$880, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$880, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$881, DW_AT_name("ACK5")
	.dwattr $C$DW$881, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$881, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$882	.dwtag  DW_TAG_member
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$882, DW_AT_name("ACK6")
	.dwattr $C$DW$882, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$882, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$882, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$883	.dwtag  DW_TAG_member
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$883, DW_AT_name("ACK7")
	.dwattr $C$DW$883, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$883, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$883, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$883, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$884	.dwtag  DW_TAG_member
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$884, DW_AT_name("ACK8")
	.dwattr $C$DW$884, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$884, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$884, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$885	.dwtag  DW_TAG_member
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$885, DW_AT_name("ACK9")
	.dwattr $C$DW$885, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$885, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$885, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$885, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$886	.dwtag  DW_TAG_member
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$886, DW_AT_name("ACK10")
	.dwattr $C$DW$886, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$886, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$886, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$886, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$887	.dwtag  DW_TAG_member
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$887, DW_AT_name("ACK11")
	.dwattr $C$DW$887, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$887, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$887, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$887, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$888	.dwtag  DW_TAG_member
	.dwattr $C$DW$888, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$888, DW_AT_name("ACK12")
	.dwattr $C$DW$888, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$888, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$888, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$888, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$889	.dwtag  DW_TAG_member
	.dwattr $C$DW$889, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$889, DW_AT_name("rsvd1")
	.dwattr $C$DW$889, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$889, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$889, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$889, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$108


$C$DW$T$109	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$109, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$109, DW_AT_byte_size(0x01)
$C$DW$890	.dwtag  DW_TAG_member
	.dwattr $C$DW$890, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$890, DW_AT_name("all")
	.dwattr $C$DW$890, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$890, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$890, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$891	.dwtag  DW_TAG_member
	.dwattr $C$DW$891, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$891, DW_AT_name("bit")
	.dwattr $C$DW$891, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$891, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$891, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$109


$C$DW$T$110	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$110, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x01)
$C$DW$892	.dwtag  DW_TAG_member
	.dwattr $C$DW$892, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$892, DW_AT_name("ENPIE")
	.dwattr $C$DW$892, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$892, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$892, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$892, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$893	.dwtag  DW_TAG_member
	.dwattr $C$DW$893, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$893, DW_AT_name("PIEVECT")
	.dwattr $C$DW$893, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$893, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$893, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$893, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$110


$C$DW$T$111	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$111, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$111, DW_AT_byte_size(0x01)
$C$DW$894	.dwtag  DW_TAG_member
	.dwattr $C$DW$894, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$894, DW_AT_name("all")
	.dwattr $C$DW$894, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$894, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$894, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$895	.dwtag  DW_TAG_member
	.dwattr $C$DW$895, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$895, DW_AT_name("bit")
	.dwattr $C$DW$895, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$895, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$895, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$111


$C$DW$T$112	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$112, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x01)
$C$DW$896	.dwtag  DW_TAG_member
	.dwattr $C$DW$896, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$896, DW_AT_name("INTx1")
	.dwattr $C$DW$896, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$896, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$896, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$896, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$897	.dwtag  DW_TAG_member
	.dwattr $C$DW$897, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$897, DW_AT_name("INTx2")
	.dwattr $C$DW$897, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$897, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$897, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$897, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$898	.dwtag  DW_TAG_member
	.dwattr $C$DW$898, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$898, DW_AT_name("INTx3")
	.dwattr $C$DW$898, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$898, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$898, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$898, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$899	.dwtag  DW_TAG_member
	.dwattr $C$DW$899, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$899, DW_AT_name("INTx4")
	.dwattr $C$DW$899, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$899, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$899, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$899, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$900	.dwtag  DW_TAG_member
	.dwattr $C$DW$900, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$900, DW_AT_name("INTx5")
	.dwattr $C$DW$900, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$900, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$900, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$900, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$901	.dwtag  DW_TAG_member
	.dwattr $C$DW$901, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$901, DW_AT_name("INTx6")
	.dwattr $C$DW$901, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$901, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$901, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$901, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$902	.dwtag  DW_TAG_member
	.dwattr $C$DW$902, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$902, DW_AT_name("INTx7")
	.dwattr $C$DW$902, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$902, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$902, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$902, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$903	.dwtag  DW_TAG_member
	.dwattr $C$DW$903, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$903, DW_AT_name("INTx8")
	.dwattr $C$DW$903, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$903, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$903, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$903, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$904	.dwtag  DW_TAG_member
	.dwattr $C$DW$904, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$904, DW_AT_name("rsvd1")
	.dwattr $C$DW$904, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$904, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$904, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$904, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$112


$C$DW$T$113	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$113, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$113, DW_AT_byte_size(0x01)
$C$DW$905	.dwtag  DW_TAG_member
	.dwattr $C$DW$905, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$905, DW_AT_name("all")
	.dwattr $C$DW$905, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$905, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$905, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$906	.dwtag  DW_TAG_member
	.dwattr $C$DW$906, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$906, DW_AT_name("bit")
	.dwattr $C$DW$906, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$906, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$906, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$113


$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x01)
$C$DW$907	.dwtag  DW_TAG_member
	.dwattr $C$DW$907, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$907, DW_AT_name("INTx1")
	.dwattr $C$DW$907, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$907, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$907, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$907, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$908	.dwtag  DW_TAG_member
	.dwattr $C$DW$908, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$908, DW_AT_name("INTx2")
	.dwattr $C$DW$908, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$908, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$908, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$908, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$909	.dwtag  DW_TAG_member
	.dwattr $C$DW$909, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$909, DW_AT_name("INTx3")
	.dwattr $C$DW$909, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$909, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$909, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$909, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$910	.dwtag  DW_TAG_member
	.dwattr $C$DW$910, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$910, DW_AT_name("INTx4")
	.dwattr $C$DW$910, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$910, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$910, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$910, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$911	.dwtag  DW_TAG_member
	.dwattr $C$DW$911, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$911, DW_AT_name("INTx5")
	.dwattr $C$DW$911, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$911, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$911, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$911, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$912	.dwtag  DW_TAG_member
	.dwattr $C$DW$912, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$912, DW_AT_name("INTx6")
	.dwattr $C$DW$912, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$912, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$912, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$912, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$913	.dwtag  DW_TAG_member
	.dwattr $C$DW$913, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$913, DW_AT_name("INTx7")
	.dwattr $C$DW$913, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$913, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$913, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$913, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$914	.dwtag  DW_TAG_member
	.dwattr $C$DW$914, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$914, DW_AT_name("INTx8")
	.dwattr $C$DW$914, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$914, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$914, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$914, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$915	.dwtag  DW_TAG_member
	.dwattr $C$DW$915, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$915, DW_AT_name("rsvd1")
	.dwattr $C$DW$915, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$915, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$915, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$915, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$114


$C$DW$T$115	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$115, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$115, DW_AT_byte_size(0x01)
$C$DW$916	.dwtag  DW_TAG_member
	.dwattr $C$DW$916, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$916, DW_AT_name("all")
	.dwattr $C$DW$916, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$916, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$916, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$917	.dwtag  DW_TAG_member
	.dwattr $C$DW$917, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$917, DW_AT_name("bit")
	.dwattr $C$DW$917, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$917, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$917, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$115


$C$DW$T$116	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$116, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$116, DW_AT_byte_size(0x1a)
$C$DW$918	.dwtag  DW_TAG_member
	.dwattr $C$DW$918, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$918, DW_AT_name("PIECTRL")
	.dwattr $C$DW$918, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$918, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$918, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$919	.dwtag  DW_TAG_member
	.dwattr $C$DW$919, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$919, DW_AT_name("PIEACK")
	.dwattr $C$DW$919, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$919, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$919, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$920	.dwtag  DW_TAG_member
	.dwattr $C$DW$920, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$920, DW_AT_name("PIEIER1")
	.dwattr $C$DW$920, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$920, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$920, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$921	.dwtag  DW_TAG_member
	.dwattr $C$DW$921, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$921, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$921, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$921, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$921, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$922	.dwtag  DW_TAG_member
	.dwattr $C$DW$922, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$922, DW_AT_name("PIEIER2")
	.dwattr $C$DW$922, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$922, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$922, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$923	.dwtag  DW_TAG_member
	.dwattr $C$DW$923, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$923, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$923, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$923, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$923, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$924	.dwtag  DW_TAG_member
	.dwattr $C$DW$924, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$924, DW_AT_name("PIEIER3")
	.dwattr $C$DW$924, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$924, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$924, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$925	.dwtag  DW_TAG_member
	.dwattr $C$DW$925, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$925, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$925, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$925, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$925, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$926	.dwtag  DW_TAG_member
	.dwattr $C$DW$926, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$926, DW_AT_name("PIEIER4")
	.dwattr $C$DW$926, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$926, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$926, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$927	.dwtag  DW_TAG_member
	.dwattr $C$DW$927, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$927, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$927, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$927, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$927, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$928	.dwtag  DW_TAG_member
	.dwattr $C$DW$928, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$928, DW_AT_name("PIEIER5")
	.dwattr $C$DW$928, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$928, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$928, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$929	.dwtag  DW_TAG_member
	.dwattr $C$DW$929, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$929, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$929, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$929, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$929, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$930	.dwtag  DW_TAG_member
	.dwattr $C$DW$930, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$930, DW_AT_name("PIEIER6")
	.dwattr $C$DW$930, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$930, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$930, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$931	.dwtag  DW_TAG_member
	.dwattr $C$DW$931, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$931, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$931, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$931, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$931, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$932	.dwtag  DW_TAG_member
	.dwattr $C$DW$932, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$932, DW_AT_name("PIEIER7")
	.dwattr $C$DW$932, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$932, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$932, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$933	.dwtag  DW_TAG_member
	.dwattr $C$DW$933, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$933, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$933, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$933, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$933, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$934	.dwtag  DW_TAG_member
	.dwattr $C$DW$934, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$934, DW_AT_name("PIEIER8")
	.dwattr $C$DW$934, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$934, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$934, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$935	.dwtag  DW_TAG_member
	.dwattr $C$DW$935, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$935, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$935, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$935, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$935, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$936	.dwtag  DW_TAG_member
	.dwattr $C$DW$936, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$936, DW_AT_name("PIEIER9")
	.dwattr $C$DW$936, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$936, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$936, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$937	.dwtag  DW_TAG_member
	.dwattr $C$DW$937, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$937, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$937, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$937, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$937, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$938	.dwtag  DW_TAG_member
	.dwattr $C$DW$938, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$938, DW_AT_name("PIEIER10")
	.dwattr $C$DW$938, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$938, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$938, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$939	.dwtag  DW_TAG_member
	.dwattr $C$DW$939, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$939, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$939, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$939, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$939, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$940	.dwtag  DW_TAG_member
	.dwattr $C$DW$940, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$940, DW_AT_name("PIEIER11")
	.dwattr $C$DW$940, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$940, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$940, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$941	.dwtag  DW_TAG_member
	.dwattr $C$DW$941, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$941, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$941, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$941, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$941, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$942	.dwtag  DW_TAG_member
	.dwattr $C$DW$942, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$942, DW_AT_name("PIEIER12")
	.dwattr $C$DW$942, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$942, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$942, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$943	.dwtag  DW_TAG_member
	.dwattr $C$DW$943, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$943, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$943, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$943, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$943, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$116

$C$DW$944	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$944, DW_AT_type(*$C$DW$T$116)
$C$DW$T$239	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$239, DW_AT_type(*$C$DW$944)

$C$DW$T$118	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$118, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$118, DW_AT_byte_size(0x04)
$C$DW$945	.dwtag  DW_TAG_member
	.dwattr $C$DW$945, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$945, DW_AT_name("next")
	.dwattr $C$DW$945, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$945, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$945, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$946	.dwtag  DW_TAG_member
	.dwattr $C$DW$946, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$946, DW_AT_name("prev")
	.dwattr $C$DW$946, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$946, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$946, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$118

$C$DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$T$117	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$117, DW_AT_address_class(0x16)

$C$DW$T$120	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$120, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$120, DW_AT_byte_size(0x10)
$C$DW$947	.dwtag  DW_TAG_member
	.dwattr $C$DW$947, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$947, DW_AT_name("job")
	.dwattr $C$DW$947, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$947, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$947, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$948	.dwtag  DW_TAG_member
	.dwattr $C$DW$948, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$948, DW_AT_name("count")
	.dwattr $C$DW$948, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$948, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$948, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$949	.dwtag  DW_TAG_member
	.dwattr $C$DW$949, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$949, DW_AT_name("pendQ")
	.dwattr $C$DW$949, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$949, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$949, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$950	.dwtag  DW_TAG_member
	.dwattr $C$DW$950, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$950, DW_AT_name("name")
	.dwattr $C$DW$950, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$950, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$950, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$120

$C$DW$T$100	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$T$241	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$241, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$241, DW_AT_address_class(0x16)
$C$DW$T$242	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$242, DW_AT_type(*$C$DW$T$241)
	.dwattr $C$DW$T$242, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$243	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$243, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$243, DW_AT_language(DW_LANG_C)

$C$DW$T$94	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)
$C$DW$951	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$951, DW_AT_type(*$C$DW$T$93)
	.dwendtag $C$DW$T$94

$C$DW$T$95	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_address_class(0x16)
$C$DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)

$C$DW$T$145	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$145, DW_AT_language(DW_LANG_C)
$C$DW$952	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$952, DW_AT_type(*$C$DW$T$144)
	.dwendtag $C$DW$T$145

$C$DW$T$146	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$T$146, DW_AT_address_class(0x16)
$C$DW$T$147	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$147, DW_AT_language(DW_LANG_C)
$C$DW$T$149	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$149, DW_AT_language(DW_LANG_C)
$C$DW$T$159	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$159, DW_AT_language(DW_LANG_C)
$C$DW$T$148	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$148, DW_AT_language(DW_LANG_C)
$C$DW$T$158	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$158, DW_AT_language(DW_LANG_C)
$C$DW$T$150	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$T$150, DW_AT_language(DW_LANG_C)

$C$DW$T$154	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$154, DW_AT_language(DW_LANG_C)
$C$DW$953	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$953, DW_AT_type(*$C$DW$T$144)
$C$DW$954	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$954, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$154

$C$DW$T$155	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$T$155, DW_AT_address_class(0x16)
$C$DW$T$187	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$187, DW_AT_language(DW_LANG_C)
$C$DW$T$156	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$156, DW_AT_language(DW_LANG_C)
$C$DW$T$160	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$160, DW_AT_language(DW_LANG_C)

$C$DW$T$178	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$178, DW_AT_language(DW_LANG_C)
$C$DW$955	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$955, DW_AT_type(*$C$DW$T$144)
$C$DW$956	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$956, DW_AT_type(*$C$DW$T$6)
$C$DW$957	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$957, DW_AT_type(*$C$DW$T$9)
$C$DW$958	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$958, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$178

$C$DW$T$179	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$179, DW_AT_type(*$C$DW$T$178)
	.dwattr $C$DW$T$179, DW_AT_address_class(0x16)
$C$DW$T$180	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$180, DW_AT_type(*$C$DW$T$179)
	.dwattr $C$DW$T$180, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$161	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$161, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$161, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$959	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$959, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$122	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$122, DW_AT_address_class(0x16)
$C$DW$960	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$960, DW_AT_type(*$C$DW$T$6)
$C$DW$T$133	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$960)
$C$DW$T$134	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$T$134, DW_AT_address_class(0x16)
$C$DW$T$181	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$181, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$181, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$152	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$152, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$152, DW_AT_address_class(0x16)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$249	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$249, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$249, DW_AT_language(DW_LANG_C)
$C$DW$961	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$961, DW_AT_type(*$C$DW$T$9)
$C$DW$T$131	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$961)
$C$DW$T$132	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$T$132, DW_AT_address_class(0x16)
$C$DW$T$153	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$153, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$91	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)
$C$DW$962	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$962, DW_AT_type(*$C$DW$T$91)
$C$DW$T$254	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$254, DW_AT_type(*$C$DW$962)
$C$DW$T$255	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$255, DW_AT_type(*$C$DW$T$254)
	.dwattr $C$DW$T$255, DW_AT_address_class(0x16)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$963	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$963, DW_AT_type(*$C$DW$T$26)
$C$DW$T$262	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$262, DW_AT_type(*$C$DW$963)
$C$DW$T$263	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$263, DW_AT_type(*$C$DW$T$262)
	.dwattr $C$DW$T$263, DW_AT_address_class(0x16)

$C$DW$T$135	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$135, DW_AT_language(DW_LANG_C)
$C$DW$964	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$964, DW_AT_type(*$C$DW$T$6)
$C$DW$965	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$965, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$135

$C$DW$T$136	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$136, DW_AT_address_class(0x16)
$C$DW$T$137	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$T$137, DW_AT_language(DW_LANG_C)
$C$DW$T$151	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$151, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$151, DW_AT_address_class(0x16)

$C$DW$T$162	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$162, DW_AT_language(DW_LANG_C)
$C$DW$966	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$966, DW_AT_type(*$C$DW$T$144)
$C$DW$967	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$967, DW_AT_type(*$C$DW$T$125)
$C$DW$968	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$968, DW_AT_type(*$C$DW$T$6)
$C$DW$969	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$969, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$162

$C$DW$T$163	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$163, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$163, DW_AT_address_class(0x16)
$C$DW$T$164	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$163)
	.dwattr $C$DW$T$164, DW_AT_language(DW_LANG_C)
$C$DW$970	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$970, DW_AT_type(*$C$DW$T$164)
$C$DW$T$165	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$165, DW_AT_type(*$C$DW$970)
$C$DW$T$166	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$166, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$T$166, DW_AT_address_class(0x16)
$C$DW$T$167	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$T$166)
	.dwattr $C$DW$T$167, DW_AT_address_class(0x16)

$C$DW$T$171	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$171, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$171, DW_AT_language(DW_LANG_C)
$C$DW$971	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$971, DW_AT_type(*$C$DW$T$144)
$C$DW$972	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$972, DW_AT_type(*$C$DW$T$9)
$C$DW$973	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$973, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$171

$C$DW$T$172	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$172, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$T$172, DW_AT_address_class(0x16)
$C$DW$T$173	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$173, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$T$173, DW_AT_language(DW_LANG_C)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$104	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$104, DW_AT_address_class(0x16)
$C$DW$T$105	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_language(DW_LANG_C)

$C$DW$T$174	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$174, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$174, DW_AT_byte_size(0x01)
$C$DW$974	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$975	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$174

$C$DW$T$175	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$T$175, DW_AT_language(DW_LANG_C)

$C$DW$T$140	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$140, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x01)
$C$DW$976	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$977	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$978	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$979	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$980	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$981	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$982	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$983	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$140

$C$DW$T$141	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$T$141, DW_AT_language(DW_LANG_C)

$C$DW$T$157	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$157, DW_AT_byte_size(0x80)
$C$DW$984	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$984, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$157


$C$DW$T$121	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$121, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$121, DW_AT_byte_size(0x06)
$C$DW$985	.dwtag  DW_TAG_member
	.dwattr $C$DW$985, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$985, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$985, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$985, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$985, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$986	.dwtag  DW_TAG_member
	.dwattr $C$DW$986, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$986, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$986, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$986, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$986, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$987	.dwtag  DW_TAG_member
	.dwattr $C$DW$987, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$987, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$987, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$987, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$987, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$988	.dwtag  DW_TAG_member
	.dwattr $C$DW$988, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$988, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$988, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$988, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$988, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$989	.dwtag  DW_TAG_member
	.dwattr $C$DW$989, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$989, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$989, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$989, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$989, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$990	.dwtag  DW_TAG_member
	.dwattr $C$DW$990, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$990, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$990, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$990, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$990, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$121

$C$DW$T$128	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)
$C$DW$991	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$991, DW_AT_type(*$C$DW$T$128)
$C$DW$T$129	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$991)
$C$DW$T$130	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$T$130, DW_AT_address_class(0x16)

$C$DW$T$184	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$184, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$184, DW_AT_byte_size(0x132)
$C$DW$992	.dwtag  DW_TAG_member
	.dwattr $C$DW$992, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$992, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$992, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$992, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$992, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$993	.dwtag  DW_TAG_member
	.dwattr $C$DW$993, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$993, DW_AT_name("objdict")
	.dwattr $C$DW$993, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$993, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$993, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$994	.dwtag  DW_TAG_member
	.dwattr $C$DW$994, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$994, DW_AT_name("PDO_status")
	.dwattr $C$DW$994, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$994, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$994, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$995	.dwtag  DW_TAG_member
	.dwattr $C$DW$995, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$995, DW_AT_name("firstIndex")
	.dwattr $C$DW$995, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$995, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$995, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$996	.dwtag  DW_TAG_member
	.dwattr $C$DW$996, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$996, DW_AT_name("lastIndex")
	.dwattr $C$DW$996, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$996, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$996, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$997	.dwtag  DW_TAG_member
	.dwattr $C$DW$997, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$997, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$997, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$997, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$997, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$998	.dwtag  DW_TAG_member
	.dwattr $C$DW$998, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$998, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$998, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$998, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$998, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$999	.dwtag  DW_TAG_member
	.dwattr $C$DW$999, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$999, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$999, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$999, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$999, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1000	.dwtag  DW_TAG_member
	.dwattr $C$DW$1000, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$1000, DW_AT_name("transfers")
	.dwattr $C$DW$1000, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$1000, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1000, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1001	.dwtag  DW_TAG_member
	.dwattr $C$DW$1001, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$1001, DW_AT_name("nodeState")
	.dwattr $C$DW$1001, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$1001, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1001, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1002	.dwtag  DW_TAG_member
	.dwattr $C$DW$1002, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$1002, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$1002, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$1002, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$1002, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1003	.dwtag  DW_TAG_member
	.dwattr $C$DW$1003, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$1003, DW_AT_name("initialisation")
	.dwattr $C$DW$1003, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$1003, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$1003, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1004	.dwtag  DW_TAG_member
	.dwattr $C$DW$1004, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$1004, DW_AT_name("preOperational")
	.dwattr $C$DW$1004, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$1004, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$1004, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1005	.dwtag  DW_TAG_member
	.dwattr $C$DW$1005, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$1005, DW_AT_name("operational")
	.dwattr $C$DW$1005, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$1005, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$1005, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1006	.dwtag  DW_TAG_member
	.dwattr $C$DW$1006, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$1006, DW_AT_name("stopped")
	.dwattr $C$DW$1006, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$1006, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$1006, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1007	.dwtag  DW_TAG_member
	.dwattr $C$DW$1007, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$1007, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$1007, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$1007, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$1007, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1008	.dwtag  DW_TAG_member
	.dwattr $C$DW$1008, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$1008, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$1008, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$1008, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$1008, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1009	.dwtag  DW_TAG_member
	.dwattr $C$DW$1009, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$1009, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$1009, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$1009, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$1009, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1010	.dwtag  DW_TAG_member
	.dwattr $C$DW$1010, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$1010, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$1010, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$1010, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$1010, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1011	.dwtag  DW_TAG_member
	.dwattr $C$DW$1011, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$1011, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$1011, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$1011, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$1011, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1012	.dwtag  DW_TAG_member
	.dwattr $C$DW$1012, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$1012, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$1012, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$1012, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$1012, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1013	.dwtag  DW_TAG_member
	.dwattr $C$DW$1013, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1013, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$1013, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$1013, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$1013, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1014	.dwtag  DW_TAG_member
	.dwattr $C$DW$1014, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$1014, DW_AT_name("heartbeatError")
	.dwattr $C$DW$1014, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$1014, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$1014, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1015	.dwtag  DW_TAG_member
	.dwattr $C$DW$1015, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$1015, DW_AT_name("NMTable")
	.dwattr $C$DW$1015, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$1015, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$1015, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1016	.dwtag  DW_TAG_member
	.dwattr $C$DW$1016, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1016, DW_AT_name("syncTimer")
	.dwattr $C$DW$1016, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$1016, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$1016, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1017	.dwtag  DW_TAG_member
	.dwattr $C$DW$1017, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$1017, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$1017, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$1017, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$1017, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1018	.dwtag  DW_TAG_member
	.dwattr $C$DW$1018, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$1018, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$1018, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$1018, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$1018, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1019	.dwtag  DW_TAG_member
	.dwattr $C$DW$1019, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$1019, DW_AT_name("pre_sync")
	.dwattr $C$DW$1019, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$1019, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$1019, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1020	.dwtag  DW_TAG_member
	.dwattr $C$DW$1020, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$1020, DW_AT_name("post_TPDO")
	.dwattr $C$DW$1020, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$1020, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$1020, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1021	.dwtag  DW_TAG_member
	.dwattr $C$DW$1021, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$1021, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$1021, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$1021, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$1021, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1022	.dwtag  DW_TAG_member
	.dwattr $C$DW$1022, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1022, DW_AT_name("toggle")
	.dwattr $C$DW$1022, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$1022, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$1022, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1023	.dwtag  DW_TAG_member
	.dwattr $C$DW$1023, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$1023, DW_AT_name("canHandle")
	.dwattr $C$DW$1023, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$1023, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$1023, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1024	.dwtag  DW_TAG_member
	.dwattr $C$DW$1024, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$1024, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$1024, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$1024, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$1024, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1025	.dwtag  DW_TAG_member
	.dwattr $C$DW$1025, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$1025, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$1025, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$1025, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$1025, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1026	.dwtag  DW_TAG_member
	.dwattr $C$DW$1026, DW_AT_type(*$C$DW$T$164)
	.dwattr $C$DW$1026, DW_AT_name("globalCallback")
	.dwattr $C$DW$1026, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$1026, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$1026, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1027	.dwtag  DW_TAG_member
	.dwattr $C$DW$1027, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$1027, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$1027, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$1027, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$1027, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1028	.dwtag  DW_TAG_member
	.dwattr $C$DW$1028, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$1028, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$1028, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$1028, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$1028, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1029	.dwtag  DW_TAG_member
	.dwattr $C$DW$1029, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1029, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$1029, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$1029, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$1029, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1030	.dwtag  DW_TAG_member
	.dwattr $C$DW$1030, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1030, DW_AT_name("dcf_request")
	.dwattr $C$DW$1030, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$1030, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$1030, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1031	.dwtag  DW_TAG_member
	.dwattr $C$DW$1031, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$1031, DW_AT_name("error_state")
	.dwattr $C$DW$1031, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$1031, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$1031, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1032	.dwtag  DW_TAG_member
	.dwattr $C$DW$1032, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1032, DW_AT_name("error_history_size")
	.dwattr $C$DW$1032, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$1032, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$1032, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1033	.dwtag  DW_TAG_member
	.dwattr $C$DW$1033, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$1033, DW_AT_name("error_number")
	.dwattr $C$DW$1033, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$1033, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$1033, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1034	.dwtag  DW_TAG_member
	.dwattr $C$DW$1034, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$1034, DW_AT_name("error_first_element")
	.dwattr $C$DW$1034, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$1034, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$1034, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1035	.dwtag  DW_TAG_member
	.dwattr $C$DW$1035, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$1035, DW_AT_name("error_register")
	.dwattr $C$DW$1035, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$1035, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$1035, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1036	.dwtag  DW_TAG_member
	.dwattr $C$DW$1036, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$1036, DW_AT_name("error_cobid")
	.dwattr $C$DW$1036, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$1036, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$1036, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1037	.dwtag  DW_TAG_member
	.dwattr $C$DW$1037, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$1037, DW_AT_name("error_data")
	.dwattr $C$DW$1037, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$1037, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$1037, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1038	.dwtag  DW_TAG_member
	.dwattr $C$DW$1038, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$1038, DW_AT_name("post_emcy")
	.dwattr $C$DW$1038, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$1038, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$1038, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1039	.dwtag  DW_TAG_member
	.dwattr $C$DW$1039, DW_AT_type(*$C$DW$T$181)
	.dwattr $C$DW$1039, DW_AT_name("lss_transfer")
	.dwattr $C$DW$1039, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$1039, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$1039, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1040	.dwtag  DW_TAG_member
	.dwattr $C$DW$1040, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$1040, DW_AT_name("eeprom_index")
	.dwattr $C$DW$1040, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$1040, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$1040, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1041	.dwtag  DW_TAG_member
	.dwattr $C$DW$1041, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1041, DW_AT_name("eeprom_size")
	.dwattr $C$DW$1041, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$1041, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$1041, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$184

$C$DW$T$143	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$T$143, DW_AT_language(DW_LANG_C)
$C$DW$T$144	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$T$144, DW_AT_address_class(0x16)

$C$DW$T$186	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$186, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$186, DW_AT_byte_size(0x0e)
$C$DW$1042	.dwtag  DW_TAG_member
	.dwattr $C$DW$1042, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1042, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$1042, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$1042, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1042, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1043	.dwtag  DW_TAG_member
	.dwattr $C$DW$1043, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1043, DW_AT_name("event_timer")
	.dwattr $C$DW$1043, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$1043, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1043, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1044	.dwtag  DW_TAG_member
	.dwattr $C$DW$1044, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1044, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$1044, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$1044, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1044, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1045	.dwtag  DW_TAG_member
	.dwattr $C$DW$1045, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$1045, DW_AT_name("last_message")
	.dwattr $C$DW$1045, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$1045, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1045, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$186

$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)
$C$DW$T$127	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$T$127, DW_AT_address_class(0x16)

$C$DW$T$188	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$188, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$188, DW_AT_byte_size(0x14)
$C$DW$1046	.dwtag  DW_TAG_member
	.dwattr $C$DW$1046, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1046, DW_AT_name("nodeId")
	.dwattr $C$DW$1046, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$1046, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1046, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1047	.dwtag  DW_TAG_member
	.dwattr $C$DW$1047, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1047, DW_AT_name("whoami")
	.dwattr $C$DW$1047, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$1047, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1047, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1048	.dwtag  DW_TAG_member
	.dwattr $C$DW$1048, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1048, DW_AT_name("state")
	.dwattr $C$DW$1048, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$1048, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1048, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1049	.dwtag  DW_TAG_member
	.dwattr $C$DW$1049, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1049, DW_AT_name("toggle")
	.dwattr $C$DW$1049, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$1049, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1049, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1050	.dwtag  DW_TAG_member
	.dwattr $C$DW$1050, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1050, DW_AT_name("abortCode")
	.dwattr $C$DW$1050, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$1050, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1050, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1051	.dwtag  DW_TAG_member
	.dwattr $C$DW$1051, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1051, DW_AT_name("index")
	.dwattr $C$DW$1051, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$1051, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1051, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1052	.dwtag  DW_TAG_member
	.dwattr $C$DW$1052, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1052, DW_AT_name("subIndex")
	.dwattr $C$DW$1052, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$1052, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$1052, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1053	.dwtag  DW_TAG_member
	.dwattr $C$DW$1053, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1053, DW_AT_name("port")
	.dwattr $C$DW$1053, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$1053, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1053, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1054	.dwtag  DW_TAG_member
	.dwattr $C$DW$1054, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1054, DW_AT_name("count")
	.dwattr $C$DW$1054, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$1054, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1054, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1055	.dwtag  DW_TAG_member
	.dwattr $C$DW$1055, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1055, DW_AT_name("offset")
	.dwattr $C$DW$1055, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$1055, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1055, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1056	.dwtag  DW_TAG_member
	.dwattr $C$DW$1056, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$1056, DW_AT_name("datap")
	.dwattr $C$DW$1056, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$1056, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1056, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1057	.dwtag  DW_TAG_member
	.dwattr $C$DW$1057, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1057, DW_AT_name("dataType")
	.dwattr $C$DW$1057, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$1057, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1057, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1058	.dwtag  DW_TAG_member
	.dwattr $C$DW$1058, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1058, DW_AT_name("timer")
	.dwattr $C$DW$1058, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$1058, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$1058, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1059	.dwtag  DW_TAG_member
	.dwattr $C$DW$1059, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$1059, DW_AT_name("Callback")
	.dwattr $C$DW$1059, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$1059, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$1059, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$188

$C$DW$T$138	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$T$138, DW_AT_language(DW_LANG_C)

$C$DW$T$139	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$T$139, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x3c)
$C$DW$1060	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1060, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$139


$C$DW$T$192	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$192, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$192, DW_AT_byte_size(0x04)
$C$DW$1061	.dwtag  DW_TAG_member
	.dwattr $C$DW$1061, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$1061, DW_AT_name("pSubindex")
	.dwattr $C$DW$1061, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$1061, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1061, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1062	.dwtag  DW_TAG_member
	.dwattr $C$DW$1062, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1062, DW_AT_name("bSubCount")
	.dwattr $C$DW$1062, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$1062, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1062, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1063	.dwtag  DW_TAG_member
	.dwattr $C$DW$1063, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1063, DW_AT_name("index")
	.dwattr $C$DW$1063, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$1063, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1063, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$192

$C$DW$T$123	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$192)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)
$C$DW$1064	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1064, DW_AT_type(*$C$DW$T$123)
$C$DW$T$124	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$1064)
$C$DW$T$125	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_address_class(0x16)

$C$DW$T$168	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$168, DW_AT_language(DW_LANG_C)
$C$DW$1065	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1065, DW_AT_type(*$C$DW$T$9)
$C$DW$1066	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1066, DW_AT_type(*$C$DW$T$151)
$C$DW$1067	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1067, DW_AT_type(*$C$DW$T$167)
	.dwendtag $C$DW$T$168

$C$DW$T$169	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$T$169, DW_AT_address_class(0x16)
$C$DW$T$170	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$T$170, DW_AT_language(DW_LANG_C)

$C$DW$T$193	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$193, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$193, DW_AT_byte_size(0x08)
$C$DW$1068	.dwtag  DW_TAG_member
	.dwattr $C$DW$1068, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1068, DW_AT_name("bAccessType")
	.dwattr $C$DW$1068, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$1068, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1068, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1069	.dwtag  DW_TAG_member
	.dwattr $C$DW$1069, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1069, DW_AT_name("bDataType")
	.dwattr $C$DW$1069, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$1069, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1069, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1070	.dwtag  DW_TAG_member
	.dwattr $C$DW$1070, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1070, DW_AT_name("size")
	.dwattr $C$DW$1070, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$1070, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1070, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1071	.dwtag  DW_TAG_member
	.dwattr $C$DW$1071, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1071, DW_AT_name("pObject")
	.dwattr $C$DW$1071, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$1071, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1071, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1072	.dwtag  DW_TAG_member
	.dwattr $C$DW$1072, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1072, DW_AT_name("bProcessor")
	.dwattr $C$DW$1072, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$1072, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1072, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$193

$C$DW$1073	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1073, DW_AT_type(*$C$DW$T$193)
$C$DW$T$189	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$189, DW_AT_type(*$C$DW$1073)
$C$DW$T$190	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$190, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$T$190, DW_AT_language(DW_LANG_C)
$C$DW$T$191	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$191, DW_AT_type(*$C$DW$T$190)
	.dwattr $C$DW$T$191, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$1074	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$1074, DW_AT_location[DW_OP_reg0]
$C$DW$1075	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$1075, DW_AT_location[DW_OP_reg1]
$C$DW$1076	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$1076, DW_AT_location[DW_OP_reg2]
$C$DW$1077	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$1077, DW_AT_location[DW_OP_reg3]
$C$DW$1078	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$1078, DW_AT_location[DW_OP_reg20]
$C$DW$1079	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$1079, DW_AT_location[DW_OP_reg21]
$C$DW$1080	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$1080, DW_AT_location[DW_OP_reg22]
$C$DW$1081	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$1081, DW_AT_location[DW_OP_reg23]
$C$DW$1082	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$1082, DW_AT_location[DW_OP_reg24]
$C$DW$1083	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$1083, DW_AT_location[DW_OP_reg25]
$C$DW$1084	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$1084, DW_AT_location[DW_OP_reg26]
$C$DW$1085	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$1085, DW_AT_location[DW_OP_reg28]
$C$DW$1086	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$1086, DW_AT_location[DW_OP_reg29]
$C$DW$1087	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$1087, DW_AT_location[DW_OP_reg30]
$C$DW$1088	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$1088, DW_AT_location[DW_OP_reg31]
$C$DW$1089	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$1089, DW_AT_location[DW_OP_regx 0x20]
$C$DW$1090	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$1090, DW_AT_location[DW_OP_regx 0x21]
$C$DW$1091	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$1091, DW_AT_location[DW_OP_regx 0x22]
$C$DW$1092	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$1092, DW_AT_location[DW_OP_regx 0x23]
$C$DW$1093	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$1093, DW_AT_location[DW_OP_regx 0x24]
$C$DW$1094	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$1094, DW_AT_location[DW_OP_regx 0x25]
$C$DW$1095	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$1095, DW_AT_location[DW_OP_regx 0x26]
$C$DW$1096	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$1096, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$1097	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$1097, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$1098	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$1098, DW_AT_location[DW_OP_reg4]
$C$DW$1099	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$1099, DW_AT_location[DW_OP_reg6]
$C$DW$1100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$1100, DW_AT_location[DW_OP_reg8]
$C$DW$1101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$1101, DW_AT_location[DW_OP_reg10]
$C$DW$1102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$1102, DW_AT_location[DW_OP_reg12]
$C$DW$1103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$1103, DW_AT_location[DW_OP_reg14]
$C$DW$1104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$1104, DW_AT_location[DW_OP_reg16]
$C$DW$1105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$1105, DW_AT_location[DW_OP_reg17]
$C$DW$1106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$1106, DW_AT_location[DW_OP_reg18]
$C$DW$1107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$1107, DW_AT_location[DW_OP_reg19]
$C$DW$1108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$1108, DW_AT_location[DW_OP_reg5]
$C$DW$1109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$1109, DW_AT_location[DW_OP_reg7]
$C$DW$1110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$1110, DW_AT_location[DW_OP_reg9]
$C$DW$1111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$1111, DW_AT_location[DW_OP_reg11]
$C$DW$1112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$1112, DW_AT_location[DW_OP_reg13]
$C$DW$1113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$1113, DW_AT_location[DW_OP_reg15]
$C$DW$1114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$1114, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$1115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$1115, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$1116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$1116, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$1117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$1117, DW_AT_location[DW_OP_regx 0x30]
$C$DW$1118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$1118, DW_AT_location[DW_OP_regx 0x33]
$C$DW$1119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$1119, DW_AT_location[DW_OP_regx 0x34]
$C$DW$1120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$1120, DW_AT_location[DW_OP_regx 0x37]
$C$DW$1121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$1121, DW_AT_location[DW_OP_regx 0x38]
$C$DW$1122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$1122, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$1123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$1123, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$1124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$1124, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$1125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$1125, DW_AT_location[DW_OP_regx 0x40]
$C$DW$1126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$1126, DW_AT_location[DW_OP_regx 0x43]
$C$DW$1127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$1127, DW_AT_location[DW_OP_regx 0x44]
$C$DW$1128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$1128, DW_AT_location[DW_OP_regx 0x47]
$C$DW$1129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$1129, DW_AT_location[DW_OP_regx 0x48]
$C$DW$1130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$1130, DW_AT_location[DW_OP_regx 0x49]
$C$DW$1131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$1131, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$1132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$1132, DW_AT_location[DW_OP_regx 0x27]
$C$DW$1133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$1133, DW_AT_location[DW_OP_regx 0x28]
$C$DW$1134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$1134, DW_AT_location[DW_OP_reg27]
$C$DW$1135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$1135, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

