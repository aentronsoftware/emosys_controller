;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0976412 
	.sect	".text"
	.clink
	.global	_DMA_configAddresses

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("DMA_configAddresses")
	.dwattr $C$DW$3, DW_AT_low_pc(_DMA_configAddresses)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_DMA_configAddresses")
	.dwattr $C$DW$3, DW_AT_external
	.dwattr $C$DW$3, DW_AT_TI_begin_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x32)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 52,column 1,is_stmt,address _DMA_configAddresses

	.dwfde $C$DW$CIE, _DMA_configAddresses
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("destAddr")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_destAddr")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg12]
$C$DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("srcAddr")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_srcAddr")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _DMA_configAddresses          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_DMA_configAddresses:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -2]
$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("destAddr")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_destAddr")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$8, DW_AT_location[DW_OP_breg20 -4]
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("srcAddr")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_srcAddr")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[6],XAR5          ; [CPU_] |52| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |52| 
        MOVL      *-SP[2],ACC           ; [CPU_] |52| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 58,column 5,is_stmt
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_name("___eallow")
	.dwattr $C$DW$10, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |58| 
        ; call occurs [#___eallow] ; [] |58| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 63,column 5,is_stmt
        MOVL      XAR6,*-SP[6]          ; [CPU_] |63| 
        MOVB      ACC,#16               ; [CPU_] |63| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |63| 
        MOVL      XAR4,ACC              ; [CPU_] |63| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |63| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 64,column 5,is_stmt
        MOVL      XAR6,*-SP[6]          ; [CPU_] |64| 
        MOVB      ACC,#18               ; [CPU_] |64| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |64| 
        MOVL      XAR4,ACC              ; [CPU_] |64| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |64| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 69,column 5,is_stmt
        MOVL      XAR6,*-SP[4]          ; [CPU_] |69| 
        MOVB      ACC,#24               ; [CPU_] |69| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |69| 
        MOVL      XAR4,ACC              ; [CPU_] |69| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |69| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 70,column 5,is_stmt
        MOVL      XAR6,*-SP[4]          ; [CPU_] |70| 
        MOVB      ACC,#26               ; [CPU_] |70| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |70| 
        MOVL      XAR4,ACC              ; [CPU_] |70| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |70| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 72,column 5,is_stmt
$C$DW$11	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$11, DW_AT_low_pc(0x00)
	.dwattr $C$DW$11, DW_AT_name("___edis")
	.dwattr $C$DW$11, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |72| 
        ; call occurs [#___edis] ; [] |72| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 73,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$12	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$12, DW_AT_low_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x49)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink
	.global	_DMA_configBurst

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("DMA_configBurst")
	.dwattr $C$DW$13, DW_AT_low_pc(_DMA_configBurst)
	.dwattr $C$DW$13, DW_AT_high_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_DMA_configBurst")
	.dwattr $C$DW$13, DW_AT_external
	.dwattr $C$DW$13, DW_AT_TI_begin_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$13, DW_AT_TI_begin_line(0x50)
	.dwattr $C$DW$13, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$13, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 82,column 1,is_stmt,address _DMA_configBurst

	.dwfde $C$DW$CIE, _DMA_configBurst
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_reg0]
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("size")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg12]
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("srcStep")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_srcStep")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg14]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("destStep")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_destStep")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -7]

;***************************************************************
;* FNAME: _DMA_configBurst              FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_DMA_configBurst:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -2]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("size")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -3]
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("srcStep")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_srcStep")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |82| 
        MOV       *-SP[3],AR4           ; [CPU_] |82| 
        MOVL      *-SP[2],ACC           ; [CPU_] |82| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 89,column 5,is_stmt
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("___eallow")
	.dwattr $C$DW$21, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |89| 
        ; call occurs [#___eallow] ; [] |89| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 94,column 5,is_stmt
        MOVB      ACC,#2                ; [CPU_] |94| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |94| 
        MOVL      XAR4,ACC              ; [CPU_] |94| 
        MOV       AL,*-SP[3]            ; [CPU_] |94| 
        ADDB      AL,#-1                ; [CPU_] |94| 
        MOV       *+XAR4[0],AL          ; [CPU_] |94| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 95,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |95| 
        MOVB      ACC,#4                ; [CPU_] |95| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |95| 
        MOVL      XAR4,ACC              ; [CPU_] |95| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |95| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 96,column 5,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |96| 
        MOVB      ACC,#5                ; [CPU_] |96| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |96| 
        MOVL      XAR4,ACC              ; [CPU_] |96| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |96| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 98,column 5,is_stmt
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("___edis")
	.dwattr $C$DW$22, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |98| 
        ; call occurs [#___edis] ; [] |98| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 99,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$13, DW_AT_TI_end_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$13, DW_AT_TI_end_line(0x63)
	.dwattr $C$DW$13, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$13

	.sect	".text"
	.clink
	.global	_DMA_configTransfer

$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("DMA_configTransfer")
	.dwattr $C$DW$24, DW_AT_low_pc(_DMA_configTransfer)
	.dwattr $C$DW$24, DW_AT_high_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_DMA_configTransfer")
	.dwattr $C$DW$24, DW_AT_external
	.dwattr $C$DW$24, DW_AT_TI_begin_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$24, DW_AT_TI_begin_line(0x6a)
	.dwattr $C$DW$24, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$24, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 108,column 1,is_stmt,address _DMA_configTransfer

	.dwfde $C$DW$CIE, _DMA_configTransfer
$C$DW$25	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg0]
$C$DW$26	.dwtag  DW_TAG_formal_parameter, DW_AT_name("transferSize")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_transferSize")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -8]
$C$DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_name("srcStep")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_srcStep")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg12]
$C$DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("destStep")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_destStep")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _DMA_configTransfer           FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_DMA_configTransfer:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -2]
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("srcStep")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_srcStep")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_breg20 -3]
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("destStep")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_destStep")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |108| 
        MOV       *-SP[3],AR4           ; [CPU_] |108| 
        MOVL      *-SP[2],ACC           ; [CPU_] |108| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 115,column 5,is_stmt
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("___eallow")
	.dwattr $C$DW$32, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |115| 
        ; call occurs [#___eallow] ; [] |115| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 120,column 5,is_stmt
        MOVB      ACC,#6                ; [CPU_] |120| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |120| 
        MOVL      XAR4,ACC              ; [CPU_] |120| 
        MOV       AL,*-SP[8]            ; [CPU_] |120| 
        ADDB      AL,#-1                ; [CPU_] |120| 
        MOV       *+XAR4[0],AL          ; [CPU_] |120| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 121,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |121| 
        MOVB      ACC,#8                ; [CPU_] |121| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |121| 
        MOVL      XAR4,ACC              ; [CPU_] |121| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |121| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 122,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |122| 
        MOVB      ACC,#9                ; [CPU_] |122| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |122| 
        MOVL      XAR4,ACC              ; [CPU_] |122| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |122| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 124,column 5,is_stmt
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("___edis")
	.dwattr $C$DW$33, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |124| 
        ; call occurs [#___edis] ; [] |124| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 125,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$24, DW_AT_TI_end_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$24, DW_AT_TI_end_line(0x7d)
	.dwattr $C$DW$24, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$24

	.sect	".text"
	.clink
	.global	_DMA_configWrap

$C$DW$35	.dwtag  DW_TAG_subprogram, DW_AT_name("DMA_configWrap")
	.dwattr $C$DW$35, DW_AT_low_pc(_DMA_configWrap)
	.dwattr $C$DW$35, DW_AT_high_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_DMA_configWrap")
	.dwattr $C$DW$35, DW_AT_external
	.dwattr $C$DW$35, DW_AT_TI_begin_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$35, DW_AT_TI_begin_line(0x84)
	.dwattr $C$DW$35, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$35, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 134,column 1,is_stmt,address _DMA_configWrap

	.dwfde $C$DW$CIE, _DMA_configWrap
$C$DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg0]
$C$DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_name("srcWrapSize")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_srcWrapSize")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -8]
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("srcStep")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_srcStep")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg12]
$C$DW$39	.dwtag  DW_TAG_formal_parameter, DW_AT_name("destWrapSize")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_destWrapSize")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -10]
$C$DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("destStep")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_destStep")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _DMA_configWrap               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_DMA_configWrap:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -2]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("srcStep")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_srcStep")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -3]
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("destStep")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_destStep")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |134| 
        MOV       *-SP[3],AR4           ; [CPU_] |134| 
        MOVL      *-SP[2],ACC           ; [CPU_] |134| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 141,column 5,is_stmt
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("___eallow")
	.dwattr $C$DW$44, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |141| 
        ; call occurs [#___eallow] ; [] |141| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 146,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |146| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |146| 
        MOVL      XAR4,ACC              ; [CPU_] |146| 
        MOV       AL,*-SP[8]            ; [CPU_] |146| 
        ADDB      AL,#-1                ; [CPU_] |146| 
        MOV       *+XAR4[0],AL          ; [CPU_] |146| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 147,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |147| 
        MOVB      ACC,#12               ; [CPU_] |147| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |147| 
        MOVL      XAR4,ACC              ; [CPU_] |147| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |147| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 149,column 5,is_stmt
        MOVB      ACC,#13               ; [CPU_] |149| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |149| 
        MOVL      XAR4,ACC              ; [CPU_] |149| 
        MOV       AL,*-SP[10]           ; [CPU_] |149| 
        ADDB      AL,#-1                ; [CPU_] |149| 
        MOV       *+XAR4[0],AL          ; [CPU_] |149| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 150,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |150| 
        MOVB      ACC,#15               ; [CPU_] |150| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |150| 
        MOVL      XAR4,ACC              ; [CPU_] |150| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |150| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 152,column 5,is_stmt
$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_name("___edis")
	.dwattr $C$DW$45, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |152| 
        ; call occurs [#___edis] ; [] |152| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 153,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$35, DW_AT_TI_end_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$35, DW_AT_TI_end_line(0x99)
	.dwattr $C$DW$35, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$35

	.sect	".text"
	.clink
	.global	_DMA_configMode

$C$DW$47	.dwtag  DW_TAG_subprogram, DW_AT_name("DMA_configMode")
	.dwattr $C$DW$47, DW_AT_low_pc(_DMA_configMode)
	.dwattr $C$DW$47, DW_AT_high_pc(0x00)
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_DMA_configMode")
	.dwattr $C$DW$47, DW_AT_external
	.dwattr $C$DW$47, DW_AT_TI_begin_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$47, DW_AT_TI_begin_line(0xa0)
	.dwattr $C$DW$47, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$47, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 161,column 1,is_stmt,address _DMA_configMode

	.dwfde $C$DW$CIE, _DMA_configMode
$C$DW$48	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg0]
$C$DW$49	.dwtag  DW_TAG_formal_parameter, DW_AT_name("trigger")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_trigger")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg12]
$C$DW$50	.dwtag  DW_TAG_formal_parameter, DW_AT_name("config")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_config")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_breg20 -8]

;***************************************************************
;* FNAME: _DMA_configMode               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_DMA_configMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -2]
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("trigger")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_trigger")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |161| 
        MOVL      *-SP[2],ACC           ; [CPU_] |161| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 167,column 5,is_stmt
$C$DW$53	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$53, DW_AT_low_pc(0x00)
	.dwattr $C$DW$53, DW_AT_name("___eallow")
	.dwattr $C$DW$53, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |167| 
        ; call occurs [#___eallow] ; [] |167| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 173,column 5,is_stmt
        B         $C$L7,UNC             ; [CPU_] |173| 
        ; branch occurs ; [] |173| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 179,column 13,is_stmt
        MOVL      XAR4,#31126           ; [CPU_] |179| 
        SETC      SXM                   ; [CPU_] 
        MOVL      P,*+XAR4[0]           ; [CPU_] |179| 
        MOV       ACC,*-SP[3]           ; [CPU_] |179| 
        AND       PL,#65280             ; [CPU_] |179| 
        OR        AL,PL                 ; [CPU_] |179| 
        OR        AH,PH                 ; [CPU_] |179| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |179| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 187,column 13,is_stmt
        MOV       AL,*(0:0x1020)        ; [CPU_] |187| 
        AND       AL,AL,#0xffe0         ; [CPU_] |187| 
        ORB       AL,#0x01              ; [CPU_] |187| 
        MOV       *(0:0x1020),AL        ; [CPU_] |187| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 189,column 13,is_stmt
        B         $C$L9,UNC             ; [CPU_] |189| 
        ; branch occurs ; [] |189| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 195,column 13,is_stmt
        MOVL      XAR4,#31126           ; [CPU_] |195| 
        SETC      SXM                   ; [CPU_] 
        MOVL      P,*+XAR4[0]           ; [CPU_] |195| 
        MOV       ACC,*-SP[3]           ; [CPU_] |195| 
        AND       PL,#255               ; [CPU_] |195| 
        LSL       ACC,8                 ; [CPU_] |195| 
        OR        AL,PL                 ; [CPU_] |195| 
        OR        AH,PH                 ; [CPU_] |195| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |195| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 203,column 13,is_stmt
        MOV       AL,*(0:0x1040)        ; [CPU_] |203| 
        AND       AL,AL,#0xffe0         ; [CPU_] |203| 
        ORB       AL,#0x02              ; [CPU_] |203| 
        MOV       *(0:0x1040),AL        ; [CPU_] |203| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 205,column 13,is_stmt
        B         $C$L9,UNC             ; [CPU_] |205| 
        ; branch occurs ; [] |205| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 211,column 13,is_stmt
        MOVL      XAR4,#31126           ; [CPU_] |211| 
        SETC      SXM                   ; [CPU_] 
        MOVL      P,*+XAR4[0]           ; [CPU_] |211| 
        MOV       ACC,*-SP[3]           ; [CPU_] |211| 
        AND       PH,#65280             ; [CPU_] |211| 
        LSL       ACC,16                ; [CPU_] |211| 
        OR        AL,PL                 ; [CPU_] |211| 
        OR        AH,PH                 ; [CPU_] |211| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |211| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 219,column 13,is_stmt
        MOV       AL,*(0:0x1060)        ; [CPU_] |219| 
        AND       AL,AL,#0xffe0         ; [CPU_] |219| 
        ORB       AL,#0x03              ; [CPU_] |219| 
        MOV       *(0:0x1060),AL        ; [CPU_] |219| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 221,column 13,is_stmt
        B         $C$L9,UNC             ; [CPU_] |221| 
        ; branch occurs ; [] |221| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 227,column 13,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31126           ; [CPU_] |227| 
        MOV       ACC,*-SP[3]           ; [CPU_] |227| 
        MOV       T,#24                 ; [CPU_] |227| 
        MOVL      P,*+XAR4[0]           ; [CPU_] |227| 
        AND       PH,#255               ; [CPU_] |227| 
        LSLL      ACC,T                 ; [CPU_] |227| 
        OR        AL,PL                 ; [CPU_] |227| 
        OR        AH,PH                 ; [CPU_] |227| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |227| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 235,column 13,is_stmt
        MOV       AL,*(0:0x1080)        ; [CPU_] |235| 
        AND       AL,AL,#0xffe0         ; [CPU_] |235| 
        ORB       AL,#0x04              ; [CPU_] |235| 
        MOV       *(0:0x1080),AL        ; [CPU_] |235| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 237,column 13,is_stmt
        B         $C$L9,UNC             ; [CPU_] |237| 
        ; branch occurs ; [] |237| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 243,column 13,is_stmt
        MOVL      XAR4,#31128           ; [CPU_] |243| 
        SETC      SXM                   ; [CPU_] 
        MOVL      P,*+XAR4[0]           ; [CPU_] |243| 
        MOV       ACC,*-SP[3]           ; [CPU_] |243| 
        AND       PL,#65280             ; [CPU_] |243| 
        OR        AL,PL                 ; [CPU_] |243| 
        OR        AH,PH                 ; [CPU_] |243| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |243| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 251,column 13,is_stmt
        MOV       AL,*(0:0x10a0)        ; [CPU_] |251| 
        AND       AL,AL,#0xffe0         ; [CPU_] |251| 
        ORB       AL,#0x05              ; [CPU_] |251| 
        MOV       *(0:0x10a0),AL        ; [CPU_] |251| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 253,column 13,is_stmt
        B         $C$L9,UNC             ; [CPU_] |253| 
        ; branch occurs ; [] |253| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 259,column 13,is_stmt
        MOVL      XAR4,#31128           ; [CPU_] |259| 
        SETC      SXM                   ; [CPU_] 
        MOVL      P,*+XAR4[0]           ; [CPU_] |259| 
        MOV       ACC,*-SP[3]           ; [CPU_] |259| 
        AND       PL,#255               ; [CPU_] |259| 
        LSL       ACC,8                 ; [CPU_] |259| 
        OR        AL,PL                 ; [CPU_] |259| 
        OR        AH,PH                 ; [CPU_] |259| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |259| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 267,column 13,is_stmt
        MOV       AL,*(0:0x10c0)        ; [CPU_] |267| 
        AND       AL,AL,#0xffe0         ; [CPU_] |267| 
        ORB       AL,#0x06              ; [CPU_] |267| 
        MOV       *(0:0x10c0),AL        ; [CPU_] |267| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 269,column 13,is_stmt
        B         $C$L9,UNC             ; [CPU_] |269| 
        ; branch occurs ; [] |269| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 173,column 5,is_stmt
        MOVL      XAR6,*-SP[2]          ; [CPU_] |173| 
        MOV       ACC,#4224             ; [CPU_] |173| 
        CMPL      ACC,XAR6              ; [CPU_] |173| 
        B         $C$L8,LT              ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
        CMPL      ACC,XAR6              ; [CPU_] |173| 
        BF        $C$L4,EQ              ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
        MOV       ACC,#4128             ; [CPU_] |173| 
        CMPL      ACC,XAR6              ; [CPU_] |173| 
        BF        $C$L1,EQ              ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
        MOV       ACC,#4160             ; [CPU_] |173| 
        CMPL      ACC,XAR6              ; [CPU_] |173| 
        BF        $C$L2,EQ              ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
        MOV       ACC,#4192             ; [CPU_] |173| 
        CMPL      ACC,XAR6              ; [CPU_] |173| 
        BF        $C$L3,EQ              ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
        B         $C$L9,UNC             ; [CPU_] |173| 
        ; branch occurs ; [] |173| 
$C$L8:    
        MOV       ACC,#4256             ; [CPU_] |173| 
        CMPL      ACC,XAR6              ; [CPU_] |173| 
        BF        $C$L5,EQ              ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
        MOV       ACC,#4288             ; [CPU_] |173| 
        CMPL      ACC,XAR6              ; [CPU_] |173| 
        BF        $C$L6,EQ              ; [CPU_] |173| 
        ; branchcc occurs ; [] |173| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 281,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |281| 
        AND       *+XAR4[0],#0xb3ff     ; [CPU_] |281| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 283,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |283| 
        MOV       AL,*-SP[8]            ; [CPU_] |283| 
        OR        *+XAR4[0],AL          ; [CPU_] |283| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 285,column 5,is_stmt
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_name("___edis")
	.dwattr $C$DW$54, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |285| 
        ; call occurs [#___edis] ; [] |285| 
	.dwpsn	file "../ExtraData/driverlib2/dma.c",line 286,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$47, DW_AT_TI_end_file("../ExtraData/driverlib2/dma.c")
	.dwattr $C$DW$47, DW_AT_TI_end_line(0x11e)
	.dwattr $C$DW$47, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$47

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$56	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SOFTWARE"), DW_AT_const_value(0x00)
$C$DW$57	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCA1"), DW_AT_const_value(0x01)
$C$DW$58	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCA2"), DW_AT_const_value(0x02)
$C$DW$59	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCA3"), DW_AT_const_value(0x03)
$C$DW$60	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCA4"), DW_AT_const_value(0x04)
$C$DW$61	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCAEVT"), DW_AT_const_value(0x05)
$C$DW$62	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCB1"), DW_AT_const_value(0x06)
$C$DW$63	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCB2"), DW_AT_const_value(0x07)
$C$DW$64	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCB3"), DW_AT_const_value(0x08)
$C$DW$65	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCB4"), DW_AT_const_value(0x09)
$C$DW$66	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCBEVT"), DW_AT_const_value(0x0a)
$C$DW$67	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCD1"), DW_AT_const_value(0x10)
$C$DW$68	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCD2"), DW_AT_const_value(0x11)
$C$DW$69	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCD3"), DW_AT_const_value(0x12)
$C$DW$70	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCD4"), DW_AT_const_value(0x13)
$C$DW$71	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_ADCDEVT"), DW_AT_const_value(0x14)
$C$DW$72	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_XINT1"), DW_AT_const_value(0x1d)
$C$DW$73	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_XINT2"), DW_AT_const_value(0x1e)
$C$DW$74	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_XINT3"), DW_AT_const_value(0x1f)
$C$DW$75	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_XINT4"), DW_AT_const_value(0x20)
$C$DW$76	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_XINT5"), DW_AT_const_value(0x21)
$C$DW$77	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM1SOCA"), DW_AT_const_value(0x24)
$C$DW$78	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM1SOCB"), DW_AT_const_value(0x25)
$C$DW$79	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM2SOCA"), DW_AT_const_value(0x26)
$C$DW$80	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM2SOCB"), DW_AT_const_value(0x27)
$C$DW$81	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM3SOCA"), DW_AT_const_value(0x28)
$C$DW$82	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM3SOCB"), DW_AT_const_value(0x29)
$C$DW$83	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM4SOCA"), DW_AT_const_value(0x2a)
$C$DW$84	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM4SOCB"), DW_AT_const_value(0x2b)
$C$DW$85	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM5SOCA"), DW_AT_const_value(0x2c)
$C$DW$86	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM5SOCB"), DW_AT_const_value(0x2d)
$C$DW$87	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM6SOCA"), DW_AT_const_value(0x2e)
$C$DW$88	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM6SOCB"), DW_AT_const_value(0x2f)
$C$DW$89	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM7SOCA"), DW_AT_const_value(0x30)
$C$DW$90	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM7SOCB"), DW_AT_const_value(0x31)
$C$DW$91	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM8SOCA"), DW_AT_const_value(0x32)
$C$DW$92	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM8SOCB"), DW_AT_const_value(0x33)
$C$DW$93	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM9SOCA"), DW_AT_const_value(0x34)
$C$DW$94	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM9SOCB"), DW_AT_const_value(0x35)
$C$DW$95	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM10SOCA"), DW_AT_const_value(0x36)
$C$DW$96	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM10SOCB"), DW_AT_const_value(0x37)
$C$DW$97	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM11SOCA"), DW_AT_const_value(0x38)
$C$DW$98	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM11SOCB"), DW_AT_const_value(0x39)
$C$DW$99	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM12SOCA"), DW_AT_const_value(0x3a)
$C$DW$100	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_EPWM12SOCB"), DW_AT_const_value(0x3b)
$C$DW$101	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_TINT0"), DW_AT_const_value(0x44)
$C$DW$102	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_TINT1"), DW_AT_const_value(0x45)
$C$DW$103	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_TINT2"), DW_AT_const_value(0x46)
$C$DW$104	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_MCBSPAMXEVT"), DW_AT_const_value(0x47)
$C$DW$105	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_MCBSPAMREVT"), DW_AT_const_value(0x48)
$C$DW$106	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_MCBSPBMXEVT"), DW_AT_const_value(0x49)
$C$DW$107	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_MCBSPBMREVT"), DW_AT_const_value(0x4a)
$C$DW$108	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM1FLT1"), DW_AT_const_value(0x5f)
$C$DW$109	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM1FLT2"), DW_AT_const_value(0x60)
$C$DW$110	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM1FLT3"), DW_AT_const_value(0x61)
$C$DW$111	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM1FLT4"), DW_AT_const_value(0x62)
$C$DW$112	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM2FLT1"), DW_AT_const_value(0x63)
$C$DW$113	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM2FLT2"), DW_AT_const_value(0x64)
$C$DW$114	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM2FLT3"), DW_AT_const_value(0x65)
$C$DW$115	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SDFM2FLT4"), DW_AT_const_value(0x66)
$C$DW$116	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SPIATX"), DW_AT_const_value(0x6d)
$C$DW$117	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SPIARX"), DW_AT_const_value(0x6e)
$C$DW$118	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SPIBTX"), DW_AT_const_value(0x6f)
$C$DW$119	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SPIBRX"), DW_AT_const_value(0x70)
$C$DW$120	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SPICTX"), DW_AT_const_value(0x71)
$C$DW$121	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_SPICRX"), DW_AT_const_value(0x72)
$C$DW$122	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_CLB1INT"), DW_AT_const_value(0x7f)
$C$DW$123	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_CLB2INT"), DW_AT_const_value(0x80)
$C$DW$124	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_CLB3INT"), DW_AT_const_value(0x81)
$C$DW$125	.dwtag  DW_TAG_enumerator, DW_AT_name("DMA_TRIGGER_CLB4INT"), DW_AT_const_value(0x82)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("DMA_Trigger")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$126	.dwtag  DW_TAG_TI_far_type
$C$DW$T$23	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$126)
$C$DW$T$24	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_address_class(0x16)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg0]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_reg1]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_reg2]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_reg3]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_reg20]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_reg21]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg22]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg23]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg24]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg25]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_reg26]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg28]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg29]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg30]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg31]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x20]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x21]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x22]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x23]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x24]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x25]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x26]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg4]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg6]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg8]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg10]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_reg12]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_reg14]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_reg16]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_reg17]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_reg18]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_reg19]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_reg5]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_reg7]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_reg9]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_reg11]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_reg13]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_reg15]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_regx 0x30]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_regx 0x33]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x34]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_regx 0x37]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_regx 0x38]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x40]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_regx 0x43]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x44]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x47]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x48]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x49]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x27]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x28]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_reg27]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

