;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("__enable_interrupts")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("___enable_interrupts")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("__disable_interrupts")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("___disable_interrupts")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1725612 
	.sect	".text"
	.clink

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_enableMaster")
	.dwattr $C$DW$5, DW_AT_low_pc(_Interrupt_enableMaster)
	.dwattr $C$DW$5, DW_AT_high_pc(0x00)
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_Interrupt_enableMaster")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$5, DW_AT_TI_begin_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$5, DW_AT_TI_begin_line(0xf3)
	.dwattr $C$DW$5, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$5, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 244,column 1,is_stmt,address _Interrupt_enableMaster

	.dwfde $C$DW$CIE, _Interrupt_enableMaster

;***************************************************************
;* FNAME: _Interrupt_enableMaster       FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_Interrupt_enableMaster:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 248,column 5,is_stmt
        PUSH      ST1                   ; [CPU_] |248| 
        CLRC      INTM, DBGM            ; [CPU_] |248| 
        MOV       AL,*--SP              ; [CPU_] |248| 
        TBIT      AL,#0                 ; [CPU_] |248| 
        BF        $C$L1,NTC             ; [CPU_] |248| 
        ; branchcc occurs ; [] |248| 
        MOVB      AL,#1                 ; [CPU_] |248| 
        B         $C$L2,UNC             ; [CPU_] |248| 
        ; branch occurs ; [] |248| 
$C$L1:    
        MOVB      AL,#0                 ; [CPU_] |248| 
$C$L2:    
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 249,column 1,is_stmt
$C$DW$6	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$6, DW_AT_low_pc(0x00)
	.dwattr $C$DW$6, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$5, DW_AT_TI_end_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$5, DW_AT_TI_end_line(0xf9)
	.dwattr $C$DW$5, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$5

	.sect	".text"
	.clink

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_disableMaster")
	.dwattr $C$DW$7, DW_AT_low_pc(_Interrupt_disableMaster)
	.dwattr $C$DW$7, DW_AT_high_pc(0x00)
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_Interrupt_disableMaster")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$7, DW_AT_TI_begin_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$7, DW_AT_TI_begin_line(0x107)
	.dwattr $C$DW$7, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$7, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 264,column 1,is_stmt,address _Interrupt_disableMaster

	.dwfde $C$DW$CIE, _Interrupt_disableMaster

;***************************************************************
;* FNAME: _Interrupt_disableMaster      FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_Interrupt_disableMaster:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 268,column 5,is_stmt
        PUSH      ST1                   ; [CPU_] |268| 
        SETC      INTM, DBGM            ; [CPU_] |268| 
        MOV       AL,*--SP              ; [CPU_] |268| 
        TBIT      AL,#0                 ; [CPU_] |268| 
        BF        $C$L3,NTC             ; [CPU_] |268| 
        ; branchcc occurs ; [] |268| 
        MOVB      AL,#1                 ; [CPU_] |268| 
        B         $C$L4,UNC             ; [CPU_] |268| 
        ; branch occurs ; [] |268| 
$C$L3:    
        MOVB      AL,#0                 ; [CPU_] |268| 
$C$L4:    
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 269,column 1,is_stmt
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$7, DW_AT_TI_end_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$7, DW_AT_TI_end_line(0x10d)
	.dwattr $C$DW$7, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$7

	.sect	".text"
	.clink

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_defaultHandler")
	.dwattr $C$DW$9, DW_AT_low_pc(_Interrupt_defaultHandler)
	.dwattr $C$DW$9, DW_AT_high_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_Interrupt_defaultHandler")
	.dwattr $C$DW$9, DW_AT_TI_begin_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$9, DW_AT_TI_begin_line(0x8c)
	.dwattr $C$DW$9, DW_AT_TI_begin_column(0x0d)
	.dwattr $C$DW$9, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 141,column 1,is_stmt,address _Interrupt_defaultHandler

	.dwfde $C$DW$CIE, _Interrupt_defaultHandler

;***************************************************************
;* FNAME: _Interrupt_defaultHandler     FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_Interrupt_defaultHandler:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("pieVect")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_pieVect")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_breg20 -1]
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("vectID")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_vectID")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 150,column 5,is_stmt
        MOV       *-SP[1],*(0:0x0ce0)   ; [CPU_] |150| 
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 151,column 5,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |151| 
        ANDB      AL,#0xfe              ; [CPU_] |151| 
        LSR       AL,1                  ; [CPU_] |151| 
        MOV       *-SP[2],AL            ; [CPU_] |151| 
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 156,column 5,is_stmt
        CMP       *-SP[1],#3584         ; [CPU_] |156| 
        B         $C$L5,LO              ; [CPU_] |156| 
        ; branchcc occurs ; [] |156| 
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 158,column 9,is_stmt
        ADD       *-SP[2],#128          ; [CPU_] |158| 
$C$L5:    
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 166,column 5,is_stmt
 ESTOP0
$C$L6:    
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 170,column 5,is_stmt
        B         $C$L6,UNC             ; [CPU_] |170| 
        ; branch occurs ; [] |170| 
	.dwattr $C$DW$9, DW_AT_TI_end_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$9, DW_AT_TI_end_line(0xab)
	.dwattr $C$DW$9, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$9

	.sect	".text"
	.clink

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_illegalOperationHandler")
	.dwattr $C$DW$12, DW_AT_low_pc(_Interrupt_illegalOperationHandler)
	.dwattr $C$DW$12, DW_AT_high_pc(0x00)
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_Interrupt_illegalOperationHandler")
	.dwattr $C$DW$12, DW_AT_TI_begin_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$12, DW_AT_TI_begin_line(0xbd)
	.dwattr $C$DW$12, DW_AT_TI_begin_column(0x0d)
	.dwattr $C$DW$12, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 190,column 1,is_stmt,address _Interrupt_illegalOperationHandler

	.dwfde $C$DW$CIE, _Interrupt_illegalOperationHandler

;***************************************************************
;* FNAME: _Interrupt_illegalOperationHandler FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_Interrupt_illegalOperationHandler:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 195,column 5,is_stmt
 ESTOP0
$C$L7:    
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 199,column 5,is_stmt
        B         $C$L7,UNC             ; [CPU_] |199| 
        ; branch occurs ; [] |199| 
	.dwattr $C$DW$12, DW_AT_TI_end_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$12, DW_AT_TI_end_line(0xc8)
	.dwattr $C$DW$12, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$12

	.sect	".text"
	.clink

$C$DW$13	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_nmiHandler")
	.dwattr $C$DW$13, DW_AT_low_pc(_Interrupt_nmiHandler)
	.dwattr $C$DW$13, DW_AT_high_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_Interrupt_nmiHandler")
	.dwattr $C$DW$13, DW_AT_TI_begin_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$13, DW_AT_TI_begin_line(0xd9)
	.dwattr $C$DW$13, DW_AT_TI_begin_column(0x0d)
	.dwattr $C$DW$13, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 218,column 1,is_stmt,address _Interrupt_nmiHandler

	.dwfde $C$DW$CIE, _Interrupt_nmiHandler

;***************************************************************
;* FNAME: _Interrupt_nmiHandler         FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_Interrupt_nmiHandler:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 224,column 5,is_stmt
 ESTOP0
$C$L8:    
	.dwpsn	file "..\ExtraData\driverlib2\interrupt.h",line 228,column 5,is_stmt
        B         $C$L8,UNC             ; [CPU_] |228| 
        ; branch occurs ; [] |228| 
	.dwattr $C$DW$13, DW_AT_TI_end_file("..\ExtraData\driverlib2\interrupt.h")
	.dwattr $C$DW$13, DW_AT_TI_end_line(0xe5)
	.dwattr $C$DW$13, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$13

	.sect	".text"
	.clink

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_clearIFR")
	.dwattr $C$DW$14, DW_AT_low_pc(_Interrupt_clearIFR)
	.dwattr $C$DW$14, DW_AT_high_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_Interrupt_clearIFR")
	.dwattr $C$DW$14, DW_AT_TI_begin_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$14, DW_AT_TI_begin_line(0x38)
	.dwattr $C$DW$14, DW_AT_TI_begin_column(0x0d)
	.dwattr $C$DW$14, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 57,column 1,is_stmt,address _Interrupt_clearIFR

	.dwfde $C$DW$CIE, _Interrupt_clearIFR
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("group")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_group")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Interrupt_clearIFR           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_Interrupt_clearIFR:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("group")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_group")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |57| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 58,column 5,is_stmt
        B         $C$L25,UNC            ; [CPU_] |58| 
        ; branch occurs ; [] |58| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 61,column 13,is_stmt
        AND       IFR,#0xfffe           ; [CPU_] |61| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 62,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |62| 
        ; branch occurs ; [] |62| 
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 64,column 13,is_stmt
        AND       IFR,#0xfffd           ; [CPU_] |64| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 65,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |65| 
        ; branch occurs ; [] |65| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 67,column 13,is_stmt
        AND       IFR,#0xfffb           ; [CPU_] |67| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 68,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |68| 
        ; branch occurs ; [] |68| 
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 70,column 13,is_stmt
        AND       IFR,#0xfff7           ; [CPU_] |70| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 71,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |71| 
        ; branch occurs ; [] |71| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 73,column 13,is_stmt
        AND       IFR,#0xffef           ; [CPU_] |73| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 74,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |74| 
        ; branch occurs ; [] |74| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 76,column 13,is_stmt
        AND       IFR,#0xffdf           ; [CPU_] |76| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 77,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |77| 
        ; branch occurs ; [] |77| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 79,column 13,is_stmt
        AND       IFR,#0xffbf           ; [CPU_] |79| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 80,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |80| 
        ; branch occurs ; [] |80| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 82,column 13,is_stmt
        AND       IFR,#0xff7f           ; [CPU_] |82| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 83,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |83| 
        ; branch occurs ; [] |83| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 85,column 13,is_stmt
        AND       IFR,#0xfeff           ; [CPU_] |85| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 86,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |86| 
        ; branch occurs ; [] |86| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 88,column 13,is_stmt
        AND       IFR,#0xfdff           ; [CPU_] |88| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 89,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |89| 
        ; branch occurs ; [] |89| 
$C$L19:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 91,column 13,is_stmt
        AND       IFR,#0xfbff           ; [CPU_] |91| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 92,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |92| 
        ; branch occurs ; [] |92| 
$C$L20:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 94,column 13,is_stmt
        AND       IFR,#0xf7ff           ; [CPU_] |94| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 95,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |95| 
        ; branch occurs ; [] |95| 
$C$L21:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 97,column 13,is_stmt
        AND       IFR,#0xefff           ; [CPU_] |97| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 98,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |98| 
        ; branch occurs ; [] |98| 
$C$L22:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 100,column 13,is_stmt
        AND       IFR,#0xdfff           ; [CPU_] |100| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 101,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |101| 
        ; branch occurs ; [] |101| 
$C$L23:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 103,column 13,is_stmt
        AND       IFR,#0xbfff           ; [CPU_] |103| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 104,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |104| 
        ; branch occurs ; [] |104| 
$C$L24:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 106,column 13,is_stmt
        AND       IFR,#0x7fff           ; [CPU_] |106| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 107,column 13,is_stmt
        B         $C$L29,UNC            ; [CPU_] |107| 
        ; branch occurs ; [] |107| 
$C$L25:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 58,column 5,is_stmt
        CMPB      AL,#128               ; [CPU_] |58| 
        MOVZ      AR6,*-SP[1]           ; [CPU_] |58| 
        B         $C$L27,GT             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMPB      AL,#128               ; [CPU_] |58| 
        BF        $C$L16,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMPB      AL,#8                 ; [CPU_] |58| 
        B         $C$L26,GT             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMPB      AL,#8                 ; [CPU_] |58| 
        BF        $C$L12,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CLRC      SXM                   ; [CPU_] 
        MOVZ      AR7,AR6               ; [CPU_] |58| 
        MOV       ACC,#32768            ; [CPU_] |58| 
        CMPL      ACC,XAR7              ; [CPU_] |58| 
        BF        $C$L24,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOV       AL,AR6                ; [CPU_] 
        CMPB      AL,#1                 ; [CPU_] |58| 
        BF        $C$L9,EQ              ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMPB      AL,#2                 ; [CPU_] |58| 
        BF        $C$L10,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMPB      AL,#4                 ; [CPU_] |58| 
        BF        $C$L11,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        B         $C$L29,UNC            ; [CPU_] |58| 
        ; branch occurs ; [] |58| 
$C$L26:    
        CMPB      AL,#16                ; [CPU_] |58| 
        BF        $C$L13,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMPB      AL,#32                ; [CPU_] |58| 
        BF        $C$L14,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMPB      AL,#64                ; [CPU_] |58| 
        BF        $C$L15,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        B         $C$L29,UNC            ; [CPU_] |58| 
        ; branch occurs ; [] |58| 
$C$L27:    
        CMP       AR6,#2048             ; [CPU_] |58| 
        B         $C$L28,GT             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        CMP       AR6,#2048             ; [CPU_] |58| 
        BF        $C$L20,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOVZ      AR7,AR6               ; [CPU_] |58| 
        MOV       ACC,#256              ; [CPU_] |58| 
        CMPL      ACC,XAR7              ; [CPU_] |58| 
        BF        $C$L17,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOVZ      AR7,AR6               ; [CPU_] |58| 
        MOV       ACC,#512              ; [CPU_] |58| 
        CMPL      ACC,XAR7              ; [CPU_] |58| 
        BF        $C$L18,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOVZ      AR6,AR6               ; [CPU_] |58| 
        MOV       ACC,#1024             ; [CPU_] |58| 
        CMPL      ACC,XAR6              ; [CPU_] |58| 
        BF        $C$L19,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        B         $C$L29,UNC            ; [CPU_] |58| 
        ; branch occurs ; [] |58| 
$C$L28:    
        MOVZ      AR7,AR6               ; [CPU_] |58| 
        MOV       ACC,#4096             ; [CPU_] |58| 
        CMPL      ACC,XAR7              ; [CPU_] |58| 
        BF        $C$L21,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOVZ      AR7,AR6               ; [CPU_] |58| 
        MOV       ACC,#8192             ; [CPU_] |58| 
        CMPL      ACC,XAR7              ; [CPU_] |58| 
        BF        $C$L22,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        MOVZ      AR6,AR6               ; [CPU_] |58| 
        MOV       ACC,#16384            ; [CPU_] |58| 
        CMPL      ACC,XAR6              ; [CPU_] |58| 
        BF        $C$L23,EQ             ; [CPU_] |58| 
        ; branchcc occurs ; [] |58| 
        B         $C$L29,UNC            ; [CPU_] |58| 
        ; branch occurs ; [] |58| 
$C$L29:    
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$17	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$17, DW_AT_low_pc(0x00)
	.dwattr $C$DW$17, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$14, DW_AT_TI_end_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$14, DW_AT_TI_end_line(0x73)
	.dwattr $C$DW$14, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$14

	.sect	".text"
	.clink
	.global	_Interrupt_initModule

$C$DW$18	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_initModule")
	.dwattr $C$DW$18, DW_AT_low_pc(_Interrupt_initModule)
	.dwattr $C$DW$18, DW_AT_high_pc(0x00)
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_Interrupt_initModule")
	.dwattr $C$DW$18, DW_AT_external
	.dwattr $C$DW$18, DW_AT_TI_begin_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$18, DW_AT_TI_begin_line(0x7b)
	.dwattr $C$DW$18, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$18, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 124,column 1,is_stmt,address _Interrupt_initModule

	.dwfde $C$DW$CIE, _Interrupt_initModule

;***************************************************************
;* FNAME: _Interrupt_initModule         FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_Interrupt_initModule:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 128,column 5,is_stmt
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("_Interrupt_disableMaster")
	.dwattr $C$DW$19, DW_AT_TI_call
        LCR       #_Interrupt_disableMaster ; [CPU_] |128| 
        ; call occurs [#_Interrupt_disableMaster] ; [] |128| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 129,column 5,is_stmt
        AND       IER,#0x0000           ; [CPU_] |129| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 130,column 5,is_stmt
        AND       IFR,#0x0000           ; [CPU_] |130| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 135,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |135| 
        MOV       *(0:0x0ce2),AL        ; [CPU_] |135| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 136,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |136| 
        MOV       *(0:0x0ce4),AL        ; [CPU_] |136| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 137,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |137| 
        MOV       *(0:0x0ce6),AL        ; [CPU_] |137| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 138,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |138| 
        MOV       *(0:0x0ce8),AL        ; [CPU_] |138| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 139,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |139| 
        MOV       *(0:0x0cea),AL        ; [CPU_] |139| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 140,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |140| 
        MOV       *(0:0x0cec),AL        ; [CPU_] |140| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 141,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |141| 
        MOV       *(0:0x0cee),AL        ; [CPU_] |141| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 142,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |142| 
        MOV       *(0:0x0cf0),AL        ; [CPU_] |142| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 143,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |143| 
        MOV       *(0:0x0cf2),AL        ; [CPU_] |143| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 144,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |144| 
        MOV       *(0:0x0cf4),AL        ; [CPU_] |144| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 145,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |145| 
        MOV       *(0:0x0cf6),AL        ; [CPU_] |145| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 146,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |146| 
        MOV       *(0:0x0cf8),AL        ; [CPU_] |146| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 151,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |151| 
        MOV       *(0:0x0ce3),AL        ; [CPU_] |151| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 152,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |152| 
        MOV       *(0:0x0ce5),AL        ; [CPU_] |152| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 153,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |153| 
        MOV       *(0:0x0ce7),AL        ; [CPU_] |153| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 154,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |154| 
        MOV       *(0:0x0ce9),AL        ; [CPU_] |154| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 155,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |155| 
        MOV       *(0:0x0ceb),AL        ; [CPU_] |155| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 156,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |156| 
        MOV       *(0:0x0ced),AL        ; [CPU_] |156| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 157,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |157| 
        MOV       *(0:0x0cef),AL        ; [CPU_] |157| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 158,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |158| 
        MOV       *(0:0x0cf1),AL        ; [CPU_] |158| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 159,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |159| 
        MOV       *(0:0x0cf3),AL        ; [CPU_] |159| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 160,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |160| 
        MOV       *(0:0x0cf5),AL        ; [CPU_] |160| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 161,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |161| 
        MOV       *(0:0x0cf7),AL        ; [CPU_] |161| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 162,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |162| 
        MOV       *(0:0x0cf9),AL        ; [CPU_] |162| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 167,column 5,is_stmt
        MOV       AL,*(0:0x0ce0)        ; [CPU_] |167| 
        ORB       AL,#0x01              ; [CPU_] |167| 
        MOV       *(0:0x0ce0),AL        ; [CPU_] |167| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 169,column 1,is_stmt
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$18, DW_AT_TI_end_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$18, DW_AT_TI_end_line(0xa9)
	.dwattr $C$DW$18, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$18

	.sect	".text"
	.clink
	.global	_Interrupt_initVectorTable

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_initVectorTable")
	.dwattr $C$DW$21, DW_AT_low_pc(_Interrupt_initVectorTable)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_Interrupt_initVectorTable")
	.dwattr $C$DW$21, DW_AT_external
	.dwattr $C$DW$21, DW_AT_TI_begin_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0xb1)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 178,column 1,is_stmt,address _Interrupt_initVectorTable

	.dwfde $C$DW$CIE, _Interrupt_initVectorTable

;***************************************************************
;* FNAME: _Interrupt_initVectorTable    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_Interrupt_initVectorTable:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$22	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_breg20 -1]
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 181,column 5,is_stmt
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("___eallow")
	.dwattr $C$DW$23, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |181| 
        ; call occurs [#___eallow] ; [] |181| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 187,column 9,is_stmt
        MOVB      *-SP[1],#3,UNC        ; [CPU_] |187| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 187,column 17,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |187| 
        CMPB      AL,#224               ; [CPU_] |187| 
        B         $C$L31,HIS            ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
$C$L30:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 189,column 9,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |189| 
        MOVL      XAR5,#_Interrupt_defaultHandler ; [CPU_U] |189| 
        MOVZ      AR4,AL                ; [CPU_] |189| 
        ADD       AR4,#3328             ; [CPU_] |189| 
        MOVL      *+XAR4[0],XAR5        ; [CPU_] |189| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 187,column 27,is_stmt
        INC       *-SP[1]               ; [CPU_] |187| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 187,column 17,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |187| 
        CMPB      AL,#224               ; [CPU_] |187| 
        B         $C$L30,LO             ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
$C$L31:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 196,column 5,is_stmt
        MOVL      XAR5,#_Interrupt_nmiHandler ; [CPU_U] |196| 
        MOVL      XAR4,#3364            ; [CPU_] |196| 
        MOVL      *+XAR4[0],XAR5        ; [CPU_] |196| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 198,column 5,is_stmt
        MOVL      XAR4,#3366            ; [CPU_] |198| 
        MOVL      XAR5,#_Interrupt_illegalOperationHandler ; [CPU_U] |198| 
        MOVL      *+XAR4[0],XAR5        ; [CPU_] |198| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 201,column 5,is_stmt
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("___edis")
	.dwattr $C$DW$24, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |201| 
        ; call occurs [#___edis] ; [] |201| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 202,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$21, DW_AT_TI_end_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0xca)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$21

	.sect	".text"
	.clink
	.global	_Interrupt_enable

$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_enable")
	.dwattr $C$DW$26, DW_AT_low_pc(_Interrupt_enable)
	.dwattr $C$DW$26, DW_AT_high_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_Interrupt_enable")
	.dwattr $C$DW$26, DW_AT_external
	.dwattr $C$DW$26, DW_AT_TI_begin_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$26, DW_AT_TI_begin_line(0xd2)
	.dwattr $C$DW$26, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$26, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 211,column 1,is_stmt,address _Interrupt_enable

	.dwfde $C$DW$CIE, _Interrupt_enable
$C$DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_name("interruptNumber")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_interruptNumber")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Interrupt_enable             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_Interrupt_enable:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("interruptNumber")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_interruptNumber")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -2]
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("intsDisabled")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_intsDisabled")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -3]
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("intGroup")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_intGroup")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_breg20 -4]
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("groupMask")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_groupMask")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -5]
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("vectID")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_vectID")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],ACC           ; [CPU_] |211| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 217,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |217| 
        MOVH      *-SP[6],ACC << 0      ; [CPU_] |217| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 222,column 5,is_stmt
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("_Interrupt_disableMaster")
	.dwattr $C$DW$33, DW_AT_TI_call
        LCR       #_Interrupt_disableMaster ; [CPU_] |222| 
        ; call occurs [#_Interrupt_disableMaster] ; [] |222| 
        MOV       *-SP[3],AL            ; [CPU_] |222| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 227,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |227| 
        CMPB      AL,#32                ; [CPU_] |227| 
        B         $C$L32,LO             ; [CPU_] |227| 
        ; branchcc occurs ; [] |227| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 229,column 9,is_stmt
        AND       AL,*-SP[2],#0xff00    ; [CPU_] |229| 
        LSR       AL,8                  ; [CPU_] |229| 
        ADDB      AL,#-1                ; [CPU_] |229| 
        MOV       *-SP[4],AL            ; [CPU_] |229| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 230,column 9,is_stmt
        MOV       T,*-SP[4]             ; [CPU_] |230| 
        MOVB      AL,#1                 ; [CPU_] |230| 
        LSL       AL,T                  ; [CPU_] |230| 
        MOV       *-SP[5],AL            ; [CPU_] |230| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 232,column 9,is_stmt
        MOV       ACC,*-SP[4] << #1     ; [CPU_] |232| 
        ADD       AL,#3298              ; [CPU_] |232| 
        MOVZ      AR4,AL                ; [CPU_] |232| 
        MOV       AL,*-SP[2]            ; [CPU_] |232| 
        ANDB      AL,#0xff              ; [CPU_] |232| 
        ADDB      AL,#-1                ; [CPU_] |232| 
        MOV       T,AL                  ; [CPU_] |232| 
        MOVB      AL,#1                 ; [CPU_] |232| 
        LSL       AL,T                  ; [CPU_] |232| 
        OR        *+XAR4[0],AL          ; [CPU_] |232| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 238,column 9,is_stmt
        MOV       AL,IER                ; [CPU_] |238| 
        OR        AL,*-SP[5]            ; [CPU_] |238| 
        MOV       IER,AL                ; [CPU_] |238| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 239,column 5,is_stmt
        B         $C$L33,UNC            ; [CPU_] |239| 
        ; branch occurs ; [] |239| 
$C$L32:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 244,column 10,is_stmt
        CMPB      AL,#13                ; [CPU_] |244| 
        B         $C$L33,LO             ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
        CMPB      AL,#16                ; [CPU_] |244| 
        B         $C$L33,HI             ; [CPU_] |244| 
        ; branchcc occurs ; [] |244| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 246,column 9,is_stmt
        ADDB      AL,#-1                ; [CPU_] |246| 
        MOV       T,AL                  ; [CPU_] |246| 
        MOVB      AL,#1                 ; [CPU_] |246| 
        LSL       AL,T                  ; [CPU_] |246| 
        MOV       AH,IER                ; [CPU_] |246| 
        OR        AL,AH                 ; [CPU_] |246| 
        MOV       IER,AL                ; [CPU_] |246| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 247,column 5,is_stmt
$C$L33:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 258,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |258| 
        BF        $C$L34,NEQ            ; [CPU_] |258| 
        ; branchcc occurs ; [] |258| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 260,column 9,is_stmt
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_Interrupt_enableMaster")
	.dwattr $C$DW$34, DW_AT_TI_call
        LCR       #_Interrupt_enableMaster ; [CPU_] |260| 
        ; call occurs [#_Interrupt_enableMaster] ; [] |260| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 262,column 1,is_stmt
$C$L34:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$26, DW_AT_TI_end_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$26, DW_AT_TI_end_line(0x106)
	.dwattr $C$DW$26, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$26

	.sect	".text"
	.clink
	.global	_Interrupt_disable

$C$DW$36	.dwtag  DW_TAG_subprogram, DW_AT_name("Interrupt_disable")
	.dwattr $C$DW$36, DW_AT_low_pc(_Interrupt_disable)
	.dwattr $C$DW$36, DW_AT_high_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_Interrupt_disable")
	.dwattr $C$DW$36, DW_AT_external
	.dwattr $C$DW$36, DW_AT_TI_begin_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$36, DW_AT_TI_begin_line(0x10e)
	.dwattr $C$DW$36, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$36, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 271,column 1,is_stmt,address _Interrupt_disable

	.dwfde $C$DW$CIE, _Interrupt_disable
$C$DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_name("interruptNumber")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_interruptNumber")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Interrupt_disable            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_Interrupt_disable:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("interruptNumber")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_interruptNumber")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -2]
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("intsDisabled")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_intsDisabled")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -3]
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("intGroup")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_intGroup")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg20 -4]
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("groupMask")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_groupMask")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_breg20 -5]
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("vectID")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_vectID")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],ACC           ; [CPU_] |271| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 277,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |277| 
        MOVH      *-SP[6],ACC << 0      ; [CPU_] |277| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 279,column 5,is_stmt
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("_Interrupt_disableMaster")
	.dwattr $C$DW$43, DW_AT_TI_call
        LCR       #_Interrupt_disableMaster ; [CPU_] |279| 
        ; call occurs [#_Interrupt_disableMaster] ; [] |279| 
        MOV       *-SP[3],AL            ; [CPU_] |279| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 284,column 5,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |284| 
        CMPB      AL,#32                ; [CPU_] |284| 
        B         $C$L35,LO             ; [CPU_] |284| 
        ; branchcc occurs ; [] |284| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 286,column 9,is_stmt
        AND       AL,*-SP[2],#0xff00    ; [CPU_] |286| 
        LSR       AL,8                  ; [CPU_] |286| 
        ADDB      AL,#-1                ; [CPU_] |286| 
        MOV       *-SP[4],AL            ; [CPU_] |286| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 287,column 9,is_stmt
        MOV       T,*-SP[4]             ; [CPU_] |287| 
        MOVB      AL,#1                 ; [CPU_] |287| 
        LSL       AL,T                  ; [CPU_] |287| 
        MOV       *-SP[5],AL            ; [CPU_] |287| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 292,column 9,is_stmt
        MOV       ACC,*-SP[4] << #1     ; [CPU_] |292| 
        ADD       AL,#3298              ; [CPU_] |292| 
        MOVZ      AR4,AL                ; [CPU_] |292| 
        MOV       AL,*-SP[2]            ; [CPU_] |292| 
        ANDB      AL,#0xff              ; [CPU_] |292| 
        ADDB      AL,#-1                ; [CPU_] |292| 
        MOV       T,AL                  ; [CPU_] |292| 
        MOVB      AL,#1                 ; [CPU_] |292| 
        LSL       AL,T                  ; [CPU_] |292| 
        NOT       AL                    ; [CPU_] |292| 
        AND       *+XAR4[0],AL          ; [CPU_] |292| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 298,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 299,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 300,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 301,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 302,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 304,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |304| 
        SPM       #0                    ; [CPU_] 
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("_Interrupt_clearIFR")
	.dwattr $C$DW$44, DW_AT_TI_call
        LCR       #_Interrupt_clearIFR  ; [CPU_] |304| 
        ; call occurs [#_Interrupt_clearIFR] ; [] |304| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 309,column 9,is_stmt
        MOV       *(0:0x0ce1),*-SP[5]   ; [CPU_] |309| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 310,column 5,is_stmt
        B         $C$L36,UNC            ; [CPU_] |310| 
        ; branch occurs ; [] |310| 
$C$L35:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 315,column 10,is_stmt
        CMPB      AL,#13                ; [CPU_] |315| 
        B         $C$L36,LO             ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
        CMPB      AL,#16                ; [CPU_] |315| 
        B         $C$L36,HI             ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 317,column 9,is_stmt
        ADDB      AL,#-1                ; [CPU_] |317| 
        MOV       T,AL                  ; [CPU_] |317| 
        MOVB      AL,#1                 ; [CPU_] |317| 
        LSL       AL,T                  ; [CPU_] |317| 
        NOT       AL                    ; [CPU_] |317| 
        MOV       AH,IER                ; [CPU_] |317| 
        AND       AL,AH                 ; [CPU_] |317| 
        MOV       IER,AL                ; [CPU_] |317| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 322,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 323,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 324,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 325,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 326,column 9,is_stmt
 NOP
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 328,column 9,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |328| 
        SPM       #0                    ; [CPU_] 
        ADDB      AL,#-1                ; [CPU_] |328| 
        MOV       T,AL                  ; [CPU_] |328| 
        MOVB      AL,#1                 ; [CPU_] |328| 
        LSL       AL,T                  ; [CPU_] |328| 
$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_name("_Interrupt_clearIFR")
	.dwattr $C$DW$45, DW_AT_TI_call
        LCR       #_Interrupt_clearIFR  ; [CPU_] |328| 
        ; call occurs [#_Interrupt_clearIFR] ; [] |328| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 329,column 5,is_stmt
$C$L36:    
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 340,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |340| 
        BF        $C$L37,NEQ            ; [CPU_] |340| 
        ; branchcc occurs ; [] |340| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 342,column 9,is_stmt
$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_name("_Interrupt_enableMaster")
	.dwattr $C$DW$46, DW_AT_TI_call
        LCR       #_Interrupt_enableMaster ; [CPU_] |342| 
        ; call occurs [#_Interrupt_enableMaster] ; [] |342| 
	.dwpsn	file "../ExtraData/driverlib2/interrupt.c",line 344,column 1,is_stmt
$C$L37:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$47	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$47, DW_AT_low_pc(0x00)
	.dwattr $C$DW$47, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$36, DW_AT_TI_end_file("../ExtraData/driverlib2/interrupt.c")
	.dwattr $C$DW$36, DW_AT_TI_end_line(0x158)
	.dwattr $C$DW$36, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$36

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$21	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$21, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$21, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("uintptr_t")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg0]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg1]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg2]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg3]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg20]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg21]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg22]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg23]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg24]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg25]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg26]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg28]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg29]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg30]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg31]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x20]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x21]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x22]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x23]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x24]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x25]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x26]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg4]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg6]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg8]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg10]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_reg12]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg14]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg16]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg17]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg18]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg19]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg5]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg7]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg9]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg11]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg13]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg15]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x30]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x33]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x34]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x37]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x38]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x40]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x43]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x44]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x47]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x48]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x49]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x27]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x28]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg27]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

