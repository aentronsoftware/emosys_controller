;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\0776012 
	.sect	".text"
	.clink

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("XBAR_setInputPin")
	.dwattr $C$DW$3, DW_AT_low_pc(_XBAR_setInputPin)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_XBAR_setInputPin")
	.dwattr $C$DW$3, DW_AT_TI_begin_file("..\ExtraData\driverlib2\xbar.h")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x3b0)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\xbar.h",line 945,column 1,is_stmt,address _XBAR_setInputPin

	.dwfde $C$DW$CIE, _XBAR_setInputPin
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("input")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_input")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _XBAR_setInputPin             FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_XBAR_setInputPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("input")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_input")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -1]
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[2],AH            ; [CPU_] |945| 
        MOV       *-SP[1],AL            ; [CPU_] |945| 
	.dwpsn	file "..\ExtraData\driverlib2\xbar.h",line 955,column 5,is_stmt
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_name("___eallow")
	.dwattr $C$DW$8, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |955| 
        ; call occurs [#___eallow] ; [] |955| 
	.dwpsn	file "..\ExtraData\driverlib2\xbar.h",line 957,column 5,is_stmt
        MOVZ      AR4,*-SP[1]           ; [CPU_] |957| 
        MOV       AL,*-SP[2]            ; [CPU_] |957| 
        ADD       AR4,#30976            ; [CPU_] |957| 
        MOV       *+XAR4[0],AL          ; [CPU_] |957| 
	.dwpsn	file "..\ExtraData\driverlib2\xbar.h",line 959,column 5,is_stmt
$C$DW$9	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$9, DW_AT_low_pc(0x00)
	.dwattr $C$DW$9, DW_AT_name("___edis")
	.dwattr $C$DW$9, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |959| 
        ; call occurs [#___edis] ; [] |959| 
	.dwpsn	file "..\ExtraData\driverlib2\xbar.h",line 960,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("..\ExtraData\driverlib2\xbar.h")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x3c0)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink
	.global	_GPIO_setDirectionMode

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setDirectionMode")
	.dwattr $C$DW$11, DW_AT_low_pc(_GPIO_setDirectionMode)
	.dwattr $C$DW$11, DW_AT_high_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_GPIO_setDirectionMode")
	.dwattr $C$DW$11, DW_AT_external
	.dwattr $C$DW$11, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$11, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$11, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$11, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 52,column 1,is_stmt,address _GPIO_setDirectionMode

	.dwfde $C$DW$CIE, _GPIO_setDirectionMode
$C$DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg0]
$C$DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pinIO")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_pinIO")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _GPIO_setDirectionMode        FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_GPIO_setDirectionMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -2]
$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("pinIO")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_pinIO")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -3]
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -6]
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("pinMask")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_pinMask")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[3],AR4           ; [CPU_] |52| 
        MOVL      *-SP[2],ACC           ; [CPU_] |52| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 61,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |61| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |61| 
        SFR       ACC,5                 ; [CPU_] |61| 
        LSL       ACC,5                 ; [CPU_] |61| 
        LSL       ACC,1                 ; [CPU_] |61| 
        ADDL      XAR4,ACC              ; [CPU_] |61| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |61| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 63,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |63| 
        MOVL      XAR6,ACC              ; [CPU_] |63| 
        AND       AL,*-SP[2],#0x001f    ; [CPU_] |63| 
        MOV       T,AL                  ; [CPU_] |63| 
        MOVL      ACC,XAR6              ; [CPU_] |63| 
        LSLL      ACC,T                 ; [CPU_] |63| 
        MOVL      *-SP[8],ACC           ; [CPU_] |63| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 65,column 5,is_stmt
$C$DW$18	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$18, DW_AT_low_pc(0x00)
	.dwattr $C$DW$18, DW_AT_name("___eallow")
	.dwattr $C$DW$18, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |65| 
        ; call occurs [#___eallow] ; [] |65| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 70,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |70| 
        CMPB      AL,#1                 ; [CPU_] |70| 
        BF        $C$L1,NEQ             ; [CPU_] |70| 
        ; branchcc occurs ; [] |70| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 75,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |75| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |75| 
        ADDB      XAR4,#10              ; [CPU_] |75| 
        OR        *+XAR4[0],AL          ; [CPU_] |75| 
        OR        *+XAR4[1],AH          ; [CPU_] |75| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 76,column 5,is_stmt
        B         $C$L2,UNC             ; [CPU_] |76| 
        ; branch occurs ; [] |76| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 82,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |82| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |82| 
        NOT       ACC                   ; [CPU_] |82| 
        ADDB      XAR4,#10              ; [CPU_] |82| 
        AND       *+XAR4[0],AL          ; [CPU_] |82| 
        AND       *+XAR4[1],AH          ; [CPU_] |82| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 85,column 5,is_stmt
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("___edis")
	.dwattr $C$DW$19, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |85| 
        ; call occurs [#___edis] ; [] |85| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 86,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$11, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$11, DW_AT_TI_end_line(0x56)
	.dwattr $C$DW$11, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$11

	.sect	".text"
	.clink
	.global	_GPIO_getDirectionMode

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_getDirectionMode")
	.dwattr $C$DW$21, DW_AT_low_pc(_GPIO_getDirectionMode)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_GPIO_getDirectionMode")
	.dwattr $C$DW$21, DW_AT_external
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$21, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0x5e)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 95,column 1,is_stmt,address _GPIO_getDirectionMode

	.dwfde $C$DW$CIE, _GPIO_getDirectionMode
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _GPIO_getDirectionMode        FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_GPIO_getDirectionMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -2]
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[2],ACC           ; [CPU_] |95| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 103,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |103| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |103| 
        SFR       ACC,5                 ; [CPU_] |103| 
        LSL       ACC,5                 ; [CPU_] |103| 
        LSL       ACC,1                 ; [CPU_] |103| 
        ADDL      XAR4,ACC              ; [CPU_] |103| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |103| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 106,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |106| 
        AND       AL,*-SP[2],#0x001f    ; [CPU_] |106| 
        MOVB      XAR0,#10              ; [CPU_] |106| 
        MOV       T,AL                  ; [CPU_] |106| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |106| 
        LSRL      ACC,T                 ; [CPU_] |106| 
        MOVL      XAR6,ACC              ; [CPU_] |106| 
        MOV       AL,AR6                ; [CPU_] |106| 
        ANDB      AL,#0x01              ; [CPU_] |106| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 109,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$21, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x6d)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$21

	.sect	".text"
	.clink
	.global	_GPIO_setInterruptPin

$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setInterruptPin")
	.dwattr $C$DW$26, DW_AT_low_pc(_GPIO_setInterruptPin)
	.dwattr $C$DW$26, DW_AT_high_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_GPIO_setInterruptPin")
	.dwattr $C$DW$26, DW_AT_external
	.dwattr $C$DW$26, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$26, DW_AT_TI_begin_line(0x75)
	.dwattr $C$DW$26, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$26, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 118,column 1,is_stmt,address _GPIO_setInterruptPin

	.dwfde $C$DW$CIE, _GPIO_setInterruptPin
$C$DW$27	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg0]
$C$DW$28	.dwtag  DW_TAG_formal_parameter, DW_AT_name("extIntNum")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_extIntNum")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _GPIO_setInterruptPin         FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_GPIO_setInterruptPin:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$29	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_breg20 -2]
$C$DW$30	.dwtag  DW_TAG_variable, DW_AT_name("extIntNum")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_extIntNum")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_breg20 -3]
$C$DW$31	.dwtag  DW_TAG_variable, DW_AT_name("input")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_input")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[3],AR4           ; [CPU_] |118| 
        MOVL      *-SP[2],ACC           ; [CPU_] |118| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 129,column 5,is_stmt
        B         $C$L9,UNC             ; [CPU_] |129| 
        ; branch occurs ; [] |129| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 132,column 13,is_stmt
        MOVB      *-SP[4],#3,UNC        ; [CPU_] |132| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 133,column 13,is_stmt
        B         $C$L11,UNC            ; [CPU_] |133| 
        ; branch occurs ; [] |133| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 136,column 13,is_stmt
        MOVB      *-SP[4],#4,UNC        ; [CPU_] |136| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 137,column 13,is_stmt
        B         $C$L11,UNC            ; [CPU_] |137| 
        ; branch occurs ; [] |137| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 140,column 13,is_stmt
        MOVB      *-SP[4],#5,UNC        ; [CPU_] |140| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 141,column 13,is_stmt
        B         $C$L11,UNC            ; [CPU_] |141| 
        ; branch occurs ; [] |141| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 144,column 13,is_stmt
        MOVB      *-SP[4],#12,UNC       ; [CPU_] |144| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 145,column 13,is_stmt
        B         $C$L11,UNC            ; [CPU_] |145| 
        ; branch occurs ; [] |145| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 148,column 13,is_stmt
        MOVB      *-SP[4],#13,UNC       ; [CPU_] |148| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 149,column 13,is_stmt
        B         $C$L11,UNC            ; [CPU_] |149| 
        ; branch occurs ; [] |149| 
$C$L8:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 157,column 13,is_stmt
        MOV       *-SP[4],#0            ; [CPU_] |157| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 158,column 13,is_stmt
        B         $C$L11,UNC            ; [CPU_] |158| 
        ; branch occurs ; [] |158| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 129,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |129| 
        CMPB      AL,#2                 ; [CPU_] |129| 
        B         $C$L10,GT             ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        CMPB      AL,#2                 ; [CPU_] |129| 
        BF        $C$L5,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        CMPB      AL,#0                 ; [CPU_] |129| 
        BF        $C$L3,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        CMPB      AL,#1                 ; [CPU_] |129| 
        BF        $C$L4,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        B         $C$L8,UNC             ; [CPU_] |129| 
        ; branch occurs ; [] |129| 
$C$L10:    
        CMPB      AL,#3                 ; [CPU_] |129| 
        BF        $C$L6,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        CMPB      AL,#4                 ; [CPU_] |129| 
        BF        $C$L7,EQ              ; [CPU_] |129| 
        ; branchcc occurs ; [] |129| 
        B         $C$L8,UNC             ; [CPU_] |129| 
        ; branch occurs ; [] |129| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 161,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |161| 
        BF        $C$L12,EQ             ; [CPU_] |161| 
        ; branchcc occurs ; [] |161| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 163,column 9,is_stmt
        MOV       AH,*-SP[2]            ; [CPU_] |163| 
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("_XBAR_setInputPin")
	.dwattr $C$DW$32, DW_AT_TI_call
        LCR       #_XBAR_setInputPin    ; [CPU_] |163| 
        ; call occurs [#_XBAR_setInputPin] ; [] |163| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 165,column 1,is_stmt
$C$L12:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$26, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$26, DW_AT_TI_end_line(0xa5)
	.dwattr $C$DW$26, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$26

	.sect	".text"
	.clink
	.global	_GPIO_setPadConfig

$C$DW$34	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setPadConfig")
	.dwattr $C$DW$34, DW_AT_low_pc(_GPIO_setPadConfig)
	.dwattr $C$DW$34, DW_AT_high_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_GPIO_setPadConfig")
	.dwattr $C$DW$34, DW_AT_external
	.dwattr $C$DW$34, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$34, DW_AT_TI_begin_line(0xad)
	.dwattr $C$DW$34, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$34, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 174,column 1,is_stmt,address _GPIO_setPadConfig

	.dwfde $C$DW$CIE, _GPIO_setPadConfig
$C$DW$35	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg0]
$C$DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pinType")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_pinType")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_breg20 -10]

;***************************************************************
;* FNAME: _GPIO_setPadConfig            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_GPIO_setPadConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -2]
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_breg20 -4]
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("pinMask")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_pinMask")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[2],ACC           ; [CPU_] |174| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 183,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |183| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |183| 
        SFR       ACC,5                 ; [CPU_] |183| 
        LSL       ACC,5                 ; [CPU_] |183| 
        LSL       ACC,1                 ; [CPU_] |183| 
        ADDL      XAR4,ACC              ; [CPU_] |183| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |183| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 185,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |185| 
        MOVL      XAR6,ACC              ; [CPU_] |185| 
        AND       AL,*-SP[2],#0x001f    ; [CPU_] |185| 
        MOV       T,AL                  ; [CPU_] |185| 
        MOVL      ACC,XAR6              ; [CPU_] |185| 
        LSLL      ACC,T                 ; [CPU_] |185| 
        MOVL      *-SP[6],ACC           ; [CPU_] |185| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 187,column 5,is_stmt
$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("___eallow")
	.dwattr $C$DW$40, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |187| 
        ; call occurs [#___eallow] ; [] |187| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 192,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |192| 
        SUBB      XAR4,#10              ; [CPU_U] |192| 
        TBIT      *+XAR4[0],#2          ; [CPU_] |192| 
        BF        $C$L13,NTC            ; [CPU_] |192| 
        ; branchcc occurs ; [] |192| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 194,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |194| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |194| 
        ADDB      XAR4,#18              ; [CPU_] |194| 
        OR        *+XAR4[0],AL          ; [CPU_] |194| 
        OR        *+XAR4[1],AH          ; [CPU_] |194| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 195,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |195| 
        ; branch occurs ; [] |195| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 198,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |198| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |198| 
        NOT       ACC                   ; [CPU_] |198| 
        ADDB      XAR4,#18              ; [CPU_] |198| 
        AND       *+XAR4[0],AL          ; [CPU_] |198| 
        AND       *+XAR4[1],AH          ; [CPU_] |198| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 204,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |204| 
        SUBB      XAR4,#10              ; [CPU_U] |204| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |204| 
        BF        $C$L15,NTC            ; [CPU_] |204| 
        ; branchcc occurs ; [] |204| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 206,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |206| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |206| 
        NOT       ACC                   ; [CPU_] |206| 
        ADDB      XAR4,#12              ; [CPU_] |206| 
        AND       *+XAR4[0],AL          ; [CPU_] |206| 
        AND       *+XAR4[1],AH          ; [CPU_] |206| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 207,column 5,is_stmt
        B         $C$L16,UNC            ; [CPU_] |207| 
        ; branch occurs ; [] |207| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 210,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |210| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |210| 
        ADDB      XAR4,#12              ; [CPU_] |210| 
        OR        *+XAR4[0],AL          ; [CPU_] |210| 
        OR        *+XAR4[1],AH          ; [CPU_] |210| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 216,column 5,is_stmt
        MOVZ      AR4,SP                ; [CPU_U] |216| 
        SUBB      XAR4,#10              ; [CPU_U] |216| 
        TBIT      *+XAR4[0],#1          ; [CPU_] |216| 
        BF        $C$L17,NTC            ; [CPU_] |216| 
        ; branchcc occurs ; [] |216| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 218,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |218| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |218| 
        ADDB      XAR4,#16              ; [CPU_] |218| 
        OR        *+XAR4[0],AL          ; [CPU_] |218| 
        OR        *+XAR4[1],AH          ; [CPU_] |218| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 219,column 5,is_stmt
        B         $C$L18,UNC            ; [CPU_] |219| 
        ; branch occurs ; [] |219| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 222,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |222| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |222| 
        NOT       ACC                   ; [CPU_] |222| 
        ADDB      XAR4,#16              ; [CPU_] |222| 
        AND       *+XAR4[0],AL          ; [CPU_] |222| 
        AND       *+XAR4[1],AH          ; [CPU_] |222| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 225,column 5,is_stmt
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("___edis")
	.dwattr $C$DW$41, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |225| 
        ; call occurs [#___edis] ; [] |225| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 226,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$34, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$34, DW_AT_TI_end_line(0xe2)
	.dwattr $C$DW$34, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$34

	.sect	".text"
	.clink
	.global	_GPIO_getPadConfig

$C$DW$43	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_getPadConfig")
	.dwattr $C$DW$43, DW_AT_low_pc(_GPIO_getPadConfig)
	.dwattr $C$DW$43, DW_AT_high_pc(0x00)
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_GPIO_getPadConfig")
	.dwattr $C$DW$43, DW_AT_external
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$43, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$43, DW_AT_TI_begin_line(0xea)
	.dwattr $C$DW$43, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$43, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 235,column 1,is_stmt,address _GPIO_getPadConfig

	.dwfde $C$DW$CIE, _GPIO_getPadConfig
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _GPIO_getPadConfig            FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_GPIO_getPadConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -2]
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -4]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("pinMask")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_pinMask")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -6]
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("pinTypeRes")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_pinTypeRes")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[2],ACC           ; [CPU_] |235| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 245,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |245| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |245| 
        SFR       ACC,5                 ; [CPU_] |245| 
        LSL       ACC,5                 ; [CPU_] |245| 
        LSL       ACC,1                 ; [CPU_] |245| 
        ADDL      XAR4,ACC              ; [CPU_] |245| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |245| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 247,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |247| 
        MOVL      XAR6,ACC              ; [CPU_] |247| 
        AND       AL,*-SP[2],#0x001f    ; [CPU_] |247| 
        MOV       T,AL                  ; [CPU_] |247| 
        MOVL      ACC,XAR6              ; [CPU_] |247| 
        LSLL      ACC,T                 ; [CPU_] |247| 
        MOVL      *-SP[6],ACC           ; [CPU_] |247| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 249,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |249| 
        MOVL      *-SP[8],ACC           ; [CPU_] |249| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 254,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |254| 
        MOVB      XAR0,#18              ; [CPU_] |254| 
        MOVB      XAR6,#0               ; [CPU_] |254| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |254| 
        AND       AL,*-SP[6]            ; [CPU_] |254| 
        AND       AH,*-SP[5]            ; [CPU_] |254| 
        CMPL      ACC,XAR6              ; [CPU_] |254| 
        BF        $C$L19,EQ             ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 256,column 9,is_stmt
        OR        *-SP[8],#4            ; [CPU_] |256| 
$C$L19:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 262,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |262| 
        MOVB      XAR0,#12              ; [CPU_] |262| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |262| 
        AND       AL,*-SP[6]            ; [CPU_] |262| 
        AND       AH,*-SP[5]            ; [CPU_] |262| 
        CMPL      ACC,XAR6              ; [CPU_] |262| 
        BF        $C$L20,NEQ            ; [CPU_] |262| 
        ; branchcc occurs ; [] |262| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 264,column 9,is_stmt
        OR        *-SP[8],#1            ; [CPU_] |264| 
$C$L20:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 270,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |270| 
        MOVB      XAR0,#16              ; [CPU_] |270| 
        MOVL      ACC,*+XAR4[AR0]       ; [CPU_] |270| 
        AND       AL,*-SP[6]            ; [CPU_] |270| 
        AND       AH,*-SP[5]            ; [CPU_] |270| 
        CMPL      ACC,XAR6              ; [CPU_] |270| 
        BF        $C$L21,EQ             ; [CPU_] |270| 
        ; branchcc occurs ; [] |270| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 272,column 9,is_stmt
        OR        *-SP[8],#2            ; [CPU_] |272| 
$C$L21:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 275,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |275| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 276,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$43, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$43, DW_AT_TI_end_line(0x114)
	.dwattr $C$DW$43, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$43

	.sect	".text"
	.clink
	.global	_GPIO_setQualificationMode

$C$DW$50	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setQualificationMode")
	.dwattr $C$DW$50, DW_AT_low_pc(_GPIO_setQualificationMode)
	.dwattr $C$DW$50, DW_AT_high_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_GPIO_setQualificationMode")
	.dwattr $C$DW$50, DW_AT_external
	.dwattr $C$DW$50, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$50, DW_AT_TI_begin_line(0x11c)
	.dwattr $C$DW$50, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$50, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 285,column 1,is_stmt,address _GPIO_setQualificationMode

	.dwfde $C$DW$CIE, _GPIO_setQualificationMode
$C$DW$51	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg0]
$C$DW$52	.dwtag  DW_TAG_formal_parameter, DW_AT_name("qualification")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_qualification")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _GPIO_setQualificationMode    FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_GPIO_setQualificationMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_breg20 -2]
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("qualification")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_qualification")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_breg20 -3]
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_breg20 -6]
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("qSelIndex")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_qSelIndex")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_breg20 -8]
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("shiftAmt")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_shiftAmt")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[3],AR4           ; [CPU_] |285| 
        MOVL      *-SP[2],ACC           ; [CPU_] |285| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 295,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |295| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |295| 
        SFR       ACC,5                 ; [CPU_] |295| 
        LSL       ACC,5                 ; [CPU_] |295| 
        LSL       ACC,1                 ; [CPU_] |295| 
        ADDL      XAR4,ACC              ; [CPU_] |295| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |295| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 297,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |297| 
        MOVB      AH,#0                 ; [CPU_] |297| 
        ANDB      AL,#0x0f              ; [CPU_] |297| 
        LSL       ACC,1                 ; [CPU_] |297| 
        MOVL      *-SP[10],ACC          ; [CPU_] |297| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 298,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |298| 
        MOVB      AH,#0                 ; [CPU_] |298| 
        ANDB      AL,#0x1f              ; [CPU_] |298| 
        SFR       ACC,4                 ; [CPU_] |298| 
        ADDB      ACC,#1                ; [CPU_] |298| 
        MOVL      *-SP[8],ACC           ; [CPU_] |298| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 303,column 5,is_stmt
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_name("___eallow")
	.dwattr $C$DW$58, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |303| 
        ; call occurs [#___eallow] ; [] |303| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 305,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |305| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |305| 
        MOV       T,*-SP[10]            ; [CPU_] |305| 
        LSL       ACC,1                 ; [CPU_] |305| 
        ADDL      XAR4,ACC              ; [CPU_] |305| 
        MOVB      ACC,#3                ; [CPU_] |305| 
        LSLL      ACC,T                 ; [CPU_] |305| 
        NOT       ACC                   ; [CPU_] |305| 
        AND       *+XAR4[0],AL          ; [CPU_] |305| 
        AND       *+XAR4[1],AH          ; [CPU_] |305| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 306,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |306| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |306| 
        SETC      SXM                   ; [CPU_] 
        MOV       T,*-SP[10]            ; [CPU_] |306| 
        LSL       ACC,1                 ; [CPU_] |306| 
        ADDL      XAR4,ACC              ; [CPU_] |306| 
        MOV       ACC,*-SP[3]           ; [CPU_] |306| 
        LSLL      ACC,T                 ; [CPU_] |306| 
        OR        *+XAR4[0],AL          ; [CPU_] |306| 
        OR        *+XAR4[1],AH          ; [CPU_] |306| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 308,column 5,is_stmt
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_name("___edis")
	.dwattr $C$DW$59, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |308| 
        ; call occurs [#___edis] ; [] |308| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 309,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$50, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$50, DW_AT_TI_end_line(0x135)
	.dwattr $C$DW$50, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$50

	.sect	".text"
	.clink
	.global	_GPIO_getQualificationMode

$C$DW$61	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_getQualificationMode")
	.dwattr $C$DW$61, DW_AT_low_pc(_GPIO_getQualificationMode)
	.dwattr $C$DW$61, DW_AT_high_pc(0x00)
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_GPIO_getQualificationMode")
	.dwattr $C$DW$61, DW_AT_external
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$61, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$61, DW_AT_TI_begin_line(0x13d)
	.dwattr $C$DW$61, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$61, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 318,column 1,is_stmt,address _GPIO_getQualificationMode

	.dwfde $C$DW$CIE, _GPIO_getQualificationMode
$C$DW$62	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _GPIO_getQualificationMode    FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_GPIO_getQualificationMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_breg20 -2]
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -4]
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("qSelIndex")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_qSelIndex")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -6]
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("qualRes")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_qualRes")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_breg20 -8]
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("shiftAmt")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_shiftAmt")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_breg20 -10]
        MOVL      *-SP[2],ACC           ; [CPU_] |318| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 329,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |329| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |329| 
        SFR       ACC,5                 ; [CPU_] |329| 
        LSL       ACC,5                 ; [CPU_] |329| 
        LSL       ACC,1                 ; [CPU_] |329| 
        ADDL      XAR4,ACC              ; [CPU_] |329| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |329| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 331,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |331| 
        MOVB      AH,#0                 ; [CPU_] |331| 
        ANDB      AL,#0x0f              ; [CPU_] |331| 
        LSL       ACC,1                 ; [CPU_] |331| 
        MOVL      *-SP[10],ACC          ; [CPU_] |331| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 332,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |332| 
        MOVB      AH,#0                 ; [CPU_] |332| 
        ANDB      AL,#0x1f              ; [CPU_] |332| 
        SFR       ACC,4                 ; [CPU_] |332| 
        ADDB      ACC,#1                ; [CPU_] |332| 
        MOVL      *-SP[6],ACC           ; [CPU_] |332| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 338,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |338| 
        MOV       T,*-SP[10]            ; [CPU_] |338| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |338| 
        LSL       ACC,1                 ; [CPU_] |338| 
        ADDL      XAR4,ACC              ; [CPU_] |338| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |338| 
        LSRL      ACC,T                 ; [CPU_] |338| 
        MOVB      AH,#0                 ; [CPU_] |338| 
        ANDB      AL,#0x03              ; [CPU_] |338| 
        MOVL      *-SP[8],ACC           ; [CPU_] |338| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 340,column 5,is_stmt
        MOV       AL,*-SP[8]            ; [CPU_] |340| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 341,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$61, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$61, DW_AT_TI_end_line(0x155)
	.dwattr $C$DW$61, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$61

	.sect	".text"
	.clink
	.global	_GPIO_setQualificationPeriod

$C$DW$69	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setQualificationPeriod")
	.dwattr $C$DW$69, DW_AT_low_pc(_GPIO_setQualificationPeriod)
	.dwattr $C$DW$69, DW_AT_high_pc(0x00)
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_GPIO_setQualificationPeriod")
	.dwattr $C$DW$69, DW_AT_external
	.dwattr $C$DW$69, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$69, DW_AT_TI_begin_line(0x15d)
	.dwattr $C$DW$69, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$69, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 350,column 1,is_stmt,address _GPIO_setQualificationPeriod

	.dwfde $C$DW$CIE, _GPIO_setQualificationPeriod
$C$DW$70	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg0]
$C$DW$71	.dwtag  DW_TAG_formal_parameter, DW_AT_name("divider")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_divider")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -14]

;***************************************************************
;* FNAME: _GPIO_setQualificationPeriod  FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_GPIO_setQualificationPeriod:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_breg20 -2]
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_breg20 -4]
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("pinMask")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_pinMask")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_breg20 -6]
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("regVal")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_regVal")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -8]
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("shiftAmt")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_shiftAmt")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_breg20 -10]
        MOVL      *-SP[2],ACC           ; [CPU_] |350| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 360,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |360| 
        MOVB      AH,#0                 ; [CPU_] |360| 
        ANDB      AL,#0x18              ; [CPU_] |360| 
        MOVL      *-SP[10],ACC          ; [CPU_] |360| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 361,column 5,is_stmt
        MOV       T,*-SP[10]            ; [CPU_] |361| 
        MOVB      ACC,#255              ; [CPU_] |361| 
        LSLL      ACC,T                 ; [CPU_] |361| 
        MOVL      *-SP[6],ACC           ; [CPU_] |361| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 367,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*-SP[14]          ; [CPU_] |367| 
        SFR       ACC,1                 ; [CPU_] |367| 
        LSLL      ACC,T                 ; [CPU_] |367| 
        MOVL      *-SP[8],ACC           ; [CPU_] |367| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 372,column 5,is_stmt
        MOVL      XAR4,#31744           ; [CPU_U] |372| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |372| 
        SFR       ACC,5                 ; [CPU_] |372| 
        LSL       ACC,5                 ; [CPU_] |372| 
        LSL       ACC,1                 ; [CPU_] |372| 
        ADDL      XAR4,ACC              ; [CPU_] |372| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |372| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 375,column 5,is_stmt
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("___eallow")
	.dwattr $C$DW$77, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |375| 
        ; call occurs [#___eallow] ; [] |375| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 376,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |376| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |376| 
        NOT       ACC                   ; [CPU_] |376| 
        AND       *+XAR4[0],AL          ; [CPU_] |376| 
        AND       *+XAR4[1],AH          ; [CPU_] |376| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 377,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |377| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |377| 
        OR        *+XAR4[0],AL          ; [CPU_] |377| 
        OR        *+XAR4[1],AH          ; [CPU_] |377| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 378,column 5,is_stmt
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_name("___edis")
	.dwattr $C$DW$78, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |378| 
        ; call occurs [#___edis] ; [] |378| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 379,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$79	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$79, DW_AT_low_pc(0x00)
	.dwattr $C$DW$79, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$69, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$69, DW_AT_TI_end_line(0x17b)
	.dwattr $C$DW$69, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$69

	.sect	".text"
	.clink
	.global	_GPIO_setMasterCore

$C$DW$80	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setMasterCore")
	.dwattr $C$DW$80, DW_AT_low_pc(_GPIO_setMasterCore)
	.dwattr $C$DW$80, DW_AT_high_pc(0x00)
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_GPIO_setMasterCore")
	.dwattr $C$DW$80, DW_AT_external
	.dwattr $C$DW$80, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$80, DW_AT_TI_begin_line(0x183)
	.dwattr $C$DW$80, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$80, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 388,column 1,is_stmt,address _GPIO_setMasterCore

	.dwfde $C$DW$CIE, _GPIO_setMasterCore
$C$DW$81	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg0]
$C$DW$82	.dwtag  DW_TAG_formal_parameter, DW_AT_name("core")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_core")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _GPIO_setMasterCore           FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_GPIO_setMasterCore:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_breg20 -2]
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("core")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_core")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -3]
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -6]
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("cSelIndex")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_cSelIndex")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_breg20 -8]
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("shiftAmt")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_shiftAmt")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_breg20 -10]
        MOV       *-SP[3],AR4           ; [CPU_] |388| 
        MOVL      *-SP[2],ACC           ; [CPU_] |388| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 398,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |398| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |398| 
        SFR       ACC,5                 ; [CPU_] |398| 
        LSL       ACC,5                 ; [CPU_] |398| 
        LSL       ACC,1                 ; [CPU_] |398| 
        ADDL      XAR4,ACC              ; [CPU_] |398| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |398| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 400,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |400| 
        MOVB      AH,#0                 ; [CPU_] |400| 
        ANDB      AL,#0x07              ; [CPU_] |400| 
        LSL       ACC,2                 ; [CPU_] |400| 
        MOVL      *-SP[10],ACC          ; [CPU_] |400| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 401,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |401| 
        MOVB      AH,#0                 ; [CPU_] |401| 
        ANDB      AL,#0x1f              ; [CPU_] |401| 
        SFR       ACC,3                 ; [CPU_] |401| 
        ADDB      ACC,#20               ; [CPU_] |401| 
        MOVL      *-SP[8],ACC           ; [CPU_] |401| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 406,column 5,is_stmt
$C$DW$88	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$88, DW_AT_low_pc(0x00)
	.dwattr $C$DW$88, DW_AT_name("___eallow")
	.dwattr $C$DW$88, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |406| 
        ; call occurs [#___eallow] ; [] |406| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 407,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |407| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |407| 
        MOV       T,*-SP[10]            ; [CPU_] |407| 
        LSL       ACC,1                 ; [CPU_] |407| 
        ADDL      XAR4,ACC              ; [CPU_] |407| 
        MOVB      ACC,#15               ; [CPU_] |407| 
        LSLL      ACC,T                 ; [CPU_] |407| 
        NOT       ACC                   ; [CPU_] |407| 
        AND       *+XAR4[0],AL          ; [CPU_] |407| 
        AND       *+XAR4[1],AH          ; [CPU_] |407| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 408,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |408| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |408| 
        SETC      SXM                   ; [CPU_] 
        MOV       T,*-SP[10]            ; [CPU_] |408| 
        LSL       ACC,1                 ; [CPU_] |408| 
        ADDL      XAR4,ACC              ; [CPU_] |408| 
        MOV       ACC,*-SP[3]           ; [CPU_] |408| 
        LSLL      ACC,T                 ; [CPU_] |408| 
        OR        *+XAR4[0],AL          ; [CPU_] |408| 
        OR        *+XAR4[1],AH          ; [CPU_] |408| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 409,column 5,is_stmt
$C$DW$89	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$89, DW_AT_low_pc(0x00)
	.dwattr $C$DW$89, DW_AT_name("___edis")
	.dwattr $C$DW$89, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |409| 
        ; call occurs [#___edis] ; [] |409| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 410,column 1,is_stmt
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$80, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$80, DW_AT_TI_end_line(0x19a)
	.dwattr $C$DW$80, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$80

	.sect	".text"
	.clink
	.global	_GPIO_setAnalogMode

$C$DW$91	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setAnalogMode")
	.dwattr $C$DW$91, DW_AT_low_pc(_GPIO_setAnalogMode)
	.dwattr $C$DW$91, DW_AT_high_pc(0x00)
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_GPIO_setAnalogMode")
	.dwattr $C$DW$91, DW_AT_external
	.dwattr $C$DW$91, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$91, DW_AT_TI_begin_line(0x1a2)
	.dwattr $C$DW$91, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$91, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 419,column 1,is_stmt,address _GPIO_setAnalogMode

	.dwfde $C$DW$CIE, _GPIO_setAnalogMode
$C$DW$92	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pin")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg0]
$C$DW$93	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mode")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _GPIO_setAnalogMode           FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_GPIO_setAnalogMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("pin")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_pin")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$94, DW_AT_location[DW_OP_breg20 -2]
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("mode")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_breg20 -3]
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("gpioBaseAddr")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_gpioBaseAddr")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_breg20 -6]
$C$DW$97	.dwtag  DW_TAG_variable, DW_AT_name("pinMask")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_pinMask")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[3],AR4           ; [CPU_] |419| 
        MOVL      *-SP[2],ACC           ; [CPU_] |419| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 428,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |428| 
        MOVL      XAR6,ACC              ; [CPU_] |428| 
        AND       AL,*-SP[2],#0x001f    ; [CPU_] |428| 
        MOV       T,AL                  ; [CPU_] |428| 
        MOVL      ACC,XAR6              ; [CPU_] |428| 
        LSLL      ACC,T                 ; [CPU_] |428| 
        MOVL      *-SP[8],ACC           ; [CPU_] |428| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 429,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      XAR4,#31744           ; [CPU_U] |429| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |429| 
        SFR       ACC,5                 ; [CPU_] |429| 
        LSL       ACC,5                 ; [CPU_] |429| 
        LSL       ACC,1                 ; [CPU_] |429| 
        ADDL      XAR4,ACC              ; [CPU_] |429| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |429| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 432,column 5,is_stmt
$C$DW$98	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$98, DW_AT_low_pc(0x00)
	.dwattr $C$DW$98, DW_AT_name("___eallow")
	.dwattr $C$DW$98, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |432| 
        ; call occurs [#___eallow] ; [] |432| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 437,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |437| 
        CMPB      AL,#1                 ; [CPU_] |437| 
        BF        $C$L22,NEQ            ; [CPU_] |437| 
        ; branchcc occurs ; [] |437| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 442,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |442| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |442| 
        ADDB      XAR4,#20              ; [CPU_] |442| 
        OR        *+XAR4[0],AL          ; [CPU_] |442| 
        OR        *+XAR4[1],AH          ; [CPU_] |442| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 443,column 5,is_stmt
        B         $C$L23,UNC            ; [CPU_] |443| 
        ; branch occurs ; [] |443| 
$C$L22:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 449,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |449| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |449| 
        NOT       ACC                   ; [CPU_] |449| 
        ADDB      XAR4,#20              ; [CPU_] |449| 
        AND       *+XAR4[0],AL          ; [CPU_] |449| 
        AND       *+XAR4[1],AH          ; [CPU_] |449| 
$C$L23:    
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 452,column 5,is_stmt
$C$DW$99	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$99, DW_AT_low_pc(0x00)
	.dwattr $C$DW$99, DW_AT_name("___edis")
	.dwattr $C$DW$99, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |452| 
        ; call occurs [#___edis] ; [] |452| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 453,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$91, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$91, DW_AT_TI_end_line(0x1c5)
	.dwattr $C$DW$91, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$91

	.sect	".text"
	.clink
	.global	_GPIO_setPinConfig

$C$DW$101	.dwtag  DW_TAG_subprogram, DW_AT_name("GPIO_setPinConfig")
	.dwattr $C$DW$101, DW_AT_low_pc(_GPIO_setPinConfig)
	.dwattr $C$DW$101, DW_AT_high_pc(0x00)
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_GPIO_setPinConfig")
	.dwattr $C$DW$101, DW_AT_external
	.dwattr $C$DW$101, DW_AT_TI_begin_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$101, DW_AT_TI_begin_line(0x1cd)
	.dwattr $C$DW$101, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$101, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 462,column 1,is_stmt,address _GPIO_setPinConfig

	.dwfde $C$DW$CIE, _GPIO_setPinConfig
$C$DW$102	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pinConfig")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_pinConfig")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _GPIO_setPinConfig            FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_GPIO_setPinConfig:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("pinConfig")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_pinConfig")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_breg20 -2]
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("muxRegAddr")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_muxRegAddr")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_breg20 -4]
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("pinMask")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_pinMask")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -6]
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("shiftAmt")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_shiftAmt")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[2],ACC           ; [CPU_] |462| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 466,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |466| 
        MOVU      ACC,AH                ; [CPU_] |466| 
        ADD       ACC,#31 << 10         ; [CPU_] |466| 
        MOVL      *-SP[4],ACC           ; [CPU_] |466| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 467,column 5,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOVL      ACC,*-SP[2]           ; [CPU_] |467| 
        SFR       ACC,8                 ; [CPU_] |467| 
        MOVB      AH,#0                 ; [CPU_] |467| 
        ANDB      AL,#0xff              ; [CPU_] |467| 
        MOVL      *-SP[8],ACC           ; [CPU_] |467| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 468,column 5,is_stmt
        MOV       T,*-SP[8]             ; [CPU_] |468| 
        MOVB      ACC,#3                ; [CPU_] |468| 
        LSLL      ACC,T                 ; [CPU_] |468| 
        MOVL      *-SP[6],ACC           ; [CPU_] |468| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 470,column 5,is_stmt
$C$DW$107	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$107, DW_AT_low_pc(0x00)
	.dwattr $C$DW$107, DW_AT_name("___eallow")
	.dwattr $C$DW$107, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |470| 
        ; call occurs [#___eallow] ; [] |470| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 475,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |475| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |475| 
        NOT       ACC                   ; [CPU_] |475| 
        AND       *+XAR4[0],AL          ; [CPU_] |475| 
        AND       *+XAR4[1],AH          ; [CPU_] |475| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 480,column 5,is_stmt
        MOVB      ACC,#26               ; [CPU_] |480| 
        MOVL      P,*-SP[6]             ; [CPU_] |480| 
        CLRC      SXM                   ; [CPU_] 
        ADDL      ACC,*-SP[4]           ; [CPU_] |480| 
        MOVL      XAR4,ACC              ; [CPU_] |480| 
        MOVP      T,*-SP[8]             ; [CPU_] 
        NOT       ACC                   ; [CPU_] |480| 
        MOVL      P,ACC                 ; [CPU_] |480| 
        MOV       AL,PL                 ; [CPU_] |480| 
        MOV       AH,PH                 ; [CPU_] |480| 
        AND       AL,*+XAR4[0]          ; [CPU_] |480| 
        MOV       PL,AL                 ; [CPU_] |480| 
        AND       AH,*+XAR4[1]          ; [CPU_] |480| 
        MOV       PH,AH                 ; [CPU_] |480| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |480| 
        SFR       ACC,2                 ; [CPU_] |480| 
        MOVB      AH,#0                 ; [CPU_] |480| 
        ANDB      AL,#0x03              ; [CPU_] |480| 
        LSLL      ACC,T                 ; [CPU_] |480| 
        MOVZ      AR6,AL                ; [CPU_] |480| 
        MOV       AL,PL                 ; [CPU_] |480| 
        OR        AL,AR6                ; [CPU_] |480| 
        MOV       PL,AL                 ; [CPU_] |480| 
        MOV       AL,AH                 ; [CPU_] |480| 
        MOV       AH,PH                 ; [CPU_] |480| 
        OR        AH,AL                 ; [CPU_] |480| 
        MOV       PH,AH                 ; [CPU_] |480| 
        MOVB      ACC,#26               ; [CPU_] |480| 
        ADDL      ACC,*-SP[4]           ; [CPU_] |480| 
        MOVL      XAR4,ACC              ; [CPU_] |480| 
        MOVL      *+XAR4[0],P           ; [CPU_] |480| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 487,column 5,is_stmt
        MOV       T,*-SP[8]             ; [CPU_] |487| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |487| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |487| 
        MOVB      AH,#0                 ; [CPU_] |487| 
        ANDB      AL,#0x03              ; [CPU_] |487| 
        LSLL      ACC,T                 ; [CPU_] |487| 
        OR        *+XAR4[0],AL          ; [CPU_] |487| 
        OR        *+XAR4[1],AH          ; [CPU_] |487| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 488,column 5,is_stmt
$C$DW$108	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$108, DW_AT_low_pc(0x00)
	.dwattr $C$DW$108, DW_AT_name("___edis")
	.dwattr $C$DW$108, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |488| 
        ; call occurs [#___edis] ; [] |488| 
	.dwpsn	file "../ExtraData/driverlib2/gpio.c",line 489,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$109	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$109, DW_AT_low_pc(0x00)
	.dwattr $C$DW$109, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$101, DW_AT_TI_end_file("../ExtraData/driverlib2/gpio.c")
	.dwattr $C$DW$101, DW_AT_TI_end_line(0x1e9)
	.dwattr $C$DW$101, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$101

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$110	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT1"), DW_AT_const_value(0x00)
$C$DW$111	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT2"), DW_AT_const_value(0x01)
$C$DW$112	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT3"), DW_AT_const_value(0x02)
$C$DW$113	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT4"), DW_AT_const_value(0x03)
$C$DW$114	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT5"), DW_AT_const_value(0x04)
$C$DW$115	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT6"), DW_AT_const_value(0x05)
$C$DW$116	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT7"), DW_AT_const_value(0x06)
$C$DW$117	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT8"), DW_AT_const_value(0x07)
$C$DW$118	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT9"), DW_AT_const_value(0x08)
$C$DW$119	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT10"), DW_AT_const_value(0x09)
$C$DW$120	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT11"), DW_AT_const_value(0x0a)
$C$DW$121	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT12"), DW_AT_const_value(0x0b)
$C$DW$122	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT13"), DW_AT_const_value(0x0c)
$C$DW$123	.dwtag  DW_TAG_enumerator, DW_AT_name("XBAR_INPUT14"), DW_AT_const_value(0x0d)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("XBAR_InputNum")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$124	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_DIR_MODE_IN"), DW_AT_const_value(0x00)
$C$DW$125	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_DIR_MODE_OUT"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("GPIO_Direction")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$25	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$126	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_INT_XINT1"), DW_AT_const_value(0x00)
$C$DW$127	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_INT_XINT2"), DW_AT_const_value(0x01)
$C$DW$128	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_INT_XINT3"), DW_AT_const_value(0x02)
$C$DW$129	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_INT_XINT4"), DW_AT_const_value(0x03)
$C$DW$130	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_INT_XINT5"), DW_AT_const_value(0x04)
	.dwendtag $C$DW$T$25

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("GPIO_ExternalIntNum")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$131	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_QUAL_SYNC"), DW_AT_const_value(0x00)
$C$DW$132	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_QUAL_3SAMPLE"), DW_AT_const_value(0x01)
$C$DW$133	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_QUAL_6SAMPLE"), DW_AT_const_value(0x02)
$C$DW$134	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_QUAL_ASYNC"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$27

$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("GPIO_QualificationMode")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$T$30	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$30, DW_AT_byte_size(0x01)
$C$DW$135	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_CORE_CPU1"), DW_AT_const_value(0x00)
$C$DW$136	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_CORE_CPU1_CLA1"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$30

$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("GPIO_CoreSelect")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)

$C$DW$T$32	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x01)
$C$DW$137	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_ANALOG_DISABLED"), DW_AT_const_value(0x00)
$C$DW$138	.dwtag  DW_TAG_enumerator, DW_AT_name("GPIO_ANALOG_ENABLED"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$32

$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("GPIO_AnalogMode")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$139	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$23)
$C$DW$T$50	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$139)
$C$DW$T$51	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_address_class(0x16)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("uintptr_t")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg0]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg1]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_reg2]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg3]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg20]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg21]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg22]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg23]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_reg24]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_reg25]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg26]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg28]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg29]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg30]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg31]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x20]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x21]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x22]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x23]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x24]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x25]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x26]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_reg4]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_reg6]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_reg8]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_reg10]
$C$DW$168	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_reg12]
$C$DW$169	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg14]
$C$DW$170	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg16]
$C$DW$171	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg17]
$C$DW$172	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg18]
$C$DW$173	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg19]
$C$DW$174	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_reg5]
$C$DW$175	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_reg7]
$C$DW$176	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_reg9]
$C$DW$177	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_reg11]
$C$DW$178	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_reg13]
$C$DW$179	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_reg15]
$C$DW$180	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$181	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$182	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$183	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$183, DW_AT_location[DW_OP_regx 0x30]
$C$DW$184	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$184, DW_AT_location[DW_OP_regx 0x33]
$C$DW$185	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_regx 0x34]
$C$DW$186	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x37]
$C$DW$187	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x38]
$C$DW$188	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$189	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$190	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$191	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_regx 0x40]
$C$DW$192	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_regx 0x43]
$C$DW$193	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_regx 0x44]
$C$DW$194	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_regx 0x47]
$C$DW$195	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_regx 0x48]
$C$DW$196	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_regx 0x49]
$C$DW$197	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$198	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$198, DW_AT_location[DW_OP_regx 0x27]
$C$DW$199	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$199, DW_AT_location[DW_OP_regx 0x28]
$C$DW$200	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg27]
$C$DW$201	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

