;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:04 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\1350412 
	.sect	".text"
	.clink

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableLoopback")
	.dwattr $C$DW$1, DW_AT_low_pc(_McBSP_disableLoopback)
	.dwattr $C$DW$1, DW_AT_high_pc(0x00)
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_McBSP_disableLoopback")
	.dwattr $C$DW$1, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$1, DW_AT_TI_begin_line(0x29b)
	.dwattr $C$DW$1, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$1, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 668,column 1,is_stmt,address _McBSP_disableLoopback

	.dwfde $C$DW$CIE, _McBSP_disableLoopback
$C$DW$2	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$2, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_disableLoopback        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableLoopback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$3, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |668| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 677,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |677| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |677| 
        MOVL      XAR4,ACC              ; [CPU_] |677| 
        AND       *+XAR4[0],#0x7fff     ; [CPU_] |677| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 678,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$4	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$4, DW_AT_low_pc(0x00)
	.dwattr $C$DW$4, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$1, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$1, DW_AT_TI_end_line(0x2a6)
	.dwattr $C$DW$1, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$1

	.sect	".text"
	.clink

$C$DW$5	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableLoopback")
	.dwattr $C$DW$5, DW_AT_low_pc(_McBSP_enableLoopback)
	.dwattr $C$DW$5, DW_AT_high_pc(0x00)
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_McBSP_enableLoopback")
	.dwattr $C$DW$5, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$5, DW_AT_TI_begin_line(0x2b4)
	.dwattr $C$DW$5, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$5, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 693,column 1,is_stmt,address _McBSP_enableLoopback

	.dwfde $C$DW$CIE, _McBSP_enableLoopback
$C$DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_enableLoopback         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableLoopback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |693| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 702,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |702| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |702| 
        MOVL      XAR4,ACC              ; [CPU_] |702| 
        OR        *+XAR4[0],#0x8000     ; [CPU_] |702| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 703,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$5, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$5, DW_AT_TI_end_line(0x2bf)
	.dwattr $C$DW$5, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$5

	.sect	".text"
	.clink

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxSignExtension")
	.dwattr $C$DW$9, DW_AT_low_pc(_McBSP_setRxSignExtension)
	.dwattr $C$DW$9, DW_AT_high_pc(0x00)
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_McBSP_setRxSignExtension")
	.dwattr $C$DW$9, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$9, DW_AT_TI_begin_line(0x2d2)
	.dwattr $C$DW$9, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$9, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 723,column 1,is_stmt,address _McBSP_setRxSignExtension

	.dwfde $C$DW$CIE, _McBSP_setRxSignExtension
$C$DW$10	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg0]
$C$DW$11	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mode")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxSignExtension     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxSignExtension:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -2]
$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("mode")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |723| 
        MOVL      *-SP[2],ACC           ; [CPU_] |723| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 732,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |732| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |732| 
        MOVL      XAR4,ACC              ; [CPU_] |732| 
        AND       AL,*+XAR4[0],#0x9fff  ; [CPU_] |732| 
        OR        AL,*-SP[3]            ; [CPU_] |732| 
        MOVZ      AR6,AL                ; [CPU_] |732| 
        MOVB      ACC,#5                ; [CPU_] |732| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |732| 
        MOVL      XAR4,ACC              ; [CPU_] |732| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |732| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 734,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$14	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$14, DW_AT_low_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$9, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$9, DW_AT_TI_end_line(0x2de)
	.dwattr $C$DW$9, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$9

	.sect	".text"
	.clink

$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setClockStopMode")
	.dwattr $C$DW$15, DW_AT_low_pc(_McBSP_setClockStopMode)
	.dwattr $C$DW$15, DW_AT_high_pc(0x00)
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_McBSP_setClockStopMode")
	.dwattr $C$DW$15, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$15, DW_AT_TI_begin_line(0x2f3)
	.dwattr $C$DW$15, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$15, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 756,column 1,is_stmt,address _McBSP_setClockStopMode

	.dwfde $C$DW$CIE, _McBSP_setClockStopMode
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg0]
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("mode")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setClockStopMode       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setClockStopMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -2]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("mode")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_mode")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |756| 
        MOVL      *-SP[2],ACC           ; [CPU_] |756| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 765,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |765| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |765| 
        MOVL      XAR4,ACC              ; [CPU_] |765| 
        AND       AL,*+XAR4[0],#0xe7ff  ; [CPU_] |765| 
        OR        AL,*-SP[3]            ; [CPU_] |765| 
        MOVZ      AR6,AL                ; [CPU_] |765| 
        MOVB      ACC,#5                ; [CPU_] |765| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |765| 
        MOVL      XAR4,ACC              ; [CPU_] |765| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |765| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 767,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$15, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$15, DW_AT_TI_end_line(0x2ff)
	.dwattr $C$DW$15, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$15

	.sect	".text"
	.clink

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableDxPinDelay")
	.dwattr $C$DW$21, DW_AT_low_pc(_McBSP_disableDxPinDelay)
	.dwattr $C$DW$21, DW_AT_high_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_McBSP_disableDxPinDelay")
	.dwattr $C$DW$21, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$21, DW_AT_TI_begin_line(0x30d)
	.dwattr $C$DW$21, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$21, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 782,column 1,is_stmt,address _McBSP_disableDxPinDelay

	.dwfde $C$DW$CIE, _McBSP_disableDxPinDelay
$C$DW$22	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_disableDxPinDelay      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableDxPinDelay:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$23	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |782| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 791,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |791| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |791| 
        MOVL      XAR4,ACC              ; [CPU_] |791| 
        AND       *+XAR4[0],#0xff7f     ; [CPU_] |791| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 792,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$21, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$21, DW_AT_TI_end_line(0x318)
	.dwattr $C$DW$21, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$21

	.sect	".text"
	.clink

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableDxPinDelay")
	.dwattr $C$DW$25, DW_AT_low_pc(_McBSP_enableDxPinDelay)
	.dwattr $C$DW$25, DW_AT_high_pc(0x00)
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_McBSP_enableDxPinDelay")
	.dwattr $C$DW$25, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$25, DW_AT_TI_begin_line(0x327)
	.dwattr $C$DW$25, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$25, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 808,column 1,is_stmt,address _McBSP_enableDxPinDelay

	.dwfde $C$DW$CIE, _McBSP_enableDxPinDelay
$C$DW$26	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_enableDxPinDelay       FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableDxPinDelay:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |808| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 817,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |817| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |817| 
        MOVL      XAR4,ACC              ; [CPU_] |817| 
        OR        *+XAR4[0],#0x0080     ; [CPU_] |817| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 818,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$25, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$25, DW_AT_TI_end_line(0x332)
	.dwattr $C$DW$25, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$25

	.sect	".text"
	.clink

$C$DW$29	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxInterruptSource")
	.dwattr $C$DW$29, DW_AT_low_pc(_McBSP_setRxInterruptSource)
	.dwattr $C$DW$29, DW_AT_high_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_McBSP_setRxInterruptSource")
	.dwattr $C$DW$29, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$29, DW_AT_TI_begin_line(0x346)
	.dwattr $C$DW$29, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$29, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 840,column 1,is_stmt,address _McBSP_setRxInterruptSource

	.dwfde $C$DW$CIE, _McBSP_setRxInterruptSource
$C$DW$30	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg0]
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("interruptSource")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_interruptSource")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxInterruptSource   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxInterruptSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -2]
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("interruptSource")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_interruptSource")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |840| 
        MOVL      *-SP[2],ACC           ; [CPU_] |840| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 849,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |849| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |849| 
        MOVL      XAR4,ACC              ; [CPU_] |849| 
        AND       AL,*+XAR4[0],#0xffcf  ; [CPU_] |849| 
        OR        AL,*-SP[3]            ; [CPU_] |849| 
        MOVZ      AR6,AL                ; [CPU_] |849| 
        MOVB      ACC,#5                ; [CPU_] |849| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |849| 
        MOVL      XAR4,ACC              ; [CPU_] |849| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |849| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 852,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$29, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$29, DW_AT_TI_end_line(0x354)
	.dwattr $C$DW$29, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$29

	.sect	".text"
	.clink

$C$DW$35	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_isRxReady")
	.dwattr $C$DW$35, DW_AT_low_pc(_McBSP_isRxReady)
	.dwattr $C$DW$35, DW_AT_high_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_McBSP_isRxReady")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$35, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$35, DW_AT_TI_begin_line(0x39d)
	.dwattr $C$DW$35, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$35, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 926,column 1,is_stmt,address _McBSP_isRxReady

	.dwfde $C$DW$CIE, _McBSP_isRxReady
$C$DW$36	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_isRxReady              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_isRxReady:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |926| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 935,column 5,is_stmt
        MOVB      ACC,#5                ; [CPU_] |935| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |935| 
        MOVL      XAR4,ACC              ; [CPU_] |935| 
        MOVB      AL,#0                 ; [CPU_] |935| 
        MOV       AH,*+XAR4[0]          ; [CPU_] |935| 
        ANDB      AH,#0x02              ; [CPU_] |935| 
        CMPB      AH,#2                 ; [CPU_] |935| 
        BF        $C$L1,NEQ             ; [CPU_] |935| 
        ; branchcc occurs ; [] |935| 
        MOVB      AL,#1                 ; [CPU_] |935| 
$C$L1:    
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 937,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$35, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$35, DW_AT_TI_end_line(0x3a9)
	.dwattr $C$DW$35, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$35

	.sect	".text"
	.clink

$C$DW$39	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxInterruptSource")
	.dwattr $C$DW$39, DW_AT_low_pc(_McBSP_setTxInterruptSource)
	.dwattr $C$DW$39, DW_AT_high_pc(0x00)
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_McBSP_setTxInterruptSource")
	.dwattr $C$DW$39, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$39, DW_AT_TI_begin_line(0x477)
	.dwattr $C$DW$39, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$39, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1145,column 1,is_stmt,address _McBSP_setTxInterruptSource

	.dwfde $C$DW$CIE, _McBSP_setTxInterruptSource
$C$DW$40	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg0]
$C$DW$41	.dwtag  DW_TAG_formal_parameter, DW_AT_name("interruptSource")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_interruptSource")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxInterruptSource   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxInterruptSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_breg20 -2]
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("interruptSource")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_interruptSource")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1145| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1145| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1154,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |1154| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1154| 
        MOVL      XAR4,ACC              ; [CPU_] |1154| 
        AND       AL,*+XAR4[0],#0xffcf  ; [CPU_] |1154| 
        OR        AL,*-SP[3]            ; [CPU_] |1154| 
        MOVZ      AR6,AL                ; [CPU_] |1154| 
        MOVB      ACC,#4                ; [CPU_] |1154| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1154| 
        MOVL      XAR4,ACC              ; [CPU_] |1154| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1154| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1157,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$39, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$39, DW_AT_TI_end_line(0x485)
	.dwattr $C$DW$39, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$39

	.sect	".text"
	.clink

$C$DW$45	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_isTxReady")
	.dwattr $C$DW$45, DW_AT_low_pc(_McBSP_isTxReady)
	.dwattr $C$DW$45, DW_AT_high_pc(0x00)
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_McBSP_isTxReady")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$45, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$45, DW_AT_TI_begin_line(0x4cd)
	.dwattr $C$DW$45, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$45, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1230,column 1,is_stmt,address _McBSP_isTxReady

	.dwfde $C$DW$CIE, _McBSP_isTxReady
$C$DW$46	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_isTxReady              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_isTxReady:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1230| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1239,column 5,is_stmt
        MOVB      ACC,#4                ; [CPU_] |1239| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1239| 
        MOVL      XAR4,ACC              ; [CPU_] |1239| 
        MOVB      AL,#0                 ; [CPU_] |1239| 
        MOV       AH,*+XAR4[0]          ; [CPU_] |1239| 
        ANDB      AH,#0x02              ; [CPU_] |1239| 
        CMPB      AH,#2                 ; [CPU_] |1239| 
        BF        $C$L2,NEQ             ; [CPU_] |1239| 
        ; branchcc occurs ; [] |1239| 
        MOVB      AL,#1                 ; [CPU_] |1239| 
$C$L2:    
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1241,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$45, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$45, DW_AT_TI_end_line(0x4d9)
	.dwattr $C$DW$45, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$45

	.sect	".text"
	.clink

$C$DW$49	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableTwoPhaseRx")
	.dwattr $C$DW$49, DW_AT_low_pc(_McBSP_disableTwoPhaseRx)
	.dwattr $C$DW$49, DW_AT_high_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_McBSP_disableTwoPhaseRx")
	.dwattr $C$DW$49, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$49, DW_AT_TI_begin_line(0x519)
	.dwattr $C$DW$49, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$49, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1306,column 1,is_stmt,address _McBSP_disableTwoPhaseRx

	.dwfde $C$DW$CIE, _McBSP_disableTwoPhaseRx
$C$DW$50	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_disableTwoPhaseRx      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableTwoPhaseRx:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1306| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1315,column 5,is_stmt
        MOVB      ACC,#6                ; [CPU_] |1315| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1315| 
        MOVL      XAR4,ACC              ; [CPU_] |1315| 
        AND       *+XAR4[0],#0x7fff     ; [CPU_] |1315| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1316,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x00)
	.dwattr $C$DW$52, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$49, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$49, DW_AT_TI_end_line(0x524)
	.dwattr $C$DW$49, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$49

	.sect	".text"
	.clink

$C$DW$53	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableTwoPhaseRx")
	.dwattr $C$DW$53, DW_AT_low_pc(_McBSP_enableTwoPhaseRx)
	.dwattr $C$DW$53, DW_AT_high_pc(0x00)
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_McBSP_enableTwoPhaseRx")
	.dwattr $C$DW$53, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$53, DW_AT_TI_begin_line(0x532)
	.dwattr $C$DW$53, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$53, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1331,column 1,is_stmt,address _McBSP_enableTwoPhaseRx

	.dwfde $C$DW$CIE, _McBSP_enableTwoPhaseRx
$C$DW$54	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_enableTwoPhaseRx       FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableTwoPhaseRx:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1331| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1340,column 5,is_stmt
        MOVB      ACC,#6                ; [CPU_] |1340| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1340| 
        MOVL      XAR4,ACC              ; [CPU_] |1340| 
        OR        *+XAR4[0],#0x8000     ; [CPU_] |1340| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1341,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$56	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$56, DW_AT_low_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$53, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$53, DW_AT_TI_end_line(0x53d)
	.dwattr $C$DW$53, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$53

	.sect	".text"
	.clink

$C$DW$57	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxCompandingMode")
	.dwattr $C$DW$57, DW_AT_low_pc(_McBSP_setRxCompandingMode)
	.dwattr $C$DW$57, DW_AT_high_pc(0x00)
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_McBSP_setRxCompandingMode")
	.dwattr $C$DW$57, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$57, DW_AT_TI_begin_line(0x553)
	.dwattr $C$DW$57, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$57, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1365,column 1,is_stmt,address _McBSP_setRxCompandingMode

	.dwfde $C$DW$CIE, _McBSP_setRxCompandingMode
$C$DW$58	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg0]
$C$DW$59	.dwtag  DW_TAG_formal_parameter, DW_AT_name("compandingMode")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_compandingMode")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxCompandingMode    FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxCompandingMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_breg20 -2]
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("compandingMode")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_compandingMode")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1365| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1365| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1374,column 5,is_stmt
        MOVB      ACC,#6                ; [CPU_] |1374| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1374| 
        MOVL      XAR4,ACC              ; [CPU_] |1374| 
        AND       AL,*+XAR4[0],#0xffe7  ; [CPU_] |1374| 
        OR        AL,*-SP[3]            ; [CPU_] |1374| 
        MOVZ      AR6,AL                ; [CPU_] |1374| 
        MOVB      ACC,#6                ; [CPU_] |1374| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1374| 
        MOVL      XAR4,ACC              ; [CPU_] |1374| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1374| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1377,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$62	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$62, DW_AT_low_pc(0x00)
	.dwattr $C$DW$62, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$57, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$57, DW_AT_TI_end_line(0x561)
	.dwattr $C$DW$57, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$57

	.sect	".text"
	.clink

$C$DW$63	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableRxFrameSyncErrorDetection")
	.dwattr $C$DW$63, DW_AT_low_pc(_McBSP_enableRxFrameSyncErrorDetection)
	.dwattr $C$DW$63, DW_AT_high_pc(0x00)
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_McBSP_enableRxFrameSyncErrorDetection")
	.dwattr $C$DW$63, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$63, DW_AT_TI_begin_line(0x58b)
	.dwattr $C$DW$63, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$63, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1420,column 1,is_stmt,address _McBSP_enableRxFrameSyncErrorDetection

	.dwfde $C$DW$CIE, _McBSP_enableRxFrameSyncErrorDetection
$C$DW$64	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_enableRxFrameSyncErrorDetection FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableRxFrameSyncErrorDetection:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1420| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1429,column 5,is_stmt
        MOVB      ACC,#6                ; [CPU_] |1429| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1429| 
        MOVL      XAR4,ACC              ; [CPU_] |1429| 
        AND       *+XAR4[0],#0xfffb     ; [CPU_] |1429| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1430,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$63, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$63, DW_AT_TI_end_line(0x596)
	.dwattr $C$DW$63, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$63

	.sect	".text"
	.clink

$C$DW$67	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxDataDelayBits")
	.dwattr $C$DW$67, DW_AT_low_pc(_McBSP_setRxDataDelayBits)
	.dwattr $C$DW$67, DW_AT_high_pc(0x00)
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_McBSP_setRxDataDelayBits")
	.dwattr $C$DW$67, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$67, DW_AT_TI_begin_line(0x5a8)
	.dwattr $C$DW$67, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$67, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1449,column 1,is_stmt,address _McBSP_setRxDataDelayBits

	.dwfde $C$DW$CIE, _McBSP_setRxDataDelayBits
$C$DW$68	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg0]
$C$DW$69	.dwtag  DW_TAG_formal_parameter, DW_AT_name("delayBits")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_delayBits")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxDataDelayBits     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxDataDelayBits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_breg20 -2]
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("delayBits")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_delayBits")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1449| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1449| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1458,column 5,is_stmt
        MOVB      ACC,#6                ; [CPU_] |1458| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1458| 
        MOVL      XAR4,ACC              ; [CPU_] |1458| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1458| 
        OR        AL,*-SP[3]            ; [CPU_] |1458| 
        MOVZ      AR6,AL                ; [CPU_] |1458| 
        MOVB      ACC,#6                ; [CPU_] |1458| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1458| 
        MOVL      XAR4,ACC              ; [CPU_] |1458| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1458| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1461,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$72	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$72, DW_AT_low_pc(0x00)
	.dwattr $C$DW$72, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$67, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$67, DW_AT_TI_end_line(0x5b5)
	.dwattr $C$DW$67, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$67

	.sect	".text"
	.clink

$C$DW$73	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableTwoPhaseTx")
	.dwattr $C$DW$73, DW_AT_low_pc(_McBSP_disableTwoPhaseTx)
	.dwattr $C$DW$73, DW_AT_high_pc(0x00)
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_McBSP_disableTwoPhaseTx")
	.dwattr $C$DW$73, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$73, DW_AT_TI_begin_line(0x5c3)
	.dwattr $C$DW$73, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$73, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1476,column 1,is_stmt,address _McBSP_disableTwoPhaseTx

	.dwfde $C$DW$CIE, _McBSP_disableTwoPhaseTx
$C$DW$74	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_disableTwoPhaseTx      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableTwoPhaseTx:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1476| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1485,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |1485| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1485| 
        MOVL      XAR4,ACC              ; [CPU_] |1485| 
        AND       *+XAR4[0],#0x7fff     ; [CPU_] |1485| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1486,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$73, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$73, DW_AT_TI_end_line(0x5ce)
	.dwattr $C$DW$73, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$73

	.sect	".text"
	.clink

$C$DW$77	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableTwoPhaseTx")
	.dwattr $C$DW$77, DW_AT_low_pc(_McBSP_enableTwoPhaseTx)
	.dwattr $C$DW$77, DW_AT_high_pc(0x00)
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_McBSP_enableTwoPhaseTx")
	.dwattr $C$DW$77, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$77, DW_AT_TI_begin_line(0x5dc)
	.dwattr $C$DW$77, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$77, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1501,column 1,is_stmt,address _McBSP_enableTwoPhaseTx

	.dwfde $C$DW$CIE, _McBSP_enableTwoPhaseTx
$C$DW$78	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_enableTwoPhaseTx       FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableTwoPhaseTx:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1501| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1510,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |1510| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1510| 
        MOVL      XAR4,ACC              ; [CPU_] |1510| 
        OR        *+XAR4[0],#0x8000     ; [CPU_] |1510| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1511,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$80	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$80, DW_AT_low_pc(0x00)
	.dwattr $C$DW$80, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$77, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$77, DW_AT_TI_end_line(0x5e7)
	.dwattr $C$DW$77, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$77

	.sect	".text"
	.clink

$C$DW$81	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxCompandingMode")
	.dwattr $C$DW$81, DW_AT_low_pc(_McBSP_setTxCompandingMode)
	.dwattr $C$DW$81, DW_AT_high_pc(0x00)
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_McBSP_setTxCompandingMode")
	.dwattr $C$DW$81, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$81, DW_AT_TI_begin_line(0x5fd)
	.dwattr $C$DW$81, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$81, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1535,column 1,is_stmt,address _McBSP_setTxCompandingMode

	.dwfde $C$DW$CIE, _McBSP_setTxCompandingMode
$C$DW$82	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg0]
$C$DW$83	.dwtag  DW_TAG_formal_parameter, DW_AT_name("compandingMode")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_compandingMode")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxCompandingMode    FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxCompandingMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_breg20 -2]
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("compandingMode")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_compandingMode")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1535| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1535| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1544,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |1544| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1544| 
        MOVL      XAR4,ACC              ; [CPU_] |1544| 
        AND       AL,*+XAR4[0],#0xffe7  ; [CPU_] |1544| 
        OR        AL,*-SP[3]            ; [CPU_] |1544| 
        MOVZ      AR6,AL                ; [CPU_] |1544| 
        MOVB      ACC,#8                ; [CPU_] |1544| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1544| 
        MOVL      XAR4,ACC              ; [CPU_] |1544| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1544| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1547,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$86	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$86, DW_AT_low_pc(0x00)
	.dwattr $C$DW$86, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$81, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$81, DW_AT_TI_end_line(0x60b)
	.dwattr $C$DW$81, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$81

	.sect	".text"
	.clink

$C$DW$87	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableTxFrameSyncErrorDetection")
	.dwattr $C$DW$87, DW_AT_low_pc(_McBSP_disableTxFrameSyncErrorDetection)
	.dwattr $C$DW$87, DW_AT_high_pc(0x00)
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_McBSP_disableTxFrameSyncErrorDetection")
	.dwattr $C$DW$87, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$87, DW_AT_TI_begin_line(0x61a)
	.dwattr $C$DW$87, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$87, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1563,column 1,is_stmt,address _McBSP_disableTxFrameSyncErrorDetection

	.dwfde $C$DW$CIE, _McBSP_disableTxFrameSyncErrorDetection
$C$DW$88	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_disableTxFrameSyncErrorDetection FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableTxFrameSyncErrorDetection:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1563| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1572,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |1572| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1572| 
        MOVL      XAR4,ACC              ; [CPU_] |1572| 
        OR        *+XAR4[0],#0x0004     ; [CPU_] |1572| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1573,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$90	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$90, DW_AT_low_pc(0x00)
	.dwattr $C$DW$90, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$87, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$87, DW_AT_TI_end_line(0x625)
	.dwattr $C$DW$87, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$87

	.sect	".text"
	.clink

$C$DW$91	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableTxFrameSyncErrorDetection")
	.dwattr $C$DW$91, DW_AT_low_pc(_McBSP_enableTxFrameSyncErrorDetection)
	.dwattr $C$DW$91, DW_AT_high_pc(0x00)
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_McBSP_enableTxFrameSyncErrorDetection")
	.dwattr $C$DW$91, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$91, DW_AT_TI_begin_line(0x634)
	.dwattr $C$DW$91, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$91, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1589,column 1,is_stmt,address _McBSP_enableTxFrameSyncErrorDetection

	.dwfde $C$DW$CIE, _McBSP_enableTxFrameSyncErrorDetection
$C$DW$92	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_enableTxFrameSyncErrorDetection FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableTxFrameSyncErrorDetection:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1589| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1598,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |1598| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1598| 
        MOVL      XAR4,ACC              ; [CPU_] |1598| 
        AND       *+XAR4[0],#0xfffb     ; [CPU_] |1598| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1599,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$94	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$94, DW_AT_low_pc(0x00)
	.dwattr $C$DW$94, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$91, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$91, DW_AT_TI_end_line(0x63f)
	.dwattr $C$DW$91, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$91

	.sect	".text"
	.clink

$C$DW$95	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxDataDelayBits")
	.dwattr $C$DW$95, DW_AT_low_pc(_McBSP_setTxDataDelayBits)
	.dwattr $C$DW$95, DW_AT_high_pc(0x00)
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_McBSP_setTxDataDelayBits")
	.dwattr $C$DW$95, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$95, DW_AT_TI_begin_line(0x651)
	.dwattr $C$DW$95, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$95, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1618,column 1,is_stmt,address _McBSP_setTxDataDelayBits

	.dwfde $C$DW$CIE, _McBSP_setTxDataDelayBits
$C$DW$96	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_reg0]
$C$DW$97	.dwtag  DW_TAG_formal_parameter, DW_AT_name("delayBits")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_delayBits")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxDataDelayBits     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxDataDelayBits:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_breg20 -2]
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("delayBits")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_delayBits")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1618| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1618| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1627,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |1627| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1627| 
        MOVL      XAR4,ACC              ; [CPU_] |1627| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |1627| 
        OR        AL,*-SP[3]            ; [CPU_] |1627| 
        MOVZ      AR6,AL                ; [CPU_] |1627| 
        MOVB      ACC,#8                ; [CPU_] |1627| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1627| 
        MOVL      XAR4,ACC              ; [CPU_] |1627| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1627| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1630,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$100	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$100, DW_AT_low_pc(0x00)
	.dwattr $C$DW$100, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$95, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$95, DW_AT_TI_end_line(0x65e)
	.dwattr $C$DW$95, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$95

	.sect	".text"
	.clink

$C$DW$101	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setFrameSyncPulsePeriod")
	.dwattr $C$DW$101, DW_AT_low_pc(_McBSP_setFrameSyncPulsePeriod)
	.dwattr $C$DW$101, DW_AT_high_pc(0x00)
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_McBSP_setFrameSyncPulsePeriod")
	.dwattr $C$DW$101, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$101, DW_AT_TI_begin_line(0x670)
	.dwattr $C$DW$101, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$101, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1649,column 1,is_stmt,address _McBSP_setFrameSyncPulsePeriod

	.dwfde $C$DW$CIE, _McBSP_setFrameSyncPulsePeriod
$C$DW$102	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg0]
$C$DW$103	.dwtag  DW_TAG_formal_parameter, DW_AT_name("frameClockDivider")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_frameClockDivider")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setFrameSyncPulsePeriod FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setFrameSyncPulsePeriod:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_breg20 -2]
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("frameClockDivider")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_frameClockDivider")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1649| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1649| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1659,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |1659| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1659| 
        MOVL      XAR4,ACC              ; [CPU_] |1659| 
        AND       AL,*+XAR4[0],#0xf000  ; [CPU_] |1659| 
        OR        AL,*-SP[3]            ; [CPU_] |1659| 
        MOVZ      AR6,AL                ; [CPU_] |1659| 
        MOVB      ACC,#10               ; [CPU_] |1659| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1659| 
        MOVL      XAR4,ACC              ; [CPU_] |1659| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1659| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1661,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$106	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$106, DW_AT_low_pc(0x00)
	.dwattr $C$DW$106, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$101, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$101, DW_AT_TI_end_line(0x67d)
	.dwattr $C$DW$101, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$101

	.sect	".text"
	.clink

$C$DW$107	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setFrameSyncPulseWidthDivider")
	.dwattr $C$DW$107, DW_AT_low_pc(_McBSP_setFrameSyncPulseWidthDivider)
	.dwattr $C$DW$107, DW_AT_high_pc(0x00)
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_McBSP_setFrameSyncPulseWidthDivider")
	.dwattr $C$DW$107, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$107, DW_AT_TI_begin_line(0x68f)
	.dwattr $C$DW$107, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$107, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1680,column 1,is_stmt,address _McBSP_setFrameSyncPulseWidthDivider

	.dwfde $C$DW$CIE, _McBSP_setFrameSyncPulseWidthDivider
$C$DW$108	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg0]
$C$DW$109	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pulseWidthDivider")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_pulseWidthDivider")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setFrameSyncPulseWidthDivider FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setFrameSyncPulseWidthDivider:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$110	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_breg20 -2]
$C$DW$111	.dwtag  DW_TAG_variable, DW_AT_name("pulseWidthDivider")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_pulseWidthDivider")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1680| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1680| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1690,column 5,is_stmt
        MOVB      ACC,#11               ; [CPU_] |1690| 
        MOVZ      AR7,*-SP[3]           ; [CPU_] |1690| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1690| 
        MOVL      XAR4,ACC              ; [CPU_] |1690| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1690| 
        MOVB      AL.MSB,AR7            ; [CPU_] |1690| 
        MOVZ      AR6,AL                ; [CPU_] |1690| 
        MOVB      ACC,#11               ; [CPU_] |1690| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1690| 
        MOVL      XAR4,ACC              ; [CPU_] |1690| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1690| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1693,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$112	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$112, DW_AT_low_pc(0x00)
	.dwattr $C$DW$112, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$107, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$107, DW_AT_TI_end_line(0x69d)
	.dwattr $C$DW$107, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$107

	.sect	".text"
	.clink

$C$DW$113	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setSRGDataClockDivider")
	.dwattr $C$DW$113, DW_AT_low_pc(_McBSP_setSRGDataClockDivider)
	.dwattr $C$DW$113, DW_AT_high_pc(0x00)
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_McBSP_setSRGDataClockDivider")
	.dwattr $C$DW$113, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$113, DW_AT_TI_begin_line(0x6af)
	.dwattr $C$DW$113, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$113, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1712,column 1,is_stmt,address _McBSP_setSRGDataClockDivider

	.dwfde $C$DW$CIE, _McBSP_setSRGDataClockDivider
$C$DW$114	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$114, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg0]
$C$DW$115	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataClockDivider")
	.dwattr $C$DW$115, DW_AT_TI_symbol_name("_dataClockDivider")
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setSRGDataClockDivider FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setSRGDataClockDivider:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$116	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$116, DW_AT_location[DW_OP_breg20 -2]
$C$DW$117	.dwtag  DW_TAG_variable, DW_AT_name("dataClockDivider")
	.dwattr $C$DW$117, DW_AT_TI_symbol_name("_dataClockDivider")
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$117, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1712| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1712| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1722,column 5,is_stmt
        MOVB      ACC,#11               ; [CPU_] |1722| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1722| 
        MOVL      XAR4,ACC              ; [CPU_] |1722| 
        AND       AL,*+XAR4[0],#0xff00  ; [CPU_] |1722| 
        OR        AL,*-SP[3]            ; [CPU_] |1722| 
        MOVZ      AR6,AL                ; [CPU_] |1722| 
        MOVB      ACC,#11               ; [CPU_] |1722| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1722| 
        MOVL      XAR4,ACC              ; [CPU_] |1722| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1722| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1725,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$118	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$118, DW_AT_low_pc(0x00)
	.dwattr $C$DW$118, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$113, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$113, DW_AT_TI_end_line(0x6bd)
	.dwattr $C$DW$113, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$113

	.sect	".text"
	.clink

$C$DW$119	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableSRGSyncFSR")
	.dwattr $C$DW$119, DW_AT_low_pc(_McBSP_disableSRGSyncFSR)
	.dwattr $C$DW$119, DW_AT_high_pc(0x00)
	.dwattr $C$DW$119, DW_AT_TI_symbol_name("_McBSP_disableSRGSyncFSR")
	.dwattr $C$DW$119, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$119, DW_AT_TI_begin_line(0x6cc)
	.dwattr $C$DW$119, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$119, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1741,column 1,is_stmt,address _McBSP_disableSRGSyncFSR

	.dwfde $C$DW$CIE, _McBSP_disableSRGSyncFSR
$C$DW$120	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_disableSRGSyncFSR      FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableSRGSyncFSR:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$121	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$121, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$121, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1741| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1750,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |1750| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1750| 
        MOVL      XAR4,ACC              ; [CPU_] |1750| 
        AND       *+XAR4[0],#0x7fff     ; [CPU_] |1750| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1751,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$122	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$122, DW_AT_low_pc(0x00)
	.dwattr $C$DW$122, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$119, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$119, DW_AT_TI_end_line(0x6d7)
	.dwattr $C$DW$119, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$119

	.sect	".text"
	.clink

$C$DW$123	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableSRGSyncFSR")
	.dwattr $C$DW$123, DW_AT_low_pc(_McBSP_enableSRGSyncFSR)
	.dwattr $C$DW$123, DW_AT_high_pc(0x00)
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_McBSP_enableSRGSyncFSR")
	.dwattr $C$DW$123, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$123, DW_AT_TI_begin_line(0x6e6)
	.dwattr $C$DW$123, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$123, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1767,column 1,is_stmt,address _McBSP_enableSRGSyncFSR

	.dwfde $C$DW$CIE, _McBSP_enableSRGSyncFSR
$C$DW$124	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_enableSRGSyncFSR       FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableSRGSyncFSR:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |1767| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1776,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |1776| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1776| 
        MOVL      XAR4,ACC              ; [CPU_] |1776| 
        OR        *+XAR4[0],#0x8000     ; [CPU_] |1776| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1777,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$126	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$126, DW_AT_low_pc(0x00)
	.dwattr $C$DW$126, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$123, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$123, DW_AT_TI_end_line(0x6f1)
	.dwattr $C$DW$123, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$123

	.sect	".text"
	.clink

$C$DW$127	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxSRGClockSource")
	.dwattr $C$DW$127, DW_AT_low_pc(_McBSP_setRxSRGClockSource)
	.dwattr $C$DW$127, DW_AT_high_pc(0x00)
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_McBSP_setRxSRGClockSource")
	.dwattr $C$DW$127, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$127, DW_AT_TI_begin_line(0x704)
	.dwattr $C$DW$127, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$127, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1798,column 1,is_stmt,address _McBSP_setRxSRGClockSource

	.dwfde $C$DW$CIE, _McBSP_setRxSRGClockSource
$C$DW$128	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$128, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_reg0]
$C$DW$129	.dwtag  DW_TAG_formal_parameter, DW_AT_name("srgClockSource")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_srgClockSource")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxSRGClockSource    FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxSRGClockSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$130	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$130, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$130, DW_AT_location[DW_OP_breg20 -2]
$C$DW$131	.dwtag  DW_TAG_variable, DW_AT_name("srgClockSource")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_srgClockSource")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$131, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1798| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1798| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1807,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |1807| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1807| 
        MOVL      XAR4,ACC              ; [CPU_] |1807| 
        MOV       AL,*-SP[3]            ; [CPU_] |1807| 
        ANDB      AL,#0x01              ; [CPU_] |1807| 
        AND       AH,*+XAR4[0],#0xdfff  ; [CPU_] |1807| 
        LSL       AL,13                 ; [CPU_] |1807| 
        MOVZ      AR6,AL                ; [CPU_] |1807| 
        OR        AR6,AH                ; [CPU_] |1807| 
        MOVB      ACC,#10               ; [CPU_] |1807| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1807| 
        MOVL      XAR4,ACC              ; [CPU_] |1807| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1807| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1814,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |1814| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1814| 
        MOVL      XAR4,ACC              ; [CPU_] |1814| 
        MOV       AL,*-SP[3]            ; [CPU_] |1814| 
        LSR       AL,1                  ; [CPU_] |1814| 
        AND       AH,*+XAR4[0],#0xff7f  ; [CPU_] |1814| 
        LSL       AL,7                  ; [CPU_] |1814| 
        MOVZ      AR6,AL                ; [CPU_] |1814| 
        OR        AR6,AH                ; [CPU_] |1814| 
        MOVB      ACC,#18               ; [CPU_] |1814| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1814| 
        MOVL      XAR4,ACC              ; [CPU_] |1814| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1814| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1817,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$132	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$132, DW_AT_low_pc(0x00)
	.dwattr $C$DW$132, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$127, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$127, DW_AT_TI_end_line(0x719)
	.dwattr $C$DW$127, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$127

	.sect	".text"
	.clink

$C$DW$133	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxSRGClockSource")
	.dwattr $C$DW$133, DW_AT_low_pc(_McBSP_setTxSRGClockSource)
	.dwattr $C$DW$133, DW_AT_high_pc(0x00)
	.dwattr $C$DW$133, DW_AT_TI_symbol_name("_McBSP_setTxSRGClockSource")
	.dwattr $C$DW$133, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$133, DW_AT_TI_begin_line(0x72c)
	.dwattr $C$DW$133, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$133, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1838,column 1,is_stmt,address _McBSP_setTxSRGClockSource

	.dwfde $C$DW$CIE, _McBSP_setTxSRGClockSource
$C$DW$134	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg0]
$C$DW$135	.dwtag  DW_TAG_formal_parameter, DW_AT_name("srgClockSource")
	.dwattr $C$DW$135, DW_AT_TI_symbol_name("_srgClockSource")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxSRGClockSource    FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxSRGClockSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$136	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$136, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_breg20 -2]
$C$DW$137	.dwtag  DW_TAG_variable, DW_AT_name("srgClockSource")
	.dwattr $C$DW$137, DW_AT_TI_symbol_name("_srgClockSource")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1838| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1838| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1847,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |1847| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1847| 
        MOVL      XAR4,ACC              ; [CPU_] |1847| 
        MOV       AL,*-SP[3]            ; [CPU_] |1847| 
        ANDB      AL,#0x01              ; [CPU_] |1847| 
        AND       AH,*+XAR4[0],#0xdfff  ; [CPU_] |1847| 
        LSL       AL,13                 ; [CPU_] |1847| 
        MOVZ      AR6,AL                ; [CPU_] |1847| 
        OR        AR6,AH                ; [CPU_] |1847| 
        MOVB      ACC,#10               ; [CPU_] |1847| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1847| 
        MOVL      XAR4,ACC              ; [CPU_] |1847| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1847| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1853,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |1853| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1853| 
        MOVL      XAR4,ACC              ; [CPU_] |1853| 
        MOV       AL,*-SP[3]            ; [CPU_] |1853| 
        LSR       AL,1                  ; [CPU_] |1853| 
        AND       AH,*+XAR4[0],#0xff7f  ; [CPU_] |1853| 
        LSL       AL,7                  ; [CPU_] |1853| 
        MOVZ      AR6,AL                ; [CPU_] |1853| 
        OR        AR6,AH                ; [CPU_] |1853| 
        MOVB      ACC,#18               ; [CPU_] |1853| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1853| 
        MOVL      XAR4,ACC              ; [CPU_] |1853| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1853| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1856,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$138	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$138, DW_AT_low_pc(0x00)
	.dwattr $C$DW$138, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$133, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$133, DW_AT_TI_end_line(0x740)
	.dwattr $C$DW$133, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$133

	.sect	".text"
	.clink

$C$DW$139	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxInternalFrameSyncSource")
	.dwattr $C$DW$139, DW_AT_low_pc(_McBSP_setTxInternalFrameSyncSource)
	.dwattr $C$DW$139, DW_AT_high_pc(0x00)
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_McBSP_setTxInternalFrameSyncSource")
	.dwattr $C$DW$139, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$139, DW_AT_TI_begin_line(0x75b)
	.dwattr $C$DW$139, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$139, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1885,column 1,is_stmt,address _McBSP_setTxInternalFrameSyncSource

	.dwfde $C$DW$CIE, _McBSP_setTxInternalFrameSyncSource
$C$DW$140	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg0]
$C$DW$141	.dwtag  DW_TAG_formal_parameter, DW_AT_name("syncMode")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_syncMode")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxInternalFrameSyncSource FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxInternalFrameSyncSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_breg20 -2]
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("syncMode")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_syncMode")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1885| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1885| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1894,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |1894| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1894| 
        MOVL      XAR4,ACC              ; [CPU_] |1894| 
        AND       AL,*+XAR4[0],#0xefff  ; [CPU_] |1894| 
        OR        AL,*-SP[3]            ; [CPU_] |1894| 
        MOVZ      AR6,AL                ; [CPU_] |1894| 
        MOVB      ACC,#10               ; [CPU_] |1894| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1894| 
        MOVL      XAR4,ACC              ; [CPU_] |1894| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1894| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1896,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$144	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$144, DW_AT_low_pc(0x00)
	.dwattr $C$DW$144, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$139, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$139, DW_AT_TI_end_line(0x768)
	.dwattr $C$DW$139, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$139

	.sect	".text"
	.clink

$C$DW$145	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxMultichannelPartition")
	.dwattr $C$DW$145, DW_AT_low_pc(_McBSP_setRxMultichannelPartition)
	.dwattr $C$DW$145, DW_AT_high_pc(0x00)
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_McBSP_setRxMultichannelPartition")
	.dwattr $C$DW$145, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$145, DW_AT_TI_begin_line(0x779)
	.dwattr $C$DW$145, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$145, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1915,column 1,is_stmt,address _McBSP_setRxMultichannelPartition

	.dwfde $C$DW$CIE, _McBSP_setRxMultichannelPartition
$C$DW$146	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$146, DW_AT_location[DW_OP_reg0]
$C$DW$147	.dwtag  DW_TAG_formal_parameter, DW_AT_name("partition")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxMultichannelPartition FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxMultichannelPartition:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$148, DW_AT_location[DW_OP_breg20 -2]
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("partition")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$149, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1915| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1915| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1924,column 5,is_stmt
        MOVB      ACC,#13               ; [CPU_] |1924| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1924| 
        MOVL      XAR4,ACC              ; [CPU_] |1924| 
        AND       AL,*+XAR4[0],#0xfdff  ; [CPU_] |1924| 
        OR        AL,*-SP[3]            ; [CPU_] |1924| 
        MOVZ      AR6,AL                ; [CPU_] |1924| 
        MOVB      ACC,#13               ; [CPU_] |1924| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1924| 
        MOVL      XAR4,ACC              ; [CPU_] |1924| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1924| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1926,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$150	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$150, DW_AT_low_pc(0x00)
	.dwattr $C$DW$150, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$145, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$145, DW_AT_TI_end_line(0x786)
	.dwattr $C$DW$145, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$145

	.sect	".text"
	.clink

$C$DW$151	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxTwoPartitionBlock")
	.dwattr $C$DW$151, DW_AT_low_pc(_McBSP_setRxTwoPartitionBlock)
	.dwattr $C$DW$151, DW_AT_high_pc(0x00)
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_McBSP_setRxTwoPartitionBlock")
	.dwattr $C$DW$151, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$151, DW_AT_TI_begin_line(0x79c)
	.dwattr $C$DW$151, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$151, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1949,column 1,is_stmt,address _McBSP_setRxTwoPartitionBlock

	.dwfde $C$DW$CIE, _McBSP_setRxTwoPartitionBlock
$C$DW$152	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$152, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg0]
$C$DW$153	.dwtag  DW_TAG_formal_parameter, DW_AT_name("block")
	.dwattr $C$DW$153, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxTwoPartitionBlock FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxTwoPartitionBlock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$154, DW_AT_location[DW_OP_breg20 -2]
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$155, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |1949| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1949| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1958,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |1958| 
        BF        $C$L3,EQ              ; [CPU_] |1958| 
        ; branchcc occurs ; [] |1958| 
        CMPB      AL,#2                 ; [CPU_] |1958| 
        BF        $C$L3,EQ              ; [CPU_] |1958| 
        ; branchcc occurs ; [] |1958| 
        CMPB      AL,#4                 ; [CPU_] |1958| 
        BF        $C$L3,EQ              ; [CPU_] |1958| 
        ; branchcc occurs ; [] |1958| 
        CMPB      AL,#6                 ; [CPU_] |1958| 
        BF        $C$L4,NEQ             ; [CPU_] |1958| 
        ; branchcc occurs ; [] |1958| 
$C$L3:    
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1966,column 9,is_stmt
        MOVB      ACC,#13               ; [CPU_] |1966| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1966| 
        MOVL      XAR4,ACC              ; [CPU_] |1966| 
        MOV       AL,*-SP[3]            ; [CPU_] |1966| 
        LSR       AL,1                  ; [CPU_] |1966| 
        AND       AH,*+XAR4[0],#0xff9f  ; [CPU_] |1966| 
        LSL       AL,5                  ; [CPU_] |1966| 
        MOVZ      AR6,AL                ; [CPU_] |1966| 
        OR        AR6,AH                ; [CPU_] |1966| 
        MOVB      ACC,#13               ; [CPU_] |1966| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1966| 
        MOVL      XAR4,ACC              ; [CPU_] |1966| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1966| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1969,column 5,is_stmt
        B         $C$L5,UNC             ; [CPU_] |1969| 
        ; branch occurs ; [] |1969| 
$C$L4:    
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1975,column 9,is_stmt
        MOVB      ACC,#13               ; [CPU_] |1975| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1975| 
        MOVL      XAR4,ACC              ; [CPU_] |1975| 
        MOV       AL,*-SP[3]            ; [CPU_] |1975| 
        LSR       AL,1                  ; [CPU_] |1975| 
        AND       AH,*+XAR4[0],#0xfe7f  ; [CPU_] |1975| 
        LSL       AL,7                  ; [CPU_] |1975| 
        MOVZ      AR6,AL                ; [CPU_] |1975| 
        OR        AR6,AH                ; [CPU_] |1975| 
        MOVB      ACC,#13               ; [CPU_] |1975| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |1975| 
        MOVL      XAR4,ACC              ; [CPU_] |1975| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |1975| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 1979,column 1,is_stmt
$C$L5:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$156	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$156, DW_AT_low_pc(0x00)
	.dwattr $C$DW$156, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$151, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$151, DW_AT_TI_end_line(0x7bb)
	.dwattr $C$DW$151, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$151

	.sect	".text"
	.clink

$C$DW$157	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxChannelMode")
	.dwattr $C$DW$157, DW_AT_low_pc(_McBSP_setRxChannelMode)
	.dwattr $C$DW$157, DW_AT_high_pc(0x00)
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_McBSP_setRxChannelMode")
	.dwattr $C$DW$157, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$157, DW_AT_TI_begin_line(0x7eb)
	.dwattr $C$DW$157, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$157, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2028,column 1,is_stmt,address _McBSP_setRxChannelMode

	.dwfde $C$DW$CIE, _McBSP_setRxChannelMode
$C$DW$158	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$158, DW_AT_location[DW_OP_reg0]
$C$DW$159	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channelMode")
	.dwattr $C$DW$159, DW_AT_TI_symbol_name("_channelMode")
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$159, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxChannelMode       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxChannelMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$160	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$160, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$160, DW_AT_location[DW_OP_breg20 -2]
$C$DW$161	.dwtag  DW_TAG_variable, DW_AT_name("channelMode")
	.dwattr $C$DW$161, DW_AT_TI_symbol_name("_channelMode")
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$161, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2028| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2028| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2037,column 5,is_stmt
        MOVB      ACC,#13               ; [CPU_] |2037| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2037| 
        MOVL      XAR4,ACC              ; [CPU_] |2037| 
        AND       AL,*+XAR4[0],#0xfffe  ; [CPU_] |2037| 
        OR        AL,*-SP[3]            ; [CPU_] |2037| 
        MOVZ      AR6,AL                ; [CPU_] |2037| 
        MOVB      ACC,#13               ; [CPU_] |2037| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2037| 
        MOVL      XAR4,ACC              ; [CPU_] |2037| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2037| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2039,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$162	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$162, DW_AT_low_pc(0x00)
	.dwattr $C$DW$162, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$157, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$157, DW_AT_TI_end_line(0x7f7)
	.dwattr $C$DW$157, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$157

	.sect	".text"
	.clink

$C$DW$163	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxMultichannelPartition")
	.dwattr $C$DW$163, DW_AT_low_pc(_McBSP_setTxMultichannelPartition)
	.dwattr $C$DW$163, DW_AT_high_pc(0x00)
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_McBSP_setTxMultichannelPartition")
	.dwattr $C$DW$163, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$163, DW_AT_TI_begin_line(0x808)
	.dwattr $C$DW$163, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$163, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2058,column 1,is_stmt,address _McBSP_setTxMultichannelPartition

	.dwfde $C$DW$CIE, _McBSP_setTxMultichannelPartition
$C$DW$164	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$164, DW_AT_location[DW_OP_reg0]
$C$DW$165	.dwtag  DW_TAG_formal_parameter, DW_AT_name("partition")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$165, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxMultichannelPartition FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxMultichannelPartition:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$166, DW_AT_location[DW_OP_breg20 -2]
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("partition")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2058| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2058| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2067,column 5,is_stmt
        MOVB      ACC,#12               ; [CPU_] |2067| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2067| 
        MOVL      XAR4,ACC              ; [CPU_] |2067| 
        AND       AL,*+XAR4[0],#0xfdff  ; [CPU_] |2067| 
        OR        AL,*-SP[3]            ; [CPU_] |2067| 
        MOVZ      AR6,AL                ; [CPU_] |2067| 
        MOVB      ACC,#12               ; [CPU_] |2067| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2067| 
        MOVL      XAR4,ACC              ; [CPU_] |2067| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2067| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2069,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$168	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$168, DW_AT_low_pc(0x00)
	.dwattr $C$DW$168, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$163, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$163, DW_AT_TI_end_line(0x815)
	.dwattr $C$DW$163, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$163

	.sect	".text"
	.clink

$C$DW$169	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxTwoPartitionBlock")
	.dwattr $C$DW$169, DW_AT_low_pc(_McBSP_setTxTwoPartitionBlock)
	.dwattr $C$DW$169, DW_AT_high_pc(0x00)
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_McBSP_setTxTwoPartitionBlock")
	.dwattr $C$DW$169, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$169, DW_AT_TI_begin_line(0x82b)
	.dwattr $C$DW$169, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$169, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2092,column 1,is_stmt,address _McBSP_setTxTwoPartitionBlock

	.dwfde $C$DW$CIE, _McBSP_setTxTwoPartitionBlock
$C$DW$170	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg0]
$C$DW$171	.dwtag  DW_TAG_formal_parameter, DW_AT_name("block")
	.dwattr $C$DW$171, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxTwoPartitionBlock FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxTwoPartitionBlock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$172	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$172, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_breg20 -2]
$C$DW$173	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$173, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2092| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2092| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2098,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |2098| 
        BF        $C$L6,EQ              ; [CPU_] |2098| 
        ; branchcc occurs ; [] |2098| 
        CMPB      AL,#2                 ; [CPU_] |2098| 
        BF        $C$L6,EQ              ; [CPU_] |2098| 
        ; branchcc occurs ; [] |2098| 
        CMPB      AL,#4                 ; [CPU_] |2098| 
        BF        $C$L6,EQ              ; [CPU_] |2098| 
        ; branchcc occurs ; [] |2098| 
        CMPB      AL,#6                 ; [CPU_] |2098| 
        BF        $C$L7,NEQ             ; [CPU_] |2098| 
        ; branchcc occurs ; [] |2098| 
$C$L6:    
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2106,column 9,is_stmt
        MOVB      ACC,#12               ; [CPU_] |2106| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2106| 
        MOVL      XAR4,ACC              ; [CPU_] |2106| 
        MOV       AL,*-SP[3]            ; [CPU_] |2106| 
        LSR       AL,1                  ; [CPU_] |2106| 
        AND       AH,*+XAR4[0],#0xff9f  ; [CPU_] |2106| 
        LSL       AL,5                  ; [CPU_] |2106| 
        MOVZ      AR6,AL                ; [CPU_] |2106| 
        OR        AR6,AH                ; [CPU_] |2106| 
        MOVB      ACC,#12               ; [CPU_] |2106| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2106| 
        MOVL      XAR4,ACC              ; [CPU_] |2106| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2106| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2109,column 5,is_stmt
        B         $C$L8,UNC             ; [CPU_] |2109| 
        ; branch occurs ; [] |2109| 
$C$L7:    
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2115,column 9,is_stmt
        MOVB      ACC,#12               ; [CPU_] |2115| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2115| 
        MOVL      XAR4,ACC              ; [CPU_] |2115| 
        MOV       AL,*-SP[3]            ; [CPU_] |2115| 
        LSR       AL,1                  ; [CPU_] |2115| 
        AND       AH,*+XAR4[0],#0xfe7f  ; [CPU_] |2115| 
        LSL       AL,7                  ; [CPU_] |2115| 
        MOVZ      AR6,AL                ; [CPU_] |2115| 
        OR        AR6,AH                ; [CPU_] |2115| 
        MOVB      ACC,#12               ; [CPU_] |2115| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2115| 
        MOVL      XAR4,ACC              ; [CPU_] |2115| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2115| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2119,column 1,is_stmt
$C$L8:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$174	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$174, DW_AT_low_pc(0x00)
	.dwattr $C$DW$174, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$169, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$169, DW_AT_TI_end_line(0x847)
	.dwattr $C$DW$169, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$169

	.sect	".text"
	.clink

$C$DW$175	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxChannelMode")
	.dwattr $C$DW$175, DW_AT_low_pc(_McBSP_setTxChannelMode)
	.dwattr $C$DW$175, DW_AT_high_pc(0x00)
	.dwattr $C$DW$175, DW_AT_TI_symbol_name("_McBSP_setTxChannelMode")
	.dwattr $C$DW$175, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$175, DW_AT_TI_begin_line(0x87c)
	.dwattr $C$DW$175, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$175, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2173,column 1,is_stmt,address _McBSP_setTxChannelMode

	.dwfde $C$DW$CIE, _McBSP_setTxChannelMode
$C$DW$176	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_reg0]
$C$DW$177	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channelMode")
	.dwattr $C$DW$177, DW_AT_TI_symbol_name("_channelMode")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxChannelMode       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxChannelMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$178	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$178, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_breg20 -2]
$C$DW$179	.dwtag  DW_TAG_variable, DW_AT_name("channelMode")
	.dwattr $C$DW$179, DW_AT_TI_symbol_name("_channelMode")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$179, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2173| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2173| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2182,column 5,is_stmt
        MOVB      ACC,#12               ; [CPU_] |2182| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2182| 
        MOVL      XAR4,ACC              ; [CPU_] |2182| 
        AND       AL,*+XAR4[0],#0xfffc  ; [CPU_] |2182| 
        OR        AL,*-SP[3]            ; [CPU_] |2182| 
        MOVZ      AR6,AL                ; [CPU_] |2182| 
        MOVB      ACC,#12               ; [CPU_] |2182| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2182| 
        MOVL      XAR4,ACC              ; [CPU_] |2182| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2182| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2185,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$180	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$180, DW_AT_low_pc(0x00)
	.dwattr $C$DW$180, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$175, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$175, DW_AT_TI_end_line(0x889)
	.dwattr $C$DW$175, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$175

	.sect	".text"
	.clink

$C$DW$181	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxFrameSyncSource")
	.dwattr $C$DW$181, DW_AT_low_pc(_McBSP_setTxFrameSyncSource)
	.dwattr $C$DW$181, DW_AT_high_pc(0x00)
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_McBSP_setTxFrameSyncSource")
	.dwattr $C$DW$181, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$181, DW_AT_TI_begin_line(0x89e)
	.dwattr $C$DW$181, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$181, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2208,column 1,is_stmt,address _McBSP_setTxFrameSyncSource

	.dwfde $C$DW$CIE, _McBSP_setTxFrameSyncSource
$C$DW$182	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$182, DW_AT_location[DW_OP_reg0]
$C$DW$183	.dwtag  DW_TAG_formal_parameter, DW_AT_name("syncSource")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_syncSource")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$183, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxFrameSyncSource   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxFrameSyncSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$184	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$184, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_breg20 -2]
$C$DW$185	.dwtag  DW_TAG_variable, DW_AT_name("syncSource")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_syncSource")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2208| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2208| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2217,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2217| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2217| 
        MOVL      XAR4,ACC              ; [CPU_] |2217| 
        AND       AL,*+XAR4[0],#0xf7ff  ; [CPU_] |2217| 
        OR        AL,*-SP[3]            ; [CPU_] |2217| 
        MOVZ      AR6,AL                ; [CPU_] |2217| 
        MOVB      ACC,#18               ; [CPU_] |2217| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2217| 
        MOVL      XAR4,ACC              ; [CPU_] |2217| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2217| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2219,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$186	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$186, DW_AT_low_pc(0x00)
	.dwattr $C$DW$186, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$181, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$181, DW_AT_TI_end_line(0x8ab)
	.dwattr $C$DW$181, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$181

	.sect	".text"
	.clink

$C$DW$187	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxFrameSyncSource")
	.dwattr $C$DW$187, DW_AT_low_pc(_McBSP_setRxFrameSyncSource)
	.dwattr $C$DW$187, DW_AT_high_pc(0x00)
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_McBSP_setRxFrameSyncSource")
	.dwattr $C$DW$187, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$187, DW_AT_TI_begin_line(0x8c0)
	.dwattr $C$DW$187, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$187, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2242,column 1,is_stmt,address _McBSP_setRxFrameSyncSource

	.dwfde $C$DW$CIE, _McBSP_setRxFrameSyncSource
$C$DW$188	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$188, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$188, DW_AT_location[DW_OP_reg0]
$C$DW$189	.dwtag  DW_TAG_formal_parameter, DW_AT_name("syncSource")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_syncSource")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$189, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxFrameSyncSource   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxFrameSyncSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$190	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_breg20 -2]
$C$DW$191	.dwtag  DW_TAG_variable, DW_AT_name("syncSource")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_syncSource")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2242| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2242| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2251,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2251| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2251| 
        MOVL      XAR4,ACC              ; [CPU_] |2251| 
        AND       AL,*+XAR4[0],#0xfbff  ; [CPU_] |2251| 
        OR        AL,*-SP[3]            ; [CPU_] |2251| 
        MOVZ      AR6,AL                ; [CPU_] |2251| 
        MOVB      ACC,#18               ; [CPU_] |2251| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2251| 
        MOVL      XAR4,ACC              ; [CPU_] |2251| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2251| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2253,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$192	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$192, DW_AT_low_pc(0x00)
	.dwattr $C$DW$192, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$187, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$187, DW_AT_TI_end_line(0x8cd)
	.dwattr $C$DW$187, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$187

	.sect	".text"
	.clink

$C$DW$193	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxClockSource")
	.dwattr $C$DW$193, DW_AT_low_pc(_McBSP_setTxClockSource)
	.dwattr $C$DW$193, DW_AT_high_pc(0x00)
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_McBSP_setTxClockSource")
	.dwattr $C$DW$193, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$193, DW_AT_TI_begin_line(0x8e0)
	.dwattr $C$DW$193, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$193, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2273,column 1,is_stmt,address _McBSP_setTxClockSource

	.dwfde $C$DW$CIE, _McBSP_setTxClockSource
$C$DW$194	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg0]
$C$DW$195	.dwtag  DW_TAG_formal_parameter, DW_AT_name("clockSource")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_clockSource")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxClockSource       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxClockSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$196	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$196, DW_AT_location[DW_OP_breg20 -2]
$C$DW$197	.dwtag  DW_TAG_variable, DW_AT_name("clockSource")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_clockSource")
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$197, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2273| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2273| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2282,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2282| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2282| 
        MOVL      XAR4,ACC              ; [CPU_] |2282| 
        AND       AL,*+XAR4[0],#0xfdff  ; [CPU_] |2282| 
        OR        AL,*-SP[3]            ; [CPU_] |2282| 
        MOVZ      AR6,AL                ; [CPU_] |2282| 
        MOVB      ACC,#18               ; [CPU_] |2282| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2282| 
        MOVL      XAR4,ACC              ; [CPU_] |2282| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2282| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2284,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$198	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$198, DW_AT_low_pc(0x00)
	.dwattr $C$DW$198, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$193, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$193, DW_AT_TI_end_line(0x8ec)
	.dwattr $C$DW$193, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$193

	.sect	".text"
	.clink

$C$DW$199	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxClockSource")
	.dwattr $C$DW$199, DW_AT_low_pc(_McBSP_setRxClockSource)
	.dwattr $C$DW$199, DW_AT_high_pc(0x00)
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_McBSP_setRxClockSource")
	.dwattr $C$DW$199, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$199, DW_AT_TI_begin_line(0x8ff)
	.dwattr $C$DW$199, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$199, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2304,column 1,is_stmt,address _McBSP_setRxClockSource

	.dwfde $C$DW$CIE, _McBSP_setRxClockSource
$C$DW$200	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg0]
$C$DW$201	.dwtag  DW_TAG_formal_parameter, DW_AT_name("clockSource")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_clockSource")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxClockSource       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxClockSource:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$202	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_breg20 -2]
$C$DW$203	.dwtag  DW_TAG_variable, DW_AT_name("clockSource")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_clockSource")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$203, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2304| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2304| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2313,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2313| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2313| 
        MOVL      XAR4,ACC              ; [CPU_] |2313| 
        AND       AL,*+XAR4[0],#0xfeff  ; [CPU_] |2313| 
        OR        AL,*-SP[3]            ; [CPU_] |2313| 
        MOVZ      AR6,AL                ; [CPU_] |2313| 
        MOVB      ACC,#18               ; [CPU_] |2313| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2313| 
        MOVL      XAR4,ACC              ; [CPU_] |2313| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2313| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2315,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$204	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$204, DW_AT_low_pc(0x00)
	.dwattr $C$DW$204, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$199, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$199, DW_AT_TI_end_line(0x90b)
	.dwattr $C$DW$199, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$199

	.sect	".text"
	.clink

$C$DW$205	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxFrameSyncPolarity")
	.dwattr $C$DW$205, DW_AT_low_pc(_McBSP_setTxFrameSyncPolarity)
	.dwattr $C$DW$205, DW_AT_high_pc(0x00)
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_McBSP_setTxFrameSyncPolarity")
	.dwattr $C$DW$205, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$205, DW_AT_TI_begin_line(0x91d)
	.dwattr $C$DW$205, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$205, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2335,column 1,is_stmt,address _McBSP_setTxFrameSyncPolarity

	.dwfde $C$DW$CIE, _McBSP_setTxFrameSyncPolarity
$C$DW$206	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg0]
$C$DW$207	.dwtag  DW_TAG_formal_parameter, DW_AT_name("syncPolarity")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_syncPolarity")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxFrameSyncPolarity FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxFrameSyncPolarity:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_breg20 -2]
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("syncPolarity")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_syncPolarity")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2335| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2335| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2344,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2344| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2344| 
        MOVL      XAR4,ACC              ; [CPU_] |2344| 
        AND       AL,*+XAR4[0],#0xfff7  ; [CPU_] |2344| 
        OR        AL,*-SP[3]            ; [CPU_] |2344| 
        MOVZ      AR6,AL                ; [CPU_] |2344| 
        MOVB      ACC,#18               ; [CPU_] |2344| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2344| 
        MOVL      XAR4,ACC              ; [CPU_] |2344| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2344| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2346,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$210	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$210, DW_AT_low_pc(0x00)
	.dwattr $C$DW$210, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$205, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$205, DW_AT_TI_end_line(0x92a)
	.dwattr $C$DW$205, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$205

	.sect	".text"
	.clink

$C$DW$211	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxFrameSyncPolarity")
	.dwattr $C$DW$211, DW_AT_low_pc(_McBSP_setRxFrameSyncPolarity)
	.dwattr $C$DW$211, DW_AT_high_pc(0x00)
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_McBSP_setRxFrameSyncPolarity")
	.dwattr $C$DW$211, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$211, DW_AT_TI_begin_line(0x93b)
	.dwattr $C$DW$211, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$211, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2365,column 1,is_stmt,address _McBSP_setRxFrameSyncPolarity

	.dwfde $C$DW$CIE, _McBSP_setRxFrameSyncPolarity
$C$DW$212	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_reg0]
$C$DW$213	.dwtag  DW_TAG_formal_parameter, DW_AT_name("syncPolarity")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_syncPolarity")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxFrameSyncPolarity FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxFrameSyncPolarity:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$214	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_breg20 -2]
$C$DW$215	.dwtag  DW_TAG_variable, DW_AT_name("syncPolarity")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_syncPolarity")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2365| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2365| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2374,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2374| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2374| 
        MOVL      XAR4,ACC              ; [CPU_] |2374| 
        AND       AL,*+XAR4[0],#0xfffb  ; [CPU_] |2374| 
        OR        AL,*-SP[3]            ; [CPU_] |2374| 
        MOVZ      AR6,AL                ; [CPU_] |2374| 
        MOVB      ACC,#18               ; [CPU_] |2374| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2374| 
        MOVL      XAR4,ACC              ; [CPU_] |2374| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2374| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2376,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$211, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$211, DW_AT_TI_end_line(0x948)
	.dwattr $C$DW$211, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$211

	.sect	".text"
	.clink

$C$DW$217	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxClockPolarity")
	.dwattr $C$DW$217, DW_AT_low_pc(_McBSP_setTxClockPolarity)
	.dwattr $C$DW$217, DW_AT_high_pc(0x00)
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_McBSP_setTxClockPolarity")
	.dwattr $C$DW$217, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$217, DW_AT_TI_begin_line(0x95a)
	.dwattr $C$DW$217, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$217, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2396,column 1,is_stmt,address _McBSP_setTxClockPolarity

	.dwfde $C$DW$CIE, _McBSP_setTxClockPolarity
$C$DW$218	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg0]
$C$DW$219	.dwtag  DW_TAG_formal_parameter, DW_AT_name("clockPolarity")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_clockPolarity")
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$219, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setTxClockPolarity     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxClockPolarity:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$220	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_breg20 -2]
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("clockPolarity")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_clockPolarity")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2396| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2396| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2405,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2405| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2405| 
        MOVL      XAR4,ACC              ; [CPU_] |2405| 
        AND       AL,*+XAR4[0],#0xfffd  ; [CPU_] |2405| 
        OR        AL,*-SP[3]            ; [CPU_] |2405| 
        MOVZ      AR6,AL                ; [CPU_] |2405| 
        MOVB      ACC,#18               ; [CPU_] |2405| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2405| 
        MOVL      XAR4,ACC              ; [CPU_] |2405| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2405| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2408,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$222	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$222, DW_AT_low_pc(0x00)
	.dwattr $C$DW$222, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$217, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$217, DW_AT_TI_end_line(0x968)
	.dwattr $C$DW$217, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$217

	.sect	".text"
	.clink

$C$DW$223	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxClockPolarity")
	.dwattr $C$DW$223, DW_AT_low_pc(_McBSP_setRxClockPolarity)
	.dwattr $C$DW$223, DW_AT_high_pc(0x00)
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_McBSP_setRxClockPolarity")
	.dwattr $C$DW$223, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$223, DW_AT_TI_begin_line(0x97b)
	.dwattr $C$DW$223, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$223, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2429,column 1,is_stmt,address _McBSP_setRxClockPolarity

	.dwfde $C$DW$CIE, _McBSP_setRxClockPolarity
$C$DW$224	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg0]
$C$DW$225	.dwtag  DW_TAG_formal_parameter, DW_AT_name("clockPolarity")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_clockPolarity")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_setRxClockPolarity     FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxClockPolarity:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$226	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_breg20 -2]
$C$DW$227	.dwtag  DW_TAG_variable, DW_AT_name("clockPolarity")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_clockPolarity")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2429| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2429| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2438,column 5,is_stmt
        MOVB      ACC,#18               ; [CPU_] |2438| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2438| 
        MOVL      XAR4,ACC              ; [CPU_] |2438| 
        AND       AL,*+XAR4[0],#0xfffe  ; [CPU_] |2438| 
        OR        AL,*-SP[3]            ; [CPU_] |2438| 
        MOVZ      AR6,AL                ; [CPU_] |2438| 
        MOVB      ACC,#18               ; [CPU_] |2438| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2438| 
        MOVL      XAR4,ACC              ; [CPU_] |2438| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2438| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2441,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$223, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$223, DW_AT_TI_end_line(0x989)
	.dwattr $C$DW$223, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$223

	.sect	".text"
	.clink

$C$DW$229	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_read16bitData")
	.dwattr $C$DW$229, DW_AT_low_pc(_McBSP_read16bitData)
	.dwattr $C$DW$229, DW_AT_high_pc(0x00)
	.dwattr $C$DW$229, DW_AT_TI_symbol_name("_McBSP_read16bitData")
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$229, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$229, DW_AT_TI_begin_line(0x997)
	.dwattr $C$DW$229, DW_AT_TI_begin_column(0x0a)
	.dwattr $C$DW$229, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2456,column 1,is_stmt,address _McBSP_read16bitData

	.dwfde $C$DW$CIE, _McBSP_read16bitData
$C$DW$230	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$230, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$230, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_read16bitData          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_read16bitData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$231	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$231, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |2456| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2465,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |2465| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2465| 
        MOVL      XAR4,ACC              ; [CPU_] |2465| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |2465| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2466,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$229, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$229, DW_AT_TI_end_line(0x9a2)
	.dwattr $C$DW$229, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$229

	.sect	".text"
	.clink

$C$DW$233	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_read32bitData")
	.dwattr $C$DW$233, DW_AT_low_pc(_McBSP_read32bitData)
	.dwattr $C$DW$233, DW_AT_high_pc(0x00)
	.dwattr $C$DW$233, DW_AT_TI_symbol_name("_McBSP_read32bitData")
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$233, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$233, DW_AT_TI_begin_line(0x9b0)
	.dwattr $C$DW$233, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$233, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2481,column 1,is_stmt,address _McBSP_read32bitData

	.dwfde $C$DW$CIE, _McBSP_read32bitData
$C$DW$234	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$234, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _McBSP_read32bitData          FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_read32bitData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$235	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$235, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |2481| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2490,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |2490| 
        MOVL      XAR5,*-SP[2]          ; [CPU_] |2490| 
        CLRC      SXM                   ; [CPU_] 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2490| 
        MOVL      XAR4,ACC              ; [CPU_] |2490| 
        MOV       ACC,*+XAR5[0] << 16   ; [CPU_] |2490| 
        OR        ACC,*+XAR4[0]         ; [CPU_] |2490| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2492,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$236	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$236, DW_AT_low_pc(0x00)
	.dwattr $C$DW$236, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$233, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$233, DW_AT_TI_end_line(0x9bc)
	.dwattr $C$DW$233, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$233

	.sect	".text"
	.clink

$C$DW$237	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_write16bitData")
	.dwattr $C$DW$237, DW_AT_low_pc(_McBSP_write16bitData)
	.dwattr $C$DW$237, DW_AT_high_pc(0x00)
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_McBSP_write16bitData")
	.dwattr $C$DW$237, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$237, DW_AT_TI_begin_line(0x9cb)
	.dwattr $C$DW$237, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$237, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2508,column 1,is_stmt,address _McBSP_write16bitData

	.dwfde $C$DW$CIE, _McBSP_write16bitData
$C$DW$238	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_reg0]
$C$DW$239	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$239, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_write16bitData         FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_write16bitData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$240	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$240, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_breg20 -2]
$C$DW$241	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$241, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$241, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |2508| 
        MOVL      *-SP[2],ACC           ; [CPU_] |2508| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2517,column 5,is_stmt
        MOVZ      AR6,*-SP[3]           ; [CPU_] |2517| 
        MOVB      ACC,#3                ; [CPU_] |2517| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2517| 
        MOVL      XAR4,ACC              ; [CPU_] |2517| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2517| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2518,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$242	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$242, DW_AT_low_pc(0x00)
	.dwattr $C$DW$242, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$237, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$237, DW_AT_TI_end_line(0x9d6)
	.dwattr $C$DW$237, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$237

	.sect	".text"
	.clink

$C$DW$243	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_write32bitData")
	.dwattr $C$DW$243, DW_AT_low_pc(_McBSP_write32bitData)
	.dwattr $C$DW$243, DW_AT_high_pc(0x00)
	.dwattr $C$DW$243, DW_AT_TI_symbol_name("_McBSP_write32bitData")
	.dwattr $C$DW$243, DW_AT_TI_begin_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$243, DW_AT_TI_begin_line(0x9e5)
	.dwattr $C$DW$243, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$243, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2534,column 1,is_stmt,address _McBSP_write32bitData

	.dwfde $C$DW$CIE, _McBSP_write32bitData
$C$DW$244	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$244, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_reg0]
$C$DW$245	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$245, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_breg20 -6]

;***************************************************************
;* FNAME: _McBSP_write32bitData         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_write32bitData:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$246	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |2534| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2543,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |2543| 
        MOVU      ACC,AH                ; [CPU_] |2543| 
        MOVL      XAR6,ACC              ; [CPU_] |2543| 
        MOVB      ACC,#2                ; [CPU_] |2543| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2543| 
        MOVL      XAR4,ACC              ; [CPU_] |2543| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2543| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2548,column 5,is_stmt
        MOVZ      AR6,*-SP[6]           ; [CPU_] |2548| 
        MOVB      ACC,#3                ; [CPU_] |2548| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |2548| 
        MOVL      XAR4,ACC              ; [CPU_] |2548| 
        MOV       *+XAR4[0],AR6         ; [CPU_] |2548| 
	.dwpsn	file "..\ExtraData\driverlib2\mcbsp.h",line 2549,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$243, DW_AT_TI_end_file("..\ExtraData\driverlib2\mcbsp.h")
	.dwattr $C$DW$243, DW_AT_TI_end_line(0x9f5)
	.dwattr $C$DW$243, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$243

	.sect	".text"
	.clink
	.global	_McBSP_transmit16BitDataNonBlocking

$C$DW$248	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_transmit16BitDataNonBlocking")
	.dwattr $C$DW$248, DW_AT_low_pc(_McBSP_transmit16BitDataNonBlocking)
	.dwattr $C$DW$248, DW_AT_high_pc(0x00)
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_McBSP_transmit16BitDataNonBlocking")
	.dwattr $C$DW$248, DW_AT_external
	.dwattr $C$DW$248, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$248, DW_AT_TI_begin_line(0x35)
	.dwattr $C$DW$248, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$248, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 54,column 1,is_stmt,address _McBSP_transmit16BitDataNonBlocking

	.dwfde $C$DW$CIE, _McBSP_transmit16BitDataNonBlocking
$C$DW$249	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_reg0]
$C$DW$250	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$250, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_transmit16BitDataNonBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_transmit16BitDataNonBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$251	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$251, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_breg20 -2]
$C$DW$252	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$252, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |54| 
        MOVL      *-SP[2],ACC           ; [CPU_] |54| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 63,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |63| 
$C$DW$253	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$253, DW_AT_low_pc(0x00)
	.dwattr $C$DW$253, DW_AT_name("_McBSP_write16bitData")
	.dwattr $C$DW$253, DW_AT_TI_call
        LCR       #_McBSP_write16bitData ; [CPU_] |63| 
        ; call occurs [#_McBSP_write16bitData] ; [] |63| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 64,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$254	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$254, DW_AT_low_pc(0x00)
	.dwattr $C$DW$254, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$248, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$248, DW_AT_TI_end_line(0x40)
	.dwattr $C$DW$248, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$248

	.sect	".text"
	.clink
	.global	_McBSP_transmit16BitDataBlocking

$C$DW$255	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_transmit16BitDataBlocking")
	.dwattr $C$DW$255, DW_AT_low_pc(_McBSP_transmit16BitDataBlocking)
	.dwattr $C$DW$255, DW_AT_high_pc(0x00)
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_McBSP_transmit16BitDataBlocking")
	.dwattr $C$DW$255, DW_AT_external
	.dwattr $C$DW$255, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$255, DW_AT_TI_begin_line(0x48)
	.dwattr $C$DW$255, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$255, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 73,column 1,is_stmt,address _McBSP_transmit16BitDataBlocking

	.dwfde $C$DW$CIE, _McBSP_transmit16BitDataBlocking
$C$DW$256	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$256, DW_AT_location[DW_OP_reg0]
$C$DW$257	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$257, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_transmit16BitDataBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_McBSP_transmit16BitDataBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$258	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$258, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$258, DW_AT_location[DW_OP_breg20 -2]
$C$DW$259	.dwtag  DW_TAG_variable, DW_AT_name("data")
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[3],AR4           ; [CPU_] |73| 
        MOVL      *-SP[2],ACC           ; [CPU_] |73| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 82,column 5,is_stmt
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 82,column 11,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |82| 
$C$DW$260	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$260, DW_AT_low_pc(0x00)
	.dwattr $C$DW$260, DW_AT_name("_McBSP_isTxReady")
	.dwattr $C$DW$260, DW_AT_TI_call
        LCR       #_McBSP_isTxReady     ; [CPU_] |82| 
        ; call occurs [#_McBSP_isTxReady] ; [] |82| 
        CMPB      AL,#0                 ; [CPU_] |82| 
        BF        $C$L9,EQ              ; [CPU_] |82| 
        ; branchcc occurs ; [] |82| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 89,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |89| 
        MOVZ      AR4,*-SP[3]           ; [CPU_] |89| 
$C$DW$261	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$261, DW_AT_low_pc(0x00)
	.dwattr $C$DW$261, DW_AT_name("_McBSP_write16bitData")
	.dwattr $C$DW$261, DW_AT_TI_call
        LCR       #_McBSP_write16bitData ; [CPU_] |89| 
        ; call occurs [#_McBSP_write16bitData] ; [] |89| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 90,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$255, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$255, DW_AT_TI_end_line(0x5a)
	.dwattr $C$DW$255, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$255

	.sect	".text"
	.clink
	.global	_McBSP_transmit32BitDataNonBlocking

$C$DW$263	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_transmit32BitDataNonBlocking")
	.dwattr $C$DW$263, DW_AT_low_pc(_McBSP_transmit32BitDataNonBlocking)
	.dwattr $C$DW$263, DW_AT_high_pc(0x00)
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_McBSP_transmit32BitDataNonBlocking")
	.dwattr $C$DW$263, DW_AT_external
	.dwattr $C$DW$263, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$263, DW_AT_TI_begin_line(0x62)
	.dwattr $C$DW$263, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$263, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 99,column 1,is_stmt,address _McBSP_transmit32BitDataNonBlocking

	.dwfde $C$DW$CIE, _McBSP_transmit32BitDataNonBlocking
$C$DW$264	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_reg0]
$C$DW$265	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$265, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$265, DW_AT_location[DW_OP_breg20 -8]

;***************************************************************
;* FNAME: _McBSP_transmit32BitDataNonBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_transmit32BitDataNonBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$266	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$266, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],ACC           ; [CPU_] |99| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 108,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |108| 
        MOVL      *-SP[2],ACC           ; [CPU_] |108| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |108| 
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_name("_McBSP_write32bitData")
	.dwattr $C$DW$267, DW_AT_TI_call
        LCR       #_McBSP_write32bitData ; [CPU_] |108| 
        ; call occurs [#_McBSP_write32bitData] ; [] |108| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 109,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$268	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$268, DW_AT_low_pc(0x00)
	.dwattr $C$DW$268, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$263, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$263, DW_AT_TI_end_line(0x6d)
	.dwattr $C$DW$263, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$263

	.sect	".text"
	.clink
	.global	_McBSP_transmit32BitDataBlocking

$C$DW$269	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_transmit32BitDataBlocking")
	.dwattr $C$DW$269, DW_AT_low_pc(_McBSP_transmit32BitDataBlocking)
	.dwattr $C$DW$269, DW_AT_high_pc(0x00)
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_McBSP_transmit32BitDataBlocking")
	.dwattr $C$DW$269, DW_AT_external
	.dwattr $C$DW$269, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$269, DW_AT_TI_begin_line(0x75)
	.dwattr $C$DW$269, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$269, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 118,column 1,is_stmt,address _McBSP_transmit32BitDataBlocking

	.dwfde $C$DW$CIE, _McBSP_transmit32BitDataBlocking
$C$DW$270	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$270, DW_AT_location[DW_OP_reg0]
$C$DW$271	.dwtag  DW_TAG_formal_parameter, DW_AT_name("data")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$271, DW_AT_location[DW_OP_breg20 -8]

;***************************************************************
;* FNAME: _McBSP_transmit32BitDataBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_McBSP_transmit32BitDataBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$272	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$272, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],ACC           ; [CPU_] |118| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 127,column 5,is_stmt
$C$L10:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 127,column 11,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |127| 
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_name("_McBSP_isTxReady")
	.dwattr $C$DW$273, DW_AT_TI_call
        LCR       #_McBSP_isTxReady     ; [CPU_] |127| 
        ; call occurs [#_McBSP_isTxReady] ; [] |127| 
        CMPB      AL,#0                 ; [CPU_] |127| 
        BF        $C$L10,EQ             ; [CPU_] |127| 
        ; branchcc occurs ; [] |127| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 134,column 5,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |134| 
        MOVL      *-SP[2],ACC           ; [CPU_] |134| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |134| 
$C$DW$274	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$274, DW_AT_low_pc(0x00)
	.dwattr $C$DW$274, DW_AT_name("_McBSP_write32bitData")
	.dwattr $C$DW$274, DW_AT_TI_call
        LCR       #_McBSP_write32bitData ; [CPU_] |134| 
        ; call occurs [#_McBSP_write32bitData] ; [] |134| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 135,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$275	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$275, DW_AT_low_pc(0x00)
	.dwattr $C$DW$275, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$269, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$269, DW_AT_TI_end_line(0x87)
	.dwattr $C$DW$269, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$269

	.sect	".text"
	.clink
	.global	_McBSP_receive16BitDataNonBlocking

$C$DW$276	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_receive16BitDataNonBlocking")
	.dwattr $C$DW$276, DW_AT_low_pc(_McBSP_receive16BitDataNonBlocking)
	.dwattr $C$DW$276, DW_AT_high_pc(0x00)
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_McBSP_receive16BitDataNonBlocking")
	.dwattr $C$DW$276, DW_AT_external
	.dwattr $C$DW$276, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$276, DW_AT_TI_begin_line(0x8f)
	.dwattr $C$DW$276, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$276, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 144,column 1,is_stmt,address _McBSP_receive16BitDataNonBlocking

	.dwfde $C$DW$CIE, _McBSP_receive16BitDataNonBlocking
$C$DW$277	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$277, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$277, DW_AT_location[DW_OP_reg0]
$C$DW$278	.dwtag  DW_TAG_formal_parameter, DW_AT_name("receiveData")
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$278, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_receive16BitDataNonBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_receive16BitDataNonBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$279	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$279, DW_AT_location[DW_OP_breg20 -2]
$C$DW$280	.dwtag  DW_TAG_variable, DW_AT_name("receiveData")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$280, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |144| 
        MOVL      *-SP[2],ACC           ; [CPU_] |144| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 153,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |153| 
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("_McBSP_read16bitData")
	.dwattr $C$DW$281, DW_AT_TI_call
        LCR       #_McBSP_read16bitData ; [CPU_] |153| 
        ; call occurs [#_McBSP_read16bitData] ; [] |153| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |153| 
        MOV       *+XAR4[0],AL          ; [CPU_] |153| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 154,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$282	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$276, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$276, DW_AT_TI_end_line(0x9a)
	.dwattr $C$DW$276, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$276

	.sect	".text"
	.clink
	.global	_McBSP_receive16BitDataBlocking

$C$DW$283	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_receive16BitDataBlocking")
	.dwattr $C$DW$283, DW_AT_low_pc(_McBSP_receive16BitDataBlocking)
	.dwattr $C$DW$283, DW_AT_high_pc(0x00)
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_McBSP_receive16BitDataBlocking")
	.dwattr $C$DW$283, DW_AT_external
	.dwattr $C$DW$283, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$283, DW_AT_TI_begin_line(0xa2)
	.dwattr $C$DW$283, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$283, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 163,column 1,is_stmt,address _McBSP_receive16BitDataBlocking

	.dwfde $C$DW$CIE, _McBSP_receive16BitDataBlocking
$C$DW$284	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$284, DW_AT_location[DW_OP_reg0]
$C$DW$285	.dwtag  DW_TAG_formal_parameter, DW_AT_name("receiveData")
	.dwattr $C$DW$285, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$285, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_receive16BitDataBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_receive16BitDataBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$286	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$286, DW_AT_location[DW_OP_breg20 -2]
$C$DW$287	.dwtag  DW_TAG_variable, DW_AT_name("receiveData")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$287, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |163| 
        MOVL      *-SP[2],ACC           ; [CPU_] |163| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 172,column 5,is_stmt
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 172,column 11,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |172| 
$C$DW$288	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$288, DW_AT_low_pc(0x00)
	.dwattr $C$DW$288, DW_AT_name("_McBSP_isRxReady")
	.dwattr $C$DW$288, DW_AT_TI_call
        LCR       #_McBSP_isRxReady     ; [CPU_] |172| 
        ; call occurs [#_McBSP_isRxReady] ; [] |172| 
        CMPB      AL,#0                 ; [CPU_] |172| 
        BF        $C$L11,EQ             ; [CPU_] |172| 
        ; branchcc occurs ; [] |172| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 179,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |179| 
$C$DW$289	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$289, DW_AT_low_pc(0x00)
	.dwattr $C$DW$289, DW_AT_name("_McBSP_read16bitData")
	.dwattr $C$DW$289, DW_AT_TI_call
        LCR       #_McBSP_read16bitData ; [CPU_] |179| 
        ; call occurs [#_McBSP_read16bitData] ; [] |179| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |179| 
        MOV       *+XAR4[0],AL          ; [CPU_] |179| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 180,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$290	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$290, DW_AT_low_pc(0x00)
	.dwattr $C$DW$290, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$283, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$283, DW_AT_TI_end_line(0xb4)
	.dwattr $C$DW$283, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$283

	.sect	".text"
	.clink
	.global	_McBSP_receive32BitDataNonBlocking

$C$DW$291	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_receive32BitDataNonBlocking")
	.dwattr $C$DW$291, DW_AT_low_pc(_McBSP_receive32BitDataNonBlocking)
	.dwattr $C$DW$291, DW_AT_high_pc(0x00)
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_McBSP_receive32BitDataNonBlocking")
	.dwattr $C$DW$291, DW_AT_external
	.dwattr $C$DW$291, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$291, DW_AT_TI_begin_line(0xbc)
	.dwattr $C$DW$291, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$291, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 189,column 1,is_stmt,address _McBSP_receive32BitDataNonBlocking

	.dwfde $C$DW$CIE, _McBSP_receive32BitDataNonBlocking
$C$DW$292	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_reg0]
$C$DW$293	.dwtag  DW_TAG_formal_parameter, DW_AT_name("receiveData")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$293, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_receive32BitDataNonBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_receive32BitDataNonBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$294	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$294, DW_AT_location[DW_OP_breg20 -2]
$C$DW$295	.dwtag  DW_TAG_variable, DW_AT_name("receiveData")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$295, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |189| 
        MOVL      *-SP[2],ACC           ; [CPU_] |189| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 198,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |198| 
$C$DW$296	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$296, DW_AT_low_pc(0x00)
	.dwattr $C$DW$296, DW_AT_name("_McBSP_read32bitData")
	.dwattr $C$DW$296, DW_AT_TI_call
        LCR       #_McBSP_read32bitData ; [CPU_] |198| 
        ; call occurs [#_McBSP_read32bitData] ; [] |198| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |198| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |198| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 199,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$297	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$297, DW_AT_low_pc(0x00)
	.dwattr $C$DW$297, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$291, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$291, DW_AT_TI_end_line(0xc7)
	.dwattr $C$DW$291, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$291

	.sect	".text"
	.clink
	.global	_McBSP_receive32BitDataBlocking

$C$DW$298	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_receive32BitDataBlocking")
	.dwattr $C$DW$298, DW_AT_low_pc(_McBSP_receive32BitDataBlocking)
	.dwattr $C$DW$298, DW_AT_high_pc(0x00)
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_McBSP_receive32BitDataBlocking")
	.dwattr $C$DW$298, DW_AT_external
	.dwattr $C$DW$298, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$298, DW_AT_TI_begin_line(0xcf)
	.dwattr $C$DW$298, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$298, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 208,column 1,is_stmt,address _McBSP_receive32BitDataBlocking

	.dwfde $C$DW$CIE, _McBSP_receive32BitDataBlocking
$C$DW$299	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$299, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$299, DW_AT_location[DW_OP_reg0]
$C$DW$300	.dwtag  DW_TAG_formal_parameter, DW_AT_name("receiveData")
	.dwattr $C$DW$300, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$300, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_receive32BitDataBlocking FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_receive32BitDataBlocking:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$301	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$301, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$301, DW_AT_location[DW_OP_breg20 -2]
$C$DW$302	.dwtag  DW_TAG_variable, DW_AT_name("receiveData")
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_receiveData")
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$302, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |208| 
        MOVL      *-SP[2],ACC           ; [CPU_] |208| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 217,column 5,is_stmt
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 217,column 11,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |217| 
$C$DW$303	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$303, DW_AT_low_pc(0x00)
	.dwattr $C$DW$303, DW_AT_name("_McBSP_isRxReady")
	.dwattr $C$DW$303, DW_AT_TI_call
        LCR       #_McBSP_isRxReady     ; [CPU_] |217| 
        ; call occurs [#_McBSP_isRxReady] ; [] |217| 
        CMPB      AL,#0                 ; [CPU_] |217| 
        BF        $C$L12,EQ             ; [CPU_] |217| 
        ; branchcc occurs ; [] |217| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 224,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |224| 
$C$DW$304	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$304, DW_AT_low_pc(0x00)
	.dwattr $C$DW$304, DW_AT_name("_McBSP_read32bitData")
	.dwattr $C$DW$304, DW_AT_TI_call
        LCR       #_McBSP_read32bitData ; [CPU_] |224| 
        ; call occurs [#_McBSP_read32bitData] ; [] |224| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |224| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |224| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 225,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$305	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$305, DW_AT_low_pc(0x00)
	.dwattr $C$DW$305, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$298, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$298, DW_AT_TI_end_line(0xe1)
	.dwattr $C$DW$298, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$298

	.sect	".text"
	.clink
	.global	_McBSP_setRxDataSize

$C$DW$306	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setRxDataSize")
	.dwattr $C$DW$306, DW_AT_low_pc(_McBSP_setRxDataSize)
	.dwattr $C$DW$306, DW_AT_high_pc(0x00)
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_McBSP_setRxDataSize")
	.dwattr $C$DW$306, DW_AT_external
	.dwattr $C$DW$306, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$306, DW_AT_TI_begin_line(0xe9)
	.dwattr $C$DW$306, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$306, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 236,column 1,is_stmt,address _McBSP_setRxDataSize

	.dwfde $C$DW$CIE, _McBSP_setRxDataSize
$C$DW$307	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$307, DW_AT_location[DW_OP_reg0]
$C$DW$308	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataFrame")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_dataFrame")
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$308, DW_AT_location[DW_OP_reg12]
$C$DW$309	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bitsPerWord")
	.dwattr $C$DW$309, DW_AT_TI_symbol_name("_bitsPerWord")
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$309, DW_AT_location[DW_OP_reg14]
$C$DW$310	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wordsPerFrame")
	.dwattr $C$DW$310, DW_AT_TI_symbol_name("_wordsPerFrame")
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$310, DW_AT_location[DW_OP_breg20 -7]

;***************************************************************
;* FNAME: _McBSP_setRxDataSize          FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_setRxDataSize:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$311	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$311, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$311, DW_AT_location[DW_OP_breg20 -2]
$C$DW$312	.dwtag  DW_TAG_variable, DW_AT_name("dataFrame")
	.dwattr $C$DW$312, DW_AT_TI_symbol_name("_dataFrame")
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$312, DW_AT_location[DW_OP_breg20 -3]
$C$DW$313	.dwtag  DW_TAG_variable, DW_AT_name("bitsPerWord")
	.dwattr $C$DW$313, DW_AT_TI_symbol_name("_bitsPerWord")
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$313, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |236| 
        MOV       *-SP[3],AR4           ; [CPU_] |236| 
        MOVL      *-SP[2],ACC           ; [CPU_] |236| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 243,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |243| 
        BF        $C$L13,NEQ            ; [CPU_] |243| 
        ; branchcc occurs ; [] |243| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 249,column 9,is_stmt
        MOVB      ACC,#7                ; [CPU_] |249| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |249| 
        MOVL      XAR4,ACC              ; [CPU_] |249| 
        AND       AL,*+XAR4[0],#0x801f  ; [CPU_] |249| 
        OR        AL,*-SP[4]            ; [CPU_] |249| 
        MOVZ      AR6,AL                ; [CPU_] |249| 
        MOV       ACC,*-SP[7] << #8     ; [CPU_] |249| 
        OR        AL,AR6                ; [CPU_] |249| 
        MOVZ      AR7,AL                ; [CPU_] |249| 
        MOVB      ACC,#7                ; [CPU_] |249| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |249| 
        MOVL      XAR4,ACC              ; [CPU_] |249| 
        MOV       *+XAR4[0],AR7         ; [CPU_] |249| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 252,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |252| 
        ; branch occurs ; [] |252| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 259,column 9,is_stmt
        MOVB      ACC,#6                ; [CPU_] |259| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |259| 
        MOVL      XAR4,ACC              ; [CPU_] |259| 
        AND       AL,*+XAR4[0],#0x801f  ; [CPU_] |259| 
        OR        AL,*-SP[4]            ; [CPU_] |259| 
        MOVZ      AR6,AL                ; [CPU_] |259| 
        MOV       ACC,*-SP[7] << #8     ; [CPU_] |259| 
        OR        AL,AR6                ; [CPU_] |259| 
        MOVZ      AR7,AL                ; [CPU_] |259| 
        MOVB      ACC,#6                ; [CPU_] |259| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |259| 
        MOVL      XAR4,ACC              ; [CPU_] |259| 
        MOV       *+XAR4[0],AR7         ; [CPU_] |259| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 263,column 1,is_stmt
$C$L14:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$314	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$314, DW_AT_low_pc(0x00)
	.dwattr $C$DW$314, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$306, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$306, DW_AT_TI_end_line(0x107)
	.dwattr $C$DW$306, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$306

	.sect	".text"
	.clink
	.global	_McBSP_setTxDataSize

$C$DW$315	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_setTxDataSize")
	.dwattr $C$DW$315, DW_AT_low_pc(_McBSP_setTxDataSize)
	.dwattr $C$DW$315, DW_AT_high_pc(0x00)
	.dwattr $C$DW$315, DW_AT_TI_symbol_name("_McBSP_setTxDataSize")
	.dwattr $C$DW$315, DW_AT_external
	.dwattr $C$DW$315, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$315, DW_AT_TI_begin_line(0x10f)
	.dwattr $C$DW$315, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$315, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 274,column 1,is_stmt,address _McBSP_setTxDataSize

	.dwfde $C$DW$CIE, _McBSP_setTxDataSize
$C$DW$316	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$316, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$316, DW_AT_location[DW_OP_reg0]
$C$DW$317	.dwtag  DW_TAG_formal_parameter, DW_AT_name("dataFrame")
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_dataFrame")
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$317, DW_AT_location[DW_OP_reg12]
$C$DW$318	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bitsPerWord")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_bitsPerWord")
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$318, DW_AT_location[DW_OP_reg14]
$C$DW$319	.dwtag  DW_TAG_formal_parameter, DW_AT_name("wordsPerFrame")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_wordsPerFrame")
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$319, DW_AT_location[DW_OP_breg20 -7]

;***************************************************************
;* FNAME: _McBSP_setTxDataSize          FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_setTxDataSize:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$320	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$320, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$320, DW_AT_location[DW_OP_breg20 -2]
$C$DW$321	.dwtag  DW_TAG_variable, DW_AT_name("dataFrame")
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_dataFrame")
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$321, DW_AT_location[DW_OP_breg20 -3]
$C$DW$322	.dwtag  DW_TAG_variable, DW_AT_name("bitsPerWord")
	.dwattr $C$DW$322, DW_AT_TI_symbol_name("_bitsPerWord")
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$322, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[4],AR5           ; [CPU_] |274| 
        MOV       *-SP[3],AR4           ; [CPU_] |274| 
        MOVL      *-SP[2],ACC           ; [CPU_] |274| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 281,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |281| 
        BF        $C$L15,NEQ            ; [CPU_] |281| 
        ; branchcc occurs ; [] |281| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 286,column 9,is_stmt
        MOVB      ACC,#9                ; [CPU_] |286| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |286| 
        MOVL      XAR4,ACC              ; [CPU_] |286| 
        AND       AL,*+XAR4[0],#0x801f  ; [CPU_] |286| 
        OR        AL,*-SP[4]            ; [CPU_] |286| 
        MOVZ      AR6,AL                ; [CPU_] |286| 
        MOV       ACC,*-SP[7] << #8     ; [CPU_] |286| 
        OR        AL,AR6                ; [CPU_] |286| 
        MOVZ      AR7,AL                ; [CPU_] |286| 
        MOVB      ACC,#9                ; [CPU_] |286| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |286| 
        MOVL      XAR4,ACC              ; [CPU_] |286| 
        MOV       *+XAR4[0],AR7         ; [CPU_] |286| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 289,column 5,is_stmt
        B         $C$L16,UNC            ; [CPU_] |289| 
        ; branch occurs ; [] |289| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 295,column 9,is_stmt
        MOVB      ACC,#8                ; [CPU_] |295| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |295| 
        MOVL      XAR4,ACC              ; [CPU_] |295| 
        AND       AL,*+XAR4[0],#0x801f  ; [CPU_] |295| 
        OR        AL,*-SP[4]            ; [CPU_] |295| 
        MOVZ      AR6,AL                ; [CPU_] |295| 
        MOV       ACC,*-SP[7] << #8     ; [CPU_] |295| 
        OR        AL,AR6                ; [CPU_] |295| 
        MOVZ      AR7,AL                ; [CPU_] |295| 
        MOVB      ACC,#8                ; [CPU_] |295| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |295| 
        MOVL      XAR4,ACC              ; [CPU_] |295| 
        MOV       *+XAR4[0],AR7         ; [CPU_] |295| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 299,column 1,is_stmt
$C$L16:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$323	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$323, DW_AT_low_pc(0x00)
	.dwattr $C$DW$323, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$315, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$315, DW_AT_TI_end_line(0x12b)
	.dwattr $C$DW$315, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$315

	.sect	".text"
	.clink
	.global	_McBSP_disableRxChannel

$C$DW$324	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableRxChannel")
	.dwattr $C$DW$324, DW_AT_low_pc(_McBSP_disableRxChannel)
	.dwattr $C$DW$324, DW_AT_high_pc(0x00)
	.dwattr $C$DW$324, DW_AT_TI_symbol_name("_McBSP_disableRxChannel")
	.dwattr $C$DW$324, DW_AT_external
	.dwattr $C$DW$324, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$324, DW_AT_TI_begin_line(0x133)
	.dwattr $C$DW$324, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$324, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 310,column 1,is_stmt,address _McBSP_disableRxChannel

	.dwfde $C$DW$CIE, _McBSP_disableRxChannel
$C$DW$325	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$325, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$325, DW_AT_location[DW_OP_reg0]
$C$DW$326	.dwtag  DW_TAG_formal_parameter, DW_AT_name("partition")
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$326, DW_AT_location[DW_OP_reg12]
$C$DW$327	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$327, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _McBSP_disableRxChannel       FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableRxChannel:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$328	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$328, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$328, DW_AT_location[DW_OP_breg20 -2]
$C$DW$329	.dwtag  DW_TAG_variable, DW_AT_name("partition")
	.dwattr $C$DW$329, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$329, DW_AT_location[DW_OP_breg20 -3]
$C$DW$330	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$330, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$330, DW_AT_location[DW_OP_breg20 -4]
$C$DW$331	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$331, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$331, DW_AT_location[DW_OP_breg20 -5]
$C$DW$332	.dwtag  DW_TAG_variable, DW_AT_name("bitOffset")
	.dwattr $C$DW$332, DW_AT_TI_symbol_name("_bitOffset")
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$332, DW_AT_location[DW_OP_breg20 -6]
$C$DW$333	.dwtag  DW_TAG_variable, DW_AT_name("registerOffset")
	.dwattr $C$DW$333, DW_AT_TI_symbol_name("_registerOffset")
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$333, DW_AT_location[DW_OP_breg20 -7]
$C$DW$334	.dwtag  DW_TAG_variable, DW_AT_name("oddBlock")
	.dwattr $C$DW$334, DW_AT_TI_symbol_name("_oddBlock")
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$334, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[4],AR5           ; [CPU_] |310| 
        MOV       *-SP[3],AR4           ; [CPU_] |310| 
        MOVL      *-SP[2],ACC           ; [CPU_] |310| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 325,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |325| 
        LSR       AL,4                  ; [CPU_] |325| 
        MOV       *-SP[5],AL            ; [CPU_] |325| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 330,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |330| 
        MOV       ACC,*-SP[5] << #4     ; [CPU_] |330| 
        SUB       AR6,AL                ; [CPU_] |330| 
        MOV       *-SP[6],AR6           ; [CPU_] |330| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 335,column 5,is_stmt
        CMP       *-SP[3],#512          ; [CPU_] |335| 
        BF        $C$L18,NEQ            ; [CPU_] |335| 
        ; branchcc occurs ; [] |335| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 340,column 9,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |340| 
        CMPB      AL,#32                ; [CPU_] |340| 
        B         $C$L17,HIS            ; [CPU_] |340| 
        ; branchcc occurs ; [] |340| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 345,column 13,is_stmt
        LSR       AL,4                  ; [CPU_] |345| 
        MOV       *-SP[7],AL            ; [CPU_] |345| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 346,column 9,is_stmt
        B         $C$L19,UNC            ; [CPU_] |346| 
        ; branch occurs ; [] |346| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 357,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |357| 
        ANDB      AL,#0x01              ; [CPU_] |357| 
        MOV       *-SP[8],AL            ; [CPU_] |357| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 358,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |358| 
        SUB       AL,*-SP[8]            ; [CPU_] |358| 
        ADDB      AL,#-2                ; [CPU_] |358| 
        LSL       AL,1                  ; [CPU_] |358| 
        ADD       AL,*-SP[8]            ; [CPU_] |358| 
        ADDB      AL,#5                 ; [CPU_] |358| 
        MOV       *-SP[7],AL            ; [CPU_] |358| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 361,column 5,is_stmt
        B         $C$L19,UNC            ; [CPU_] |361| 
        ; branch occurs ; [] |361| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 371,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |371| 
        ANDB      AL,#0x01              ; [CPU_] |371| 
        MOV       *-SP[7],AL            ; [CPU_] |371| 
$C$L19:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 374,column 5,is_stmt
        MOVU      ACC,*-SP[7]           ; [CPU_] |374| 
        MOV       T,*-SP[6]             ; [CPU_] |374| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |374| 
        ADDB      ACC,#14               ; [CPU_] |374| 
        MOVL      XAR4,ACC              ; [CPU_] |374| 
        MOVB      AL,#1                 ; [CPU_] |374| 
        LSL       AL,T                  ; [CPU_] |374| 
        NOT       AL                    ; [CPU_] |374| 
        AND       *+XAR4[0],AL          ; [CPU_] |374| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 375,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$335	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$335, DW_AT_low_pc(0x00)
	.dwattr $C$DW$335, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$324, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$324, DW_AT_TI_end_line(0x177)
	.dwattr $C$DW$324, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$324

	.sect	".text"
	.clink
	.global	_McBSP_enableRxChannel

$C$DW$336	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableRxChannel")
	.dwattr $C$DW$336, DW_AT_low_pc(_McBSP_enableRxChannel)
	.dwattr $C$DW$336, DW_AT_high_pc(0x00)
	.dwattr $C$DW$336, DW_AT_TI_symbol_name("_McBSP_enableRxChannel")
	.dwattr $C$DW$336, DW_AT_external
	.dwattr $C$DW$336, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$336, DW_AT_TI_begin_line(0x17f)
	.dwattr $C$DW$336, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$336, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 386,column 1,is_stmt,address _McBSP_enableRxChannel

	.dwfde $C$DW$CIE, _McBSP_enableRxChannel
$C$DW$337	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$337, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$337, DW_AT_location[DW_OP_reg0]
$C$DW$338	.dwtag  DW_TAG_formal_parameter, DW_AT_name("partition")
	.dwattr $C$DW$338, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$338, DW_AT_location[DW_OP_reg12]
$C$DW$339	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$339, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$339, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _McBSP_enableRxChannel        FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableRxChannel:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$340	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$340, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$340, DW_AT_location[DW_OP_breg20 -2]
$C$DW$341	.dwtag  DW_TAG_variable, DW_AT_name("partition")
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$341, DW_AT_location[DW_OP_breg20 -3]
$C$DW$342	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$342, DW_AT_location[DW_OP_breg20 -4]
$C$DW$343	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$343, DW_AT_location[DW_OP_breg20 -5]
$C$DW$344	.dwtag  DW_TAG_variable, DW_AT_name("bitOffset")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_bitOffset")
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$344, DW_AT_location[DW_OP_breg20 -6]
$C$DW$345	.dwtag  DW_TAG_variable, DW_AT_name("registerOffset")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_registerOffset")
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$345, DW_AT_location[DW_OP_breg20 -7]
$C$DW$346	.dwtag  DW_TAG_variable, DW_AT_name("oddBlock")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_oddBlock")
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$346, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[4],AR5           ; [CPU_] |386| 
        MOV       *-SP[3],AR4           ; [CPU_] |386| 
        MOVL      *-SP[2],ACC           ; [CPU_] |386| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 401,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |401| 
        LSR       AL,4                  ; [CPU_] |401| 
        MOV       *-SP[5],AL            ; [CPU_] |401| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 406,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |406| 
        MOV       ACC,*-SP[5] << #4     ; [CPU_] |406| 
        SUB       AR6,AL                ; [CPU_] |406| 
        MOV       *-SP[6],AR6           ; [CPU_] |406| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 411,column 5,is_stmt
        CMP       *-SP[3],#512          ; [CPU_] |411| 
        BF        $C$L21,NEQ            ; [CPU_] |411| 
        ; branchcc occurs ; [] |411| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 416,column 9,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |416| 
        CMPB      AL,#32                ; [CPU_] |416| 
        B         $C$L20,HIS            ; [CPU_] |416| 
        ; branchcc occurs ; [] |416| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 421,column 13,is_stmt
        LSR       AL,4                  ; [CPU_] |421| 
        MOV       *-SP[7],AL            ; [CPU_] |421| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 422,column 9,is_stmt
        B         $C$L22,UNC            ; [CPU_] |422| 
        ; branch occurs ; [] |422| 
$C$L20:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 433,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |433| 
        ANDB      AL,#0x01              ; [CPU_] |433| 
        MOV       *-SP[8],AL            ; [CPU_] |433| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 434,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |434| 
        SUB       AL,*-SP[8]            ; [CPU_] |434| 
        ADDB      AL,#-2                ; [CPU_] |434| 
        LSL       AL,1                  ; [CPU_] |434| 
        ADD       AL,*-SP[8]            ; [CPU_] |434| 
        ADDB      AL,#5                 ; [CPU_] |434| 
        MOV       *-SP[7],AL            ; [CPU_] |434| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 437,column 5,is_stmt
        B         $C$L22,UNC            ; [CPU_] |437| 
        ; branch occurs ; [] |437| 
$C$L21:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 447,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |447| 
        ANDB      AL,#0x01              ; [CPU_] |447| 
        MOV       *-SP[7],AL            ; [CPU_] |447| 
$C$L22:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 450,column 5,is_stmt
        MOVU      ACC,*-SP[7]           ; [CPU_] |450| 
        MOV       T,*-SP[6]             ; [CPU_] |450| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |450| 
        ADDB      ACC,#14               ; [CPU_] |450| 
        MOVL      XAR4,ACC              ; [CPU_] |450| 
        MOVB      AL,#1                 ; [CPU_] |450| 
        LSL       AL,T                  ; [CPU_] |450| 
        OR        *+XAR4[0],AL          ; [CPU_] |450| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 451,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$347	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$347, DW_AT_low_pc(0x00)
	.dwattr $C$DW$347, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$336, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$336, DW_AT_TI_end_line(0x1c3)
	.dwattr $C$DW$336, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$336

	.sect	".text"
	.clink
	.global	_McBSP_disableTxChannel

$C$DW$348	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_disableTxChannel")
	.dwattr $C$DW$348, DW_AT_low_pc(_McBSP_disableTxChannel)
	.dwattr $C$DW$348, DW_AT_high_pc(0x00)
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_McBSP_disableTxChannel")
	.dwattr $C$DW$348, DW_AT_external
	.dwattr $C$DW$348, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$348, DW_AT_TI_begin_line(0x1cb)
	.dwattr $C$DW$348, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$348, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 462,column 1,is_stmt,address _McBSP_disableTxChannel

	.dwfde $C$DW$CIE, _McBSP_disableTxChannel
$C$DW$349	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$349, DW_AT_location[DW_OP_reg0]
$C$DW$350	.dwtag  DW_TAG_formal_parameter, DW_AT_name("partition")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$350, DW_AT_location[DW_OP_reg12]
$C$DW$351	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$351, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _McBSP_disableTxChannel       FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_McBSP_disableTxChannel:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$352	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$352, DW_AT_location[DW_OP_breg20 -2]
$C$DW$353	.dwtag  DW_TAG_variable, DW_AT_name("partition")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$353, DW_AT_location[DW_OP_breg20 -3]
$C$DW$354	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$354, DW_AT_location[DW_OP_breg20 -4]
$C$DW$355	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$355, DW_AT_location[DW_OP_breg20 -5]
$C$DW$356	.dwtag  DW_TAG_variable, DW_AT_name("bitOffset")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_bitOffset")
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$356, DW_AT_location[DW_OP_breg20 -6]
$C$DW$357	.dwtag  DW_TAG_variable, DW_AT_name("registerOffset")
	.dwattr $C$DW$357, DW_AT_TI_symbol_name("_registerOffset")
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$357, DW_AT_location[DW_OP_breg20 -7]
$C$DW$358	.dwtag  DW_TAG_variable, DW_AT_name("oddBlock")
	.dwattr $C$DW$358, DW_AT_TI_symbol_name("_oddBlock")
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$358, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[4],AR5           ; [CPU_] |462| 
        MOV       *-SP[3],AR4           ; [CPU_] |462| 
        MOVL      *-SP[2],ACC           ; [CPU_] |462| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 477,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |477| 
        LSR       AL,4                  ; [CPU_] |477| 
        MOV       *-SP[5],AL            ; [CPU_] |477| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 482,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |482| 
        MOV       ACC,*-SP[5] << #4     ; [CPU_] |482| 
        SUB       AR6,AL                ; [CPU_] |482| 
        MOV       *-SP[6],AR6           ; [CPU_] |482| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 487,column 5,is_stmt
        CMP       *-SP[3],#512          ; [CPU_] |487| 
        BF        $C$L24,NEQ            ; [CPU_] |487| 
        ; branchcc occurs ; [] |487| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 492,column 9,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |492| 
        CMPB      AL,#32                ; [CPU_] |492| 
        B         $C$L23,HIS            ; [CPU_] |492| 
        ; branchcc occurs ; [] |492| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 497,column 13,is_stmt
        LSR       AL,4                  ; [CPU_] |497| 
        MOV       *-SP[7],AL            ; [CPU_] |497| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 498,column 9,is_stmt
        B         $C$L25,UNC            ; [CPU_] |498| 
        ; branch occurs ; [] |498| 
$C$L23:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 509,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |509| 
        ANDB      AL,#0x01              ; [CPU_] |509| 
        MOV       *-SP[8],AL            ; [CPU_] |509| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 510,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |510| 
        SUB       AL,*-SP[8]            ; [CPU_] |510| 
        ADDB      AL,#-2                ; [CPU_] |510| 
        LSL       AL,1                  ; [CPU_] |510| 
        ADD       AL,*-SP[8]            ; [CPU_] |510| 
        ADDB      AL,#5                 ; [CPU_] |510| 
        MOV       *-SP[7],AL            ; [CPU_] |510| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 513,column 5,is_stmt
        B         $C$L25,UNC            ; [CPU_] |513| 
        ; branch occurs ; [] |513| 
$C$L24:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 523,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |523| 
        ANDB      AL,#0x01              ; [CPU_] |523| 
        MOV       *-SP[7],AL            ; [CPU_] |523| 
$C$L25:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 526,column 5,is_stmt
        MOVU      ACC,*-SP[7]           ; [CPU_] |526| 
        MOV       T,*-SP[6]             ; [CPU_] |526| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |526| 
        ADDB      ACC,#16               ; [CPU_] |526| 
        MOVL      XAR4,ACC              ; [CPU_] |526| 
        MOVB      AL,#1                 ; [CPU_] |526| 
        LSL       AL,T                  ; [CPU_] |526| 
        NOT       AL                    ; [CPU_] |526| 
        AND       *+XAR4[0],AL          ; [CPU_] |526| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 527,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$359	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$359, DW_AT_low_pc(0x00)
	.dwattr $C$DW$359, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$348, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$348, DW_AT_TI_end_line(0x20f)
	.dwattr $C$DW$348, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$348

	.sect	".text"
	.clink
	.global	_McBSP_enableTxChannel

$C$DW$360	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_enableTxChannel")
	.dwattr $C$DW$360, DW_AT_low_pc(_McBSP_enableTxChannel)
	.dwattr $C$DW$360, DW_AT_high_pc(0x00)
	.dwattr $C$DW$360, DW_AT_TI_symbol_name("_McBSP_enableTxChannel")
	.dwattr $C$DW$360, DW_AT_external
	.dwattr $C$DW$360, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$360, DW_AT_TI_begin_line(0x216)
	.dwattr $C$DW$360, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$360, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 537,column 1,is_stmt,address _McBSP_enableTxChannel

	.dwfde $C$DW$CIE, _McBSP_enableTxChannel
$C$DW$361	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$361, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$361, DW_AT_location[DW_OP_reg0]
$C$DW$362	.dwtag  DW_TAG_formal_parameter, DW_AT_name("partition")
	.dwattr $C$DW$362, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$362, DW_AT_location[DW_OP_reg12]
$C$DW$363	.dwtag  DW_TAG_formal_parameter, DW_AT_name("channel")
	.dwattr $C$DW$363, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$363, DW_AT_location[DW_OP_reg14]

;***************************************************************
;* FNAME: _McBSP_enableTxChannel        FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_McBSP_enableTxChannel:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$364	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$364, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$364, DW_AT_location[DW_OP_breg20 -2]
$C$DW$365	.dwtag  DW_TAG_variable, DW_AT_name("partition")
	.dwattr $C$DW$365, DW_AT_TI_symbol_name("_partition")
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$365, DW_AT_location[DW_OP_breg20 -3]
$C$DW$366	.dwtag  DW_TAG_variable, DW_AT_name("channel")
	.dwattr $C$DW$366, DW_AT_TI_symbol_name("_channel")
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$366, DW_AT_location[DW_OP_breg20 -4]
$C$DW$367	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$367, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$367, DW_AT_location[DW_OP_breg20 -5]
$C$DW$368	.dwtag  DW_TAG_variable, DW_AT_name("bitOffset")
	.dwattr $C$DW$368, DW_AT_TI_symbol_name("_bitOffset")
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$368, DW_AT_location[DW_OP_breg20 -6]
$C$DW$369	.dwtag  DW_TAG_variable, DW_AT_name("registerOffset")
	.dwattr $C$DW$369, DW_AT_TI_symbol_name("_registerOffset")
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$369, DW_AT_location[DW_OP_breg20 -7]
$C$DW$370	.dwtag  DW_TAG_variable, DW_AT_name("oddBlock")
	.dwattr $C$DW$370, DW_AT_TI_symbol_name("_oddBlock")
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$370, DW_AT_location[DW_OP_breg20 -8]
        MOV       *-SP[4],AR5           ; [CPU_] |537| 
        MOV       *-SP[3],AR4           ; [CPU_] |537| 
        MOVL      *-SP[2],ACC           ; [CPU_] |537| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 552,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |552| 
        LSR       AL,4                  ; [CPU_] |552| 
        MOV       *-SP[5],AL            ; [CPU_] |552| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 557,column 5,is_stmt
        MOVZ      AR6,*-SP[4]           ; [CPU_] |557| 
        MOV       ACC,*-SP[5] << #4     ; [CPU_] |557| 
        SUB       AR6,AL                ; [CPU_] |557| 
        MOV       *-SP[6],AR6           ; [CPU_] |557| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 562,column 5,is_stmt
        CMP       *-SP[3],#512          ; [CPU_] |562| 
        BF        $C$L27,NEQ            ; [CPU_] |562| 
        ; branchcc occurs ; [] |562| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 567,column 9,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |567| 
        CMPB      AL,#32                ; [CPU_] |567| 
        B         $C$L26,HIS            ; [CPU_] |567| 
        ; branchcc occurs ; [] |567| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 572,column 13,is_stmt
        LSR       AL,4                  ; [CPU_] |572| 
        MOV       *-SP[7],AL            ; [CPU_] |572| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 573,column 9,is_stmt
        B         $C$L28,UNC            ; [CPU_] |573| 
        ; branch occurs ; [] |573| 
$C$L26:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 584,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |584| 
        ANDB      AL,#0x01              ; [CPU_] |584| 
        MOV       *-SP[8],AL            ; [CPU_] |584| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 585,column 13,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |585| 
        SUB       AL,*-SP[8]            ; [CPU_] |585| 
        ADDB      AL,#-2                ; [CPU_] |585| 
        LSL       AL,1                  ; [CPU_] |585| 
        ADD       AL,*-SP[8]            ; [CPU_] |585| 
        ADDB      AL,#5                 ; [CPU_] |585| 
        MOV       *-SP[7],AL            ; [CPU_] |585| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 588,column 5,is_stmt
        B         $C$L28,UNC            ; [CPU_] |588| 
        ; branch occurs ; [] |588| 
$C$L27:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 598,column 9,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |598| 
        ANDB      AL,#0x01              ; [CPU_] |598| 
        MOV       *-SP[7],AL            ; [CPU_] |598| 
$C$L28:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 601,column 5,is_stmt
        MOVU      ACC,*-SP[7]           ; [CPU_] |601| 
        MOV       T,*-SP[6]             ; [CPU_] |601| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |601| 
        ADDB      ACC,#16               ; [CPU_] |601| 
        MOVL      XAR4,ACC              ; [CPU_] |601| 
        MOVB      AL,#1                 ; [CPU_] |601| 
        LSL       AL,T                  ; [CPU_] |601| 
        OR        *+XAR4[0],AL          ; [CPU_] |601| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 602,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$371	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$371, DW_AT_low_pc(0x00)
	.dwattr $C$DW$371, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$360, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$360, DW_AT_TI_end_line(0x25a)
	.dwattr $C$DW$360, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$360

	.sect	".text"
	.clink
	.global	_McBSP_configureTxClock

$C$DW$372	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureTxClock")
	.dwattr $C$DW$372, DW_AT_low_pc(_McBSP_configureTxClock)
	.dwattr $C$DW$372, DW_AT_high_pc(0x00)
	.dwattr $C$DW$372, DW_AT_TI_symbol_name("_McBSP_configureTxClock")
	.dwattr $C$DW$372, DW_AT_external
	.dwattr $C$DW$372, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$372, DW_AT_TI_begin_line(0x262)
	.dwattr $C$DW$372, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$372, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 611,column 1,is_stmt,address _McBSP_configureTxClock

	.dwfde $C$DW$CIE, _McBSP_configureTxClock
$C$DW$373	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$373, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$373, DW_AT_location[DW_OP_reg0]
$C$DW$374	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrClockParams")
	.dwattr $C$DW$374, DW_AT_TI_symbol_name("_ptrClockParams")
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$374, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureTxClock       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureTxClock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$375	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$375, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$375, DW_AT_location[DW_OP_breg20 -2]
$C$DW$376	.dwtag  DW_TAG_variable, DW_AT_name("ptrClockParams")
	.dwattr $C$DW$376, DW_AT_TI_symbol_name("_ptrClockParams")
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$376, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |611| 
        MOVL      *-SP[2],ACC           ; [CPU_] |611| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 620,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |620| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |620| 
        MOVZ      AR4,*+XAR4[2]         ; [CPU_] |620| 
$C$DW$377	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$377, DW_AT_low_pc(0x00)
	.dwattr $C$DW$377, DW_AT_name("_McBSP_setTxClockSource")
	.dwattr $C$DW$377, DW_AT_TI_call
        LCR       #_McBSP_setTxClockSource ; [CPU_] |620| 
        ; call occurs [#_McBSP_setTxClockSource] ; [] |620| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 626,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |626| 
        CMP       *+XAR4[2],#512        ; [CPU_] |626| 
        BF        $C$L31,NEQ            ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 632,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |632| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |632| 
        MOVZ      AR4,*+XAR4[5]         ; [CPU_] |632| 
$C$DW$378	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$378, DW_AT_low_pc(0x00)
	.dwattr $C$DW$378, DW_AT_name("_McBSP_setTxSRGClockSource")
	.dwattr $C$DW$378, DW_AT_TI_call
        LCR       #_McBSP_setTxSRGClockSource ; [CPU_] |632| 
        ; call occurs [#_McBSP_setTxSRGClockSource] ; [] |632| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 639,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |639| 
        MOV       AL,*+XAR4[5]          ; [CPU_] |639| 
        CMPB      AL,#2                 ; [CPU_] |639| 
        BF        $C$L30,NEQ            ; [CPU_] |639| 
        ; branchcc occurs ; [] |639| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 645,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |645| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |645| 
        MOVZ      AR4,*+XAR4[7]         ; [CPU_] |645| 
$C$DW$379	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$379, DW_AT_low_pc(0x00)
	.dwattr $C$DW$379, DW_AT_name("_McBSP_setRxClockPolarity")
	.dwattr $C$DW$379, DW_AT_TI_call
        LCR       #_McBSP_setRxClockPolarity ; [CPU_] |645| 
        ; call occurs [#_McBSP_setRxClockPolarity] ; [] |645| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 652,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |652| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |652| 
        BF        $C$L29,EQ             ; [CPU_] |652| 
        ; branchcc occurs ; [] |652| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 654,column 17,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |654| 
$C$DW$380	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$380, DW_AT_low_pc(0x00)
	.dwattr $C$DW$380, DW_AT_name("_McBSP_enableSRGSyncFSR")
	.dwattr $C$DW$380, DW_AT_TI_call
        LCR       #_McBSP_enableSRGSyncFSR ; [CPU_] |654| 
        ; call occurs [#_McBSP_enableSRGSyncFSR] ; [] |654| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 655,column 13,is_stmt
        B         $C$L30,UNC            ; [CPU_] |655| 
        ; branch occurs ; [] |655| 
$C$L29:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 658,column 17,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |658| 
$C$DW$381	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$381, DW_AT_low_pc(0x00)
	.dwattr $C$DW$381, DW_AT_name("_McBSP_disableSRGSyncFSR")
	.dwattr $C$DW$381, DW_AT_TI_call
        LCR       #_McBSP_disableSRGSyncFSR ; [CPU_] |658| 
        ; call occurs [#_McBSP_disableSRGSyncFSR] ; [] |658| 
$C$L30:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 665,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |665| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |665| 
        MOVZ      AR4,*+XAR4[1]         ; [CPU_] |665| 
$C$DW$382	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$382, DW_AT_low_pc(0x00)
	.dwattr $C$DW$382, DW_AT_name("_McBSP_setSRGDataClockDivider")
	.dwattr $C$DW$382, DW_AT_TI_call
        LCR       #_McBSP_setSRGDataClockDivider ; [CPU_] |665| 
        ; call occurs [#_McBSP_setSRGDataClockDivider] ; [] |665| 
$C$L31:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 673,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |673| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |673| 
        MOVZ      AR4,*+XAR4[6]         ; [CPU_] |673| 
$C$DW$383	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$383, DW_AT_low_pc(0x00)
	.dwattr $C$DW$383, DW_AT_name("_McBSP_setTxClockPolarity")
	.dwattr $C$DW$383, DW_AT_TI_call
        LCR       #_McBSP_setTxClockPolarity ; [CPU_] |673| 
        ; call occurs [#_McBSP_setTxClockPolarity] ; [] |673| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 675,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$384	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$384, DW_AT_low_pc(0x00)
	.dwattr $C$DW$384, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$372, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$372, DW_AT_TI_end_line(0x2a3)
	.dwattr $C$DW$372, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$372

	.sect	".text"
	.clink
	.global	_McBSP_configureRxClock

$C$DW$385	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureRxClock")
	.dwattr $C$DW$385, DW_AT_low_pc(_McBSP_configureRxClock)
	.dwattr $C$DW$385, DW_AT_high_pc(0x00)
	.dwattr $C$DW$385, DW_AT_TI_symbol_name("_McBSP_configureRxClock")
	.dwattr $C$DW$385, DW_AT_external
	.dwattr $C$DW$385, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$385, DW_AT_TI_begin_line(0x2ab)
	.dwattr $C$DW$385, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$385, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 684,column 1,is_stmt,address _McBSP_configureRxClock

	.dwfde $C$DW$CIE, _McBSP_configureRxClock
$C$DW$386	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$386, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$386, DW_AT_location[DW_OP_reg0]
$C$DW$387	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrClockParams")
	.dwattr $C$DW$387, DW_AT_TI_symbol_name("_ptrClockParams")
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$387, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureRxClock       FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureRxClock:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$388	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$388, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$388, DW_AT_location[DW_OP_breg20 -2]
$C$DW$389	.dwtag  DW_TAG_variable, DW_AT_name("ptrClockParams")
	.dwattr $C$DW$389, DW_AT_TI_symbol_name("_ptrClockParams")
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$389, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |684| 
        MOVL      *-SP[2],ACC           ; [CPU_] |684| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 693,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |693| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |693| 
        MOVZ      AR4,*+XAR4[3]         ; [CPU_] |693| 
$C$DW$390	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$390, DW_AT_low_pc(0x00)
	.dwattr $C$DW$390, DW_AT_name("_McBSP_setRxClockSource")
	.dwattr $C$DW$390, DW_AT_TI_call
        LCR       #_McBSP_setRxClockSource ; [CPU_] |693| 
        ; call occurs [#_McBSP_setRxClockSource] ; [] |693| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 699,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |699| 
        CMP       *+XAR4[3],#256        ; [CPU_] |699| 
        BF        $C$L33,NEQ            ; [CPU_] |699| 
        ; branchcc occurs ; [] |699| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 705,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |705| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |705| 
        MOVZ      AR4,*+XAR4[4]         ; [CPU_] |705| 
$C$DW$391	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$391, DW_AT_low_pc(0x00)
	.dwattr $C$DW$391, DW_AT_name("_McBSP_setRxSRGClockSource")
	.dwattr $C$DW$391, DW_AT_TI_call
        LCR       #_McBSP_setRxSRGClockSource ; [CPU_] |705| 
        ; call occurs [#_McBSP_setRxSRGClockSource] ; [] |705| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 713,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |713| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |713| 
        CMPB      AL,#3                 ; [CPU_] |713| 
        BF        $C$L32,NEQ            ; [CPU_] |713| 
        ; branchcc occurs ; [] |713| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 719,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |719| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |719| 
        MOVZ      AR4,*+XAR4[6]         ; [CPU_] |719| 
$C$DW$392	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$392, DW_AT_low_pc(0x00)
	.dwattr $C$DW$392, DW_AT_name("_McBSP_setTxClockPolarity")
	.dwattr $C$DW$392, DW_AT_TI_call
        LCR       #_McBSP_setTxClockPolarity ; [CPU_] |719| 
        ; call occurs [#_McBSP_setTxClockPolarity] ; [] |719| 
$C$L32:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 726,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |726| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |726| 
        MOVZ      AR4,*+XAR4[1]         ; [CPU_] |726| 
$C$DW$393	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$393, DW_AT_low_pc(0x00)
	.dwattr $C$DW$393, DW_AT_name("_McBSP_setSRGDataClockDivider")
	.dwattr $C$DW$393, DW_AT_TI_call
        LCR       #_McBSP_setSRGDataClockDivider ; [CPU_] |726| 
        ; call occurs [#_McBSP_setSRGDataClockDivider] ; [] |726| 
$C$L33:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 734,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |734| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |734| 
        MOVZ      AR4,*+XAR4[7]         ; [CPU_] |734| 
$C$DW$394	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$394, DW_AT_low_pc(0x00)
	.dwattr $C$DW$394, DW_AT_name("_McBSP_setRxClockPolarity")
	.dwattr $C$DW$394, DW_AT_TI_call
        LCR       #_McBSP_setRxClockPolarity ; [CPU_] |734| 
        ; call occurs [#_McBSP_setRxClockPolarity] ; [] |734| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 736,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$395	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$395, DW_AT_low_pc(0x00)
	.dwattr $C$DW$395, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$385, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$385, DW_AT_TI_end_line(0x2e0)
	.dwattr $C$DW$385, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$385

	.sect	".text"
	.clink
	.global	_McBSP_configureTxFrameSync

$C$DW$396	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureTxFrameSync")
	.dwattr $C$DW$396, DW_AT_low_pc(_McBSP_configureTxFrameSync)
	.dwattr $C$DW$396, DW_AT_high_pc(0x00)
	.dwattr $C$DW$396, DW_AT_TI_symbol_name("_McBSP_configureTxFrameSync")
	.dwattr $C$DW$396, DW_AT_external
	.dwattr $C$DW$396, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$396, DW_AT_TI_begin_line(0x2e8)
	.dwattr $C$DW$396, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$396, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 746,column 1,is_stmt,address _McBSP_configureTxFrameSync

	.dwfde $C$DW$CIE, _McBSP_configureTxFrameSync
$C$DW$397	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$397, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$397, DW_AT_location[DW_OP_reg0]
$C$DW$398	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrFsyncParams")
	.dwattr $C$DW$398, DW_AT_TI_symbol_name("_ptrFsyncParams")
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$398, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureTxFrameSync   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureTxFrameSync:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$399	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$399, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$399, DW_AT_location[DW_OP_breg20 -2]
$C$DW$400	.dwtag  DW_TAG_variable, DW_AT_name("ptrFsyncParams")
	.dwattr $C$DW$400, DW_AT_TI_symbol_name("_ptrFsyncParams")
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$400, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |746| 
        MOVL      *-SP[2],ACC           ; [CPU_] |746| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 755,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |755| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |755| 
        MOVZ      AR4,*+XAR4[4]         ; [CPU_] |755| 
$C$DW$401	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$401, DW_AT_low_pc(0x00)
	.dwattr $C$DW$401, DW_AT_name("_McBSP_setTxFrameSyncSource")
	.dwattr $C$DW$401, DW_AT_TI_call
        LCR       #_McBSP_setTxFrameSyncSource ; [CPU_] |755| 
        ; call occurs [#_McBSP_setTxFrameSyncSource] ; [] |755| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 761,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |761| 
        CMP       *+XAR4[4],#2048       ; [CPU_] |761| 
        BF        $C$L34,NEQ            ; [CPU_] |761| 
        ; branchcc occurs ; [] |761| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 767,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |767| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |767| 
        MOVZ      AR4,*+XAR4[5]         ; [CPU_] |767| 
$C$DW$402	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$402, DW_AT_low_pc(0x00)
	.dwattr $C$DW$402, DW_AT_name("_McBSP_setTxInternalFrameSyncSource")
	.dwattr $C$DW$402, DW_AT_TI_call
        LCR       #_McBSP_setTxInternalFrameSyncSource ; [CPU_] |767| 
        ; call occurs [#_McBSP_setTxInternalFrameSyncSource] ; [] |767| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 774,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |774| 
        CMP       *+XAR4[5],#4096       ; [CPU_] |774| 
        BF        $C$L34,NEQ            ; [CPU_] |774| 
        ; branchcc occurs ; [] |774| 
        MOVL      XAR4,*-SP[4]          ; [CPU_] |774| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |774| 
        BF        $C$L34,NEQ            ; [CPU_] |774| 
        ; branchcc occurs ; [] |774| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 781,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |781| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |781| 
        MOVZ      AR4,*+XAR4[2]         ; [CPU_] |781| 
$C$DW$403	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$403, DW_AT_low_pc(0x00)
	.dwattr $C$DW$403, DW_AT_name("_McBSP_setFrameSyncPulsePeriod")
	.dwattr $C$DW$403, DW_AT_TI_call
        LCR       #_McBSP_setFrameSyncPulsePeriod ; [CPU_] |781| 
        ; call occurs [#_McBSP_setFrameSyncPulsePeriod] ; [] |781| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 783,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |783| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |783| 
        MOVZ      AR4,*+XAR4[3]         ; [CPU_] |783| 
$C$DW$404	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$404, DW_AT_low_pc(0x00)
	.dwattr $C$DW$404, DW_AT_name("_McBSP_setFrameSyncPulseWidthDivider")
	.dwattr $C$DW$404, DW_AT_TI_call
        LCR       #_McBSP_setFrameSyncPulseWidthDivider ; [CPU_] |783| 
        ; call occurs [#_McBSP_setFrameSyncPulseWidthDivider] ; [] |783| 
$C$L34:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 791,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |791| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |791| 
        MOVZ      AR4,*+XAR4[6]         ; [CPU_] |791| 
$C$DW$405	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$405, DW_AT_low_pc(0x00)
	.dwattr $C$DW$405, DW_AT_name("_McBSP_setTxFrameSyncPolarity")
	.dwattr $C$DW$405, DW_AT_TI_call
        LCR       #_McBSP_setTxFrameSyncPolarity ; [CPU_] |791| 
        ; call occurs [#_McBSP_setTxFrameSyncPolarity] ; [] |791| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 797,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |797| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |797| 
        BF        $C$L35,EQ             ; [CPU_] |797| 
        ; branchcc occurs ; [] |797| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 799,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |799| 
$C$DW$406	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$406, DW_AT_low_pc(0x00)
	.dwattr $C$DW$406, DW_AT_name("_McBSP_enableTxFrameSyncErrorDetection")
	.dwattr $C$DW$406, DW_AT_TI_call
        LCR       #_McBSP_enableTxFrameSyncErrorDetection ; [CPU_] |799| 
        ; call occurs [#_McBSP_enableTxFrameSyncErrorDetection] ; [] |799| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 800,column 5,is_stmt
        B         $C$L36,UNC            ; [CPU_] |800| 
        ; branch occurs ; [] |800| 
$C$L35:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 803,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |803| 
$C$DW$407	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$407, DW_AT_low_pc(0x00)
	.dwattr $C$DW$407, DW_AT_name("_McBSP_disableTxFrameSyncErrorDetection")
	.dwattr $C$DW$407, DW_AT_TI_call
        LCR       #_McBSP_disableTxFrameSyncErrorDetection ; [CPU_] |803| 
        ; call occurs [#_McBSP_disableTxFrameSyncErrorDetection] ; [] |803| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 805,column 1,is_stmt
$C$L36:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$408	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$408, DW_AT_low_pc(0x00)
	.dwattr $C$DW$408, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$396, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$396, DW_AT_TI_end_line(0x325)
	.dwattr $C$DW$396, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$396

	.sect	".text"
	.clink
	.global	_McBSP_configureRxFrameSync

$C$DW$409	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureRxFrameSync")
	.dwattr $C$DW$409, DW_AT_low_pc(_McBSP_configureRxFrameSync)
	.dwattr $C$DW$409, DW_AT_high_pc(0x00)
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_McBSP_configureRxFrameSync")
	.dwattr $C$DW$409, DW_AT_external
	.dwattr $C$DW$409, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$409, DW_AT_TI_begin_line(0x32d)
	.dwattr $C$DW$409, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$409, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 815,column 1,is_stmt,address _McBSP_configureRxFrameSync

	.dwfde $C$DW$CIE, _McBSP_configureRxFrameSync
$C$DW$410	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$410, DW_AT_location[DW_OP_reg0]
$C$DW$411	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrFsyncParams")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_ptrFsyncParams")
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$411, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureRxFrameSync   FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureRxFrameSync:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$412	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$412, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$412, DW_AT_location[DW_OP_breg20 -2]
$C$DW$413	.dwtag  DW_TAG_variable, DW_AT_name("ptrFsyncParams")
	.dwattr $C$DW$413, DW_AT_TI_symbol_name("_ptrFsyncParams")
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$413, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |815| 
        MOVL      *-SP[2],ACC           ; [CPU_] |815| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 824,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |824| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |824| 
        MOVZ      AR4,*+XAR4[4]         ; [CPU_] |824| 
$C$DW$414	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$414, DW_AT_low_pc(0x00)
	.dwattr $C$DW$414, DW_AT_name("_McBSP_setRxFrameSyncSource")
	.dwattr $C$DW$414, DW_AT_TI_call
        LCR       #_McBSP_setRxFrameSyncSource ; [CPU_] |824| 
        ; call occurs [#_McBSP_setRxFrameSyncSource] ; [] |824| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 830,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |830| 
        CMP       *+XAR4[4],#1024       ; [CPU_] |830| 
        BF        $C$L37,NEQ            ; [CPU_] |830| 
        ; branchcc occurs ; [] |830| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 836,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |836| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |836| 
        BF        $C$L37,NEQ            ; [CPU_] |836| 
        ; branchcc occurs ; [] |836| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 841,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |841| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |841| 
        MOVZ      AR4,*+XAR4[2]         ; [CPU_] |841| 
$C$DW$415	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$415, DW_AT_low_pc(0x00)
	.dwattr $C$DW$415, DW_AT_name("_McBSP_setFrameSyncPulsePeriod")
	.dwattr $C$DW$415, DW_AT_TI_call
        LCR       #_McBSP_setFrameSyncPulsePeriod ; [CPU_] |841| 
        ; call occurs [#_McBSP_setFrameSyncPulsePeriod] ; [] |841| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 843,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |843| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |843| 
        MOVZ      AR4,*+XAR4[3]         ; [CPU_] |843| 
$C$DW$416	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$416, DW_AT_low_pc(0x00)
	.dwattr $C$DW$416, DW_AT_name("_McBSP_setFrameSyncPulseWidthDivider")
	.dwattr $C$DW$416, DW_AT_TI_call
        LCR       #_McBSP_setFrameSyncPulseWidthDivider ; [CPU_] |843| 
        ; call occurs [#_McBSP_setFrameSyncPulseWidthDivider] ; [] |843| 
$C$L37:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 851,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |851| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |851| 
        MOVZ      AR4,*+XAR4[5]         ; [CPU_] |851| 
$C$DW$417	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$417, DW_AT_low_pc(0x00)
	.dwattr $C$DW$417, DW_AT_name("_McBSP_setRxFrameSyncPolarity")
	.dwattr $C$DW$417, DW_AT_TI_call
        LCR       #_McBSP_setRxFrameSyncPolarity ; [CPU_] |851| 
        ; call occurs [#_McBSP_setRxFrameSyncPolarity] ; [] |851| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 857,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |857| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |857| 
        BF        $C$L38,EQ             ; [CPU_] |857| 
        ; branchcc occurs ; [] |857| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 859,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |859| 
$C$DW$418	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$418, DW_AT_low_pc(0x00)
	.dwattr $C$DW$418, DW_AT_name("_McBSP_enableRxFrameSyncErrorDetection")
	.dwattr $C$DW$418, DW_AT_TI_call
        LCR       #_McBSP_enableRxFrameSyncErrorDetection ; [CPU_] |859| 
        ; call occurs [#_McBSP_enableRxFrameSyncErrorDetection] ; [] |859| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 860,column 5,is_stmt
        B         $C$L39,UNC            ; [CPU_] |860| 
        ; branch occurs ; [] |860| 
$C$L38:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 863,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |863| 
$C$DW$419	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$419, DW_AT_low_pc(0x00)
	.dwattr $C$DW$419, DW_AT_name("_McBSP_disableTxFrameSyncErrorDetection")
	.dwattr $C$DW$419, DW_AT_TI_call
        LCR       #_McBSP_disableTxFrameSyncErrorDetection ; [CPU_] |863| 
        ; call occurs [#_McBSP_disableTxFrameSyncErrorDetection] ; [] |863| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 865,column 1,is_stmt
$C$L39:    
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$420	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$420, DW_AT_low_pc(0x00)
	.dwattr $C$DW$420, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$409, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$409, DW_AT_TI_end_line(0x361)
	.dwattr $C$DW$409, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$409

	.sect	".text"
	.clink
	.global	_McBSP_configureTxDataFormat

$C$DW$421	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureTxDataFormat")
	.dwattr $C$DW$421, DW_AT_low_pc(_McBSP_configureTxDataFormat)
	.dwattr $C$DW$421, DW_AT_high_pc(0x00)
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_McBSP_configureTxDataFormat")
	.dwattr $C$DW$421, DW_AT_external
	.dwattr $C$DW$421, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$421, DW_AT_TI_begin_line(0x369)
	.dwattr $C$DW$421, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$421, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 875,column 1,is_stmt,address _McBSP_configureTxDataFormat

	.dwfde $C$DW$CIE, _McBSP_configureTxDataFormat
$C$DW$422	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$422, DW_AT_location[DW_OP_reg0]
$C$DW$423	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrDataParams")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_ptrDataParams")
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$423, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureTxDataFormat  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureTxDataFormat:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$424	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$424, DW_AT_location[DW_OP_breg20 -4]
$C$DW$425	.dwtag  DW_TAG_variable, DW_AT_name("ptrDataParams")
	.dwattr $C$DW$425, DW_AT_TI_symbol_name("_ptrDataParams")
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$425, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[6],XAR4          ; [CPU_] |875| 
        MOVL      *-SP[4],ACC           ; [CPU_] |875| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 884,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |884| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |884| 
        BF        $C$L40,EQ             ; [CPU_] |884| 
        ; branchcc occurs ; [] |884| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 886,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |886| 
$C$DW$426	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$426, DW_AT_low_pc(0x00)
	.dwattr $C$DW$426, DW_AT_name("_McBSP_enableLoopback")
	.dwattr $C$DW$426, DW_AT_TI_call
        LCR       #_McBSP_enableLoopback ; [CPU_] |886| 
        ; call occurs [#_McBSP_enableLoopback] ; [] |886| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 887,column 5,is_stmt
        B         $C$L41,UNC            ; [CPU_] |887| 
        ; branch occurs ; [] |887| 
$C$L40:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 890,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |890| 
$C$DW$427	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$427, DW_AT_low_pc(0x00)
	.dwattr $C$DW$427, DW_AT_name("_McBSP_disableLoopback")
	.dwattr $C$DW$427, DW_AT_TI_call
        LCR       #_McBSP_disableLoopback ; [CPU_] |890| 
        ; call occurs [#_McBSP_disableLoopback] ; [] |890| 
$C$L41:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 896,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |896| 
        MOVB      XAR4,#0               ; [CPU_] |896| 
$C$DW$428	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$428, DW_AT_low_pc(0x00)
	.dwattr $C$DW$428, DW_AT_name("_McBSP_setClockStopMode")
	.dwattr $C$DW$428, DW_AT_TI_call
        LCR       #_McBSP_setClockStopMode ; [CPU_] |896| 
        ; call occurs [#_McBSP_setClockStopMode] ; [] |896| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 901,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |901| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |901| 
        MOV       *-SP[1],AL            ; [CPU_] |901| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |901| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |901| 
        MOVZ      AR5,*+XAR4[6]         ; [CPU_] |901| 
        MOVB      XAR4,#0               ; [CPU_] |901| 
$C$DW$429	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$429, DW_AT_low_pc(0x00)
	.dwattr $C$DW$429, DW_AT_name("_McBSP_setTxDataSize")
	.dwattr $C$DW$429, DW_AT_TI_call
        LCR       #_McBSP_setTxDataSize ; [CPU_] |901| 
        ; call occurs [#_McBSP_setTxDataSize] ; [] |901| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 908,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |908| 
$C$DW$430	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$430, DW_AT_low_pc(0x00)
	.dwattr $C$DW$430, DW_AT_name("_McBSP_disableTwoPhaseTx")
	.dwattr $C$DW$430, DW_AT_TI_call
        LCR       #_McBSP_disableTwoPhaseTx ; [CPU_] |908| 
        ; call occurs [#_McBSP_disableTwoPhaseTx] ; [] |908| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 913,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |913| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |913| 
        BF        $C$L42,EQ             ; [CPU_] |913| 
        ; branchcc occurs ; [] |913| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 918,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |918| 
$C$DW$431	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$431, DW_AT_low_pc(0x00)
	.dwattr $C$DW$431, DW_AT_name("_McBSP_enableTwoPhaseTx")
	.dwattr $C$DW$431, DW_AT_TI_call
        LCR       #_McBSP_enableTwoPhaseTx ; [CPU_] |918| 
        ; call occurs [#_McBSP_enableTwoPhaseTx] ; [] |918| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 923,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |923| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |923| 
        MOV       *-SP[1],AL            ; [CPU_] |923| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |923| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |923| 
        MOVZ      AR5,*+XAR4[7]         ; [CPU_] |923| 
        MOVB      XAR4,#1               ; [CPU_] |923| 
$C$DW$432	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$432, DW_AT_low_pc(0x00)
	.dwattr $C$DW$432, DW_AT_name("_McBSP_setTxDataSize")
	.dwattr $C$DW$432, DW_AT_TI_call
        LCR       #_McBSP_setTxDataSize ; [CPU_] |923| 
        ; call occurs [#_McBSP_setTxDataSize] ; [] |923| 
$C$L42:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 931,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |931| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |931| 
        MOVB      XAR0,#8               ; [CPU_] |931| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |931| 
$C$DW$433	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$433, DW_AT_low_pc(0x00)
	.dwattr $C$DW$433, DW_AT_name("_McBSP_setTxCompandingMode")
	.dwattr $C$DW$433, DW_AT_TI_call
        LCR       #_McBSP_setTxCompandingMode ; [CPU_] |931| 
        ; call occurs [#_McBSP_setTxCompandingMode] ; [] |931| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 937,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |937| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |937| 
        MOVB      XAR0,#9               ; [CPU_] |937| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |937| 
$C$DW$434	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$434, DW_AT_low_pc(0x00)
	.dwattr $C$DW$434, DW_AT_name("_McBSP_setTxDataDelayBits")
	.dwattr $C$DW$434, DW_AT_TI_call
        LCR       #_McBSP_setTxDataDelayBits ; [CPU_] |937| 
        ; call occurs [#_McBSP_setTxDataDelayBits] ; [] |937| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 943,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |943| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |943| 
        BF        $C$L43,EQ             ; [CPU_] |943| 
        ; branchcc occurs ; [] |943| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 945,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |945| 
$C$DW$435	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$435, DW_AT_low_pc(0x00)
	.dwattr $C$DW$435, DW_AT_name("_McBSP_enableDxPinDelay")
	.dwattr $C$DW$435, DW_AT_TI_call
        LCR       #_McBSP_enableDxPinDelay ; [CPU_] |945| 
        ; call occurs [#_McBSP_enableDxPinDelay] ; [] |945| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 946,column 5,is_stmt
        B         $C$L44,UNC            ; [CPU_] |946| 
        ; branch occurs ; [] |946| 
$C$L43:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 949,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |949| 
$C$DW$436	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$436, DW_AT_low_pc(0x00)
	.dwattr $C$DW$436, DW_AT_name("_McBSP_disableDxPinDelay")
	.dwattr $C$DW$436, DW_AT_TI_call
        LCR       #_McBSP_disableDxPinDelay ; [CPU_] |949| 
        ; call occurs [#_McBSP_disableDxPinDelay] ; [] |949| 
$C$L44:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 955,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |955| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |955| 
        MOVB      XAR0,#10              ; [CPU_] |955| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |955| 
$C$DW$437	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$437, DW_AT_low_pc(0x00)
	.dwattr $C$DW$437, DW_AT_name("_McBSP_setTxInterruptSource")
	.dwattr $C$DW$437, DW_AT_TI_call
        LCR       #_McBSP_setTxInterruptSource ; [CPU_] |955| 
        ; call occurs [#_McBSP_setTxInterruptSource] ; [] |955| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 957,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$438	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$438, DW_AT_low_pc(0x00)
	.dwattr $C$DW$438, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$421, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$421, DW_AT_TI_end_line(0x3bd)
	.dwattr $C$DW$421, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$421

	.sect	".text"
	.clink
	.global	_McBSP_configureRxDataFormat

$C$DW$439	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureRxDataFormat")
	.dwattr $C$DW$439, DW_AT_low_pc(_McBSP_configureRxDataFormat)
	.dwattr $C$DW$439, DW_AT_high_pc(0x00)
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_McBSP_configureRxDataFormat")
	.dwattr $C$DW$439, DW_AT_external
	.dwattr $C$DW$439, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$439, DW_AT_TI_begin_line(0x3c5)
	.dwattr $C$DW$439, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$439, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 967,column 1,is_stmt,address _McBSP_configureRxDataFormat

	.dwfde $C$DW$CIE, _McBSP_configureRxDataFormat
$C$DW$440	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$440, DW_AT_location[DW_OP_reg0]
$C$DW$441	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrDataParams")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_ptrDataParams")
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureRxDataFormat  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureRxDataFormat:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$442	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$442, DW_AT_location[DW_OP_breg20 -4]
$C$DW$443	.dwtag  DW_TAG_variable, DW_AT_name("ptrDataParams")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_ptrDataParams")
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$443, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[6],XAR4          ; [CPU_] |967| 
        MOVL      *-SP[4],ACC           ; [CPU_] |967| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 976,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |976| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |976| 
        BF        $C$L45,EQ             ; [CPU_] |976| 
        ; branchcc occurs ; [] |976| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 978,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |978| 
$C$DW$444	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$444, DW_AT_low_pc(0x00)
	.dwattr $C$DW$444, DW_AT_name("_McBSP_enableLoopback")
	.dwattr $C$DW$444, DW_AT_TI_call
        LCR       #_McBSP_enableLoopback ; [CPU_] |978| 
        ; call occurs [#_McBSP_enableLoopback] ; [] |978| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 979,column 5,is_stmt
        B         $C$L46,UNC            ; [CPU_] |979| 
        ; branch occurs ; [] |979| 
$C$L45:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 982,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |982| 
$C$DW$445	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$445, DW_AT_low_pc(0x00)
	.dwattr $C$DW$445, DW_AT_name("_McBSP_disableLoopback")
	.dwattr $C$DW$445, DW_AT_TI_call
        LCR       #_McBSP_disableLoopback ; [CPU_] |982| 
        ; call occurs [#_McBSP_disableLoopback] ; [] |982| 
$C$L46:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 988,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |988| 
        MOVB      XAR4,#0               ; [CPU_] |988| 
$C$DW$446	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$446, DW_AT_low_pc(0x00)
	.dwattr $C$DW$446, DW_AT_name("_McBSP_setClockStopMode")
	.dwattr $C$DW$446, DW_AT_TI_call
        LCR       #_McBSP_setClockStopMode ; [CPU_] |988| 
        ; call occurs [#_McBSP_setClockStopMode] ; [] |988| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 993,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |993| 
        MOV       AL,*+XAR4[2]          ; [CPU_] |993| 
        MOV       *-SP[1],AL            ; [CPU_] |993| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |993| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |993| 
        MOVZ      AR5,*+XAR4[5]         ; [CPU_] |993| 
        MOVB      XAR4,#0               ; [CPU_] |993| 
$C$DW$447	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$447, DW_AT_low_pc(0x00)
	.dwattr $C$DW$447, DW_AT_name("_McBSP_setRxDataSize")
	.dwattr $C$DW$447, DW_AT_TI_call
        LCR       #_McBSP_setRxDataSize ; [CPU_] |993| 
        ; call occurs [#_McBSP_setRxDataSize] ; [] |993| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1000,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1000| 
$C$DW$448	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$448, DW_AT_low_pc(0x00)
	.dwattr $C$DW$448, DW_AT_name("_McBSP_disableTwoPhaseRx")
	.dwattr $C$DW$448, DW_AT_TI_call
        LCR       #_McBSP_disableTwoPhaseRx ; [CPU_] |1000| 
        ; call occurs [#_McBSP_disableTwoPhaseRx] ; [] |1000| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1005,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1005| 
        MOV       AL,*+XAR4[1]          ; [CPU_] |1005| 
        BF        $C$L47,EQ             ; [CPU_] |1005| 
        ; branchcc occurs ; [] |1005| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1010,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1010| 
$C$DW$449	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$449, DW_AT_low_pc(0x00)
	.dwattr $C$DW$449, DW_AT_name("_McBSP_enableTwoPhaseRx")
	.dwattr $C$DW$449, DW_AT_TI_call
        LCR       #_McBSP_enableTwoPhaseRx ; [CPU_] |1010| 
        ; call occurs [#_McBSP_enableTwoPhaseRx] ; [] |1010| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1015,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1015| 
        MOV       AL,*+XAR4[3]          ; [CPU_] |1015| 
        MOV       *-SP[1],AL            ; [CPU_] |1015| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1015| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1015| 
        MOVZ      AR5,*+XAR4[6]         ; [CPU_] |1015| 
        MOVB      XAR4,#1               ; [CPU_] |1015| 
$C$DW$450	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$450, DW_AT_low_pc(0x00)
	.dwattr $C$DW$450, DW_AT_name("_McBSP_setRxDataSize")
	.dwattr $C$DW$450, DW_AT_TI_call
        LCR       #_McBSP_setRxDataSize ; [CPU_] |1015| 
        ; call occurs [#_McBSP_setRxDataSize] ; [] |1015| 
$C$L47:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1023,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1023| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1023| 
        MOVZ      AR4,*+XAR4[7]         ; [CPU_] |1023| 
$C$DW$451	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$451, DW_AT_low_pc(0x00)
	.dwattr $C$DW$451, DW_AT_name("_McBSP_setRxCompandingMode")
	.dwattr $C$DW$451, DW_AT_TI_call
        LCR       #_McBSP_setRxCompandingMode ; [CPU_] |1023| 
        ; call occurs [#_McBSP_setRxCompandingMode] ; [] |1023| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1029,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1029| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1029| 
        MOVB      XAR0,#8               ; [CPU_] |1029| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |1029| 
$C$DW$452	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$452, DW_AT_low_pc(0x00)
	.dwattr $C$DW$452, DW_AT_name("_McBSP_setRxDataDelayBits")
	.dwattr $C$DW$452, DW_AT_TI_call
        LCR       #_McBSP_setRxDataDelayBits ; [CPU_] |1029| 
        ; call occurs [#_McBSP_setRxDataDelayBits] ; [] |1029| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1035,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1035| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1035| 
        MOVB      XAR0,#9               ; [CPU_] |1035| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |1035| 
$C$DW$453	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$453, DW_AT_low_pc(0x00)
	.dwattr $C$DW$453, DW_AT_name("_McBSP_setRxSignExtension")
	.dwattr $C$DW$453, DW_AT_TI_call
        LCR       #_McBSP_setRxSignExtension ; [CPU_] |1035| 
        ; call occurs [#_McBSP_setRxSignExtension] ; [] |1035| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1041,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1041| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1041| 
        MOVB      XAR0,#10              ; [CPU_] |1041| 
        MOVZ      AR4,*+XAR4[AR0]       ; [CPU_] |1041| 
$C$DW$454	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$454, DW_AT_low_pc(0x00)
	.dwattr $C$DW$454, DW_AT_name("_McBSP_setRxInterruptSource")
	.dwattr $C$DW$454, DW_AT_TI_call
        LCR       #_McBSP_setRxInterruptSource ; [CPU_] |1041| 
        ; call occurs [#_McBSP_setRxInterruptSource] ; [] |1041| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1043,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$455	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$455, DW_AT_low_pc(0x00)
	.dwattr $C$DW$455, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$439, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$439, DW_AT_TI_end_line(0x413)
	.dwattr $C$DW$439, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$439

	.sect	".text"
	.clink
	.global	_McBSP_configureTxMultichannel

$C$DW$456	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureTxMultichannel")
	.dwattr $C$DW$456, DW_AT_low_pc(_McBSP_configureTxMultichannel)
	.dwattr $C$DW$456, DW_AT_high_pc(0x00)
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_McBSP_configureTxMultichannel")
	.dwattr $C$DW$456, DW_AT_external
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$456, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$456, DW_AT_TI_begin_line(0x41b)
	.dwattr $C$DW$456, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$456, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1053,column 1,is_stmt,address _McBSP_configureTxMultichannel

	.dwfde $C$DW$CIE, _McBSP_configureTxMultichannel
$C$DW$457	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$457, DW_AT_location[DW_OP_reg0]
$C$DW$458	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrMchnParams")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_ptrMchnParams")
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$458, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureTxMultichannel FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureTxMultichannel:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$459	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$459, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$459, DW_AT_location[DW_OP_breg20 -2]
$C$DW$460	.dwtag  DW_TAG_variable, DW_AT_name("ptrMchnParams")
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_ptrMchnParams")
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$460, DW_AT_location[DW_OP_breg20 -4]
$C$DW$461	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$461, DW_AT_location[DW_OP_breg20 -5]
$C$DW$462	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$462, DW_AT_location[DW_OP_breg20 -6]
$C$DW$463	.dwtag  DW_TAG_variable, DW_AT_name("partitionAblock")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_partitionAblock")
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$463, DW_AT_location[DW_OP_breg20 -7]
$C$DW$464	.dwtag  DW_TAG_variable, DW_AT_name("partitionBblock")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_partitionBblock")
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$464, DW_AT_location[DW_OP_breg20 -8]
$C$DW$465	.dwtag  DW_TAG_variable, DW_AT_name("partitionAflag")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_partitionAflag")
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$465, DW_AT_location[DW_OP_breg20 -9]
$C$DW$466	.dwtag  DW_TAG_variable, DW_AT_name("partitionBflag")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_partitionBflag")
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$466, DW_AT_location[DW_OP_breg20 -10]
$C$DW$467	.dwtag  DW_TAG_variable, DW_AT_name("errorTx")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_errorTx")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$467, DW_AT_location[DW_OP_breg20 -11]
        MOVL      *-SP[4],XAR4          ; [CPU_] |1053| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1053| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1062,column 5,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |1062| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1063,column 5,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |1063| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1064,column 5,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |1064| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1074,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1074| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1074| 
        MOVZ      AR4,*+XAR4[4]         ; [CPU_] |1074| 
$C$DW$468	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$468, DW_AT_low_pc(0x00)
	.dwattr $C$DW$468, DW_AT_name("_McBSP_setTxChannelMode")
	.dwattr $C$DW$468, DW_AT_TI_call
        LCR       #_McBSP_setTxChannelMode ; [CPU_] |1074| 
        ; call occurs [#_McBSP_setTxChannelMode] ; [] |1074| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1083,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1083| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |1083| 
        BF        $C$L61,EQ             ; [CPU_] |1083| 
        ; branchcc occurs ; [] |1083| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1089,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1089| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1089| 
        MOVZ      AR4,*+XAR4[5]         ; [CPU_] |1089| 
$C$DW$469	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$469, DW_AT_low_pc(0x00)
	.dwattr $C$DW$469, DW_AT_name("_McBSP_setTxMultichannelPartition")
	.dwattr $C$DW$469, DW_AT_TI_call
        LCR       #_McBSP_setTxMultichannelPartition ; [CPU_] |1089| 
        ; call occurs [#_McBSP_setTxMultichannelPartition] ; [] |1089| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1095,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |1095| 
$C$DW$470	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$470, DW_AT_low_pc(0x00)
	.dwattr $C$DW$470, DW_AT_name("_McBSP_disableTwoPhaseTx")
	.dwattr $C$DW$470, DW_AT_TI_call
        LCR       #_McBSP_disableTwoPhaseTx ; [CPU_] |1095| 
        ; call occurs [#_McBSP_disableTwoPhaseTx] ; [] |1095| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1100,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1100| 
        MOV       AL,*+XAR4[5]          ; [CPU_] |1100| 
        BF        $C$L57,NEQ            ; [CPU_] |1100| 
        ; branchcc occurs ; [] |1100| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1103,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1103| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1103| 
        CMPB      AL,#32                ; [CPU_] |1103| 
        B         $C$L48,LOS            ; [CPU_] |1103| 
        ; branchcc occurs ; [] |1103| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1105,column 17,is_stmt
        MOVB      *-SP[11],#1,UNC       ; [CPU_] |1105| 
$C$L48:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1107,column 13,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |1107| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1108,column 13,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |1108| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1115,column 17,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |1115| 
        B         $C$L56,UNC            ; [CPU_] |1115| 
        ; branch occurs ; [] |1115| 
$C$L49:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1121,column 17,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1121| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |1121| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1121| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |1121| 
        LSR       AL,4                  ; [CPU_] |1121| 
        MOV       *-SP[6],AL            ; [CPU_] |1121| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1128,column 17,is_stmt
        TBIT      *-SP[6],#0            ; [CPU_] |1128| 
        BF        $C$L52,TC             ; [CPU_] |1128| 
        ; branchcc occurs ; [] |1128| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1133,column 21,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |1133| 
        BF        $C$L50,NEQ            ; [CPU_] |1133| 
        ; branchcc occurs ; [] |1133| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1138,column 25,is_stmt
        MOVZ      AR4,*-SP[6]           ; [CPU_] |1138| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1138| 
$C$DW$471	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$471, DW_AT_low_pc(0x00)
	.dwattr $C$DW$471, DW_AT_name("_McBSP_setTxTwoPartitionBlock")
	.dwattr $C$DW$471, DW_AT_TI_call
        LCR       #_McBSP_setTxTwoPartitionBlock ; [CPU_] |1138| 
        ; call occurs [#_McBSP_setTxTwoPartitionBlock] ; [] |1138| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1146,column 25,is_stmt
        MOVB      *-SP[9],#1,UNC        ; [CPU_] |1146| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1147,column 25,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1147| 
        MOV       *-SP[7],AL            ; [CPU_] |1147| 
$C$L50:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1154,column 21,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |1154| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |1154| 
        CMPL      ACC,XAR6              ; [CPU_] |1154| 
        BF        $C$L51,NEQ            ; [CPU_] |1154| 
        ; branchcc occurs ; [] |1154| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1160,column 25,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1160| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1160| 
        MOV       AL,*-SP[5]            ; [CPU_] |1160| 
        ADD       AL,*+XAR4[0]          ; [CPU_] |1160| 
        MOVZ      AR5,AL                ; [CPU_] |1160| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1160| 
        MOVB      XAR4,#0               ; [CPU_] |1160| 
$C$DW$472	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$472, DW_AT_low_pc(0x00)
	.dwattr $C$DW$472, DW_AT_name("_McBSP_enableTxChannel")
	.dwattr $C$DW$472, DW_AT_TI_call
        LCR       #_McBSP_enableTxChannel ; [CPU_] |1160| 
        ; call occurs [#_McBSP_enableTxChannel] ; [] |1160| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1164,column 21,is_stmt
        B         $C$L55,UNC            ; [CPU_] |1164| 
        ; branch occurs ; [] |1164| 
$C$L51:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1167,column 25,is_stmt
        MOVB      *-SP[11],#2,UNC       ; [CPU_] |1167| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1169,column 17,is_stmt
        B         $C$L55,UNC            ; [CPU_] |1169| 
        ; branch occurs ; [] |1169| 
$C$L52:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1180,column 21,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |1180| 
        BF        $C$L53,NEQ            ; [CPU_] |1180| 
        ; branchcc occurs ; [] |1180| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1185,column 25,is_stmt
        MOVZ      AR4,*-SP[6]           ; [CPU_] |1185| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1185| 
$C$DW$473	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$473, DW_AT_low_pc(0x00)
	.dwattr $C$DW$473, DW_AT_name("_McBSP_setTxTwoPartitionBlock")
	.dwattr $C$DW$473, DW_AT_TI_call
        LCR       #_McBSP_setTxTwoPartitionBlock ; [CPU_] |1185| 
        ; call occurs [#_McBSP_setTxTwoPartitionBlock] ; [] |1185| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1193,column 25,is_stmt
        MOVB      *-SP[10],#1,UNC       ; [CPU_] |1193| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1194,column 25,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1194| 
        MOV       *-SP[8],AL            ; [CPU_] |1194| 
$C$L53:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1201,column 21,is_stmt
        MOVZ      AR6,*-SP[8]           ; [CPU_] |1201| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |1201| 
        CMPL      ACC,XAR6              ; [CPU_] |1201| 
        BF        $C$L54,NEQ            ; [CPU_] |1201| 
        ; branchcc occurs ; [] |1201| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1207,column 25,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1207| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |1207| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1207| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1207| 
        MOVZ      AR5,*+XAR4[AR0]       ; [CPU_] |1207| 
        MOVB      XAR4,#0               ; [CPU_] |1207| 
$C$DW$474	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$474, DW_AT_low_pc(0x00)
	.dwattr $C$DW$474, DW_AT_name("_McBSP_enableTxChannel")
	.dwattr $C$DW$474, DW_AT_TI_call
        LCR       #_McBSP_enableTxChannel ; [CPU_] |1207| 
        ; call occurs [#_McBSP_enableTxChannel] ; [] |1207| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1211,column 21,is_stmt
        B         $C$L55,UNC            ; [CPU_] |1211| 
        ; branch occurs ; [] |1211| 
$C$L54:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1214,column 25,is_stmt
        OR        *-SP[11],#0x0004      ; [CPU_] |1214| 
$C$L55:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1116,column 17,is_stmt
        INC       *-SP[5]               ; [CPU_] |1116| 
$C$L56:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1115,column 29,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1115| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1115| 
        CMP       AL,*-SP[5]            ; [CPU_] |1115| 
        B         $C$L49,HI             ; [CPU_] |1115| 
        ; branchcc occurs ; [] |1115| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1218,column 9,is_stmt
        B         $C$L61,UNC            ; [CPU_] |1218| 
        ; branch occurs ; [] |1218| 
$C$L57:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1225,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1225| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1225| 
        CMPB      AL,#128               ; [CPU_] |1225| 
        B         $C$L58,LOS            ; [CPU_] |1225| 
        ; branchcc occurs ; [] |1225| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1227,column 17,is_stmt
        MOVB      *-SP[11],#1,UNC       ; [CPU_] |1227| 
$C$L58:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1229,column 17,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |1229| 
        B         $C$L60,UNC            ; [CPU_] |1229| 
        ; branch occurs ; [] |1229| 
$C$L59:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1235,column 17,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1235| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |1235| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1235| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1235| 
        MOVZ      AR5,*+XAR4[AR0]       ; [CPU_] |1235| 
        MOVL      XAR4,#512             ; [CPU_] |1235| 
$C$DW$475	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$475, DW_AT_low_pc(0x00)
	.dwattr $C$DW$475, DW_AT_name("_McBSP_enableTxChannel")
	.dwattr $C$DW$475, DW_AT_TI_call
        LCR       #_McBSP_enableTxChannel ; [CPU_] |1235| 
        ; call occurs [#_McBSP_enableTxChannel] ; [] |1235| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1230,column 17,is_stmt
        INC       *-SP[5]               ; [CPU_] |1230| 
$C$L60:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1229,column 29,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1229| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1229| 
        CMP       AL,*-SP[5]            ; [CPU_] |1229| 
        B         $C$L59,HI             ; [CPU_] |1229| 
        ; branchcc occurs ; [] |1229| 
$C$L61:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1241,column 5,is_stmt
        MOV       AL,*-SP[11]           ; [CPU_] |1241| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1242,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$476	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$476, DW_AT_low_pc(0x00)
	.dwattr $C$DW$476, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$456, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$456, DW_AT_TI_end_line(0x4da)
	.dwattr $C$DW$456, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$456

	.sect	".text"
	.clink
	.global	_McBSP_configureRxMultichannel

$C$DW$477	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureRxMultichannel")
	.dwattr $C$DW$477, DW_AT_low_pc(_McBSP_configureRxMultichannel)
	.dwattr $C$DW$477, DW_AT_high_pc(0x00)
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_McBSP_configureRxMultichannel")
	.dwattr $C$DW$477, DW_AT_external
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$477, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$477, DW_AT_TI_begin_line(0x4e2)
	.dwattr $C$DW$477, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$477, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1252,column 1,is_stmt,address _McBSP_configureRxMultichannel

	.dwfde $C$DW$CIE, _McBSP_configureRxMultichannel
$C$DW$478	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$478, DW_AT_location[DW_OP_reg0]
$C$DW$479	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrMchnParams")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_ptrMchnParams")
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$479, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureRxMultichannel FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureRxMultichannel:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$480	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$480, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$480, DW_AT_location[DW_OP_breg20 -2]
$C$DW$481	.dwtag  DW_TAG_variable, DW_AT_name("ptrMchnParams")
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_ptrMchnParams")
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$481, DW_AT_location[DW_OP_breg20 -4]
$C$DW$482	.dwtag  DW_TAG_variable, DW_AT_name("index")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$482, DW_AT_location[DW_OP_breg20 -5]
$C$DW$483	.dwtag  DW_TAG_variable, DW_AT_name("block")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_block")
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$483, DW_AT_location[DW_OP_breg20 -6]
$C$DW$484	.dwtag  DW_TAG_variable, DW_AT_name("partitionAblock")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_partitionAblock")
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$484, DW_AT_location[DW_OP_breg20 -7]
$C$DW$485	.dwtag  DW_TAG_variable, DW_AT_name("partitionBblock")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_partitionBblock")
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$485, DW_AT_location[DW_OP_breg20 -8]
$C$DW$486	.dwtag  DW_TAG_variable, DW_AT_name("partitionAflag")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_partitionAflag")
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$486, DW_AT_location[DW_OP_breg20 -9]
$C$DW$487	.dwtag  DW_TAG_variable, DW_AT_name("partitionBflag")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_partitionBflag")
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$487, DW_AT_location[DW_OP_breg20 -10]
$C$DW$488	.dwtag  DW_TAG_variable, DW_AT_name("errorRx")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_errorRx")
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$488, DW_AT_location[DW_OP_breg20 -11]
        MOVL      *-SP[4],XAR4          ; [CPU_] |1252| 
        MOVL      *-SP[2],ACC           ; [CPU_] |1252| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1261,column 5,is_stmt
        MOV       *-SP[11],#0           ; [CPU_] |1261| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1262,column 5,is_stmt
        MOV       *-SP[7],#0            ; [CPU_] |1262| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1263,column 5,is_stmt
        MOV       *-SP[8],#0            ; [CPU_] |1263| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1273,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1273| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1273| 
        MOVZ      AR4,*+XAR4[4]         ; [CPU_] |1273| 
$C$DW$489	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$489, DW_AT_low_pc(0x00)
	.dwattr $C$DW$489, DW_AT_name("_McBSP_setRxChannelMode")
	.dwattr $C$DW$489, DW_AT_TI_call
        LCR       #_McBSP_setRxChannelMode ; [CPU_] |1273| 
        ; call occurs [#_McBSP_setRxChannelMode] ; [] |1273| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1279,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1279| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1279| 
        MOVZ      AR4,*+XAR4[5]         ; [CPU_] |1279| 
$C$DW$490	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$490, DW_AT_low_pc(0x00)
	.dwattr $C$DW$490, DW_AT_name("_McBSP_setRxMultichannelPartition")
	.dwattr $C$DW$490, DW_AT_TI_call
        LCR       #_McBSP_setRxMultichannelPartition ; [CPU_] |1279| 
        ; call occurs [#_McBSP_setRxMultichannelPartition] ; [] |1279| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1286,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1286| 
        MOV       AL,*+XAR4[4]          ; [CPU_] |1286| 
        CMPB      AL,#1                 ; [CPU_] |1286| 
        BF        $C$L75,NEQ            ; [CPU_] |1286| 
        ; branchcc occurs ; [] |1286| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1292,column 9,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |1292| 
$C$DW$491	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$491, DW_AT_low_pc(0x00)
	.dwattr $C$DW$491, DW_AT_name("_McBSP_disableTwoPhaseRx")
	.dwattr $C$DW$491, DW_AT_TI_call
        LCR       #_McBSP_disableTwoPhaseRx ; [CPU_] |1292| 
        ; call occurs [#_McBSP_disableTwoPhaseRx] ; [] |1292| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1297,column 9,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1297| 
        MOV       AL,*+XAR4[5]          ; [CPU_] |1297| 
        BF        $C$L71,NEQ            ; [CPU_] |1297| 
        ; branchcc occurs ; [] |1297| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1300,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1300| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1300| 
        CMPB      AL,#32                ; [CPU_] |1300| 
        B         $C$L62,LOS            ; [CPU_] |1300| 
        ; branchcc occurs ; [] |1300| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1302,column 17,is_stmt
        MOVB      *-SP[11],#1,UNC       ; [CPU_] |1302| 
$C$L62:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1304,column 13,is_stmt
        MOV       *-SP[9],#0            ; [CPU_] |1304| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1305,column 13,is_stmt
        MOV       *-SP[10],#0           ; [CPU_] |1305| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1312,column 17,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |1312| 
        B         $C$L70,UNC            ; [CPU_] |1312| 
        ; branch occurs ; [] |1312| 
$C$L63:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1318,column 17,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1318| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |1318| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1318| 
        MOV       AL,*+XAR4[AR0]        ; [CPU_] |1318| 
        LSR       AL,4                  ; [CPU_] |1318| 
        MOV       *-SP[6],AL            ; [CPU_] |1318| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1324,column 17,is_stmt
        TBIT      *-SP[6],#0            ; [CPU_] |1324| 
        BF        $C$L66,TC             ; [CPU_] |1324| 
        ; branchcc occurs ; [] |1324| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1329,column 21,is_stmt
        MOV       AL,*-SP[9]            ; [CPU_] |1329| 
        BF        $C$L64,NEQ            ; [CPU_] |1329| 
        ; branchcc occurs ; [] |1329| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1334,column 25,is_stmt
        MOVZ      AR4,*-SP[6]           ; [CPU_] |1334| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1334| 
$C$DW$492	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$492, DW_AT_low_pc(0x00)
	.dwattr $C$DW$492, DW_AT_name("_McBSP_setRxTwoPartitionBlock")
	.dwattr $C$DW$492, DW_AT_TI_call
        LCR       #_McBSP_setRxTwoPartitionBlock ; [CPU_] |1334| 
        ; call occurs [#_McBSP_setRxTwoPartitionBlock] ; [] |1334| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1342,column 25,is_stmt
        MOVB      *-SP[9],#1,UNC        ; [CPU_] |1342| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1343,column 25,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1343| 
        MOV       *-SP[7],AL            ; [CPU_] |1343| 
$C$L64:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1350,column 21,is_stmt
        MOVZ      AR6,*-SP[7]           ; [CPU_] |1350| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |1350| 
        CMPL      ACC,XAR6              ; [CPU_] |1350| 
        BF        $C$L65,NEQ            ; [CPU_] |1350| 
        ; branchcc occurs ; [] |1350| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1356,column 25,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1356| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |1356| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1356| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1356| 
        MOVZ      AR5,*+XAR4[AR0]       ; [CPU_] |1356| 
        MOVB      XAR4,#0               ; [CPU_] |1356| 
$C$DW$493	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$493, DW_AT_low_pc(0x00)
	.dwattr $C$DW$493, DW_AT_name("_McBSP_enableRxChannel")
	.dwattr $C$DW$493, DW_AT_TI_call
        LCR       #_McBSP_enableRxChannel ; [CPU_] |1356| 
        ; call occurs [#_McBSP_enableRxChannel] ; [] |1356| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1360,column 21,is_stmt
        B         $C$L69,UNC            ; [CPU_] |1360| 
        ; branch occurs ; [] |1360| 
$C$L65:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1363,column 25,is_stmt
        MOVB      *-SP[11],#2,UNC       ; [CPU_] |1363| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1365,column 17,is_stmt
        B         $C$L69,UNC            ; [CPU_] |1365| 
        ; branch occurs ; [] |1365| 
$C$L66:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1376,column 21,is_stmt
        MOV       AL,*-SP[10]           ; [CPU_] |1376| 
        BF        $C$L67,NEQ            ; [CPU_] |1376| 
        ; branchcc occurs ; [] |1376| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1381,column 25,is_stmt
        MOVZ      AR4,*-SP[6]           ; [CPU_] |1381| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1381| 
$C$DW$494	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$494, DW_AT_low_pc(0x00)
	.dwattr $C$DW$494, DW_AT_name("_McBSP_setRxTwoPartitionBlock")
	.dwattr $C$DW$494, DW_AT_TI_call
        LCR       #_McBSP_setRxTwoPartitionBlock ; [CPU_] |1381| 
        ; call occurs [#_McBSP_setRxTwoPartitionBlock] ; [] |1381| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1388,column 25,is_stmt
        MOVB      *-SP[10],#1,UNC       ; [CPU_] |1388| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1389,column 25,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1389| 
        MOV       *-SP[8],AL            ; [CPU_] |1389| 
$C$L67:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1396,column 21,is_stmt
        MOVZ      AR6,*-SP[8]           ; [CPU_] |1396| 
        MOVU      ACC,*-SP[6]           ; [CPU_] |1396| 
        CMPL      ACC,XAR6              ; [CPU_] |1396| 
        BF        $C$L68,NEQ            ; [CPU_] |1396| 
        ; branchcc occurs ; [] |1396| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1402,column 25,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1402| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |1402| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1402| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1402| 
        MOVZ      AR5,*+XAR4[AR0]       ; [CPU_] |1402| 
        MOVB      XAR4,#0               ; [CPU_] |1402| 
$C$DW$495	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$495, DW_AT_low_pc(0x00)
	.dwattr $C$DW$495, DW_AT_name("_McBSP_enableRxChannel")
	.dwattr $C$DW$495, DW_AT_TI_call
        LCR       #_McBSP_enableRxChannel ; [CPU_] |1402| 
        ; call occurs [#_McBSP_enableRxChannel] ; [] |1402| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1406,column 21,is_stmt
        B         $C$L69,UNC            ; [CPU_] |1406| 
        ; branch occurs ; [] |1406| 
$C$L68:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1409,column 25,is_stmt
        OR        *-SP[11],#0x0004      ; [CPU_] |1409| 
$C$L69:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1313,column 17,is_stmt
        INC       *-SP[5]               ; [CPU_] |1313| 
$C$L70:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1312,column 29,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1312| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1312| 
        CMP       AL,*-SP[5]            ; [CPU_] |1312| 
        B         $C$L63,HI             ; [CPU_] |1312| 
        ; branchcc occurs ; [] |1312| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1413,column 9,is_stmt
        B         $C$L75,UNC            ; [CPU_] |1413| 
        ; branch occurs ; [] |1413| 
$C$L71:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1420,column 13,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1420| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1420| 
        CMPB      AL,#128               ; [CPU_] |1420| 
        B         $C$L72,LOS            ; [CPU_] |1420| 
        ; branchcc occurs ; [] |1420| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1422,column 17,is_stmt
        MOVB      *-SP[11],#1,UNC       ; [CPU_] |1422| 
$C$L72:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1424,column 17,is_stmt
        MOV       *-SP[5],#0            ; [CPU_] |1424| 
        B         $C$L74,UNC            ; [CPU_] |1424| 
        ; branch occurs ; [] |1424| 
$C$L73:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1430,column 17,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1430| 
        MOVZ      AR0,*-SP[5]           ; [CPU_] |1430| 
        MOVL      XAR4,*+XAR4[2]        ; [CPU_] |1430| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |1430| 
        MOVZ      AR5,*+XAR4[AR0]       ; [CPU_] |1430| 
        MOVL      XAR4,#512             ; [CPU_] |1430| 
$C$DW$496	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$496, DW_AT_low_pc(0x00)
	.dwattr $C$DW$496, DW_AT_name("_McBSP_enableRxChannel")
	.dwattr $C$DW$496, DW_AT_TI_call
        LCR       #_McBSP_enableRxChannel ; [CPU_] |1430| 
        ; call occurs [#_McBSP_enableRxChannel] ; [] |1430| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1425,column 17,is_stmt
        INC       *-SP[5]               ; [CPU_] |1425| 
$C$L74:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1424,column 29,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |1424| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1424| 
        CMP       AL,*-SP[5]            ; [CPU_] |1424| 
        B         $C$L73,HI             ; [CPU_] |1424| 
        ; branchcc occurs ; [] |1424| 
$C$L75:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1436,column 5,is_stmt
        MOV       AL,*-SP[11]           ; [CPU_] |1436| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1437,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$497	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$497, DW_AT_low_pc(0x00)
	.dwattr $C$DW$497, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$477, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$477, DW_AT_TI_end_line(0x59d)
	.dwattr $C$DW$477, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$477

	.sect	".text"
	.clink
	.global	_McBSP_configureSPIMasterMode

$C$DW$498	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureSPIMasterMode")
	.dwattr $C$DW$498, DW_AT_low_pc(_McBSP_configureSPIMasterMode)
	.dwattr $C$DW$498, DW_AT_high_pc(0x00)
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_McBSP_configureSPIMasterMode")
	.dwattr $C$DW$498, DW_AT_external
	.dwattr $C$DW$498, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$498, DW_AT_TI_begin_line(0x5a5)
	.dwattr $C$DW$498, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$498, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1447,column 1,is_stmt,address _McBSP_configureSPIMasterMode

	.dwfde $C$DW$CIE, _McBSP_configureSPIMasterMode
$C$DW$499	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$499, DW_AT_location[DW_OP_reg0]
$C$DW$500	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrSPIMasterMode")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_ptrSPIMasterMode")
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$500, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureSPIMasterMode FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureSPIMasterMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$501	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$501, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$501, DW_AT_location[DW_OP_breg20 -4]
$C$DW$502	.dwtag  DW_TAG_variable, DW_AT_name("ptrSPIMasterMode")
	.dwattr $C$DW$502, DW_AT_TI_symbol_name("_ptrSPIMasterMode")
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$502, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[6],XAR4          ; [CPU_] |1447| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1447| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1451,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1451| 
        CMP       *+XAR4[4],#4096       ; [CPU_] |1451| 
        BF        $C$L76,EQ             ; [CPU_] |1451| 
        ; branchcc occurs ; [] |1451| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1451| 
        CMP       *+XAR4[4],#6144       ; [CPU_] |1451| 
        BF        $C$L79,NEQ            ; [CPU_] |1451| 
        ; branchcc occurs ; [] |1451| 
$C$L76:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1457,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1457| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1457| 
        MOVZ      AR4,*+XAR4[4]         ; [CPU_] |1457| 
$C$DW$503	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$503, DW_AT_low_pc(0x00)
	.dwattr $C$DW$503, DW_AT_name("_McBSP_setClockStopMode")
	.dwattr $C$DW$503, DW_AT_TI_call
        LCR       #_McBSP_setClockStopMode ; [CPU_] |1457| 
        ; call occurs [#_McBSP_setClockStopMode] ; [] |1457| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1463,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1463| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1463| 
        BF        $C$L77,EQ             ; [CPU_] |1463| 
        ; branchcc occurs ; [] |1463| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1465,column 13,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1465| 
$C$DW$504	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$504, DW_AT_low_pc(0x00)
	.dwattr $C$DW$504, DW_AT_name("_McBSP_enableLoopback")
	.dwattr $C$DW$504, DW_AT_TI_call
        LCR       #_McBSP_enableLoopback ; [CPU_] |1465| 
        ; call occurs [#_McBSP_enableLoopback] ; [] |1465| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1466,column 9,is_stmt
        B         $C$L78,UNC            ; [CPU_] |1466| 
        ; branch occurs ; [] |1466| 
$C$L77:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1469,column 13,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1469| 
$C$DW$505	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$505, DW_AT_low_pc(0x00)
	.dwattr $C$DW$505, DW_AT_name("_McBSP_disableLoopback")
	.dwattr $C$DW$505, DW_AT_TI_call
        LCR       #_McBSP_disableLoopback ; [CPU_] |1469| 
        ; call occurs [#_McBSP_disableLoopback] ; [] |1469| 
$C$L78:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1476,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1476| 
        MOVL      XAR4,#512             ; [CPU_] |1476| 
$C$DW$506	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$506, DW_AT_low_pc(0x00)
	.dwattr $C$DW$506, DW_AT_name("_McBSP_setTxClockSource")
	.dwattr $C$DW$506, DW_AT_TI_call
        LCR       #_McBSP_setTxClockSource ; [CPU_] |1476| 
        ; call occurs [#_McBSP_setTxClockSource] ; [] |1476| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1481,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1481| 
        MOVB      XAR4,#1               ; [CPU_] |1481| 
$C$DW$507	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$507, DW_AT_low_pc(0x00)
	.dwattr $C$DW$507, DW_AT_name("_McBSP_setTxSRGClockSource")
	.dwattr $C$DW$507, DW_AT_TI_call
        LCR       #_McBSP_setTxSRGClockSource ; [CPU_] |1481| 
        ; call occurs [#_McBSP_setTxSRGClockSource] ; [] |1481| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1486,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1486| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1486| 
        MOVZ      AR4,*+XAR4[2]         ; [CPU_] |1486| 
$C$DW$508	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$508, DW_AT_low_pc(0x00)
	.dwattr $C$DW$508, DW_AT_name("_McBSP_setSRGDataClockDivider")
	.dwattr $C$DW$508, DW_AT_TI_call
        LCR       #_McBSP_setSRGDataClockDivider ; [CPU_] |1486| 
        ; call occurs [#_McBSP_setSRGDataClockDivider] ; [] |1486| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1492,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1492| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1492| 
        MOVZ      AR4,*+XAR4[6]         ; [CPU_] |1492| 
$C$DW$509	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$509, DW_AT_low_pc(0x00)
	.dwattr $C$DW$509, DW_AT_name("_McBSP_setTxClockPolarity")
	.dwattr $C$DW$509, DW_AT_TI_call
        LCR       #_McBSP_setTxClockPolarity ; [CPU_] |1492| 
        ; call occurs [#_McBSP_setTxClockPolarity] ; [] |1492| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1498,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1498| 
        MOVL      XAR4,#2048            ; [CPU_] |1498| 
$C$DW$510	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$510, DW_AT_low_pc(0x00)
	.dwattr $C$DW$510, DW_AT_name("_McBSP_setTxFrameSyncSource")
	.dwattr $C$DW$510, DW_AT_TI_call
        LCR       #_McBSP_setTxFrameSyncSource ; [CPU_] |1498| 
        ; call occurs [#_McBSP_setTxFrameSyncSource] ; [] |1498| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1503,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1503| 
        MOVB      XAR4,#0               ; [CPU_] |1503| 
$C$DW$511	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$511, DW_AT_low_pc(0x00)
	.dwattr $C$DW$511, DW_AT_name("_McBSP_setTxInternalFrameSyncSource")
	.dwattr $C$DW$511, DW_AT_TI_call
        LCR       #_McBSP_setTxInternalFrameSyncSource ; [CPU_] |1503| 
        ; call occurs [#_McBSP_setTxInternalFrameSyncSource] ; [] |1503| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1509,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1509| 
        MOVB      XAR4,#8               ; [CPU_] |1509| 
$C$DW$512	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$512, DW_AT_low_pc(0x00)
	.dwattr $C$DW$512, DW_AT_name("_McBSP_setTxFrameSyncPolarity")
	.dwattr $C$DW$512, DW_AT_TI_call
        LCR       #_McBSP_setTxFrameSyncPolarity ; [CPU_] |1509| 
        ; call occurs [#_McBSP_setTxFrameSyncPolarity] ; [] |1509| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1515,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1515| 
$C$DW$513	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$513, DW_AT_low_pc(0x00)
	.dwattr $C$DW$513, DW_AT_name("_McBSP_disableTwoPhaseTx")
	.dwattr $C$DW$513, DW_AT_TI_call
        LCR       #_McBSP_disableTwoPhaseTx ; [CPU_] |1515| 
        ; call occurs [#_McBSP_disableTwoPhaseTx] ; [] |1515| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1520,column 9,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |1520| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1520| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1520| 
        MOVZ      AR5,*+XAR4[5]         ; [CPU_] |1520| 
        MOVB      XAR4,#0               ; [CPU_] |1520| 
$C$DW$514	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$514, DW_AT_low_pc(0x00)
	.dwattr $C$DW$514, DW_AT_name("_McBSP_setTxDataSize")
	.dwattr $C$DW$514, DW_AT_TI_call
        LCR       #_McBSP_setTxDataSize ; [CPU_] |1520| 
        ; call occurs [#_McBSP_setTxDataSize] ; [] |1520| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1522,column 9,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |1522| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1522| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1522| 
        MOVZ      AR5,*+XAR4[5]         ; [CPU_] |1522| 
        MOVB      XAR4,#0               ; [CPU_] |1522| 
$C$DW$515	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$515, DW_AT_low_pc(0x00)
	.dwattr $C$DW$515, DW_AT_name("_McBSP_setRxDataSize")
	.dwattr $C$DW$515, DW_AT_TI_call
        LCR       #_McBSP_setRxDataSize ; [CPU_] |1522| 
        ; call occurs [#_McBSP_setRxDataSize] ; [] |1522| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1529,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1529| 
        MOVB      XAR4,#1               ; [CPU_] |1529| 
$C$DW$516	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$516, DW_AT_low_pc(0x00)
	.dwattr $C$DW$516, DW_AT_name("_McBSP_setTxDataDelayBits")
	.dwattr $C$DW$516, DW_AT_TI_call
        LCR       #_McBSP_setTxDataDelayBits ; [CPU_] |1529| 
        ; call occurs [#_McBSP_setTxDataDelayBits] ; [] |1529| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1530,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1530| 
        MOVB      XAR4,#1               ; [CPU_] |1530| 
$C$DW$517	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$517, DW_AT_low_pc(0x00)
	.dwattr $C$DW$517, DW_AT_name("_McBSP_setRxDataDelayBits")
	.dwattr $C$DW$517, DW_AT_TI_call
        LCR       #_McBSP_setRxDataDelayBits ; [CPU_] |1530| 
        ; call occurs [#_McBSP_setRxDataDelayBits] ; [] |1530| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1532,column 1,is_stmt
$C$L79:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$518	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$518, DW_AT_low_pc(0x00)
	.dwattr $C$DW$518, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$498, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$498, DW_AT_TI_end_line(0x5fc)
	.dwattr $C$DW$498, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$498

	.sect	".text"
	.clink
	.global	_McBSP_configureSPISlaveMode

$C$DW$519	.dwtag  DW_TAG_subprogram, DW_AT_name("McBSP_configureSPISlaveMode")
	.dwattr $C$DW$519, DW_AT_low_pc(_McBSP_configureSPISlaveMode)
	.dwattr $C$DW$519, DW_AT_high_pc(0x00)
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_McBSP_configureSPISlaveMode")
	.dwattr $C$DW$519, DW_AT_external
	.dwattr $C$DW$519, DW_AT_TI_begin_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$519, DW_AT_TI_begin_line(0x604)
	.dwattr $C$DW$519, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$519, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1542,column 1,is_stmt,address _McBSP_configureSPISlaveMode

	.dwfde $C$DW$CIE, _McBSP_configureSPISlaveMode
$C$DW$520	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$520, DW_AT_location[DW_OP_reg0]
$C$DW$521	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ptrSPISlaveMode")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_ptrSPISlaveMode")
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$521, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _McBSP_configureSPISlaveMode  FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_McBSP_configureSPISlaveMode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$522	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$522, DW_AT_location[DW_OP_breg20 -4]
$C$DW$523	.dwtag  DW_TAG_variable, DW_AT_name("ptrSPISlaveMode")
	.dwattr $C$DW$523, DW_AT_TI_symbol_name("_ptrSPISlaveMode")
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$523, DW_AT_location[DW_OP_breg20 -6]
        MOVL      *-SP[6],XAR4          ; [CPU_] |1542| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1542| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1547,column 5,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1547| 
        CMP       *+XAR4[1],#4096       ; [CPU_] |1547| 
        BF        $C$L80,EQ             ; [CPU_] |1547| 
        ; branchcc occurs ; [] |1547| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1547| 
        CMP       *+XAR4[1],#6144       ; [CPU_] |1547| 
        BF        $C$L83,NEQ            ; [CPU_] |1547| 
        ; branchcc occurs ; [] |1547| 
$C$L80:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1553,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1553| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1553| 
        MOVZ      AR4,*+XAR4[1]         ; [CPU_] |1553| 
$C$DW$524	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$524, DW_AT_low_pc(0x00)
	.dwattr $C$DW$524, DW_AT_name("_McBSP_setClockStopMode")
	.dwattr $C$DW$524, DW_AT_TI_call
        LCR       #_McBSP_setClockStopMode ; [CPU_] |1553| 
        ; call occurs [#_McBSP_setClockStopMode] ; [] |1553| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1558,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1558| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |1558| 
        BF        $C$L81,EQ             ; [CPU_] |1558| 
        ; branchcc occurs ; [] |1558| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1560,column 13,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1560| 
$C$DW$525	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$525, DW_AT_low_pc(0x00)
	.dwattr $C$DW$525, DW_AT_name("_McBSP_enableLoopback")
	.dwattr $C$DW$525, DW_AT_TI_call
        LCR       #_McBSP_enableLoopback ; [CPU_] |1560| 
        ; call occurs [#_McBSP_enableLoopback] ; [] |1560| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1561,column 9,is_stmt
        B         $C$L82,UNC            ; [CPU_] |1561| 
        ; branch occurs ; [] |1561| 
$C$L81:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1564,column 13,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1564| 
$C$DW$526	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$526, DW_AT_low_pc(0x00)
	.dwattr $C$DW$526, DW_AT_name("_McBSP_disableLoopback")
	.dwattr $C$DW$526, DW_AT_TI_call
        LCR       #_McBSP_disableLoopback ; [CPU_] |1564| 
        ; call occurs [#_McBSP_disableLoopback] ; [] |1564| 
$C$L82:    
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1571,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1571| 
        MOVB      XAR4,#0               ; [CPU_] |1571| 
$C$DW$527	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$527, DW_AT_low_pc(0x00)
	.dwattr $C$DW$527, DW_AT_name("_McBSP_setTxClockSource")
	.dwattr $C$DW$527, DW_AT_TI_call
        LCR       #_McBSP_setTxClockSource ; [CPU_] |1571| 
        ; call occurs [#_McBSP_setTxClockSource] ; [] |1571| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1576,column 9,is_stmt
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1576| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1576| 
        MOVZ      AR4,*+XAR4[3]         ; [CPU_] |1576| 
$C$DW$528	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$528, DW_AT_low_pc(0x00)
	.dwattr $C$DW$528, DW_AT_name("_McBSP_setTxClockPolarity")
	.dwattr $C$DW$528, DW_AT_TI_call
        LCR       #_McBSP_setTxClockPolarity ; [CPU_] |1576| 
        ; call occurs [#_McBSP_setTxClockPolarity] ; [] |1576| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1583,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1583| 
        MOVB      XAR4,#1               ; [CPU_] |1583| 
$C$DW$529	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$529, DW_AT_low_pc(0x00)
	.dwattr $C$DW$529, DW_AT_name("_McBSP_setRxSRGClockSource")
	.dwattr $C$DW$529, DW_AT_TI_call
        LCR       #_McBSP_setRxSRGClockSource ; [CPU_] |1583| 
        ; call occurs [#_McBSP_setRxSRGClockSource] ; [] |1583| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1588,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1588| 
        MOVB      XAR4,#1               ; [CPU_] |1588| 
$C$DW$530	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$530, DW_AT_low_pc(0x00)
	.dwattr $C$DW$530, DW_AT_name("_McBSP_setSRGDataClockDivider")
	.dwattr $C$DW$530, DW_AT_TI_call
        LCR       #_McBSP_setSRGDataClockDivider ; [CPU_] |1588| 
        ; call occurs [#_McBSP_setSRGDataClockDivider] ; [] |1588| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1594,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1594| 
        MOVB      XAR4,#0               ; [CPU_] |1594| 
$C$DW$531	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$531, DW_AT_low_pc(0x00)
	.dwattr $C$DW$531, DW_AT_name("_McBSP_setTxFrameSyncSource")
	.dwattr $C$DW$531, DW_AT_TI_call
        LCR       #_McBSP_setTxFrameSyncSource ; [CPU_] |1594| 
        ; call occurs [#_McBSP_setTxFrameSyncSource] ; [] |1594| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1599,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1599| 
        MOVB      XAR4,#8               ; [CPU_] |1599| 
$C$DW$532	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$532, DW_AT_low_pc(0x00)
	.dwattr $C$DW$532, DW_AT_name("_McBSP_setTxFrameSyncPolarity")
	.dwattr $C$DW$532, DW_AT_TI_call
        LCR       #_McBSP_setTxFrameSyncPolarity ; [CPU_] |1599| 
        ; call occurs [#_McBSP_setTxFrameSyncPolarity] ; [] |1599| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1605,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1605| 
$C$DW$533	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$533, DW_AT_low_pc(0x00)
	.dwattr $C$DW$533, DW_AT_name("_McBSP_disableTwoPhaseTx")
	.dwattr $C$DW$533, DW_AT_TI_call
        LCR       #_McBSP_disableTwoPhaseTx ; [CPU_] |1605| 
        ; call occurs [#_McBSP_disableTwoPhaseTx] ; [] |1605| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1610,column 9,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |1610| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1610| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1610| 
        MOVZ      AR5,*+XAR4[2]         ; [CPU_] |1610| 
        MOVB      XAR4,#0               ; [CPU_] |1610| 
$C$DW$534	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$534, DW_AT_low_pc(0x00)
	.dwattr $C$DW$534, DW_AT_name("_McBSP_setTxDataSize")
	.dwattr $C$DW$534, DW_AT_TI_call
        LCR       #_McBSP_setTxDataSize ; [CPU_] |1610| 
        ; call occurs [#_McBSP_setTxDataSize] ; [] |1610| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1612,column 9,is_stmt
        MOV       *-SP[1],#0            ; [CPU_] |1612| 
        MOVL      XAR4,*-SP[6]          ; [CPU_] |1612| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |1612| 
        MOVZ      AR5,*+XAR4[2]         ; [CPU_] |1612| 
        MOVB      XAR4,#0               ; [CPU_] |1612| 
$C$DW$535	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$535, DW_AT_low_pc(0x00)
	.dwattr $C$DW$535, DW_AT_name("_McBSP_setRxDataSize")
	.dwattr $C$DW$535, DW_AT_TI_call
        LCR       #_McBSP_setRxDataSize ; [CPU_] |1612| 
        ; call occurs [#_McBSP_setRxDataSize] ; [] |1612| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1618,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1618| 
        MOVB      XAR4,#0               ; [CPU_] |1618| 
$C$DW$536	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$536, DW_AT_low_pc(0x00)
	.dwattr $C$DW$536, DW_AT_name("_McBSP_setTxDataDelayBits")
	.dwattr $C$DW$536, DW_AT_TI_call
        LCR       #_McBSP_setTxDataDelayBits ; [CPU_] |1618| 
        ; call occurs [#_McBSP_setTxDataDelayBits] ; [] |1618| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1619,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |1619| 
        MOVB      XAR4,#0               ; [CPU_] |1619| 
$C$DW$537	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$537, DW_AT_low_pc(0x00)
	.dwattr $C$DW$537, DW_AT_name("_McBSP_setRxDataDelayBits")
	.dwattr $C$DW$537, DW_AT_TI_call
        LCR       #_McBSP_setRxDataDelayBits ; [CPU_] |1619| 
        ; call occurs [#_McBSP_setRxDataDelayBits] ; [] |1619| 
	.dwpsn	file "../ExtraData/driverlib2/mcbsp.c",line 1621,column 1,is_stmt
$C$L83:    
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$538	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$538, DW_AT_low_pc(0x00)
	.dwattr $C$DW$538, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$519, DW_AT_TI_end_file("../ExtraData/driverlib2/mcbsp.c")
	.dwattr $C$DW$519, DW_AT_TI_end_line(0x655)
	.dwattr $C$DW$519, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$519


;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$539	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_EXTERNAL_TX_CLOCK_SOURCE"), DW_AT_const_value(0x00)
$C$DW$540	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_INTERNAL_TX_CLOCK_SOURCE"), DW_AT_const_value(0x200)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxClockSource")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)
$C$DW$541	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$22)
$C$DW$T$91	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$541)

$C$DW$T$23	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$542	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_EXTERNAL_RX_CLOCK_SOURCE"), DW_AT_const_value(0x00)
$C$DW$543	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_INTERNAL_RX_CLOCK_SOURCE"), DW_AT_const_value(0x100)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxClockSource")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$544	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$24)
$C$DW$T$92	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$544)

$C$DW$T$25	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$545	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_SRG_RX_CLOCK_SOURCE_LSPCLK"), DW_AT_const_value(0x01)
$C$DW$546	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_SRG_RX_CLOCK_SOURCE_MCLKX_PIN"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$25

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_SRGRxClockSource")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$547	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$26)
$C$DW$T$80	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$547)

$C$DW$T$27	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$548	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_SRG_TX_CLOCK_SOURCE_LSPCLK"), DW_AT_const_value(0x01)
$C$DW$549	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_SRG_TX_CLOCK_SOURCE_MCLKR_PIN"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$27

$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_SRGTxClockSource")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$550	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$28)
$C$DW$T$81	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$550)

$C$DW$T$29	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$551	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_POLARITY_RISING_EDGE"), DW_AT_const_value(0x00)
$C$DW$552	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_POLARITY_FALLING_EDGE"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$29

$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxClockPolarity")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$553	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$30)
$C$DW$T$95	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$553)

$C$DW$T$31	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$554	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_POLARITY_FALLING_EDGE"), DW_AT_const_value(0x00)
$C$DW$555	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_POLARITY_RISING_EDGE"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$31

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxClockPolarity")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$556	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$32)
$C$DW$T$96	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$556)

$C$DW$T$34	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x01)
$C$DW$557	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_EXTERNAL_FRAME_SYNC_SOURCE"), DW_AT_const_value(0x00)
$C$DW$558	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_INTERNAL_FRAME_SYNC_SOURCE"), DW_AT_const_value(0x800)
	.dwendtag $C$DW$T$34

$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxFrameSyncSource")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)
$C$DW$559	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$35)
$C$DW$T$89	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$559)

$C$DW$T$36	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x01)
$C$DW$560	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_INTERNAL_FRAME_SYNC_DATA"), DW_AT_const_value(0x00)
$C$DW$561	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_INTERNAL_FRAME_SYNC_SRG"), DW_AT_const_value(0x1000)
	.dwendtag $C$DW$T$36

$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxInternalFrameSyncSource")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$562	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$37)
$C$DW$T$82	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$562)

$C$DW$T$38	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x01)
$C$DW$563	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_FRAME_SYNC_POLARITY_HIGH"), DW_AT_const_value(0x00)
$C$DW$564	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_FRAME_SYNC_POLARITY_LOW"), DW_AT_const_value(0x08)
	.dwendtag $C$DW$T$38

$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxFrameSyncPolarity")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$565	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$39)
$C$DW$T$93	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$565)

$C$DW$T$41	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$41, DW_AT_byte_size(0x01)
$C$DW$566	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_EXTERNAL_FRAME_SYNC_SOURCE"), DW_AT_const_value(0x00)
$C$DW$567	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_INTERNAL_FRAME_SYNC_SOURCE"), DW_AT_const_value(0x400)
	.dwendtag $C$DW$T$41

$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxFrameSyncSource")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$568	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$42)
$C$DW$T$90	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$568)

$C$DW$T$43	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x01)
$C$DW$569	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_FRAME_SYNC_POLARITY_HIGH"), DW_AT_const_value(0x00)
$C$DW$570	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_FRAME_SYNC_POLARITY_LOW"), DW_AT_const_value(0x04)
	.dwendtag $C$DW$T$43

$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxFrameSyncPolarity")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$571	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$44)
$C$DW$T$94	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$571)

$C$DW$T$46	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$46, DW_AT_byte_size(0x01)
$C$DW$572	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_CLOCK_MCBSP_MODE"), DW_AT_const_value(0x00)
$C$DW$573	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_CLOCK_SPI_MODE_NO_DELAY"), DW_AT_const_value(0x1000)
$C$DW$574	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_CLOCK_SPI_MODE_DELAY"), DW_AT_const_value(0x1800)
	.dwendtag $C$DW$T$46

$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_ClockStopMode")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$575	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$47)
$C$DW$T$75	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$575)

$C$DW$T$48	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x01)
$C$DW$576	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_BITS_PER_WORD_8"), DW_AT_const_value(0x00)
$C$DW$577	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_BITS_PER_WORD_12"), DW_AT_const_value(0x20)
$C$DW$578	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_BITS_PER_WORD_16"), DW_AT_const_value(0x40)
$C$DW$579	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_BITS_PER_WORD_20"), DW_AT_const_value(0x60)
$C$DW$580	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_BITS_PER_WORD_24"), DW_AT_const_value(0x80)
$C$DW$581	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_BITS_PER_WORD_32"), DW_AT_const_value(0xa0)
	.dwendtag $C$DW$T$48

$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_DataBitsPerWord")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$582	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$49)
$C$DW$T$100	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$582)

$C$DW$T$50	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x01)
$C$DW$583	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_COMPANDING_NONE"), DW_AT_const_value(0x00)
$C$DW$584	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_COMPANDING_NONE_LSB_FIRST"), DW_AT_const_value(0x08)
$C$DW$585	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_COMPANDING_U_LAW_SET"), DW_AT_const_value(0x10)
$C$DW$586	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_COMPANDING_A_LAW_SET"), DW_AT_const_value(0x18)
	.dwendtag $C$DW$T$50

$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_CompandingMode")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$587	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$51)
$C$DW$T$78	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$587)

$C$DW$T$52	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x01)
$C$DW$588	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_DATA_DELAY_BIT_0"), DW_AT_const_value(0x00)
$C$DW$589	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_DATA_DELAY_BIT_1"), DW_AT_const_value(0x01)
$C$DW$590	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_DATA_DELAY_BIT_2"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$52

$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_DataDelayBits")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
$C$DW$591	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$53)
$C$DW$T$79	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$591)

$C$DW$T$54	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x01)
$C$DW$592	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_ISR_SOURCE_TX_READY"), DW_AT_const_value(0x00)
$C$DW$593	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_ISR_SOURCE_END_OF_BLOCK"), DW_AT_const_value(0x10)
$C$DW$594	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_ISR_SOURCE_FRAME_SYNC"), DW_AT_const_value(0x20)
$C$DW$595	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_ISR_SOURCE_SYNC_ERROR"), DW_AT_const_value(0x30)
	.dwendtag $C$DW$T$54

$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxInterruptSource")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
$C$DW$596	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$55)
$C$DW$T$77	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$596)

$C$DW$T$57	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x01)
$C$DW$597	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RIGHT_JUSTIFY_FILL_ZERO"), DW_AT_const_value(0x00)
$C$DW$598	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RIGHT_JUSTIFY_FILL_SIGN"), DW_AT_const_value(0x2000)
$C$DW$599	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_LEFT_JUSTIFY_FILL_ZER0"), DW_AT_const_value(0x4000)
	.dwendtag $C$DW$T$57

$C$DW$T$58	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxSignExtensionMode")
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_language(DW_LANG_C)
$C$DW$600	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$58)
$C$DW$T$74	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$600)

$C$DW$T$59	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x01)
$C$DW$601	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_ISR_SOURCE_SERIAL_WORD"), DW_AT_const_value(0x00)
$C$DW$602	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_ISR_SOURCE_END_OF_BLOCK"), DW_AT_const_value(0x10)
$C$DW$603	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_ISR_SOURCE_FRAME_SYNC"), DW_AT_const_value(0x20)
$C$DW$604	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_ISR_SOURCE_SYNC_ERROR"), DW_AT_const_value(0x30)
	.dwendtag $C$DW$T$59

$C$DW$T$60	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxInterruptSource")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$T$60, DW_AT_language(DW_LANG_C)
$C$DW$605	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$60)
$C$DW$T$76	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$605)

$C$DW$T$63	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x01)
$C$DW$606	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_ALL_TX_CHANNELS_ENABLED"), DW_AT_const_value(0x00)
$C$DW$607	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_TX_CHANNEL_SELECTION_ENABLED"), DW_AT_const_value(0x01)
$C$DW$608	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_ENABLE_MASKED_TX_CHANNEL_SELECTION"), DW_AT_const_value(0x02)
$C$DW$609	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_SYMMERTIC_RX_TX_SELECTION"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$63

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxChannelMode")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)
$C$DW$610	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$64)
$C$DW$T$88	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$610)

$C$DW$T$65	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x01)
$C$DW$611	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_MULTICHANNEL_TWO_PARTITION"), DW_AT_const_value(0x00)
$C$DW$612	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_MULTICHANNEL_EIGHT_PARTITION"), DW_AT_const_value(0x200)
	.dwendtag $C$DW$T$65

$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_MultichannelPartition")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)
$C$DW$613	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$66)
$C$DW$T$83	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$613)

$C$DW$T$68	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x01)
$C$DW$614	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_ALL_RX_CHANNELS_ENABLED"), DW_AT_const_value(0x00)
$C$DW$615	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_RX_CHANNEL_SELECTION_ENABLED"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$68

$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxChannelMode")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$616	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$69)
$C$DW$T$87	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$616)

$C$DW$T$84	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$84, DW_AT_byte_size(0x01)
$C$DW$617	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_0"), DW_AT_const_value(0x00)
$C$DW$618	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_1"), DW_AT_const_value(0x01)
$C$DW$619	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_2"), DW_AT_const_value(0x02)
$C$DW$620	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_3"), DW_AT_const_value(0x03)
$C$DW$621	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_4"), DW_AT_const_value(0x04)
$C$DW$622	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_5"), DW_AT_const_value(0x05)
$C$DW$623	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_6"), DW_AT_const_value(0x06)
$C$DW$624	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PARTITION_BLOCK_7"), DW_AT_const_value(0x07)
	.dwendtag $C$DW$T$84

$C$DW$T$85	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_PartitionBlock")
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_language(DW_LANG_C)
$C$DW$625	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$85)
$C$DW$T$86	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$625)

$C$DW$T$97	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$97, DW_AT_byte_size(0x01)
$C$DW$626	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PHASE_ONE_FRAME"), DW_AT_const_value(0x00)
$C$DW$627	.dwtag  DW_TAG_enumerator, DW_AT_name("MCBSP_PHASE_TWO_FRAME"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$97

$C$DW$T$98	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_DataPhaseFrame")
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)
$C$DW$628	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$98)
$C$DW$T$99	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$628)

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x08)
$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$629, DW_AT_name("clockSRGSyncFlag")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_clockSRGSyncFlag")
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$630, DW_AT_name("clockSRGDivider")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_clockSRGDivider")
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$631, DW_AT_name("clockSourceTx")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_clockSourceTx")
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$632, DW_AT_name("clockSourceRx")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_clockSourceRx")
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$633, DW_AT_name("clockRxSRGSource")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_clockRxSRGSource")
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$634, DW_AT_name("clockTxSRGSource")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_clockTxSRGSource")
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$635, DW_AT_name("clockMCLKXPolarity")
	.dwattr $C$DW$635, DW_AT_TI_symbol_name("_clockMCLKXPolarity")
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$636, DW_AT_name("clockMCLKRPolarity")
	.dwattr $C$DW$636, DW_AT_TI_symbol_name("_clockMCLKRPolarity")
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33

$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_ClockParams")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$637	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$101)
$C$DW$T$102	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$637)
$C$DW$T$103	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$103, DW_AT_address_class(0x16)

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x07)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$638, DW_AT_name("syncSRGSyncFSRFlag")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_syncSRGSyncFSRFlag")
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$639, DW_AT_name("syncErrorDetect")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_syncErrorDetect")
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$640, DW_AT_name("syncClockDivider")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_syncClockDivider")
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$641, DW_AT_name("syncPulseDivider")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_syncPulseDivider")
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$642, DW_AT_name("syncSourceTx")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_syncSourceTx")
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$643, DW_AT_name("syncIntSource")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_syncIntSource")
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$644, DW_AT_name("syncFSXPolarity")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_syncFSXPolarity")
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$40

$C$DW$T$104	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxFsyncParams")
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$104, DW_AT_language(DW_LANG_C)
$C$DW$645	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$104)
$C$DW$T$105	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$645)
$C$DW$T$106	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$106, DW_AT_address_class(0x16)

$C$DW$T$45	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x06)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$646, DW_AT_name("syncSRGSyncFSRFlag")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_syncSRGSyncFSRFlag")
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$647, DW_AT_name("syncErrorDetect")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_syncErrorDetect")
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$648, DW_AT_name("syncClockDivider")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_syncClockDivider")
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$649, DW_AT_name("syncPulseDivider")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_syncPulseDivider")
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$650, DW_AT_name("syncSourceRx")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_syncSourceRx")
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$651, DW_AT_name("syncFSRPolarity")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_syncFSRPolarity")
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$45

$C$DW$T$107	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxFsyncParams")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)
$C$DW$652	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$107)
$C$DW$T$108	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$652)
$C$DW$T$109	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_address_class(0x16)

$C$DW$T$56	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x0b)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$653, DW_AT_name("loopbackModeFlag")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_loopbackModeFlag")
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$654, DW_AT_name("twoPhaseModeFlag")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_twoPhaseModeFlag")
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$655, DW_AT_name("pinDelayEnableFlag")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_pinDelayEnableFlag")
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$656, DW_AT_name("phase1FrameLength")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_phase1FrameLength")
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$657, DW_AT_name("phase2FrameLength")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_phase2FrameLength")
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$658, DW_AT_name("clockStopMode")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_clockStopMode")
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$659, DW_AT_name("phase1WordLength")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_phase1WordLength")
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$660, DW_AT_name("phase2WordLength")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_phase2WordLength")
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$661, DW_AT_name("compandingMode")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_compandingMode")
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$662, DW_AT_name("dataDelayBits")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_dataDelayBits")
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$663, DW_AT_name("interruptMode")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_interruptMode")
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56

$C$DW$T$110	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxDataParams")
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
$C$DW$664	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$110)
$C$DW$T$111	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$664)
$C$DW$T$112	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$112, DW_AT_address_class(0x16)

$C$DW$T$61	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x0b)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$665, DW_AT_name("loopbackModeFlag")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_loopbackModeFlag")
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$666, DW_AT_name("twoPhaseModeFlag")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_twoPhaseModeFlag")
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$667, DW_AT_name("phase1FrameLength")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_phase1FrameLength")
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$668, DW_AT_name("phase2FrameLength")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_phase2FrameLength")
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$669, DW_AT_name("clockStopMode")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_clockStopMode")
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$670, DW_AT_name("phase1WordLength")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_phase1WordLength")
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$671, DW_AT_name("phase2WordLength")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_phase2WordLength")
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$672, DW_AT_name("compandingMode")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_compandingMode")
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$673, DW_AT_name("dataDelayBits")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_dataDelayBits")
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$674, DW_AT_name("signExtMode")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_signExtMode")
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$675, DW_AT_name("interruptMode")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_interruptMode")
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61

$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxDataParams")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)
$C$DW$676	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$113)
$C$DW$T$114	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$676)
$C$DW$T$115	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x16)

$C$DW$T$67	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x06)
$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$677, DW_AT_name("channelCountTx")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_channelCountTx")
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$678, DW_AT_name("ptrChannelsListTx")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_ptrChannelsListTx")
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$679, DW_AT_name("multichannelModeTx")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_multichannelModeTx")
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$680, DW_AT_name("partitionTx")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_partitionTx")
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$67

$C$DW$T$116	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_TxMultichannelParams")
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$116, DW_AT_language(DW_LANG_C)
$C$DW$681	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$116)
$C$DW$T$117	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$681)
$C$DW$T$118	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x16)

$C$DW$T$70	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$70, DW_AT_byte_size(0x06)
$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$682, DW_AT_name("channelCountRx")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_channelCountRx")
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$683, DW_AT_name("ptrChannelsListRx")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_ptrChannelsListRx")
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$684, DW_AT_name("multichannelModeRx")
	.dwattr $C$DW$684, DW_AT_TI_symbol_name("_multichannelModeRx")
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$685, DW_AT_name("partitionRx")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_partitionRx")
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$70

$C$DW$T$119	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_RxMultichannelParams")
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$119, DW_AT_language(DW_LANG_C)
$C$DW$686	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$119)
$C$DW$T$120	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$686)
$C$DW$T$121	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$T$121, DW_AT_address_class(0x16)

$C$DW$T$72	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x08)
$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$687, DW_AT_name("loopbackModeFlag")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_loopbackModeFlag")
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$688, DW_AT_name("clockSRGDivider")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_clockSRGDivider")
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$689, DW_AT_name("clockStopMode")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_clockStopMode")
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$690, DW_AT_name("wordLength")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_wordLength")
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$691, DW_AT_name("spiMode")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_spiMode")
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$72

$C$DW$T$122	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_SPIMasterModeParams")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$122, DW_AT_language(DW_LANG_C)
$C$DW$692	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$122)
$C$DW$T$123	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$692)
$C$DW$T$124	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$124, DW_AT_address_class(0x16)

$C$DW$T$73	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x04)
$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$693, DW_AT_name("loopbackModeFlag")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_loopbackModeFlag")
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$694, DW_AT_name("clockStopMode")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_clockStopMode")
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$695, DW_AT_name("wordLength")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_wordLength")
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$696, DW_AT_name("spiMode")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_spiMode")
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$73

$C$DW$T$125	.dwtag  DW_TAG_typedef, DW_AT_name("McBSP_SPISlaveModeParams")
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$125, DW_AT_language(DW_LANG_C)
$C$DW$697	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$125)
$C$DW$T$126	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$697)
$C$DW$T$127	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$T$127, DW_AT_address_class(0x16)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$T$62	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$62, DW_AT_address_class(0x16)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)
$C$DW$T$177	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$177, DW_AT_address_class(0x16)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$698	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$698, DW_AT_location[DW_OP_reg0]
$C$DW$699	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$699, DW_AT_location[DW_OP_reg1]
$C$DW$700	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$700, DW_AT_location[DW_OP_reg2]
$C$DW$701	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$701, DW_AT_location[DW_OP_reg3]
$C$DW$702	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$702, DW_AT_location[DW_OP_reg20]
$C$DW$703	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$703, DW_AT_location[DW_OP_reg21]
$C$DW$704	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$704, DW_AT_location[DW_OP_reg22]
$C$DW$705	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$705, DW_AT_location[DW_OP_reg23]
$C$DW$706	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$706, DW_AT_location[DW_OP_reg24]
$C$DW$707	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$707, DW_AT_location[DW_OP_reg25]
$C$DW$708	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$708, DW_AT_location[DW_OP_reg26]
$C$DW$709	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$709, DW_AT_location[DW_OP_reg28]
$C$DW$710	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$710, DW_AT_location[DW_OP_reg29]
$C$DW$711	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$711, DW_AT_location[DW_OP_reg30]
$C$DW$712	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$712, DW_AT_location[DW_OP_reg31]
$C$DW$713	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$713, DW_AT_location[DW_OP_regx 0x20]
$C$DW$714	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$714, DW_AT_location[DW_OP_regx 0x21]
$C$DW$715	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$715, DW_AT_location[DW_OP_regx 0x22]
$C$DW$716	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$716, DW_AT_location[DW_OP_regx 0x23]
$C$DW$717	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$717, DW_AT_location[DW_OP_regx 0x24]
$C$DW$718	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$718, DW_AT_location[DW_OP_regx 0x25]
$C$DW$719	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$719, DW_AT_location[DW_OP_regx 0x26]
$C$DW$720	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$720, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$721	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$721, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$722	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$722, DW_AT_location[DW_OP_reg4]
$C$DW$723	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$723, DW_AT_location[DW_OP_reg6]
$C$DW$724	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$724, DW_AT_location[DW_OP_reg8]
$C$DW$725	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$725, DW_AT_location[DW_OP_reg10]
$C$DW$726	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$726, DW_AT_location[DW_OP_reg12]
$C$DW$727	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$727, DW_AT_location[DW_OP_reg14]
$C$DW$728	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$728, DW_AT_location[DW_OP_reg16]
$C$DW$729	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$729, DW_AT_location[DW_OP_reg17]
$C$DW$730	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$730, DW_AT_location[DW_OP_reg18]
$C$DW$731	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$731, DW_AT_location[DW_OP_reg19]
$C$DW$732	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$732, DW_AT_location[DW_OP_reg5]
$C$DW$733	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$733, DW_AT_location[DW_OP_reg7]
$C$DW$734	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$734, DW_AT_location[DW_OP_reg9]
$C$DW$735	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$735, DW_AT_location[DW_OP_reg11]
$C$DW$736	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$736, DW_AT_location[DW_OP_reg13]
$C$DW$737	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$737, DW_AT_location[DW_OP_reg15]
$C$DW$738	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$738, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$739	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$739, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$740	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$740, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$741	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$741, DW_AT_location[DW_OP_regx 0x30]
$C$DW$742	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$742, DW_AT_location[DW_OP_regx 0x33]
$C$DW$743	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$743, DW_AT_location[DW_OP_regx 0x34]
$C$DW$744	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$744, DW_AT_location[DW_OP_regx 0x37]
$C$DW$745	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$745, DW_AT_location[DW_OP_regx 0x38]
$C$DW$746	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$746, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$747	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$747, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$748	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$748, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$749	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$749, DW_AT_location[DW_OP_regx 0x40]
$C$DW$750	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$750, DW_AT_location[DW_OP_regx 0x43]
$C$DW$751	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$751, DW_AT_location[DW_OP_regx 0x44]
$C$DW$752	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$752, DW_AT_location[DW_OP_regx 0x47]
$C$DW$753	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$753, DW_AT_location[DW_OP_regx 0x48]
$C$DW$754	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$754, DW_AT_location[DW_OP_regx 0x49]
$C$DW$755	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$755, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$756	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$756, DW_AT_location[DW_OP_regx 0x27]
$C$DW$757	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$757, DW_AT_location[DW_OP_regx 0x28]
$C$DW$758	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$758, DW_AT_location[DW_OP_reg27]
$C$DW$759	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$759, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

