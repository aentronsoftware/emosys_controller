;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:03 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2344012 
	.sect	".text"
	.clink

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_getRAMZone")
	.dwattr $C$DW$3, DW_AT_low_pc(_DCSM_getRAMZone)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_DCSM_getRAMZone")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$3, DW_AT_TI_begin_file("..\ExtraData\driverlib2\dcsm.h")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x19c)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 413,column 1,is_stmt,address _DCSM_getRAMZone

	.dwfde $C$DW$CIE, _DCSM_getRAMZone
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("module")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_module")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DCSM_getRAMZone              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_DCSM_getRAMZone:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$5	.dwtag  DW_TAG_variable, DW_AT_name("module")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_module")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_breg20 -1]
$C$DW$6	.dwtag  DW_TAG_variable, DW_AT_name("shift")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_shift")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[1],AL            ; [CPU_] |413| 
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 414,column 20,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |414| 
        MOV       *-SP[2],AL            ; [CPU_] |414| 
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 418,column 5,is_stmt
        MOVL      XAR4,#389236          ; [CPU_U] |418| 
        MOV       T,*-SP[2]             ; [CPU_] |418| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |418| 
        LSRL      ACC,T                 ; [CPU_] |418| 
        ANDB      AL,#0x03              ; [CPU_] |418| 
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 421,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$7	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$7, DW_AT_low_pc(0x00)
	.dwattr $C$DW$7, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("..\ExtraData\driverlib2\dcsm.h")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x1a5)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink

$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_getFlashSectorZone")
	.dwattr $C$DW$8, DW_AT_low_pc(_DCSM_getFlashSectorZone)
	.dwattr $C$DW$8, DW_AT_high_pc(0x00)
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_DCSM_getFlashSectorZone")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$8, DW_AT_TI_begin_file("..\ExtraData\driverlib2\dcsm.h")
	.dwattr $C$DW$8, DW_AT_TI_begin_line(0x1b6)
	.dwattr $C$DW$8, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$8, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 439,column 1,is_stmt,address _DCSM_getFlashSectorZone

	.dwfde $C$DW$CIE, _DCSM_getFlashSectorZone
$C$DW$9	.dwtag  DW_TAG_formal_parameter, DW_AT_name("sector")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_sector")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DCSM_getFlashSectorZone      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  5 Auto,  0 SOE     *
;***************************************************************

_DCSM_getFlashSectorZone:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("sector")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_sector")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_breg20 -1]
$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("sectStat")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_sectStat")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_breg20 -4]
$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("shift")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_shift")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_breg20 -5]
        MOV       *-SP[1],AL            ; [CPU_] |439| 
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 446,column 5,is_stmt
        MOVL      XAR4,#389234          ; [CPU_U] |446| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |446| 
        MOVL      *-SP[4],ACC           ; [CPU_] |446| 
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 447,column 5,is_stmt
        MOV       ACC,*-SP[1] << #1     ; [CPU_] |447| 
        MOV       *-SP[5],AL            ; [CPU_] |447| 
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 452,column 5,is_stmt
        MOV       T,*-SP[5]             ; [CPU_] |452| 
        MOVL      ACC,*-SP[4]           ; [CPU_] |452| 
        LSRL      ACC,T                 ; [CPU_] |452| 
        ANDB      AL,#0x03              ; [CPU_] |452| 
	.dwpsn	file "..\ExtraData\driverlib2\dcsm.h",line 453,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$13	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$13, DW_AT_low_pc(0x00)
	.dwattr $C$DW$13, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$8, DW_AT_TI_end_file("..\ExtraData\driverlib2\dcsm.h")
	.dwattr $C$DW$8, DW_AT_TI_end_line(0x1c5)
	.dwattr $C$DW$8, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$8

	.sect	".text"
	.clink
	.global	_DCSM_unlockZone1CSM

$C$DW$14	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_unlockZone1CSM")
	.dwattr $C$DW$14, DW_AT_low_pc(_DCSM_unlockZone1CSM)
	.dwattr $C$DW$14, DW_AT_high_pc(0x00)
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_DCSM_unlockZone1CSM")
	.dwattr $C$DW$14, DW_AT_external
	.dwattr $C$DW$14, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$14, DW_AT_TI_begin_line(0x33)
	.dwattr $C$DW$14, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$14, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 52,column 1,is_stmt,address _DCSM_unlockZone1CSM

	.dwfde $C$DW$CIE, _DCSM_unlockZone1CSM
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("psCMDKey")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_psCMDKey")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _DCSM_unlockZone1CSM          FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_DCSM_unlockZone1CSM:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$16	.dwtag  DW_TAG_variable, DW_AT_name("psCMDKey")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_psCMDKey")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -2]
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("linkPointer")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_linkPointer")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -4]
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("zsbBase")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_zsbBase")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -6]
$C$DW$19	.dwtag  DW_TAG_variable, DW_AT_name("bitPos")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_bitPos")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_breg20 -8]
$C$DW$20	.dwtag  DW_TAG_variable, DW_AT_name("zeroFound")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_zeroFound")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_breg20 -10]
        MOVL      *-SP[2],XAR4          ; [CPU_] |52| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 54,column 23,is_stmt
        MOVL      XAR4,#491552          ; [CPU_U] |54| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |54| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 55,column 20,is_stmt
        MOVB      ACC,#28               ; [CPU_] |55| 
        MOVL      *-SP[8],ACC           ; [CPU_] |55| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 56,column 23,is_stmt
        MOVB      ACC,#0                ; [CPU_] |56| 
        MOVL      *-SP[10],ACC          ; [CPU_] |56| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 63,column 5,is_stmt
        MOVL      XAR4,#389120          ; [CPU_U] |63| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |63| 
        MOVL      *-SP[4],ACC           ; [CPU_] |63| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 68,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |68| 
        LSL       ACC,3                 ; [CPU_] |68| 
        MOVL      *-SP[4],ACC           ; [CPU_] |68| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 74,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |80| 
        B         $C$L3,UNC             ; [CPU_] |74| 
        ; branch occurs ; [] |74| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 80,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |80| 
        AND       ACC,#32768 << 16      ; [CPU_] |80| 
        CMPL      ACC,XAR6              ; [CPU_] |80| 
        BF        $C$L2,NEQ             ; [CPU_] |80| 
        ; branchcc occurs ; [] |80| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 82,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |82| 
        MOVL      *-SP[10],ACC          ; [CPU_] |82| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 88,column 13,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |88| 
        LSL       ACC,4                 ; [CPU_] |88| 
        ADD       ACC,#30723 << 4       ; [CPU_] |88| 
        MOVL      *-SP[6],ACC           ; [CPU_] |88| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 89,column 9,is_stmt
        B         $C$L3,UNC             ; [CPU_] |89| 
        ; branch occurs ; [] |89| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 96,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |96| 
        SUBL      *-SP[8],ACC           ; [CPU_] |96| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 97,column 13,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |97| 
        LSL       ACC,1                 ; [CPU_] |97| 
        MOVL      *-SP[4],ACC           ; [CPU_] |97| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 74,column 11,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |74| 
        BF        $C$L4,NEQ             ; [CPU_] |74| 
        ; branchcc occurs ; [] |74| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |74| 
        B         $C$L1,GEQ             ; [CPU_] |74| 
        ; branchcc occurs ; [] |74| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 105,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |105| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |105| 
        MOVL      XAR4,ACC              ; [CPU_] |105| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |105| 
        MOVL      *-SP[4],ACC           ; [CPU_] |105| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 106,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |106| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |106| 
        MOVL      XAR4,ACC              ; [CPU_] |106| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |106| 
        MOVL      *-SP[4],ACC           ; [CPU_] |106| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 107,column 5,is_stmt
        MOVB      ACC,#12               ; [CPU_] |107| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |107| 
        MOVL      XAR4,ACC              ; [CPU_] |107| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |107| 
        MOVL      *-SP[4],ACC           ; [CPU_] |107| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 108,column 5,is_stmt
        MOVB      ACC,#14               ; [CPU_] |108| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |108| 
        MOVL      XAR4,ACC              ; [CPU_] |108| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |108| 
        MOVL      *-SP[4],ACC           ; [CPU_] |108| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 110,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |110| 
        BF        $C$L5,EQ              ; [CPU_] |110| 
        ; branchcc occurs ; [] |110| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 112,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |112| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |112| 
        MOVL      XAR4,#389136          ; [CPU_U] |112| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |112| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 113,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |113| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |113| 
        MOVL      XAR4,#389138          ; [CPU_U] |113| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |113| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 114,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |114| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |114| 
        MOVL      XAR4,#389140          ; [CPU_U] |114| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |114| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 115,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |115| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |115| 
        MOVL      XAR4,#389142          ; [CPU_U] |115| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |115| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 117,column 1,is_stmt
$C$L5:    
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$14, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$14, DW_AT_TI_end_line(0x75)
	.dwattr $C$DW$14, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$14

	.sect	".text"
	.clink
	.global	_DCSM_unlockZone2CSM

$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_unlockZone2CSM")
	.dwattr $C$DW$22, DW_AT_low_pc(_DCSM_unlockZone2CSM)
	.dwattr $C$DW$22, DW_AT_high_pc(0x00)
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_DCSM_unlockZone2CSM")
	.dwattr $C$DW$22, DW_AT_external
	.dwattr $C$DW$22, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$22, DW_AT_TI_begin_line(0x7d)
	.dwattr $C$DW$22, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$22, DW_AT_TI_max_frame_size(-12)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 126,column 1,is_stmt,address _DCSM_unlockZone2CSM

	.dwfde $C$DW$CIE, _DCSM_unlockZone2CSM
$C$DW$23	.dwtag  DW_TAG_formal_parameter, DW_AT_name("psCMDKey")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_psCMDKey")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _DCSM_unlockZone2CSM          FR SIZE:  10           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 10 Auto,  0 SOE     *
;***************************************************************

_DCSM_unlockZone2CSM:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -12
$C$DW$24	.dwtag  DW_TAG_variable, DW_AT_name("psCMDKey")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_psCMDKey")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_breg20 -2]
$C$DW$25	.dwtag  DW_TAG_variable, DW_AT_name("linkPointer")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_linkPointer")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_breg20 -4]
$C$DW$26	.dwtag  DW_TAG_variable, DW_AT_name("zsbBase")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_zsbBase")
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$26, DW_AT_location[DW_OP_breg20 -6]
$C$DW$27	.dwtag  DW_TAG_variable, DW_AT_name("bitPos")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_bitPos")
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$27, DW_AT_location[DW_OP_breg20 -8]
$C$DW$28	.dwtag  DW_TAG_variable, DW_AT_name("zeroFound")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_zeroFound")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_breg20 -10]
        MOVL      *-SP[2],XAR4          ; [CPU_] |126| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 128,column 22,is_stmt
        MOVL      XAR4,#492064          ; [CPU_U] |128| 
        MOVL      *-SP[6],XAR4          ; [CPU_] |128| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 129,column 20,is_stmt
        MOVB      ACC,#28               ; [CPU_] |129| 
        MOVL      *-SP[8],ACC           ; [CPU_] |129| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 130,column 23,is_stmt
        MOVB      ACC,#0                ; [CPU_] |130| 
        MOVL      *-SP[10],ACC          ; [CPU_] |130| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 137,column 5,is_stmt
        MOVL      XAR4,#389184          ; [CPU_U] |137| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |137| 
        MOVL      *-SP[4],ACC           ; [CPU_] |137| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 142,column 5,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |142| 
        LSL       ACC,3                 ; [CPU_] |142| 
        MOVL      *-SP[4],ACC           ; [CPU_] |142| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 148,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |154| 
        B         $C$L8,UNC             ; [CPU_] |148| 
        ; branch occurs ; [] |148| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 154,column 9,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |154| 
        AND       ACC,#32768 << 16      ; [CPU_] |154| 
        CMPL      ACC,XAR6              ; [CPU_] |154| 
        BF        $C$L7,NEQ             ; [CPU_] |154| 
        ; branchcc occurs ; [] |154| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 156,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |156| 
        MOVL      *-SP[10],ACC          ; [CPU_] |156| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 162,column 13,is_stmt
        MOVL      ACC,*-SP[8]           ; [CPU_] |162| 
        LSL       ACC,4                 ; [CPU_] |162| 
        ADD       ACC,#30755 << 4       ; [CPU_] |162| 
        MOVL      *-SP[6],ACC           ; [CPU_] |162| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 163,column 9,is_stmt
        B         $C$L8,UNC             ; [CPU_] |163| 
        ; branch occurs ; [] |163| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 170,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |170| 
        SUBL      *-SP[8],ACC           ; [CPU_] |170| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 171,column 13,is_stmt
        MOVL      ACC,*-SP[4]           ; [CPU_] |171| 
        LSL       ACC,1                 ; [CPU_] |171| 
        MOVL      *-SP[4],ACC           ; [CPU_] |171| 
$C$L8:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 148,column 11,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |148| 
        BF        $C$L9,NEQ             ; [CPU_] |148| 
        ; branchcc occurs ; [] |148| 
        MOVL      ACC,*-SP[8]           ; [CPU_] |148| 
        B         $C$L6,GEQ             ; [CPU_] |148| 
        ; branchcc occurs ; [] |148| 
$C$L9:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 179,column 5,is_stmt
        MOVB      ACC,#8                ; [CPU_] |179| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |179| 
        MOVL      XAR4,ACC              ; [CPU_] |179| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |179| 
        MOVL      *-SP[4],ACC           ; [CPU_] |179| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 180,column 5,is_stmt
        MOVB      ACC,#10               ; [CPU_] |180| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |180| 
        MOVL      XAR4,ACC              ; [CPU_] |180| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |180| 
        MOVL      *-SP[4],ACC           ; [CPU_] |180| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 181,column 5,is_stmt
        MOVB      ACC,#12               ; [CPU_] |181| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |181| 
        MOVL      XAR4,ACC              ; [CPU_] |181| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |181| 
        MOVL      *-SP[4],ACC           ; [CPU_] |181| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 182,column 5,is_stmt
        MOVB      ACC,#14               ; [CPU_] |182| 
        ADDL      ACC,*-SP[6]           ; [CPU_] |182| 
        MOVL      XAR4,ACC              ; [CPU_] |182| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |182| 
        MOVL      *-SP[4],ACC           ; [CPU_] |182| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 184,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |184| 
        BF        $C$L10,EQ             ; [CPU_] |184| 
        ; branchcc occurs ; [] |184| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 186,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |186| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |186| 
        MOVL      XAR4,#389200          ; [CPU_U] |186| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |186| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 187,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |187| 
        MOVL      ACC,*+XAR4[2]         ; [CPU_] |187| 
        MOVL      XAR4,#389202          ; [CPU_U] |187| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |187| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 188,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |188| 
        MOVL      ACC,*+XAR4[4]         ; [CPU_] |188| 
        MOVL      XAR4,#389204          ; [CPU_U] |188| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |188| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 189,column 9,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |189| 
        MOVL      ACC,*+XAR4[6]         ; [CPU_] |189| 
        MOVL      XAR4,#389206          ; [CPU_U] |189| 
        MOVL      *+XAR4[0],ACC         ; [CPU_] |189| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 191,column 1,is_stmt
$C$L10:    
        SUBB      SP,#10                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$22, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$22, DW_AT_TI_end_line(0xbf)
	.dwattr $C$DW$22, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$22

	.sect	".text"
	.clink
	.global	_DCSM_getZone1FlashEXEStatus

$C$DW$30	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_getZone1FlashEXEStatus")
	.dwattr $C$DW$30, DW_AT_low_pc(_DCSM_getZone1FlashEXEStatus)
	.dwattr $C$DW$30, DW_AT_high_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("_DCSM_getZone1FlashEXEStatus")
	.dwattr $C$DW$30, DW_AT_external
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$30, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$30, DW_AT_TI_begin_line(0xc6)
	.dwattr $C$DW$30, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$30, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 199,column 1,is_stmt,address _DCSM_getZone1FlashEXEStatus

	.dwfde $C$DW$CIE, _DCSM_getZone1FlashEXEStatus
$C$DW$31	.dwtag  DW_TAG_formal_parameter, DW_AT_name("sector")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_sector")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DCSM_getZone1FlashEXEStatus  FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_DCSM_getZone1FlashEXEStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$32	.dwtag  DW_TAG_variable, DW_AT_name("sector")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_sector")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_breg20 -1]
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("regValue")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_regValue")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -2]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[1],AL            ; [CPU_] |199| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 206,column 5,is_stmt
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_DCSM_getFlashSectorZone")
	.dwattr $C$DW$35, DW_AT_TI_call
        LCR       #_DCSM_getFlashSectorZone ; [CPU_] |206| 
        ; call occurs [#_DCSM_getFlashSectorZone] ; [] |206| 
        CMPB      AL,#1                 ; [CPU_] |206| 
        BF        $C$L11,EQ             ; [CPU_] |206| 
        ; branchcc occurs ; [] |206| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 208,column 9,is_stmt
        MOVB      *-SP[3],#2,UNC        ; [CPU_] |208| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 209,column 5,is_stmt
        B         $C$L12,UNC            ; [CPU_] |209| 
        ; branch occurs ; [] |209| 
$C$L11:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 215,column 9,is_stmt
        MOVL      XAR4,#389150          ; [CPU_U] |215| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |215| 
        MOV       *-SP[2],AL            ; [CPU_] |215| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 220,column 9,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |220| 
        LSR       AL,T                  ; [CPU_] |220| 
        ANDB      AL,#0x01              ; [CPU_] |220| 
        MOV       *-SP[3],AL            ; [CPU_] |220| 
$C$L12:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 223,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |223| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 224,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$30, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$30, DW_AT_TI_end_line(0xe0)
	.dwattr $C$DW$30, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$30

	.sect	".text"
	.clink
	.global	_DCSM_getZone1RAMEXEStatus

$C$DW$37	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_getZone1RAMEXEStatus")
	.dwattr $C$DW$37, DW_AT_low_pc(_DCSM_getZone1RAMEXEStatus)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_DCSM_getZone1RAMEXEStatus")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$37, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$37, DW_AT_TI_begin_line(0xe8)
	.dwattr $C$DW$37, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 233,column 1,is_stmt,address _DCSM_getZone1RAMEXEStatus

	.dwfde $C$DW$CIE, _DCSM_getZone1RAMEXEStatus
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("module")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_module")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DCSM_getZone1RAMEXEStatus    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_DCSM_getZone1RAMEXEStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("module")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_module")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -1]
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[1],AL            ; [CPU_] |233| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 240,column 5,is_stmt
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("_DCSM_getRAMZone")
	.dwattr $C$DW$41, DW_AT_TI_call
        LCR       #_DCSM_getRAMZone     ; [CPU_] |240| 
        ; call occurs [#_DCSM_getRAMZone] ; [] |240| 
        CMPB      AL,#1                 ; [CPU_] |240| 
        BF        $C$L13,EQ             ; [CPU_] |240| 
        ; branchcc occurs ; [] |240| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 242,column 9,is_stmt
        MOVB      *-SP[2],#2,UNC        ; [CPU_] |242| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 243,column 5,is_stmt
        B         $C$L14,UNC            ; [CPU_] |243| 
        ; branch occurs ; [] |243| 
$C$L13:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 249,column 9,is_stmt
        MOVL      XAR4,#389152          ; [CPU_U] |249| 
        MOV       T,*-SP[1]             ; [CPU_] |249| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |249| 
        LSR       AL,T                  ; [CPU_] |249| 
        ANDB      AL,#0x01              ; [CPU_] |249| 
        MOV       *-SP[2],AL            ; [CPU_] |249| 
$C$L14:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 252,column 5,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |252| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 253,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$37, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0xfd)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

	.sect	".text"
	.clink
	.global	_DCSM_getZone2FlashEXEStatus

$C$DW$43	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_getZone2FlashEXEStatus")
	.dwattr $C$DW$43, DW_AT_low_pc(_DCSM_getZone2FlashEXEStatus)
	.dwattr $C$DW$43, DW_AT_high_pc(0x00)
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_DCSM_getZone2FlashEXEStatus")
	.dwattr $C$DW$43, DW_AT_external
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$43, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$43, DW_AT_TI_begin_line(0x105)
	.dwattr $C$DW$43, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$43, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 262,column 1,is_stmt,address _DCSM_getZone2FlashEXEStatus

	.dwfde $C$DW$CIE, _DCSM_getZone2FlashEXEStatus
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("sector")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_sector")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DCSM_getZone2FlashEXEStatus  FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_DCSM_getZone2FlashEXEStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("sector")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_sector")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_breg20 -1]
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("regValue")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_regValue")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -2]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -3]
        MOV       *-SP[1],AL            ; [CPU_] |262| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 269,column 5,is_stmt
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_name("_DCSM_getFlashSectorZone")
	.dwattr $C$DW$48, DW_AT_TI_call
        LCR       #_DCSM_getFlashSectorZone ; [CPU_] |269| 
        ; call occurs [#_DCSM_getFlashSectorZone] ; [] |269| 
        CMPB      AL,#2                 ; [CPU_] |269| 
        BF        $C$L15,EQ             ; [CPU_] |269| 
        ; branchcc occurs ; [] |269| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 271,column 9,is_stmt
        MOVB      *-SP[3],#2,UNC        ; [CPU_] |271| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 272,column 5,is_stmt
        B         $C$L16,UNC            ; [CPU_] |272| 
        ; branch occurs ; [] |272| 
$C$L15:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 278,column 9,is_stmt
        MOVL      XAR4,#389214          ; [CPU_U] |278| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |278| 
        MOV       *-SP[2],AL            ; [CPU_] |278| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 283,column 9,is_stmt
        MOV       T,*-SP[1]             ; [CPU_] |283| 
        LSR       AL,T                  ; [CPU_] |283| 
        ANDB      AL,#0x01              ; [CPU_] |283| 
        MOV       *-SP[3],AL            ; [CPU_] |283| 
$C$L16:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 287,column 5,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |287| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 288,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$43, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$43, DW_AT_TI_end_line(0x120)
	.dwattr $C$DW$43, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$43

	.sect	".text"
	.clink
	.global	_DCSM_getZone2RAMEXEStatus

$C$DW$50	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_getZone2RAMEXEStatus")
	.dwattr $C$DW$50, DW_AT_low_pc(_DCSM_getZone2RAMEXEStatus)
	.dwattr $C$DW$50, DW_AT_high_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_DCSM_getZone2RAMEXEStatus")
	.dwattr $C$DW$50, DW_AT_external
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$50, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$50, DW_AT_TI_begin_line(0x128)
	.dwattr $C$DW$50, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$50, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 297,column 1,is_stmt,address _DCSM_getZone2RAMEXEStatus

	.dwfde $C$DW$CIE, _DCSM_getZone2RAMEXEStatus
$C$DW$51	.dwtag  DW_TAG_formal_parameter, DW_AT_name("module")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_module")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DCSM_getZone2RAMEXEStatus    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_DCSM_getZone2RAMEXEStatus:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("module")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_module")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_breg20 -1]
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("status")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_status")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[1],AL            ; [CPU_] |297| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 304,column 5,is_stmt
$C$DW$54	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$54, DW_AT_low_pc(0x00)
	.dwattr $C$DW$54, DW_AT_name("_DCSM_getRAMZone")
	.dwattr $C$DW$54, DW_AT_TI_call
        LCR       #_DCSM_getRAMZone     ; [CPU_] |304| 
        ; call occurs [#_DCSM_getRAMZone] ; [] |304| 
        CMPB      AL,#2                 ; [CPU_] |304| 
        BF        $C$L17,EQ             ; [CPU_] |304| 
        ; branchcc occurs ; [] |304| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 306,column 9,is_stmt
        MOVB      *-SP[2],#2,UNC        ; [CPU_] |306| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 307,column 5,is_stmt
        B         $C$L18,UNC            ; [CPU_] |307| 
        ; branch occurs ; [] |307| 
$C$L17:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 313,column 9,is_stmt
        MOVL      XAR4,#389216          ; [CPU_U] |313| 
        MOV       T,*-SP[1]             ; [CPU_] |313| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |313| 
        LSR       AL,T                  ; [CPU_] |313| 
        ANDB      AL,#0x01              ; [CPU_] |313| 
        MOV       *-SP[2],AL            ; [CPU_] |313| 
$C$L18:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 316,column 5,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |316| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 317,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$55	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$55, DW_AT_low_pc(0x00)
	.dwattr $C$DW$55, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$50, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$50, DW_AT_TI_end_line(0x13d)
	.dwattr $C$DW$50, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$50

	.sect	".text"
	.clink
	.global	_DCSM_claimZoneSemaphore

$C$DW$56	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_claimZoneSemaphore")
	.dwattr $C$DW$56, DW_AT_low_pc(_DCSM_claimZoneSemaphore)
	.dwattr $C$DW$56, DW_AT_high_pc(0x00)
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_DCSM_claimZoneSemaphore")
	.dwattr $C$DW$56, DW_AT_external
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$56, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$56, DW_AT_TI_begin_line(0x145)
	.dwattr $C$DW$56, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$56, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 326,column 1,is_stmt,address _DCSM_claimZoneSemaphore

	.dwfde $C$DW$CIE, _DCSM_claimZoneSemaphore
$C$DW$57	.dwtag  DW_TAG_formal_parameter, DW_AT_name("zone")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_zone")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _DCSM_claimZoneSemaphore      FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_DCSM_claimZoneSemaphore:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("zone")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_zone")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_breg20 -1]
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("regAddress")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_regAddress")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_breg20 -4]
        MOV       *-SP[1],AL            ; [CPU_] |326| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 330,column 25,is_stmt
        MOVL      XAR4,#389232          ; [CPU_U] |330| 
        MOVL      *-SP[4],XAR4          ; [CPU_] |330| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 332,column 5,is_stmt
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_name("___eallow")
	.dwattr $C$DW$60, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |332| 
        ; call occurs [#___eallow] ; [] |332| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 338,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |338| 
        MOV       AL,*-SP[1]            ; [CPU_] |338| 
        OR        AL,#0xa500            ; [CPU_] |338| 
        MOV       *+XAR4[0],AL          ; [CPU_] |338| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 340,column 5,is_stmt
$C$DW$61	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$61, DW_AT_low_pc(0x00)
	.dwattr $C$DW$61, DW_AT_name("___edis")
	.dwattr $C$DW$61, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |340| 
        ; call occurs [#___edis] ; [] |340| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 346,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |346| 
        MOVU      ACC,*-SP[1]           ; [CPU_] |346| 
        MOVL      XAR6,ACC              ; [CPU_] |346| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |346| 
        ANDB      AL,#0x03              ; [CPU_] |346| 
        MOVZ      AR7,AL                ; [CPU_] |346| 
        MOVL      ACC,XAR6              ; [CPU_] |346| 
        CMPL      ACC,XAR7              ; [CPU_] |346| 
        BF        $C$L19,NEQ            ; [CPU_] |346| 
        ; branchcc occurs ; [] |346| 
        MOVB      AL,#1                 ; [CPU_] |346| 
        B         $C$L20,UNC            ; [CPU_] |346| 
        ; branch occurs ; [] |346| 
$C$L19:    
        MOVB      AL,#0                 ; [CPU_] |346| 
$C$L20:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 348,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$62	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$62, DW_AT_low_pc(0x00)
	.dwattr $C$DW$62, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$56, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$56, DW_AT_TI_end_line(0x15c)
	.dwattr $C$DW$56, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$56

	.sect	".text"
	.clink
	.global	_DCSM_releaseZoneSemaphore

$C$DW$63	.dwtag  DW_TAG_subprogram, DW_AT_name("DCSM_releaseZoneSemaphore")
	.dwattr $C$DW$63, DW_AT_low_pc(_DCSM_releaseZoneSemaphore)
	.dwattr $C$DW$63, DW_AT_high_pc(0x00)
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_DCSM_releaseZoneSemaphore")
	.dwattr $C$DW$63, DW_AT_external
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$63, DW_AT_TI_begin_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$63, DW_AT_TI_begin_line(0x164)
	.dwattr $C$DW$63, DW_AT_TI_begin_column(0x01)
	.dwattr $C$DW$63, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 357,column 1,is_stmt,address _DCSM_releaseZoneSemaphore

	.dwfde $C$DW$CIE, _DCSM_releaseZoneSemaphore

;***************************************************************
;* FNAME: _DCSM_releaseZoneSemaphore    FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_DCSM_releaseZoneSemaphore:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("regAddress")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_regAddress")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 361,column 25,is_stmt
        MOVL      XAR4,#389232          ; [CPU_U] |361| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |361| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 363,column 5,is_stmt
$C$DW$65	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$65, DW_AT_low_pc(0x00)
	.dwattr $C$DW$65, DW_AT_name("___eallow")
	.dwattr $C$DW$65, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |363| 
        ; call occurs [#___eallow] ; [] |363| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 369,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |369| 
        MOV       *+XAR4[0],#42240      ; [CPU_] |369| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 370,column 5,is_stmt
$C$DW$66	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$66, DW_AT_low_pc(0x00)
	.dwattr $C$DW$66, DW_AT_name("___edis")
	.dwattr $C$DW$66, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |370| 
        ; call occurs [#___edis] ; [] |370| 
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 376,column 5,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |376| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |376| 
        ANDB      AL,#0x03              ; [CPU_] |376| 
        BF        $C$L21,NEQ            ; [CPU_] |376| 
        ; branchcc occurs ; [] |376| 
        MOVB      AL,#1                 ; [CPU_] |376| 
        B         $C$L22,UNC            ; [CPU_] |376| 
        ; branch occurs ; [] |376| 
$C$L21:    
        MOVB      AL,#0                 ; [CPU_] |376| 
$C$L22:    
	.dwpsn	file "../ExtraData/driverlib2/dcsm.c",line 377,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$63, DW_AT_TI_end_file("../ExtraData/driverlib2/dcsm.c")
	.dwattr $C$DW$63, DW_AT_TI_end_line(0x179)
	.dwattr $C$DW$63, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$63

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$21	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x01)
$C$DW$68	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_MEMORY_INACCESSIBLE"), DW_AT_const_value(0x00)
$C$DW$69	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_MEMORY_ZONE1"), DW_AT_const_value(0x01)
$C$DW$70	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_MEMORY_ZONE2"), DW_AT_const_value(0x02)
$C$DW$71	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_MEMORY_FULL_ACCESS"), DW_AT_const_value(0x03)
	.dwendtag $C$DW$T$21

$C$DW$T$22	.dwtag  DW_TAG_typedef, DW_AT_name("DCSM_MemoryStatus")
	.dwattr $C$DW$T$22, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$22, DW_AT_language(DW_LANG_C)

$C$DW$T$23	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x01)
$C$DW$72	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMLS0"), DW_AT_const_value(0x00)
$C$DW$73	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMLS1"), DW_AT_const_value(0x01)
$C$DW$74	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMLS2"), DW_AT_const_value(0x02)
$C$DW$75	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMLS3"), DW_AT_const_value(0x03)
$C$DW$76	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMLS4"), DW_AT_const_value(0x04)
$C$DW$77	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMLS5"), DW_AT_const_value(0x05)
$C$DW$78	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMD0"), DW_AT_const_value(0x06)
$C$DW$79	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_RAMD1"), DW_AT_const_value(0x07)
$C$DW$80	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_CLA"), DW_AT_const_value(0x0e)
	.dwendtag $C$DW$T$23

$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("DCSM_RAMModule")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)

$C$DW$T$27	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$81	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_A"), DW_AT_const_value(0x00)
$C$DW$82	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_B"), DW_AT_const_value(0x01)
$C$DW$83	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_C"), DW_AT_const_value(0x02)
$C$DW$84	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_D"), DW_AT_const_value(0x03)
$C$DW$85	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_E"), DW_AT_const_value(0x04)
$C$DW$86	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_F"), DW_AT_const_value(0x05)
$C$DW$87	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_G"), DW_AT_const_value(0x06)
$C$DW$88	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_H"), DW_AT_const_value(0x07)
$C$DW$89	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_I"), DW_AT_const_value(0x08)
$C$DW$90	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_J"), DW_AT_const_value(0x09)
$C$DW$91	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_K"), DW_AT_const_value(0x0a)
$C$DW$92	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_L"), DW_AT_const_value(0x0b)
$C$DW$93	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_M"), DW_AT_const_value(0x0c)
$C$DW$94	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_SECTOR_N"), DW_AT_const_value(0x0d)
	.dwendtag $C$DW$T$27

$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("DCSM_Sector")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)

$C$DW$T$31	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x01)
$C$DW$95	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_PROTECTED"), DW_AT_const_value(0x00)
$C$DW$96	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_UNPROTECTED"), DW_AT_const_value(0x01)
$C$DW$97	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_INCORRECT_ZONE"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$31

$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("DCSM_EXEOnlyStatus")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)

$C$DW$T$35	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x01)
$C$DW$98	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_FLSEM_ZONE1"), DW_AT_const_value(0x01)
$C$DW$99	.dwtag  DW_TAG_enumerator, DW_AT_name("DCSM_FLSEM_ZONE2"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$35

$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("DCSM_SemaphoreZone")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x08)
$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$100, DW_AT_name("csmKey0")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_csmKey0")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$101, DW_AT_name("csmKey1")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_csmKey1")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$102, DW_AT_name("csmKey2")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_csmKey2")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$103, DW_AT_name("csmKey3")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_csmKey3")
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("DCSM_CSMPasswordKey")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$104	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$37)
$C$DW$T$38	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$104)
$C$DW$T$39	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x16)
$C$DW$105	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$39)
$C$DW$T$40	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$105)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("_Bool")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$19	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg0]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg1]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg2]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg3]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_reg20]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg21]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_reg22]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg23]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_reg24]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg25]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg26]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg28]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg29]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg30]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg31]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x20]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x21]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x22]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x23]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x24]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x25]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x26]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_reg4]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_reg6]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_reg8]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_reg10]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_reg12]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_reg14]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg16]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_reg17]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_reg18]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg19]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg5]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg7]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_reg9]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg11]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_reg13]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg15]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x30]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x33]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x34]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x37]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x38]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x40]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x43]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x44]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x47]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x48]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0x49]
$C$DW$163	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$164	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_regx 0x27]
$C$DW$165	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_regx 0x28]
$C$DW$166	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_reg27]
$C$DW$167	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

