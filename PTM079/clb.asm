;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Wed Jul 28 18:45:02 2021                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\AEC_Standard\PTM079")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("__edis")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("___edis")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("__eallow")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("___eallow")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2171212 
	.sect	".text"
	.clink

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("CLB_writeInterface")
	.dwattr $C$DW$3, DW_AT_low_pc(_CLB_writeInterface)
	.dwattr $C$DW$3, DW_AT_high_pc(0x00)
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_CLB_writeInterface")
	.dwattr $C$DW$3, DW_AT_TI_begin_file("..\ExtraData\driverlib2\clb.h")
	.dwattr $C$DW$3, DW_AT_TI_begin_line(0x1f6)
	.dwattr $C$DW$3, DW_AT_TI_begin_column(0x14)
	.dwattr $C$DW$3, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "..\ExtraData\driverlib2\clb.h",line 504,column 1,is_stmt,address _CLB_writeInterface

	.dwfde $C$DW$CIE, _CLB_writeInterface
$C$DW$4	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$4, DW_AT_location[DW_OP_reg0]
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("address")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_breg20 -6]
$C$DW$6	.dwtag  DW_TAG_formal_parameter, DW_AT_name("value")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_value")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_breg20 -8]

;***************************************************************
;* FNAME: _CLB_writeInterface           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CLB_writeInterface:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$7	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$7, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],ACC           ; [CPU_] |504| 
	.dwpsn	file "..\ExtraData\driverlib2\clb.h",line 508,column 5,is_stmt
$C$DW$8	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$8, DW_AT_low_pc(0x00)
	.dwattr $C$DW$8, DW_AT_name("___eallow")
	.dwattr $C$DW$8, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |508| 
        ; call occurs [#___eallow] ; [] |508| 
	.dwpsn	file "..\ExtraData\driverlib2\clb.h",line 509,column 5,is_stmt
        MOVL      XAR6,*-SP[6]          ; [CPU_] |509| 
        MOV       ACC,#258              ; [CPU_] |509| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |509| 
        MOVL      XAR4,ACC              ; [CPU_] |509| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |509| 
	.dwpsn	file "..\ExtraData\driverlib2\clb.h",line 510,column 5,is_stmt
        MOVL      XAR6,*-SP[8]          ; [CPU_] |510| 
        MOV       ACC,#260              ; [CPU_] |510| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |510| 
        MOVL      XAR4,ACC              ; [CPU_] |510| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |510| 
	.dwpsn	file "..\ExtraData\driverlib2\clb.h",line 511,column 5,is_stmt
        MOV       ACC,#256              ; [CPU_] |511| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |511| 
        MOVL      XAR4,ACC              ; [CPU_] |511| 
        OR        *+XAR4[0],#1          ; [CPU_] |511| 
	.dwpsn	file "..\ExtraData\driverlib2\clb.h",line 512,column 5,is_stmt
$C$DW$9	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$9, DW_AT_low_pc(0x00)
	.dwattr $C$DW$9, DW_AT_name("___edis")
	.dwattr $C$DW$9, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |512| 
        ; call occurs [#___edis] ; [] |512| 
	.dwpsn	file "..\ExtraData\driverlib2\clb.h",line 513,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$10	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$10, DW_AT_low_pc(0x00)
	.dwattr $C$DW$10, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$3, DW_AT_TI_end_file("..\ExtraData\driverlib2\clb.h")
	.dwattr $C$DW$3, DW_AT_TI_end_line(0x201)
	.dwattr $C$DW$3, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$3

	.sect	".text"
	.clink
	.global	_CLB_configCounterLoadMatch

$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("CLB_configCounterLoadMatch")
	.dwattr $C$DW$11, DW_AT_low_pc(_CLB_configCounterLoadMatch)
	.dwattr $C$DW$11, DW_AT_high_pc(0x00)
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_CLB_configCounterLoadMatch")
	.dwattr $C$DW$11, DW_AT_external
	.dwattr $C$DW$11, DW_AT_TI_begin_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$11, DW_AT_TI_begin_line(0x32)
	.dwattr $C$DW$11, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$11, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 52,column 1,is_stmt,address _CLB_configCounterLoadMatch

	.dwfde $C$DW$CIE, _CLB_configCounterLoadMatch
$C$DW$12	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg0]
$C$DW$13	.dwtag  DW_TAG_formal_parameter, DW_AT_name("counterID")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_counterID")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_reg12]
$C$DW$14	.dwtag  DW_TAG_formal_parameter, DW_AT_name("load")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_load")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_breg20 -12]
$C$DW$15	.dwtag  DW_TAG_formal_parameter, DW_AT_name("match1")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_match1")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_breg20 -14]
$C$DW$16	.dwtag  DW_TAG_formal_parameter, DW_AT_name("match2")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_match2")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_breg20 -16]

;***************************************************************
;* FNAME: _CLB_configCounterLoadMatch   FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            4 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CLB_configCounterLoadMatch:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$17	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_breg20 -6]
$C$DW$18	.dwtag  DW_TAG_variable, DW_AT_name("counterID")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_counterID")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_breg20 -7]
        MOV       *-SP[7],AR4           ; [CPU_] |52| 
        MOVL      *-SP[6],ACC           ; [CPU_] |52| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 55,column 5,is_stmt
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("___eallow")
	.dwattr $C$DW$19, DW_AT_TI_call
        LCR       #___eallow            ; [CPU_] |55| 
        ; call occurs [#___eallow] ; [] |55| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 56,column 5,is_stmt
        B         $C$L4,UNC             ; [CPU_] |56| 
        ; branch occurs ; [] |56| 
$C$L1:    
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 59,column 13,is_stmt
        MOVB      ACC,#0                ; [CPU_] |59| 
        MOVL      *-SP[2],ACC           ; [CPU_] |59| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |59| 
        MOVL      *-SP[4],ACC           ; [CPU_] |59| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |59| 
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x00)
	.dwattr $C$DW$20, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$20, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |59| 
        ; call occurs [#_CLB_writeInterface] ; [] |59| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 60,column 13,is_stmt
        MOVB      ACC,#4                ; [CPU_] |60| 
        MOVL      *-SP[2],ACC           ; [CPU_] |60| 
        MOVL      ACC,*-SP[14]          ; [CPU_] |60| 
        MOVL      *-SP[4],ACC           ; [CPU_] |60| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |60| 
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$21, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |60| 
        ; call occurs [#_CLB_writeInterface] ; [] |60| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 61,column 13,is_stmt
        MOVB      ACC,#8                ; [CPU_] |61| 
        MOVL      *-SP[2],ACC           ; [CPU_] |61| 
        MOVL      ACC,*-SP[16]          ; [CPU_] |61| 
        MOVL      *-SP[4],ACC           ; [CPU_] |61| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |61| 
$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x00)
	.dwattr $C$DW$22, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$22, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |61| 
        ; call occurs [#_CLB_writeInterface] ; [] |61| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 62,column 13,is_stmt
        B         $C$L5,UNC             ; [CPU_] |62| 
        ; branch occurs ; [] |62| 
$C$L2:    
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 65,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |65| 
        MOVL      *-SP[2],ACC           ; [CPU_] |65| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |65| 
        MOVL      *-SP[4],ACC           ; [CPU_] |65| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |65| 
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$23, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |65| 
        ; call occurs [#_CLB_writeInterface] ; [] |65| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 66,column 13,is_stmt
        MOVB      ACC,#5                ; [CPU_] |66| 
        MOVL      *-SP[2],ACC           ; [CPU_] |66| 
        MOVL      ACC,*-SP[14]          ; [CPU_] |66| 
        MOVL      *-SP[4],ACC           ; [CPU_] |66| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |66| 
$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$24, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |66| 
        ; call occurs [#_CLB_writeInterface] ; [] |66| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 67,column 13,is_stmt
        MOVB      ACC,#9                ; [CPU_] |67| 
        MOVL      *-SP[2],ACC           ; [CPU_] |67| 
        MOVL      ACC,*-SP[16]          ; [CPU_] |67| 
        MOVL      *-SP[4],ACC           ; [CPU_] |67| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |67| 
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$25, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |67| 
        ; call occurs [#_CLB_writeInterface] ; [] |67| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 68,column 13,is_stmt
        B         $C$L5,UNC             ; [CPU_] |68| 
        ; branch occurs ; [] |68| 
$C$L3:    
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 71,column 13,is_stmt
        MOVB      ACC,#2                ; [CPU_] |71| 
        MOVL      *-SP[2],ACC           ; [CPU_] |71| 
        MOVL      ACC,*-SP[12]          ; [CPU_] |71| 
        MOVL      *-SP[4],ACC           ; [CPU_] |71| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |71| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$26, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |71| 
        ; call occurs [#_CLB_writeInterface] ; [] |71| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 72,column 13,is_stmt
        MOVB      ACC,#6                ; [CPU_] |72| 
        MOVL      *-SP[2],ACC           ; [CPU_] |72| 
        MOVL      ACC,*-SP[14]          ; [CPU_] |72| 
        MOVL      *-SP[4],ACC           ; [CPU_] |72| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |72| 
$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$27, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |72| 
        ; call occurs [#_CLB_writeInterface] ; [] |72| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 73,column 13,is_stmt
        MOVB      ACC,#10               ; [CPU_] |73| 
        MOVL      *-SP[2],ACC           ; [CPU_] |73| 
        MOVL      ACC,*-SP[16]          ; [CPU_] |73| 
        MOVL      *-SP[4],ACC           ; [CPU_] |73| 
        MOVL      ACC,*-SP[6]           ; [CPU_] |73| 
$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_CLB_writeInterface")
	.dwattr $C$DW$28, DW_AT_TI_call
        LCR       #_CLB_writeInterface  ; [CPU_] |73| 
        ; call occurs [#_CLB_writeInterface] ; [] |73| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 74,column 13,is_stmt
        B         $C$L5,UNC             ; [CPU_] |74| 
        ; branch occurs ; [] |74| 
$C$L4:    
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 56,column 5,is_stmt
        MOV       AL,*-SP[7]            ; [CPU_] |56| 
        BF        $C$L1,EQ              ; [CPU_] |56| 
        ; branchcc occurs ; [] |56| 
        CMPB      AL,#1                 ; [CPU_] |56| 
        BF        $C$L2,EQ              ; [CPU_] |56| 
        ; branchcc occurs ; [] |56| 
        CMPB      AL,#2                 ; [CPU_] |56| 
        BF        $C$L3,EQ              ; [CPU_] |56| 
        ; branchcc occurs ; [] |56| 
$C$L5:    
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 82,column 5,is_stmt
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("___edis")
	.dwattr $C$DW$29, DW_AT_TI_call
        LCR       #___edis              ; [CPU_] |82| 
        ; call occurs [#___edis] ; [] |82| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 83,column 1,is_stmt
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$11, DW_AT_TI_end_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$11, DW_AT_TI_end_line(0x53)
	.dwattr $C$DW$11, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$11

	.sect	".text"
	.clink
	.global	_CLB_clearFIFOs

$C$DW$31	.dwtag  DW_TAG_subprogram, DW_AT_name("CLB_clearFIFOs")
	.dwattr $C$DW$31, DW_AT_low_pc(_CLB_clearFIFOs)
	.dwattr $C$DW$31, DW_AT_high_pc(0x00)
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("_CLB_clearFIFOs")
	.dwattr $C$DW$31, DW_AT_external
	.dwattr $C$DW$31, DW_AT_TI_begin_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$31, DW_AT_TI_begin_line(0x5a)
	.dwattr $C$DW$31, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$31, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 91,column 1,is_stmt,address _CLB_clearFIFOs

	.dwfde $C$DW$CIE, _CLB_clearFIFOs
$C$DW$32	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _CLB_clearFIFOs               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_CLB_clearFIFOs:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_breg20 -2]
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],ACC           ; [CPU_] |91| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 96,column 9,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |96| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 96,column 17,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |96| 
        CMPB      AL,#4                 ; [CPU_] |96| 
        B         $C$L7,HIS             ; [CPU_] |96| 
        ; branchcc occurs ; [] |96| 
$C$L6:    
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 98,column 9,is_stmt
        MOV       ACC,*-SP[3] << #1     ; [CPU_] |98| 
        ADD       AL,#256               ; [CPU_] |98| 
        MOVB      XAR6,#0               ; [CPU_] |98| 
        MOVU      ACC,AL                ; [CPU_] |98| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |98| 
        ADD       ACC,#1 << 9           ; [CPU_] |98| 
        MOVL      XAR4,ACC              ; [CPU_] |98| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |98| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 96,column 36,is_stmt
        INC       *-SP[3]               ; [CPU_] |96| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 96,column 17,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |96| 
        CMPB      AL,#4                 ; [CPU_] |96| 
        B         $C$L6,LO              ; [CPU_] |96| 
        ; branchcc occurs ; [] |96| 
$C$L7:    
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 101,column 5,is_stmt
        MOVB      XAR6,#0               ; [CPU_] |101| 
        MOV       ACC,#270              ; [CPU_] |101| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |101| 
        MOVL      XAR4,ACC              ; [CPU_] |101| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |101| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 102,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$31, DW_AT_TI_end_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$31, DW_AT_TI_end_line(0x66)
	.dwattr $C$DW$31, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$31

	.sect	".text"
	.clink
	.global	_CLB_writeFIFOs

$C$DW$36	.dwtag  DW_TAG_subprogram, DW_AT_name("CLB_writeFIFOs")
	.dwattr $C$DW$36, DW_AT_low_pc(_CLB_writeFIFOs)
	.dwattr $C$DW$36, DW_AT_high_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_CLB_writeFIFOs")
	.dwattr $C$DW$36, DW_AT_external
	.dwattr $C$DW$36, DW_AT_TI_begin_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$36, DW_AT_TI_begin_line(0x6d)
	.dwattr $C$DW$36, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$36, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 110,column 1,is_stmt,address _CLB_writeFIFOs

	.dwfde $C$DW$CIE, _CLB_writeFIFOs
$C$DW$37	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg0]
$C$DW$38	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pullData")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_pullData")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _CLB_writeFIFOs               FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CLB_writeFIFOs:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_breg20 -2]
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("pullData")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_pullData")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |110| 
        MOVL      *-SP[2],ACC           ; [CPU_] |110| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 116,column 5,is_stmt
        MOVL      ACC,*-SP[2]           ; [CPU_] |116| 
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("_CLB_clearFIFOs")
	.dwattr $C$DW$41, DW_AT_TI_call
        LCR       #_CLB_clearFIFOs      ; [CPU_] |116| 
        ; call occurs [#_CLB_clearFIFOs] ; [] |116| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 121,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |121| 
        MOVL      XAR6,*+XAR4[0]        ; [CPU_] |121| 
        MOV       ACC,#768              ; [CPU_] |121| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |121| 
        MOVL      XAR4,ACC              ; [CPU_] |121| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |121| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 122,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |122| 
        MOVL      XAR6,*+XAR4[2]        ; [CPU_] |122| 
        MOV       ACC,#770              ; [CPU_] |122| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |122| 
        MOVL      XAR4,ACC              ; [CPU_] |122| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |122| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 123,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |123| 
        MOVL      XAR6,*+XAR4[4]        ; [CPU_] |123| 
        MOV       ACC,#772              ; [CPU_] |123| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |123| 
        MOVL      XAR4,ACC              ; [CPU_] |123| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |123| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 124,column 5,is_stmt
        MOVL      XAR4,*-SP[4]          ; [CPU_] |124| 
        MOVL      XAR6,*+XAR4[6]        ; [CPU_] |124| 
        MOV       ACC,#774              ; [CPU_] |124| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |124| 
        MOVL      XAR4,ACC              ; [CPU_] |124| 
        MOVL      *+XAR4[0],XAR6        ; [CPU_] |124| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 125,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$36, DW_AT_TI_end_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$36, DW_AT_TI_end_line(0x7d)
	.dwattr $C$DW$36, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$36

	.sect	".text"
	.clink
	.global	_CLB_readFIFOs

$C$DW$43	.dwtag  DW_TAG_subprogram, DW_AT_name("CLB_readFIFOs")
	.dwattr $C$DW$43, DW_AT_low_pc(_CLB_readFIFOs)
	.dwattr $C$DW$43, DW_AT_high_pc(0x00)
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_CLB_readFIFOs")
	.dwattr $C$DW$43, DW_AT_external
	.dwattr $C$DW$43, DW_AT_TI_begin_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$43, DW_AT_TI_begin_line(0x84)
	.dwattr $C$DW$43, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$43, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 133,column 1,is_stmt,address _CLB_readFIFOs

	.dwfde $C$DW$CIE, _CLB_readFIFOs
$C$DW$44	.dwtag  DW_TAG_formal_parameter, DW_AT_name("base")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg0]
$C$DW$45	.dwtag  DW_TAG_formal_parameter, DW_AT_name("pushData")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_pushData")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _CLB_readFIFOs                FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  4 Auto,  0 SOE     *
;***************************************************************

_CLB_readFIFOs:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("base")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_base")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_breg20 -2]
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("pushData")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_pushData")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_breg20 -4]
        MOVL      *-SP[4],XAR4          ; [CPU_] |133| 
        MOVL      *-SP[2],ACC           ; [CPU_] |133| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 139,column 5,is_stmt
        MOV       ACC,#512              ; [CPU_] |139| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |139| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |139| 
        MOVL      XAR4,ACC              ; [CPU_] |139| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |139| 
        MOVL      *+XAR5[0],ACC         ; [CPU_] |139| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 140,column 5,is_stmt
        MOV       ACC,#514              ; [CPU_] |140| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |140| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |140| 
        MOVL      XAR4,ACC              ; [CPU_] |140| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |140| 
        MOVL      *+XAR5[2],ACC         ; [CPU_] |140| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 141,column 5,is_stmt
        MOV       ACC,#516              ; [CPU_] |141| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |141| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |141| 
        MOVL      XAR4,ACC              ; [CPU_] |141| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |141| 
        MOVL      *+XAR5[4],ACC         ; [CPU_] |141| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 142,column 5,is_stmt
        MOV       ACC,#518              ; [CPU_] |142| 
        MOVL      XAR5,*-SP[4]          ; [CPU_] |142| 
        ADDL      ACC,*-SP[2]           ; [CPU_] |142| 
        MOVL      XAR4,ACC              ; [CPU_] |142| 
        MOVL      ACC,*+XAR4[0]         ; [CPU_] |142| 
        MOVL      *+XAR5[6],ACC         ; [CPU_] |142| 
	.dwpsn	file "../ExtraData/driverlib2/clb.c",line 143,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$43, DW_AT_TI_end_file("../ExtraData/driverlib2/clb.c")
	.dwattr $C$DW$43, DW_AT_TI_end_line(0x8f)
	.dwattr $C$DW$43, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$43

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	___edis
	.global	___eallow

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$19	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x01)
$C$DW$49	.dwtag  DW_TAG_enumerator, DW_AT_name("CLB_CTR0"), DW_AT_const_value(0x00)
$C$DW$50	.dwtag  DW_TAG_enumerator, DW_AT_name("CLB_CTR1"), DW_AT_const_value(0x01)
$C$DW$51	.dwtag  DW_TAG_enumerator, DW_AT_name("CLB_CTR2"), DW_AT_const_value(0x02)
	.dwendtag $C$DW$T$19

$C$DW$T$20	.dwtag  DW_TAG_typedef, DW_AT_name("CLB_Counters")
	.dwattr $C$DW$T$20, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$T$20, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$32	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$32, DW_AT_address_class(0x16)
$C$DW$52	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$23)
$C$DW$T$29	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$52)
$C$DW$T$30	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x16)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg0]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg1]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg2]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg3]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg20]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg21]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg22]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg23]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg24]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg25]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg26]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg28]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg29]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg30]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg31]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x20]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x21]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x22]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x23]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x24]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x25]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x26]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_reg4]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_reg6]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_reg8]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_reg10]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_reg12]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg14]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg16]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg17]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg18]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg19]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg5]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg7]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg9]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg11]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_reg13]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_reg15]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x30]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x33]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x34]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x37]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x38]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x40]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x43]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x44]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x47]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x48]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x49]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x27]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x28]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg27]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

