;***************************************************************
;* TMS320C2000 C/C++ Codegen                        PC v6.2.11 *
;* Date/Time created: Mon Jul 25 10:48:12 2022                 *
;***************************************************************
	.compiler_opts --cla_support=cla0 --float_support=fpu32 --hll_source=on --mem_model:code=flat --mem_model:data=large --object_format=coff --silicon_version=28 --symdebug:dwarf --symdebug:dwarf_version=2 
FP	.set	XAR2

$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("../gateway.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C2000 C/C++ Codegen PC v6.2.11 Copyright (c) 1996-2015 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("C:\Users\aen17\Desktop\Working\SVN\Repository\Emosys_Controller\PTM079")
;**************************************************************
;* CINIT RECORDS                                              *
;**************************************************************
	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_FullCharge_Active+0,32
	.bits	1,16			; _FullCharge_Active @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_NoCharge_Active+0,32
	.bits	1,16			; _NoCharge_Active @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Modules_Present+0,32
	.bits	0,16			; _Modules_Present @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TrickleCharge_Active+0,32
	.bits	1,16			; _TrickleCharge_Active @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Sync_count+0,32
	.bits	98,16			; _Sync_count @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_EventResetVoltH+0,32
	.bits	1,16			; _EventResetVoltH @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_EventResetTemp+0,32
	.bits	1,16			; _EventResetTemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Charger_Current_State+0,32
	.bits	-1,16			; _Charger_Current_State @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_EventResetVoltL+0,32
	.bits	1,16			; _EventResetVoltL @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_BatteryVoltage+0,32
	.bits	0,16			; _BatteryVoltage @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_OldGAT_max+0,32
	.bits	0,16			; _OldGAT_max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_OldGAT_min+0,32
	.bits	0,16			; _OldGAT_min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Delay_Error+0,32
	.bits	0,16			; _Delay_Error @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CANbus_ErrorResettick$1+0,32
	.bits	0,16			; _CANbus_ErrorResettick$1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_GAT_max+0,32
	.bits	1,16			; _GAT_max @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_GAT_min+0,32
	.bits	65535,16			; _GAT_min @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Oldmaxtemp+0,32
	.bits	0,16			; _Oldmaxtemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Heater_status+0,32
	.bits	0,16			; _Heater_status @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Oldmintemp+0,32
	.bits	0,16			; _Oldmintemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestCurrentEnable+0,32
	.bits	0,16			; _TestCurrentEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_InitOK+0,32
	.bits	0,16			; _InitOK @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_UnderCurrent+0,32
	.bits	0,16			; _UnderCurrent @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_TestPositionEnable+0,32
	.bits	1,16			; _TestPositionEnable @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CANbus_timer3+0,32
	.bits	0,16			; _CANbus_timer3 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CANbus_timer2+0,32
	.bits	0,16			; _CANbus_timer2 @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CounterReset+0,32
	.bits	0,16			; _CounterReset @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CANbus_ErrorReset+0,32
	.bits	0,16			; _CANbus_ErrorReset @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CANComm_ErrorReset+0,32
	.bits	0,16			; _CANComm_ErrorReset @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_make10ms+0,32
	.bits	0,16			; _make10ms @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_Charger_Previous_State+0,32
	.bits	0,16			; _Charger_Previous_State @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_ReceiveNew+0,32
	.bits	0,16			; _ReceiveNew @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_SequenceRunning+0,32
	.bits	0,16			; _SequenceRunning @ 0

	.sect	".cinit"
	.align	1
	.field  	-1,16
	.field  	_CANbus_timer+0,32
	.bits	0,16			; _CANbus_timer @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_TimerTemp+0,32
	.bits	0,32			; _TimerTemp @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Temp_counter+0,32
	.bits	0,32			; _GV_Temp_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Temp_counter1+0,32
	.bits	0,32			; _GV_Temp_counter1 @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_current_counter+0,32
	.bits	0,32			; _GV_current_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Overvoltage_counter+0,32
	.bits	0,32			; _GV_Overvoltage_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Undervoltage_counter+0,32
	.bits	0,32			; _GV_Undervoltage_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_GV_Blinking_counter+0,32
	.bits	0,32			; _GV_Blinking_counter @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_BootNodeId+0,32
	.bits	99,32			; _BootNodeId @ 0

	.sect	".cinit"
	.align	1
	.field  	-2,16
	.field  	_BootCommand+0,32
	.bits	0,32			; _BootCommand @ 0


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Unlock")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_USB_Unlock")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external

$C$DW$2	.dwtag  DW_TAG_subprogram, DW_AT_name("genCRC32Table")
	.dwattr $C$DW$2, DW_AT_TI_symbol_name("_genCRC32Table")
	.dwattr $C$DW$2, DW_AT_declaration
	.dwattr $C$DW$2, DW_AT_external

$C$DW$3	.dwtag  DW_TAG_subprogram, DW_AT_name("ADS_Init")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_ADS_Init")
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_SetError")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_ERR_SetError")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$4


$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorUnderVoltage")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external

$C$DW$7	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddLog")
	.dwattr $C$DW$7, DW_AT_TI_symbol_name("_PAR_AddLog")
	.dwattr $C$DW$7, DW_AT_declaration
	.dwattr $C$DW$7, DW_AT_external

$C$DW$8	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_SetParamDependantVars")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external

$C$DW$9	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_GetLogNB")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("_PAR_GetLogNB")
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$9


$C$DW$11	.dwtag  DW_TAG_subprogram, DW_AT_name("ChargerError")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_ChargerError")
	.dwattr $C$DW$11, DW_AT_declaration
	.dwattr $C$DW$11, DW_AT_external

$C$DW$12	.dwtag  DW_TAG_subprogram, DW_AT_name("setNodeId")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_setNodeId")
	.dwattr $C$DW$12, DW_AT_declaration
	.dwattr $C$DW$12, DW_AT_external
$C$DW$13	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$97)
$C$DW$14	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$12


$C$DW$15	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ClearWarning")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_ERR_ClearWarning")
	.dwattr $C$DW$15, DW_AT_declaration
	.dwattr $C$DW$15, DW_AT_external
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$15


$C$DW$17	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_HandleWarning")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_ERR_HandleWarning")
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$13)
	.dwendtag $C$DW$17


$C$DW$19	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Stop")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_USB_Stop")
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external

$C$DW$20	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Reset")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_HAL_Reset")
	.dwattr $C$DW$20, DW_AT_declaration
	.dwattr $C$DW$20, DW_AT_external

$C$DW$21	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Random")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_HAL_Random")
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external

$C$DW$22	.dwtag  DW_TAG_subprogram, DW_AT_name("DIC_SetNodeId")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_DIC_SetNodeId")
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$22


$C$DW$24	.dwtag  DW_TAG_subprogram, DW_AT_name("REC_StartRecorder")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_REC_StartRecorder")
	.dwattr $C$DW$24, DW_AT_declaration
	.dwattr $C$DW$24, DW_AT_external

$C$DW$25	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Init")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_HAL_Init")
	.dwattr $C$DW$25, DW_AT_declaration
	.dwattr $C$DW$25, DW_AT_external

$C$DW$26	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverVoltage")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$26, DW_AT_declaration
	.dwattr $C$DW$26, DW_AT_external

$C$DW$27	.dwtag  DW_TAG_subprogram, DW_AT_name("HAL_Unlock")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_HAL_Unlock")
	.dwattr $C$DW$27, DW_AT_declaration
	.dwattr $C$DW$27, DW_AT_external

$C$DW$28	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverCurrent")
	.dwattr $C$DW$28, DW_AT_TI_symbol_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external

$C$DW$29	.dwtag  DW_TAG_subprogram, DW_AT_name("canInit")
	.dwattr $C$DW$29, DW_AT_TI_symbol_name("_canInit")
	.dwattr $C$DW$29, DW_AT_declaration
	.dwattr $C$DW$29, DW_AT_external
$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$9)
$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$29


$C$DW$32	.dwtag  DW_TAG_subprogram, DW_AT_name("ERR_ErrorOverTemp")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$32, DW_AT_declaration
	.dwattr $C$DW$32, DW_AT_external
$C$DW$33	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Safety_Low_Voltage_Delay")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Safety_Low_Voltage_Delay")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$33, DW_AT_declaration
	.dwattr $C$DW$33, DW_AT_external
$C$DW$34	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_Config")
	.dwattr $C$DW$34, DW_AT_TI_symbol_name("_ODP_Board_Config")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$34, DW_AT_declaration
	.dwattr $C$DW$34, DW_AT_external
$C$DW$35	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Board_RelayStatus")
	.dwattr $C$DW$35, DW_AT_TI_symbol_name("_ODV_Board_RelayStatus")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$35, DW_AT_declaration
	.dwattr $C$DW$35, DW_AT_external
$C$DW$36	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_BaudRate")
	.dwattr $C$DW$36, DW_AT_TI_symbol_name("_ODP_Board_BaudRate")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$36, DW_AT_declaration
	.dwattr $C$DW$36, DW_AT_external
$C$DW$37	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Sleep_Timeout")
	.dwattr $C$DW$37, DW_AT_TI_symbol_name("_ODP_Sleep_Timeout")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$37, DW_AT_declaration
	.dwattr $C$DW$37, DW_AT_external
$C$DW$38	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Temp_Heater_ON_Min")
	.dwattr $C$DW$38, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Temp_Heater_ON_Min")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$38, DW_AT_declaration
	.dwattr $C$DW$38, DW_AT_external
$C$DW$39	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Heater_Voltage_OFF")
	.dwattr $C$DW$39, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Heater_Voltage_OFF")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$39, DW_AT_declaration
	.dwattr $C$DW$39, DW_AT_external
$C$DW$40	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Sleep_Current")
	.dwattr $C$DW$40, DW_AT_TI_symbol_name("_ODP_Sleep_Current")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$40, DW_AT_declaration
	.dwattr $C$DW$40, DW_AT_external
$C$DW$41	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_TempHeater_OFF_Max")
	.dwattr $C$DW$41, DW_AT_TI_symbol_name("_ODP_Settings_AUD_TempHeater_OFF_Max")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$41, DW_AT_declaration
	.dwattr $C$DW$41, DW_AT_external
$C$DW$42	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_TimeOut")
	.dwattr $C$DW$42, DW_AT_TI_symbol_name("_ODP_CommError_TimeOut")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$42, DW_AT_declaration
	.dwattr $C$DW$42, DW_AT_external
$C$DW$43	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ChargerData_SlaveLimTemp")
	.dwattr $C$DW$43, DW_AT_TI_symbol_name("_ODV_ChargerData_SlaveLimTemp")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_external
$C$DW$44	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Delay")
	.dwattr $C$DW$44, DW_AT_TI_symbol_name("_ODP_CommError_Delay")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$44, DW_AT_declaration
	.dwattr $C$DW$44, DW_AT_external
$C$DW$45	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_OverVoltage_ErrCounter")
	.dwattr $C$DW$45, DW_AT_TI_symbol_name("_ODP_CommError_OverVoltage_ErrCounter")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_external
$C$DW$46	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_OverTemp_ErrCounter")
	.dwattr $C$DW$46, DW_AT_TI_symbol_name("_ODP_CommError_OverTemp_ErrCounter")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$46, DW_AT_declaration
	.dwattr $C$DW$46, DW_AT_external
$C$DW$47	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Temperature_Delay")
	.dwattr $C$DW$47, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Temperature_Delay")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$47, DW_AT_declaration
	.dwattr $C$DW$47, DW_AT_external
$C$DW$48	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_Charge_NotSucessful")
	.dwattr $C$DW$48, DW_AT_TI_symbol_name("_ODP_CommError_Charge_NotSucessful")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$48, DW_AT_declaration
	.dwattr $C$DW$48, DW_AT_external
$C$DW$49	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineEvent")
	.dwattr $C$DW$49, DW_AT_TI_symbol_name("_ODV_MachineEvent")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$49, DW_AT_declaration
	.dwattr $C$DW$49, DW_AT_external
$C$DW$50	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ChargerData_MasterLimTemp")
	.dwattr $C$DW$50, DW_AT_TI_symbol_name("_ODV_ChargerData_MasterLimTemp")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$50, DW_AT_declaration
	.dwattr $C$DW$50, DW_AT_external
$C$DW$51	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ChargerData_ChargerError")
	.dwattr $C$DW$51, DW_AT_TI_symbol_name("_ODV_ChargerData_ChargerError")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_external
$C$DW$52	.dwtag  DW_TAG_variable, DW_AT_name("ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$52, DW_AT_TI_symbol_name("_ODP_CommError_LowVoltage_ErrCounter")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$52, DW_AT_declaration
	.dwattr $C$DW$52, DW_AT_external
$C$DW$53	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$53, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmin")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_external
$C$DW$54	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$54, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Tmax")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$54, DW_AT_declaration
	.dwattr $C$DW$54, DW_AT_external
$C$DW$55	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$55, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Voltage_delay")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$55, DW_AT_declaration
	.dwattr $C$DW$55, DW_AT_external
$C$DW$56	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$56, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Resistor_Delay")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$56, DW_AT_declaration
	.dwattr $C$DW$56, DW_AT_external
$C$DW$57	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$57, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Current_delay")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$57, DW_AT_declaration
	.dwattr $C$DW$57, DW_AT_external
$C$DW$58	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$58, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Charge_In_Thres_Cur")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$58, DW_AT_declaration
	.dwattr $C$DW$58, DW_AT_external
$C$DW$59	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$59, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin_bal_delta")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_external
$C$DW$60	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$60, DW_AT_TI_symbol_name("_ODP_SafetyLimits_OverVoltage")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$60, DW_AT_declaration
	.dwattr $C$DW$60, DW_AT_external
$C$DW$61	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$61, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Overcurrent")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_external
$C$DW$62	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$62, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderVoltage")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$62, DW_AT_declaration
	.dwattr $C$DW$62, DW_AT_external
$C$DW$63	.dwtag  DW_TAG_variable, DW_AT_name("ODP_NbOfModules")
	.dwattr $C$DW$63, DW_AT_TI_symbol_name("_ODP_NbOfModules")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
$C$DW$64	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Current_C_D_Mode")
	.dwattr $C$DW$64, DW_AT_TI_symbol_name("_ODP_Current_C_D_Mode")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$64, DW_AT_declaration
	.dwattr $C$DW$64, DW_AT_external
$C$DW$65	.dwtag  DW_TAG_variable, DW_AT_name("ODV_MachineMode")
	.dwattr $C$DW$65, DW_AT_TI_symbol_name("_ODV_MachineMode")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$65, DW_AT_declaration
	.dwattr $C$DW$65, DW_AT_external
$C$DW$66	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RelayResetTime")
	.dwattr $C$DW$66, DW_AT_TI_symbol_name("_ODP_RelayResetTime")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$66, DW_AT_declaration
	.dwattr $C$DW$66, DW_AT_external
$C$DW$67	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Settings_AUD_Gateway_Current_Ringsaver")
	.dwattr $C$DW$67, DW_AT_TI_symbol_name("_ODP_Settings_AUD_Gateway_Current_Ringsaver")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$67, DW_AT_declaration
	.dwattr $C$DW$67, DW_AT_external
$C$DW$68	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_WarningMin")
	.dwattr $C$DW$68, DW_AT_TI_symbol_name("_ODP_Temperature_WarningMin")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$68, DW_AT_declaration
	.dwattr $C$DW$68, DW_AT_external
$C$DW$69	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$69, DW_AT_TI_symbol_name("_ODP_SafetyLimits_UnderCurrent")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_external
$C$DW$70	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_ChargeAllowed")
	.dwattr $C$DW$70, DW_AT_TI_symbol_name("_ODV_Current_ChargeAllowed")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$70, DW_AT_declaration
	.dwattr $C$DW$70, DW_AT_external
$C$DW$71	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Temperature_WarningMax")
	.dwattr $C$DW$71, DW_AT_TI_symbol_name("_ODP_Temperature_WarningMax")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$71, DW_AT_declaration
	.dwattr $C$DW$71, DW_AT_external
	.global	_FullCharge_Active
_FullCharge_Active:	.usect	".ebss",1,1,0
$C$DW$72	.dwtag  DW_TAG_variable, DW_AT_name("FullCharge_Active")
	.dwattr $C$DW$72, DW_AT_TI_symbol_name("_FullCharge_Active")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_addr _FullCharge_Active]
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$72, DW_AT_external
	.global	_NoCharge_Active
_NoCharge_Active:	.usect	".ebss",1,1,0
$C$DW$73	.dwtag  DW_TAG_variable, DW_AT_name("NoCharge_Active")
	.dwattr $C$DW$73, DW_AT_TI_symbol_name("_NoCharge_Active")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_addr _NoCharge_Active]
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$73, DW_AT_external
	.global	_Modules_Present
_Modules_Present:	.usect	".ebss",1,1,0
$C$DW$74	.dwtag  DW_TAG_variable, DW_AT_name("Modules_Present")
	.dwattr $C$DW$74, DW_AT_TI_symbol_name("_Modules_Present")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_addr _Modules_Present]
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$74, DW_AT_external
	.global	_TrickleCharge_Active
_TrickleCharge_Active:	.usect	".ebss",1,1,0
$C$DW$75	.dwtag  DW_TAG_variable, DW_AT_name("TrickleCharge_Active")
	.dwattr $C$DW$75, DW_AT_TI_symbol_name("_TrickleCharge_Active")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_addr _TrickleCharge_Active]
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$75, DW_AT_external
	.global	_Sync_count
_Sync_count:	.usect	".ebss",1,1,0
$C$DW$76	.dwtag  DW_TAG_variable, DW_AT_name("Sync_count")
	.dwattr $C$DW$76, DW_AT_TI_symbol_name("_Sync_count")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_addr _Sync_count]
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$76, DW_AT_external
	.global	_EventResetVoltH
_EventResetVoltH:	.usect	".ebss",1,1,0
$C$DW$77	.dwtag  DW_TAG_variable, DW_AT_name("EventResetVoltH")
	.dwattr $C$DW$77, DW_AT_TI_symbol_name("_EventResetVoltH")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_addr _EventResetVoltH]
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$77, DW_AT_external
	.global	_EventResetTemp
_EventResetTemp:	.usect	".ebss",1,1,0
$C$DW$78	.dwtag  DW_TAG_variable, DW_AT_name("EventResetTemp")
	.dwattr $C$DW$78, DW_AT_TI_symbol_name("_EventResetTemp")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_addr _EventResetTemp]
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$78, DW_AT_external
	.global	_Charger_Current_State
_Charger_Current_State:	.usect	".ebss",1,1,0
$C$DW$79	.dwtag  DW_TAG_variable, DW_AT_name("Charger_Current_State")
	.dwattr $C$DW$79, DW_AT_TI_symbol_name("_Charger_Current_State")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_addr _Charger_Current_State]
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$79, DW_AT_external
	.global	_EventResetVoltL
_EventResetVoltL:	.usect	".ebss",1,1,0
$C$DW$80	.dwtag  DW_TAG_variable, DW_AT_name("EventResetVoltL")
	.dwattr $C$DW$80, DW_AT_TI_symbol_name("_EventResetVoltL")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_addr _EventResetVoltL]
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$80, DW_AT_external
	.global	_BatteryVoltage
_BatteryVoltage:	.usect	".ebss",1,1,0
$C$DW$81	.dwtag  DW_TAG_variable, DW_AT_name("BatteryVoltage")
	.dwattr $C$DW$81, DW_AT_TI_symbol_name("_BatteryVoltage")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_addr _BatteryVoltage]
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$81, DW_AT_external
	.global	_OldGAT_max
_OldGAT_max:	.usect	".ebss",1,1,0
$C$DW$82	.dwtag  DW_TAG_variable, DW_AT_name("OldGAT_max")
	.dwattr $C$DW$82, DW_AT_TI_symbol_name("_OldGAT_max")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_addr _OldGAT_max]
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$82, DW_AT_external
	.global	_OldGAT_min
_OldGAT_min:	.usect	".ebss",1,1,0
$C$DW$83	.dwtag  DW_TAG_variable, DW_AT_name("OldGAT_min")
	.dwattr $C$DW$83, DW_AT_TI_symbol_name("_OldGAT_min")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_addr _OldGAT_min]
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$83, DW_AT_external
$C$DW$84	.dwtag  DW_TAG_variable, DW_AT_name("TimeLogIndex")
	.dwattr $C$DW$84, DW_AT_TI_symbol_name("_TimeLogIndex")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$84, DW_AT_declaration
	.dwattr $C$DW$84, DW_AT_external
	.global	_Delay_Error
_Delay_Error:	.usect	".ebss",1,1,0
$C$DW$85	.dwtag  DW_TAG_variable, DW_AT_name("Delay_Error")
	.dwattr $C$DW$85, DW_AT_TI_symbol_name("_Delay_Error")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_addr _Delay_Error]
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$85, DW_AT_external
_CANbus_ErrorResettick$1:	.usect	".ebss",1,1,0
	.global	_GAT_max
_GAT_max:	.usect	".ebss",1,1,0
$C$DW$86	.dwtag  DW_TAG_variable, DW_AT_name("GAT_max")
	.dwattr $C$DW$86, DW_AT_TI_symbol_name("_GAT_max")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_addr _GAT_max]
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$86, DW_AT_external
	.global	_GAT_min
_GAT_min:	.usect	".ebss",1,1,0
$C$DW$87	.dwtag  DW_TAG_variable, DW_AT_name("GAT_min")
	.dwattr $C$DW$87, DW_AT_TI_symbol_name("_GAT_min")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_addr _GAT_min]
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$87, DW_AT_external
	.global	_Oldmaxtemp
_Oldmaxtemp:	.usect	".ebss",1,1,0
$C$DW$88	.dwtag  DW_TAG_variable, DW_AT_name("Oldmaxtemp")
	.dwattr $C$DW$88, DW_AT_TI_symbol_name("_Oldmaxtemp")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_addr _Oldmaxtemp]
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$88, DW_AT_external
	.global	_Heater_status
_Heater_status:	.usect	".ebss",1,1,0
$C$DW$89	.dwtag  DW_TAG_variable, DW_AT_name("Heater_status")
	.dwattr $C$DW$89, DW_AT_TI_symbol_name("_Heater_status")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_addr _Heater_status]
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$89, DW_AT_external
	.global	_Oldmintemp
_Oldmintemp:	.usect	".ebss",1,1,0
$C$DW$90	.dwtag  DW_TAG_variable, DW_AT_name("Oldmintemp")
	.dwattr $C$DW$90, DW_AT_TI_symbol_name("_Oldmintemp")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_addr _Oldmintemp]
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$90, DW_AT_external
	.global	_TestCurrentEnable
_TestCurrentEnable:	.usect	".ebss",1,1,0
$C$DW$91	.dwtag  DW_TAG_variable, DW_AT_name("TestCurrentEnable")
	.dwattr $C$DW$91, DW_AT_TI_symbol_name("_TestCurrentEnable")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_addr _TestCurrentEnable]
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$91, DW_AT_external
$C$DW$92	.dwtag  DW_TAG_variable, DW_AT_name("HAL_NewCurPoint")
	.dwattr $C$DW$92, DW_AT_TI_symbol_name("_HAL_NewCurPoint")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$92, DW_AT_declaration
	.dwattr $C$DW$92, DW_AT_external
	.global	_InitOK
_InitOK:	.usect	".ebss",1,1,0
$C$DW$93	.dwtag  DW_TAG_variable, DW_AT_name("InitOK")
	.dwattr $C$DW$93, DW_AT_TI_symbol_name("_InitOK")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_addr _InitOK]
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$93, DW_AT_external
	.global	_UnderCurrent
_UnderCurrent:	.usect	".ebss",1,1,0
$C$DW$94	.dwtag  DW_TAG_variable, DW_AT_name("UnderCurrent")
	.dwattr $C$DW$94, DW_AT_TI_symbol_name("_UnderCurrent")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_addr _UnderCurrent]
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$94, DW_AT_external
	.global	_TestPositionEnable
_TestPositionEnable:	.usect	".ebss",1,1,0
$C$DW$95	.dwtag  DW_TAG_variable, DW_AT_name("TestPositionEnable")
	.dwattr $C$DW$95, DW_AT_TI_symbol_name("_TestPositionEnable")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_addr _TestPositionEnable]
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$95, DW_AT_external
$C$DW$96	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Version")
	.dwattr $C$DW$96, DW_AT_TI_symbol_name("_ODV_Version")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$96, DW_AT_declaration
	.dwattr $C$DW$96, DW_AT_external
$C$DW$97	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Current_DischargeAllowed")
	.dwattr $C$DW$97, DW_AT_TI_symbol_name("_ODV_Current_DischargeAllowed")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$97, DW_AT_declaration
	.dwattr $C$DW$97, DW_AT_external
$C$DW$98	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$98, DW_AT_TI_symbol_name("_ODV_Write_Outputs_16_Bit")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$210)
	.dwattr $C$DW$98, DW_AT_declaration
	.dwattr $C$DW$98, DW_AT_external
$C$DW$99	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Debug")
	.dwattr $C$DW$99, DW_AT_TI_symbol_name("_ODV_Debug")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$99, DW_AT_declaration
	.dwattr $C$DW$99, DW_AT_external
$C$DW$100	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Controlword")
	.dwattr $C$DW$100, DW_AT_TI_symbol_name("_ODV_Controlword")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$100, DW_AT_declaration
	.dwattr $C$DW$100, DW_AT_external
	.global	_CANbus_timer3
_CANbus_timer3:	.usect	".ebss",1,1,0
$C$DW$101	.dwtag  DW_TAG_variable, DW_AT_name("CANbus_timer3")
	.dwattr $C$DW$101, DW_AT_TI_symbol_name("_CANbus_timer3")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_addr _CANbus_timer3]
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$101, DW_AT_external
	.global	_CANbus_timer2
_CANbus_timer2:	.usect	".ebss",1,1,0
$C$DW$102	.dwtag  DW_TAG_variable, DW_AT_name("CANbus_timer2")
	.dwattr $C$DW$102, DW_AT_TI_symbol_name("_CANbus_timer2")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_addr _CANbus_timer2]
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$102, DW_AT_external
	.global	_CounterReset
_CounterReset:	.usect	".ebss",1,1,0
$C$DW$103	.dwtag  DW_TAG_variable, DW_AT_name("CounterReset")
	.dwattr $C$DW$103, DW_AT_TI_symbol_name("_CounterReset")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_addr _CounterReset]
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$103, DW_AT_external
	.global	_CANbus_ErrorReset
_CANbus_ErrorReset:	.usect	".ebss",1,1,0
$C$DW$104	.dwtag  DW_TAG_variable, DW_AT_name("CANbus_ErrorReset")
	.dwattr $C$DW$104, DW_AT_TI_symbol_name("_CANbus_ErrorReset")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_addr _CANbus_ErrorReset]
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$104, DW_AT_external
	.global	_CANComm_ErrorReset
_CANComm_ErrorReset:	.usect	".ebss",1,1,0
$C$DW$105	.dwtag  DW_TAG_variable, DW_AT_name("CANComm_ErrorReset")
	.dwattr $C$DW$105, DW_AT_TI_symbol_name("_CANComm_ErrorReset")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_addr _CANComm_ErrorReset]
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$105, DW_AT_external
	.global	_make10ms
_make10ms:	.usect	".ebss",1,1,0
$C$DW$106	.dwtag  DW_TAG_variable, DW_AT_name("make10ms")
	.dwattr $C$DW$106, DW_AT_TI_symbol_name("_make10ms")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_addr _make10ms]
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$106, DW_AT_external
	.global	_Charger_Previous_State
_Charger_Previous_State:	.usect	".ebss",1,1,0
$C$DW$107	.dwtag  DW_TAG_variable, DW_AT_name("Charger_Previous_State")
	.dwattr $C$DW$107, DW_AT_TI_symbol_name("_Charger_Previous_State")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_addr _Charger_Previous_State]
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$107, DW_AT_external
	.global	_ReceiveNew
_ReceiveNew:	.usect	".ebss",1,1,0
$C$DW$108	.dwtag  DW_TAG_variable, DW_AT_name("ReceiveNew")
	.dwattr $C$DW$108, DW_AT_TI_symbol_name("_ReceiveNew")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_addr _ReceiveNew]
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$108, DW_AT_external
	.global	_SequenceRunning
_SequenceRunning:	.usect	".ebss",1,1,0
$C$DW$109	.dwtag  DW_TAG_variable, DW_AT_name("SequenceRunning")
	.dwattr $C$DW$109, DW_AT_TI_symbol_name("_SequenceRunning")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_addr _SequenceRunning]
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$109, DW_AT_external
	.global	_CANbus_timer
_CANbus_timer:	.usect	".ebss",1,1,0
$C$DW$110	.dwtag  DW_TAG_variable, DW_AT_name("CANbus_timer")
	.dwattr $C$DW$110, DW_AT_TI_symbol_name("_CANbus_timer")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_addr _CANbus_timer]
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$110, DW_AT_external
$C$DW$111	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Multiunits")
	.dwattr $C$DW$111, DW_AT_TI_symbol_name("_ODV_Recorder_Multiunits")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$111, DW_AT_declaration
	.dwattr $C$DW$111, DW_AT_external
$C$DW$112	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Recorder_Variables")
	.dwattr $C$DW$112, DW_AT_TI_symbol_name("_ODV_Recorder_Variables")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$112, DW_AT_declaration
	.dwattr $C$DW$112, DW_AT_external

$C$DW$113	.dwtag  DW_TAG_subprogram, DW_AT_name("setState")
	.dwattr $C$DW$113, DW_AT_TI_symbol_name("_setState")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$113, DW_AT_declaration
	.dwattr $C$DW$113, DW_AT_external
$C$DW$114	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$97)
$C$DW$115	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$94)
	.dwendtag $C$DW$113


$C$DW$116	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_post")
	.dwattr $C$DW$116, DW_AT_TI_symbol_name("_MBX_post")
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$116, DW_AT_declaration
	.dwattr $C$DW$116, DW_AT_external
$C$DW$117	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$164)
$C$DW$118	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$171)
$C$DW$119	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$50)
	.dwendtag $C$DW$116


$C$DW$120	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddVariables")
	.dwattr $C$DW$120, DW_AT_TI_symbol_name("_PAR_AddVariables")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$120, DW_AT_declaration
	.dwattr $C$DW$120, DW_AT_external
$C$DW$121	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$120

$C$DW$122	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Temperature")
	.dwattr $C$DW$122, DW_AT_TI_symbol_name("_ODV_Gateway_Temperature")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$122, DW_AT_declaration
	.dwattr $C$DW$122, DW_AT_external
$C$DW$123	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_State")
	.dwattr $C$DW$123, DW_AT_TI_symbol_name("_ODV_Gateway_State")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$123, DW_AT_declaration
	.dwattr $C$DW$123, DW_AT_external
$C$DW$124	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Voltage")
	.dwattr $C$DW$124, DW_AT_TI_symbol_name("_ODV_Gateway_Voltage")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$124, DW_AT_declaration
	.dwattr $C$DW$124, DW_AT_external
$C$DW$125	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Current")
	.dwattr $C$DW$125, DW_AT_TI_symbol_name("_ODV_Gateway_Current")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$125, DW_AT_declaration
	.dwattr $C$DW$125, DW_AT_external
$C$DW$126	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SciSend")
	.dwattr $C$DW$126, DW_AT_TI_symbol_name("_ODV_SciSend")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$126, DW_AT_declaration
	.dwattr $C$DW$126, DW_AT_external

$C$DW$127	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_InitParam")
	.dwattr $C$DW$127, DW_AT_TI_symbol_name("_PAR_InitParam")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$127, DW_AT_declaration
	.dwattr $C$DW$127, DW_AT_external
$C$DW$128	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$97)
	.dwendtag $C$DW$127


$C$DW$129	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_AddMultiUnits")
	.dwattr $C$DW$129, DW_AT_TI_symbol_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$129, DW_AT_declaration
	.dwattr $C$DW$129, DW_AT_external
$C$DW$130	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$129


$C$DW$131	.dwtag  DW_TAG_subprogram, DW_AT_name("USB_Start")
	.dwattr $C$DW$131, DW_AT_TI_symbol_name("_USB_Start")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$131, DW_AT_declaration
	.dwattr $C$DW$131, DW_AT_external

$C$DW$132	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_UpdateCode")
	.dwattr $C$DW$132, DW_AT_TI_symbol_name("_PAR_UpdateCode")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$132, DW_AT_declaration
	.dwattr $C$DW$132, DW_AT_external
$C$DW$133	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$132


$C$DW$134	.dwtag  DW_TAG_subprogram, DW_AT_name("I2C_Command")
	.dwattr $C$DW$134, DW_AT_TI_symbol_name("_I2C_Command")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$134, DW_AT_declaration
	.dwattr $C$DW$134, DW_AT_external
$C$DW$135	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$9)
$C$DW$136	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$52)
$C$DW$137	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$9)
$C$DW$138	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$134

$C$DW$139	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Gateway_Relay_Error_Count")
	.dwattr $C$DW$139, DW_AT_TI_symbol_name("_ODP_Gateway_Relay_Error_Count")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$139, DW_AT_declaration
	.dwattr $C$DW$139, DW_AT_external
$C$DW$140	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Control_Heater")
	.dwattr $C$DW$140, DW_AT_TI_symbol_name("_ODV_Gateway_Control_Heater")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$140, DW_AT_declaration
	.dwattr $C$DW$140, DW_AT_external
$C$DW$141	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Gateway_Delay_Relay_Error")
	.dwattr $C$DW$141, DW_AT_TI_symbol_name("_ODP_Gateway_Delay_Relay_Error")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$141, DW_AT_declaration
	.dwattr $C$DW$141, DW_AT_external
$C$DW$142	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Alive_Counter")
	.dwattr $C$DW$142, DW_AT_TI_symbol_name("_ODV_Gateway_Alive_Counter")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$142, DW_AT_declaration
	.dwattr $C$DW$142, DW_AT_external
$C$DW$143	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Gateway_IsoResistor_Limit_Max")
	.dwattr $C$DW$143, DW_AT_TI_symbol_name("_ODP_Gateway_IsoResistor_Limit_Max")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$143, DW_AT_declaration
	.dwattr $C$DW$143, DW_AT_external
$C$DW$144	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Heater_Status")
	.dwattr $C$DW$144, DW_AT_TI_symbol_name("_ODV_Gateway_Heater_Status")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$144, DW_AT_declaration
	.dwattr $C$DW$144, DW_AT_external
$C$DW$145	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$145, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmin")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$145, DW_AT_declaration
	.dwattr $C$DW$145, DW_AT_external
$C$DW$146	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$146, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax_bal_delta")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$146, DW_AT_declaration
	.dwattr $C$DW$146, DW_AT_external
$C$DW$147	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$147, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Tmax")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$147, DW_AT_declaration
	.dwattr $C$DW$147, DW_AT_external
$C$DW$148	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umax")
	.dwattr $C$DW$148, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umax")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$148, DW_AT_declaration
	.dwattr $C$DW$148, DW_AT_external
$C$DW$149	.dwtag  DW_TAG_variable, DW_AT_name("ODP_SafetyLimits_Umin")
	.dwattr $C$DW$149, DW_AT_TI_symbol_name("_ODP_SafetyLimits_Umin")
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$149, DW_AT_declaration
	.dwattr $C$DW$149, DW_AT_external
$C$DW$150	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Requested_ChargerStage")
	.dwattr $C$DW$150, DW_AT_TI_symbol_name("_ODV_Gateway_Requested_ChargerStage")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$150, DW_AT_declaration
	.dwattr $C$DW$150, DW_AT_external

$C$DW$151	.dwtag  DW_TAG_subprogram, DW_AT_name("SEM_pend")
	.dwattr $C$DW$151, DW_AT_TI_symbol_name("_SEM_pend")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$151, DW_AT_declaration
	.dwattr $C$DW$151, DW_AT_external
$C$DW$152	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$170)
$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$50)
	.dwendtag $C$DW$151

$C$DW$154	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$154, DW_AT_TI_symbol_name("_ODV_Gateway_MaxDeltaCellVoltage")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$154, DW_AT_declaration
	.dwattr $C$DW$154, DW_AT_external
$C$DW$155	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MinModTemp")
	.dwattr $C$DW$155, DW_AT_TI_symbol_name("_ODV_Gateway_MinModTemp")
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$155, DW_AT_declaration
	.dwattr $C$DW$155, DW_AT_external
$C$DW$156	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$156, DW_AT_TI_symbol_name("_ODV_Gateway_MinCellVoltage")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$156, DW_AT_declaration
	.dwattr $C$DW$156, DW_AT_external
$C$DW$157	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$157, DW_AT_TI_symbol_name("_ODV_Gateway_MaxCellVoltage")
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$157, DW_AT_declaration
	.dwattr $C$DW$157, DW_AT_external

$C$DW$158	.dwtag  DW_TAG_subprogram, DW_AT_name("MBX_pend")
	.dwattr $C$DW$158, DW_AT_TI_symbol_name("_MBX_pend")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$158, DW_AT_declaration
	.dwattr $C$DW$158, DW_AT_external
$C$DW$159	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$164)
$C$DW$160	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$171)
$C$DW$161	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$50)
	.dwendtag $C$DW$158

$C$DW$162	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_MaxModTemp")
	.dwattr $C$DW$162, DW_AT_TI_symbol_name("_ODV_Gateway_MaxModTemp")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$162, DW_AT_declaration
	.dwattr $C$DW$162, DW_AT_external
$C$DW$163	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_LogNB")
	.dwattr $C$DW$163, DW_AT_TI_symbol_name("_ODV_Gateway_LogNB")
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$163, DW_AT_declaration
	.dwattr $C$DW$163, DW_AT_external
	.global	_TimerTemp
_TimerTemp:	.usect	".ebss",2,1,1
$C$DW$164	.dwtag  DW_TAG_variable, DW_AT_name("TimerTemp")
	.dwattr $C$DW$164, DW_AT_TI_symbol_name("_TimerTemp")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_addr _TimerTemp]
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$164, DW_AT_external
	.global	_GV_Temp_counter
_GV_Temp_counter:	.usect	".ebss",2,1,1
$C$DW$165	.dwtag  DW_TAG_variable, DW_AT_name("GV_Temp_counter")
	.dwattr $C$DW$165, DW_AT_TI_symbol_name("_GV_Temp_counter")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_addr _GV_Temp_counter]
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$165, DW_AT_external
	.global	_GV_Temp_counter1
_GV_Temp_counter1:	.usect	".ebss",2,1,1
$C$DW$166	.dwtag  DW_TAG_variable, DW_AT_name("GV_Temp_counter1")
	.dwattr $C$DW$166, DW_AT_TI_symbol_name("_GV_Temp_counter1")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_addr _GV_Temp_counter1]
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$166, DW_AT_external
	.global	_GV_current_counter
_GV_current_counter:	.usect	".ebss",2,1,1
$C$DW$167	.dwtag  DW_TAG_variable, DW_AT_name("GV_current_counter")
	.dwattr $C$DW$167, DW_AT_TI_symbol_name("_GV_current_counter")
	.dwattr $C$DW$167, DW_AT_location[DW_OP_addr _GV_current_counter]
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$167, DW_AT_external
	.global	_GV_Overvoltage_counter
_GV_Overvoltage_counter:	.usect	".ebss",2,1,1
$C$DW$168	.dwtag  DW_TAG_variable, DW_AT_name("GV_Overvoltage_counter")
	.dwattr $C$DW$168, DW_AT_TI_symbol_name("_GV_Overvoltage_counter")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_addr _GV_Overvoltage_counter]
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$168, DW_AT_external
	.global	_GV_Undervoltage_counter
_GV_Undervoltage_counter:	.usect	".ebss",2,1,1
$C$DW$169	.dwtag  DW_TAG_variable, DW_AT_name("GV_Undervoltage_counter")
	.dwattr $C$DW$169, DW_AT_TI_symbol_name("_GV_Undervoltage_counter")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_addr _GV_Undervoltage_counter]
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$169, DW_AT_external

$C$DW$170	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_StoreODSubIndex")
	.dwattr $C$DW$170, DW_AT_TI_symbol_name("_PAR_StoreODSubIndex")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$170, DW_AT_declaration
	.dwattr $C$DW$170, DW_AT_external
$C$DW$171	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$97)
$C$DW$172	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$9)
$C$DW$173	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$170


$C$DW$174	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteAllPermanentParam")
	.dwattr $C$DW$174, DW_AT_TI_symbol_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$174, DW_AT_declaration
	.dwattr $C$DW$174, DW_AT_external
$C$DW$175	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$97)
	.dwendtag $C$DW$174


$C$DW$176	.dwtag  DW_TAG_subprogram, DW_AT_name("getCRC32_cpu")
	.dwattr $C$DW$176, DW_AT_TI_symbol_name("_getCRC32_cpu")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$176, DW_AT_declaration
	.dwattr $C$DW$176, DW_AT_external
$C$DW$177	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$13)
$C$DW$178	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$106)
$C$DW$179	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$148)
$C$DW$180	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$9)
	.dwendtag $C$DW$176

	.global	_BoardODdata
_BoardODdata:	.usect	".ebss",2,1,1
$C$DW$181	.dwtag  DW_TAG_variable, DW_AT_name("BoardODdata")
	.dwattr $C$DW$181, DW_AT_TI_symbol_name("_BoardODdata")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_addr _BoardODdata]
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$181, DW_AT_external
	.global	_GV_Blinking_counter
_GV_Blinking_counter:	.usect	".ebss",2,1,1
$C$DW$182	.dwtag  DW_TAG_variable, DW_AT_name("GV_Blinking_counter")
	.dwattr $C$DW$182, DW_AT_TI_symbol_name("_GV_Blinking_counter")
	.dwattr $C$DW$182, DW_AT_location[DW_OP_addr _GV_Blinking_counter]
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$182, DW_AT_external

$C$DW$183	.dwtag  DW_TAG_subprogram, DW_AT_name("PAR_WriteStatisticParam")
	.dwattr $C$DW$183, DW_AT_TI_symbol_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$183, DW_AT_declaration
	.dwattr $C$DW$183, DW_AT_external
$C$DW$184	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$8)
	.dwendtag $C$DW$183

	.global	_BootNodeId
_BootNodeId:	.usect	"BootCommand",2,1,1
$C$DW$185	.dwtag  DW_TAG_variable, DW_AT_name("BootNodeId")
	.dwattr $C$DW$185, DW_AT_TI_symbol_name("_BootNodeId")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_addr _BootNodeId]
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$185, DW_AT_external
	.global	_BootCommand
_BootCommand:	.usect	"BootCommand",2,1,1
$C$DW$186	.dwtag  DW_TAG_variable, DW_AT_name("BootCommand")
	.dwattr $C$DW$186, DW_AT_TI_symbol_name("_BootCommand")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_addr _BootCommand]
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$186, DW_AT_external

$C$DW$187	.dwtag  DW_TAG_subprogram, DW_AT_name("CNV_Round")
	.dwattr $C$DW$187, DW_AT_TI_symbol_name("_CNV_Round")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$187, DW_AT_declaration
	.dwattr $C$DW$187, DW_AT_external
$C$DW$188	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$187

$C$DW$189	.dwtag  DW_TAG_variable, DW_AT_name("ODP_OnTime")
	.dwattr $C$DW$189, DW_AT_TI_symbol_name("_ODP_OnTime")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$189, DW_AT_declaration
	.dwattr $C$DW$189, DW_AT_external
$C$DW$190	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$190, DW_AT_TI_symbol_name("_ODV_ErrorDsp_ErrorNumber")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$190, DW_AT_declaration
	.dwattr $C$DW$190, DW_AT_external
$C$DW$191	.dwtag  DW_TAG_variable, DW_AT_name("ODV_StoreParameters")
	.dwattr $C$DW$191, DW_AT_TI_symbol_name("_ODV_StoreParameters")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$191, DW_AT_declaration
	.dwattr $C$DW$191, DW_AT_external
$C$DW$192	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RestoreDefaultParameters")
	.dwattr $C$DW$192, DW_AT_TI_symbol_name("_ODV_RestoreDefaultParameters")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$192, DW_AT_declaration
	.dwattr $C$DW$192, DW_AT_external
$C$DW$193	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Board_RevisionNumber")
	.dwattr $C$DW$193, DW_AT_TI_symbol_name("_ODP_Board_RevisionNumber")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$193, DW_AT_declaration
	.dwattr $C$DW$193, DW_AT_external
$C$DW$194	.dwtag  DW_TAG_variable, DW_AT_name("ODV_SysTick_ms")
	.dwattr $C$DW$194, DW_AT_TI_symbol_name("_ODV_SysTick_ms")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$194, DW_AT_declaration
	.dwattr $C$DW$194, DW_AT_external
$C$DW$195	.dwtag  DW_TAG_variable, DW_AT_name("ODP_Battery_Capacity")
	.dwattr $C$DW$195, DW_AT_TI_symbol_name("_ODP_Battery_Capacity")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$195, DW_AT_declaration
	.dwattr $C$DW$195, DW_AT_external
$C$DW$196	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Errorcode")
	.dwattr $C$DW$196, DW_AT_TI_symbol_name("_ODV_Gateway_Errorcode")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$196, DW_AT_declaration
	.dwattr $C$DW$196, DW_AT_external
$C$DW$197	.dwtag  DW_TAG_variable, DW_AT_name("ODP_RandomNB")
	.dwattr $C$DW$197, DW_AT_TI_symbol_name("_ODP_RandomNB")
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$197, DW_AT_declaration
	.dwattr $C$DW$197, DW_AT_external
$C$DW$198	.dwtag  DW_TAG_variable, DW_AT_name("MMSConfig")
	.dwattr $C$DW$198, DW_AT_TI_symbol_name("_MMSConfig")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$198, DW_AT_declaration
	.dwattr $C$DW$198, DW_AT_external
$C$DW$199	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_Total")
	.dwattr $C$DW$199, DW_AT_TI_symbol_name("_PAR_Capacity_Total")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$199, DW_AT_declaration
	.dwattr $C$DW$199, DW_AT_external
$C$DW$200	.dwtag  DW_TAG_variable, DW_AT_name("ODV_ResetHW")
	.dwattr $C$DW$200, DW_AT_TI_symbol_name("_ODV_ResetHW")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$200, DW_AT_declaration
	.dwattr $C$DW$200, DW_AT_external
$C$DW$201	.dwtag  DW_TAG_variable, DW_AT_name("ODV_RTC_Text")
	.dwattr $C$DW$201, DW_AT_TI_symbol_name("_ODV_RTC_Text")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$201, DW_AT_declaration
	.dwattr $C$DW$201, DW_AT_external
$C$DW$202	.dwtag  DW_TAG_variable, DW_AT_name("EVOCharger")
	.dwattr $C$DW$202, DW_AT_TI_symbol_name("_EVOCharger")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$202, DW_AT_declaration
	.dwattr $C$DW$202, DW_AT_external
$C$DW$203	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Gateway_Date_Time")
	.dwattr $C$DW$203, DW_AT_TI_symbol_name("_ODV_Gateway_Date_Time")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$203, DW_AT_declaration
	.dwattr $C$DW$203, DW_AT_external
$C$DW$204	.dwtag  DW_TAG_variable, DW_AT_name("PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$204, DW_AT_TI_symbol_name("_PAR_Capacity_TotalLife_Used")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$204, DW_AT_declaration
	.dwattr $C$DW$204, DW_AT_external
$C$DW$205	.dwtag  DW_TAG_variable, DW_AT_name("golden_CRC_values")
	.dwattr $C$DW$205, DW_AT_TI_symbol_name("_golden_CRC_values")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$205, DW_AT_declaration
	.dwattr $C$DW$205, DW_AT_external
	.global	_MasterData_Frame
_MasterData_Frame:	.usect	".ebss",11,1,0
$C$DW$206	.dwtag  DW_TAG_variable, DW_AT_name("MasterData_Frame")
	.dwattr $C$DW$206, DW_AT_TI_symbol_name("_MasterData_Frame")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_addr _MasterData_Frame]
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$206, DW_AT_external
	.global	_SlaveData_Frame
_SlaveData_Frame:	.usect	".ebss",11,1,0
$C$DW$207	.dwtag  DW_TAG_variable, DW_AT_name("SlaveData_Frame")
	.dwattr $C$DW$207, DW_AT_TI_symbol_name("_SlaveData_Frame")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_addr _SlaveData_Frame]
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$207, DW_AT_external
$C$DW$208	.dwtag  DW_TAG_variable, DW_AT_name("TSK_timerSem")
	.dwattr $C$DW$208, DW_AT_TI_symbol_name("_TSK_timerSem")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$208, DW_AT_declaration
	.dwattr $C$DW$208, DW_AT_external
$C$DW$209	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$209, DW_AT_TI_symbol_name("_ODV_Read_Analogue_Input_16_Bit")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$209, DW_AT_declaration
	.dwattr $C$DW$209, DW_AT_external
$C$DW$210	.dwtag  DW_TAG_variable, DW_AT_name("PieCtrlRegs")
	.dwattr $C$DW$210, DW_AT_TI_symbol_name("_PieCtrlRegs")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$210, DW_AT_declaration
	.dwattr $C$DW$210, DW_AT_external
$C$DW$211	.dwtag  DW_TAG_variable, DW_AT_name("GpioDataRegs")
	.dwattr $C$DW$211, DW_AT_TI_symbol_name("_GpioDataRegs")
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$211, DW_AT_declaration
	.dwattr $C$DW$211, DW_AT_external
$C$DW$212	.dwtag  DW_TAG_variable, DW_AT_name("mailboxSDOout")
	.dwattr $C$DW$212, DW_AT_TI_symbol_name("_mailboxSDOout")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$212, DW_AT_declaration
	.dwattr $C$DW$212, DW_AT_external
$C$DW$213	.dwtag  DW_TAG_variable, DW_AT_name("sci_rx_mbox")
	.dwattr $C$DW$213, DW_AT_TI_symbol_name("_sci_rx_mbox")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$213, DW_AT_declaration
	.dwattr $C$DW$213, DW_AT_external
$C$DW$214	.dwtag  DW_TAG_variable, DW_AT_name("can_rx_mbox")
	.dwattr $C$DW$214, DW_AT_TI_symbol_name("_can_rx_mbox")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$214, DW_AT_declaration
	.dwattr $C$DW$214, DW_AT_external
$C$DW$215	.dwtag  DW_TAG_variable, DW_AT_name("can_tx_mbox")
	.dwattr $C$DW$215, DW_AT_TI_symbol_name("_can_tx_mbox")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$215, DW_AT_declaration
	.dwattr $C$DW$215, DW_AT_external
$C$DW$216	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_MinCellVoltage")
	.dwattr $C$DW$216, DW_AT_TI_symbol_name("_ODV_Modules_MinCellVoltage")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$216, DW_AT_declaration
	.dwattr $C$DW$216, DW_AT_external
$C$DW$217	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Alarms")
	.dwattr $C$DW$217, DW_AT_TI_symbol_name("_ODV_Modules_Alarms")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$217, DW_AT_declaration
	.dwattr $C$DW$217, DW_AT_external
$C$DW$218	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Temperature_MIN")
	.dwattr $C$DW$218, DW_AT_TI_symbol_name("_ODV_Modules_Temperature_MIN")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$218, DW_AT_declaration
	.dwattr $C$DW$218, DW_AT_external
$C$DW$219	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Temperature")
	.dwattr $C$DW$219, DW_AT_TI_symbol_name("_ODV_Modules_Temperature")
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$219, DW_AT_declaration
	.dwattr $C$DW$219, DW_AT_external
$C$DW$220	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Heater")
	.dwattr $C$DW$220, DW_AT_TI_symbol_name("_ODV_Modules_Heater")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$220, DW_AT_declaration
	.dwattr $C$DW$220, DW_AT_external
$C$DW$221	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_MaxCellVoltage")
	.dwattr $C$DW$221, DW_AT_TI_symbol_name("_ODV_Modules_MaxCellVoltage")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$221, DW_AT_declaration
	.dwattr $C$DW$221, DW_AT_external
$C$DW$222	.dwtag  DW_TAG_variable, DW_AT_name("ODV_Modules_Voltage")
	.dwattr $C$DW$222, DW_AT_TI_symbol_name("_ODV_Modules_Voltage")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$209)
	.dwattr $C$DW$222, DW_AT_declaration
	.dwattr $C$DW$222, DW_AT_external
$C$DW$223	.dwtag  DW_TAG_variable, DW_AT_name("ODI_gateway_dict_Data")
	.dwattr $C$DW$223, DW_AT_TI_symbol_name("_ODI_gateway_dict_Data")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$223, DW_AT_declaration
	.dwattr $C$DW$223, DW_AT_external
;	C:\ti\ccsv5\tools\compiler\c2000_6.2.11\bin\ac2000.exe -@C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2509613 
	.sect	".text"
	.clink
	.global	_CheckfirmwareCRC

$C$DW$224	.dwtag  DW_TAG_subprogram, DW_AT_name("CheckfirmwareCRC")
	.dwattr $C$DW$224, DW_AT_low_pc(_CheckfirmwareCRC)
	.dwattr $C$DW$224, DW_AT_high_pc(0x00)
	.dwattr $C$DW$224, DW_AT_TI_symbol_name("_CheckfirmwareCRC")
	.dwattr $C$DW$224, DW_AT_external
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$224, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$224, DW_AT_TI_begin_line(0x56)
	.dwattr $C$DW$224, DW_AT_TI_begin_column(0x08)
	.dwattr $C$DW$224, DW_AT_TI_max_frame_size(-14)
	.dwpsn	file "../gateway.c",line 87,column 1,is_stmt,address _CheckfirmwareCRC

	.dwfde $C$DW$CIE, _CheckfirmwareCRC

;***************************************************************
;* FNAME: _CheckfirmwareCRC             FR SIZE:  12           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            1 Parameter, 11 Auto,  0 SOE     *
;***************************************************************

_CheckfirmwareCRC:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -14
$C$DW$225	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$225, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_breg20 -2]
$C$DW$226	.dwtag  DW_TAG_variable, DW_AT_name("temp")
	.dwattr $C$DW$226, DW_AT_TI_symbol_name("_temp")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_breg20 -4]
$C$DW$227	.dwtag  DW_TAG_variable, DW_AT_name("crc_rec")
	.dwattr $C$DW$227, DW_AT_TI_symbol_name("_crc_rec")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_breg20 -12]
	.dwpsn	file "../gateway.c",line 91,column 8,is_stmt
        MOV       *-SP[2],#0            ; [CPU_] |91| 
        B         $C$L3,UNC             ; [CPU_] |91| 
        ; branch occurs ; [] |91| 
$C$L1:    
	.dwpsn	file "../gateway.c",line 93,column 5,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVZ      AR4,SP                ; [CPU_U] |93| 
        MOVL      XAR7,#_golden_CRC_values+2 ; [CPU_U] |93| 
        MOV       ACC,*-SP[2] << 3      ; [CPU_] |93| 
        SUBB      XAR4,#12              ; [CPU_U] |93| 
        ADDL      XAR7,ACC              ; [CPU_] |93| 
        RPT       #7
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |93| 
	.dwpsn	file "../gateway.c",line 94,column 5,is_stmt
        MOV       ACC,*-SP[8] << #1     ; [CPU_] |94| 
        MOV       *-SP[1],AL            ; [CPU_] |94| 
        MOVL      XAR4,*-SP[10]         ; [CPU_] |94| 
        MOVB      XAR5,#0               ; [CPU_] |94| 
        MOVB      ACC,#0                ; [CPU_] |94| 
$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("_getCRC32_cpu")
	.dwattr $C$DW$228, DW_AT_TI_call
        LCR       #_getCRC32_cpu        ; [CPU_] |94| 
        ; call occurs [#_getCRC32_cpu] ; [] |94| 
        MOVL      *-SP[4],ACC           ; [CPU_] |94| 
	.dwpsn	file "../gateway.c",line 95,column 5,is_stmt
        MOVL      ACC,*-SP[6]           ; [CPU_] |95| 
        CMPL      ACC,*-SP[4]           ; [CPU_] |95| 
        BF        $C$L2,EQ              ; [CPU_] |95| 
        ; branchcc occurs ; [] |95| 
	.dwpsn	file "../gateway.c",line 96,column 7,is_stmt
        MOVB      AL,#0                 ; [CPU_] |96| 
        B         $C$L4,UNC             ; [CPU_] |96| 
        ; branch occurs ; [] |96| 
$C$L2:    
	.dwpsn	file "../gateway.c",line 91,column 47,is_stmt
        INC       *-SP[2]               ; [CPU_] |91| 
$C$L3:    
	.dwpsn	file "../gateway.c",line 91,column 15,is_stmt
        MOVW      DP,#_golden_CRC_values+1 ; [CPU_U] 
        MOV       AL,@_golden_CRC_values+1 ; [CPU_] |91| 
        CMP       AL,*-SP[2]            ; [CPU_] |91| 
        B         $C$L1,HI              ; [CPU_] |91| 
        ; branchcc occurs ; [] |91| 
	.dwpsn	file "../gateway.c",line 107,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |107| 
$C$L4:    
	.dwpsn	file "../gateway.c",line 108,column 1,is_stmt
        SUBB      SP,#12                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$224, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$224, DW_AT_TI_end_line(0x6c)
	.dwattr $C$DW$224, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$224

	.sect	".text"
	.clink
	.global	_FnResetNode

$C$DW$230	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetNode")
	.dwattr $C$DW$230, DW_AT_low_pc(_FnResetNode)
	.dwattr $C$DW$230, DW_AT_high_pc(0x00)
	.dwattr $C$DW$230, DW_AT_TI_symbol_name("_FnResetNode")
	.dwattr $C$DW$230, DW_AT_external
	.dwattr $C$DW$230, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$230, DW_AT_TI_begin_line(0x70)
	.dwattr $C$DW$230, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$230, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 112,column 29,is_stmt,address _FnResetNode

	.dwfde $C$DW$CIE, _FnResetNode
$C$DW$231	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$231, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$231, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnResetNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$232	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$232, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$232, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |112| 
	.dwpsn	file "../gateway.c",line 115,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$233	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$233, DW_AT_low_pc(0x00)
	.dwattr $C$DW$233, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$230, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$230, DW_AT_TI_end_line(0x73)
	.dwattr $C$DW$230, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$230

	.sect	".text"
	.clink
	.global	_FnResetCommunications

$C$DW$234	.dwtag  DW_TAG_subprogram, DW_AT_name("FnResetCommunications")
	.dwattr $C$DW$234, DW_AT_low_pc(_FnResetCommunications)
	.dwattr $C$DW$234, DW_AT_high_pc(0x00)
	.dwattr $C$DW$234, DW_AT_TI_symbol_name("_FnResetCommunications")
	.dwattr $C$DW$234, DW_AT_external
	.dwattr $C$DW$234, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$234, DW_AT_TI_begin_line(0x76)
	.dwattr $C$DW$234, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$234, DW_AT_TI_max_frame_size(-22)
	.dwpsn	file "../gateway.c",line 118,column 39,is_stmt,address _FnResetCommunications

	.dwfde $C$DW$CIE, _FnResetCommunications
$C$DW$235	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$235, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$235, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnResetCommunications        FR SIZE:  20           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 19 Auto,  0 SOE     *
;***************************************************************

_FnResetCommunications:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -22
$C$DW$236	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$236, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$236, DW_AT_location[DW_OP_breg20 -2]
$C$DW$237	.dwtag  DW_TAG_variable, DW_AT_name("node_id")
	.dwattr $C$DW$237, DW_AT_TI_symbol_name("_node_id")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_breg20 -3]
$C$DW$238	.dwtag  DW_TAG_variable, DW_AT_name("dummy_m")
	.dwattr $C$DW$238, DW_AT_TI_symbol_name("_dummy_m")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$224)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_breg20 -19]
        MOVL      *-SP[2],XAR4          ; [CPU_] |118| 
	.dwpsn	file "../gateway.c",line 123,column 3,is_stmt
$C$L5:    
        MOVZ      AR5,SP                ; [CPU_U] |123| 
        MOVL      XAR4,#_mailboxSDOout  ; [CPU_U] |123| 
        MOVB      AL,#0                 ; [CPU_] |123| 
        SUBB      XAR5,#19              ; [CPU_U] |123| 
$C$DW$239	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$239, DW_AT_low_pc(0x00)
	.dwattr $C$DW$239, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$239, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |123| 
        ; call occurs [#_MBX_pend] ; [] |123| 
        CMPB      AL,#0                 ; [CPU_] |123| 
        BF        $C$L5,NEQ             ; [CPU_] |123| 
        ; branchcc occurs ; [] |123| 
	.dwpsn	file "../gateway.c",line 124,column 3,is_stmt
$C$L6:    
        MOVZ      AR5,SP                ; [CPU_U] |124| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |124| 
        MOVB      AL,#0                 ; [CPU_] |124| 
        SUBB      XAR5,#19              ; [CPU_U] |124| 
$C$DW$240	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$240, DW_AT_low_pc(0x00)
	.dwattr $C$DW$240, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$240, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |124| 
        ; call occurs [#_MBX_pend] ; [] |124| 
        CMPB      AL,#0                 ; [CPU_] |124| 
        BF        $C$L6,NEQ             ; [CPU_] |124| 
        ; branchcc occurs ; [] |124| 
	.dwpsn	file "../gateway.c",line 125,column 3,is_stmt
$C$L7:    
        MOVZ      AR5,SP                ; [CPU_U] |125| 
        MOVL      XAR4,#_can_rx_mbox    ; [CPU_U] |125| 
        MOVB      AL,#0                 ; [CPU_] |125| 
        SUBB      XAR5,#19              ; [CPU_U] |125| 
$C$DW$241	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$241, DW_AT_low_pc(0x00)
	.dwattr $C$DW$241, DW_AT_name("_MBX_pend")
	.dwattr $C$DW$241, DW_AT_TI_call
        LCR       #_MBX_pend            ; [CPU_] |125| 
        ; call occurs [#_MBX_pend] ; [] |125| 
        CMPB      AL,#0                 ; [CPU_] |125| 
        BF        $C$L7,NEQ             ; [CPU_] |125| 
        ; branchcc occurs ; [] |125| 
	.dwpsn	file "../gateway.c",line 127,column 3,is_stmt
        MOVW      DP,#_ODP_Board_RevisionNumber ; [CPU_U] 
        MOV       AL,@_ODP_Board_RevisionNumber ; [CPU_] |127| 
        MOV       *-SP[3],AL            ; [CPU_] |127| 
	.dwpsn	file "../gateway.c",line 134,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |134| 
$C$DW$242	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$242, DW_AT_low_pc(0x00)
	.dwattr $C$DW$242, DW_AT_name("_setNodeId")
	.dwattr $C$DW$242, DW_AT_TI_call
        LCR       #_setNodeId           ; [CPU_] |134| 
        ; call occurs [#_setNodeId] ; [] |134| 
	.dwpsn	file "../gateway.c",line 137,column 3,is_stmt
        MOV       AH,*-SP[3]            ; [CPU_] |137| 
        MOVW      DP,#_ODP_Board_BaudRate ; [CPU_U] 
        MOV       AL,@_ODP_Board_BaudRate ; [CPU_] |137| 
$C$DW$243	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$243, DW_AT_low_pc(0x00)
	.dwattr $C$DW$243, DW_AT_name("_canInit")
	.dwattr $C$DW$243, DW_AT_TI_call
        LCR       #_canInit             ; [CPU_] |137| 
        ; call occurs [#_canInit] ; [] |137| 
	.dwpsn	file "../gateway.c",line 139,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |139| 
$C$DW$244	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$244, DW_AT_low_pc(0x00)
	.dwattr $C$DW$244, DW_AT_name("_DIC_SetNodeId")
	.dwattr $C$DW$244, DW_AT_TI_call
        LCR       #_DIC_SetNodeId       ; [CPU_] |139| 
        ; call occurs [#_DIC_SetNodeId] ; [] |139| 
	.dwpsn	file "../gateway.c",line 140,column 1,is_stmt
        SUBB      SP,#20                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$245	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$245, DW_AT_low_pc(0x00)
	.dwattr $C$DW$245, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$234, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$234, DW_AT_TI_end_line(0x8c)
	.dwattr $C$DW$234, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$234

	.sect	".text"
	.clink
	.global	_FnInitialiseNode

$C$DW$246	.dwtag  DW_TAG_subprogram, DW_AT_name("FnInitialiseNode")
	.dwattr $C$DW$246, DW_AT_low_pc(_FnInitialiseNode)
	.dwattr $C$DW$246, DW_AT_high_pc(0x00)
	.dwattr $C$DW$246, DW_AT_TI_symbol_name("_FnInitialiseNode")
	.dwattr $C$DW$246, DW_AT_external
	.dwattr $C$DW$246, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$246, DW_AT_TI_begin_line(0x92)
	.dwattr $C$DW$246, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$246, DW_AT_TI_max_frame_size(-6)
	.dwpsn	file "../gateway.c",line 146,column 35,is_stmt,address _FnInitialiseNode

	.dwfde $C$DW$CIE, _FnInitialiseNode
$C$DW$247	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$247, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$247, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnInitialiseNode             FR SIZE:   4           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  3 Auto,  0 SOE     *
;***************************************************************

_FnInitialiseNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -6
$C$DW$248	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$248, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$248, DW_AT_location[DW_OP_breg20 -2]
$C$DW$249	.dwtag  DW_TAG_variable, DW_AT_name("CrcFirmwareTest")
	.dwattr $C$DW$249, DW_AT_TI_symbol_name("_CrcFirmwareTest")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_breg20 -3]
        MOVL      *-SP[2],XAR4          ; [CPU_] |146| 
	.dwpsn	file "../gateway.c",line 147,column 26,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |147| 
	.dwpsn	file "../gateway.c",line 149,column 3,is_stmt
$C$DW$250	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$250, DW_AT_low_pc(0x00)
	.dwattr $C$DW$250, DW_AT_name("_genCRC32Table")
	.dwattr $C$DW$250, DW_AT_TI_call
        LCR       #_genCRC32Table       ; [CPU_] |149| 
        ; call occurs [#_genCRC32Table] ; [] |149| 
	.dwpsn	file "../gateway.c",line 151,column 3,is_stmt
$C$DW$251	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$251, DW_AT_low_pc(0x00)
	.dwattr $C$DW$251, DW_AT_name("_CheckfirmwareCRC")
	.dwattr $C$DW$251, DW_AT_TI_call
        LCR       #_CheckfirmwareCRC    ; [CPU_] |151| 
        ; call occurs [#_CheckfirmwareCRC] ; [] |151| 
        MOV       *-SP[3],AL            ; [CPU_] |151| 
	.dwpsn	file "../gateway.c",line 152,column 3,is_stmt
        MOVW      DP,#_PieCtrlRegs+19   ; [CPU_U] 
        OR        @_PieCtrlRegs+19,#0x0020 ; [CPU_] |152| 
	.dwpsn	file "../gateway.c",line 153,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |153| 
	.dwpsn	file "../gateway.c",line 154,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,#_ODP_Board_Config ; [CPU_U] |154| 
        MOVL      @_MMSConfig,XAR4      ; [CPU_] |154| 
	.dwpsn	file "../gateway.c",line 156,column 3,is_stmt
        MOVL      XAR4,*-SP[2]          ; [CPU_] |156| 
$C$DW$252	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$252, DW_AT_low_pc(0x00)
	.dwattr $C$DW$252, DW_AT_name("_PAR_InitParam")
	.dwattr $C$DW$252, DW_AT_TI_call
        LCR       #_PAR_InitParam       ; [CPU_] |156| 
        ; call occurs [#_PAR_InitParam] ; [] |156| 
        CMPB      AL,#0                 ; [CPU_] |156| 
        BF        $C$L8,EQ              ; [CPU_] |156| 
        ; branchcc occurs ; [] |156| 
        MOV       AL,*-SP[3]            ; [CPU_] |156| 
        CMPB      AL,#1                 ; [CPU_] |156| 
        BF        $C$L8,NEQ             ; [CPU_] |156| 
        ; branchcc occurs ; [] |156| 
	.dwpsn	file "../gateway.c",line 158,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#1,UNC       ; [CPU_] |158| 
	.dwpsn	file "../gateway.c",line 159,column 3,is_stmt
        B         $C$L9,UNC             ; [CPU_] |159| 
        ; branch occurs ; [] |159| 
$C$L8:    
	.dwpsn	file "../gateway.c",line 161,column 5,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOVB      @_InitOK,#2,UNC       ; [CPU_] |161| 
$C$L9:    
	.dwpsn	file "../gateway.c",line 164,column 3,is_stmt
$C$DW$253	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$253, DW_AT_low_pc(0x00)
	.dwattr $C$DW$253, DW_AT_name("_PAR_SetParamDependantVars")
	.dwattr $C$DW$253, DW_AT_TI_call
        LCR       #_PAR_SetParamDependantVars ; [CPU_] |164| 
        ; call occurs [#_PAR_SetParamDependantVars] ; [] |164| 
	.dwpsn	file "../gateway.c",line 165,column 1,is_stmt
        SUBB      SP,#4                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$254	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$254, DW_AT_low_pc(0x00)
	.dwattr $C$DW$254, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$246, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$246, DW_AT_TI_end_line(0xa5)
	.dwattr $C$DW$246, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$246

	.sect	".text"
	.clink
	.global	_FnEnterPreOperational

$C$DW$255	.dwtag  DW_TAG_subprogram, DW_AT_name("FnEnterPreOperational")
	.dwattr $C$DW$255, DW_AT_low_pc(_FnEnterPreOperational)
	.dwattr $C$DW$255, DW_AT_high_pc(0x00)
	.dwattr $C$DW$255, DW_AT_TI_symbol_name("_FnEnterPreOperational")
	.dwattr $C$DW$255, DW_AT_external
	.dwattr $C$DW$255, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$255, DW_AT_TI_begin_line(0xa9)
	.dwattr $C$DW$255, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$255, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 169,column 40,is_stmt,address _FnEnterPreOperational

	.dwfde $C$DW$CIE, _FnEnterPreOperational
$C$DW$256	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$256, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$256, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnEnterPreOperational        FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnEnterPreOperational:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$257	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$257, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$257, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |169| 
	.dwpsn	file "../gateway.c",line 170,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOVB      @_ODV_Controlword,#6,UNC ; [CPU_] |170| 
	.dwpsn	file "../gateway.c",line 171,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$258	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$258, DW_AT_low_pc(0x00)
	.dwattr $C$DW$258, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$255, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$255, DW_AT_TI_end_line(0xab)
	.dwattr $C$DW$255, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$255

	.sect	".text"
	.clink
	.global	_FnStartNode

$C$DW$259	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStartNode")
	.dwattr $C$DW$259, DW_AT_low_pc(_FnStartNode)
	.dwattr $C$DW$259, DW_AT_high_pc(0x00)
	.dwattr $C$DW$259, DW_AT_TI_symbol_name("_FnStartNode")
	.dwattr $C$DW$259, DW_AT_external
	.dwattr $C$DW$259, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$259, DW_AT_TI_begin_line(0xae)
	.dwattr $C$DW$259, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$259, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 174,column 30,is_stmt,address _FnStartNode

	.dwfde $C$DW$CIE, _FnStartNode
$C$DW$260	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$260, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$260, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStartNode                  FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStartNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$261	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$261, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |174| 
	.dwpsn	file "../gateway.c",line 176,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$262	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$262, DW_AT_low_pc(0x00)
	.dwattr $C$DW$262, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$259, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$259, DW_AT_TI_end_line(0xb0)
	.dwattr $C$DW$259, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$259

	.sect	".text"
	.clink
	.global	_ChargerCommError

$C$DW$263	.dwtag  DW_TAG_subprogram, DW_AT_name("ChargerCommError")
	.dwattr $C$DW$263, DW_AT_low_pc(_ChargerCommError)
	.dwattr $C$DW$263, DW_AT_high_pc(0x00)
	.dwattr $C$DW$263, DW_AT_TI_symbol_name("_ChargerCommError")
	.dwattr $C$DW$263, DW_AT_external
	.dwattr $C$DW$263, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$263, DW_AT_TI_begin_line(0xb2)
	.dwattr $C$DW$263, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$263, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../gateway.c",line 178,column 25,is_stmt,address _ChargerCommError

	.dwfde $C$DW$CIE, _ChargerCommError
$C$DW$264	.dwtag  DW_TAG_variable, DW_AT_name("CANbus_ErrorResettick")
	.dwattr $C$DW$264, DW_AT_TI_symbol_name("_CANbus_ErrorResettick$1")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_addr _CANbus_ErrorResettick$1]

;***************************************************************
;* FNAME: _ChargerCommError             FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_ChargerCommError:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../gateway.c",line 181,column 2,is_stmt
        MOVW      DP,#_ODV_ChargerData_MasterLimTemp ; [CPU_U] 
        MOV       AL,@_ODV_ChargerData_MasterLimTemp ; [CPU_] |181| 
        BF        $C$L10,EQ             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
        MOVW      DP,#_ODV_ChargerData_SlaveLimTemp ; [CPU_U] 
        MOV       AL,@_ODV_ChargerData_SlaveLimTemp ; [CPU_] |181| 
        BF        $C$L10,EQ             ; [CPU_] |181| 
        ; branchcc occurs ; [] |181| 
	.dwpsn	file "../gateway.c",line 182,column 3,is_stmt
        MOVW      DP,#_ODV_ChargerData_MasterLimTemp ; [CPU_U] 
        MOV       @_ODV_ChargerData_MasterLimTemp,#0 ; [CPU_] |182| 
	.dwpsn	file "../gateway.c",line 183,column 3,is_stmt
        MOVW      DP,#_ODV_ChargerData_SlaveLimTemp ; [CPU_U] 
        MOV       @_ODV_ChargerData_SlaveLimTemp,#0 ; [CPU_] |183| 
	.dwpsn	file "../gateway.c",line 184,column 3,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        AND       @_ODV_ChargerData_ChargerError,#0xff7f ; [CPU_] |184| 
	.dwpsn	file "../gateway.c",line 185,column 3,is_stmt
        MOVW      DP,#_CANbus_ErrorResettick$1 ; [CPU_U] 
        MOV       @_CANbus_ErrorResettick$1,#0 ; [CPU_] |185| 
	.dwpsn	file "../gateway.c",line 186,column 2,is_stmt
        B         $C$L11,UNC            ; [CPU_] |186| 
        ; branch occurs ; [] |186| 
$C$L10:    
	.dwpsn	file "../gateway.c",line 187,column 3,is_stmt
        MOVW      DP,#_CANbus_ErrorResettick$1 ; [CPU_U] 
        MOV       AL,@_CANbus_ErrorResettick$1 ; [CPU_] |187| 
        CMPB      AL,#2                 ; [CPU_] |187| 
        B         $C$L11,LEQ            ; [CPU_] |187| 
        ; branchcc occurs ; [] |187| 
	.dwpsn	file "../gateway.c",line 188,column 4,is_stmt
        INC       @_CANbus_ErrorResettick$1 ; [CPU_] |188| 
	.dwpsn	file "../gateway.c",line 189,column 4,is_stmt
        MOVW      DP,#_ODV_ChargerData_ChargerError ; [CPU_U] 
        OR        @_ODV_ChargerData_ChargerError,#0x0080 ; [CPU_] |189| 
	.dwpsn	file "../gateway.c",line 190,column 4,is_stmt
        MOVW      DP,#_CANbus_ErrorResettick$1 ; [CPU_U] 
        MOV       @_CANbus_ErrorResettick$1,#0 ; [CPU_] |190| 
	.dwpsn	file "../gateway.c",line 208,column 1,is_stmt
$C$L11:    
$C$DW$265	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$265, DW_AT_low_pc(0x00)
	.dwattr $C$DW$265, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$263, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$263, DW_AT_TI_end_line(0xd0)
	.dwattr $C$DW$263, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$263

	.sect	".text"
	.clink
	.global	_InitialReset

$C$DW$266	.dwtag  DW_TAG_subprogram, DW_AT_name("InitialReset")
	.dwattr $C$DW$266, DW_AT_low_pc(_InitialReset)
	.dwattr $C$DW$266, DW_AT_high_pc(0x00)
	.dwattr $C$DW$266, DW_AT_TI_symbol_name("_InitialReset")
	.dwattr $C$DW$266, DW_AT_external
	.dwattr $C$DW$266, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$266, DW_AT_TI_begin_line(0xd2)
	.dwattr $C$DW$266, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$266, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../gateway.c",line 210,column 21,is_stmt,address _InitialReset

	.dwfde $C$DW$CIE, _InitialReset

;***************************************************************
;* FNAME: _InitialReset                 FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_InitialReset:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../gateway.c",line 211,column 2,is_stmt
        MOVW      DP,#_NoCharge_Active  ; [CPU_U] 
        MOVB      @_NoCharge_Active,#1,UNC ; [CPU_] |211| 
	.dwpsn	file "../gateway.c",line 212,column 2,is_stmt
        MOVB      @_FullCharge_Active,#1,UNC ; [CPU_] |212| 
	.dwpsn	file "../gateway.c",line 213,column 2,is_stmt
        MOVB      @_TrickleCharge_Active,#1,UNC ; [CPU_] |213| 
	.dwpsn	file "../gateway.c",line 214,column 1,is_stmt
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$266, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$266, DW_AT_TI_end_line(0xd6)
	.dwattr $C$DW$266, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$266

	.sect	".text"
	.clink
	.global	_CheckState_Change

$C$DW$268	.dwtag  DW_TAG_subprogram, DW_AT_name("CheckState_Change")
	.dwattr $C$DW$268, DW_AT_low_pc(_CheckState_Change)
	.dwattr $C$DW$268, DW_AT_high_pc(0x00)
	.dwattr $C$DW$268, DW_AT_TI_symbol_name("_CheckState_Change")
	.dwattr $C$DW$268, DW_AT_external
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$268, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$268, DW_AT_TI_begin_line(0xd7)
	.dwattr $C$DW$268, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$268, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 215,column 48,is_stmt,address _CheckState_Change

	.dwfde $C$DW$CIE, _CheckState_Change
$C$DW$269	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Present")
	.dwattr $C$DW$269, DW_AT_TI_symbol_name("_Present")
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$269, DW_AT_location[DW_OP_reg0]
$C$DW$270	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Next")
	.dwattr $C$DW$270, DW_AT_TI_symbol_name("_Next")
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$270, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _CheckState_Change            FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_CheckState_Change:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$271	.dwtag  DW_TAG_variable, DW_AT_name("Present")
	.dwattr $C$DW$271, DW_AT_TI_symbol_name("_Present")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$271, DW_AT_location[DW_OP_breg20 -1]
$C$DW$272	.dwtag  DW_TAG_variable, DW_AT_name("Next")
	.dwattr $C$DW$272, DW_AT_TI_symbol_name("_Next")
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$272, DW_AT_location[DW_OP_breg20 -2]
        MOV       *-SP[2],AH            ; [CPU_] |215| 
        MOV       *-SP[1],AL            ; [CPU_] |215| 
	.dwpsn	file "../gateway.c",line 216,column 2,is_stmt
        MOV       AL,*-SP[2]            ; [CPU_] |216| 
        CMP       AL,*-SP[1]            ; [CPU_] |216| 
        BF        $C$L12,EQ             ; [CPU_] |216| 
        ; branchcc occurs ; [] |216| 
	.dwpsn	file "../gateway.c",line 217,column 3,is_stmt
        MOVB      AL,#1                 ; [CPU_] |217| 
        B         $C$L13,UNC            ; [CPU_] |217| 
        ; branch occurs ; [] |217| 
$C$L12:    
	.dwpsn	file "../gateway.c",line 218,column 9,is_stmt
        MOVB      AL,#0                 ; [CPU_] |218| 
$C$L13:    
	.dwpsn	file "../gateway.c",line 219,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$273	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$273, DW_AT_low_pc(0x00)
	.dwattr $C$DW$273, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$268, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$268, DW_AT_TI_end_line(0xdb)
	.dwattr $C$DW$268, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$268

	.sect	".text"
	.clink
	.global	_Nocharge_Mode

$C$DW$274	.dwtag  DW_TAG_subprogram, DW_AT_name("Nocharge_Mode")
	.dwattr $C$DW$274, DW_AT_low_pc(_Nocharge_Mode)
	.dwattr $C$DW$274, DW_AT_high_pc(0x00)
	.dwattr $C$DW$274, DW_AT_TI_symbol_name("_Nocharge_Mode")
	.dwattr $C$DW$274, DW_AT_external
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$274, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$274, DW_AT_TI_begin_line(0xdd)
	.dwattr $C$DW$274, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$274, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 221,column 32,is_stmt,address _Nocharge_Mode

	.dwfde $C$DW$CIE, _Nocharge_Mode
$C$DW$275	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Voltage")
	.dwattr $C$DW$275, DW_AT_TI_symbol_name("_Voltage")
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$275, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _Nocharge_Mode                FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_Nocharge_Mode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$276	.dwtag  DW_TAG_variable, DW_AT_name("Voltage")
	.dwattr $C$DW$276, DW_AT_TI_symbol_name("_Voltage")
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$276, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |221| 
	.dwpsn	file "../gateway.c",line 222,column 2,is_stmt
        CMP       AL,#761               ; [CPU_] |222| 
        B         $C$L14,LT             ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
        MOVW      DP,#_NoCharge_Active  ; [CPU_U] 
        MOV       AL,@_NoCharge_Active  ; [CPU_] |222| 
        BF        $C$L15,NEQ            ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
$C$L14:    
        MOV       AL,*-SP[1]            ; [CPU_] |222| 
        CMP       AL,#953               ; [CPU_] |222| 
        B         $C$L16,LEQ            ; [CPU_] |222| 
        ; branchcc occurs ; [] |222| 
$C$L15:    
	.dwpsn	file "../gateway.c",line 223,column 3,is_stmt
        MOVW      DP,#_NoCharge_Active  ; [CPU_U] 
        MOVB      @_NoCharge_Active,#1,UNC ; [CPU_] |223| 
	.dwpsn	file "../gateway.c",line 224,column 2,is_stmt
        B         $C$L17,UNC            ; [CPU_] |224| 
        ; branch occurs ; [] |224| 
$C$L16:    
	.dwpsn	file "../gateway.c",line 224,column 9,is_stmt
        CMP       AL,#761               ; [CPU_] |224| 
        B         $C$L17,GEQ            ; [CPU_] |224| 
        ; branchcc occurs ; [] |224| 
	.dwpsn	file "../gateway.c",line 225,column 3,is_stmt
        MOVW      DP,#_NoCharge_Active  ; [CPU_U] 
        MOV       @_NoCharge_Active,#0  ; [CPU_] |225| 
$C$L17:    
	.dwpsn	file "../gateway.c",line 228,column 2,is_stmt
        MOVW      DP,#_NoCharge_Active  ; [CPU_U] 
        MOV       AL,@_NoCharge_Active  ; [CPU_] |228| 
        BF        $C$L18,NEQ            ; [CPU_] |228| 
        ; branchcc occurs ; [] |228| 
	.dwpsn	file "../gateway.c",line 228,column 23,is_stmt
        MOVB      AL,#200               ; [CPU_] |228| 
        B         $C$L19,UNC            ; [CPU_] |228| 
        ; branch occurs ; [] |228| 
$C$L18:    
	.dwpsn	file "../gateway.c",line 230,column 2,is_stmt
        MOVB      AL,#0                 ; [CPU_] |230| 
$C$L19:    
	.dwpsn	file "../gateway.c",line 231,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$277	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$277, DW_AT_low_pc(0x00)
	.dwattr $C$DW$277, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$274, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$274, DW_AT_TI_end_line(0xe7)
	.dwattr $C$DW$274, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$274

	.sect	".text"
	.clink
	.global	_FullCharge_Mode

$C$DW$278	.dwtag  DW_TAG_subprogram, DW_AT_name("FullCharge_Mode")
	.dwattr $C$DW$278, DW_AT_low_pc(_FullCharge_Mode)
	.dwattr $C$DW$278, DW_AT_high_pc(0x00)
	.dwattr $C$DW$278, DW_AT_TI_symbol_name("_FullCharge_Mode")
	.dwattr $C$DW$278, DW_AT_external
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$278, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$278, DW_AT_TI_begin_line(0xe9)
	.dwattr $C$DW$278, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$278, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 233,column 33,is_stmt,address _FullCharge_Mode

	.dwfde $C$DW$CIE, _FullCharge_Mode
$C$DW$279	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Voltage")
	.dwattr $C$DW$279, DW_AT_TI_symbol_name("_Voltage")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$279, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _FullCharge_Mode              FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_FullCharge_Mode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$280	.dwtag  DW_TAG_variable, DW_AT_name("Voltage")
	.dwattr $C$DW$280, DW_AT_TI_symbol_name("_Voltage")
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$280, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |233| 
	.dwpsn	file "../gateway.c",line 234,column 2,is_stmt
        CMP       AL,#953               ; [CPU_] |234| 
        B         $C$L20,GT             ; [CPU_] |234| 
        ; branchcc occurs ; [] |234| 
        MOVW      DP,#_FullCharge_Active ; [CPU_U] 
        MOV       AL,@_FullCharge_Active ; [CPU_] |234| 
        BF        $C$L20,EQ             ; [CPU_] |234| 
        ; branchcc occurs ; [] |234| 
	.dwpsn	file "../gateway.c",line 235,column 3,is_stmt
        MOVB      @_FullCharge_Active,#1,UNC ; [CPU_] |235| 
	.dwpsn	file "../gateway.c",line 236,column 2,is_stmt
        B         $C$L22,UNC            ; [CPU_] |236| 
        ; branch occurs ; [] |236| 
$C$L20:    
	.dwpsn	file "../gateway.c",line 236,column 9,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |236| 
        CMP       AL,#953               ; [CPU_] |236| 
        B         $C$L21,LEQ            ; [CPU_] |236| 
        ; branchcc occurs ; [] |236| 
	.dwpsn	file "../gateway.c",line 237,column 3,is_stmt
        MOVW      DP,#_FullCharge_Active ; [CPU_U] 
        MOV       @_FullCharge_Active,#0 ; [CPU_] |237| 
	.dwpsn	file "../gateway.c",line 238,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |238| 
        B         $C$L24,UNC            ; [CPU_] |238| 
        ; branch occurs ; [] |238| 
$C$L21:    
	.dwpsn	file "../gateway.c",line 239,column 9,is_stmt
        CMP       AL,#943               ; [CPU_] |239| 
        B         $C$L22,GEQ            ; [CPU_] |239| 
        ; branchcc occurs ; [] |239| 
        MOVW      DP,#_FullCharge_Active ; [CPU_U] 
        MOV       AL,@_FullCharge_Active ; [CPU_] |239| 
        BF        $C$L22,NEQ            ; [CPU_] |239| 
        ; branchcc occurs ; [] |239| 
	.dwpsn	file "../gateway.c",line 240,column 3,is_stmt
        MOVB      @_FullCharge_Active,#1,UNC ; [CPU_] |240| 
$C$L22:    
	.dwpsn	file "../gateway.c",line 243,column 2,is_stmt
        MOVW      DP,#_FullCharge_Active ; [CPU_U] 
        MOV       AL,@_FullCharge_Active ; [CPU_] |243| 
        BF        $C$L23,EQ             ; [CPU_] |243| 
        ; branchcc occurs ; [] |243| 
	.dwpsn	file "../gateway.c",line 243,column 24,is_stmt
        MOVB      AL,#200               ; [CPU_] |243| 
        B         $C$L24,UNC            ; [CPU_] |243| 
        ; branch occurs ; [] |243| 
$C$L23:    
	.dwpsn	file "../gateway.c",line 245,column 2,is_stmt
        MOVB      AL,#0                 ; [CPU_] |245| 
$C$L24:    
	.dwpsn	file "../gateway.c",line 246,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$278, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$278, DW_AT_TI_end_line(0xf6)
	.dwattr $C$DW$278, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$278

	.sect	".text"
	.clink
	.global	_TrickleCharge_Mode

$C$DW$282	.dwtag  DW_TAG_subprogram, DW_AT_name("TrickleCharge_Mode")
	.dwattr $C$DW$282, DW_AT_low_pc(_TrickleCharge_Mode)
	.dwattr $C$DW$282, DW_AT_high_pc(0x00)
	.dwattr $C$DW$282, DW_AT_TI_symbol_name("_TrickleCharge_Mode")
	.dwattr $C$DW$282, DW_AT_external
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$282, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$282, DW_AT_TI_begin_line(0xf8)
	.dwattr $C$DW$282, DW_AT_TI_begin_column(0x05)
	.dwattr $C$DW$282, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 248,column 36,is_stmt,address _TrickleCharge_Mode

	.dwfde $C$DW$CIE, _TrickleCharge_Mode
$C$DW$283	.dwtag  DW_TAG_formal_parameter, DW_AT_name("Voltage")
	.dwattr $C$DW$283, DW_AT_TI_symbol_name("_Voltage")
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$283, DW_AT_location[DW_OP_reg0]

;***************************************************************
;* FNAME: _TrickleCharge_Mode           FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  1 Auto,  0 SOE     *
;***************************************************************

_TrickleCharge_Mode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$284	.dwtag  DW_TAG_variable, DW_AT_name("Voltage")
	.dwattr $C$DW$284, DW_AT_TI_symbol_name("_Voltage")
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$284, DW_AT_location[DW_OP_breg20 -1]
        MOV       *-SP[1],AL            ; [CPU_] |248| 
	.dwpsn	file "../gateway.c",line 249,column 2,is_stmt
        CMP       AL,#857               ; [CPU_] |249| 
        B         $C$L25,GT             ; [CPU_] |249| 
        ; branchcc occurs ; [] |249| 
        MOVW      DP,#_TrickleCharge_Active ; [CPU_U] 
        MOV       AL,@_TrickleCharge_Active ; [CPU_] |249| 
        BF        $C$L25,EQ             ; [CPU_] |249| 
        ; branchcc occurs ; [] |249| 
	.dwpsn	file "../gateway.c",line 250,column 3,is_stmt
        MOVB      @_TrickleCharge_Active,#1,UNC ; [CPU_] |250| 
	.dwpsn	file "../gateway.c",line 251,column 2,is_stmt
        B         $C$L27,UNC            ; [CPU_] |251| 
        ; branch occurs ; [] |251| 
$C$L25:    
	.dwpsn	file "../gateway.c",line 251,column 9,is_stmt
        MOV       AL,*-SP[1]            ; [CPU_] |251| 
        CMP       AL,#857               ; [CPU_] |251| 
        B         $C$L26,LEQ            ; [CPU_] |251| 
        ; branchcc occurs ; [] |251| 
	.dwpsn	file "../gateway.c",line 252,column 3,is_stmt
        MOVW      DP,#_TrickleCharge_Active ; [CPU_U] 
        MOV       @_TrickleCharge_Active,#0 ; [CPU_] |252| 
	.dwpsn	file "../gateway.c",line 253,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |253| 
        B         $C$L29,UNC            ; [CPU_] |253| 
        ; branch occurs ; [] |253| 
$C$L26:    
	.dwpsn	file "../gateway.c",line 254,column 9,is_stmt
        CMP       AL,#809               ; [CPU_] |254| 
        B         $C$L27,GEQ            ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
        MOVW      DP,#_TrickleCharge_Active ; [CPU_U] 
        MOV       AL,@_TrickleCharge_Active ; [CPU_] |254| 
        BF        $C$L27,NEQ            ; [CPU_] |254| 
        ; branchcc occurs ; [] |254| 
	.dwpsn	file "../gateway.c",line 255,column 3,is_stmt
        MOVB      @_TrickleCharge_Active,#1,UNC ; [CPU_] |255| 
$C$L27:    
	.dwpsn	file "../gateway.c",line 258,column 2,is_stmt
        MOVW      DP,#_TrickleCharge_Active ; [CPU_U] 
        MOV       AL,@_TrickleCharge_Active ; [CPU_] |258| 
        BF        $C$L28,EQ             ; [CPU_] |258| 
        ; branchcc occurs ; [] |258| 
	.dwpsn	file "../gateway.c",line 258,column 27,is_stmt
        MOVB      AL,#200               ; [CPU_] |258| 
        B         $C$L29,UNC            ; [CPU_] |258| 
        ; branch occurs ; [] |258| 
$C$L28:    
	.dwpsn	file "../gateway.c",line 260,column 2,is_stmt
        MOVB      AL,#0                 ; [CPU_] |260| 
$C$L29:    
	.dwpsn	file "../gateway.c",line 261,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$285	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$285, DW_AT_low_pc(0x00)
	.dwattr $C$DW$285, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$282, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$282, DW_AT_TI_end_line(0x105)
	.dwattr $C$DW$282, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$282

	.sect	".text"
	.clink
	.global	_Charger_Control

$C$DW$286	.dwtag  DW_TAG_subprogram, DW_AT_name("Charger_Control")
	.dwattr $C$DW$286, DW_AT_low_pc(_Charger_Control)
	.dwattr $C$DW$286, DW_AT_high_pc(0x00)
	.dwattr $C$DW$286, DW_AT_TI_symbol_name("_Charger_Control")
	.dwattr $C$DW$286, DW_AT_external
	.dwattr $C$DW$286, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$286, DW_AT_TI_begin_line(0x107)
	.dwattr $C$DW$286, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$286, DW_AT_TI_max_frame_size(-38)
	.dwpsn	file "../gateway.c",line 263,column 116,is_stmt,address _Charger_Control

	.dwfde $C$DW$CIE, _Charger_Control
$C$DW$287	.dwtag  DW_TAG_formal_parameter, DW_AT_name("EVOCharger_MasterData")
	.dwattr $C$DW$287, DW_AT_TI_symbol_name("_EVOCharger_MasterData")
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$287, DW_AT_location[DW_OP_reg12]
$C$DW$288	.dwtag  DW_TAG_formal_parameter, DW_AT_name("EVOCharger_SlaveData")
	.dwattr $C$DW$288, DW_AT_TI_symbol_name("_EVOCharger_SlaveData")
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$288, DW_AT_location[DW_OP_reg14]
$C$DW$289	.dwtag  DW_TAG_formal_parameter, DW_AT_name("ChargerSettings")
	.dwattr $C$DW$289, DW_AT_TI_symbol_name("_ChargerSettings")
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$289, DW_AT_location[DW_OP_breg20 -40]

;***************************************************************
;* FNAME: _Charger_Control              FR SIZE:  36           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter, 36 Auto,  0 SOE     *
;***************************************************************

_Charger_Control:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -38
$C$DW$290	.dwtag  DW_TAG_variable, DW_AT_name("EVOCharger_MasterData")
	.dwattr $C$DW$290, DW_AT_TI_symbol_name("_EVOCharger_MasterData")
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$290, DW_AT_location[DW_OP_breg20 -2]
$C$DW$291	.dwtag  DW_TAG_variable, DW_AT_name("EVOCharger_MasterData")
	.dwattr $C$DW$291, DW_AT_TI_symbol_name("_EVOCharger_MasterData")
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$291, DW_AT_location[DW_OP_breg20 -13]
$C$DW$292	.dwtag  DW_TAG_variable, DW_AT_name("EVOCharger_SlaveData")
	.dwattr $C$DW$292, DW_AT_TI_symbol_name("_EVOCharger_SlaveData")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_breg20 -16]
$C$DW$293	.dwtag  DW_TAG_variable, DW_AT_name("EVOCharger_SlaveData")
	.dwattr $C$DW$293, DW_AT_TI_symbol_name("_EVOCharger_SlaveData")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$293, DW_AT_location[DW_OP_breg20 -27]
$C$DW$294	.dwtag  DW_TAG_variable, DW_AT_name("ChargerSettings")
	.dwattr $C$DW$294, DW_AT_TI_symbol_name("_ChargerSettings")
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$294, DW_AT_location[DW_OP_breg20 -32]
$C$DW$295	.dwtag  DW_TAG_variable, DW_AT_name("CANStatus")
	.dwattr $C$DW$295, DW_AT_TI_symbol_name("_CANStatus")
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$295, DW_AT_location[DW_OP_breg20 -33]
$C$DW$296	.dwtag  DW_TAG_variable, DW_AT_name("ACCurrent")
	.dwattr $C$DW$296, DW_AT_TI_symbol_name("_ACCurrent")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$296, DW_AT_location[DW_OP_breg20 -34]
$C$DW$297	.dwtag  DW_TAG_variable, DW_AT_name("DCVoltage")
	.dwattr $C$DW$297, DW_AT_TI_symbol_name("_DCVoltage")
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$297, DW_AT_location[DW_OP_breg20 -35]
$C$DW$298	.dwtag  DW_TAG_variable, DW_AT_name("DCCurrent")
	.dwattr $C$DW$298, DW_AT_TI_symbol_name("_DCCurrent")
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$298, DW_AT_location[DW_OP_breg20 -36]
        MOVL      *-SP[2],XAR4          ; [CPU_] |263| 
        MOVL      XAR7,*-SP[2]          ; [CPU_] |263| 
        MOVL      *-SP[16],XAR5         ; [CPU_] |263| 
        MOVZ      AR5,SP                ; [CPU_U] |263| 
        SUBB      XAR5,#13              ; [CPU_U] |263| 
        RPT       #10
||     PREAD     *XAR5++,*XAR7         ; [CPU_] |263| 
        MOVL      XAR7,*-SP[16]         ; [CPU_] |263| 
        MOVZ      AR4,SP                ; [CPU_U] |263| 
        SUBB      XAR4,#27              ; [CPU_U] |263| 
        RPT       #10
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |263| 
        MOVL      XAR7,*-SP[40]         ; [CPU_] |263| 
        MOVZ      AR4,SP                ; [CPU_U] |263| 
        SUBB      XAR4,#32              ; [CPU_U] |263| 
        RPT       #3
||     PREAD     *XAR4++,*XAR7         ; [CPU_] |263| 
	.dwpsn	file "../gateway.c",line 266,column 2,is_stmt
        MOV       AL,*-SP[32]           ; [CPU_] |266| 
        MOV       *-SP[33],AL           ; [CPU_] |266| 
	.dwpsn	file "../gateway.c",line 268,column 2,is_stmt
        MOV       AL,*-SP[31]           ; [CPU_] |268| 
        MOV       *-SP[34],AL           ; [CPU_] |268| 
	.dwpsn	file "../gateway.c",line 269,column 2,is_stmt
        MOV       AL,*-SP[30]           ; [CPU_] |269| 
        MOV       *-SP[35],AL           ; [CPU_] |269| 
	.dwpsn	file "../gateway.c",line 270,column 2,is_stmt
        MOV       AL,*-SP[29]           ; [CPU_] |270| 
        MOV       *-SP[36],AL           ; [CPU_] |270| 
	.dwpsn	file "../gateway.c",line 272,column 2,is_stmt
        MOV       *-SP[13],#1384        ; [CPU_] |272| 
	.dwpsn	file "../gateway.c",line 272,column 41,is_stmt
        MOVB      *-SP[11],#8,UNC       ; [CPU_] |272| 
	.dwpsn	file "../gateway.c",line 272,column 72,is_stmt
        MOV       *-SP[12],#0           ; [CPU_] |272| 
	.dwpsn	file "../gateway.c",line 273,column 2,is_stmt
        MOV       AL,*-SP[33]           ; [CPU_] |273| 
        MOV       *-SP[10],AL           ; [CPU_] |273| 
	.dwpsn	file "../gateway.c",line 273,column 45,is_stmt
        MOV       AL,*-SP[34]           ; [CPU_] |273| 
        ANDB      AL,#0xff              ; [CPU_] |273| 
        MOV       *-SP[9],AL            ; [CPU_] |273| 
	.dwpsn	file "../gateway.c",line 273,column 95,is_stmt
        MOV       AL,*-SP[34]           ; [CPU_] |273| 
        LSR       AL,8                  ; [CPU_] |273| 
        MOV       *-SP[8],AL            ; [CPU_] |273| 
	.dwpsn	file "../gateway.c",line 274,column 2,is_stmt
        MOV       AL,*-SP[35]           ; [CPU_] |274| 
        ANDB      AL,#0xff              ; [CPU_] |274| 
        MOV       *-SP[7],AL            ; [CPU_] |274| 
	.dwpsn	file "../gateway.c",line 274,column 52,is_stmt
        MOV       AL,*-SP[35]           ; [CPU_] |274| 
        LSR       AL,8                  ; [CPU_] |274| 
        MOV       *-SP[6],AL            ; [CPU_] |274| 
	.dwpsn	file "../gateway.c",line 274,column 100,is_stmt
        MOV       AL,*-SP[36]           ; [CPU_] |274| 
        ANDB      AL,#0xff              ; [CPU_] |274| 
        MOV       *-SP[5],AL            ; [CPU_] |274| 
	.dwpsn	file "../gateway.c",line 274,column 150,is_stmt
        MOV       AL,*-SP[36]           ; [CPU_] |274| 
        LSR       AL,8                  ; [CPU_] |274| 
        MOV       *-SP[4],AL            ; [CPU_] |274| 
	.dwpsn	file "../gateway.c",line 274,column 198,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |274| 
	.dwpsn	file "../gateway.c",line 276,column 2,is_stmt
        MOV       *-SP[27],#1400        ; [CPU_] |276| 
	.dwpsn	file "../gateway.c",line 276,column 42,is_stmt
        MOVB      *-SP[25],#8,UNC       ; [CPU_] |276| 
	.dwpsn	file "../gateway.c",line 276,column 72,is_stmt
        MOV       *-SP[26],#0           ; [CPU_] |276| 
	.dwpsn	file "../gateway.c",line 277,column 2,is_stmt
        MOV       AL,*-SP[33]           ; [CPU_] |277| 
        MOV       *-SP[24],AL           ; [CPU_] |277| 
	.dwpsn	file "../gateway.c",line 277,column 44,is_stmt
        MOV       AL,*-SP[34]           ; [CPU_] |277| 
        ANDB      AL,#0xff              ; [CPU_] |277| 
        MOV       *-SP[23],AL           ; [CPU_] |277| 
	.dwpsn	file "../gateway.c",line 277,column 93,is_stmt
        MOV       AL,*-SP[34]           ; [CPU_] |277| 
        LSR       AL,8                  ; [CPU_] |277| 
        MOV       *-SP[22],AL           ; [CPU_] |277| 
	.dwpsn	file "../gateway.c",line 278,column 2,is_stmt
        MOV       AL,*-SP[35]           ; [CPU_] |278| 
        ANDB      AL,#0xff              ; [CPU_] |278| 
        MOV       *-SP[21],AL           ; [CPU_] |278| 
	.dwpsn	file "../gateway.c",line 278,column 51,is_stmt
        MOV       AL,*-SP[35]           ; [CPU_] |278| 
        LSR       AL,8                  ; [CPU_] |278| 
        MOV       *-SP[20],AL           ; [CPU_] |278| 
	.dwpsn	file "../gateway.c",line 278,column 98,is_stmt
        MOV       AL,*-SP[36]           ; [CPU_] |278| 
        ANDB      AL,#0xff              ; [CPU_] |278| 
        MOV       *-SP[19],AL           ; [CPU_] |278| 
	.dwpsn	file "../gateway.c",line 278,column 147,is_stmt
        MOV       AL,*-SP[36]           ; [CPU_] |278| 
        LSR       AL,8                  ; [CPU_] |278| 
        MOV       *-SP[18],AL           ; [CPU_] |278| 
	.dwpsn	file "../gateway.c",line 278,column 194,is_stmt
        MOV       *-SP[17],#0           ; [CPU_] |278| 
	.dwpsn	file "../gateway.c",line 280,column 2,is_stmt
        MOVZ      AR5,SP                ; [CPU_U] |280| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |280| 
        MOVB      AL,#0                 ; [CPU_] |280| 
        SUBB      XAR5,#13              ; [CPU_U] |280| 
$C$DW$299	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$299, DW_AT_low_pc(0x00)
	.dwattr $C$DW$299, DW_AT_name("_MBX_post")
	.dwattr $C$DW$299, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |280| 
        ; call occurs [#_MBX_post] ; [] |280| 
	.dwpsn	file "../gateway.c",line 281,column 2,is_stmt
        MOVB      AL,#0                 ; [CPU_] |281| 
        MOVZ      AR5,SP                ; [CPU_U] |281| 
        MOVL      XAR4,#_can_tx_mbox    ; [CPU_U] |281| 
        SUBB      XAR5,#27              ; [CPU_U] |281| 
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_name("_MBX_post")
	.dwattr $C$DW$300, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |281| 
        ; call occurs [#_MBX_post] ; [] |281| 
	.dwpsn	file "../gateway.c",line 282,column 1,is_stmt
        SUBB      SP,#36                ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$286, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$286, DW_AT_TI_end_line(0x11a)
	.dwattr $C$DW$286, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$286

	.sect	".text"
	.clink
	.global	_PreSync

$C$DW$302	.dwtag  DW_TAG_subprogram, DW_AT_name("PreSync")
	.dwattr $C$DW$302, DW_AT_low_pc(_PreSync)
	.dwattr $C$DW$302, DW_AT_high_pc(0x00)
	.dwattr $C$DW$302, DW_AT_TI_symbol_name("_PreSync")
	.dwattr $C$DW$302, DW_AT_external
	.dwattr $C$DW$302, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$302, DW_AT_TI_begin_line(0x128)
	.dwattr $C$DW$302, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$302, DW_AT_TI_max_frame_size(-10)
	.dwpsn	file "../gateway.c",line 296,column 26,is_stmt,address _PreSync

	.dwfde $C$DW$CIE, _PreSync
$C$DW$303	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$303, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$303, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _PreSync                      FR SIZE:   8           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  8 Auto,  0 SOE     *
;***************************************************************

_PreSync:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -10
$C$DW$304	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$304, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$304, DW_AT_location[DW_OP_breg20 -2]
$C$DW$305	.dwtag  DW_TAG_variable, DW_AT_name("i")
	.dwattr $C$DW$305, DW_AT_TI_symbol_name("_i")
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$305, DW_AT_location[DW_OP_breg20 -3]
$C$DW$306	.dwtag  DW_TAG_variable, DW_AT_name("maxtemp")
	.dwattr $C$DW$306, DW_AT_TI_symbol_name("_maxtemp")
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$306, DW_AT_location[DW_OP_breg20 -4]
$C$DW$307	.dwtag  DW_TAG_variable, DW_AT_name("mintemp")
	.dwattr $C$DW$307, DW_AT_TI_symbol_name("_mintemp")
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$307, DW_AT_location[DW_OP_breg20 -5]
$C$DW$308	.dwtag  DW_TAG_variable, DW_AT_name("SystemBatt")
	.dwattr $C$DW$308, DW_AT_TI_symbol_name("_SystemBatt")
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$308, DW_AT_location[DW_OP_breg20 -8]
        MOVL      *-SP[2],XAR4          ; [CPU_] |296| 
	.dwpsn	file "../gateway.c",line 297,column 8,is_stmt
        MOV       *-SP[3],#0            ; [CPU_] |297| 
	.dwpsn	file "../gateway.c",line 298,column 2,is_stmt
        MOVW      DP,#_Heater_status    ; [CPU_U] 
        MOV       @_Heater_status,#0    ; [CPU_] |298| 
	.dwpsn	file "../gateway.c",line 299,column 15,is_stmt
        MOV       *-SP[4],#-55          ; [CPU_] |299| 
	.dwpsn	file "../gateway.c",line 299,column 30,is_stmt
        MOVB      *-SP[5],#127,UNC      ; [CPU_] |299| 
	.dwpsn	file "../gateway.c",line 300,column 20,is_stmt
        MOVB      ACC,#0                ; [CPU_] |300| 
        MOVL      *-SP[8],ACC           ; [CPU_] |300| 
	.dwpsn	file "../gateway.c",line 302,column 3,is_stmt
        DEC       @_Sync_count          ; [CPU_] |302| 
	.dwpsn	file "../gateway.c",line 303,column 3,is_stmt
        MOV       AL,@_Sync_count       ; [CPU_] |303| 
        BF        $C$L52,NEQ            ; [CPU_] |303| 
        ; branchcc occurs ; [] |303| 
	.dwpsn	file "../gateway.c",line 304,column 5,is_stmt
        MOV       @_Modules_Present,#0  ; [CPU_] |304| 
	.dwpsn	file "../gateway.c",line 305,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |305| 
        CMPB      AL,#98                ; [CPU_] |305| 
        B         $C$L36,GT             ; [CPU_] |305| 
        ; branchcc occurs ; [] |305| 
$C$L30:    
	.dwpsn	file "../gateway.c",line 306,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |306| 
        MOV       ACC,*-SP[3]           ; [CPU_] |306| 
        ADDL      XAR4,ACC              ; [CPU_] |306| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |306| 
        CMPB      AL,#255               ; [CPU_] |306| 
        BF        $C$L35,EQ             ; [CPU_] |306| 
        ; branchcc occurs ; [] |306| 
	.dwpsn	file "../gateway.c",line 308,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |308| 
        MOVL      XAR4,#_ODV_Modules_MinCellVoltage ; [CPU_U] |308| 
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        ADDL      XAR4,ACC              ; [CPU_] |308| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |308| 
        CMP       AL,@_GAT_min          ; [CPU_] |308| 
        B         $C$L31,HIS            ; [CPU_] |308| 
        ; branchcc occurs ; [] |308| 
	.dwpsn	file "../gateway.c",line 308,column 54,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |308| 
        MOVL      XAR7,#_ODV_Modules_MinCellVoltage ; [CPU_U] |308| 
        ADDL      XAR7,ACC              ; [CPU_] |308| 
        MOV       AL,*XAR7              ; [CPU_] |308| 
        MOV       @_GAT_min,AL          ; [CPU_] |308| 
$C$L31:    
	.dwpsn	file "../gateway.c",line 309,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |309| 
        MOVL      XAR4,#_ODV_Modules_MaxCellVoltage ; [CPU_U] |309| 
        ADDL      XAR4,ACC              ; [CPU_] |309| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |309| 
        CMP       AL,@_GAT_max          ; [CPU_] |309| 
        B         $C$L32,LOS            ; [CPU_] |309| 
        ; branchcc occurs ; [] |309| 
	.dwpsn	file "../gateway.c",line 309,column 54,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |309| 
        MOVL      XAR7,#_ODV_Modules_MaxCellVoltage ; [CPU_U] |309| 
        ADDL      XAR7,ACC              ; [CPU_] |309| 
        MOV       AL,*XAR7              ; [CPU_] |309| 
        MOV       @_GAT_max,AL          ; [CPU_] |309| 
$C$L32:    
	.dwpsn	file "../gateway.c",line 310,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |310| 
        MOVL      XAR4,#_ODV_Modules_Temperature ; [CPU_U] |310| 
        ADDL      XAR4,ACC              ; [CPU_] |310| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |310| 
        CMP       AL,*-SP[4]            ; [CPU_] |310| 
        B         $C$L33,LEQ            ; [CPU_] |310| 
        ; branchcc occurs ; [] |310| 
	.dwpsn	file "../gateway.c",line 310,column 51,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |310| 
        MOVL      XAR7,#_ODV_Modules_Temperature ; [CPU_U] |310| 
        ADDL      XAR7,ACC              ; [CPU_] |310| 
        MOV       AL,*XAR7              ; [CPU_] |310| 
        MOV       *-SP[4],AL            ; [CPU_] |310| 
$C$L33:    
	.dwpsn	file "../gateway.c",line 311,column 9,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |311| 
        MOVL      XAR4,#_ODV_Modules_Temperature_MIN ; [CPU_U] |311| 
        ADDL      XAR4,ACC              ; [CPU_] |311| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |311| 
        CMP       AL,*-SP[5]            ; [CPU_] |311| 
        B         $C$L34,HIS            ; [CPU_] |311| 
        ; branchcc occurs ; [] |311| 
	.dwpsn	file "../gateway.c",line 311,column 55,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |311| 
        MOVL      XAR7,#_ODV_Modules_Temperature_MIN ; [CPU_U] |311| 
        ADDL      XAR7,ACC              ; [CPU_] |311| 
        MOV       AL,*XAR7              ; [CPU_] |311| 
        MOV       *-SP[5],AL            ; [CPU_] |311| 
$C$L34:    
	.dwpsn	file "../gateway.c",line 312,column 3,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |312| 
        MOVL      XAR4,#_ODV_Modules_Heater ; [CPU_U] |312| 
        ADDL      XAR4,ACC              ; [CPU_] |312| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |312| 
        ADD       @_Heater_status,AL    ; [CPU_] |312| 
	.dwpsn	file "../gateway.c",line 313,column 3,is_stmt
        MOVL      XAR4,#_ODV_Modules_Voltage ; [CPU_U] |313| 
        MOV       ACC,*-SP[3]           ; [CPU_] |313| 
        ADDL      XAR4,ACC              ; [CPU_] |313| 
        MOVU      ACC,*+XAR4[0]         ; [CPU_] |313| 
        ADDL      ACC,*-SP[8]           ; [CPU_] |313| 
        MOVL      *-SP[8],ACC           ; [CPU_] |313| 
	.dwpsn	file "../gateway.c",line 315,column 9,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |315| 
        BF        $C$L35,EQ             ; [CPU_] |315| 
        ; branchcc occurs ; [] |315| 
	.dwpsn	file "../gateway.c",line 317,column 11,is_stmt
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        INC       @_Modules_Present     ; [CPU_] |317| 
	.dwpsn	file "../gateway.c",line 318,column 11,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |318| 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |318| 
        ADDL      XAR4,ACC              ; [CPU_] |318| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |318| 
        BF        $C$L35,EQ             ; [CPU_] |318| 
        ; branchcc occurs ; [] |318| 
        MOV       ACC,*-SP[3]           ; [CPU_] |318| 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |318| 
        ADDL      XAR4,ACC              ; [CPU_] |318| 
        MOV       AL,*+XAR4[0]          ; [CPU_] |318| 
        CMPB      AL,#255               ; [CPU_] |318| 
        B         $C$L35,HIS            ; [CPU_] |318| 
        ; branchcc occurs ; [] |318| 
	.dwpsn	file "../gateway.c",line 320,column 12,is_stmt
        INC       @_Delay_Error         ; [CPU_] |320| 
	.dwpsn	file "../gateway.c",line 321,column 12,is_stmt
        MOVW      DP,#_ODP_Gateway_Delay_Relay_Error ; [CPU_U] 
        MOV       ACC,@_ODP_Gateway_Delay_Relay_Error << #1 ; [CPU_] |321| 
        MOVW      DP,#_Delay_Error      ; [CPU_U] 
        CMP       AL,@_Delay_Error      ; [CPU_] |321| 
        B         $C$L35,HIS            ; [CPU_] |321| 
        ; branchcc occurs ; [] |321| 
	.dwpsn	file "../gateway.c",line 323,column 13,is_stmt
        MOV       ACC,*-SP[3]           ; [CPU_] |323| 
        MOVZ      AR4,*-SP[3]           ; [CPU_] |323| 
        MOVL      XAR5,#_ODV_Modules_Alarms ; [CPU_U] |323| 
        MOV       T,#24                 ; [CPU_] |323| 
        MOV       PL,#0                 ; [CPU_] |323| 
        MOV       PH,#32768             ; [CPU_] |323| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        ADDL      XAR5,ACC              ; [CPU_] |323| 
        ADDB      XAR4,#1               ; [CPU_] |323| 
        MOV       ACC,AR4               ; [CPU_] |323| 
        LSLL      ACC,T                 ; [CPU_] |323| 
        ADDU      ACC,*+XAR5[0]         ; [CPU_] |323| 
        ADDL      P,ACC                 ; [CPU_] |323| 
        MOVL      @_ODV_ErrorDsp_ErrorNumber,P ; [CPU_] |323| 
	.dwpsn	file "../gateway.c",line 324,column 13,is_stmt
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |324| 
$C$DW$309	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$309, DW_AT_low_pc(0x00)
	.dwattr $C$DW$309, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$309, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |324| 
        ; call occurs [#_ERR_SetError] ; [] |324| 
	.dwpsn	file "../gateway.c",line 325,column 13,is_stmt
        MOVW      DP,#_Delay_Error      ; [CPU_U] 
        MOV       @_Delay_Error,#0      ; [CPU_] |325| 
	.dwpsn	file "../gateway.c",line 328,column 5,is_stmt
$C$L35:    
	.dwpsn	file "../gateway.c",line 336,column 7,is_stmt
        SETC      SXM                   ; [CPU_] 
        MOVL      XAR4,#_ODV_Modules_Alarms ; [CPU_U] |336| 
        MOV       ACC,*-SP[3]           ; [CPU_] |336| 
        ADDL      XAR4,ACC              ; [CPU_] |336| 
        MOVB      *+XAR4[0],#255,UNC    ; [CPU_] |336| 
	.dwpsn	file "../gateway.c",line 337,column 7,is_stmt
        INC       *-SP[3]               ; [CPU_] |337| 
	.dwpsn	file "../gateway.c",line 305,column 12,is_stmt
        MOV       AL,*-SP[3]            ; [CPU_] |305| 
        CMPB      AL,#98                ; [CPU_] |305| 
        B         $C$L30,LEQ            ; [CPU_] |305| 
        ; branchcc occurs ; [] |305| 
$C$L36:    
	.dwpsn	file "../gateway.c",line 340,column 5,is_stmt
        MOV       AL,*-SP[4]            ; [CPU_] |340| 
        CMP       AL,#-55               ; [CPU_] |340| 
        BF        $C$L37,EQ             ; [CPU_] |340| 
        ; branchcc occurs ; [] |340| 
	.dwpsn	file "../gateway.c",line 342,column 7,is_stmt
        MOVW      DP,#_Oldmaxtemp       ; [CPU_U] 
        MOV       @_Oldmaxtemp,AL       ; [CPU_] |342| 
$C$L37:    
	.dwpsn	file "../gateway.c",line 345,column 6,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |345| 
        CMPB      AL,#127               ; [CPU_] |345| 
        BF        $C$L38,EQ             ; [CPU_] |345| 
        ; branchcc occurs ; [] |345| 
	.dwpsn	file "../gateway.c",line 347,column 7,is_stmt
        MOVW      DP,#_Oldmintemp       ; [CPU_U] 
        MOV       @_Oldmintemp,AL       ; [CPU_] |347| 
$C$L38:    
	.dwpsn	file "../gateway.c",line 349,column 6,is_stmt
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        MOVZ      AR6,@_GAT_min         ; [CPU_] |349| 
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#65535            ; [CPU_] |349| 
        CMPL      ACC,XAR6              ; [CPU_] |349| 
        BF        $C$L39,EQ             ; [CPU_] |349| 
        ; branchcc occurs ; [] |349| 
	.dwpsn	file "../gateway.c",line 351,column 7,is_stmt
        MOV       AL,@_GAT_min          ; [CPU_] |351| 
        MOV       @_OldGAT_min,AL       ; [CPU_] |351| 
$C$L39:    
	.dwpsn	file "../gateway.c",line 353,column 6,is_stmt
        MOV       AL,@_GAT_max          ; [CPU_] |353| 
        CMPB      AL,#1                 ; [CPU_] |353| 
        BF        $C$L40,EQ             ; [CPU_] |353| 
        ; branchcc occurs ; [] |353| 
	.dwpsn	file "../gateway.c",line 355,column 7,is_stmt
        MOV       @_OldGAT_max,AL       ; [CPU_] |355| 
$C$L40:    
	.dwpsn	file "../gateway.c",line 358,column 6,is_stmt
        MOVW      DP,#_ODV_Gateway_Control_Heater ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_Control_Heater ; [CPU_] |358| 
        BF        $C$L44,NEQ            ; [CPU_] |358| 
        ; branchcc occurs ; [] |358| 
	.dwpsn	file "../gateway.c",line 360,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |360| 
        CMPB      AL,#35                ; [CPU_] |360| 
        B         $C$L43,GEQ            ; [CPU_] |360| 
        ; branchcc occurs ; [] |360| 
	.dwpsn	file "../gateway.c",line 362,column 5,is_stmt
        MOVW      DP,#_ODP_Settings_AUD_TempHeater_OFF_Max ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_TempHeater_OFF_Max ; [CPU_] |362| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |362| 
        B         $C$L41,LOS            ; [CPU_] |362| 
        ; branchcc occurs ; [] |362| 
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Voltage ; [CPU_] |362| 
        MOV       AL,AH                 ; [CPU_] |362| 
        ASR       AL,1                  ; [CPU_] |362| 
        MOVW      DP,#_ODP_Settings_AUD_Heater_Voltage_OFF ; [CPU_U] 
        LSR       AL,14                 ; [CPU_] |362| 
        ADD       AL,AH                 ; [CPU_] |362| 
        ASR       AL,2                  ; [CPU_] |362| 
        CMP       AL,@_ODP_Settings_AUD_Heater_Voltage_OFF ; [CPU_] |362| 
        B         $C$L42,HIS            ; [CPU_] |362| 
        ; branchcc occurs ; [] |362| 
$C$L41:    
        MOVW      DP,#_ODP_Settings_AUD_Heater_Voltage_OFF ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_Heater_Voltage_OFF ; [CPU_] |362| 
        BF        $C$L42,NEQ            ; [CPU_] |362| 
        ; branchcc occurs ; [] |362| 
	.dwpsn	file "../gateway.c",line 364,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOV       @_ODV_Gateway_Heater_Status,#0 ; [CPU_] |364| 
	.dwpsn	file "../gateway.c",line 365,column 6,is_stmt
        B         $C$L46,UNC            ; [CPU_] |365| 
        ; branch occurs ; [] |365| 
$C$L42:    
	.dwpsn	file "../gateway.c",line 366,column 11,is_stmt
        MOVW      DP,#_ODP_Settings_AUD_Temp_Heater_ON_Min ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_Temp_Heater_ON_Min ; [CPU_] |366| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |366| 
        B         $C$L46,LOS            ; [CPU_] |366| 
        ; branchcc occurs ; [] |366| 
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |366| 
        BF        $C$L46,NEQ            ; [CPU_] |366| 
        ; branchcc occurs ; [] |366| 
	.dwpsn	file "../gateway.c",line 368,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOVB      @_ODV_Gateway_Heater_Status,#1,UNC ; [CPU_] |368| 
	.dwpsn	file "../gateway.c",line 370,column 7,is_stmt
        B         $C$L46,UNC            ; [CPU_] |370| 
        ; branch occurs ; [] |370| 
$C$L43:    
	.dwpsn	file "../gateway.c",line 373,column 8,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOV       @_ODV_Gateway_Heater_Status,#0 ; [CPU_] |373| 
	.dwpsn	file "../gateway.c",line 375,column 6,is_stmt
        B         $C$L46,UNC            ; [CPU_] |375| 
        ; branch occurs ; [] |375| 
$C$L44:    
	.dwpsn	file "../gateway.c",line 376,column 11,is_stmt
        CMPB      AL,#1                 ; [CPU_] |376| 
        BF        $C$L45,NEQ            ; [CPU_] |376| 
        ; branchcc occurs ; [] |376| 
	.dwpsn	file "../gateway.c",line 378,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOVB      @_ODV_Gateway_Heater_Status,#1,UNC ; [CPU_] |378| 
	.dwpsn	file "../gateway.c",line 379,column 6,is_stmt
        B         $C$L46,UNC            ; [CPU_] |379| 
        ; branch occurs ; [] |379| 
$C$L45:    
	.dwpsn	file "../gateway.c",line 382,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_Heater_Status ; [CPU_U] 
        MOV       @_ODV_Gateway_Heater_Status,#0 ; [CPU_] |382| 
$C$L46:    
	.dwpsn	file "../gateway.c",line 385,column 6,is_stmt
        MOVB      ACC,#200              ; [CPU_] |385| 
        MOVW      DP,#_TimerTemp        ; [CPU_U] 
        CMPL      ACC,@_TimerTemp       ; [CPU_] |385| 
        B         $C$L47,HIS            ; [CPU_] |385| 
        ; branchcc occurs ; [] |385| 
	.dwpsn	file "../gateway.c",line 387,column 7,is_stmt
        MOVB      ACC,#0                ; [CPU_] |387| 
        MOVL      @_TimerTemp,ACC       ; [CPU_] |387| 
	.dwpsn	file "../gateway.c",line 388,column 7,is_stmt
        MOV       AL,@_Oldmaxtemp       ; [CPU_] |388| 
        MOVW      DP,#_ODV_Gateway_MaxModTemp ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxModTemp,AL ; [CPU_] |388| 
	.dwpsn	file "../gateway.c",line 389,column 7,is_stmt
        MOVW      DP,#_Oldmintemp       ; [CPU_U] 
        MOV       AL,@_Oldmintemp       ; [CPU_] |389| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        MOV       @_ODV_Gateway_MinModTemp,AL ; [CPU_] |389| 
	.dwpsn	file "../gateway.c",line 390,column 7,is_stmt
        MOVW      DP,#_OldGAT_min       ; [CPU_U] 
        MOV       AL,@_OldGAT_min       ; [CPU_] |390| 
        MOVW      DP,#_ODV_Gateway_MinCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MinCellVoltage,AL ; [CPU_] |390| 
	.dwpsn	file "../gateway.c",line 391,column 7,is_stmt
        MOVW      DP,#_OldGAT_max       ; [CPU_U] 
        MOV       AL,@_OldGAT_max       ; [CPU_] |391| 
        MOVW      DP,#_ODV_Gateway_MaxCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxCellVoltage,AL ; [CPU_] |391| 
	.dwpsn	file "../gateway.c",line 392,column 7,is_stmt
        MOVW      DP,#_OldGAT_min       ; [CPU_U] 
        SUB       AL,@_OldGAT_min       ; [CPU_] |392| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        MOV       @_ODV_Gateway_MaxDeltaCellVoltage,AL ; [CPU_] |392| 
	.dwpsn	file "../gateway.c",line 393,column 7,is_stmt
        MOVL      XAR4,#1000            ; [CPU_U] |393| 
        MOVL      P,*-SP[8]             ; [CPU_] |393| 
        MOVW      DP,#_BatteryVoltage   ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |393| 
        RPT       #31
||     SUBCUL    ACC,XAR4              ; [CPU_] |393| 
        MOV       @_BatteryVoltage,P    ; [CPU_] |393| 
$C$L47:    
	.dwpsn	file "../gateway.c",line 398,column 3,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |398| 
        BF        $C$L51,EQ             ; [CPU_] |398| 
        ; branchcc occurs ; [] |398| 
	.dwpsn	file "../gateway.c",line 400,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax_bal_delta ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax_bal_delta ; [CPU_] |400| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_] |400| 
        B         $C$L49,HIS            ; [CPU_] |400| 
        ; branchcc occurs ; [] |400| 
	.dwpsn	file "../gateway.c",line 402,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        MOVIZ     R1H,#16288            ; [CPU_] |402| 
        I16TOF32  R0H,@_ODV_Gateway_Current ; [CPU_] |402| 
$C$DW$310	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$310, DW_AT_low_pc(0x00)
	.dwattr $C$DW$310, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$310, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |402| 
        ; call occurs [#FS$$DIV] ; [] |402| 
$C$DW$311	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$311, DW_AT_low_pc(0x00)
	.dwattr $C$DW$311, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$311, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |402| 
        ; call occurs [#_CNV_Round] ; [] |402| 
        MOVL      XAR6,ACC              ; [CPU_] |402| 
        MOVB      ACC,#3                ; [CPU_] |402| 
        CMPL      ACC,XAR6              ; [CPU_] |402| 
        B         $C$L48,GEQ            ; [CPU_] |402| 
        ; branchcc occurs ; [] |402| 
	.dwpsn	file "../gateway.c",line 404,column 6,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |404| 
        MOVL      ACC,XAR4              ; [CPU_] |404| 
$C$DW$312	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$312, DW_AT_low_pc(0x00)
	.dwattr $C$DW$312, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$312, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |404| 
        ; call occurs [#_ERR_HandleWarning] ; [] |404| 
	.dwpsn	file "../gateway.c",line 405,column 5,is_stmt
        B         $C$L49,UNC            ; [CPU_] |405| 
        ; branch occurs ; [] |405| 
$C$L48:    
	.dwpsn	file "../gateway.c",line 406,column 10,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |406| 
        MOVL      ACC,XAR4              ; [CPU_] |406| 
$C$DW$313	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$313, DW_AT_low_pc(0x00)
	.dwattr $C$DW$313, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$313, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |406| 
        ; call occurs [#_ERR_ClearWarning] ; [] |406| 
$C$L49:    
	.dwpsn	file "../gateway.c",line 408,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin_bal_delta ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin_bal_delta ; [CPU_] |408| 
        MOVW      DP,#_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxDeltaCellVoltage ; [CPU_] |408| 
        B         $C$L50,HIS            ; [CPU_] |408| 
        ; branchcc occurs ; [] |408| 
	.dwpsn	file "../gateway.c",line 410,column 5,is_stmt
        MOV       AL,#0                 ; [CPU_] |410| 
        MOV       AH,#128               ; [CPU_] |410| 
$C$DW$314	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$314, DW_AT_low_pc(0x00)
	.dwattr $C$DW$314, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$314, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |410| 
        ; call occurs [#_ERR_HandleWarning] ; [] |410| 
	.dwpsn	file "../gateway.c",line 411,column 4,is_stmt
        B         $C$L51,UNC            ; [CPU_] |411| 
        ; branch occurs ; [] |411| 
$C$L50:    
	.dwpsn	file "../gateway.c",line 412,column 10,is_stmt
        MOV       AL,#0                 ; [CPU_] |412| 
        MOV       AH,#128               ; [CPU_] |412| 
$C$DW$315	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$315, DW_AT_low_pc(0x00)
	.dwattr $C$DW$315, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$315, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |412| 
        ; call occurs [#_ERR_ClearWarning] ; [] |412| 
$C$L51:    
	.dwpsn	file "../gateway.c",line 415,column 5,is_stmt
        MOVW      DP,#_GAT_min          ; [CPU_U] 
        MOV       @_GAT_min,#65535      ; [CPU_] |415| 
	.dwpsn	file "../gateway.c",line 416,column 5,is_stmt
        MOVB      @_GAT_max,#1,UNC      ; [CPU_] |416| 
	.dwpsn	file "../gateway.c",line 417,column 5,is_stmt
        MOVB      @_Sync_count,#99,UNC  ; [CPU_] |417| 
	.dwpsn	file "../gateway.c",line 419,column 1,is_stmt
$C$L52:    
        SUBB      SP,#8                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$316	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$316, DW_AT_low_pc(0x00)
	.dwattr $C$DW$316, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$302, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$302, DW_AT_TI_end_line(0x1a3)
	.dwattr $C$DW$302, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$302

	.sect	".text"
	.clink
	.global	_FnStopNode

$C$DW$317	.dwtag  DW_TAG_subprogram, DW_AT_name("FnStopNode")
	.dwattr $C$DW$317, DW_AT_low_pc(_FnStopNode)
	.dwattr $C$DW$317, DW_AT_high_pc(0x00)
	.dwattr $C$DW$317, DW_AT_TI_symbol_name("_FnStopNode")
	.dwattr $C$DW$317, DW_AT_external
	.dwattr $C$DW$317, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$317, DW_AT_TI_begin_line(0x1a6)
	.dwattr $C$DW$317, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$317, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 422,column 29,is_stmt,address _FnStopNode

	.dwfde $C$DW$CIE, _FnStopNode
$C$DW$318	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$318, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$318, DW_AT_location[DW_OP_reg12]

;***************************************************************
;* FNAME: _FnStopNode                   FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_FnStopNode:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$319	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$319, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$319, DW_AT_location[DW_OP_breg20 -2]
        MOVL      *-SP[2],XAR4          ; [CPU_] |422| 
	.dwpsn	file "../gateway.c",line 424,column 3,is_stmt
        MOVW      DP,#_ODV_Controlword  ; [CPU_U] 
        MOV       @_ODV_Controlword,#0  ; [CPU_] |424| 
	.dwpsn	file "../gateway.c",line 425,column 1,is_stmt
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$320	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$320, DW_AT_low_pc(0x00)
	.dwattr $C$DW$320, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$317, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$317, DW_AT_TI_end_line(0x1a9)
	.dwattr $C$DW$317, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$317

	.sect	".text"
	.clink
	.global	_canOpenInit

$C$DW$321	.dwtag  DW_TAG_subprogram, DW_AT_name("canOpenInit")
	.dwattr $C$DW$321, DW_AT_low_pc(_canOpenInit)
	.dwattr $C$DW$321, DW_AT_high_pc(0x00)
	.dwattr $C$DW$321, DW_AT_TI_symbol_name("_canOpenInit")
	.dwattr $C$DW$321, DW_AT_external
	.dwattr $C$DW$321, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$321, DW_AT_TI_begin_line(0x1ad)
	.dwattr $C$DW$321, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$321, DW_AT_TI_max_frame_size(-2)
	.dwpsn	file "../gateway.c",line 431,column 1,is_stmt,address _canOpenInit

	.dwfde $C$DW$CIE, _canOpenInit

;***************************************************************
;* FNAME: _canOpenInit                  FR SIZE:   0           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  0 Auto,  0 SOE     *
;***************************************************************

_canOpenInit:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
	.dwpsn	file "../gateway.c",line 433,column 3,is_stmt
        MOVL      XAR4,#_ODI_gateway_dict_Data ; [CPU_U] |433| 
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      @_BoardODdata,XAR4    ; [CPU_] |433| 
	.dwpsn	file "../gateway.c",line 435,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |435| 
        MOVB      XAR0,#84              ; [CPU_] |435| 
        MOVL      XAR4,#_FnInitialiseNode ; [CPU_U] |435| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |435| 
	.dwpsn	file "../gateway.c",line 436,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |436| 
        MOVB      XAR0,#86              ; [CPU_] |436| 
        MOVL      XAR4,#_FnEnterPreOperational ; [CPU_U] |436| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |436| 
	.dwpsn	file "../gateway.c",line 437,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |437| 
        MOVB      XAR0,#88              ; [CPU_] |437| 
        MOVL      XAR4,#_FnStartNode    ; [CPU_U] |437| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |437| 
	.dwpsn	file "../gateway.c",line 438,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |438| 
        MOVB      XAR0,#90              ; [CPU_] |438| 
        MOVL      XAR4,#_FnStopNode     ; [CPU_U] |438| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |438| 
	.dwpsn	file "../gateway.c",line 439,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |439| 
        MOVB      XAR0,#92              ; [CPU_] |439| 
        MOVL      XAR4,#_FnResetNode    ; [CPU_U] |439| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |439| 
	.dwpsn	file "../gateway.c",line 440,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |440| 
        MOVB      XAR0,#94              ; [CPU_] |440| 
        MOVL      XAR4,#_FnResetCommunications ; [CPU_U] |440| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |440| 
	.dwpsn	file "../gateway.c",line 442,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |442| 
        MOVB      XAR0,#252             ; [CPU_] |442| 
        MOVL      XAR4,#_PAR_StoreODSubIndex ; [CPU_U] |442| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |442| 
	.dwpsn	file "../gateway.c",line 444,column 3,is_stmt
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |444| 
        MOVB      ACC,#0                ; [CPU_] |444| 
        MOVB      XAR0,#254             ; [CPU_] |444| 
        MOVL      *+XAR4[AR0],ACC       ; [CPU_] |444| 
	.dwpsn	file "../gateway.c",line 445,column 3,is_stmt
        MOVL      XAR5,@_BoardODdata    ; [CPU_] |445| 
        MOVB      XAR0,#242             ; [CPU_] |445| 
        MOVL      XAR4,#_PreSync        ; [CPU_U] |445| 
        MOVL      *+XAR5[AR0],XAR4      ; [CPU_] |445| 
	.dwpsn	file "../gateway.c",line 448,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |448| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |448| 
$C$DW$322	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$322, DW_AT_low_pc(0x00)
	.dwattr $C$DW$322, DW_AT_name("_setState")
	.dwattr $C$DW$322, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |448| 
        ; call occurs [#_setState] ; [] |448| 
	.dwpsn	file "../gateway.c",line 449,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |449| 
$C$DW$323	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$323, DW_AT_low_pc(0x00)
	.dwattr $C$DW$323, DW_AT_name("_FnResetCommunications")
	.dwattr $C$DW$323, DW_AT_TI_call
        LCR       #_FnResetCommunications ; [CPU_] |449| 
        ; call occurs [#_FnResetCommunications] ; [] |449| 
	.dwpsn	file "../gateway.c",line 456,column 3,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVB      AL,#5                 ; [CPU_] |456| 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |456| 
$C$DW$324	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$324, DW_AT_low_pc(0x00)
	.dwattr $C$DW$324, DW_AT_name("_setState")
	.dwattr $C$DW$324, DW_AT_TI_call
        LCR       #_setState            ; [CPU_] |456| 
        ; call occurs [#_setState] ; [] |456| 
	.dwpsn	file "../gateway.c",line 458,column 1,is_stmt
$C$DW$325	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$325, DW_AT_low_pc(0x00)
	.dwattr $C$DW$325, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$321, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$321, DW_AT_TI_end_line(0x1ca)
	.dwattr $C$DW$321, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$321

	.sect	".text"
	.clink
	.global	_taskFnGestionMachine

$C$DW$326	.dwtag  DW_TAG_subprogram, DW_AT_name("taskFnGestionMachine")
	.dwattr $C$DW$326, DW_AT_low_pc(_taskFnGestionMachine)
	.dwattr $C$DW$326, DW_AT_high_pc(0x00)
	.dwattr $C$DW$326, DW_AT_TI_symbol_name("_taskFnGestionMachine")
	.dwattr $C$DW$326, DW_AT_external
	.dwattr $C$DW$326, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$326, DW_AT_TI_begin_line(0x1d4)
	.dwattr $C$DW$326, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$326, DW_AT_TI_max_frame_size(-4)
	.dwpsn	file "../gateway.c",line 468,column 32,is_stmt,address _taskFnGestionMachine

	.dwfde $C$DW$CIE, _taskFnGestionMachine

;***************************************************************
;* FNAME: _taskFnGestionMachine         FR SIZE:   2           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  2 Auto,  0 SOE     *
;***************************************************************

_taskFnGestionMachine:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -4
$C$DW$327	.dwtag  DW_TAG_variable, DW_AT_name("old_time")
	.dwattr $C$DW$327, DW_AT_TI_symbol_name("_old_time")
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$327, DW_AT_location[DW_OP_breg20 -2]
	.dwpsn	file "../gateway.c",line 474,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |474| 
        MOVB      AL,#2                 ; [CPU_] |474| 
$C$DW$328	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$328, DW_AT_low_pc(0x00)
	.dwattr $C$DW$328, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$328, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |474| 
        ; call occurs [#_SEM_pend] ; [] |474| 
	.dwpsn	file "../gateway.c",line 475,column 3,is_stmt
$C$DW$329	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$329, DW_AT_low_pc(0x00)
	.dwattr $C$DW$329, DW_AT_name("_HAL_Init")
	.dwattr $C$DW$329, DW_AT_TI_call
        LCR       #_HAL_Init            ; [CPU_] |475| 
        ; call occurs [#_HAL_Init] ; [] |475| 
	.dwpsn	file "../gateway.c",line 476,column 3,is_stmt
$C$DW$330	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$330, DW_AT_low_pc(0x00)
	.dwattr $C$DW$330, DW_AT_name("_ADS_Init")
	.dwattr $C$DW$330, DW_AT_TI_call
        LCR       #_ADS_Init            ; [CPU_] |476| 
        ; call occurs [#_ADS_Init] ; [] |476| 
	.dwpsn	file "../gateway.c",line 477,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |477| 
        BF        $C$L54,NEQ            ; [CPU_] |477| 
        ; branchcc occurs ; [] |477| 
$C$L53:    
	.dwpsn	file "../gateway.c",line 478,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |478| 
        MOVB      AL,#1                 ; [CPU_] |478| 
$C$DW$331	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$331, DW_AT_low_pc(0x00)
	.dwattr $C$DW$331, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$331, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |478| 
        ; call occurs [#_SEM_pend] ; [] |478| 
	.dwpsn	file "../gateway.c",line 477,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |477| 
        BF        $C$L53,EQ             ; [CPU_] |477| 
        ; branchcc occurs ; [] |477| 
$C$L54:    
	.dwpsn	file "../gateway.c",line 480,column 3,is_stmt
$C$DW$332	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$332, DW_AT_low_pc(0x00)
	.dwattr $C$DW$332, DW_AT_name("_USB_Start")
	.dwattr $C$DW$332, DW_AT_TI_call
        LCR       #_USB_Start           ; [CPU_] |480| 
        ; call occurs [#_USB_Start] ; [] |480| 
	.dwpsn	file "../gateway.c",line 481,column 3,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |481| 
        MOVL      *-SP[2],ACC           ; [CPU_] |481| 
	.dwpsn	file "../gateway.c",line 482,column 9,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      ACC,@_BootCommand     ; [CPU_] |482| 
        BF        $C$L57,NEQ            ; [CPU_] |482| 
        ; branchcc occurs ; [] |482| 
$C$L55:    
	.dwpsn	file "../gateway.c",line 486,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVB      XAR6,#63              ; [CPU_] |486| 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |486| 
        MOVB      AH,#0                 ; [CPU_] |486| 
        ANDB      AL,#0x3f              ; [CPU_] |486| 
        CMPL      ACC,XAR6              ; [CPU_] |486| 
        BF        $C$L56,NEQ            ; [CPU_] |486| 
        ; branchcc occurs ; [] |486| 
        MOVB      XAR6,#62              ; [CPU_] |486| 
        MOVL      ACC,*-SP[2]           ; [CPU_] |486| 
        MOVB      AH,#0                 ; [CPU_] |486| 
        ANDB      AL,#0x3f              ; [CPU_] |486| 
        CMPL      ACC,XAR6              ; [CPU_] |486| 
        BF        $C$L56,NEQ            ; [CPU_] |486| 
        ; branchcc occurs ; [] |486| 
	.dwpsn	file "../gateway.c",line 489,column 7,is_stmt
        MOV       AL,#-1                ; [CPU_] |489| 
$C$DW$333	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$333, DW_AT_low_pc(0x00)
	.dwattr $C$DW$333, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$333, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |489| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |489| 
	.dwpsn	file "../gateway.c",line 490,column 7,is_stmt
        MOV       AL,#-5                ; [CPU_] |490| 
$C$DW$334	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$334, DW_AT_low_pc(0x00)
	.dwattr $C$DW$334, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$334, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |490| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |490| 
$C$L56:    
	.dwpsn	file "../gateway.c",line 492,column 5,is_stmt
        MOVW      DP,#_ODP_OnTime       ; [CPU_U] 
        MOVL      ACC,@_ODP_OnTime      ; [CPU_] |492| 
        MOVL      *-SP[2],ACC           ; [CPU_] |492| 
	.dwpsn	file "../gateway.c",line 493,column 5,is_stmt
$C$DW$335	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$335, DW_AT_low_pc(0x00)
	.dwattr $C$DW$335, DW_AT_name("_USB_Unlock")
	.dwattr $C$DW$335, DW_AT_TI_call
        LCR       #_USB_Unlock          ; [CPU_] |493| 
        ; call occurs [#_USB_Unlock] ; [] |493| 
	.dwpsn	file "../gateway.c",line 494,column 5,is_stmt
        MOVB      AL,#2                 ; [CPU_] |494| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |494| 
$C$DW$336	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$336, DW_AT_low_pc(0x00)
	.dwattr $C$DW$336, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$336, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |494| 
        ; call occurs [#_SEM_pend] ; [] |494| 
	.dwpsn	file "../gateway.c",line 482,column 9,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      ACC,@_BootCommand     ; [CPU_] |482| 
        BF        $C$L55,EQ             ; [CPU_] |482| 
        ; branchcc occurs ; [] |482| 
$C$L57:    
	.dwpsn	file "../gateway.c",line 496,column 3,is_stmt
$C$DW$337	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$337, DW_AT_low_pc(0x00)
	.dwattr $C$DW$337, DW_AT_name("_USB_Stop")
	.dwattr $C$DW$337, DW_AT_TI_call
        LCR       #_USB_Stop            ; [CPU_] |496| 
        ; call occurs [#_USB_Stop] ; [] |496| 
	.dwpsn	file "../gateway.c",line 497,column 3,is_stmt
        MOVB      AL,#2                 ; [CPU_] |497| 
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |497| 
$C$DW$338	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$338, DW_AT_low_pc(0x00)
	.dwattr $C$DW$338, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$338, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |497| 
        ; call occurs [#_SEM_pend] ; [] |497| 
	.dwpsn	file "../gateway.c",line 498,column 3,is_stmt
$C$DW$339	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$339, DW_AT_low_pc(0x00)
	.dwattr $C$DW$339, DW_AT_name("_HAL_Reset")
	.dwattr $C$DW$339, DW_AT_TI_call
        LCR       #_HAL_Reset           ; [CPU_] |498| 
        ; call occurs [#_HAL_Reset] ; [] |498| 
	.dwpsn	file "../gateway.c",line 499,column 3,is_stmt
        MOV       AL,#28009             ; [CPU_] |499| 
        MOV       AH,#21093             ; [CPU_] |499| 
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVL      @_BootCommand,ACC     ; [CPU_] |499| 
	.dwpsn	file "../gateway.c",line 500,column 3,is_stmt
 LB 0x3F7FF6 
	.dwpsn	file "../gateway.c",line 501,column 1,is_stmt
        SPM       #0                    ; [CPU_] 
        SUBB      SP,#2                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$340	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$340, DW_AT_low_pc(0x00)
	.dwattr $C$DW$340, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$326, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$326, DW_AT_TI_end_line(0x1f5)
	.dwattr $C$DW$326, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$326

	.sect	".text"
	.clink
	.global	_TaskSciSendReceive

$C$DW$341	.dwtag  DW_TAG_subprogram, DW_AT_name("TaskSciSendReceive")
	.dwattr $C$DW$341, DW_AT_low_pc(_TaskSciSendReceive)
	.dwattr $C$DW$341, DW_AT_high_pc(0x00)
	.dwattr $C$DW$341, DW_AT_TI_symbol_name("_TaskSciSendReceive")
	.dwattr $C$DW$341, DW_AT_external
	.dwattr $C$DW$341, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$341, DW_AT_TI_begin_line(0x205)
	.dwattr $C$DW$341, DW_AT_TI_begin_column(0x06)
	.dwattr $C$DW$341, DW_AT_TI_max_frame_size(-30)
	.dwpsn	file "../gateway.c",line 517,column 30,is_stmt,address _TaskSciSendReceive

	.dwfde $C$DW$CIE, _TaskSciSendReceive

;***************************************************************
;* FNAME: _TaskSciSendReceive           FR SIZE:  28           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            2 Parameter, 25 Auto,  0 SOE     *
;***************************************************************

_TaskSciSendReceive:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#28                ; [CPU_U] 
	.dwcfi	cfa_offset, -30
$C$DW$342	.dwtag  DW_TAG_variable, DW_AT_name("start")
	.dwattr $C$DW$342, DW_AT_TI_symbol_name("_start")
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$342, DW_AT_location[DW_OP_breg20 -4]
$C$DW$343	.dwtag  DW_TAG_variable, DW_AT_name("insulation_counter")
	.dwattr $C$DW$343, DW_AT_TI_symbol_name("_insulation_counter")
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$343, DW_AT_location[DW_OP_breg20 -6]
$C$DW$344	.dwtag  DW_TAG_variable, DW_AT_name("current_counter")
	.dwattr $C$DW$344, DW_AT_TI_symbol_name("_current_counter")
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$344, DW_AT_location[DW_OP_breg20 -8]
$C$DW$345	.dwtag  DW_TAG_variable, DW_AT_name("voltage_counter")
	.dwattr $C$DW$345, DW_AT_TI_symbol_name("_voltage_counter")
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$345, DW_AT_location[DW_OP_breg20 -10]
$C$DW$346	.dwtag  DW_TAG_variable, DW_AT_name("Precharge_counter")
	.dwattr $C$DW$346, DW_AT_TI_symbol_name("_Precharge_counter")
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$346, DW_AT_location[DW_OP_breg20 -12]
$C$DW$347	.dwtag  DW_TAG_variable, DW_AT_name("sleep_counter")
	.dwattr $C$DW$347, DW_AT_TI_symbol_name("_sleep_counter")
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$347, DW_AT_location[DW_OP_breg20 -14]
$C$DW$348	.dwtag  DW_TAG_variable, DW_AT_name("current_counter2")
	.dwattr $C$DW$348, DW_AT_TI_symbol_name("_current_counter2")
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$348, DW_AT_location[DW_OP_breg20 -16]
$C$DW$349	.dwtag  DW_TAG_variable, DW_AT_name("reset_relay")
	.dwattr $C$DW$349, DW_AT_TI_symbol_name("_reset_relay")
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$349, DW_AT_location[DW_OP_breg20 -18]
$C$DW$350	.dwtag  DW_TAG_variable, DW_AT_name("repeatcheck")
	.dwattr $C$DW$350, DW_AT_TI_symbol_name("_repeatcheck")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$350, DW_AT_location[DW_OP_breg20 -20]
$C$DW$351	.dwtag  DW_TAG_variable, DW_AT_name("com_counter")
	.dwattr $C$DW$351, DW_AT_TI_symbol_name("_com_counter")
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$351, DW_AT_location[DW_OP_breg20 -21]
$C$DW$352	.dwtag  DW_TAG_variable, DW_AT_name("cur")
	.dwattr $C$DW$352, DW_AT_TI_symbol_name("_cur")
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$352, DW_AT_location[DW_OP_breg20 -22]
$C$DW$353	.dwtag  DW_TAG_variable, DW_AT_name("BatteryCurrent")
	.dwattr $C$DW$353, DW_AT_TI_symbol_name("_BatteryCurrent")
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$353, DW_AT_location[DW_OP_breg20 -23]
$C$DW$354	.dwtag  DW_TAG_variable, DW_AT_name("volt")
	.dwattr $C$DW$354, DW_AT_TI_symbol_name("_volt")
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$354, DW_AT_location[DW_OP_breg20 -24]
$C$DW$355	.dwtag  DW_TAG_variable, DW_AT_name("curf")
	.dwattr $C$DW$355, DW_AT_TI_symbol_name("_curf")
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$355, DW_AT_location[DW_OP_breg20 -26]
$C$DW$356	.dwtag  DW_TAG_variable, DW_AT_name("checkenable")
	.dwattr $C$DW$356, DW_AT_TI_symbol_name("_checkenable")
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$356, DW_AT_location[DW_OP_breg20 -27]
	.dwpsn	file "../gateway.c",line 518,column 45,is_stmt
        MOVB      ACC,#0                ; [CPU_] |518| 
        MOVL      *-SP[6],ACC           ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 518,column 66,is_stmt
        MOVL      *-SP[8],ACC           ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 518,column 87,is_stmt
        MOVL      *-SP[10],ACC          ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 518,column 109,is_stmt
        MOVL      *-SP[12],ACC          ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 518,column 128,is_stmt
        MOVL      *-SP[14],ACC          ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 518,column 150,is_stmt
        MOVL      *-SP[16],ACC          ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 518,column 167,is_stmt
        MOVL      *-SP[18],ACC          ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 518,column 184,is_stmt
        MOVL      *-SP[20],ACC          ; [CPU_] |518| 
	.dwpsn	file "../gateway.c",line 519,column 22,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |519| 
	.dwpsn	file "../gateway.c",line 524,column 21,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |524| 
	.dwpsn	file "../gateway.c",line 525,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#-1 ; [CPU_] |525| 
	.dwpsn	file "../gateway.c",line 526,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |526| 
        BF        $C$L59,NEQ            ; [CPU_] |526| 
        ; branchcc occurs ; [] |526| 
$C$L58:    
	.dwpsn	file "../gateway.c",line 527,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |527| 
        MOVB      AL,#1                 ; [CPU_] |527| 
$C$DW$357	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$357, DW_AT_low_pc(0x00)
	.dwattr $C$DW$357, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$357, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |527| 
        ; call occurs [#_SEM_pend] ; [] |527| 
	.dwpsn	file "../gateway.c",line 526,column 9,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |526| 
        BF        $C$L58,EQ             ; [CPU_] |526| 
        ; branchcc occurs ; [] |526| 
$C$L59:    
	.dwpsn	file "../gateway.c",line 529,column 10,is_stmt
        CMPB      AL,#1                 ; [CPU_] |529| 
        BF        $C$L61,EQ             ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
$C$L60:    
	.dwpsn	file "../gateway.c",line 531,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |531| 
        MOVB      AL,#1                 ; [CPU_] |531| 
$C$DW$358	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$358, DW_AT_low_pc(0x00)
	.dwattr $C$DW$358, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$358, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |531| 
        ; call occurs [#_SEM_pend] ; [] |531| 
	.dwpsn	file "../gateway.c",line 529,column 10,is_stmt
        MOVW      DP,#_InitOK           ; [CPU_U] 
        MOV       AL,@_InitOK           ; [CPU_] |529| 
        CMPB      AL,#1                 ; [CPU_] |529| 
        BF        $C$L60,NEQ            ; [CPU_] |529| 
        ; branchcc occurs ; [] |529| 
$C$L61:    
	.dwpsn	file "../gateway.c",line 533,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |533| 
        MOVB      AL,#10                ; [CPU_] |533| 
$C$DW$359	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$359, DW_AT_low_pc(0x00)
	.dwattr $C$DW$359, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$359, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |533| 
        ; call occurs [#_SEM_pend] ; [] |533| 
	.dwpsn	file "../gateway.c",line 534,column 3,is_stmt
        MOVW      DP,#_ODV_MachineEvent ; [CPU_U] 
        MOV       @_ODV_MachineEvent,#0 ; [CPU_] |534| 
	.dwpsn	file "../gateway.c",line 535,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |535| 
	.dwpsn	file "../gateway.c",line 536,column 3,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |536| 
	.dwpsn	file "../gateway.c",line 537,column 3,is_stmt
        MOVW      DP,#_ODV_Gateway_Errorcode ; [CPU_U] 
        MOVB      ACC,#0                ; [CPU_] |537| 
        MOVL      @_ODV_Gateway_Errorcode,ACC ; [CPU_] |537| 
	.dwpsn	file "../gateway.c",line 538,column 3,is_stmt
        MOVB      XAR5,#0               ; [CPU_] |538| 
        MOVB      AL,#9                 ; [CPU_] |538| 
        MOVB      AH,#7                 ; [CPU_] |538| 
        MOVL      XAR4,#_ODV_Gateway_Date_Time ; [CPU_U] |538| 
$C$DW$360	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$360, DW_AT_low_pc(0x00)
	.dwattr $C$DW$360, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$360, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |538| 
        ; call occurs [#_I2C_Command] ; [] |538| 
	.dwpsn	file "../gateway.c",line 543,column 3,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |543| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |543| 
        BF        $C$L62,TC             ; [CPU_] |543| 
        ; branchcc occurs ; [] |543| 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |543| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |543| 
        LSR       AL,1                  ; [CPU_] |543| 
        CMPB      AL,#1                 ; [CPU_] |543| 
        BF        $C$L62,NEQ            ; [CPU_] |543| 
        ; branchcc occurs ; [] |543| 
	.dwpsn	file "../gateway.c",line 544,column 5,is_stmt
        MOVW      DP,#_ODP_CommError_TimeOut ; [CPU_U] 
        MOV       @_ODP_CommError_TimeOut,#0 ; [CPU_] |544| 
	.dwpsn	file "../gateway.c",line 545,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |545| 
$C$L62:    
	.dwpsn	file "../gateway.c",line 548,column 3,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |548| 
        MOV       AL,#500               ; [CPU_] |548| 
$C$DW$361	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$361, DW_AT_low_pc(0x00)
	.dwattr $C$DW$361, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$361, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |548| 
        ; call occurs [#_SEM_pend] ; [] |548| 
	.dwpsn	file "../gateway.c",line 549,column 3,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOV       AL,@_ODP_NbOfModules  ; [CPU_] |549| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        MOV       @_Modules_Present,AL  ; [CPU_] |549| 
	.dwpsn	file "../gateway.c",line 550,column 10,is_stmt
$C$L63:    
	.dwpsn	file "../gateway.c",line 552,column 4,is_stmt
        MOVB      ACC,#1                ; [CPU_] |552| 
        MOVW      DP,#_TimerTemp        ; [CPU_U] 
        ADDL      @_TimerTemp,ACC       ; [CPU_] |552| 
	.dwpsn	file "../gateway.c",line 553,column 4,is_stmt
        INC       @_CANbus_timer        ; [CPU_] |553| 
	.dwpsn	file "../gateway.c",line 554,column 4,is_stmt
        INC       @_CANbus_timer2       ; [CPU_] |554| 
	.dwpsn	file "../gateway.c",line 556,column 3,is_stmt
        CMP       @_CANbus_timer,#1000  ; [CPU_] |556| 
        B         $C$L73,LOS            ; [CPU_] |556| 
        ; branchcc occurs ; [] |556| 
	.dwpsn	file "../gateway.c",line 558,column 4,is_stmt
        MOVW      DP,#_EVOCharger       ; [CPU_U] 
        MOVB      @_EVOCharger,#136,UNC ; [CPU_] |558| 
	.dwpsn	file "../gateway.c",line 560,column 4,is_stmt
        MOVB      @_EVOCharger+1,#160,UNC ; [CPU_] |560| 
	.dwpsn	file "../gateway.c",line 561,column 4,is_stmt
        MOV       @_EVOCharger+2,#9540  ; [CPU_] |561| 
	.dwpsn	file "../gateway.c",line 562,column 4,is_stmt
        MOV       @_EVOCharger+3,#0     ; [CPU_] |562| 
	.dwpsn	file "../gateway.c",line 565,column 4,is_stmt
        MOVIZ     R1H,#16288            ; [CPU_] |565| 
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        I16TOF32  R0H,@_ODV_Gateway_Current ; [CPU_] |565| 
$C$DW$362	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$362, DW_AT_low_pc(0x00)
	.dwattr $C$DW$362, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$362, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |565| 
        ; call occurs [#FS$$DIV] ; [] |565| 
$C$DW$363	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$363, DW_AT_low_pc(0x00)
	.dwattr $C$DW$363, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$363, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |565| 
        ; call occurs [#_CNV_Round] ; [] |565| 
        MOV       *-SP[23],AL           ; [CPU_] |565| 
	.dwpsn	file "../gateway.c",line 568,column 4,is_stmt
        MOVW      DP,#_ODV_Gateway_Requested_ChargerStage ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Requested_ChargerStage ; [CPU_] |568| 
        MOVW      DP,#_Charger_Current_State ; [CPU_U] 
        MOV       AL,@_Charger_Current_State ; [CPU_] |568| 
$C$DW$364	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$364, DW_AT_low_pc(0x00)
	.dwattr $C$DW$364, DW_AT_name("_CheckState_Change")
	.dwattr $C$DW$364, DW_AT_TI_call
        LCR       #_CheckState_Change   ; [CPU_] |568| 
        ; call occurs [#_CheckState_Change] ; [] |568| 
        CMPB      AL,#0                 ; [CPU_] |568| 
        BF        $C$L68,EQ             ; [CPU_] |568| 
        ; branchcc occurs ; [] |568| 
	.dwpsn	file "../gateway.c",line 569,column 5,is_stmt
        MOV       @_Charger_Current_State,#0 ; [CPU_] |569| 
	.dwpsn	file "../gateway.c",line 572,column 4,is_stmt
        B         $C$L68,UNC            ; [CPU_] |572| 
        ; branch occurs ; [] |572| 
$C$L64:    
	.dwpsn	file "../gateway.c",line 574,column 5,is_stmt
$C$DW$365	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$365, DW_AT_low_pc(0x00)
	.dwattr $C$DW$365, DW_AT_name("_InitialReset")
	.dwattr $C$DW$365, DW_AT_TI_call
        LCR       #_InitialReset        ; [CPU_] |574| 
        ; call occurs [#_InitialReset] ; [] |574| 
	.dwpsn	file "../gateway.c",line 575,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Requested_ChargerStage ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_Requested_ChargerStage ; [CPU_] |575| 
        MOVW      DP,#_Charger_Current_State ; [CPU_U] 
        MOV       @_Charger_Current_State,AL ; [CPU_] |575| 
	.dwpsn	file "../gateway.c",line 576,column 5,is_stmt
        B         $C$L69,UNC            ; [CPU_] |576| 
        ; branch occurs ; [] |576| 
$C$L65:    
	.dwpsn	file "../gateway.c",line 578,column 5,is_stmt
        MOV       AL,@_BatteryVoltage   ; [CPU_] |578| 
$C$DW$366	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$366, DW_AT_low_pc(0x00)
	.dwattr $C$DW$366, DW_AT_name("_Nocharge_Mode")
	.dwattr $C$DW$366, DW_AT_TI_call
        LCR       #_Nocharge_Mode       ; [CPU_] |578| 
        ; call occurs [#_Nocharge_Mode] ; [] |578| 
        MOVW      DP,#_EVOCharger+3     ; [CPU_U] 
        MOV       @_EVOCharger+3,AL     ; [CPU_] |578| 
	.dwpsn	file "../gateway.c",line 579,column 5,is_stmt
        B         $C$L69,UNC            ; [CPU_] |579| 
        ; branch occurs ; [] |579| 
$C$L66:    
	.dwpsn	file "../gateway.c",line 581,column 5,is_stmt
        MOV       AL,@_BatteryVoltage   ; [CPU_] |581| 
$C$DW$367	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$367, DW_AT_low_pc(0x00)
	.dwattr $C$DW$367, DW_AT_name("_FullCharge_Mode")
	.dwattr $C$DW$367, DW_AT_TI_call
        LCR       #_FullCharge_Mode     ; [CPU_] |581| 
        ; call occurs [#_FullCharge_Mode] ; [] |581| 
        MOVW      DP,#_EVOCharger+3     ; [CPU_U] 
        MOV       @_EVOCharger+3,AL     ; [CPU_] |581| 
	.dwpsn	file "../gateway.c",line 582,column 5,is_stmt
        B         $C$L69,UNC            ; [CPU_] |582| 
        ; branch occurs ; [] |582| 
$C$L67:    
	.dwpsn	file "../gateway.c",line 584,column 5,is_stmt
        MOV       AL,@_BatteryVoltage   ; [CPU_] |584| 
$C$DW$368	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$368, DW_AT_low_pc(0x00)
	.dwattr $C$DW$368, DW_AT_name("_TrickleCharge_Mode")
	.dwattr $C$DW$368, DW_AT_TI_call
        LCR       #_TrickleCharge_Mode  ; [CPU_] |584| 
        ; call occurs [#_TrickleCharge_Mode] ; [] |584| 
        MOVW      DP,#_EVOCharger+3     ; [CPU_U] 
        MOV       @_EVOCharger+3,AL     ; [CPU_] |584| 
	.dwpsn	file "../gateway.c",line 585,column 5,is_stmt
        B         $C$L69,UNC            ; [CPU_] |585| 
        ; branch occurs ; [] |585| 
$C$L68:    
	.dwpsn	file "../gateway.c",line 572,column 4,is_stmt
        MOV       AL,@_Charger_Current_State ; [CPU_] |572| 
        BF        $C$L64,EQ             ; [CPU_] |572| 
        ; branchcc occurs ; [] |572| 
        CMPB      AL,#1                 ; [CPU_] |572| 
        BF        $C$L65,EQ             ; [CPU_] |572| 
        ; branchcc occurs ; [] |572| 
        CMPB      AL,#2                 ; [CPU_] |572| 
        BF        $C$L66,EQ             ; [CPU_] |572| 
        ; branchcc occurs ; [] |572| 
        CMPB      AL,#3                 ; [CPU_] |572| 
        BF        $C$L67,EQ             ; [CPU_] |572| 
        ; branchcc occurs ; [] |572| 
$C$L69:    
	.dwpsn	file "../gateway.c",line 590,column 4,is_stmt
        MOVW      DP,#_EVOCharger+3     ; [CPU_U] 
        MOV       AL,@_EVOCharger+3     ; [CPU_] |590| 
        BF        $C$L71,EQ             ; [CPU_] |590| 
        ; branchcc occurs ; [] |590| 
        MOV       AL,*-SP[23]           ; [CPU_] |590| 
        CMPB      AL,#5                 ; [CPU_] |590| 
        B         $C$L71,GT             ; [CPU_] |590| 
        ; branchcc occurs ; [] |590| 
        MOVW      DP,#_BatteryVoltage   ; [CPU_U] 
        CMP       @_BatteryVoltage,#750 ; [CPU_] |590| 
        B         $C$L71,HI             ; [CPU_] |590| 
        ; branchcc occurs ; [] |590| 
        MOV       AL,@_BatteryVoltage   ; [CPU_] |590| 
        CMPB      AL,#30                ; [CPU_] |590| 
        B         $C$L71,LO             ; [CPU_] |590| 
        ; branchcc occurs ; [] |590| 
	.dwpsn	file "../gateway.c",line 591,column 5,is_stmt
        INC       @_CANbus_timer3       ; [CPU_] |591| 
	.dwpsn	file "../gateway.c",line 592,column 5,is_stmt
        CMP       @_CANbus_timer3,#20000 ; [CPU_] |592| 
        B         $C$L72,LOS            ; [CPU_] |592| 
        ; branchcc occurs ; [] |592| 
	.dwpsn	file "../gateway.c",line 593,column 6,is_stmt
        MOV       AL,@_CounterReset     ; [CPU_] |593| 
        BF        $C$L72,EQ             ; [CPU_] |593| 
        ; branchcc occurs ; [] |593| 
	.dwpsn	file "../gateway.c",line 594,column 7,is_stmt
        MOVW      DP,#_ODP_CommError_Charge_NotSucessful ; [CPU_U] 
        MOV       AL,@_ODP_CommError_Charge_NotSucessful ; [CPU_] |594| 
        CMPB      AL,#8                 ; [CPU_] |594| 
        B         $C$L70,LOS            ; [CPU_] |594| 
        ; branchcc occurs ; [] |594| 
	.dwpsn	file "../gateway.c",line 594,column 49,is_stmt
        MOV       @_ODP_CommError_Charge_NotSucessful,#0 ; [CPU_] |594| 
$C$L70:    
	.dwpsn	file "../gateway.c",line 595,column 7,is_stmt
        INC       @_ODP_CommError_Charge_NotSucessful ; [CPU_] |595| 
	.dwpsn	file "../gateway.c",line 596,column 7,is_stmt
        MOV       AL,#-5                ; [CPU_] |596| 
$C$DW$369	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$369, DW_AT_low_pc(0x00)
	.dwattr $C$DW$369, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$369, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |596| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |596| 
	.dwpsn	file "../gateway.c",line 597,column 7,is_stmt
$C$DW$370	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$370, DW_AT_low_pc(0x00)
	.dwattr $C$DW$370, DW_AT_name("_PAR_AddLog")
	.dwattr $C$DW$370, DW_AT_TI_call
        LCR       #_PAR_AddLog          ; [CPU_] |597| 
        ; call occurs [#_PAR_AddLog] ; [] |597| 
	.dwpsn	file "../gateway.c",line 598,column 7,is_stmt
        MOVW      DP,#_CounterReset     ; [CPU_U] 
        MOV       @_CounterReset,#0     ; [CPU_] |598| 
	.dwpsn	file "../gateway.c",line 601,column 4,is_stmt
        B         $C$L72,UNC            ; [CPU_] |601| 
        ; branch occurs ; [] |601| 
$C$L71:    
	.dwpsn	file "../gateway.c",line 602,column 5,is_stmt
        MOVW      DP,#_CounterReset     ; [CPU_U] 
        MOVB      @_CounterReset,#1,UNC ; [CPU_] |602| 
	.dwpsn	file "../gateway.c",line 603,column 5,is_stmt
        MOV       @_CANbus_timer3,#0    ; [CPU_] |603| 
$C$L72:    
	.dwpsn	file "../gateway.c",line 607,column 4,is_stmt
$C$DW$371	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$371, DW_AT_low_pc(0x00)
	.dwattr $C$DW$371, DW_AT_name("_ChargerCommError")
	.dwattr $C$DW$371, DW_AT_TI_call
        LCR       #_ChargerCommError    ; [CPU_] |607| 
        ; call occurs [#_ChargerCommError] ; [] |607| 
	.dwpsn	file "../gateway.c",line 608,column 4,is_stmt
$C$DW$372	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$372, DW_AT_low_pc(0x00)
	.dwattr $C$DW$372, DW_AT_name("_ChargerError")
	.dwattr $C$DW$372, DW_AT_TI_call
        LCR       #_ChargerError        ; [CPU_] |608| 
        ; call occurs [#_ChargerError] ; [] |608| 
	.dwpsn	file "../gateway.c",line 612,column 4,is_stmt
        MOVW      DP,#_CANbus_timer     ; [CPU_U] 
        MOV       @_CANbus_timer,#0     ; [CPU_] |612| 
$C$L73:    
	.dwpsn	file "../gateway.c",line 615,column 3,is_stmt
        MOV       AL,@_CANbus_timer2    ; [CPU_] |615| 
        CMPB      AL,#100               ; [CPU_] |615| 
        B         $C$L74,LOS            ; [CPU_] |615| 
        ; branchcc occurs ; [] |615| 
	.dwpsn	file "../gateway.c",line 616,column 4,is_stmt
        MOV       @_CANbus_timer2,#0    ; [CPU_] |616| 
	.dwpsn	file "../gateway.c",line 617,column 4,is_stmt
        MOVL      XAR4,#_EVOCharger     ; [CPU_U] |617| 
        MOVL      XAR5,#_SlaveData_Frame ; [CPU_U] |617| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |617| 
        MOVL      XAR4,#_MasterData_Frame ; [CPU_U] |617| 
$C$DW$373	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$373, DW_AT_low_pc(0x00)
	.dwattr $C$DW$373, DW_AT_name("_Charger_Control")
	.dwattr $C$DW$373, DW_AT_TI_call
        LCR       #_Charger_Control     ; [CPU_] |617| 
        ; call occurs [#_Charger_Control] ; [] |617| 
$C$L74:    
	.dwpsn	file "../gateway.c",line 620,column 4,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       AL,@_ODV_Write_Outputs_16_Bit ; [CPU_] |620| 
        MOVW      DP,#_ODV_Board_RelayStatus ; [CPU_U] 
        MOV       @_ODV_Board_RelayStatus,AL ; [CPU_] |620| 
	.dwpsn	file "../gateway.c",line 622,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |622| 
        ANDB      AL,#0x0f              ; [CPU_] |622| 
        CMPB      AL,#8                 ; [CPU_] |622| 
        BF        $C$L75,NEQ            ; [CPU_] |622| 
        ; branchcc occurs ; [] |622| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |622| 
        CMPB      AL,#8                 ; [CPU_] |622| 
        BF        $C$L75,EQ             ; [CPU_] |622| 
        ; branchcc occurs ; [] |622| 
	.dwpsn	file "../gateway.c",line 623,column 7,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |623| 
        MOVL      *-SP[4],ACC           ; [CPU_] |623| 
	.dwpsn	file "../gateway.c",line 624,column 7,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#8,UNC ; [CPU_] |624| 
$C$L75:    
	.dwpsn	file "../gateway.c",line 626,column 5,is_stmt
        MOVW      DP,#_Sync_count       ; [CPU_U] 
        MOV       AL,@_Sync_count       ; [CPU_] |626| 
        CMPB      AL,#99                ; [CPU_] |626| 
        BF        $C$L77,NEQ            ; [CPU_] |626| 
        ; branchcc occurs ; [] |626| 
	.dwpsn	file "../gateway.c",line 627,column 7,is_stmt
        DEC       @_Sync_count          ; [CPU_] |627| 
	.dwpsn	file "../gateway.c",line 628,column 7,is_stmt
        MOVW      DP,#_ODP_NbOfModules  ; [CPU_U] 
        MOVU      ACC,@_ODP_NbOfModules ; [CPU_] |628| 
        MOVW      DP,#_Modules_Present  ; [CPU_U] 
        MOVZ      AR6,@_Modules_Present ; [CPU_] |628| 
        CMPL      ACC,XAR6              ; [CPU_] |628| 
        BF        $C$L76,EQ             ; [CPU_] |628| 
        ; branchcc occurs ; [] |628| 
	.dwpsn	file "../gateway.c",line 629,column 9,is_stmt
        INC       *-SP[21]              ; [CPU_] |629| 
	.dwpsn	file "../gateway.c",line 630,column 9,is_stmt
        MOVW      DP,#_ODP_Gateway_IsoResistor_Limit_Max ; [CPU_U] 
        MOVZ      AR6,*-SP[21]          ; [CPU_] |630| 
        MOV       AL,@_ODP_Gateway_IsoResistor_Limit_Max ; [CPU_] |630| 
        ADDB      AL,#6                 ; [CPU_] |630| 
        MOVU      ACC,AL                ; [CPU_] |630| 
        CMPL      ACC,XAR6              ; [CPU_] |630| 
        BF        $C$L77,NEQ            ; [CPU_] |630| 
        ; branchcc occurs ; [] |630| 
	.dwpsn	file "../gateway.c",line 631,column 10,is_stmt
        MOVL      XAR4,#65536           ; [CPU_U] |631| 
        MOVL      ACC,XAR4              ; [CPU_] |631| 
$C$DW$374	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$374, DW_AT_low_pc(0x00)
	.dwattr $C$DW$374, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$374, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |631| 
        ; call occurs [#_ERR_HandleWarning] ; [] |631| 
	.dwpsn	file "../gateway.c",line 636,column 7,is_stmt
        B         $C$L77,UNC            ; [CPU_] |636| 
        ; branch occurs ; [] |636| 
$C$L76:    
	.dwpsn	file "../gateway.c",line 638,column 8,is_stmt
        MOV       *-SP[21],#0           ; [CPU_] |638| 
	.dwpsn	file "../gateway.c",line 639,column 11,is_stmt
        MOVL      XAR4,#65536           ; [CPU_U] |639| 
        MOVL      ACC,XAR4              ; [CPU_] |639| 
$C$DW$375	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$375, DW_AT_low_pc(0x00)
	.dwattr $C$DW$375, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$375, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |639| 
        ; call occurs [#_ERR_ClearWarning] ; [] |639| 
$C$L77:    
	.dwpsn	file "../gateway.c",line 642,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AH,@_ODV_Gateway_Voltage ; [CPU_] |642| 
        MOV       AL,AH                 ; [CPU_] |642| 
        ASR       AL,1                  ; [CPU_] |642| 
        LSR       AL,14                 ; [CPU_] |642| 
        ADD       AL,AH                 ; [CPU_] |642| 
        ASR       AL,2                  ; [CPU_] |642| 
        MOV       *-SP[24],AL           ; [CPU_] |642| 
	.dwpsn	file "../gateway.c",line 645,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |645| 
        B         $C$L163,LEQ           ; [CPU_] |645| 
        ; branchcc occurs ; [] |645| 
        CMPB      AL,#4                 ; [CPU_] |645| 
        B         $C$L163,GT            ; [CPU_] |645| 
        ; branchcc occurs ; [] |645| 
	.dwpsn	file "../gateway.c",line 646,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |646| 
        BF        $C$L78,NEQ            ; [CPU_] |646| 
        ; branchcc occurs ; [] |646| 
	.dwpsn	file "../gateway.c",line 650,column 9,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |650| 
	.dwpsn	file "../gateway.c",line 651,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |651| 
	.dwpsn	file "../gateway.c",line 652,column 9,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |652| 
$C$L78:    
	.dwpsn	file "../gateway.c",line 655,column 7,is_stmt
        MOVW      DP,#_ODV_Gateway_Current ; [CPU_U] 
        MOVIZ     R1H,#16288            ; [CPU_] |655| 
        I16TOF32  R0H,@_ODV_Gateway_Current ; [CPU_] |655| 
$C$DW$376	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$376, DW_AT_low_pc(0x00)
	.dwattr $C$DW$376, DW_AT_name("FS$$DIV")
	.dwattr $C$DW$376, DW_AT_TI_call
        LCR       #FS$$DIV              ; [CPU_] |655| 
        ; call occurs [#FS$$DIV] ; [] |655| 
        MOV32     *-SP[26],R0H          ; [CPU_] |655| 
	.dwpsn	file "../gateway.c",line 656,column 7,is_stmt
$C$DW$377	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$377, DW_AT_low_pc(0x00)
	.dwattr $C$DW$377, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$377, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |656| 
        ; call occurs [#_CNV_Round] ; [] |656| 
        MOV       *-SP[22],AL           ; [CPU_] |656| 
	.dwpsn	file "../gateway.c",line 659,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Overcurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Overcurrent ; [CPU_] |659| 
        CMP       AL,*-SP[22]           ; [CPU_] |659| 
        B         $C$L79,LT             ; [CPU_] |659| 
        ; branchcc occurs ; [] |659| 
        MOVW      DP,#_ODP_SafetyLimits_UnderCurrent ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderCurrent ; [CPU_] |659| 
        CMP       AL,*-SP[22]           ; [CPU_] |659| 
        B         $C$L81,LEQ            ; [CPU_] |659| 
        ; branchcc occurs ; [] |659| 
$C$L79:    
	.dwpsn	file "../gateway.c",line 661,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |661| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        ADDL      @_GV_current_counter,ACC ; [CPU_] |661| 
	.dwpsn	file "../gateway.c",line 662,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |662| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |662| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        CMPL      ACC,@_GV_current_counter ; [CPU_] |662| 
        B         $C$L82,HIS            ; [CPU_] |662| 
        ; branchcc occurs ; [] |662| 
	.dwpsn	file "../gateway.c",line 664,column 9,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |664| 
        BF        $C$L80,NEQ            ; [CPU_] |664| 
        ; branchcc occurs ; [] |664| 
	.dwpsn	file "../gateway.c",line 664,column 44,is_stmt
$C$DW$378	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$378, DW_AT_low_pc(0x00)
	.dwattr $C$DW$378, DW_AT_name("_ERR_ErrorOverCurrent")
	.dwattr $C$DW$378, DW_AT_TI_call
        LCR       #_ERR_ErrorOverCurrent ; [CPU_] |664| 
        ; call occurs [#_ERR_ErrorOverCurrent] ; [] |664| 
$C$L80:    
	.dwpsn	file "../gateway.c",line 666,column 6,is_stmt
        MOVW      DP,#_ODP_Settings_AUD_Gateway_Current_Ringsaver ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_Gateway_Current_Ringsaver ; [CPU_] |666| 
        CMP       AL,*-SP[22]           ; [CPU_] |666| 
        B         $C$L82,HIS            ; [CPU_] |666| 
        ; branchcc occurs ; [] |666| 
	.dwpsn	file "../gateway.c",line 672,column 7,is_stmt
        B         $C$L82,UNC            ; [CPU_] |672| 
        ; branch occurs ; [] |672| 
$C$L81:    
	.dwpsn	file "../gateway.c",line 675,column 8,is_stmt
        MOVB      ACC,#0                ; [CPU_] |675| 
        MOVW      DP,#_GV_current_counter ; [CPU_U] 
        MOVL      @_GV_current_counter,ACC ; [CPU_] |675| 
$C$L82:    
	.dwpsn	file "../gateway.c",line 678,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |678| 
        CMP       AL,*-SP[24]           ; [CPU_] |678| 
        B         $C$L84,HI             ; [CPU_] |678| 
        ; branchcc occurs ; [] |678| 
	.dwpsn	file "../gateway.c",line 680,column 5,is_stmt
        MOVB      ACC,#1                ; [CPU_] |680| 
        MOVW      DP,#_GV_Overvoltage_counter ; [CPU_U] 
        ADDL      @_GV_Overvoltage_counter,ACC ; [CPU_] |680| 
	.dwpsn	file "../gateway.c",line 681,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |681| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |681| 
        MOVW      DP,#_GV_Overvoltage_counter ; [CPU_U] 
        CMPL      ACC,@_GV_Overvoltage_counter ; [CPU_] |681| 
        B         $C$L85,HIS            ; [CPU_] |681| 
        ; branchcc occurs ; [] |681| 
	.dwpsn	file "../gateway.c",line 683,column 9,is_stmt
        MOVW      DP,#_ODP_CommError_OverVoltage_ErrCounter ; [CPU_U] 
        MOV       AL,@_ODP_CommError_OverVoltage_ErrCounter ; [CPU_] |683| 
        CMPB      AL,#250               ; [CPU_] |683| 
        B         $C$L83,LOS            ; [CPU_] |683| 
        ; branchcc occurs ; [] |683| 
	.dwpsn	file "../gateway.c",line 683,column 56,is_stmt
        MOVB      @_ODP_CommError_OverVoltage_ErrCounter,#255,UNC ; [CPU_] |683| 
$C$L83:    
	.dwpsn	file "../gateway.c",line 684,column 9,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |684| 
        BF        $C$L85,NEQ            ; [CPU_] |684| 
        ; branchcc occurs ; [] |684| 
	.dwpsn	file "../gateway.c",line 686,column 10,is_stmt
        MOVW      DP,#_ODP_CommError_OverVoltage_ErrCounter ; [CPU_U] 
        INC       @_ODP_CommError_OverVoltage_ErrCounter ; [CPU_] |686| 
	.dwpsn	file "../gateway.c",line 687,column 10,is_stmt
$C$DW$379	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$379, DW_AT_low_pc(0x00)
	.dwattr $C$DW$379, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$379, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |687| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |687| 
	.dwpsn	file "../gateway.c",line 690,column 7,is_stmt
        B         $C$L85,UNC            ; [CPU_] |690| 
        ; branch occurs ; [] |690| 
$C$L84:    
	.dwpsn	file "../gateway.c",line 693,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |693| 
        MOVW      DP,#_GV_Overvoltage_counter ; [CPU_U] 
        MOVL      @_GV_Overvoltage_counter,ACC ; [CPU_] |693| 
$C$L85:    
	.dwpsn	file "../gateway.c",line 697,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |697| 
        CMP       AL,*-SP[24]           ; [CPU_] |697| 
        B         $C$L86,LO             ; [CPU_] |697| 
        ; branchcc occurs ; [] |697| 
	.dwpsn	file "../gateway.c",line 699,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |699| 
	.dwpsn	file "../gateway.c",line 700,column 5,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |700| 
        MOVL      *-SP[4],ACC           ; [CPU_] |700| 
	.dwpsn	file "../gateway.c",line 701,column 5,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |701| 
	.dwpsn	file "../gateway.c",line 703,column 5,is_stmt
        MOVW      DP,#_GV_Undervoltage_counter ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |703| 
        ADDL      @_GV_Undervoltage_counter,ACC ; [CPU_] |703| 
	.dwpsn	file "../gateway.c",line 704,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |704| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |704| 
        MOVW      DP,#_GV_Undervoltage_counter ; [CPU_U] 
        CMPL      ACC,@_GV_Undervoltage_counter ; [CPU_] |704| 
        B         $C$L87,HIS            ; [CPU_] |704| 
        ; branchcc occurs ; [] |704| 
	.dwpsn	file "../gateway.c",line 705,column 9,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |705| 
        BF        $C$L87,NEQ            ; [CPU_] |705| 
        ; branchcc occurs ; [] |705| 
	.dwpsn	file "../gateway.c",line 705,column 43,is_stmt
$C$DW$380	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$380, DW_AT_low_pc(0x00)
	.dwattr $C$DW$380, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$380, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |705| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |705| 
	.dwpsn	file "../gateway.c",line 707,column 7,is_stmt
        B         $C$L87,UNC            ; [CPU_] |707| 
        ; branch occurs ; [] |707| 
$C$L86:    
	.dwpsn	file "../gateway.c",line 710,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |710| 
        MOVW      DP,#_GV_Undervoltage_counter ; [CPU_U] 
        MOVL      @_GV_Undervoltage_counter,ACC ; [CPU_] |710| 
$C$L87:    
	.dwpsn	file "../gateway.c",line 714,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umin ; [CPU_] |714| 
        CMP       AL,*-SP[24]           ; [CPU_] |714| 
        B         $C$L88,LOS            ; [CPU_] |714| 
        ; branchcc occurs ; [] |714| 
	.dwpsn	file "../gateway.c",line 716,column 5,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |716| 
        MOVL      ACC,XAR4              ; [CPU_] |716| 
$C$DW$381	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$381, DW_AT_low_pc(0x00)
	.dwattr $C$DW$381, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$381, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |716| 
        ; call occurs [#_ERR_HandleWarning] ; [] |716| 
	.dwpsn	file "../gateway.c",line 717,column 7,is_stmt
        B         $C$L89,UNC            ; [CPU_] |717| 
        ; branch occurs ; [] |717| 
$C$L88:    
	.dwpsn	file "../gateway.c",line 720,column 5,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |720| 
        MOVL      ACC,XAR4              ; [CPU_] |720| 
$C$DW$382	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$382, DW_AT_low_pc(0x00)
	.dwattr $C$DW$382, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$382, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |720| 
        ; call occurs [#_ERR_ClearWarning] ; [] |720| 
$C$L89:    
	.dwpsn	file "../gateway.c",line 724,column 4,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |724| 
        CMP       AL,*-SP[24]           ; [CPU_] |724| 
        B         $C$L90,HIS            ; [CPU_] |724| 
        ; branchcc occurs ; [] |724| 
	.dwpsn	file "../gateway.c",line 726,column 5,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |726| 
        MOVL      ACC,XAR4              ; [CPU_] |726| 
$C$DW$383	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$383, DW_AT_low_pc(0x00)
	.dwattr $C$DW$383, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$383, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |726| 
        ; call occurs [#_ERR_HandleWarning] ; [] |726| 
	.dwpsn	file "../gateway.c",line 727,column 7,is_stmt
        B         $C$L91,UNC            ; [CPU_] |727| 
        ; branch occurs ; [] |727| 
$C$L90:    
	.dwpsn	file "../gateway.c",line 730,column 5,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |730| 
        MOVL      ACC,XAR4              ; [CPU_] |730| 
$C$DW$384	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$384, DW_AT_low_pc(0x00)
	.dwattr $C$DW$384, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$384, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |730| 
        ; call occurs [#_ERR_ClearWarning] ; [] |730| 
$C$L91:    
	.dwpsn	file "../gateway.c",line 733,column 7,is_stmt
        MOV32     R0H,*-SP[26]          ; [CPU_] |733| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |733| 
        NOP       ; [CPU_] 
        CMPF32    R0H,#0                ; [CPU_] |733| 
        MOVST0    ZF, NF                ; [CPU_] |733| 
        B         $C$L92,LT             ; [CPU_] |733| 
        ; branchcc occurs ; [] |733| 
        MOV32     R0H,*-SP[26]          ; [CPU_] |733| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |733| 
        B         $C$L93,UNC            ; [CPU_] |733| 
        ; branch occurs ; [] |733| 
$C$L92:    
        MOV32     R0H,*-SP[26]          ; [CPU_] |733| 
        MPYF32    R0H,R0H,#17530        ; [CPU_] |733| 
        NOP       ; [CPU_] 
        NEGF32    R0H,R0H               ; [CPU_] |733| 
$C$L93:    
        MOVW      DP,#_ODP_Sleep_Current ; [CPU_U] 
        UI16TOF32 R1H,@_ODP_Sleep_Current ; [CPU_] |733| 
        NOP       ; [CPU_] 
        CMPF32    R0H,R1H               ; [CPU_] |733| 
        MOVST0    ZF, NF                ; [CPU_] |733| 
        B         $C$L94,GEQ            ; [CPU_] |733| 
        ; branchcc occurs ; [] |733| 
	.dwpsn	file "../gateway.c",line 734,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |734| 
        ADDL      ACC,*-SP[14]          ; [CPU_] |734| 
        MOVL      *-SP[14],ACC          ; [CPU_] |734| 
	.dwpsn	file "../gateway.c",line 735,column 9,is_stmt
        MOV       T,#1000               ; [CPU_] |735| 
        MOVW      DP,#_ODP_Sleep_Timeout ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_Sleep_Timeout ; [CPU_] |735| 
        CMPL      ACC,*-SP[14]          ; [CPU_] |735| 
        B         $C$L95,HIS            ; [CPU_] |735| 
        ; branchcc occurs ; [] |735| 
	.dwpsn	file "../gateway.c",line 736,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |736| 
        MOVL      *-SP[14],ACC          ; [CPU_] |736| 
	.dwpsn	file "../gateway.c",line 739,column 7,is_stmt
        B         $C$L95,UNC            ; [CPU_] |739| 
        ; branch occurs ; [] |739| 
$C$L94:    
	.dwpsn	file "../gateway.c",line 740,column 12,is_stmt
        MOVL      ACC,*-SP[14]          ; [CPU_] |740| 
        BF        $C$L95,EQ             ; [CPU_] |740| 
        ; branchcc occurs ; [] |740| 
	.dwpsn	file "../gateway.c",line 740,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |740| 
        SUBL      *-SP[14],ACC          ; [CPU_] |740| 
$C$L95:    
	.dwpsn	file "../gateway.c",line 741,column 7,is_stmt
        MOVW      DP,#_ODP_RelayResetTime ; [CPU_U] 
        MOV       AL,@_ODP_RelayResetTime ; [CPU_] |741| 
        BF        $C$L97,EQ             ; [CPU_] |741| 
        ; branchcc occurs ; [] |741| 
        MOV       AL,*-SP[24]           ; [CPU_] |741| 
        CMPB      AL,#8                 ; [CPU_] |741| 
        B         $C$L97,HIS            ; [CPU_] |741| 
        ; branchcc occurs ; [] |741| 
	.dwpsn	file "../gateway.c",line 742,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |742| 
        ADDL      ACC,*-SP[18]          ; [CPU_] |742| 
        MOVL      *-SP[18],ACC          ; [CPU_] |742| 
	.dwpsn	file "../gateway.c",line 743,column 9,is_stmt
        MOV       T,#60000              ; [CPU_] |743| 
        MPYU      ACC,T,@_ODP_RelayResetTime ; [CPU_] |743| 
        CMPL      ACC,*-SP[18]          ; [CPU_] |743| 
        B         $C$L96,HI             ; [CPU_] |743| 
        ; branchcc occurs ; [] |743| 
	.dwpsn	file "../gateway.c",line 744,column 11,is_stmt
        MOVB      ACC,#0                ; [CPU_] |744| 
        MOVL      *-SP[18],ACC          ; [CPU_] |744| 
	.dwpsn	file "../gateway.c",line 745,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |745| 
	.dwpsn	file "../gateway.c",line 746,column 9,is_stmt
        B         $C$L98,UNC            ; [CPU_] |746| 
        ; branch occurs ; [] |746| 
$C$L96:    
	.dwpsn	file "../gateway.c",line 747,column 14,is_stmt
        CLRC      SXM                   ; [CPU_] 
        MOV       ACC,#50000            ; [CPU_] |747| 
        CMPL      ACC,*-SP[18]          ; [CPU_] |747| 
        B         $C$L98,HI             ; [CPU_] |747| 
        ; branchcc occurs ; [] |747| 
	.dwpsn	file "../gateway.c",line 747,column 40,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |747| 
	.dwpsn	file "../gateway.c",line 748,column 7,is_stmt
        B         $C$L98,UNC            ; [CPU_] |748| 
        ; branch occurs ; [] |748| 
$C$L97:    
	.dwpsn	file "../gateway.c",line 749,column 12,is_stmt
        MOVL      ACC,*-SP[18]          ; [CPU_] |749| 
        BF        $C$L98,EQ             ; [CPU_] |749| 
        ; branchcc occurs ; [] |749| 
	.dwpsn	file "../gateway.c",line 749,column 33,is_stmt
        MOVB      ACC,#0                ; [CPU_] |749| 
        MOVL      *-SP[18],ACC          ; [CPU_] |749| 
$C$L98:    
	.dwpsn	file "../gateway.c",line 750,column 7,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmax ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Resistor_Tmax ; [CPU_] |750| 
        MPYB      ACC,T,#10             ; [CPU_] |750| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       AL,@_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_] |750| 
        B         $C$L99,LEQ            ; [CPU_] |750| 
        ; branchcc occurs ; [] |750| 
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Tmin ; [CPU_U] 
        MOV       T,@_ODP_SafetyLimits_Resistor_Tmin ; [CPU_] |750| 
        MPYB      ACC,T,#10             ; [CPU_] |750| 
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       AL,@_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_] |750| 
        B         $C$L99,GEQ            ; [CPU_] |750| 
        ; branchcc occurs ; [] |750| 
        MOVW      DP,#_ODP_SafetyLimits_Tmax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Tmax ; [CPU_] |750| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_Temperature ; [CPU_] |750| 
        B         $C$L99,LT             ; [CPU_] |750| 
        ; branchcc occurs ; [] |750| 
        MOVW      DP,#_ODP_SafetyLimits_Tmin ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Tmin ; [CPU_] |750| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_Temperature ; [CPU_] |750| 
        B         $C$L104,LEQ           ; [CPU_] |750| 
        ; branchcc occurs ; [] |750| 
$C$L99:    
	.dwpsn	file "../gateway.c",line 753,column 8,is_stmt
        MOVW      DP,#_ODV_Read_Analogue_Input_16_Bit+1 ; [CPU_U] 
        CMP       @_ODV_Read_Analogue_Input_16_Bit+1,#-550 ; [CPU_] |753| 
        BF        $C$L100,EQ            ; [CPU_] |753| 
        ; branchcc occurs ; [] |753| 
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        CMP       @_ODV_Gateway_Temperature,#-55 ; [CPU_] |753| 
        BF        $C$L101,NEQ           ; [CPU_] |753| 
        ; branchcc occurs ; [] |753| 
$C$L100:    
	.dwpsn	file "../gateway.c",line 755,column 8,is_stmt
        MOVB      ACC,#0                ; [CPU_] |755| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        MOVL      @_GV_Temp_counter,ACC ; [CPU_] |755| 
$C$L101:    
	.dwpsn	file "../gateway.c",line 758,column 8,is_stmt
        MOVB      ACC,#1                ; [CPU_] |758| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        ADDL      @_GV_Temp_counter,ACC ; [CPU_] |758| 
	.dwpsn	file "../gateway.c",line 759,column 8,is_stmt
        MOV       T,#1000               ; [CPU_] |759| 
        MOVW      DP,#_ODP_Settings_AUD_Temperature_Delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_Settings_AUD_Temperature_Delay ; [CPU_] |759| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        CMPL      ACC,@_GV_Temp_counter ; [CPU_] |759| 
        B         $C$L105,HIS           ; [CPU_] |759| 
        ; branchcc occurs ; [] |759| 
	.dwpsn	file "../gateway.c",line 761,column 9,is_stmt
        MOVW      DP,#_ODP_Settings_AUD_Gateway_Current_Ringsaver ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_Gateway_Current_Ringsaver ; [CPU_] |761| 
        CMP       AL,*-SP[22]           ; [CPU_] |761| 
        B         $C$L103,LOS           ; [CPU_] |761| 
        ; branchcc occurs ; [] |761| 
	.dwpsn	file "../gateway.c",line 763,column 10,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |763| 
        BF        $C$L102,NEQ           ; [CPU_] |763| 
        ; branchcc occurs ; [] |763| 
	.dwpsn	file "../gateway.c",line 763,column 45,is_stmt
        MOVW      DP,#_ODP_Gateway_Relay_Error_Count ; [CPU_U] 
        INC       @_ODP_Gateway_Relay_Error_Count ; [CPU_] |763| 
$C$L102:    
	.dwpsn	file "../gateway.c",line 764,column 10,is_stmt
        MOV       AL,#-5                ; [CPU_] |764| 
$C$DW$385	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$385, DW_AT_low_pc(0x00)
	.dwattr $C$DW$385, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$385, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |764| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |764| 
$C$L103:    
	.dwpsn	file "../gateway.c",line 766,column 9,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |766| 
        BF        $C$L105,NEQ           ; [CPU_] |766| 
        ; branchcc occurs ; [] |766| 
	.dwpsn	file "../gateway.c",line 766,column 44,is_stmt
$C$DW$386	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$386, DW_AT_low_pc(0x00)
	.dwattr $C$DW$386, DW_AT_name("_ERR_ErrorOverTemp")
	.dwattr $C$DW$386, DW_AT_TI_call
        LCR       #_ERR_ErrorOverTemp   ; [CPU_] |766| 
        ; call occurs [#_ERR_ErrorOverTemp] ; [] |766| 
	.dwpsn	file "../gateway.c",line 768,column 7,is_stmt
        B         $C$L105,UNC           ; [CPU_] |768| 
        ; branch occurs ; [] |768| 
$C$L104:    
	.dwpsn	file "../gateway.c",line 771,column 8,is_stmt
        MOVB      ACC,#0                ; [CPU_] |771| 
        MOVW      DP,#_GV_Temp_counter  ; [CPU_U] 
        MOVL      @_GV_Temp_counter,ACC ; [CPU_] |771| 
$C$L105:    
	.dwpsn	file "../gateway.c",line 773,column 8,is_stmt
        MOVW      DP,#_ODV_Gateway_Temperature ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_Temperature ; [CPU_] |773| 
        CMPB      AL,#50                ; [CPU_] |773| 
        B         $C$L109,LEQ           ; [CPU_] |773| 
        ; branchcc occurs ; [] |773| 
	.dwpsn	file "../gateway.c",line 775,column 12,is_stmt
        MOVB      ACC,#1                ; [CPU_] |775| 
        MOVW      DP,#_GV_Temp_counter1 ; [CPU_U] 
        ADDL      @_GV_Temp_counter1,ACC ; [CPU_] |775| 
	.dwpsn	file "../gateway.c",line 777,column 12,is_stmt
        MOV       T,#1000               ; [CPU_] |777| 
        MOVW      DP,#_ODP_Settings_AUD_Temperature_Delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_Settings_AUD_Temperature_Delay ; [CPU_] |777| 
        MOVW      DP,#_GV_Temp_counter1 ; [CPU_U] 
        CMPL      ACC,@_GV_Temp_counter1 ; [CPU_] |777| 
        B         $C$L108,HIS           ; [CPU_] |777| 
        ; branchcc occurs ; [] |777| 
	.dwpsn	file "../gateway.c",line 779,column 13,is_stmt
        MOV       AL,@_EventResetTemp   ; [CPU_] |779| 
        BF        $C$L110,EQ            ; [CPU_] |779| 
        ; branchcc occurs ; [] |779| 
	.dwpsn	file "../gateway.c",line 781,column 8,is_stmt
        MOVW      DP,#_ODP_CommError_OverTemp_ErrCounter ; [CPU_U] 
        MOV       AL,@_ODP_CommError_OverTemp_ErrCounter ; [CPU_] |781| 
        CMPB      AL,#250               ; [CPU_] |781| 
        B         $C$L106,LOS           ; [CPU_] |781| 
        ; branchcc occurs ; [] |781| 
	.dwpsn	file "../gateway.c",line 781,column 51,is_stmt
        MOVB      @_ODP_CommError_OverTemp_ErrCounter,#255,UNC ; [CPU_] |781| 
$C$L106:    
	.dwpsn	file "../gateway.c",line 782,column 8,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |782| 
        BF        $C$L107,NEQ           ; [CPU_] |782| 
        ; branchcc occurs ; [] |782| 
	.dwpsn	file "../gateway.c",line 782,column 42,is_stmt
        MOVW      DP,#_ODP_CommError_OverTemp_ErrCounter ; [CPU_U] 
        INC       @_ODP_CommError_OverTemp_ErrCounter ; [CPU_] |782| 
$C$L107:    
	.dwpsn	file "../gateway.c",line 783,column 8,is_stmt
        MOVW      DP,#_EventResetTemp   ; [CPU_U] 
        MOV       @_EventResetTemp,#0   ; [CPU_] |783| 
	.dwpsn	file "../gateway.c",line 784,column 8,is_stmt
        MOV       AL,#-5                ; [CPU_] |784| 
$C$DW$387	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$387, DW_AT_low_pc(0x00)
	.dwattr $C$DW$387, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$387, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |784| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |784| 
	.dwpsn	file "../gateway.c",line 786,column 11,is_stmt
        B         $C$L110,UNC           ; [CPU_] |786| 
        ; branch occurs ; [] |786| 
$C$L108:    
	.dwpsn	file "../gateway.c",line 789,column 12,is_stmt
        MOVB      ACC,#0                ; [CPU_] |789| 
        MOVL      @_GV_Temp_counter1,ACC ; [CPU_] |789| 
	.dwpsn	file "../gateway.c",line 791,column 8,is_stmt
        B         $C$L110,UNC           ; [CPU_] |791| 
        ; branch occurs ; [] |791| 
$C$L109:    
	.dwpsn	file "../gateway.c",line 794,column 9,is_stmt
        MOVW      DP,#_EventResetTemp   ; [CPU_U] 
        MOVB      @_EventResetTemp,#1,UNC ; [CPU_] |794| 
$C$L110:    
	.dwpsn	file "../gateway.c",line 797,column 7,is_stmt
        MOVW      DP,#_ODP_Temperature_WarningMax ; [CPU_U] 
        MOV       AL,@_ODP_Temperature_WarningMax ; [CPU_] |797| 
        MOVW      DP,#_ODV_Gateway_MaxModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MaxModTemp ; [CPU_] |797| 
        B         $C$L111,LT            ; [CPU_] |797| 
        ; branchcc occurs ; [] |797| 
        MOVW      DP,#_ODP_Temperature_WarningMin ; [CPU_U] 
        MOV       AL,@_ODP_Temperature_WarningMin ; [CPU_] |797| 
        MOVW      DP,#_ODV_Gateway_MinModTemp ; [CPU_U] 
        CMP       AL,@_ODV_Gateway_MinModTemp ; [CPU_] |797| 
        B         $C$L112,LEQ           ; [CPU_] |797| 
        ; branchcc occurs ; [] |797| 
$C$L111:    
	.dwpsn	file "../gateway.c",line 799,column 9,is_stmt
        MOVL      XAR4,#262144          ; [CPU_U] |799| 
        MOVL      ACC,XAR4              ; [CPU_] |799| 
$C$DW$388	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$388, DW_AT_low_pc(0x00)
	.dwattr $C$DW$388, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$388, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |799| 
        ; call occurs [#_ERR_HandleWarning] ; [] |799| 
	.dwpsn	file "../gateway.c",line 800,column 7,is_stmt
        B         $C$L113,UNC           ; [CPU_] |800| 
        ; branch occurs ; [] |800| 
$C$L112:    
	.dwpsn	file "../gateway.c",line 801,column 12,is_stmt
        MOVL      XAR4,#262144          ; [CPU_U] |801| 
        MOVL      ACC,XAR4              ; [CPU_] |801| 
$C$DW$389	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$389, DW_AT_low_pc(0x00)
	.dwattr $C$DW$389, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$389, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |801| 
        ; call occurs [#_ERR_ClearWarning] ; [] |801| 
$C$L113:    
	.dwpsn	file "../gateway.c",line 802,column 7,is_stmt
        MOVW      DP,#_ODV_Current_DischargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_DischargeAllowed ; [CPU_] |802| 
        NEG       AL                    ; [CPU_] |802| 
        CMP       AL,*-SP[22]           ; [CPU_] |802| 
        B         $C$L114,GT            ; [CPU_] |802| 
        ; branchcc occurs ; [] |802| 
        MOVW      DP,#_ODV_Current_ChargeAllowed ; [CPU_U] 
        MOV       AL,@_ODV_Current_ChargeAllowed ; [CPU_] |802| 
        CMP       AL,*-SP[22]           ; [CPU_] |802| 
        B         $C$L115,GEQ           ; [CPU_] |802| 
        ; branchcc occurs ; [] |802| 
$C$L114:    
	.dwpsn	file "../gateway.c",line 804,column 9,is_stmt
        MOVL      XAR4,#131072          ; [CPU_U] |804| 
        MOVL      ACC,XAR4              ; [CPU_] |804| 
$C$DW$390	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$390, DW_AT_low_pc(0x00)
	.dwattr $C$DW$390, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$390, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |804| 
        ; call occurs [#_ERR_HandleWarning] ; [] |804| 
	.dwpsn	file "../gateway.c",line 809,column 7,is_stmt
        B         $C$L163,UNC           ; [CPU_] |809| 
        ; branch occurs ; [] |809| 
$C$L115:    
	.dwpsn	file "../gateway.c",line 810,column 12,is_stmt
        MOVL      XAR4,#131072          ; [CPU_U] |810| 
        MOVL      ACC,XAR4              ; [CPU_] |810| 
$C$DW$391	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$391, DW_AT_low_pc(0x00)
	.dwattr $C$DW$391, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$391, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |810| 
        ; call occurs [#_ERR_ClearWarning] ; [] |810| 
	.dwpsn	file "../gateway.c",line 813,column 5,is_stmt
        B         $C$L163,UNC           ; [CPU_] |813| 
        ; branch occurs ; [] |813| 
$C$L116:    
	.dwpsn	file "../gateway.c",line 816,column 9,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |816| 
        AND       AL,*+XAR4[0],#0x0002  ; [CPU_] |816| 
        LSR       AL,1                  ; [CPU_] |816| 
        CMPB      AL,#1                 ; [CPU_] |816| 
        BF        $C$L117,NEQ           ; [CPU_] |816| 
        ; branchcc occurs ; [] |816| 
	.dwpsn	file "../gateway.c",line 818,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOVB      @_ODV_Gateway_State,#1,UNC ; [CPU_] |818| 
	.dwpsn	file "../gateway.c",line 819,column 9,is_stmt
        B         $C$L118,UNC           ; [CPU_] |819| 
        ; branch occurs ; [] |819| 
$C$L117:    
	.dwpsn	file "../gateway.c",line 820,column 14,is_stmt
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |820| 
        TBIT      *+XAR4[0],#0          ; [CPU_] |820| 
        BF        $C$L118,TC            ; [CPU_] |820| 
        ; branchcc occurs ; [] |820| 
	.dwpsn	file "../gateway.c",line 822,column 10,is_stmt
        MOVW      DP,#_ODV_Gateway_Alive_Counter ; [CPU_U] 
        MOVB      @_ODV_Gateway_Alive_Counter,#1,UNC ; [CPU_] |822| 
	.dwpsn	file "../gateway.c",line 823,column 10,is_stmt
	.dwpsn	file "../gateway.c",line 825,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       @_ODV_Gateway_State,#0 ; [CPU_] |825| 
	.dwpsn	file "../gateway.c",line 826,column 10,is_stmt
        B         $C$L118,UNC           ; [CPU_] |826| 
        ; branch occurs ; [] |826| 
$C$L118:    
	.dwpsn	file "../gateway.c",line 836,column 10,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |836| 
        BF        $C$L165,EQ            ; [CPU_] |836| 
        ; branchcc occurs ; [] |836| 
	.dwpsn	file "../gateway.c",line 838,column 11,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |838| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |838| 
        BF        $C$L119,TC            ; [CPU_] |838| 
        ; branchcc occurs ; [] |838| 
	.dwpsn	file "../gateway.c",line 838,column 41,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#1,UNC ; [CPU_] |838| 
$C$L119:    
	.dwpsn	file "../gateway.c",line 840,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        AND       @_GpioDataRegs+1,#0xfeff ; [CPU_] |840| 
	.dwpsn	file "../gateway.c",line 841,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |841| 
        MOVL      *-SP[4],ACC           ; [CPU_] |841| 
	.dwpsn	file "../gateway.c",line 842,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |842| 
	.dwpsn	file "../gateway.c",line 843,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#6,UNC ; [CPU_] |843| 
	.dwpsn	file "../gateway.c",line 845,column 7,is_stmt
        B         $C$L165,UNC           ; [CPU_] |845| 
        ; branch occurs ; [] |845| 
$C$L120:    
	.dwpsn	file "../gateway.c",line 848,column 9,is_stmt
        MOVB      XAR6,#100             ; [CPU_] |848| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |848| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |848| 
        CMPL      ACC,XAR6              ; [CPU_] |848| 
        B         $C$L121,LO            ; [CPU_] |848| 
        ; branchcc occurs ; [] |848| 
	.dwpsn	file "../gateway.c",line 848,column 44,is_stmt
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |848| 
$C$L121:    
	.dwpsn	file "../gateway.c",line 849,column 9,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |849| 
        BF        $C$L165,EQ            ; [CPU_] |849| 
        ; branchcc occurs ; [] |849| 
	.dwpsn	file "../gateway.c",line 850,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_Voltage ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_Voltage ; [CPU_] |850| 
        B         $C$L123,LT            ; [CPU_] |850| 
        ; branchcc occurs ; [] |850| 
	.dwpsn	file "../gateway.c",line 851,column 13,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |851| 
        MOVL      *-SP[4],ACC           ; [CPU_] |851| 
	.dwpsn	file "../gateway.c",line 852,column 13,is_stmt
        MOVW      DP,#_MMSConfig        ; [CPU_U] 
        MOVL      XAR4,@_MMSConfig      ; [CPU_] |852| 
        TBIT      *+XAR4[0],#5          ; [CPU_] |852| 
        BF        $C$L122,TC            ; [CPU_] |852| 
        ; branchcc occurs ; [] |852| 
	.dwpsn	file "../gateway.c",line 854,column 15,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#3,UNC ; [CPU_] |854| 
	.dwpsn	file "../gateway.c",line 855,column 15,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#3,UNC ; [CPU_] |855| 
	.dwpsn	file "../gateway.c",line 861,column 13,is_stmt
        B         $C$L165,UNC           ; [CPU_] |861| 
        ; branch occurs ; [] |861| 
$C$L122:    
	.dwpsn	file "../gateway.c",line 863,column 15,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#4,UNC ; [CPU_] |863| 
	.dwpsn	file "../gateway.c",line 864,column 15,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |864| 
	.dwpsn	file "../gateway.c",line 866,column 11,is_stmt
        B         $C$L165,UNC           ; [CPU_] |866| 
        ; branch occurs ; [] |866| 
$C$L123:    
	.dwpsn	file "../gateway.c",line 868,column 13,is_stmt
        MOVB      ACC,#1                ; [CPU_] |868| 
$C$DW$392	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$392, DW_AT_low_pc(0x00)
	.dwattr $C$DW$392, DW_AT_name("_ERR_SetError")
	.dwattr $C$DW$392, DW_AT_TI_call
        LCR       #_ERR_SetError        ; [CPU_] |868| 
        ; call occurs [#_ERR_SetError] ; [] |868| 
	.dwpsn	file "../gateway.c",line 871,column 7,is_stmt
        B         $C$L165,UNC           ; [CPU_] |871| 
        ; branch occurs ; [] |871| 
$C$L124:    
	.dwpsn	file "../gateway.c",line 874,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |874| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |874| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |874| 
        CMPL      ACC,XAR6              ; [CPU_] |874| 
        B         $C$L125,LO            ; [CPU_] |874| 
        ; branchcc occurs ; [] |874| 
	.dwpsn	file "../gateway.c",line 874,column 43,is_stmt
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |874| 
$C$L125:    
	.dwpsn	file "../gateway.c",line 875,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |875| 
        CMP       AL,*-SP[24]           ; [CPU_] |875| 
        B         $C$L127,HI            ; [CPU_] |875| 
        ; branchcc occurs ; [] |875| 
        MOV       AL,*-SP[27]           ; [CPU_] |875| 
        BF        $C$L127,EQ            ; [CPU_] |875| 
        ; branchcc occurs ; [] |875| 
	.dwpsn	file "../gateway.c",line 877,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |877| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |877| 
        MOVL      *-SP[10],ACC          ; [CPU_] |877| 
	.dwpsn	file "../gateway.c",line 878,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |878| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |878| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |878| 
        B         $C$L128,HIS           ; [CPU_] |878| 
        ; branchcc occurs ; [] |878| 
	.dwpsn	file "../gateway.c",line 880,column 12,is_stmt
        MOVW      DP,#_ODP_CommError_OverVoltage_ErrCounter ; [CPU_U] 
        MOV       AL,@_ODP_CommError_OverVoltage_ErrCounter ; [CPU_] |880| 
        CMPB      AL,#250               ; [CPU_] |880| 
        B         $C$L126,LOS           ; [CPU_] |880| 
        ; branchcc occurs ; [] |880| 
	.dwpsn	file "../gateway.c",line 880,column 59,is_stmt
        MOVB      @_ODP_CommError_OverVoltage_ErrCounter,#255,UNC ; [CPU_] |880| 
$C$L126:    
	.dwpsn	file "../gateway.c",line 881,column 12,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |881| 
        BF        $C$L128,NEQ           ; [CPU_] |881| 
        ; branchcc occurs ; [] |881| 
	.dwpsn	file "../gateway.c",line 883,column 13,is_stmt
        MOVW      DP,#_ODP_CommError_OverVoltage_ErrCounter ; [CPU_U] 
        INC       @_ODP_CommError_OverVoltage_ErrCounter ; [CPU_] |883| 
	.dwpsn	file "../gateway.c",line 884,column 13,is_stmt
$C$DW$393	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$393, DW_AT_low_pc(0x00)
	.dwattr $C$DW$393, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$393, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |884| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |884| 
	.dwpsn	file "../gateway.c",line 899,column 9,is_stmt
        B         $C$L128,UNC           ; [CPU_] |899| 
        ; branch occurs ; [] |899| 
$C$L127:    
	.dwpsn	file "../gateway.c",line 900,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |900| 
        MOVL      *-SP[10],ACC          ; [CPU_] |900| 
$C$L128:    
	.dwpsn	file "../gateway.c",line 901,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |901| 
        CMP       AL,*-SP[22]           ; [CPU_] |901| 
        B         $C$L129,GT            ; [CPU_] |901| 
        ; branchcc occurs ; [] |901| 
        MOV       AL,*-SP[27]           ; [CPU_] |901| 
        BF        $C$L129,EQ            ; [CPU_] |901| 
        ; branchcc occurs ; [] |901| 
	.dwpsn	file "../gateway.c",line 903,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |903| 
        ADDL      ACC,*-SP[16]          ; [CPU_] |903| 
        MOVL      *-SP[16],ACC          ; [CPU_] |903| 
	.dwpsn	file "../gateway.c",line 904,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |904| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |904| 
        CMPL      ACC,*-SP[16]          ; [CPU_] |904| 
        B         $C$L130,HIS           ; [CPU_] |904| 
        ; branchcc occurs ; [] |904| 
	.dwpsn	file "../gateway.c",line 913,column 9,is_stmt
        B         $C$L130,UNC           ; [CPU_] |913| 
        ; branch occurs ; [] |913| 
$C$L129:    
	.dwpsn	file "../gateway.c",line 914,column 14,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |914| 
        BF        $C$L130,EQ            ; [CPU_] |914| 
        ; branchcc occurs ; [] |914| 
	.dwpsn	file "../gateway.c",line 914,column 40,is_stmt
        MOVB      ACC,#1                ; [CPU_] |914| 
        SUBL      *-SP[16],ACC          ; [CPU_] |914| 
$C$L130:    
	.dwpsn	file "../gateway.c",line 915,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |915| 
        CMP       AL,*-SP[24]           ; [CPU_] |915| 
        B         $C$L131,LOS           ; [CPU_] |915| 
        ; branchcc occurs ; [] |915| 
        MOV       AL,*-SP[27]           ; [CPU_] |915| 
        BF        $C$L131,EQ            ; [CPU_] |915| 
        ; branchcc occurs ; [] |915| 
	.dwpsn	file "../gateway.c",line 916,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |916| 
	.dwpsn	file "../gateway.c",line 917,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |917| 
        MOVL      *-SP[4],ACC           ; [CPU_] |917| 
	.dwpsn	file "../gateway.c",line 918,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |918| 
$C$L131:    
	.dwpsn	file "../gateway.c",line 920,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |920| 
        ANDB      AL,#0x0f              ; [CPU_] |920| 
        CMPB      AL,#1                 ; [CPU_] |920| 
        BF        $C$L132,NEQ           ; [CPU_] |920| 
        ; branchcc occurs ; [] |920| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |920| 
        CMP       AL,*-SP[24]           ; [CPU_] |920| 
        B         $C$L132,LOS           ; [CPU_] |920| 
        ; branchcc occurs ; [] |920| 
        MOV       AL,*-SP[27]           ; [CPU_] |920| 
        BF        $C$L132,EQ            ; [CPU_] |920| 
        ; branchcc occurs ; [] |920| 
	.dwpsn	file "../gateway.c",line 921,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |921| 
	.dwpsn	file "../gateway.c",line 922,column 11,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |922| 
        MOVL      ACC,XAR4              ; [CPU_] |922| 
$C$DW$394	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$394, DW_AT_low_pc(0x00)
	.dwattr $C$DW$394, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$394, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |922| 
        ; call occurs [#_ERR_ClearWarning] ; [] |922| 
	.dwpsn	file "../gateway.c",line 923,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |923| 
        MOVL      *-SP[4],ACC           ; [CPU_] |923| 
	.dwpsn	file "../gateway.c",line 924,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |924| 
$C$L132:    
	.dwpsn	file "../gateway.c",line 926,column 9,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |926| 
        BF        $C$L165,EQ            ; [CPU_] |926| 
        ; branchcc occurs ; [] |926| 
        MOVIZ     R0H,#15948            ; [CPU_] |926| 
        MOV32     R1H,*-SP[26]          ; [CPU_] |926| 
        MOVXI     R0H,#52429            ; [CPU_] |926| 
        CMPF32    R1H,R0H               ; [CPU_] |926| 
        MOVST0    ZF, NF                ; [CPU_] |926| 
        B         $C$L165,LT            ; [CPU_] |926| 
        ; branchcc occurs ; [] |926| 
	.dwpsn	file "../gateway.c",line 930,column 7,is_stmt
        B         $C$L165,UNC           ; [CPU_] |930| 
        ; branch occurs ; [] |930| 
$C$L133:    
	.dwpsn	file "../gateway.c",line 933,column 8,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |933| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |933| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |933| 
        CMPL      ACC,XAR6              ; [CPU_] |933| 
        B         $C$L134,LO            ; [CPU_] |933| 
        ; branchcc occurs ; [] |933| 
	.dwpsn	file "../gateway.c",line 933,column 42,is_stmt
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |933| 
$C$L134:    
	.dwpsn	file "../gateway.c",line 934,column 8,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       AL,@_ODV_Write_Outputs_16_Bit ; [CPU_] |934| 
        CMPB      AL,#5                 ; [CPU_] |934| 
        BF        $C$L135,EQ            ; [CPU_] |934| 
        ; branchcc occurs ; [] |934| 
	.dwpsn	file "../gateway.c",line 936,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Delay ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Resistor_Delay ; [CPU_] |936| 
        MOVW      DP,#_ODP_Settings_AUD_Safety_Low_Voltage_Delay ; [CPU_U] 
        MOV       @_ODP_Settings_AUD_Safety_Low_Voltage_Delay,AL ; [CPU_] |936| 
	.dwpsn	file "../gateway.c",line 937,column 9,is_stmt
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |937| 
	.dwpsn	file "../gateway.c",line 938,column 8,is_stmt
        B         $C$L136,UNC           ; [CPU_] |938| 
        ; branch occurs ; [] |938| 
$C$L135:    
	.dwpsn	file "../gateway.c",line 941,column 9,is_stmt
	.dwpsn	file "../gateway.c",line 942,column 9,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |942| 
$C$L136:    
	.dwpsn	file "../gateway.c",line 945,column 8,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |945| 
        MOVL      ACC,XAR4              ; [CPU_] |945| 
$C$DW$395	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$395, DW_AT_low_pc(0x00)
	.dwattr $C$DW$395, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$395, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |945| 
        ; call occurs [#_ERR_HandleWarning] ; [] |945| 
	.dwpsn	file "../gateway.c",line 947,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |947| 
        CMP       AL,*-SP[24]           ; [CPU_] |947| 
        B         $C$L138,LO            ; [CPU_] |947| 
        ; branchcc occurs ; [] |947| 
	.dwpsn	file "../gateway.c",line 949,column 10,is_stmt
        MOVB      ACC,#1                ; [CPU_] |949| 
        ADDL      ACC,*-SP[12]          ; [CPU_] |949| 
        MOVL      *-SP[12],ACC          ; [CPU_] |949| 
	.dwpsn	file "../gateway.c",line 951,column 10,is_stmt
        MOV       T,#1000               ; [CPU_] |951| 
        MOVW      DP,#_ODP_Settings_AUD_Safety_Low_Voltage_Delay ; [CPU_U] 
        MPY       ACC,T,@_ODP_Settings_AUD_Safety_Low_Voltage_Delay ; [CPU_] |951| 
        MOVU      ACC,AL                ; [CPU_] |951| 
        CMPL      ACC,*-SP[12]          ; [CPU_] |951| 
        B         $C$L139,HIS           ; [CPU_] |951| 
        ; branchcc occurs ; [] |951| 
	.dwpsn	file "../gateway.c",line 953,column 11,is_stmt
        MOVW      DP,#_ODP_CommError_LowVoltage_ErrCounter ; [CPU_U] 
        MOV       AL,@_ODP_CommError_LowVoltage_ErrCounter ; [CPU_] |953| 
        CMPB      AL,#250               ; [CPU_] |953| 
        B         $C$L137,LOS           ; [CPU_] |953| 
        ; branchcc occurs ; [] |953| 
	.dwpsn	file "../gateway.c",line 953,column 57,is_stmt
        MOVB      @_ODP_CommError_LowVoltage_ErrCounter,#255,UNC ; [CPU_] |953| 
$C$L137:    
	.dwpsn	file "../gateway.c",line 954,column 11,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |954| 
        BF        $C$L139,NEQ           ; [CPU_] |954| 
        ; branchcc occurs ; [] |954| 
        MOV       AL,*-SP[27]           ; [CPU_] |954| 
        BF        $C$L139,EQ            ; [CPU_] |954| 
        ; branchcc occurs ; [] |954| 
	.dwpsn	file "../gateway.c",line 956,column 12,is_stmt
$C$DW$396	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$396, DW_AT_low_pc(0x00)
	.dwattr $C$DW$396, DW_AT_name("_ERR_ErrorUnderVoltage")
	.dwattr $C$DW$396, DW_AT_TI_call
        LCR       #_ERR_ErrorUnderVoltage ; [CPU_] |956| 
        ; call occurs [#_ERR_ErrorUnderVoltage] ; [] |956| 
	.dwpsn	file "../gateway.c",line 957,column 12,is_stmt
        MOVW      DP,#_ODP_CommError_LowVoltage_ErrCounter ; [CPU_U] 
        INC       @_ODP_CommError_LowVoltage_ErrCounter ; [CPU_] |957| 
	.dwpsn	file "../gateway.c",line 960,column 9,is_stmt
        B         $C$L139,UNC           ; [CPU_] |960| 
        ; branch occurs ; [] |960| 
$C$L138:    
	.dwpsn	file "../gateway.c",line 961,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |961| 
        MOVL      *-SP[12],ACC          ; [CPU_] |961| 
$C$L139:    
	.dwpsn	file "../gateway.c",line 963,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_OverVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_OverVoltage ; [CPU_] |963| 
        CMP       AL,*-SP[24]           ; [CPU_] |963| 
        B         $C$L142,HI            ; [CPU_] |963| 
        ; branchcc occurs ; [] |963| 
	.dwpsn	file "../gateway.c",line 965,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |965| 
        ADDL      ACC,*-SP[10]          ; [CPU_] |965| 
        MOVL      *-SP[10],ACC          ; [CPU_] |965| 
	.dwpsn	file "../gateway.c",line 966,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |966| 
        MOVW      DP,#_ODP_SafetyLimits_Voltage_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Voltage_delay ; [CPU_] |966| 
        CMPL      ACC,*-SP[10]          ; [CPU_] |966| 
        B         $C$L143,HIS           ; [CPU_] |966| 
        ; branchcc occurs ; [] |966| 
	.dwpsn	file "../gateway.c",line 968,column 12,is_stmt
        MOVW      DP,#_ODP_CommError_OverVoltage_ErrCounter ; [CPU_U] 
        MOV       AL,@_ODP_CommError_OverVoltage_ErrCounter ; [CPU_] |968| 
        CMPB      AL,#250               ; [CPU_] |968| 
        B         $C$L140,LOS           ; [CPU_] |968| 
        ; branchcc occurs ; [] |968| 
	.dwpsn	file "../gateway.c",line 968,column 59,is_stmt
        MOVB      @_ODP_CommError_OverVoltage_ErrCounter,#255,UNC ; [CPU_] |968| 
$C$L140:    
	.dwpsn	file "../gateway.c",line 969,column 12,is_stmt
        MOVW      DP,#_ODV_ErrorDsp_ErrorNumber ; [CPU_U] 
        MOVL      ACC,@_ODV_ErrorDsp_ErrorNumber ; [CPU_] |969| 
        BF        $C$L141,NEQ           ; [CPU_] |969| 
        ; branchcc occurs ; [] |969| 
	.dwpsn	file "../gateway.c",line 971,column 16,is_stmt
        MOVW      DP,#_ODP_CommError_OverVoltage_ErrCounter ; [CPU_U] 
        INC       @_ODP_CommError_OverVoltage_ErrCounter ; [CPU_] |971| 
	.dwpsn	file "../gateway.c",line 972,column 16,is_stmt
$C$DW$397	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$397, DW_AT_low_pc(0x00)
	.dwattr $C$DW$397, DW_AT_name("_ERR_ErrorOverVoltage")
	.dwattr $C$DW$397, DW_AT_TI_call
        LCR       #_ERR_ErrorOverVoltage ; [CPU_] |972| 
        ; call occurs [#_ERR_ErrorOverVoltage] ; [] |972| 
$C$L141:    
	.dwpsn	file "../gateway.c",line 975,column 15,is_stmt
        MOVW      DP,#_ODP_Settings_AUD_Gateway_Current_Ringsaver ; [CPU_U] 
        MOV       AL,@_ODP_Settings_AUD_Gateway_Current_Ringsaver ; [CPU_] |975| 
        CMP       AL,*-SP[22]           ; [CPU_] |975| 
        B         $C$L143,LOS           ; [CPU_] |975| 
        ; branchcc occurs ; [] |975| 
	.dwpsn	file "../gateway.c",line 977,column 16,is_stmt
        MOVW      DP,#_ODP_Gateway_Relay_Error_Count ; [CPU_U] 
        INC       @_ODP_Gateway_Relay_Error_Count ; [CPU_] |977| 
	.dwpsn	file "../gateway.c",line 978,column 16,is_stmt
        MOV       AL,#-5                ; [CPU_] |978| 
$C$DW$398	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$398, DW_AT_low_pc(0x00)
	.dwattr $C$DW$398, DW_AT_name("_PAR_WriteStatisticParam")
	.dwattr $C$DW$398, DW_AT_TI_call
        LCR       #_PAR_WriteStatisticParam ; [CPU_] |978| 
        ; call occurs [#_PAR_WriteStatisticParam] ; [] |978| 
	.dwpsn	file "../gateway.c",line 981,column 9,is_stmt
        B         $C$L143,UNC           ; [CPU_] |981| 
        ; branch occurs ; [] |981| 
$C$L142:    
	.dwpsn	file "../gateway.c",line 982,column 14,is_stmt
        MOVB      ACC,#0                ; [CPU_] |982| 
        MOVL      *-SP[10],ACC          ; [CPU_] |982| 
$C$L143:    
	.dwpsn	file "../gateway.c",line 984,column 9,is_stmt
        MOVW      DP,#_ODP_Current_C_D_Mode ; [CPU_U] 
        MOV       AL,@_ODP_Current_C_D_Mode ; [CPU_] |984| 
        NEG       AL                    ; [CPU_] |984| 
        CMP       AL,*-SP[22]           ; [CPU_] |984| 
        B         $C$L144,GEQ           ; [CPU_] |984| 
        ; branchcc occurs ; [] |984| 
        MOVW      DP,#_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Charge_In_Thres_Cur ; [CPU_] |984| 
        NEG       AL                    ; [CPU_] |984| 
        CMP       AL,*-SP[22]           ; [CPU_] |984| 
        B         $C$L145,LT            ; [CPU_] |984| 
        ; branchcc occurs ; [] |984| 
        MOV       AL,*-SP[27]           ; [CPU_] |984| 
        BF        $C$L145,EQ            ; [CPU_] |984| 
        ; branchcc occurs ; [] |984| 
$C$L144:    
	.dwpsn	file "../gateway.c",line 986,column 11,is_stmt
        MOVB      ACC,#1                ; [CPU_] |986| 
        ADDL      ACC,*-SP[16]          ; [CPU_] |986| 
        MOVL      *-SP[16],ACC          ; [CPU_] |986| 
	.dwpsn	file "../gateway.c",line 987,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |987| 
        MOVW      DP,#_ODP_SafetyLimits_Current_delay ; [CPU_U] 
        MPYXU     ACC,T,@_ODP_SafetyLimits_Current_delay ; [CPU_] |987| 
        CMPL      ACC,*-SP[16]          ; [CPU_] |987| 
        B         $C$L146,HIS           ; [CPU_] |987| 
        ; branchcc occurs ; [] |987| 
	.dwpsn	file "../gateway.c",line 991,column 9,is_stmt
        B         $C$L146,UNC           ; [CPU_] |991| 
        ; branch occurs ; [] |991| 
$C$L145:    
	.dwpsn	file "../gateway.c",line 992,column 14,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |992| 
        BF        $C$L146,EQ            ; [CPU_] |992| 
        ; branchcc occurs ; [] |992| 
	.dwpsn	file "../gateway.c",line 992,column 40,is_stmt
        MOVB      ACC,#1                ; [CPU_] |992| 
        SUBL      *-SP[16],ACC          ; [CPU_] |992| 
$C$L146:    
	.dwpsn	file "../gateway.c",line 994,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |994| 
        ANDB      AL,#0x0f              ; [CPU_] |994| 
        CMPB      AL,#1                 ; [CPU_] |994| 
        BF        $C$L165,NEQ           ; [CPU_] |994| 
        ; branchcc occurs ; [] |994| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |994| 
        CMP       AL,*-SP[24]           ; [CPU_] |994| 
        B         $C$L165,HIS           ; [CPU_] |994| 
        ; branchcc occurs ; [] |994| 
        MOV       AL,*-SP[27]           ; [CPU_] |994| 
        BF        $C$L165,EQ            ; [CPU_] |994| 
        ; branchcc occurs ; [] |994| 
	.dwpsn	file "../gateway.c",line 995,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#1,UNC ; [CPU_] |995| 
	.dwpsn	file "../gateway.c",line 996,column 11,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |996| 
        MOVL      ACC,XAR4              ; [CPU_] |996| 
$C$DW$399	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$399, DW_AT_low_pc(0x00)
	.dwattr $C$DW$399, DW_AT_name("_ERR_ClearWarning")
	.dwattr $C$DW$399, DW_AT_TI_call
        LCR       #_ERR_ClearWarning    ; [CPU_] |996| 
        ; call occurs [#_ERR_ClearWarning] ; [] |996| 
	.dwpsn	file "../gateway.c",line 997,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |997| 
        MOVL      *-SP[4],ACC           ; [CPU_] |997| 
	.dwpsn	file "../gateway.c",line 998,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |998| 
	.dwpsn	file "../gateway.c",line 1000,column 7,is_stmt
        B         $C$L165,UNC           ; [CPU_] |1000| 
        ; branch occurs ; [] |1000| 
$C$L147:    
	.dwpsn	file "../gateway.c",line 1004,column 6,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       AL,@_ODV_Write_Outputs_16_Bit ; [CPU_] |1004| 
        CMPB      AL,#5                 ; [CPU_] |1004| 
        BF        $C$L148,EQ            ; [CPU_] |1004| 
        ; branchcc occurs ; [] |1004| 
	.dwpsn	file "../gateway.c",line 1004,column 28,is_stmt
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |1004| 
$C$L148:    
	.dwpsn	file "../gateway.c",line 1006,column 9,is_stmt
        MOVB      XAR6,#50              ; [CPU_] |1006| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1006| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |1006| 
        CMPL      ACC,XAR6              ; [CPU_] |1006| 
        B         $C$L149,LO            ; [CPU_] |1006| 
        ; branchcc occurs ; [] |1006| 
	.dwpsn	file "../gateway.c",line 1006,column 43,is_stmt
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |1006| 
$C$L149:    
	.dwpsn	file "../gateway.c",line 1007,column 9,is_stmt
        MOVL      ACC,*-SP[10]          ; [CPU_] |1007| 
        BF        $C$L150,EQ            ; [CPU_] |1007| 
        ; branchcc occurs ; [] |1007| 
	.dwpsn	file "../gateway.c",line 1007,column 34,is_stmt
        MOVB      ACC,#1                ; [CPU_] |1007| 
        SUBL      *-SP[10],ACC          ; [CPU_] |1007| 
$C$L150:    
	.dwpsn	file "../gateway.c",line 1008,column 9,is_stmt
        MOVL      ACC,*-SP[16]          ; [CPU_] |1008| 
        BF        $C$L151,EQ            ; [CPU_] |1008| 
        ; branchcc occurs ; [] |1008| 
	.dwpsn	file "../gateway.c",line 1008,column 35,is_stmt
        MOVB      ACC,#1                ; [CPU_] |1008| 
        SUBL      *-SP[16],ACC          ; [CPU_] |1008| 
$C$L151:    
	.dwpsn	file "../gateway.c",line 1009,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |1009| 
        ANDB      AL,#0x0f              ; [CPU_] |1009| 
        CMPB      AL,#2                 ; [CPU_] |1009| 
        BF        $C$L152,EQ            ; [CPU_] |1009| 
        ; branchcc occurs ; [] |1009| 
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |1009| 
        CMP       AL,*-SP[24]           ; [CPU_] |1009| 
        B         $C$L153,LO            ; [CPU_] |1009| 
        ; branchcc occurs ; [] |1009| 
        MOV       AL,*-SP[27]           ; [CPU_] |1009| 
        BF        $C$L153,EQ            ; [CPU_] |1009| 
        ; branchcc occurs ; [] |1009| 
$C$L152:    
	.dwpsn	file "../gateway.c",line 1010,column 11,is_stmt
        MOVL      XAR4,#1048576         ; [CPU_U] |1010| 
        MOVL      ACC,XAR4              ; [CPU_] |1010| 
$C$DW$400	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$400, DW_AT_low_pc(0x00)
	.dwattr $C$DW$400, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$400, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |1010| 
        ; call occurs [#_ERR_HandleWarning] ; [] |1010| 
	.dwpsn	file "../gateway.c",line 1011,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |1011| 
	.dwpsn	file "../gateway.c",line 1013,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1013| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1013| 
	.dwpsn	file "../gateway.c",line 1014,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |1014| 
$C$L153:    
	.dwpsn	file "../gateway.c",line 1016,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |1016| 
        CMP       AL,*-SP[24]           ; [CPU_] |1016| 
        B         $C$L154,LOS           ; [CPU_] |1016| 
        ; branchcc occurs ; [] |1016| 
	.dwpsn	file "../gateway.c",line 1017,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |1017| 
	.dwpsn	file "../gateway.c",line 1018,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1018| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1018| 
	.dwpsn	file "../gateway.c",line 1019,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |1019| 
$C$L154:    
	.dwpsn	file "../gateway.c",line 1021,column 9,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |1021| 
        ANDB      AL,#0x0f              ; [CPU_] |1021| 
        CMPB      AL,#4                 ; [CPU_] |1021| 
        BF        $C$L155,EQ            ; [CPU_] |1021| 
        ; branchcc occurs ; [] |1021| 
        MOVW      DP,#_ODP_SafetyLimits_Umax ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_Umax ; [CPU_] |1021| 
        CMP       AL,*-SP[24]           ; [CPU_] |1021| 
        B         $C$L165,HI            ; [CPU_] |1021| 
        ; branchcc occurs ; [] |1021| 
        MOV       AL,*-SP[27]           ; [CPU_] |1021| 
        BF        $C$L165,EQ            ; [CPU_] |1021| 
        ; branchcc occurs ; [] |1021| 
$C$L155:    
	.dwpsn	file "../gateway.c",line 1022,column 11,is_stmt
        MOVL      XAR4,#2097152         ; [CPU_U] |1022| 
        MOVL      ACC,XAR4              ; [CPU_] |1022| 
$C$DW$401	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$401, DW_AT_low_pc(0x00)
	.dwattr $C$DW$401, DW_AT_name("_ERR_HandleWarning")
	.dwattr $C$DW$401, DW_AT_TI_call
        LCR       #_ERR_HandleWarning   ; [CPU_] |1022| 
        ; call occurs [#_ERR_HandleWarning] ; [] |1022| 
	.dwpsn	file "../gateway.c",line 1023,column 11,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#4,UNC ; [CPU_] |1023| 
	.dwpsn	file "../gateway.c",line 1025,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1025| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1025| 
	.dwpsn	file "../gateway.c",line 1026,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |1026| 
	.dwpsn	file "../gateway.c",line 1028,column 7,is_stmt
        B         $C$L165,UNC           ; [CPU_] |1028| 
        ; branch occurs ; [] |1028| 
$C$L156:    
	.dwpsn	file "../gateway.c",line 1031,column 9,is_stmt
        MOVW      DP,#_ODP_SafetyLimits_UnderVoltage ; [CPU_U] 
        MOV       AL,@_ODP_SafetyLimits_UnderVoltage ; [CPU_] |1031| 
        CMP       AL,*-SP[24]           ; [CPU_] |1031| 
        B         $C$L157,HI            ; [CPU_] |1031| 
        ; branchcc occurs ; [] |1031| 
	.dwpsn	file "../gateway.c",line 1033,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOVB      @_ODV_Write_Outputs_16_Bit,#5,UNC ; [CPU_] |1033| 
	.dwpsn	file "../gateway.c",line 1034,column 11,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1034| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1034| 
	.dwpsn	file "../gateway.c",line 1035,column 11,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |1035| 
	.dwpsn	file "../gateway.c",line 1036,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |1036| 
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        ANDB      AL,#0x0f              ; [CPU_] |1036| 
        MOV       @_ODV_MachineMode,AL  ; [CPU_] |1036| 
	.dwpsn	file "../gateway.c",line 1037,column 9,is_stmt
        B         $C$L165,UNC           ; [CPU_] |1037| 
        ; branch occurs ; [] |1037| 
$C$L157:    
	.dwpsn	file "../gateway.c",line 1041,column 10,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#2,UNC ; [CPU_] |1041| 
	.dwpsn	file "../gateway.c",line 1042,column 10,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1042| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1042| 
	.dwpsn	file "../gateway.c",line 1044,column 11,is_stmt
        MOV       T,#1000               ; [CPU_] |1044| 
        MOVW      DP,#_ODP_SafetyLimits_Resistor_Delay ; [CPU_U] 
        MPYXU     P,T,@_ODP_SafetyLimits_Resistor_Delay ; [CPU_] |1044| 
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1044| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |1044| 
        CMPL      ACC,P                 ; [CPU_] |1044| 
        B         $C$L165,LO            ; [CPU_] |1044| 
        ; branchcc occurs ; [] |1044| 
	.dwpsn	file "../gateway.c",line 1068,column 7,is_stmt
        B         $C$L165,UNC           ; [CPU_] |1068| 
        ; branch occurs ; [] |1068| 
$C$L158:    
	.dwpsn	file "../gateway.c",line 1071,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1071| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |1071| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |1071| 
        CMPL      ACC,XAR6              ; [CPU_] |1071| 
        B         $C$L165,LO            ; [CPU_] |1071| 
        ; branchcc occurs ; [] |1071| 
	.dwpsn	file "../gateway.c",line 1072,column 11,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |1072| 
	.dwpsn	file "../gateway.c",line 1073,column 11,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |1073| 
	.dwpsn	file "../gateway.c",line 1076,column 9,is_stmt
        B         $C$L165,UNC           ; [CPU_] |1076| 
        ; branch occurs ; [] |1076| 
$C$L159:    
	.dwpsn	file "../gateway.c",line 1079,column 9,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1079| 
        MOVL      *-SP[4],ACC           ; [CPU_] |1079| 
	.dwpsn	file "../gateway.c",line 1080,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOVB      @_ODV_MachineMode,#128,UNC ; [CPU_] |1080| 
	.dwpsn	file "../gateway.c",line 1081,column 9,is_stmt
        MOV       *-SP[27],#0           ; [CPU_] |1081| 
$C$L160:    
	.dwpsn	file "../gateway.c",line 1084,column 6,is_stmt
        MOVW      DP,#_ODV_SysTick_ms   ; [CPU_U] 
        MOVL      ACC,@_ODV_SysTick_ms  ; [CPU_] |1084| 
        MOVW      DP,#_ODP_CommError_Delay ; [CPU_U] 
        MOVZ      AR6,@_ODP_CommError_Delay ; [CPU_] |1084| 
        SUBL      ACC,*-SP[4]           ; [CPU_] |1084| 
        CMPL      ACC,XAR6              ; [CPU_] |1084| 
        B         $C$L161,LO            ; [CPU_] |1084| 
        ; branchcc occurs ; [] |1084| 
	.dwpsn	file "../gateway.c",line 1086,column 7,is_stmt
        MOV       AL,*-SP[27]           ; [CPU_] |1086| 
        BF        $C$L161,NEQ           ; [CPU_] |1086| 
        ; branchcc occurs ; [] |1086| 
	.dwpsn	file "../gateway.c",line 1088,column 8,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |1088| 
	.dwpsn	file "../gateway.c",line 1089,column 8,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |1089| 
	.dwpsn	file "../gateway.c",line 1091,column 8,is_stmt
        MOVB      *-SP[27],#1,UNC       ; [CPU_] |1091| 
$C$L161:    
	.dwpsn	file "../gateway.c",line 1107,column 9,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |1107| 
        B         $C$L162,LEQ           ; [CPU_] |1107| 
        ; branchcc occurs ; [] |1107| 
        CMPB      AL,#4                 ; [CPU_] |1107| 
        B         $C$L162,GT            ; [CPU_] |1107| 
        ; branchcc occurs ; [] |1107| 
	.dwpsn	file "../gateway.c",line 1108,column 11,is_stmt
        MOVW      DP,#_ODV_Gateway_State ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_State ; [CPU_] |1108| 
        BF        $C$L162,NEQ           ; [CPU_] |1108| 
        ; branchcc occurs ; [] |1108| 
	.dwpsn	file "../gateway.c",line 1110,column 12,is_stmt
        MOVL      ACC,*-SP[20]          ; [CPU_] |1110| 
        BF        $C$L162,NEQ           ; [CPU_] |1110| 
        ; branchcc occurs ; [] |1110| 
	.dwpsn	file "../gateway.c",line 1112,column 13,is_stmt
        MOVW      DP,#_ODV_Write_Outputs_16_Bit ; [CPU_U] 
        MOV       @_ODV_Write_Outputs_16_Bit,#0 ; [CPU_] |1112| 
	.dwpsn	file "../gateway.c",line 1113,column 13,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       @_ODV_MachineMode,#0  ; [CPU_] |1113| 
	.dwpsn	file "../gateway.c",line 1114,column 13,is_stmt
        MOVW      DP,#_GpioDataRegs+1   ; [CPU_U] 
        OR        @_GpioDataRegs+1,#0x0100 ; [CPU_] |1114| 
$C$L162:    
	.dwpsn	file "../gateway.c",line 1118,column 9,is_stmt
        MOVB      ACC,#1                ; [CPU_] |1118| 
        ADDL      ACC,*-SP[20]          ; [CPU_] |1118| 
        MOVL      *-SP[20],ACC          ; [CPU_] |1118| 
	.dwpsn	file "../gateway.c",line 1120,column 7,is_stmt
        B         $C$L165,UNC           ; [CPU_] |1120| 
        ; branch occurs ; [] |1120| 
$C$L163:    
	.dwpsn	file "../gateway.c",line 813,column 5,is_stmt
        MOVW      DP,#_ODV_MachineMode  ; [CPU_U] 
        MOV       AL,@_ODV_MachineMode  ; [CPU_] |813| 
        CMPB      AL,#4                 ; [CPU_] |813| 
        B         $C$L164,GT            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#4                 ; [CPU_] |813| 
        BF        $C$L124,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#0                 ; [CPU_] |813| 
        BF        $C$L116,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#1                 ; [CPU_] |813| 
        BF        $C$L147,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#2                 ; [CPU_] |813| 
        BF        $C$L133,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#3                 ; [CPU_] |813| 
        BF        $C$L156,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        B         $C$L160,UNC           ; [CPU_] |813| 
        ; branch occurs ; [] |813| 
$C$L164:    
        CMPB      AL,#6                 ; [CPU_] |813| 
        BF        $C$L120,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#8                 ; [CPU_] |813| 
        BF        $C$L158,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#64                ; [CPU_] |813| 
        BF        $C$L159,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        CMPB      AL,#128               ; [CPU_] |813| 
        BF        $C$L160,EQ            ; [CPU_] |813| 
        ; branchcc occurs ; [] |813| 
        B         $C$L160,UNC           ; [CPU_] |813| 
        ; branch occurs ; [] |813| 
$C$L165:    
	.dwpsn	file "../gateway.c",line 1122,column 5,is_stmt
        MOVL      XAR4,#_TSK_timerSem   ; [CPU_U] |1122| 
        MOVB      AL,#1                 ; [CPU_] |1122| 
$C$DW$402	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$402, DW_AT_low_pc(0x00)
	.dwattr $C$DW$402, DW_AT_name("_SEM_pend")
	.dwattr $C$DW$402, DW_AT_TI_call
        LCR       #_SEM_pend            ; [CPU_] |1122| 
        ; call occurs [#_SEM_pend] ; [] |1122| 
	.dwpsn	file "../gateway.c",line 550,column 10,is_stmt
        B         $C$L63,UNC            ; [CPU_] |550| 
        ; branch occurs ; [] |550| 
	.dwattr $C$DW$341, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$341, DW_AT_TI_end_line(0x464)
	.dwattr $C$DW$341, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$341

	.sect	".text"
	.clink
	.global	_SecurityCallBack

$C$DW$403	.dwtag  DW_TAG_subprogram, DW_AT_name("SecurityCallBack")
	.dwattr $C$DW$403, DW_AT_low_pc(_SecurityCallBack)
	.dwattr $C$DW$403, DW_AT_high_pc(0x00)
	.dwattr $C$DW$403, DW_AT_TI_symbol_name("_SecurityCallBack")
	.dwattr $C$DW$403, DW_AT_external
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$403, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$403, DW_AT_TI_begin_line(0x468)
	.dwattr $C$DW$403, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$403, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1128,column 95,is_stmt,address _SecurityCallBack

	.dwfde $C$DW$CIE, _SecurityCallBack
$C$DW$404	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$404, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$404, DW_AT_location[DW_OP_reg12]
$C$DW$405	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$405, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$405, DW_AT_location[DW_OP_reg14]
$C$DW$406	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$406, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$406, DW_AT_location[DW_OP_reg0]
$C$DW$407	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$407, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$407, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SecurityCallBack             FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SecurityCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$408	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$408, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$408, DW_AT_location[DW_OP_breg20 -2]
$C$DW$409	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$409, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$409, DW_AT_location[DW_OP_breg20 -4]
$C$DW$410	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$410, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$410, DW_AT_location[DW_OP_breg20 -5]
$C$DW$411	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$411, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$411, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1128| 
        MOV       *-SP[5],AL            ; [CPU_] |1128| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1128| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1128| 
	.dwpsn	file "../gateway.c",line 1129,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1129| 
        BF        $C$L166,NEQ           ; [CPU_] |1129| 
        ; branchcc occurs ; [] |1129| 
	.dwpsn	file "../gateway.c",line 1130,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |1130| 
        BF        $C$L166,NEQ           ; [CPU_] |1130| 
        ; branchcc occurs ; [] |1130| 
	.dwpsn	file "../gateway.c",line 1130,column 28,is_stmt
$C$DW$412	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$412, DW_AT_low_pc(0x00)
	.dwattr $C$DW$412, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$412, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |1130| 
        ; call occurs [#_HAL_Random] ; [] |1130| 
$C$L166:    
	.dwpsn	file "../gateway.c",line 1132,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1132| 
        CMPB      AL,#1                 ; [CPU_] |1132| 
        BF        $C$L168,NEQ           ; [CPU_] |1132| 
        ; branchcc occurs ; [] |1132| 
	.dwpsn	file "../gateway.c",line 1133,column 5,is_stmt
        MOVW      DP,#_ODP_RandomNB     ; [CPU_U] 
        MOVL      ACC,@_ODP_RandomNB    ; [CPU_] |1133| 
        BF        $C$L167,NEQ           ; [CPU_] |1133| 
        ; branchcc occurs ; [] |1133| 
	.dwpsn	file "../gateway.c",line 1133,column 28,is_stmt
$C$DW$413	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$413, DW_AT_low_pc(0x00)
	.dwattr $C$DW$413, DW_AT_name("_HAL_Random")
	.dwattr $C$DW$413, DW_AT_TI_call
        LCR       #_HAL_Random          ; [CPU_] |1133| 
        ; call occurs [#_HAL_Random] ; [] |1133| 
        B         $C$L168,UNC           ; [CPU_] |1133| 
        ; branch occurs ; [] |1133| 
$C$L167:    
	.dwpsn	file "../gateway.c",line 1134,column 10,is_stmt
$C$DW$414	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$414, DW_AT_low_pc(0x00)
	.dwattr $C$DW$414, DW_AT_name("_HAL_Unlock")
	.dwattr $C$DW$414, DW_AT_TI_call
        LCR       #_HAL_Unlock          ; [CPU_] |1134| 
        ; call occurs [#_HAL_Unlock] ; [] |1134| 
$C$L168:    
	.dwpsn	file "../gateway.c",line 1136,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1136| 
	.dwpsn	file "../gateway.c",line 1137,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$415	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$415, DW_AT_low_pc(0x00)
	.dwattr $C$DW$415, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$403, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$403, DW_AT_TI_end_line(0x471)
	.dwattr $C$DW$403, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$403

	.sect	".text"
	.clink
	.global	_LogNBCallback

$C$DW$416	.dwtag  DW_TAG_subprogram, DW_AT_name("LogNBCallback")
	.dwattr $C$DW$416, DW_AT_low_pc(_LogNBCallback)
	.dwattr $C$DW$416, DW_AT_high_pc(0x00)
	.dwattr $C$DW$416, DW_AT_TI_symbol_name("_LogNBCallback")
	.dwattr $C$DW$416, DW_AT_external
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$416, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$416, DW_AT_TI_begin_line(0x474)
	.dwattr $C$DW$416, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$416, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1140,column 92,is_stmt,address _LogNBCallback

	.dwfde $C$DW$CIE, _LogNBCallback
$C$DW$417	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$417, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$417, DW_AT_location[DW_OP_reg12]
$C$DW$418	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$418, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$418, DW_AT_location[DW_OP_reg14]
$C$DW$419	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$419, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$419, DW_AT_location[DW_OP_reg0]
$C$DW$420	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$420, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$420, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LogNBCallback                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LogNBCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$421	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$421, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$421, DW_AT_location[DW_OP_breg20 -2]
$C$DW$422	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$422, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$422, DW_AT_location[DW_OP_breg20 -4]
$C$DW$423	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$423, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$423, DW_AT_location[DW_OP_breg20 -5]
$C$DW$424	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$424, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$424, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1140| 
        MOV       *-SP[5],AL            ; [CPU_] |1140| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1140| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1140| 
	.dwpsn	file "../gateway.c",line 1142,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1142| 
        CMPB      AL,#1                 ; [CPU_] |1142| 
        BF        $C$L169,NEQ           ; [CPU_] |1142| 
        ; branchcc occurs ; [] |1142| 
	.dwpsn	file "../gateway.c",line 1143,column 5,is_stmt
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       AL,@_ODV_Gateway_LogNB ; [CPU_] |1143| 
$C$DW$425	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$425, DW_AT_low_pc(0x00)
	.dwattr $C$DW$425, DW_AT_name("_PAR_GetLogNB")
	.dwattr $C$DW$425, DW_AT_TI_call
        LCR       #_PAR_GetLogNB        ; [CPU_] |1143| 
        ; call occurs [#_PAR_GetLogNB] ; [] |1143| 
	.dwpsn	file "../gateway.c",line 1144,column 3,is_stmt
        B         $C$L170,UNC           ; [CPU_] |1144| 
        ; branch occurs ; [] |1144| 
$C$L169:    
	.dwpsn	file "../gateway.c",line 1146,column 5,is_stmt
        MOVW      DP,#_TimeLogIndex     ; [CPU_U] 
        MOV       AL,@_TimeLogIndex     ; [CPU_] |1146| 
        MOVW      DP,#_ODV_Gateway_LogNB ; [CPU_U] 
        MOV       @_ODV_Gateway_LogNB,AL ; [CPU_] |1146| 
$C$L170:    
	.dwpsn	file "../gateway.c",line 1148,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1148| 
	.dwpsn	file "../gateway.c",line 1149,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$426	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$426, DW_AT_low_pc(0x00)
	.dwattr $C$DW$426, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$416, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$416, DW_AT_TI_end_line(0x47d)
	.dwattr $C$DW$416, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$416

	.sect	".text"
	.clink
	.global	_BatteryCallBack

$C$DW$427	.dwtag  DW_TAG_subprogram, DW_AT_name("BatteryCallBack")
	.dwattr $C$DW$427, DW_AT_low_pc(_BatteryCallBack)
	.dwattr $C$DW$427, DW_AT_high_pc(0x00)
	.dwattr $C$DW$427, DW_AT_TI_symbol_name("_BatteryCallBack")
	.dwattr $C$DW$427, DW_AT_external
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$427, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$427, DW_AT_TI_begin_line(0x47f)
	.dwattr $C$DW$427, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$427, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1151,column 94,is_stmt,address _BatteryCallBack

	.dwfde $C$DW$CIE, _BatteryCallBack
$C$DW$428	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$428, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$428, DW_AT_location[DW_OP_reg12]
$C$DW$429	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$429, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$429, DW_AT_location[DW_OP_reg14]
$C$DW$430	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$430, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$430, DW_AT_location[DW_OP_reg0]
$C$DW$431	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$431, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$431, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _BatteryCallBack              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_BatteryCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$432	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$432, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$432, DW_AT_location[DW_OP_breg20 -2]
$C$DW$433	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$433, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$433, DW_AT_location[DW_OP_breg20 -4]
$C$DW$434	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$434, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$434, DW_AT_location[DW_OP_breg20 -5]
$C$DW$435	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$435, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$435, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1151| 
        MOV       *-SP[5],AL            ; [CPU_] |1151| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1151| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1151| 
	.dwpsn	file "../gateway.c",line 1152,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1152| 
        CMPB      AL,#1                 ; [CPU_] |1152| 
        BF        $C$L171,NEQ           ; [CPU_] |1152| 
        ; branchcc occurs ; [] |1152| 
	.dwpsn	file "../gateway.c",line 1153,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |1153| 
        CMPB      AL,#1                 ; [CPU_] |1153| 
        BF        $C$L171,NEQ           ; [CPU_] |1153| 
        ; branchcc occurs ; [] |1153| 
	.dwpsn	file "../gateway.c",line 1154,column 7,is_stmt
        MOVW      DP,#_ODP_Battery_Capacity ; [CPU_U] 
        MOV32     R0H,@_ODP_Battery_Capacity ; [CPU_] |1154| 
        MPYF32    R0H,R0H,#17761        ; [CPU_] |1154| 
        NOP       ; [CPU_] 
        MPYF32    R0H,R0H,#16288        ; [CPU_] |1154| 
$C$DW$436	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$436, DW_AT_low_pc(0x00)
	.dwattr $C$DW$436, DW_AT_name("_CNV_Round")
	.dwattr $C$DW$436, DW_AT_TI_call
        LCR       #_CNV_Round           ; [CPU_] |1154| 
        ; call occurs [#_CNV_Round] ; [] |1154| 
        MOVW      DP,#_PAR_Capacity_Total ; [CPU_U] 
        MOVL      @_PAR_Capacity_Total,ACC ; [CPU_] |1154| 
	.dwpsn	file "../gateway.c",line 1156,column 7,is_stmt
        MOVW      DP,#_PAR_Capacity_TotalLife_Used ; [CPU_U] 
        ZAPA      ; [CPU_] |1156| 
        MOVL      @_PAR_Capacity_TotalLife_Used,P ; [CPU_] |1156| 
        MOVL      @_PAR_Capacity_TotalLife_Used+2,ACC ; [CPU_] |1156| 
$C$L171:    
	.dwpsn	file "../gateway.c",line 1159,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1159| 
	.dwpsn	file "../gateway.c",line 1160,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$437	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$437, DW_AT_low_pc(0x00)
	.dwattr $C$DW$437, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$427, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$427, DW_AT_TI_end_line(0x488)
	.dwattr $C$DW$427, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$427

	.sect	".text"
	.clink
	.global	_WriteTextCallback

$C$DW$438	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteTextCallback")
	.dwattr $C$DW$438, DW_AT_low_pc(_WriteTextCallback)
	.dwattr $C$DW$438, DW_AT_high_pc(0x00)
	.dwattr $C$DW$438, DW_AT_TI_symbol_name("_WriteTextCallback")
	.dwattr $C$DW$438, DW_AT_external
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$438, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$438, DW_AT_TI_begin_line(0x48a)
	.dwattr $C$DW$438, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$438, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1162,column 96,is_stmt,address _WriteTextCallback

	.dwfde $C$DW$CIE, _WriteTextCallback
$C$DW$439	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$439, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$439, DW_AT_location[DW_OP_reg12]
$C$DW$440	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$440, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$440, DW_AT_location[DW_OP_reg14]
$C$DW$441	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$441, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$441, DW_AT_location[DW_OP_reg0]
$C$DW$442	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$442, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$442, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteTextCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteTextCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$443	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$443, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$443, DW_AT_location[DW_OP_breg20 -2]
$C$DW$444	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$444, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$444, DW_AT_location[DW_OP_breg20 -4]
$C$DW$445	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$445, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$445, DW_AT_location[DW_OP_breg20 -5]
$C$DW$446	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$446, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$446, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1162| 
        MOV       *-SP[5],AL            ; [CPU_] |1162| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1162| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1162| 
	.dwpsn	file "../gateway.c",line 1164,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1164| 
        CMPB      AL,#1                 ; [CPU_] |1164| 
        BF        $C$L172,NEQ           ; [CPU_] |1164| 
        ; branchcc occurs ; [] |1164| 
	.dwpsn	file "../gateway.c",line 1166,column 5,is_stmt
        MOVB      AL,#10                ; [CPU_] |1166| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |1166| 
        MOVB      AH,#8                 ; [CPU_] |1166| 
        MOVB      XAR5,#0               ; [CPU_] |1166| 
$C$DW$447	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$447, DW_AT_low_pc(0x00)
	.dwattr $C$DW$447, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$447, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |1166| 
        ; call occurs [#_I2C_Command] ; [] |1166| 
	.dwpsn	file "../gateway.c",line 1167,column 3,is_stmt
        B         $C$L173,UNC           ; [CPU_] |1167| 
        ; branch occurs ; [] |1167| 
$C$L172:    
	.dwpsn	file "../gateway.c",line 1170,column 5,is_stmt
        MOVB      AL,#9                 ; [CPU_] |1170| 
        MOVL      XAR4,#_ODV_RTC_Text   ; [CPU_U] |1170| 
        MOVB      AH,#8                 ; [CPU_] |1170| 
        MOVB      XAR5,#0               ; [CPU_] |1170| 
$C$DW$448	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$448, DW_AT_low_pc(0x00)
	.dwattr $C$DW$448, DW_AT_name("_I2C_Command")
	.dwattr $C$DW$448, DW_AT_TI_call
        LCR       #_I2C_Command         ; [CPU_] |1170| 
        ; call occurs [#_I2C_Command] ; [] |1170| 
$C$L173:    
	.dwpsn	file "../gateway.c",line 1172,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1172| 
	.dwpsn	file "../gateway.c",line 1173,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$449	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$449, DW_AT_low_pc(0x00)
	.dwattr $C$DW$449, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$438, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$438, DW_AT_TI_end_line(0x495)
	.dwattr $C$DW$438, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$438

	.sect	".text"
	.clink
	.global	_MotCurCallback

$C$DW$450	.dwtag  DW_TAG_subprogram, DW_AT_name("MotCurCallback")
	.dwattr $C$DW$450, DW_AT_low_pc(_MotCurCallback)
	.dwattr $C$DW$450, DW_AT_high_pc(0x00)
	.dwattr $C$DW$450, DW_AT_TI_symbol_name("_MotCurCallback")
	.dwattr $C$DW$450, DW_AT_external
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$450, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$450, DW_AT_TI_begin_line(0x497)
	.dwattr $C$DW$450, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$450, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1175,column 93,is_stmt,address _MotCurCallback

	.dwfde $C$DW$CIE, _MotCurCallback
$C$DW$451	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$451, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$451, DW_AT_location[DW_OP_reg12]
$C$DW$452	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$452, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$452, DW_AT_location[DW_OP_reg14]
$C$DW$453	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$453, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$453, DW_AT_location[DW_OP_reg0]
$C$DW$454	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$454, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$454, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MotCurCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MotCurCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$455	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$455, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$455, DW_AT_location[DW_OP_breg20 -2]
$C$DW$456	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$456, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$456, DW_AT_location[DW_OP_breg20 -4]
$C$DW$457	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$457, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$457, DW_AT_location[DW_OP_breg20 -5]
$C$DW$458	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$458, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$458, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1175| 
        MOV       *-SP[5],AL            ; [CPU_] |1175| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1175| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1175| 
	.dwpsn	file "../gateway.c",line 1176,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1176| 
        CMPB      AL,#1                 ; [CPU_] |1176| 
        BF        $C$L174,NEQ           ; [CPU_] |1176| 
        ; branchcc occurs ; [] |1176| 
	.dwpsn	file "../gateway.c",line 1177,column 5,is_stmt
        MOV       AL,*-SP[5]            ; [CPU_] |1177| 
        CMPB      AL,#2                 ; [CPU_] |1177| 
        BF        $C$L174,NEQ           ; [CPU_] |1177| 
        ; branchcc occurs ; [] |1177| 
	.dwpsn	file "../gateway.c",line 1178,column 7,is_stmt
        MOVW      DP,#_HAL_NewCurPoint  ; [CPU_U] 
        MOVB      @_HAL_NewCurPoint,#1,UNC ; [CPU_] |1178| 
$C$L174:    
	.dwpsn	file "../gateway.c",line 1181,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1181| 
	.dwpsn	file "../gateway.c",line 1182,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$459	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$459, DW_AT_low_pc(0x00)
	.dwattr $C$DW$459, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$450, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$450, DW_AT_TI_end_line(0x49e)
	.dwattr $C$DW$450, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$450

	.sect	".text"
	.clink
	.global	_SciSendCallback

$C$DW$460	.dwtag  DW_TAG_subprogram, DW_AT_name("SciSendCallback")
	.dwattr $C$DW$460, DW_AT_low_pc(_SciSendCallback)
	.dwattr $C$DW$460, DW_AT_high_pc(0x00)
	.dwattr $C$DW$460, DW_AT_TI_symbol_name("_SciSendCallback")
	.dwattr $C$DW$460, DW_AT_external
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$460, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$460, DW_AT_TI_begin_line(0x4a1)
	.dwattr $C$DW$460, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$460, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1185,column 94,is_stmt,address _SciSendCallback

	.dwfde $C$DW$CIE, _SciSendCallback
$C$DW$461	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$461, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$461, DW_AT_location[DW_OP_reg12]
$C$DW$462	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$462, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$462, DW_AT_location[DW_OP_reg14]
$C$DW$463	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$463, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$463, DW_AT_location[DW_OP_reg0]
$C$DW$464	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$464, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$464, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SciSendCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SciSendCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$465	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$465, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$465, DW_AT_location[DW_OP_breg20 -2]
$C$DW$466	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$466, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$466, DW_AT_location[DW_OP_breg20 -4]
$C$DW$467	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$467, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$467, DW_AT_location[DW_OP_breg20 -5]
$C$DW$468	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$468, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$468, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1185| 
        MOV       *-SP[5],AL            ; [CPU_] |1185| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1185| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1185| 
	.dwpsn	file "../gateway.c",line 1186,column 3,is_stmt
        MOVB      AL,#0                 ; [CPU_] |1186| 
        MOVL      XAR4,#_sci_rx_mbox    ; [CPU_U] |1186| 
        MOVL      XAR5,#_ODV_SciSend    ; [CPU_U] |1186| 
$C$DW$469	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$469, DW_AT_low_pc(0x00)
	.dwattr $C$DW$469, DW_AT_name("_MBX_post")
	.dwattr $C$DW$469, DW_AT_TI_call
        LCR       #_MBX_post            ; [CPU_] |1186| 
        ; call occurs [#_MBX_post] ; [] |1186| 
	.dwpsn	file "../gateway.c",line 1189,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1189| 
	.dwpsn	file "../gateway.c",line 1190,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$470	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$470, DW_AT_low_pc(0x00)
	.dwattr $C$DW$470, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$460, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$460, DW_AT_TI_end_line(0x4a6)
	.dwattr $C$DW$460, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$460

	.sect	".text"
	.clink
	.global	_SinCosCallback

$C$DW$471	.dwtag  DW_TAG_subprogram, DW_AT_name("SinCosCallback")
	.dwattr $C$DW$471, DW_AT_low_pc(_SinCosCallback)
	.dwattr $C$DW$471, DW_AT_high_pc(0x00)
	.dwattr $C$DW$471, DW_AT_TI_symbol_name("_SinCosCallback")
	.dwattr $C$DW$471, DW_AT_external
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$471, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$471, DW_AT_TI_begin_line(0x4a8)
	.dwattr $C$DW$471, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$471, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1192,column 93,is_stmt,address _SinCosCallback

	.dwfde $C$DW$CIE, _SinCosCallback
$C$DW$472	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$472, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$472, DW_AT_location[DW_OP_reg12]
$C$DW$473	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$473, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$473, DW_AT_location[DW_OP_reg14]
$C$DW$474	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$474, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$474, DW_AT_location[DW_OP_reg0]
$C$DW$475	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$475, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$475, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SinCosCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SinCosCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$476	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$476, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$476, DW_AT_location[DW_OP_breg20 -2]
$C$DW$477	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$477, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$477, DW_AT_location[DW_OP_breg20 -4]
$C$DW$478	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$478, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$478, DW_AT_location[DW_OP_breg20 -5]
$C$DW$479	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$479, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$479, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1192| 
        MOV       *-SP[5],AL            ; [CPU_] |1192| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1192| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1192| 
	.dwpsn	file "../gateway.c",line 1194,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1194| 
	.dwpsn	file "../gateway.c",line 1195,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$480	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$480, DW_AT_low_pc(0x00)
	.dwattr $C$DW$480, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$471, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$471, DW_AT_TI_end_line(0x4ab)
	.dwattr $C$DW$471, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$471

	.sect	".text"
	.clink
	.global	_StartRecorderCallBack

$C$DW$481	.dwtag  DW_TAG_subprogram, DW_AT_name("StartRecorderCallBack")
	.dwattr $C$DW$481, DW_AT_low_pc(_StartRecorderCallBack)
	.dwattr $C$DW$481, DW_AT_high_pc(0x00)
	.dwattr $C$DW$481, DW_AT_TI_symbol_name("_StartRecorderCallBack")
	.dwattr $C$DW$481, DW_AT_external
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$481, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$481, DW_AT_TI_begin_line(0x4ae)
	.dwattr $C$DW$481, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$481, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1198,column 100,is_stmt,address _StartRecorderCallBack

	.dwfde $C$DW$CIE, _StartRecorderCallBack
$C$DW$482	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$482, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$482, DW_AT_location[DW_OP_reg12]
$C$DW$483	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$483, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$483, DW_AT_location[DW_OP_reg14]
$C$DW$484	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$484, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$484, DW_AT_location[DW_OP_reg0]
$C$DW$485	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$485, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$485, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _StartRecorderCallBack        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_StartRecorderCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$486	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$486, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$486, DW_AT_location[DW_OP_breg20 -2]
$C$DW$487	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$487, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$487, DW_AT_location[DW_OP_breg20 -4]
$C$DW$488	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$488, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$488, DW_AT_location[DW_OP_breg20 -5]
$C$DW$489	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$489, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$489, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1198| 
        MOV       *-SP[5],AL            ; [CPU_] |1198| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1198| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1198| 
	.dwpsn	file "../gateway.c",line 1200,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1200| 
        CMPB      AL,#1                 ; [CPU_] |1200| 
        BF        $C$L175,NEQ           ; [CPU_] |1200| 
        ; branchcc occurs ; [] |1200| 
	.dwpsn	file "../gateway.c",line 1201,column 5,is_stmt
$C$DW$490	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$490, DW_AT_low_pc(0x00)
	.dwattr $C$DW$490, DW_AT_name("_REC_StartRecorder")
	.dwattr $C$DW$490, DW_AT_TI_call
        LCR       #_REC_StartRecorder   ; [CPU_] |1201| 
        ; call occurs [#_REC_StartRecorder] ; [] |1201| 
$C$L175:    
	.dwpsn	file "../gateway.c",line 1203,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1203| 
	.dwpsn	file "../gateway.c",line 1204,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$491	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$491, DW_AT_low_pc(0x00)
	.dwattr $C$DW$491, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$481, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$481, DW_AT_TI_end_line(0x4b4)
	.dwattr $C$DW$481, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$481

	.sect	".text"
	.clink
	.global	_MultiunitsCallback

$C$DW$492	.dwtag  DW_TAG_subprogram, DW_AT_name("MultiunitsCallback")
	.dwattr $C$DW$492, DW_AT_low_pc(_MultiunitsCallback)
	.dwattr $C$DW$492, DW_AT_high_pc(0x00)
	.dwattr $C$DW$492, DW_AT_TI_symbol_name("_MultiunitsCallback")
	.dwattr $C$DW$492, DW_AT_external
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$492, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$492, DW_AT_TI_begin_line(0x4b6)
	.dwattr $C$DW$492, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$492, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1206,column 97,is_stmt,address _MultiunitsCallback

	.dwfde $C$DW$CIE, _MultiunitsCallback
$C$DW$493	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$493, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$493, DW_AT_location[DW_OP_reg12]
$C$DW$494	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$494, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$494, DW_AT_location[DW_OP_reg14]
$C$DW$495	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$495, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$495, DW_AT_location[DW_OP_reg0]
$C$DW$496	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$496, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$496, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _MultiunitsCallback           FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_MultiunitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$497	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$497, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$497, DW_AT_location[DW_OP_breg20 -2]
$C$DW$498	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$498, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$498, DW_AT_location[DW_OP_breg20 -4]
$C$DW$499	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$499, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$499, DW_AT_location[DW_OP_breg20 -5]
$C$DW$500	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$500, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$500, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1206| 
        MOV       *-SP[5],AL            ; [CPU_] |1206| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1206| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1206| 
	.dwpsn	file "../gateway.c",line 1208,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1208| 
        CMPB      AL,#1                 ; [CPU_] |1208| 
        BF        $C$L176,NEQ           ; [CPU_] |1208| 
        ; branchcc occurs ; [] |1208| 
	.dwpsn	file "../gateway.c",line 1209,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Multiunits ; [CPU_] |1209| 
$C$DW$501	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$501, DW_AT_low_pc(0x00)
	.dwattr $C$DW$501, DW_AT_name("_PAR_AddMultiUnits")
	.dwattr $C$DW$501, DW_AT_TI_call
        LCR       #_PAR_AddMultiUnits   ; [CPU_] |1209| 
        ; call occurs [#_PAR_AddMultiUnits] ; [] |1209| 
        MOVW      DP,#_ODV_Recorder_Multiunits ; [CPU_U] 
        MOV       @_ODV_Recorder_Multiunits,AL ; [CPU_] |1209| 
$C$L176:    
	.dwpsn	file "../gateway.c",line 1211,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1211| 
	.dwpsn	file "../gateway.c",line 1212,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$502	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$502, DW_AT_low_pc(0x00)
	.dwattr $C$DW$502, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$492, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$492, DW_AT_TI_end_line(0x4bc)
	.dwattr $C$DW$492, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$492

	.sect	".text"
	.clink
	.global	_VariablesCallback

$C$DW$503	.dwtag  DW_TAG_subprogram, DW_AT_name("VariablesCallback")
	.dwattr $C$DW$503, DW_AT_low_pc(_VariablesCallback)
	.dwattr $C$DW$503, DW_AT_high_pc(0x00)
	.dwattr $C$DW$503, DW_AT_TI_symbol_name("_VariablesCallback")
	.dwattr $C$DW$503, DW_AT_external
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$503, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$503, DW_AT_TI_begin_line(0x4be)
	.dwattr $C$DW$503, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$503, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1214,column 96,is_stmt,address _VariablesCallback

	.dwfde $C$DW$CIE, _VariablesCallback
$C$DW$504	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$504, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$504, DW_AT_location[DW_OP_reg12]
$C$DW$505	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$505, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$505, DW_AT_location[DW_OP_reg14]
$C$DW$506	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$506, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$506, DW_AT_location[DW_OP_reg0]
$C$DW$507	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$507, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$507, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VariablesCallback            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VariablesCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$508	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$508, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$508, DW_AT_location[DW_OP_breg20 -2]
$C$DW$509	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$509, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$509, DW_AT_location[DW_OP_breg20 -4]
$C$DW$510	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$510, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$510, DW_AT_location[DW_OP_breg20 -5]
$C$DW$511	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$511, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$511, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1214| 
        MOV       *-SP[5],AL            ; [CPU_] |1214| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1214| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1214| 
	.dwpsn	file "../gateway.c",line 1216,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1216| 
        CMPB      AL,#1                 ; [CPU_] |1216| 
        BF        $C$L177,NEQ           ; [CPU_] |1216| 
        ; branchcc occurs ; [] |1216| 
	.dwpsn	file "../gateway.c",line 1217,column 5,is_stmt
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       AL,@_ODV_Recorder_Variables ; [CPU_] |1217| 
$C$DW$512	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$512, DW_AT_low_pc(0x00)
	.dwattr $C$DW$512, DW_AT_name("_PAR_AddVariables")
	.dwattr $C$DW$512, DW_AT_TI_call
        LCR       #_PAR_AddVariables    ; [CPU_] |1217| 
        ; call occurs [#_PAR_AddVariables] ; [] |1217| 
        MOVW      DP,#_ODV_Recorder_Variables ; [CPU_U] 
        MOV       @_ODV_Recorder_Variables,AL ; [CPU_] |1217| 
$C$L177:    
	.dwpsn	file "../gateway.c",line 1219,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1219| 
	.dwpsn	file "../gateway.c",line 1220,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$513	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$513, DW_AT_low_pc(0x00)
	.dwattr $C$DW$513, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$503, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$503, DW_AT_TI_end_line(0x4c4)
	.dwattr $C$DW$503, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$503

	.sect	".text"
	.clink
	.global	_WriteOutputs8BitCallback

$C$DW$514	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs8BitCallback")
	.dwattr $C$DW$514, DW_AT_low_pc(_WriteOutputs8BitCallback)
	.dwattr $C$DW$514, DW_AT_high_pc(0x00)
	.dwattr $C$DW$514, DW_AT_TI_symbol_name("_WriteOutputs8BitCallback")
	.dwattr $C$DW$514, DW_AT_external
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$514, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$514, DW_AT_TI_begin_line(0x4c7)
	.dwattr $C$DW$514, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$514, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1223,column 103,is_stmt,address _WriteOutputs8BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs8BitCallback
$C$DW$515	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$515, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$515, DW_AT_location[DW_OP_reg12]
$C$DW$516	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$516, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$516, DW_AT_location[DW_OP_reg14]
$C$DW$517	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$517, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$517, DW_AT_location[DW_OP_reg0]
$C$DW$518	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$518, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$518, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs8BitCallback     FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs8BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$519	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$519, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$519, DW_AT_location[DW_OP_breg20 -2]
$C$DW$520	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$520, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$520, DW_AT_location[DW_OP_breg20 -4]
$C$DW$521	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$521, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$521, DW_AT_location[DW_OP_breg20 -5]
$C$DW$522	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$522, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$522, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1223| 
        MOV       *-SP[5],AL            ; [CPU_] |1223| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1223| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1223| 
	.dwpsn	file "../gateway.c",line 1224,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 1225,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1225| 
	.dwpsn	file "../gateway.c",line 1226,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$523	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$523, DW_AT_low_pc(0x00)
	.dwattr $C$DW$523, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$514, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$514, DW_AT_TI_end_line(0x4ca)
	.dwattr $C$DW$514, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$514

	.sect	".text"
	.clink
	.global	_WriteOutputs16BitCallback

$C$DW$524	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteOutputs16BitCallback")
	.dwattr $C$DW$524, DW_AT_low_pc(_WriteOutputs16BitCallback)
	.dwattr $C$DW$524, DW_AT_high_pc(0x00)
	.dwattr $C$DW$524, DW_AT_TI_symbol_name("_WriteOutputs16BitCallback")
	.dwattr $C$DW$524, DW_AT_external
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$524, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$524, DW_AT_TI_begin_line(0x4cc)
	.dwattr $C$DW$524, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$524, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1228,column 104,is_stmt,address _WriteOutputs16BitCallback

	.dwfde $C$DW$CIE, _WriteOutputs16BitCallback
$C$DW$525	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$525, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$525, DW_AT_location[DW_OP_reg12]
$C$DW$526	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$526, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$526, DW_AT_location[DW_OP_reg14]
$C$DW$527	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$527, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$527, DW_AT_location[DW_OP_reg0]
$C$DW$528	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$528, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$528, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteOutputs16BitCallback    FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteOutputs16BitCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$529	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$529, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$529, DW_AT_location[DW_OP_breg20 -2]
$C$DW$530	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$530, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$530, DW_AT_location[DW_OP_breg20 -4]
$C$DW$531	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$531, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$531, DW_AT_location[DW_OP_breg20 -5]
$C$DW$532	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$532, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$532, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1228| 
        MOV       *-SP[5],AL            ; [CPU_] |1228| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1228| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1228| 
	.dwpsn	file "../gateway.c",line 1229,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 1232,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1232| 
	.dwpsn	file "../gateway.c",line 1233,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$533	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$533, DW_AT_low_pc(0x00)
	.dwattr $C$DW$533, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$524, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$524, DW_AT_TI_end_line(0x4d1)
	.dwattr $C$DW$524, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$524

	.sect	".text"
	.clink
	.global	_ReadInputs8BitsCallback

$C$DW$534	.dwtag  DW_TAG_subprogram, DW_AT_name("ReadInputs8BitsCallback")
	.dwattr $C$DW$534, DW_AT_low_pc(_ReadInputs8BitsCallback)
	.dwattr $C$DW$534, DW_AT_high_pc(0x00)
	.dwattr $C$DW$534, DW_AT_TI_symbol_name("_ReadInputs8BitsCallback")
	.dwattr $C$DW$534, DW_AT_external
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$534, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$534, DW_AT_TI_begin_line(0x4d3)
	.dwattr $C$DW$534, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$534, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1235,column 102,is_stmt,address _ReadInputs8BitsCallback

	.dwfde $C$DW$CIE, _ReadInputs8BitsCallback
$C$DW$535	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$535, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$535, DW_AT_location[DW_OP_reg12]
$C$DW$536	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$536, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$536, DW_AT_location[DW_OP_reg14]
$C$DW$537	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$537, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$537, DW_AT_location[DW_OP_reg0]
$C$DW$538	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$538, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$538, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ReadInputs8BitsCallback      FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ReadInputs8BitsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$539	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$539, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$539, DW_AT_location[DW_OP_breg20 -2]
$C$DW$540	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$540, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$540, DW_AT_location[DW_OP_breg20 -4]
$C$DW$541	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$541, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$541, DW_AT_location[DW_OP_breg20 -5]
$C$DW$542	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$542, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$542, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1235| 
        MOV       *-SP[5],AL            ; [CPU_] |1235| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1235| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1235| 
	.dwpsn	file "../gateway.c",line 1237,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1237| 
	.dwpsn	file "../gateway.c",line 1238,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$543	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$543, DW_AT_low_pc(0x00)
	.dwattr $C$DW$543, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$534, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$534, DW_AT_TI_end_line(0x4d6)
	.dwattr $C$DW$534, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$534

	.sect	".text"
	.clink
	.global	_ControlWordCallBack

$C$DW$544	.dwtag  DW_TAG_subprogram, DW_AT_name("ControlWordCallBack")
	.dwattr $C$DW$544, DW_AT_low_pc(_ControlWordCallBack)
	.dwattr $C$DW$544, DW_AT_high_pc(0x00)
	.dwattr $C$DW$544, DW_AT_TI_symbol_name("_ControlWordCallBack")
	.dwattr $C$DW$544, DW_AT_external
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$544, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$544, DW_AT_TI_begin_line(0x4d8)
	.dwattr $C$DW$544, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$544, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1240,column 98,is_stmt,address _ControlWordCallBack

	.dwfde $C$DW$CIE, _ControlWordCallBack
$C$DW$545	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$545, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$545, DW_AT_location[DW_OP_reg12]
$C$DW$546	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$546, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$546, DW_AT_location[DW_OP_reg14]
$C$DW$547	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$547, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$547, DW_AT_location[DW_OP_reg0]
$C$DW$548	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$548, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$548, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ControlWordCallBack          FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ControlWordCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$549	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$549, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$549, DW_AT_location[DW_OP_breg20 -2]
$C$DW$550	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$550, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$550, DW_AT_location[DW_OP_breg20 -4]
$C$DW$551	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$551, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$551, DW_AT_location[DW_OP_breg20 -5]
$C$DW$552	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$552, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$552, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1240| 
        MOV       *-SP[5],AL            ; [CPU_] |1240| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1240| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1240| 
	.dwpsn	file "../gateway.c",line 1241,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 1242,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1242| 
	.dwpsn	file "../gateway.c",line 1243,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$553	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$553, DW_AT_low_pc(0x00)
	.dwattr $C$DW$553, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$544, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$544, DW_AT_TI_end_line(0x4db)
	.dwattr $C$DW$544, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$544

	.sect	".text"
	.clink
	.global	_VersionCallback

$C$DW$554	.dwtag  DW_TAG_subprogram, DW_AT_name("VersionCallback")
	.dwattr $C$DW$554, DW_AT_low_pc(_VersionCallback)
	.dwattr $C$DW$554, DW_AT_high_pc(0x00)
	.dwattr $C$DW$554, DW_AT_TI_symbol_name("_VersionCallback")
	.dwattr $C$DW$554, DW_AT_external
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$554, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$554, DW_AT_TI_begin_line(0x4dd)
	.dwattr $C$DW$554, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$554, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1245,column 94,is_stmt,address _VersionCallback

	.dwfde $C$DW$CIE, _VersionCallback
$C$DW$555	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$555, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$555, DW_AT_location[DW_OP_reg12]
$C$DW$556	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$556, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$556, DW_AT_location[DW_OP_reg14]
$C$DW$557	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$557, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$557, DW_AT_location[DW_OP_reg0]
$C$DW$558	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$558, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$558, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _VersionCallback              FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_VersionCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$559	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$559, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$559, DW_AT_location[DW_OP_breg20 -2]
$C$DW$560	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$560, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$560, DW_AT_location[DW_OP_breg20 -4]
$C$DW$561	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$561, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$561, DW_AT_location[DW_OP_breg20 -5]
$C$DW$562	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$562, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$562, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1245| 
        MOV       *-SP[5],AL            ; [CPU_] |1245| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1245| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1245| 
	.dwpsn	file "../gateway.c",line 1246,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1246| 
        BF        $C$L178,NEQ           ; [CPU_] |1246| 
        ; branchcc occurs ; [] |1246| 
	.dwpsn	file "../gateway.c",line 1246,column 27,is_stmt
        MOVW      DP,#_ODV_Version      ; [CPU_U] 
        MOVB      @_ODV_Version,#53,UNC ; [CPU_] |1246| 
$C$L178:    
	.dwpsn	file "../gateway.c",line 1247,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1247| 
	.dwpsn	file "../gateway.c",line 1248,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$563	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$563, DW_AT_low_pc(0x00)
	.dwattr $C$DW$563, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$554, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$554, DW_AT_TI_end_line(0x4e0)
	.dwattr $C$DW$554, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$554

	.sect	".text"
	.clink
	.global	_WriteAnalogueOutputsCallback

$C$DW$564	.dwtag  DW_TAG_subprogram, DW_AT_name("WriteAnalogueOutputsCallback")
	.dwattr $C$DW$564, DW_AT_low_pc(_WriteAnalogueOutputsCallback)
	.dwattr $C$DW$564, DW_AT_high_pc(0x00)
	.dwattr $C$DW$564, DW_AT_TI_symbol_name("_WriteAnalogueOutputsCallback")
	.dwattr $C$DW$564, DW_AT_external
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$564, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$564, DW_AT_TI_begin_line(0x4e2)
	.dwattr $C$DW$564, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$564, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1250,column 107,is_stmt,address _WriteAnalogueOutputsCallback

	.dwfde $C$DW$CIE, _WriteAnalogueOutputsCallback
$C$DW$565	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$565, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$565, DW_AT_location[DW_OP_reg12]
$C$DW$566	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$566, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$566, DW_AT_location[DW_OP_reg14]
$C$DW$567	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$567, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$567, DW_AT_location[DW_OP_reg0]
$C$DW$568	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$568, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$568, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _WriteAnalogueOutputsCallback FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_WriteAnalogueOutputsCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$569	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$569, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$569, DW_AT_location[DW_OP_breg20 -2]
$C$DW$570	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$570, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$570, DW_AT_location[DW_OP_breg20 -4]
$C$DW$571	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$571, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$571, DW_AT_location[DW_OP_breg20 -5]
$C$DW$572	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$572, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$572, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1250| 
        MOV       *-SP[5],AL            ; [CPU_] |1250| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1250| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1250| 
	.dwpsn	file "../gateway.c",line 1252,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1252| 
	.dwpsn	file "../gateway.c",line 1253,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$573	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$573, DW_AT_low_pc(0x00)
	.dwattr $C$DW$573, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$564, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$564, DW_AT_TI_end_line(0x4e5)
	.dwattr $C$DW$564, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$564

	.sect	".text"
	.clink
	.global	_CommErrorSetCallback

$C$DW$574	.dwtag  DW_TAG_subprogram, DW_AT_name("CommErrorSetCallback")
	.dwattr $C$DW$574, DW_AT_low_pc(_CommErrorSetCallback)
	.dwattr $C$DW$574, DW_AT_high_pc(0x00)
	.dwattr $C$DW$574, DW_AT_TI_symbol_name("_CommErrorSetCallback")
	.dwattr $C$DW$574, DW_AT_external
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$574, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$574, DW_AT_TI_begin_line(0x4e8)
	.dwattr $C$DW$574, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$574, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1256,column 99,is_stmt,address _CommErrorSetCallback

	.dwfde $C$DW$CIE, _CommErrorSetCallback
$C$DW$575	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$575, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$575, DW_AT_location[DW_OP_reg12]
$C$DW$576	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$576, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$576, DW_AT_location[DW_OP_reg14]
$C$DW$577	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$577, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$577, DW_AT_location[DW_OP_reg0]
$C$DW$578	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$578, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$578, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _CommErrorSetCallback         FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_CommErrorSetCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$579	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$579, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$579, DW_AT_location[DW_OP_breg20 -2]
$C$DW$580	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$580, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$580, DW_AT_location[DW_OP_breg20 -4]
$C$DW$581	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$581, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$581, DW_AT_location[DW_OP_breg20 -5]
$C$DW$582	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$582, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$582, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1256| 
        MOV       *-SP[5],AL            ; [CPU_] |1256| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1256| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1256| 
	.dwpsn	file "../gateway.c",line 1257,column 3,is_stmt
	.dwpsn	file "../gateway.c",line 1260,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1260| 
	.dwpsn	file "../gateway.c",line 1261,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$583	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$583, DW_AT_low_pc(0x00)
	.dwattr $C$DW$583, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$574, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$574, DW_AT_TI_end_line(0x4ed)
	.dwattr $C$DW$574, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$574

	.sect	".text"
	.clink
	.global	_SaveAllParameters

$C$DW$584	.dwtag  DW_TAG_subprogram, DW_AT_name("SaveAllParameters")
	.dwattr $C$DW$584, DW_AT_low_pc(_SaveAllParameters)
	.dwattr $C$DW$584, DW_AT_high_pc(0x00)
	.dwattr $C$DW$584, DW_AT_TI_symbol_name("_SaveAllParameters")
	.dwattr $C$DW$584, DW_AT_external
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$584, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$584, DW_AT_TI_begin_line(0x4ef)
	.dwattr $C$DW$584, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$584, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1263,column 96,is_stmt,address _SaveAllParameters

	.dwfde $C$DW$CIE, _SaveAllParameters
$C$DW$585	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$585, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$585, DW_AT_location[DW_OP_reg12]
$C$DW$586	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$586, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$586, DW_AT_location[DW_OP_reg14]
$C$DW$587	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$587, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$587, DW_AT_location[DW_OP_reg0]
$C$DW$588	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$588, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$588, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _SaveAllParameters            FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_SaveAllParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$589	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$589, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$589, DW_AT_location[DW_OP_breg20 -2]
$C$DW$590	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$590, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$590, DW_AT_location[DW_OP_breg20 -4]
$C$DW$591	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$591, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$591, DW_AT_location[DW_OP_breg20 -5]
$C$DW$592	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$592, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$592, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1263| 
        MOV       *-SP[5],AL            ; [CPU_] |1263| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1263| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1263| 
	.dwpsn	file "../gateway.c",line 1264,column 3,is_stmt
        MOVW      DP,#_ODV_StoreParameters ; [CPU_U] 
        MOV       AL,#30309             ; [CPU_] |1264| 
        MOV       AH,#29537             ; [CPU_] |1264| 
        CMPL      ACC,@_ODV_StoreParameters ; [CPU_] |1264| 
        BF        $C$L179,NEQ           ; [CPU_] |1264| 
        ; branchcc occurs ; [] |1264| 
	.dwpsn	file "../gateway.c",line 1265,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1265| 
        MOVL      @_ODV_StoreParameters,ACC ; [CPU_] |1265| 
	.dwpsn	file "../gateway.c",line 1266,column 5,is_stmt
        MOVW      DP,#_BoardODdata      ; [CPU_U] 
        MOVL      XAR4,@_BoardODdata    ; [CPU_] |1266| 
$C$DW$593	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$593, DW_AT_low_pc(0x00)
	.dwattr $C$DW$593, DW_AT_name("_PAR_WriteAllPermanentParam")
	.dwattr $C$DW$593, DW_AT_TI_call
        LCR       #_PAR_WriteAllPermanentParam ; [CPU_] |1266| 
        ; call occurs [#_PAR_WriteAllPermanentParam] ; [] |1266| 
$C$L179:    
	.dwpsn	file "../gateway.c",line 1268,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1268| 
	.dwpsn	file "../gateway.c",line 1269,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$594	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$594, DW_AT_low_pc(0x00)
	.dwattr $C$DW$594, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$584, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$584, DW_AT_TI_end_line(0x4f5)
	.dwattr $C$DW$584, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$584

	.sect	".text"
	.clink
	.global	_LoadDefaultParameters

$C$DW$595	.dwtag  DW_TAG_subprogram, DW_AT_name("LoadDefaultParameters")
	.dwattr $C$DW$595, DW_AT_low_pc(_LoadDefaultParameters)
	.dwattr $C$DW$595, DW_AT_high_pc(0x00)
	.dwattr $C$DW$595, DW_AT_TI_symbol_name("_LoadDefaultParameters")
	.dwattr $C$DW$595, DW_AT_external
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$595, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$595, DW_AT_TI_begin_line(0x4f7)
	.dwattr $C$DW$595, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$595, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1271,column 100,is_stmt,address _LoadDefaultParameters

	.dwfde $C$DW$CIE, _LoadDefaultParameters
$C$DW$596	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$596, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$596, DW_AT_location[DW_OP_reg12]
$C$DW$597	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$597, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$597, DW_AT_location[DW_OP_reg14]
$C$DW$598	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$598, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$598, DW_AT_location[DW_OP_reg0]
$C$DW$599	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$599, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$599, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _LoadDefaultParameters        FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_LoadDefaultParameters:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$600	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$600, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$600, DW_AT_location[DW_OP_breg20 -2]
$C$DW$601	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$601, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$601, DW_AT_location[DW_OP_breg20 -4]
$C$DW$602	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$602, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$602, DW_AT_location[DW_OP_breg20 -5]
$C$DW$603	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$603, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$603, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1271| 
        MOV       *-SP[5],AL            ; [CPU_] |1271| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1271| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1271| 
	.dwpsn	file "../gateway.c",line 1272,column 3,is_stmt
        MOVW      DP,#_ODV_RestoreDefaultParameters ; [CPU_U] 
        MOV       AL,#24932             ; [CPU_] |1272| 
        MOV       AH,#27759             ; [CPU_] |1272| 
        CMPL      ACC,@_ODV_RestoreDefaultParameters ; [CPU_] |1272| 
        BF        $C$L180,NEQ           ; [CPU_] |1272| 
        ; branchcc occurs ; [] |1272| 
	.dwpsn	file "../gateway.c",line 1273,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1273| 
        MOVL      @_ODV_RestoreDefaultParameters,ACC ; [CPU_] |1273| 
	.dwpsn	file "../gateway.c",line 1274,column 5,is_stmt
        MOVB      AL,#0                 ; [CPU_] |1274| 
$C$DW$604	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$604, DW_AT_low_pc(0x00)
	.dwattr $C$DW$604, DW_AT_name("_PAR_UpdateCode")
	.dwattr $C$DW$604, DW_AT_TI_call
        LCR       #_PAR_UpdateCode      ; [CPU_] |1274| 
        ; call occurs [#_PAR_UpdateCode] ; [] |1274| 
	.dwpsn	file "../gateway.c",line 1275,column 5,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |1275| 
        MOVL      @_BootCommand,ACC     ; [CPU_] |1275| 
$C$L180:    
	.dwpsn	file "../gateway.c",line 1277,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1277| 
	.dwpsn	file "../gateway.c",line 1278,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$605	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$605, DW_AT_low_pc(0x00)
	.dwattr $C$DW$605, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$595, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$595, DW_AT_TI_end_line(0x4fe)
	.dwattr $C$DW$595, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$595

	.sect	".text"
	.clink
	.global	_DebugCallBack

$C$DW$606	.dwtag  DW_TAG_subprogram, DW_AT_name("DebugCallBack")
	.dwattr $C$DW$606, DW_AT_low_pc(_DebugCallBack)
	.dwattr $C$DW$606, DW_AT_high_pc(0x00)
	.dwattr $C$DW$606, DW_AT_TI_symbol_name("_DebugCallBack")
	.dwattr $C$DW$606, DW_AT_external
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$606, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$606, DW_AT_TI_begin_line(0x501)
	.dwattr $C$DW$606, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$606, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1281,column 92,is_stmt,address _DebugCallBack

	.dwfde $C$DW$CIE, _DebugCallBack
$C$DW$607	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$607, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$607, DW_AT_location[DW_OP_reg12]
$C$DW$608	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$608, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$608, DW_AT_location[DW_OP_reg14]
$C$DW$609	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$609, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$609, DW_AT_location[DW_OP_reg0]
$C$DW$610	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$610, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$610, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _DebugCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_DebugCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$611	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$611, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$611, DW_AT_location[DW_OP_breg20 -2]
$C$DW$612	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$612, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$612, DW_AT_location[DW_OP_breg20 -4]
$C$DW$613	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$613, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$613, DW_AT_location[DW_OP_breg20 -5]
$C$DW$614	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$614, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$614, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1281| 
        MOV       *-SP[5],AL            ; [CPU_] |1281| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1281| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1281| 
	.dwpsn	file "../gateway.c",line 1282,column 3,is_stmt
        MOVW      DP,#_ODV_Debug        ; [CPU_U] 
        MOV       @_ODV_Debug,#0        ; [CPU_] |1282| 
	.dwpsn	file "../gateway.c",line 1283,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1283| 
	.dwpsn	file "../gateway.c",line 1284,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$615	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$615, DW_AT_low_pc(0x00)
	.dwattr $C$DW$615, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$606, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$606, DW_AT_TI_end_line(0x504)
	.dwattr $C$DW$606, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$606

	.sect	".text"
	.clink
	.global	_ConfigCallback

$C$DW$616	.dwtag  DW_TAG_subprogram, DW_AT_name("ConfigCallback")
	.dwattr $C$DW$616, DW_AT_low_pc(_ConfigCallback)
	.dwattr $C$DW$616, DW_AT_high_pc(0x00)
	.dwattr $C$DW$616, DW_AT_TI_symbol_name("_ConfigCallback")
	.dwattr $C$DW$616, DW_AT_external
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$616, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$616, DW_AT_TI_begin_line(0x506)
	.dwattr $C$DW$616, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$616, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1286,column 93,is_stmt,address _ConfigCallback

	.dwfde $C$DW$CIE, _ConfigCallback
$C$DW$617	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$617, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$617, DW_AT_location[DW_OP_reg12]
$C$DW$618	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$618, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$618, DW_AT_location[DW_OP_reg14]
$C$DW$619	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$619, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$619, DW_AT_location[DW_OP_reg0]
$C$DW$620	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$620, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$620, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ConfigCallback               FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ConfigCallback:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$621	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$621, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$621, DW_AT_location[DW_OP_breg20 -2]
$C$DW$622	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$622, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$622, DW_AT_location[DW_OP_breg20 -4]
$C$DW$623	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$623, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$623, DW_AT_location[DW_OP_breg20 -5]
$C$DW$624	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$624, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$624, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1286| 
        MOV       *-SP[5],AL            ; [CPU_] |1286| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1286| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1286| 
	.dwpsn	file "../gateway.c",line 1287,column 3,is_stmt
        MOV       AL,*-SP[6]            ; [CPU_] |1287| 
        CMPB      AL,#1                 ; [CPU_] |1287| 
        BF        $C$L181,NEQ           ; [CPU_] |1287| 
        ; branchcc occurs ; [] |1287| 
	.dwpsn	file "../gateway.c",line 1288,column 5,is_stmt
$C$L181:    
	.dwpsn	file "../gateway.c",line 1302,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1302| 
	.dwpsn	file "../gateway.c",line 1303,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$625	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$625, DW_AT_low_pc(0x00)
	.dwattr $C$DW$625, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$616, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$616, DW_AT_TI_end_line(0x517)
	.dwattr $C$DW$616, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$616

	.sect	".text"
	.clink
	.global	_ResetCallBack

$C$DW$626	.dwtag  DW_TAG_subprogram, DW_AT_name("ResetCallBack")
	.dwattr $C$DW$626, DW_AT_low_pc(_ResetCallBack)
	.dwattr $C$DW$626, DW_AT_high_pc(0x00)
	.dwattr $C$DW$626, DW_AT_TI_symbol_name("_ResetCallBack")
	.dwattr $C$DW$626, DW_AT_external
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$626, DW_AT_TI_begin_file("../gateway.c")
	.dwattr $C$DW$626, DW_AT_TI_begin_line(0x519)
	.dwattr $C$DW$626, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$626, DW_AT_TI_max_frame_size(-8)
	.dwpsn	file "../gateway.c",line 1305,column 92,is_stmt,address _ResetCallBack

	.dwfde $C$DW$CIE, _ResetCallBack
$C$DW$627	.dwtag  DW_TAG_formal_parameter, DW_AT_name("d")
	.dwattr $C$DW$627, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$627, DW_AT_location[DW_OP_reg12]
$C$DW$628	.dwtag  DW_TAG_formal_parameter, DW_AT_name("indextable")
	.dwattr $C$DW$628, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$628, DW_AT_location[DW_OP_reg14]
$C$DW$629	.dwtag  DW_TAG_formal_parameter, DW_AT_name("bSubindex")
	.dwattr $C$DW$629, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$629, DW_AT_location[DW_OP_reg0]
$C$DW$630	.dwtag  DW_TAG_formal_parameter, DW_AT_name("access")
	.dwattr $C$DW$630, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$630, DW_AT_location[DW_OP_reg1]

;***************************************************************
;* FNAME: _ResetCallBack                FR SIZE:   6           *
;*                                                             *
;* FUNCTION ENVIRONMENT                                        *
;*                                                             *
;* FUNCTION PROPERTIES                                         *
;*                            0 Parameter,  6 Auto,  0 SOE     *
;***************************************************************

_ResetCallBack:
	.dwcfi	cfa_offset, -2
	.dwcfi	save_reg_to_mem, 26, 0
	.dwcfi	save_reg_to_reg, 78, 26
        ADDB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -8
$C$DW$631	.dwtag  DW_TAG_variable, DW_AT_name("d")
	.dwattr $C$DW$631, DW_AT_TI_symbol_name("_d")
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$631, DW_AT_location[DW_OP_breg20 -2]
$C$DW$632	.dwtag  DW_TAG_variable, DW_AT_name("indextable")
	.dwattr $C$DW$632, DW_AT_TI_symbol_name("_indextable")
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$632, DW_AT_location[DW_OP_breg20 -4]
$C$DW$633	.dwtag  DW_TAG_variable, DW_AT_name("bSubindex")
	.dwattr $C$DW$633, DW_AT_TI_symbol_name("_bSubindex")
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$633, DW_AT_location[DW_OP_breg20 -5]
$C$DW$634	.dwtag  DW_TAG_variable, DW_AT_name("access")
	.dwattr $C$DW$634, DW_AT_TI_symbol_name("_access")
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$634, DW_AT_location[DW_OP_breg20 -6]
        MOV       *-SP[6],AH            ; [CPU_] |1305| 
        MOV       *-SP[5],AL            ; [CPU_] |1305| 
        MOVL      *-SP[4],XAR5          ; [CPU_] |1305| 
        MOVL      *-SP[2],XAR4          ; [CPU_] |1305| 
	.dwpsn	file "../gateway.c",line 1306,column 3,is_stmt
        MOVW      DP,#_ODV_ResetHW      ; [CPU_U] 
        MOV       AL,#29295             ; [CPU_] |1306| 
        MOV       AH,#31333             ; [CPU_] |1306| 
        CMPL      ACC,@_ODV_ResetHW     ; [CPU_] |1306| 
        BF        $C$L182,NEQ           ; [CPU_] |1306| 
        ; branchcc occurs ; [] |1306| 
	.dwpsn	file "../gateway.c",line 1307,column 5,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1307| 
        MOVL      @_ODV_ResetHW,ACC     ; [CPU_] |1307| 
	.dwpsn	file "../gateway.c",line 1308,column 5,is_stmt
        MOVW      DP,#_BootCommand      ; [CPU_U] 
        MOVB      ACC,#1                ; [CPU_] |1308| 
        MOVL      @_BootCommand,ACC     ; [CPU_] |1308| 
$C$L182:    
	.dwpsn	file "../gateway.c",line 1310,column 3,is_stmt
        MOVB      ACC,#0                ; [CPU_] |1310| 
	.dwpsn	file "../gateway.c",line 1311,column 1,is_stmt
        SUBB      SP,#6                 ; [CPU_U] 
	.dwcfi	cfa_offset, -2
$C$DW$635	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$635, DW_AT_low_pc(0x00)
	.dwattr $C$DW$635, DW_AT_TI_return
        LRETR     ; [CPU_] 
        ; return occurs ; [] 
	.dwattr $C$DW$626, DW_AT_TI_end_file("../gateway.c")
	.dwattr $C$DW$626, DW_AT_TI_end_line(0x51f)
	.dwattr $C$DW$626, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$626

;**************************************************************
;* UNDEFINED EXTERNAL REFERENCES                              *
;**************************************************************
	.global	_USB_Unlock
	.global	_genCRC32Table
	.global	_ADS_Init
	.global	_ERR_SetError
	.global	_ERR_ErrorUnderVoltage
	.global	_PAR_AddLog
	.global	_PAR_SetParamDependantVars
	.global	_PAR_GetLogNB
	.global	_ChargerError
	.global	_setNodeId
	.global	_ERR_ClearWarning
	.global	_ERR_HandleWarning
	.global	_USB_Stop
	.global	_HAL_Reset
	.global	_HAL_Random
	.global	_DIC_SetNodeId
	.global	_REC_StartRecorder
	.global	_HAL_Init
	.global	_ERR_ErrorOverVoltage
	.global	_HAL_Unlock
	.global	_ERR_ErrorOverCurrent
	.global	_canInit
	.global	_ERR_ErrorOverTemp
	.global	_ODP_Settings_AUD_Safety_Low_Voltage_Delay
	.global	_ODP_Board_Config
	.global	_ODV_Board_RelayStatus
	.global	_ODP_Board_BaudRate
	.global	_ODP_Sleep_Timeout
	.global	_ODP_Settings_AUD_Temp_Heater_ON_Min
	.global	_ODP_Settings_AUD_Heater_Voltage_OFF
	.global	_ODP_Sleep_Current
	.global	_ODP_Settings_AUD_TempHeater_OFF_Max
	.global	_ODP_CommError_TimeOut
	.global	_ODV_ChargerData_SlaveLimTemp
	.global	_ODP_CommError_Delay
	.global	_ODP_CommError_OverVoltage_ErrCounter
	.global	_ODP_CommError_OverTemp_ErrCounter
	.global	_ODP_Settings_AUD_Temperature_Delay
	.global	_ODP_CommError_Charge_NotSucessful
	.global	_ODV_MachineEvent
	.global	_ODV_ChargerData_MasterLimTemp
	.global	_ODV_ChargerData_ChargerError
	.global	_ODP_CommError_LowVoltage_ErrCounter
	.global	_ODP_SafetyLimits_Resistor_Tmin
	.global	_ODP_SafetyLimits_Resistor_Tmax
	.global	_ODP_SafetyLimits_Voltage_delay
	.global	_ODP_SafetyLimits_Resistor_Delay
	.global	_ODP_SafetyLimits_Current_delay
	.global	_ODP_SafetyLimits_Charge_In_Thres_Cur
	.global	_ODP_SafetyLimits_Umin_bal_delta
	.global	_ODP_SafetyLimits_OverVoltage
	.global	_ODP_SafetyLimits_Overcurrent
	.global	_ODP_SafetyLimits_UnderVoltage
	.global	_ODP_NbOfModules
	.global	_ODP_Current_C_D_Mode
	.global	_ODV_MachineMode
	.global	_ODP_RelayResetTime
	.global	_ODP_Settings_AUD_Gateway_Current_Ringsaver
	.global	_ODP_Temperature_WarningMin
	.global	_ODP_SafetyLimits_UnderCurrent
	.global	_ODV_Current_ChargeAllowed
	.global	_ODP_Temperature_WarningMax
	.global	_TimeLogIndex
	.global	_HAL_NewCurPoint
	.global	_ODV_Version
	.global	_ODV_Current_DischargeAllowed
	.global	_ODV_Write_Outputs_16_Bit
	.global	_ODV_Debug
	.global	_ODV_Controlword
	.global	_ODV_Recorder_Multiunits
	.global	_ODV_Recorder_Variables
	.global	_setState
	.global	_MBX_post
	.global	_PAR_AddVariables
	.global	_ODV_Gateway_Temperature
	.global	_ODV_Gateway_State
	.global	_ODV_Gateway_Voltage
	.global	_ODV_Gateway_Current
	.global	_ODV_SciSend
	.global	_PAR_InitParam
	.global	_PAR_AddMultiUnits
	.global	_USB_Start
	.global	_PAR_UpdateCode
	.global	_I2C_Command
	.global	_ODP_Gateway_Relay_Error_Count
	.global	_ODV_Gateway_Control_Heater
	.global	_ODP_Gateway_Delay_Relay_Error
	.global	_ODV_Gateway_Alive_Counter
	.global	_ODP_Gateway_IsoResistor_Limit_Max
	.global	_ODV_Gateway_Heater_Status
	.global	_ODP_SafetyLimits_Tmin
	.global	_ODP_SafetyLimits_Umax_bal_delta
	.global	_ODP_SafetyLimits_Tmax
	.global	_ODP_SafetyLimits_Umax
	.global	_ODP_SafetyLimits_Umin
	.global	_ODV_Gateway_Requested_ChargerStage
	.global	_SEM_pend
	.global	_ODV_Gateway_MaxDeltaCellVoltage
	.global	_ODV_Gateway_MinModTemp
	.global	_ODV_Gateway_MinCellVoltage
	.global	_ODV_Gateway_MaxCellVoltage
	.global	_MBX_pend
	.global	_ODV_Gateway_MaxModTemp
	.global	_ODV_Gateway_LogNB
	.global	_PAR_StoreODSubIndex
	.global	_PAR_WriteAllPermanentParam
	.global	_getCRC32_cpu
	.global	_PAR_WriteStatisticParam
	.global	_CNV_Round
	.global	_ODP_OnTime
	.global	_ODV_ErrorDsp_ErrorNumber
	.global	_ODV_StoreParameters
	.global	_ODV_RestoreDefaultParameters
	.global	_ODP_Board_RevisionNumber
	.global	_ODV_SysTick_ms
	.global	_ODP_Battery_Capacity
	.global	_ODV_Gateway_Errorcode
	.global	_ODP_RandomNB
	.global	_MMSConfig
	.global	_PAR_Capacity_Total
	.global	_ODV_ResetHW
	.global	_ODV_RTC_Text
	.global	_EVOCharger
	.global	_ODV_Gateway_Date_Time
	.global	_PAR_Capacity_TotalLife_Used
	.global	_golden_CRC_values
	.global	_TSK_timerSem
	.global	_ODV_Read_Analogue_Input_16_Bit
	.global	_PieCtrlRegs
	.global	_GpioDataRegs
	.global	_mailboxSDOout
	.global	_sci_rx_mbox
	.global	_can_rx_mbox
	.global	_can_tx_mbox
	.global	_ODV_Modules_MinCellVoltage
	.global	_ODV_Modules_Alarms
	.global	_ODV_Modules_Temperature_MIN
	.global	_ODV_Modules_Temperature
	.global	_ODV_Modules_Heater
	.global	_ODV_Modules_MaxCellVoltage
	.global	_ODV_Modules_Voltage
	.global	_ODI_gateway_dict_Data
	.global	FS$$DIV

;***************************************************************
;* TYPE INFORMATION                                            *
;***************************************************************

$C$DW$T$147	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$147, DW_AT_byte_size(0x01)
$C$DW$636	.dwtag  DW_TAG_enumerator, DW_AT_name("EVEN"), DW_AT_const_value(0x00)
$C$DW$637	.dwtag  DW_TAG_enumerator, DW_AT_name("ODD"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$147

$C$DW$T$148	.dwtag  DW_TAG_typedef, DW_AT_name("parity_t")
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$T$148, DW_AT_language(DW_LANG_C)

$C$DW$T$20	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x0b)
$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$638, DW_AT_name("cob_id")
	.dwattr $C$DW$638, DW_AT_TI_symbol_name("_cob_id")
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$639, DW_AT_name("rtr")
	.dwattr $C$DW$639, DW_AT_TI_symbol_name("_rtr")
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$640, DW_AT_name("len")
	.dwattr $C$DW$640, DW_AT_TI_symbol_name("_len")
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$641, DW_AT_name("data")
	.dwattr $C$DW$641, DW_AT_TI_symbol_name("_data")
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$138	.dwtag  DW_TAG_typedef, DW_AT_name("Message")
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$138, DW_AT_language(DW_LANG_C)
$C$DW$T$149	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$138)
$C$DW$T$150	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$T$150, DW_AT_address_class(0x16)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x07)
$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$642, DW_AT_name("csBoot_Up")
	.dwattr $C$DW$642, DW_AT_TI_symbol_name("_csBoot_Up")
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$643, DW_AT_name("csSDO")
	.dwattr $C$DW$643, DW_AT_TI_symbol_name("_csSDO")
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$644, DW_AT_name("csEmergency")
	.dwattr $C$DW$644, DW_AT_TI_symbol_name("_csEmergency")
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$645, DW_AT_name("csSYNC")
	.dwattr $C$DW$645, DW_AT_TI_symbol_name("_csSYNC")
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$646, DW_AT_name("csHeartbeat")
	.dwattr $C$DW$646, DW_AT_TI_symbol_name("_csHeartbeat")
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$647, DW_AT_name("csPDO")
	.dwattr $C$DW$647, DW_AT_TI_symbol_name("_csPDO")
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$648, DW_AT_name("csLSS")
	.dwattr $C$DW$648, DW_AT_TI_symbol_name("_csLSS")
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21

$C$DW$T$95	.dwtag  DW_TAG_typedef, DW_AT_name("s_state_communication")
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$T$95, DW_AT_language(DW_LANG_C)

$C$DW$T$22	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x03)
$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$649, DW_AT_name("errCode")
	.dwattr $C$DW$649, DW_AT_TI_symbol_name("_errCode")
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$650, DW_AT_name("errRegMask")
	.dwattr $C$DW$650, DW_AT_TI_symbol_name("_errRegMask")
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$651, DW_AT_name("active")
	.dwattr $C$DW$651, DW_AT_TI_symbol_name("_active")
	.dwattr $C$DW$651, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$129	.dwtag  DW_TAG_typedef, DW_AT_name("s_errors")
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$129, DW_AT_language(DW_LANG_C)

$C$DW$T$130	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$T$130, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x18)
$C$DW$652	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$652, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$130


$C$DW$T$23	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x04)
$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$653, DW_AT_name("index")
	.dwattr $C$DW$653, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$653, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$654, DW_AT_name("subindex")
	.dwattr $C$DW$654, DW_AT_TI_symbol_name("_subindex")
	.dwattr $C$DW$654, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$655, DW_AT_name("size")
	.dwattr $C$DW$655, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$656, DW_AT_name("address")
	.dwattr $C$DW$656, DW_AT_TI_symbol_name("_address")
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$23

$C$DW$T$135	.dwtag  DW_TAG_typedef, DW_AT_name("T_EepromIndexes")
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$135, DW_AT_language(DW_LANG_C)
$C$DW$T$136	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$136, DW_AT_address_class(0x16)

$C$DW$T$24	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x04)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$657, DW_AT_name("CANEnable")
	.dwattr $C$DW$657, DW_AT_TI_symbol_name("_CANEnable")
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$658, DW_AT_name("MaxACCurrent")
	.dwattr $C$DW$658, DW_AT_TI_symbol_name("_MaxACCurrent")
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$659, DW_AT_name("MaxDCVoltage")
	.dwattr $C$DW$659, DW_AT_TI_symbol_name("_MaxDCVoltage")
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$660, DW_AT_name("MaxDCCurrent")
	.dwattr $C$DW$660, DW_AT_TI_symbol_name("_MaxDCCurrent")
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$24

$C$DW$T$152	.dwtag  DW_TAG_typedef, DW_AT_name("ChargerSettings")
	.dwattr $C$DW$T$152, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$T$152, DW_AT_language(DW_LANG_C)
$C$DW$T$153	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$T$152)
$C$DW$T$154	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$T$154, DW_AT_address_class(0x16)

$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_byte_size(0x01)
$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$661, DW_AT_name("SwitchOn")
	.dwattr $C$DW$661, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$661, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$662, DW_AT_name("EnableVolt")
	.dwattr $C$DW$662, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$662, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$663, DW_AT_name("QuickStop")
	.dwattr $C$DW$663, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$663, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$664, DW_AT_name("EnableOperation")
	.dwattr $C$DW$664, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$664, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$665, DW_AT_name("OpModeSpecific")
	.dwattr $C$DW$665, DW_AT_TI_symbol_name("_OpModeSpecific")
	.dwattr $C$DW$665, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$666, DW_AT_name("ResetFault")
	.dwattr $C$DW$666, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$666, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$667, DW_AT_name("Halt")
	.dwattr $C$DW$667, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$667, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$668, DW_AT_name("Oms")
	.dwattr $C$DW$668, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$668, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$669, DW_AT_name("Rsvd")
	.dwattr $C$DW$669, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$669, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$670, DW_AT_name("Manufacturer")
	.dwattr $C$DW$670, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$670, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$25


$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_byte_size(0x01)
$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$671, DW_AT_name("SwitchOn")
	.dwattr $C$DW$671, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$671, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$672, DW_AT_name("EnableVolt")
	.dwattr $C$DW$672, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$672, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$673, DW_AT_name("QuickStop")
	.dwattr $C$DW$673, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$673, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$674, DW_AT_name("EnableOperation")
	.dwattr $C$DW$674, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$674, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$675, DW_AT_name("Rsvd0")
	.dwattr $C$DW$675, DW_AT_TI_symbol_name("_Rsvd0")
	.dwattr $C$DW$675, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x03)
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$676, DW_AT_name("ResetFault")
	.dwattr $C$DW$676, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$676, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$677, DW_AT_name("Halt")
	.dwattr $C$DW$677, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$677, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$678, DW_AT_name("Oms")
	.dwattr $C$DW$678, DW_AT_TI_symbol_name("_Oms")
	.dwattr $C$DW$678, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$679, DW_AT_name("Rsvd")
	.dwattr $C$DW$679, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$679, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$680, DW_AT_name("Manufacturer")
	.dwattr $C$DW$680, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$680, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$26


$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x01)
$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$681, DW_AT_name("SwitchOn")
	.dwattr $C$DW$681, DW_AT_TI_symbol_name("_SwitchOn")
	.dwattr $C$DW$681, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$682, DW_AT_name("EnableVolt")
	.dwattr $C$DW$682, DW_AT_TI_symbol_name("_EnableVolt")
	.dwattr $C$DW$682, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$683, DW_AT_name("QuickStop")
	.dwattr $C$DW$683, DW_AT_TI_symbol_name("_QuickStop")
	.dwattr $C$DW$683, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$684, DW_AT_name("EnableOperation")
	.dwattr $C$DW$684, DW_AT_TI_symbol_name("_EnableOperation")
	.dwattr $C$DW$684, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$685, DW_AT_name("NewSetPoint")
	.dwattr $C$DW$685, DW_AT_TI_symbol_name("_NewSetPoint")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$686, DW_AT_name("ChangeSetImm")
	.dwattr $C$DW$686, DW_AT_TI_symbol_name("_ChangeSetImm")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$687, DW_AT_name("Abs_Rel")
	.dwattr $C$DW$687, DW_AT_TI_symbol_name("_Abs_Rel")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$688, DW_AT_name("ResetFault")
	.dwattr $C$DW$688, DW_AT_TI_symbol_name("_ResetFault")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$689, DW_AT_name("Halt")
	.dwattr $C$DW$689, DW_AT_TI_symbol_name("_Halt")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$690, DW_AT_name("ChangeSetPos")
	.dwattr $C$DW$690, DW_AT_TI_symbol_name("_ChangeSetPos")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$691, DW_AT_name("Rsvd")
	.dwattr $C$DW$691, DW_AT_TI_symbol_name("_Rsvd")
	.dwattr $C$DW$691, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$692, DW_AT_name("Manufacturer")
	.dwattr $C$DW$692, DW_AT_TI_symbol_name("_Manufacturer")
	.dwattr $C$DW$692, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$27


$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x01)
$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$693, DW_AT_name("can_wk")
	.dwattr $C$DW$693, DW_AT_TI_symbol_name("_can_wk")
	.dwattr $C$DW$693, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$694, DW_AT_name("sw_wk")
	.dwattr $C$DW$694, DW_AT_TI_symbol_name("_sw_wk")
	.dwattr $C$DW$694, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$695, DW_AT_name("ch_wk")
	.dwattr $C$DW$695, DW_AT_TI_symbol_name("_ch_wk")
	.dwattr $C$DW$695, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$696, DW_AT_name("SOC2")
	.dwattr $C$DW$696, DW_AT_TI_symbol_name("_SOC2")
	.dwattr $C$DW$696, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$697, DW_AT_name("lem")
	.dwattr $C$DW$697, DW_AT_TI_symbol_name("_lem")
	.dwattr $C$DW$697, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$698, DW_AT_name("onerelay")
	.dwattr $C$DW$698, DW_AT_TI_symbol_name("_onerelay")
	.dwattr $C$DW$698, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$699, DW_AT_name("relayextra")
	.dwattr $C$DW$699, DW_AT_TI_symbol_name("_relayextra")
	.dwattr $C$DW$699, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$700, DW_AT_name("b7")
	.dwattr $C$DW$700, DW_AT_TI_symbol_name("_b7")
	.dwattr $C$DW$700, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$701, DW_AT_name("en24v")
	.dwattr $C$DW$701, DW_AT_TI_symbol_name("_en24v")
	.dwattr $C$DW$701, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$702, DW_AT_name("b9")
	.dwattr $C$DW$702, DW_AT_TI_symbol_name("_b9")
	.dwattr $C$DW$702, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$703, DW_AT_name("b10")
	.dwattr $C$DW$703, DW_AT_TI_symbol_name("_b10")
	.dwattr $C$DW$703, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$704, DW_AT_name("b11")
	.dwattr $C$DW$704, DW_AT_TI_symbol_name("_b11")
	.dwattr $C$DW$704, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$705, DW_AT_name("b12")
	.dwattr $C$DW$705, DW_AT_TI_symbol_name("_b12")
	.dwattr $C$DW$705, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$706, DW_AT_name("b13")
	.dwattr $C$DW$706, DW_AT_TI_symbol_name("_b13")
	.dwattr $C$DW$706, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$707, DW_AT_name("b14")
	.dwattr $C$DW$707, DW_AT_TI_symbol_name("_b14")
	.dwattr $C$DW$707, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$708, DW_AT_name("b15")
	.dwattr $C$DW$708, DW_AT_TI_symbol_name("_b15")
	.dwattr $C$DW$708, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$155	.dwtag  DW_TAG_typedef, DW_AT_name("TMMSConfig")
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$155, DW_AT_language(DW_LANG_C)
$C$DW$T$156	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$156, DW_AT_address_class(0x16)

$C$DW$T$29	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$29, DW_AT_byte_size(0x01)
$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$709, DW_AT_name("ControlWord")
	.dwattr $C$DW$709, DW_AT_TI_symbol_name("_ControlWord")
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$710, DW_AT_name("AnyMode")
	.dwattr $C$DW$710, DW_AT_TI_symbol_name("_AnyMode")
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$711, DW_AT_name("VelocityMode")
	.dwattr $C$DW$711, DW_AT_TI_symbol_name("_VelocityMode")
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$712, DW_AT_name("PositionMode")
	.dwattr $C$DW$712, DW_AT_TI_symbol_name("_PositionMode")
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$29

$C$DW$T$157	.dwtag  DW_TAG_typedef, DW_AT_name("TControlword")
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$157, DW_AT_language(DW_LANG_C)

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("AIODAT_BITS")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x02)
$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$713, DW_AT_name("rsvd1")
	.dwattr $C$DW$713, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$713, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$714, DW_AT_name("rsvd2")
	.dwattr $C$DW$714, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$714, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$715, DW_AT_name("AIO2")
	.dwattr $C$DW$715, DW_AT_TI_symbol_name("_AIO2")
	.dwattr $C$DW$715, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$716	.dwtag  DW_TAG_member
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$716, DW_AT_name("rsvd3")
	.dwattr $C$DW$716, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$716, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$716, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_member
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$717, DW_AT_name("AIO4")
	.dwattr $C$DW$717, DW_AT_TI_symbol_name("_AIO4")
	.dwattr $C$DW$717, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$717, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$717, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$718	.dwtag  DW_TAG_member
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$718, DW_AT_name("rsvd4")
	.dwattr $C$DW$718, DW_AT_TI_symbol_name("_rsvd4")
	.dwattr $C$DW$718, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$718, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_member
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$719, DW_AT_name("AIO6")
	.dwattr $C$DW$719, DW_AT_TI_symbol_name("_AIO6")
	.dwattr $C$DW$719, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$719, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$719, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$720	.dwtag  DW_TAG_member
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$720, DW_AT_name("rsvd5")
	.dwattr $C$DW$720, DW_AT_TI_symbol_name("_rsvd5")
	.dwattr $C$DW$720, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$720, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$720, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$721	.dwtag  DW_TAG_member
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$721, DW_AT_name("rsvd6")
	.dwattr $C$DW$721, DW_AT_TI_symbol_name("_rsvd6")
	.dwattr $C$DW$721, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$721, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$722, DW_AT_name("rsvd7")
	.dwattr $C$DW$722, DW_AT_TI_symbol_name("_rsvd7")
	.dwattr $C$DW$722, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$722, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$723, DW_AT_name("AIO10")
	.dwattr $C$DW$723, DW_AT_TI_symbol_name("_AIO10")
	.dwattr $C$DW$723, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$723, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$724, DW_AT_name("rsvd8")
	.dwattr $C$DW$724, DW_AT_TI_symbol_name("_rsvd8")
	.dwattr $C$DW$724, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$724, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$725, DW_AT_name("AIO12")
	.dwattr $C$DW$725, DW_AT_TI_symbol_name("_AIO12")
	.dwattr $C$DW$725, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$725, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$726, DW_AT_name("rsvd9")
	.dwattr $C$DW$726, DW_AT_TI_symbol_name("_rsvd9")
	.dwattr $C$DW$726, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$727, DW_AT_name("AIO14")
	.dwattr $C$DW$727, DW_AT_TI_symbol_name("_AIO14")
	.dwattr $C$DW$727, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$727, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$728, DW_AT_name("rsvd10")
	.dwattr $C$DW$728, DW_AT_TI_symbol_name("_rsvd10")
	.dwattr $C$DW$728, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$728, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$729, DW_AT_name("rsvd11")
	.dwattr $C$DW$729, DW_AT_TI_symbol_name("_rsvd11")
	.dwattr $C$DW$729, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x10)
	.dwattr $C$DW$729, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$31


$C$DW$T$33	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$33, DW_AT_name("AIODAT_REG")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x02)
$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$730, DW_AT_name("all")
	.dwattr $C$DW$730, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$730, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$731, DW_AT_name("bit")
	.dwattr $C$DW$731, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$33


$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_name("GPADAT_BITS")
	.dwattr $C$DW$T$34, DW_AT_byte_size(0x02)
$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$732, DW_AT_name("GPIO0")
	.dwattr $C$DW$732, DW_AT_TI_symbol_name("_GPIO0")
	.dwattr $C$DW$732, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$733, DW_AT_name("GPIO1")
	.dwattr $C$DW$733, DW_AT_TI_symbol_name("_GPIO1")
	.dwattr $C$DW$733, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$734, DW_AT_name("GPIO2")
	.dwattr $C$DW$734, DW_AT_TI_symbol_name("_GPIO2")
	.dwattr $C$DW$734, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$735, DW_AT_name("GPIO3")
	.dwattr $C$DW$735, DW_AT_TI_symbol_name("_GPIO3")
	.dwattr $C$DW$735, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$736, DW_AT_name("GPIO4")
	.dwattr $C$DW$736, DW_AT_TI_symbol_name("_GPIO4")
	.dwattr $C$DW$736, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$737, DW_AT_name("GPIO5")
	.dwattr $C$DW$737, DW_AT_TI_symbol_name("_GPIO5")
	.dwattr $C$DW$737, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$738, DW_AT_name("GPIO6")
	.dwattr $C$DW$738, DW_AT_TI_symbol_name("_GPIO6")
	.dwattr $C$DW$738, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$739, DW_AT_name("GPIO7")
	.dwattr $C$DW$739, DW_AT_TI_symbol_name("_GPIO7")
	.dwattr $C$DW$739, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$740, DW_AT_name("GPIO8")
	.dwattr $C$DW$740, DW_AT_TI_symbol_name("_GPIO8")
	.dwattr $C$DW$740, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$741, DW_AT_name("GPIO9")
	.dwattr $C$DW$741, DW_AT_TI_symbol_name("_GPIO9")
	.dwattr $C$DW$741, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$742, DW_AT_name("GPIO10")
	.dwattr $C$DW$742, DW_AT_TI_symbol_name("_GPIO10")
	.dwattr $C$DW$742, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$743, DW_AT_name("GPIO11")
	.dwattr $C$DW$743, DW_AT_TI_symbol_name("_GPIO11")
	.dwattr $C$DW$743, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$744, DW_AT_name("GPIO12")
	.dwattr $C$DW$744, DW_AT_TI_symbol_name("_GPIO12")
	.dwattr $C$DW$744, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$745, DW_AT_name("GPIO13")
	.dwattr $C$DW$745, DW_AT_TI_symbol_name("_GPIO13")
	.dwattr $C$DW$745, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$746, DW_AT_name("GPIO14")
	.dwattr $C$DW$746, DW_AT_TI_symbol_name("_GPIO14")
	.dwattr $C$DW$746, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$747, DW_AT_name("GPIO15")
	.dwattr $C$DW$747, DW_AT_TI_symbol_name("_GPIO15")
	.dwattr $C$DW$747, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$748, DW_AT_name("GPIO16")
	.dwattr $C$DW$748, DW_AT_TI_symbol_name("_GPIO16")
	.dwattr $C$DW$748, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$749, DW_AT_name("GPIO17")
	.dwattr $C$DW$749, DW_AT_TI_symbol_name("_GPIO17")
	.dwattr $C$DW$749, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$750, DW_AT_name("GPIO18")
	.dwattr $C$DW$750, DW_AT_TI_symbol_name("_GPIO18")
	.dwattr $C$DW$750, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$751, DW_AT_name("GPIO19")
	.dwattr $C$DW$751, DW_AT_TI_symbol_name("_GPIO19")
	.dwattr $C$DW$751, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$752, DW_AT_name("GPIO20")
	.dwattr $C$DW$752, DW_AT_TI_symbol_name("_GPIO20")
	.dwattr $C$DW$752, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$753, DW_AT_name("GPIO21")
	.dwattr $C$DW$753, DW_AT_TI_symbol_name("_GPIO21")
	.dwattr $C$DW$753, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$754, DW_AT_name("GPIO22")
	.dwattr $C$DW$754, DW_AT_TI_symbol_name("_GPIO22")
	.dwattr $C$DW$754, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$755, DW_AT_name("GPIO23")
	.dwattr $C$DW$755, DW_AT_TI_symbol_name("_GPIO23")
	.dwattr $C$DW$755, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$756, DW_AT_name("GPIO24")
	.dwattr $C$DW$756, DW_AT_TI_symbol_name("_GPIO24")
	.dwattr $C$DW$756, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$757, DW_AT_name("GPIO25")
	.dwattr $C$DW$757, DW_AT_TI_symbol_name("_GPIO25")
	.dwattr $C$DW$757, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$758, DW_AT_name("GPIO26")
	.dwattr $C$DW$758, DW_AT_TI_symbol_name("_GPIO26")
	.dwattr $C$DW$758, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$759, DW_AT_name("GPIO27")
	.dwattr $C$DW$759, DW_AT_TI_symbol_name("_GPIO27")
	.dwattr $C$DW$759, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$760, DW_AT_name("GPIO28")
	.dwattr $C$DW$760, DW_AT_TI_symbol_name("_GPIO28")
	.dwattr $C$DW$760, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$761, DW_AT_name("GPIO29")
	.dwattr $C$DW$761, DW_AT_TI_symbol_name("_GPIO29")
	.dwattr $C$DW$761, DW_AT_bit_offset(0x02), DW_AT_bit_size(0x01)
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$762, DW_AT_name("GPIO30")
	.dwattr $C$DW$762, DW_AT_TI_symbol_name("_GPIO30")
	.dwattr $C$DW$762, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x01)
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$763, DW_AT_name("GPIO31")
	.dwattr $C$DW$763, DW_AT_TI_symbol_name("_GPIO31")
	.dwattr $C$DW$763, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$34


$C$DW$T$35	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$35, DW_AT_name("GPADAT_REG")
	.dwattr $C$DW$T$35, DW_AT_byte_size(0x02)
$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$764, DW_AT_name("all")
	.dwattr $C$DW$764, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$765, DW_AT_name("bit")
	.dwattr $C$DW$765, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$35


$C$DW$T$36	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$36, DW_AT_name("GPBDAT_BITS")
	.dwattr $C$DW$T$36, DW_AT_byte_size(0x02)
$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$766, DW_AT_name("GPIO32")
	.dwattr $C$DW$766, DW_AT_TI_symbol_name("_GPIO32")
	.dwattr $C$DW$766, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$767, DW_AT_name("GPIO33")
	.dwattr $C$DW$767, DW_AT_TI_symbol_name("_GPIO33")
	.dwattr $C$DW$767, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$768, DW_AT_name("GPIO34")
	.dwattr $C$DW$768, DW_AT_TI_symbol_name("_GPIO34")
	.dwattr $C$DW$768, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$769, DW_AT_name("GPIO35")
	.dwattr $C$DW$769, DW_AT_TI_symbol_name("_GPIO35")
	.dwattr $C$DW$769, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$770, DW_AT_name("GPIO36")
	.dwattr $C$DW$770, DW_AT_TI_symbol_name("_GPIO36")
	.dwattr $C$DW$770, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$771, DW_AT_name("GPIO37")
	.dwattr $C$DW$771, DW_AT_TI_symbol_name("_GPIO37")
	.dwattr $C$DW$771, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$772, DW_AT_name("GPIO38")
	.dwattr $C$DW$772, DW_AT_TI_symbol_name("_GPIO38")
	.dwattr $C$DW$772, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$773, DW_AT_name("GPIO39")
	.dwattr $C$DW$773, DW_AT_TI_symbol_name("_GPIO39")
	.dwattr $C$DW$773, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$774, DW_AT_name("GPIO40")
	.dwattr $C$DW$774, DW_AT_TI_symbol_name("_GPIO40")
	.dwattr $C$DW$774, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$775, DW_AT_name("GPIO41")
	.dwattr $C$DW$775, DW_AT_TI_symbol_name("_GPIO41")
	.dwattr $C$DW$775, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$776, DW_AT_name("GPIO42")
	.dwattr $C$DW$776, DW_AT_TI_symbol_name("_GPIO42")
	.dwattr $C$DW$776, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$777, DW_AT_name("GPIO43")
	.dwattr $C$DW$777, DW_AT_TI_symbol_name("_GPIO43")
	.dwattr $C$DW$777, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$778, DW_AT_name("GPIO44")
	.dwattr $C$DW$778, DW_AT_TI_symbol_name("_GPIO44")
	.dwattr $C$DW$778, DW_AT_bit_offset(0x03), DW_AT_bit_size(0x01)
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$779, DW_AT_name("rsvd1")
	.dwattr $C$DW$779, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$779, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x03)
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$780, DW_AT_name("rsvd2")
	.dwattr $C$DW$780, DW_AT_TI_symbol_name("_rsvd2")
	.dwattr $C$DW$780, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x02)
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$781, DW_AT_name("GPIO50")
	.dwattr $C$DW$781, DW_AT_TI_symbol_name("_GPIO50")
	.dwattr $C$DW$781, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$782, DW_AT_name("GPIO51")
	.dwattr $C$DW$782, DW_AT_TI_symbol_name("_GPIO51")
	.dwattr $C$DW$782, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$783, DW_AT_name("GPIO52")
	.dwattr $C$DW$783, DW_AT_TI_symbol_name("_GPIO52")
	.dwattr $C$DW$783, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$784, DW_AT_name("GPIO53")
	.dwattr $C$DW$784, DW_AT_TI_symbol_name("_GPIO53")
	.dwattr $C$DW$784, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$785, DW_AT_name("GPIO54")
	.dwattr $C$DW$785, DW_AT_TI_symbol_name("_GPIO54")
	.dwattr $C$DW$785, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$786, DW_AT_name("GPIO55")
	.dwattr $C$DW$786, DW_AT_TI_symbol_name("_GPIO55")
	.dwattr $C$DW$786, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$787, DW_AT_name("GPIO56")
	.dwattr $C$DW$787, DW_AT_TI_symbol_name("_GPIO56")
	.dwattr $C$DW$787, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$788, DW_AT_name("GPIO57")
	.dwattr $C$DW$788, DW_AT_TI_symbol_name("_GPIO57")
	.dwattr $C$DW$788, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$789, DW_AT_name("GPIO58")
	.dwattr $C$DW$789, DW_AT_TI_symbol_name("_GPIO58")
	.dwattr $C$DW$789, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$790, DW_AT_name("rsvd3")
	.dwattr $C$DW$790, DW_AT_TI_symbol_name("_rsvd3")
	.dwattr $C$DW$790, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x05)
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$36


$C$DW$T$37	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$37, DW_AT_name("GPBDAT_REG")
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x02)
$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$791, DW_AT_name("all")
	.dwattr $C$DW$791, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$792, DW_AT_name("bit")
	.dwattr $C$DW$792, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$37


$C$DW$T$39	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$39, DW_AT_name("GPIO_DATA_REGS")
	.dwattr $C$DW$T$39, DW_AT_byte_size(0x20)
$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$793, DW_AT_name("GPADAT")
	.dwattr $C$DW$793, DW_AT_TI_symbol_name("_GPADAT")
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$794	.dwtag  DW_TAG_member
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$794, DW_AT_name("GPASET")
	.dwattr $C$DW$794, DW_AT_TI_symbol_name("_GPASET")
	.dwattr $C$DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$794, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$795, DW_AT_name("GPACLEAR")
	.dwattr $C$DW$795, DW_AT_TI_symbol_name("_GPACLEAR")
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$796	.dwtag  DW_TAG_member
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$796, DW_AT_name("GPATOGGLE")
	.dwattr $C$DW$796, DW_AT_TI_symbol_name("_GPATOGGLE")
	.dwattr $C$DW$796, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$796, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$797, DW_AT_name("GPBDAT")
	.dwattr $C$DW$797, DW_AT_TI_symbol_name("_GPBDAT")
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$798, DW_AT_name("GPBSET")
	.dwattr $C$DW$798, DW_AT_TI_symbol_name("_GPBSET")
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$799, DW_AT_name("GPBCLEAR")
	.dwattr $C$DW$799, DW_AT_TI_symbol_name("_GPBCLEAR")
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$800, DW_AT_name("GPBTOGGLE")
	.dwattr $C$DW$800, DW_AT_TI_symbol_name("_GPBTOGGLE")
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$801, DW_AT_name("rsvd1")
	.dwattr $C$DW$801, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$802, DW_AT_name("AIODAT")
	.dwattr $C$DW$802, DW_AT_TI_symbol_name("_AIODAT")
	.dwattr $C$DW$802, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$803	.dwtag  DW_TAG_member
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$803, DW_AT_name("AIOSET")
	.dwattr $C$DW$803, DW_AT_TI_symbol_name("_AIOSET")
	.dwattr $C$DW$803, DW_AT_data_member_location[DW_OP_plus_uconst 0x1a]
	.dwattr $C$DW$803, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$804	.dwtag  DW_TAG_member
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$804, DW_AT_name("AIOCLEAR")
	.dwattr $C$DW$804, DW_AT_TI_symbol_name("_AIOCLEAR")
	.dwattr $C$DW$804, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$804, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$805	.dwtag  DW_TAG_member
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$805, DW_AT_name("AIOTOGGLE")
	.dwattr $C$DW$805, DW_AT_TI_symbol_name("_AIOTOGGLE")
	.dwattr $C$DW$805, DW_AT_data_member_location[DW_OP_plus_uconst 0x1e]
	.dwattr $C$DW$805, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$39

$C$DW$806	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$39)
$C$DW$T$160	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$806)

$C$DW$T$47	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$47, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x08)
$C$DW$807	.dwtag  DW_TAG_member
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$807, DW_AT_name("wListElem")
	.dwattr $C$DW$807, DW_AT_TI_symbol_name("_wListElem")
	.dwattr $C$DW$807, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$807, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$808	.dwtag  DW_TAG_member
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$808, DW_AT_name("wCount")
	.dwattr $C$DW$808, DW_AT_TI_symbol_name("_wCount")
	.dwattr $C$DW$808, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$808, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$809	.dwtag  DW_TAG_member
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$809, DW_AT_name("fxn")
	.dwattr $C$DW$809, DW_AT_TI_symbol_name("_fxn")
	.dwattr $C$DW$809, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$809, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$47

$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Job")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)
$C$DW$T$42	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$42, DW_AT_address_class(0x16)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_JobHandle")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)

$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x30)
$C$DW$810	.dwtag  DW_TAG_member
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$810, DW_AT_name("dataQue")
	.dwattr $C$DW$810, DW_AT_TI_symbol_name("_dataQue")
	.dwattr $C$DW$810, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$810, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$811	.dwtag  DW_TAG_member
	.dwattr $C$DW$811, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$811, DW_AT_name("freeQue")
	.dwattr $C$DW$811, DW_AT_TI_symbol_name("_freeQue")
	.dwattr $C$DW$811, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$811, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$812	.dwtag  DW_TAG_member
	.dwattr $C$DW$812, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$812, DW_AT_name("dataSem")
	.dwattr $C$DW$812, DW_AT_TI_symbol_name("_dataSem")
	.dwattr $C$DW$812, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$812, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$813	.dwtag  DW_TAG_member
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$813, DW_AT_name("freeSem")
	.dwattr $C$DW$813, DW_AT_TI_symbol_name("_freeSem")
	.dwattr $C$DW$813, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$813, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$814	.dwtag  DW_TAG_member
	.dwattr $C$DW$814, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$814, DW_AT_name("segid")
	.dwattr $C$DW$814, DW_AT_TI_symbol_name("_segid")
	.dwattr $C$DW$814, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$814, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$815	.dwtag  DW_TAG_member
	.dwattr $C$DW$815, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$815, DW_AT_name("size")
	.dwattr $C$DW$815, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$815, DW_AT_data_member_location[DW_OP_plus_uconst 0x2a]
	.dwattr $C$DW$815, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$816	.dwtag  DW_TAG_member
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$816, DW_AT_name("length")
	.dwattr $C$DW$816, DW_AT_TI_symbol_name("_length")
	.dwattr $C$DW$816, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$816, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$817	.dwtag  DW_TAG_member
	.dwattr $C$DW$817, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$817, DW_AT_name("name")
	.dwattr $C$DW$817, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$817, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e]
	.dwattr $C$DW$817, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$54

$C$DW$T$161	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Obj")
	.dwattr $C$DW$T$161, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$161, DW_AT_language(DW_LANG_C)
$C$DW$T$163	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$163, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$163, DW_AT_address_class(0x16)
$C$DW$T$164	.dwtag  DW_TAG_typedef, DW_AT_name("MBX_Handle")
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$163)
	.dwattr $C$DW$T$164, DW_AT_language(DW_LANG_C)

$C$DW$T$55	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$55, DW_AT_name("PIEACK_BITS")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x01)
$C$DW$818	.dwtag  DW_TAG_member
	.dwattr $C$DW$818, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$818, DW_AT_name("ACK1")
	.dwattr $C$DW$818, DW_AT_TI_symbol_name("_ACK1")
	.dwattr $C$DW$818, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$818, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$818, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$819	.dwtag  DW_TAG_member
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$819, DW_AT_name("ACK2")
	.dwattr $C$DW$819, DW_AT_TI_symbol_name("_ACK2")
	.dwattr $C$DW$819, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$819, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$819, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$820	.dwtag  DW_TAG_member
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$820, DW_AT_name("ACK3")
	.dwattr $C$DW$820, DW_AT_TI_symbol_name("_ACK3")
	.dwattr $C$DW$820, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$820, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$820, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$821	.dwtag  DW_TAG_member
	.dwattr $C$DW$821, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$821, DW_AT_name("ACK4")
	.dwattr $C$DW$821, DW_AT_TI_symbol_name("_ACK4")
	.dwattr $C$DW$821, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$821, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$821, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$822	.dwtag  DW_TAG_member
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$822, DW_AT_name("ACK5")
	.dwattr $C$DW$822, DW_AT_TI_symbol_name("_ACK5")
	.dwattr $C$DW$822, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$822, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$822, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$823	.dwtag  DW_TAG_member
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$823, DW_AT_name("ACK6")
	.dwattr $C$DW$823, DW_AT_TI_symbol_name("_ACK6")
	.dwattr $C$DW$823, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$823, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$823, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$824	.dwtag  DW_TAG_member
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$824, DW_AT_name("ACK7")
	.dwattr $C$DW$824, DW_AT_TI_symbol_name("_ACK7")
	.dwattr $C$DW$824, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$824, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$824, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$825	.dwtag  DW_TAG_member
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$825, DW_AT_name("ACK8")
	.dwattr $C$DW$825, DW_AT_TI_symbol_name("_ACK8")
	.dwattr $C$DW$825, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$825, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$825, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$826	.dwtag  DW_TAG_member
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$826, DW_AT_name("ACK9")
	.dwattr $C$DW$826, DW_AT_TI_symbol_name("_ACK9")
	.dwattr $C$DW$826, DW_AT_bit_offset(0x07), DW_AT_bit_size(0x01)
	.dwattr $C$DW$826, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$826, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$827	.dwtag  DW_TAG_member
	.dwattr $C$DW$827, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$827, DW_AT_name("ACK10")
	.dwattr $C$DW$827, DW_AT_TI_symbol_name("_ACK10")
	.dwattr $C$DW$827, DW_AT_bit_offset(0x06), DW_AT_bit_size(0x01)
	.dwattr $C$DW$827, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$827, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$828	.dwtag  DW_TAG_member
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$828, DW_AT_name("ACK11")
	.dwattr $C$DW$828, DW_AT_TI_symbol_name("_ACK11")
	.dwattr $C$DW$828, DW_AT_bit_offset(0x05), DW_AT_bit_size(0x01)
	.dwattr $C$DW$828, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$828, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$829	.dwtag  DW_TAG_member
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$829, DW_AT_name("ACK12")
	.dwattr $C$DW$829, DW_AT_TI_symbol_name("_ACK12")
	.dwattr $C$DW$829, DW_AT_bit_offset(0x04), DW_AT_bit_size(0x01)
	.dwattr $C$DW$829, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$829, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$830	.dwtag  DW_TAG_member
	.dwattr $C$DW$830, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$830, DW_AT_name("rsvd1")
	.dwattr $C$DW$830, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$830, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x04)
	.dwattr $C$DW$830, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$830, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$55


$C$DW$T$56	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$56, DW_AT_name("PIEACK_REG")
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x01)
$C$DW$831	.dwtag  DW_TAG_member
	.dwattr $C$DW$831, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$831, DW_AT_name("all")
	.dwattr $C$DW$831, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$831, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$831, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$832	.dwtag  DW_TAG_member
	.dwattr $C$DW$832, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$832, DW_AT_name("bit")
	.dwattr $C$DW$832, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$832, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$832, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$56


$C$DW$T$57	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$57, DW_AT_name("PIECTRL_BITS")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x01)
$C$DW$833	.dwtag  DW_TAG_member
	.dwattr $C$DW$833, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$833, DW_AT_name("ENPIE")
	.dwattr $C$DW$833, DW_AT_TI_symbol_name("_ENPIE")
	.dwattr $C$DW$833, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$833, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$833, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$834	.dwtag  DW_TAG_member
	.dwattr $C$DW$834, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$834, DW_AT_name("PIEVECT")
	.dwattr $C$DW$834, DW_AT_TI_symbol_name("_PIEVECT")
	.dwattr $C$DW$834, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x0f)
	.dwattr $C$DW$834, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$834, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$57


$C$DW$T$58	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$58, DW_AT_name("PIECTRL_REG")
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x01)
$C$DW$835	.dwtag  DW_TAG_member
	.dwattr $C$DW$835, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$835, DW_AT_name("all")
	.dwattr $C$DW$835, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$835, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$835, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$836	.dwtag  DW_TAG_member
	.dwattr $C$DW$836, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$836, DW_AT_name("bit")
	.dwattr $C$DW$836, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$836, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$836, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$58


$C$DW$T$59	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$59, DW_AT_name("PIEIER_BITS")
	.dwattr $C$DW$T$59, DW_AT_byte_size(0x01)
$C$DW$837	.dwtag  DW_TAG_member
	.dwattr $C$DW$837, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$837, DW_AT_name("INTx1")
	.dwattr $C$DW$837, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$837, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$837, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$837, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$838	.dwtag  DW_TAG_member
	.dwattr $C$DW$838, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$838, DW_AT_name("INTx2")
	.dwattr $C$DW$838, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$838, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$838, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$838, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$839	.dwtag  DW_TAG_member
	.dwattr $C$DW$839, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$839, DW_AT_name("INTx3")
	.dwattr $C$DW$839, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$839, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$839, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$839, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$840	.dwtag  DW_TAG_member
	.dwattr $C$DW$840, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$840, DW_AT_name("INTx4")
	.dwattr $C$DW$840, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$840, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$840, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$840, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$841	.dwtag  DW_TAG_member
	.dwattr $C$DW$841, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$841, DW_AT_name("INTx5")
	.dwattr $C$DW$841, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$841, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$841, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$841, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$842	.dwtag  DW_TAG_member
	.dwattr $C$DW$842, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$842, DW_AT_name("INTx6")
	.dwattr $C$DW$842, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$842, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$842, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$842, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$843	.dwtag  DW_TAG_member
	.dwattr $C$DW$843, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$843, DW_AT_name("INTx7")
	.dwattr $C$DW$843, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$843, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$843, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$843, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$844	.dwtag  DW_TAG_member
	.dwattr $C$DW$844, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$844, DW_AT_name("INTx8")
	.dwattr $C$DW$844, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$844, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$844, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$844, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$845	.dwtag  DW_TAG_member
	.dwattr $C$DW$845, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$845, DW_AT_name("rsvd1")
	.dwattr $C$DW$845, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$845, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$845, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$845, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$59


$C$DW$T$60	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$60, DW_AT_name("PIEIER_REG")
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x01)
$C$DW$846	.dwtag  DW_TAG_member
	.dwattr $C$DW$846, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$846, DW_AT_name("all")
	.dwattr $C$DW$846, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$846, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$846, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$847	.dwtag  DW_TAG_member
	.dwattr $C$DW$847, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$847, DW_AT_name("bit")
	.dwattr $C$DW$847, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$847, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$847, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$60


$C$DW$T$61	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$61, DW_AT_name("PIEIFR_BITS")
	.dwattr $C$DW$T$61, DW_AT_byte_size(0x01)
$C$DW$848	.dwtag  DW_TAG_member
	.dwattr $C$DW$848, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$848, DW_AT_name("INTx1")
	.dwattr $C$DW$848, DW_AT_TI_symbol_name("_INTx1")
	.dwattr $C$DW$848, DW_AT_bit_offset(0x0f), DW_AT_bit_size(0x01)
	.dwattr $C$DW$848, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$848, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$849	.dwtag  DW_TAG_member
	.dwattr $C$DW$849, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$849, DW_AT_name("INTx2")
	.dwattr $C$DW$849, DW_AT_TI_symbol_name("_INTx2")
	.dwattr $C$DW$849, DW_AT_bit_offset(0x0e), DW_AT_bit_size(0x01)
	.dwattr $C$DW$849, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$849, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$850	.dwtag  DW_TAG_member
	.dwattr $C$DW$850, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$850, DW_AT_name("INTx3")
	.dwattr $C$DW$850, DW_AT_TI_symbol_name("_INTx3")
	.dwattr $C$DW$850, DW_AT_bit_offset(0x0d), DW_AT_bit_size(0x01)
	.dwattr $C$DW$850, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$850, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$851, DW_AT_name("INTx4")
	.dwattr $C$DW$851, DW_AT_TI_symbol_name("_INTx4")
	.dwattr $C$DW$851, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x01)
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$852, DW_AT_name("INTx5")
	.dwattr $C$DW$852, DW_AT_TI_symbol_name("_INTx5")
	.dwattr $C$DW$852, DW_AT_bit_offset(0x0b), DW_AT_bit_size(0x01)
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$853, DW_AT_name("INTx6")
	.dwattr $C$DW$853, DW_AT_TI_symbol_name("_INTx6")
	.dwattr $C$DW$853, DW_AT_bit_offset(0x0a), DW_AT_bit_size(0x01)
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$854, DW_AT_name("INTx7")
	.dwattr $C$DW$854, DW_AT_TI_symbol_name("_INTx7")
	.dwattr $C$DW$854, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x01)
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$855, DW_AT_name("INTx8")
	.dwattr $C$DW$855, DW_AT_TI_symbol_name("_INTx8")
	.dwattr $C$DW$855, DW_AT_bit_offset(0x08), DW_AT_bit_size(0x01)
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$856, DW_AT_name("rsvd1")
	.dwattr $C$DW$856, DW_AT_TI_symbol_name("_rsvd1")
	.dwattr $C$DW$856, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x08)
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$61


$C$DW$T$62	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$62, DW_AT_name("PIEIFR_REG")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x01)
$C$DW$857	.dwtag  DW_TAG_member
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$857, DW_AT_name("all")
	.dwattr $C$DW$857, DW_AT_TI_symbol_name("_all")
	.dwattr $C$DW$857, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$857, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$858, DW_AT_name("bit")
	.dwattr $C$DW$858, DW_AT_TI_symbol_name("_bit")
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$62


$C$DW$T$63	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$63, DW_AT_name("PIE_CTRL_REGS")
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x1a)
$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$859, DW_AT_name("PIECTRL")
	.dwattr $C$DW$859, DW_AT_TI_symbol_name("_PIECTRL")
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$860, DW_AT_name("PIEACK")
	.dwattr $C$DW$860, DW_AT_TI_symbol_name("_PIEACK")
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$861, DW_AT_name("PIEIER1")
	.dwattr $C$DW$861, DW_AT_TI_symbol_name("_PIEIER1")
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$862, DW_AT_name("PIEIFR1")
	.dwattr $C$DW$862, DW_AT_TI_symbol_name("_PIEIFR1")
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$863, DW_AT_name("PIEIER2")
	.dwattr $C$DW$863, DW_AT_TI_symbol_name("_PIEIER2")
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$864, DW_AT_name("PIEIFR2")
	.dwattr $C$DW$864, DW_AT_TI_symbol_name("_PIEIFR2")
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$865, DW_AT_name("PIEIER3")
	.dwattr $C$DW$865, DW_AT_TI_symbol_name("_PIEIER3")
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$866, DW_AT_name("PIEIFR3")
	.dwattr $C$DW$866, DW_AT_TI_symbol_name("_PIEIFR3")
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$867, DW_AT_name("PIEIER4")
	.dwattr $C$DW$867, DW_AT_TI_symbol_name("_PIEIER4")
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$868, DW_AT_name("PIEIFR4")
	.dwattr $C$DW$868, DW_AT_TI_symbol_name("_PIEIFR4")
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$869, DW_AT_name("PIEIER5")
	.dwattr $C$DW$869, DW_AT_TI_symbol_name("_PIEIER5")
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$870, DW_AT_name("PIEIFR5")
	.dwattr $C$DW$870, DW_AT_TI_symbol_name("_PIEIFR5")
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$871, DW_AT_name("PIEIER6")
	.dwattr $C$DW$871, DW_AT_TI_symbol_name("_PIEIER6")
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$872, DW_AT_name("PIEIFR6")
	.dwattr $C$DW$872, DW_AT_TI_symbol_name("_PIEIFR6")
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0xd]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$873, DW_AT_name("PIEIER7")
	.dwattr $C$DW$873, DW_AT_TI_symbol_name("_PIEIER7")
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$874, DW_AT_name("PIEIFR7")
	.dwattr $C$DW$874, DW_AT_TI_symbol_name("_PIEIFR7")
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$875, DW_AT_name("PIEIER8")
	.dwattr $C$DW$875, DW_AT_TI_symbol_name("_PIEIER8")
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$876	.dwtag  DW_TAG_member
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$876, DW_AT_name("PIEIFR8")
	.dwattr $C$DW$876, DW_AT_TI_symbol_name("_PIEIFR8")
	.dwattr $C$DW$876, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$876, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$877, DW_AT_name("PIEIER9")
	.dwattr $C$DW$877, DW_AT_TI_symbol_name("_PIEIER9")
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$878, DW_AT_name("PIEIFR9")
	.dwattr $C$DW$878, DW_AT_TI_symbol_name("_PIEIFR9")
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$879, DW_AT_name("PIEIER10")
	.dwattr $C$DW$879, DW_AT_TI_symbol_name("_PIEIER10")
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$880, DW_AT_name("PIEIFR10")
	.dwattr $C$DW$880, DW_AT_TI_symbol_name("_PIEIFR10")
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$881, DW_AT_name("PIEIER11")
	.dwattr $C$DW$881, DW_AT_TI_symbol_name("_PIEIER11")
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$882	.dwtag  DW_TAG_member
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$882, DW_AT_name("PIEIFR11")
	.dwattr $C$DW$882, DW_AT_TI_symbol_name("_PIEIFR11")
	.dwattr $C$DW$882, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$883	.dwtag  DW_TAG_member
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$883, DW_AT_name("PIEIER12")
	.dwattr $C$DW$883, DW_AT_TI_symbol_name("_PIEIER12")
	.dwattr $C$DW$883, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$883, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$884	.dwtag  DW_TAG_member
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$884, DW_AT_name("PIEIFR12")
	.dwattr $C$DW$884, DW_AT_TI_symbol_name("_PIEIFR12")
	.dwattr $C$DW$884, DW_AT_data_member_location[DW_OP_plus_uconst 0x19]
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$63

$C$DW$885	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$63)
$C$DW$T$167	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$885)

$C$DW$T$65	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$65, DW_AT_name("QUE_Elem")
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x04)
$C$DW$886	.dwtag  DW_TAG_member
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$886, DW_AT_name("next")
	.dwattr $C$DW$886, DW_AT_TI_symbol_name("_next")
	.dwattr $C$DW$886, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$886, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$887	.dwtag  DW_TAG_member
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$887, DW_AT_name("prev")
	.dwattr $C$DW$887, DW_AT_TI_symbol_name("_prev")
	.dwattr $C$DW$887, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$887, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$65

$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("QUE_Obj")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$64	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$64, DW_AT_address_class(0x16)

$C$DW$T$67	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$67, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x10)
$C$DW$888	.dwtag  DW_TAG_member
	.dwattr $C$DW$888, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$888, DW_AT_name("job")
	.dwattr $C$DW$888, DW_AT_TI_symbol_name("_job")
	.dwattr $C$DW$888, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$888, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$889	.dwtag  DW_TAG_member
	.dwattr $C$DW$889, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$889, DW_AT_name("count")
	.dwattr $C$DW$889, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$889, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$889, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$890	.dwtag  DW_TAG_member
	.dwattr $C$DW$890, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$890, DW_AT_name("pendQ")
	.dwattr $C$DW$890, DW_AT_TI_symbol_name("_pendQ")
	.dwattr $C$DW$890, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$890, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$891	.dwtag  DW_TAG_member
	.dwattr $C$DW$891, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$891, DW_AT_name("name")
	.dwattr $C$DW$891, DW_AT_TI_symbol_name("_name")
	.dwattr $C$DW$891, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$891, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$67

$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Obj")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$169	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$169, DW_AT_address_class(0x16)
$C$DW$T$170	.dwtag  DW_TAG_typedef, DW_AT_name("SEM_Handle")
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$T$170, DW_AT_language(DW_LANG_C)
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_address_class(0x16)
$C$DW$T$171	.dwtag  DW_TAG_typedef, DW_AT_name("Ptr")
	.dwattr $C$DW$T$171, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$171, DW_AT_language(DW_LANG_C)

$C$DW$T$44	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$892	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$892, DW_AT_type(*$C$DW$T$43)
	.dwendtag $C$DW$T$44

$C$DW$T$45	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_address_class(0x16)
$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("KNL_Fxn")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)

$C$DW$T$98	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$98, DW_AT_language(DW_LANG_C)
$C$DW$893	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$893, DW_AT_type(*$C$DW$T$97)
	.dwendtag $C$DW$T$98

$C$DW$T$99	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_address_class(0x16)
$C$DW$T$100	.dwtag  DW_TAG_typedef, DW_AT_name("initialisation_t")
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$100, DW_AT_language(DW_LANG_C)
$C$DW$T$102	.dwtag  DW_TAG_typedef, DW_AT_name("operational_t")
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$102, DW_AT_language(DW_LANG_C)
$C$DW$T$112	.dwtag  DW_TAG_typedef, DW_AT_name("post_TPDO_t")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$112, DW_AT_language(DW_LANG_C)
$C$DW$T$101	.dwtag  DW_TAG_typedef, DW_AT_name("preOperational_t")
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$101, DW_AT_language(DW_LANG_C)
$C$DW$T$111	.dwtag  DW_TAG_typedef, DW_AT_name("pre_sync_t")
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$111, DW_AT_language(DW_LANG_C)
$C$DW$T$103	.dwtag  DW_TAG_typedef, DW_AT_name("stopped_t")
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$103, DW_AT_language(DW_LANG_C)

$C$DW$T$107	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$107, DW_AT_language(DW_LANG_C)
$C$DW$894	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$894, DW_AT_type(*$C$DW$T$97)
$C$DW$895	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$895, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$107

$C$DW$T$108	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_address_class(0x16)
$C$DW$T$140	.dwtag  DW_TAG_typedef, DW_AT_name("SDOCallback_t")
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$140, DW_AT_language(DW_LANG_C)
$C$DW$T$109	.dwtag  DW_TAG_typedef, DW_AT_name("heartbeatError_t")
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_language(DW_LANG_C)
$C$DW$T$113	.dwtag  DW_TAG_typedef, DW_AT_name("post_SlaveBootup_t")
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$113, DW_AT_language(DW_LANG_C)

$C$DW$T$131	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$131, DW_AT_language(DW_LANG_C)
$C$DW$896	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$896, DW_AT_type(*$C$DW$T$97)
$C$DW$897	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$897, DW_AT_type(*$C$DW$T$6)
$C$DW$898	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$898, DW_AT_type(*$C$DW$T$9)
$C$DW$899	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$899, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$131

$C$DW$T$132	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$T$132, DW_AT_address_class(0x16)
$C$DW$T$133	.dwtag  DW_TAG_typedef, DW_AT_name("post_emcy_t")
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$T$133, DW_AT_language(DW_LANG_C)
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

$C$DW$T$184	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$184, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$184, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$184, DW_AT_byte_size(0x62)
$C$DW$900	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$900, DW_AT_upper_bound(0x61)
	.dwendtag $C$DW$T$184

$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$114	.dwtag  DW_TAG_typedef, DW_AT_name("CAN_PORT")
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$114, DW_AT_language(DW_LANG_C)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x08)
$C$DW$901	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$901, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x16)
$C$DW$902	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$902, DW_AT_type(*$C$DW$T$6)
$C$DW$T$86	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$902)
$C$DW$T$87	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$87, DW_AT_address_class(0x16)

$C$DW$T$198	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$198, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$198, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$198, DW_AT_byte_size(0x62)
$C$DW$903	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$903, DW_AT_upper_bound(0x61)
	.dwendtag $C$DW$T$198

$C$DW$T$134	.dwtag  DW_TAG_typedef, DW_AT_name("lss_transfer_t")
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$134, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x01)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x01)
$C$DW$T$105	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x16)

$C$DW$T$199	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$199, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$199, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$199, DW_AT_byte_size(0x13)
$C$DW$904	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$904, DW_AT_upper_bound(0x12)
	.dwendtag $C$DW$T$199

$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x01)
$C$DW$T$200	.dwtag  DW_TAG_typedef, DW_AT_name("Bool")
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$200, DW_AT_language(DW_LANG_C)
$C$DW$905	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$905, DW_AT_type(*$C$DW$T$9)
$C$DW$T$84	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$905)
$C$DW$T$85	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_address_class(0x16)
$C$DW$T$106	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$106, DW_AT_address_class(0x16)

$C$DW$T$209	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$209, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$209, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$209, DW_AT_byte_size(0x62)
$C$DW$906	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$906, DW_AT_upper_bound(0x61)
	.dwendtag $C$DW$T$209


$C$DW$T$210	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$210, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$210, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$210, DW_AT_byte_size(0x01)
$C$DW$907	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$907, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$210

$C$DW$T$68	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$68, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x01)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("Int")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x01)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("Uint16")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)

$C$DW$T$38	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$30)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x08)
$C$DW$908	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$908, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$38

$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("Uns")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x02)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x02)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("Uint32")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)

$C$DW$T$88	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$88, DW_AT_language(DW_LANG_C)
$C$DW$909	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$909, DW_AT_type(*$C$DW$T$6)
$C$DW$910	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$910, DW_AT_type(*$C$DW$T$3)
	.dwendtag $C$DW$T$88

$C$DW$T$89	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x16)
$C$DW$T$90	.dwtag  DW_TAG_typedef, DW_AT_name("valueRangeTest_t")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$90, DW_AT_language(DW_LANG_C)
$C$DW$T$104	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$104, DW_AT_address_class(0x16)

$C$DW$T$115	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$115, DW_AT_language(DW_LANG_C)
$C$DW$911	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$911, DW_AT_type(*$C$DW$T$97)
$C$DW$912	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$912, DW_AT_type(*$C$DW$T$78)
$C$DW$913	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$913, DW_AT_type(*$C$DW$T$6)
$C$DW$914	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$914, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$115

$C$DW$T$116	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_address_class(0x16)
$C$DW$T$117	.dwtag  DW_TAG_typedef, DW_AT_name("ODCallback_t")
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$116)
	.dwattr $C$DW$T$117, DW_AT_language(DW_LANG_C)
$C$DW$915	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$915, DW_AT_type(*$C$DW$T$117)
$C$DW$T$118	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$915)
$C$DW$T$119	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$119, DW_AT_address_class(0x16)
$C$DW$T$120	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$120, DW_AT_address_class(0x16)

$C$DW$T$124	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$124, DW_AT_language(DW_LANG_C)
$C$DW$916	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$916, DW_AT_type(*$C$DW$T$97)
$C$DW$917	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$917, DW_AT_type(*$C$DW$T$9)
$C$DW$918	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$918, DW_AT_type(*$C$DW$T$6)
	.dwendtag $C$DW$T$124

$C$DW$T$125	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_address_class(0x16)
$C$DW$T$126	.dwtag  DW_TAG_typedef, DW_AT_name("storeODSubIndex_t")
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$126, DW_AT_language(DW_LANG_C)
$C$DW$919	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$919, DW_AT_type(*$C$DW$T$13)
$C$DW$T$222	.dwtag  DW_TAG_volatile_type
	.dwattr $C$DW$T$222, DW_AT_type(*$C$DW$919)
$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$69	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$69, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x04)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x04)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x02)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x02)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x04)
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x16)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("String")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)

$C$DW$T$224	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$224, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$224, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$224, DW_AT_byte_size(0x10)
$C$DW$920	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$920, DW_AT_upper_bound(0x0f)
	.dwendtag $C$DW$T$224


$C$DW$T$70	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$70, DW_AT_name("crc_record")
	.dwattr $C$DW$T$70, DW_AT_byte_size(0x08)
$C$DW$921	.dwtag  DW_TAG_member
	.dwattr $C$DW$921, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$921, DW_AT_name("crc_alg_ID")
	.dwattr $C$DW$921, DW_AT_TI_symbol_name("_crc_alg_ID")
	.dwattr $C$DW$921, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$921, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$922	.dwtag  DW_TAG_member
	.dwattr $C$DW$922, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$922, DW_AT_name("page_id")
	.dwattr $C$DW$922, DW_AT_TI_symbol_name("_page_id")
	.dwattr $C$DW$922, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$922, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$923	.dwtag  DW_TAG_member
	.dwattr $C$DW$923, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$923, DW_AT_name("addr")
	.dwattr $C$DW$923, DW_AT_TI_symbol_name("_addr")
	.dwattr $C$DW$923, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$923, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$924	.dwtag  DW_TAG_member
	.dwattr $C$DW$924, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$924, DW_AT_name("size")
	.dwattr $C$DW$924, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$924, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$924, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$925	.dwtag  DW_TAG_member
	.dwattr $C$DW$925, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$925, DW_AT_name("crc_value")
	.dwattr $C$DW$925, DW_AT_TI_symbol_name("_crc_value")
	.dwattr $C$DW$925, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$925, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$70

$C$DW$T$71	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_RECORD")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$71, DW_AT_language(DW_LANG_C)

$C$DW$T$72	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$72, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$72, DW_AT_byte_size(0x08)
$C$DW$926	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$926, DW_AT_upper_bound(0x00)
	.dwendtag $C$DW$T$72


$C$DW$T$73	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$73, DW_AT_name("crc_table")
	.dwattr $C$DW$T$73, DW_AT_byte_size(0x0a)
$C$DW$927	.dwtag  DW_TAG_member
	.dwattr $C$DW$927, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$927, DW_AT_name("rec_size")
	.dwattr $C$DW$927, DW_AT_TI_symbol_name("_rec_size")
	.dwattr $C$DW$927, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$927, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$928	.dwtag  DW_TAG_member
	.dwattr $C$DW$928, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$928, DW_AT_name("num_recs")
	.dwattr $C$DW$928, DW_AT_TI_symbol_name("_num_recs")
	.dwattr $C$DW$928, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$928, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$929	.dwtag  DW_TAG_member
	.dwattr $C$DW$929, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$929, DW_AT_name("recs")
	.dwattr $C$DW$929, DW_AT_TI_symbol_name("_recs")
	.dwattr $C$DW$929, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$929, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$73

$C$DW$T$227	.dwtag  DW_TAG_typedef, DW_AT_name("CRC_TABLE")
	.dwattr $C$DW$T$227, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$227, DW_AT_language(DW_LANG_C)

$C$DW$T$127	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$127, DW_AT_name("enum_errorState")
	.dwattr $C$DW$T$127, DW_AT_byte_size(0x01)
$C$DW$930	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_free"), DW_AT_const_value(0x00)
$C$DW$931	.dwtag  DW_TAG_enumerator, DW_AT_name("Error_occurred"), DW_AT_const_value(0x01)
	.dwendtag $C$DW$T$127

$C$DW$T$128	.dwtag  DW_TAG_typedef, DW_AT_name("e_errorState")
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$128, DW_AT_language(DW_LANG_C)

$C$DW$T$93	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$93, DW_AT_name("enum_nodeState")
	.dwattr $C$DW$T$93, DW_AT_byte_size(0x01)
$C$DW$932	.dwtag  DW_TAG_enumerator, DW_AT_name("Initialisation"), DW_AT_const_value(0x00)
$C$DW$933	.dwtag  DW_TAG_enumerator, DW_AT_name("Disconnected"), DW_AT_const_value(0x01)
$C$DW$934	.dwtag  DW_TAG_enumerator, DW_AT_name("Connecting"), DW_AT_const_value(0x02)
$C$DW$935	.dwtag  DW_TAG_enumerator, DW_AT_name("Preparing"), DW_AT_const_value(0x02)
$C$DW$936	.dwtag  DW_TAG_enumerator, DW_AT_name("Stopped"), DW_AT_const_value(0x04)
$C$DW$937	.dwtag  DW_TAG_enumerator, DW_AT_name("Operational"), DW_AT_const_value(0x05)
$C$DW$938	.dwtag  DW_TAG_enumerator, DW_AT_name("Pre_operational"), DW_AT_const_value(0x7f)
$C$DW$939	.dwtag  DW_TAG_enumerator, DW_AT_name("Unknown_state"), DW_AT_const_value(0x0f)
	.dwendtag $C$DW$T$93

$C$DW$T$94	.dwtag  DW_TAG_typedef, DW_AT_name("e_nodeState")
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$94, DW_AT_language(DW_LANG_C)

$C$DW$T$110	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$110, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$110, DW_AT_byte_size(0x80)
$C$DW$940	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$940, DW_AT_upper_bound(0x7f)
	.dwendtag $C$DW$T$110


$C$DW$T$74	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$74, DW_AT_name("s_quick_index")
	.dwattr $C$DW$T$74, DW_AT_byte_size(0x06)
$C$DW$941	.dwtag  DW_TAG_member
	.dwattr $C$DW$941, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$941, DW_AT_name("SDO_SVR")
	.dwattr $C$DW$941, DW_AT_TI_symbol_name("_SDO_SVR")
	.dwattr $C$DW$941, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$941, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$942	.dwtag  DW_TAG_member
	.dwattr $C$DW$942, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$942, DW_AT_name("SDO_CLT")
	.dwattr $C$DW$942, DW_AT_TI_symbol_name("_SDO_CLT")
	.dwattr $C$DW$942, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$942, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$943	.dwtag  DW_TAG_member
	.dwattr $C$DW$943, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$943, DW_AT_name("PDO_RCV")
	.dwattr $C$DW$943, DW_AT_TI_symbol_name("_PDO_RCV")
	.dwattr $C$DW$943, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$943, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$944	.dwtag  DW_TAG_member
	.dwattr $C$DW$944, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$944, DW_AT_name("PDO_RCV_MAP")
	.dwattr $C$DW$944, DW_AT_TI_symbol_name("_PDO_RCV_MAP")
	.dwattr $C$DW$944, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$944, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$945	.dwtag  DW_TAG_member
	.dwattr $C$DW$945, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$945, DW_AT_name("PDO_TRS")
	.dwattr $C$DW$945, DW_AT_TI_symbol_name("_PDO_TRS")
	.dwattr $C$DW$945, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$945, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$946	.dwtag  DW_TAG_member
	.dwattr $C$DW$946, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$946, DW_AT_name("PDO_TRS_MAP")
	.dwattr $C$DW$946, DW_AT_TI_symbol_name("_PDO_TRS_MAP")
	.dwattr $C$DW$946, DW_AT_data_member_location[DW_OP_plus_uconst 0x5]
	.dwattr $C$DW$946, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$74

$C$DW$T$81	.dwtag  DW_TAG_typedef, DW_AT_name("quick_index")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$81, DW_AT_language(DW_LANG_C)
$C$DW$947	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$947, DW_AT_type(*$C$DW$T$81)
$C$DW$T$82	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$947)
$C$DW$T$83	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_address_class(0x16)

$C$DW$T$137	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$137, DW_AT_name("struct_CO_Data")
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x132)
$C$DW$948	.dwtag  DW_TAG_member
	.dwattr $C$DW$948, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$948, DW_AT_name("bDeviceNodeId")
	.dwattr $C$DW$948, DW_AT_TI_symbol_name("_bDeviceNodeId")
	.dwattr $C$DW$948, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$948, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$949	.dwtag  DW_TAG_member
	.dwattr $C$DW$949, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$949, DW_AT_name("objdict")
	.dwattr $C$DW$949, DW_AT_TI_symbol_name("_objdict")
	.dwattr $C$DW$949, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$949, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$950	.dwtag  DW_TAG_member
	.dwattr $C$DW$950, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$950, DW_AT_name("PDO_status")
	.dwattr $C$DW$950, DW_AT_TI_symbol_name("_PDO_status")
	.dwattr $C$DW$950, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$950, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$951	.dwtag  DW_TAG_member
	.dwattr $C$DW$951, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$951, DW_AT_name("firstIndex")
	.dwattr $C$DW$951, DW_AT_TI_symbol_name("_firstIndex")
	.dwattr $C$DW$951, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$951, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$952	.dwtag  DW_TAG_member
	.dwattr $C$DW$952, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$952, DW_AT_name("lastIndex")
	.dwattr $C$DW$952, DW_AT_TI_symbol_name("_lastIndex")
	.dwattr $C$DW$952, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$952, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$953	.dwtag  DW_TAG_member
	.dwattr $C$DW$953, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$953, DW_AT_name("ObjdictSize")
	.dwattr $C$DW$953, DW_AT_TI_symbol_name("_ObjdictSize")
	.dwattr $C$DW$953, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$953, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$954	.dwtag  DW_TAG_member
	.dwattr $C$DW$954, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$954, DW_AT_name("iam_a_slave")
	.dwattr $C$DW$954, DW_AT_TI_symbol_name("_iam_a_slave")
	.dwattr $C$DW$954, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$954, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$955	.dwtag  DW_TAG_member
	.dwattr $C$DW$955, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$955, DW_AT_name("valueRangeTest")
	.dwattr $C$DW$955, DW_AT_TI_symbol_name("_valueRangeTest")
	.dwattr $C$DW$955, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$955, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$956	.dwtag  DW_TAG_member
	.dwattr $C$DW$956, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$956, DW_AT_name("transfers")
	.dwattr $C$DW$956, DW_AT_TI_symbol_name("_transfers")
	.dwattr $C$DW$956, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$956, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$957	.dwtag  DW_TAG_member
	.dwattr $C$DW$957, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$957, DW_AT_name("nodeState")
	.dwattr $C$DW$957, DW_AT_TI_symbol_name("_nodeState")
	.dwattr $C$DW$957, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$957, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$958	.dwtag  DW_TAG_member
	.dwattr $C$DW$958, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$958, DW_AT_name("CurrentCommunicationState")
	.dwattr $C$DW$958, DW_AT_TI_symbol_name("_CurrentCommunicationState")
	.dwattr $C$DW$958, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d]
	.dwattr $C$DW$958, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$959	.dwtag  DW_TAG_member
	.dwattr $C$DW$959, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$959, DW_AT_name("initialisation")
	.dwattr $C$DW$959, DW_AT_TI_symbol_name("_initialisation")
	.dwattr $C$DW$959, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$959, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$960	.dwtag  DW_TAG_member
	.dwattr $C$DW$960, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$960, DW_AT_name("preOperational")
	.dwattr $C$DW$960, DW_AT_TI_symbol_name("_preOperational")
	.dwattr $C$DW$960, DW_AT_data_member_location[DW_OP_plus_uconst 0x56]
	.dwattr $C$DW$960, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$961	.dwtag  DW_TAG_member
	.dwattr $C$DW$961, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$961, DW_AT_name("operational")
	.dwattr $C$DW$961, DW_AT_TI_symbol_name("_operational")
	.dwattr $C$DW$961, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$961, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$962	.dwtag  DW_TAG_member
	.dwattr $C$DW$962, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$962, DW_AT_name("stopped")
	.dwattr $C$DW$962, DW_AT_TI_symbol_name("_stopped")
	.dwattr $C$DW$962, DW_AT_data_member_location[DW_OP_plus_uconst 0x5a]
	.dwattr $C$DW$962, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$963	.dwtag  DW_TAG_member
	.dwattr $C$DW$963, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$963, DW_AT_name("NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$963, DW_AT_TI_symbol_name("_NMT_Slave_Node_Reset_Callback")
	.dwattr $C$DW$963, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$963, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$964	.dwtag  DW_TAG_member
	.dwattr $C$DW$964, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$964, DW_AT_name("NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$964, DW_AT_TI_symbol_name("_NMT_Slave_Communications_Reset_Callback")
	.dwattr $C$DW$964, DW_AT_data_member_location[DW_OP_plus_uconst 0x5e]
	.dwattr $C$DW$964, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$965	.dwtag  DW_TAG_member
	.dwattr $C$DW$965, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$965, DW_AT_name("ConsumerHeartbeatCount")
	.dwattr $C$DW$965, DW_AT_TI_symbol_name("_ConsumerHeartbeatCount")
	.dwattr $C$DW$965, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$965, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$966	.dwtag  DW_TAG_member
	.dwattr $C$DW$966, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$966, DW_AT_name("ConsumerHeartbeatEntries")
	.dwattr $C$DW$966, DW_AT_TI_symbol_name("_ConsumerHeartbeatEntries")
	.dwattr $C$DW$966, DW_AT_data_member_location[DW_OP_plus_uconst 0x62]
	.dwattr $C$DW$966, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$967	.dwtag  DW_TAG_member
	.dwattr $C$DW$967, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$967, DW_AT_name("ConsumerHeartBeatTimers")
	.dwattr $C$DW$967, DW_AT_TI_symbol_name("_ConsumerHeartBeatTimers")
	.dwattr $C$DW$967, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$967, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$968	.dwtag  DW_TAG_member
	.dwattr $C$DW$968, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$968, DW_AT_name("ProducerHeartBeatTime")
	.dwattr $C$DW$968, DW_AT_TI_symbol_name("_ProducerHeartBeatTime")
	.dwattr $C$DW$968, DW_AT_data_member_location[DW_OP_plus_uconst 0x66]
	.dwattr $C$DW$968, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$969	.dwtag  DW_TAG_member
	.dwattr $C$DW$969, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$969, DW_AT_name("ProducerHeartBeatTimer")
	.dwattr $C$DW$969, DW_AT_TI_symbol_name("_ProducerHeartBeatTimer")
	.dwattr $C$DW$969, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$969, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$970	.dwtag  DW_TAG_member
	.dwattr $C$DW$970, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$970, DW_AT_name("heartbeatError")
	.dwattr $C$DW$970, DW_AT_TI_symbol_name("_heartbeatError")
	.dwattr $C$DW$970, DW_AT_data_member_location[DW_OP_plus_uconst 0x6a]
	.dwattr $C$DW$970, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$971	.dwtag  DW_TAG_member
	.dwattr $C$DW$971, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$971, DW_AT_name("NMTable")
	.dwattr $C$DW$971, DW_AT_TI_symbol_name("_NMTable")
	.dwattr $C$DW$971, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$971, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$972	.dwtag  DW_TAG_member
	.dwattr $C$DW$972, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$972, DW_AT_name("syncTimer")
	.dwattr $C$DW$972, DW_AT_TI_symbol_name("_syncTimer")
	.dwattr $C$DW$972, DW_AT_data_member_location[DW_OP_plus_uconst 0xec]
	.dwattr $C$DW$972, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$973	.dwtag  DW_TAG_member
	.dwattr $C$DW$973, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$973, DW_AT_name("COB_ID_Sync")
	.dwattr $C$DW$973, DW_AT_TI_symbol_name("_COB_ID_Sync")
	.dwattr $C$DW$973, DW_AT_data_member_location[DW_OP_plus_uconst 0xee]
	.dwattr $C$DW$973, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$974	.dwtag  DW_TAG_member
	.dwattr $C$DW$974, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$974, DW_AT_name("Sync_Cycle_Period")
	.dwattr $C$DW$974, DW_AT_TI_symbol_name("_Sync_Cycle_Period")
	.dwattr $C$DW$974, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$974, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$975	.dwtag  DW_TAG_member
	.dwattr $C$DW$975, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$975, DW_AT_name("pre_sync")
	.dwattr $C$DW$975, DW_AT_TI_symbol_name("_pre_sync")
	.dwattr $C$DW$975, DW_AT_data_member_location[DW_OP_plus_uconst 0xf2]
	.dwattr $C$DW$975, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$976	.dwtag  DW_TAG_member
	.dwattr $C$DW$976, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$976, DW_AT_name("post_TPDO")
	.dwattr $C$DW$976, DW_AT_TI_symbol_name("_post_TPDO")
	.dwattr $C$DW$976, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$976, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$977	.dwtag  DW_TAG_member
	.dwattr $C$DW$977, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$977, DW_AT_name("post_SlaveBootup")
	.dwattr $C$DW$977, DW_AT_TI_symbol_name("_post_SlaveBootup")
	.dwattr $C$DW$977, DW_AT_data_member_location[DW_OP_plus_uconst 0xf6]
	.dwattr $C$DW$977, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$978	.dwtag  DW_TAG_member
	.dwattr $C$DW$978, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$978, DW_AT_name("toggle")
	.dwattr $C$DW$978, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$978, DW_AT_data_member_location[DW_OP_plus_uconst 0xf8]
	.dwattr $C$DW$978, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$979	.dwtag  DW_TAG_member
	.dwattr $C$DW$979, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$979, DW_AT_name("canHandle")
	.dwattr $C$DW$979, DW_AT_TI_symbol_name("_canHandle")
	.dwattr $C$DW$979, DW_AT_data_member_location[DW_OP_plus_uconst 0xf9]
	.dwattr $C$DW$979, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$980	.dwtag  DW_TAG_member
	.dwattr $C$DW$980, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$980, DW_AT_name("scanIndexOD")
	.dwattr $C$DW$980, DW_AT_TI_symbol_name("_scanIndexOD")
	.dwattr $C$DW$980, DW_AT_data_member_location[DW_OP_plus_uconst 0xfa]
	.dwattr $C$DW$980, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$981	.dwtag  DW_TAG_member
	.dwattr $C$DW$981, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$981, DW_AT_name("storeODSubIndex")
	.dwattr $C$DW$981, DW_AT_TI_symbol_name("_storeODSubIndex")
	.dwattr $C$DW$981, DW_AT_data_member_location[DW_OP_plus_uconst 0xfc]
	.dwattr $C$DW$981, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$982	.dwtag  DW_TAG_member
	.dwattr $C$DW$982, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$982, DW_AT_name("globalCallback")
	.dwattr $C$DW$982, DW_AT_TI_symbol_name("_globalCallback")
	.dwattr $C$DW$982, DW_AT_data_member_location[DW_OP_plus_uconst 0xfe]
	.dwattr $C$DW$982, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$983	.dwtag  DW_TAG_member
	.dwattr $C$DW$983, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$983, DW_AT_name("dcf_odentry")
	.dwattr $C$DW$983, DW_AT_TI_symbol_name("_dcf_odentry")
	.dwattr $C$DW$983, DW_AT_data_member_location[DW_OP_plus_uconst 0x100]
	.dwattr $C$DW$983, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$984	.dwtag  DW_TAG_member
	.dwattr $C$DW$984, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$984, DW_AT_name("dcf_cursor")
	.dwattr $C$DW$984, DW_AT_TI_symbol_name("_dcf_cursor")
	.dwattr $C$DW$984, DW_AT_data_member_location[DW_OP_plus_uconst 0x102]
	.dwattr $C$DW$984, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$985	.dwtag  DW_TAG_member
	.dwattr $C$DW$985, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$985, DW_AT_name("dcf_entries_count")
	.dwattr $C$DW$985, DW_AT_TI_symbol_name("_dcf_entries_count")
	.dwattr $C$DW$985, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$985, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$986	.dwtag  DW_TAG_member
	.dwattr $C$DW$986, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$986, DW_AT_name("dcf_request")
	.dwattr $C$DW$986, DW_AT_TI_symbol_name("_dcf_request")
	.dwattr $C$DW$986, DW_AT_data_member_location[DW_OP_plus_uconst 0x106]
	.dwattr $C$DW$986, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$987	.dwtag  DW_TAG_member
	.dwattr $C$DW$987, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$987, DW_AT_name("error_state")
	.dwattr $C$DW$987, DW_AT_TI_symbol_name("_error_state")
	.dwattr $C$DW$987, DW_AT_data_member_location[DW_OP_plus_uconst 0x107]
	.dwattr $C$DW$987, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$988	.dwtag  DW_TAG_member
	.dwattr $C$DW$988, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$988, DW_AT_name("error_history_size")
	.dwattr $C$DW$988, DW_AT_TI_symbol_name("_error_history_size")
	.dwattr $C$DW$988, DW_AT_data_member_location[DW_OP_plus_uconst 0x108]
	.dwattr $C$DW$988, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$989	.dwtag  DW_TAG_member
	.dwattr $C$DW$989, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$989, DW_AT_name("error_number")
	.dwattr $C$DW$989, DW_AT_TI_symbol_name("_error_number")
	.dwattr $C$DW$989, DW_AT_data_member_location[DW_OP_plus_uconst 0x10a]
	.dwattr $C$DW$989, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$990	.dwtag  DW_TAG_member
	.dwattr $C$DW$990, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$990, DW_AT_name("error_first_element")
	.dwattr $C$DW$990, DW_AT_TI_symbol_name("_error_first_element")
	.dwattr $C$DW$990, DW_AT_data_member_location[DW_OP_plus_uconst 0x10c]
	.dwattr $C$DW$990, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$991	.dwtag  DW_TAG_member
	.dwattr $C$DW$991, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$991, DW_AT_name("error_register")
	.dwattr $C$DW$991, DW_AT_TI_symbol_name("_error_register")
	.dwattr $C$DW$991, DW_AT_data_member_location[DW_OP_plus_uconst 0x10e]
	.dwattr $C$DW$991, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$992	.dwtag  DW_TAG_member
	.dwattr $C$DW$992, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$992, DW_AT_name("error_cobid")
	.dwattr $C$DW$992, DW_AT_TI_symbol_name("_error_cobid")
	.dwattr $C$DW$992, DW_AT_data_member_location[DW_OP_plus_uconst 0x110]
	.dwattr $C$DW$992, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$993	.dwtag  DW_TAG_member
	.dwattr $C$DW$993, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$993, DW_AT_name("error_data")
	.dwattr $C$DW$993, DW_AT_TI_symbol_name("_error_data")
	.dwattr $C$DW$993, DW_AT_data_member_location[DW_OP_plus_uconst 0x112]
	.dwattr $C$DW$993, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$994	.dwtag  DW_TAG_member
	.dwattr $C$DW$994, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$994, DW_AT_name("post_emcy")
	.dwattr $C$DW$994, DW_AT_TI_symbol_name("_post_emcy")
	.dwattr $C$DW$994, DW_AT_data_member_location[DW_OP_plus_uconst 0x12a]
	.dwattr $C$DW$994, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$995	.dwtag  DW_TAG_member
	.dwattr $C$DW$995, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$995, DW_AT_name("lss_transfer")
	.dwattr $C$DW$995, DW_AT_TI_symbol_name("_lss_transfer")
	.dwattr $C$DW$995, DW_AT_data_member_location[DW_OP_plus_uconst 0x12c]
	.dwattr $C$DW$995, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$996	.dwtag  DW_TAG_member
	.dwattr $C$DW$996, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$996, DW_AT_name("eeprom_index")
	.dwattr $C$DW$996, DW_AT_TI_symbol_name("_eeprom_index")
	.dwattr $C$DW$996, DW_AT_data_member_location[DW_OP_plus_uconst 0x12e]
	.dwattr $C$DW$996, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$997	.dwtag  DW_TAG_member
	.dwattr $C$DW$997, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$997, DW_AT_name("eeprom_size")
	.dwattr $C$DW$997, DW_AT_TI_symbol_name("_eeprom_size")
	.dwattr $C$DW$997, DW_AT_data_member_location[DW_OP_plus_uconst 0x130]
	.dwattr $C$DW$997, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$137

$C$DW$T$96	.dwtag  DW_TAG_typedef, DW_AT_name("CO_Data")
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$96, DW_AT_language(DW_LANG_C)
$C$DW$T$97	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$97, DW_AT_address_class(0x16)

$C$DW$T$139	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$139, DW_AT_name("struct_s_PDO_status")
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x0e)
$C$DW$998	.dwtag  DW_TAG_member
	.dwattr $C$DW$998, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$998, DW_AT_name("transmit_type_parameter")
	.dwattr $C$DW$998, DW_AT_TI_symbol_name("_transmit_type_parameter")
	.dwattr $C$DW$998, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$998, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$999	.dwtag  DW_TAG_member
	.dwattr $C$DW$999, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$999, DW_AT_name("event_timer")
	.dwattr $C$DW$999, DW_AT_TI_symbol_name("_event_timer")
	.dwattr $C$DW$999, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$999, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1000	.dwtag  DW_TAG_member
	.dwattr $C$DW$1000, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1000, DW_AT_name("inhibit_timer")
	.dwattr $C$DW$1000, DW_AT_TI_symbol_name("_inhibit_timer")
	.dwattr $C$DW$1000, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1000, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1001	.dwtag  DW_TAG_member
	.dwattr $C$DW$1001, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$1001, DW_AT_name("last_message")
	.dwattr $C$DW$1001, DW_AT_TI_symbol_name("_last_message")
	.dwattr $C$DW$1001, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1001, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$139

$C$DW$T$79	.dwtag  DW_TAG_typedef, DW_AT_name("s_PDO_status")
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$T$79, DW_AT_language(DW_LANG_C)
$C$DW$T$80	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x16)

$C$DW$T$141	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$141, DW_AT_name("struct_s_transfer")
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x14)
$C$DW$1002	.dwtag  DW_TAG_member
	.dwattr $C$DW$1002, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1002, DW_AT_name("nodeId")
	.dwattr $C$DW$1002, DW_AT_TI_symbol_name("_nodeId")
	.dwattr $C$DW$1002, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1002, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1003	.dwtag  DW_TAG_member
	.dwattr $C$DW$1003, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1003, DW_AT_name("whoami")
	.dwattr $C$DW$1003, DW_AT_TI_symbol_name("_whoami")
	.dwattr $C$DW$1003, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1003, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1004	.dwtag  DW_TAG_member
	.dwattr $C$DW$1004, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1004, DW_AT_name("state")
	.dwattr $C$DW$1004, DW_AT_TI_symbol_name("_state")
	.dwattr $C$DW$1004, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1004, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1005	.dwtag  DW_TAG_member
	.dwattr $C$DW$1005, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1005, DW_AT_name("toggle")
	.dwattr $C$DW$1005, DW_AT_TI_symbol_name("_toggle")
	.dwattr $C$DW$1005, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1005, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1006	.dwtag  DW_TAG_member
	.dwattr $C$DW$1006, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1006, DW_AT_name("abortCode")
	.dwattr $C$DW$1006, DW_AT_TI_symbol_name("_abortCode")
	.dwattr $C$DW$1006, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1006, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1007	.dwtag  DW_TAG_member
	.dwattr $C$DW$1007, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1007, DW_AT_name("index")
	.dwattr $C$DW$1007, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$1007, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1007, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1008	.dwtag  DW_TAG_member
	.dwattr $C$DW$1008, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1008, DW_AT_name("subIndex")
	.dwattr $C$DW$1008, DW_AT_TI_symbol_name("_subIndex")
	.dwattr $C$DW$1008, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$1008, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1009	.dwtag  DW_TAG_member
	.dwattr $C$DW$1009, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1009, DW_AT_name("port")
	.dwattr $C$DW$1009, DW_AT_TI_symbol_name("_port")
	.dwattr $C$DW$1009, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1009, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1010	.dwtag  DW_TAG_member
	.dwattr $C$DW$1010, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1010, DW_AT_name("count")
	.dwattr $C$DW$1010, DW_AT_TI_symbol_name("_count")
	.dwattr $C$DW$1010, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$1010, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1011	.dwtag  DW_TAG_member
	.dwattr $C$DW$1011, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1011, DW_AT_name("offset")
	.dwattr $C$DW$1011, DW_AT_TI_symbol_name("_offset")
	.dwattr $C$DW$1011, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1011, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1012	.dwtag  DW_TAG_member
	.dwattr $C$DW$1012, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$1012, DW_AT_name("datap")
	.dwattr $C$DW$1012, DW_AT_TI_symbol_name("_datap")
	.dwattr $C$DW$1012, DW_AT_data_member_location[DW_OP_plus_uconst 0xe]
	.dwattr $C$DW$1012, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1013	.dwtag  DW_TAG_member
	.dwattr $C$DW$1013, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1013, DW_AT_name("dataType")
	.dwattr $C$DW$1013, DW_AT_TI_symbol_name("_dataType")
	.dwattr $C$DW$1013, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1013, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1014	.dwtag  DW_TAG_member
	.dwattr $C$DW$1014, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$1014, DW_AT_name("timer")
	.dwattr $C$DW$1014, DW_AT_TI_symbol_name("_timer")
	.dwattr $C$DW$1014, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$1014, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1015	.dwtag  DW_TAG_member
	.dwattr $C$DW$1015, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$1015, DW_AT_name("Callback")
	.dwattr $C$DW$1015, DW_AT_TI_symbol_name("_Callback")
	.dwattr $C$DW$1015, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$1015, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$141

$C$DW$T$91	.dwtag  DW_TAG_typedef, DW_AT_name("s_transfer")
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$91, DW_AT_language(DW_LANG_C)

$C$DW$T$92	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$92, DW_AT_byte_size(0x3c)
$C$DW$1016	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1016, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$92


$C$DW$T$145	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$145, DW_AT_name("td_indextable")
	.dwattr $C$DW$T$145, DW_AT_byte_size(0x04)
$C$DW$1017	.dwtag  DW_TAG_member
	.dwattr $C$DW$1017, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$1017, DW_AT_name("pSubindex")
	.dwattr $C$DW$1017, DW_AT_TI_symbol_name("_pSubindex")
	.dwattr $C$DW$1017, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1017, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1018	.dwtag  DW_TAG_member
	.dwattr $C$DW$1018, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1018, DW_AT_name("bSubCount")
	.dwattr $C$DW$1018, DW_AT_TI_symbol_name("_bSubCount")
	.dwattr $C$DW$1018, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1018, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1019	.dwtag  DW_TAG_member
	.dwattr $C$DW$1019, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$1019, DW_AT_name("index")
	.dwattr $C$DW$1019, DW_AT_TI_symbol_name("_index")
	.dwattr $C$DW$1019, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$1019, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$145

$C$DW$T$76	.dwtag  DW_TAG_typedef, DW_AT_name("indextable")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$T$76, DW_AT_language(DW_LANG_C)
$C$DW$1020	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1020, DW_AT_type(*$C$DW$T$76)
$C$DW$T$77	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$1020)
$C$DW$T$78	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_address_class(0x16)

$C$DW$T$121	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$T$121, DW_AT_language(DW_LANG_C)
$C$DW$1021	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1021, DW_AT_type(*$C$DW$T$9)
$C$DW$1022	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1022, DW_AT_type(*$C$DW$T$104)
$C$DW$1023	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1023, DW_AT_type(*$C$DW$T$120)
	.dwendtag $C$DW$T$121

$C$DW$T$122	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$122, DW_AT_address_class(0x16)
$C$DW$T$123	.dwtag  DW_TAG_typedef, DW_AT_name("scanIndexOD_t")
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_language(DW_LANG_C)

$C$DW$T$146	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$146, DW_AT_name("td_subindex")
	.dwattr $C$DW$T$146, DW_AT_byte_size(0x08)
$C$DW$1024	.dwtag  DW_TAG_member
	.dwattr $C$DW$1024, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1024, DW_AT_name("bAccessType")
	.dwattr $C$DW$1024, DW_AT_TI_symbol_name("_bAccessType")
	.dwattr $C$DW$1024, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$1024, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1025	.dwtag  DW_TAG_member
	.dwattr $C$DW$1025, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1025, DW_AT_name("bDataType")
	.dwattr $C$DW$1025, DW_AT_TI_symbol_name("_bDataType")
	.dwattr $C$DW$1025, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$1025, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1026	.dwtag  DW_TAG_member
	.dwattr $C$DW$1026, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$1026, DW_AT_name("size")
	.dwattr $C$DW$1026, DW_AT_TI_symbol_name("_size")
	.dwattr $C$DW$1026, DW_AT_data_member_location[DW_OP_plus_uconst 0x2]
	.dwattr $C$DW$1026, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1027	.dwtag  DW_TAG_member
	.dwattr $C$DW$1027, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1027, DW_AT_name("pObject")
	.dwattr $C$DW$1027, DW_AT_TI_symbol_name("_pObject")
	.dwattr $C$DW$1027, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1027, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1028	.dwtag  DW_TAG_member
	.dwattr $C$DW$1028, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$1028, DW_AT_name("bProcessor")
	.dwattr $C$DW$1028, DW_AT_TI_symbol_name("_bProcessor")
	.dwattr $C$DW$1028, DW_AT_data_member_location[DW_OP_plus_uconst 0x6]
	.dwattr $C$DW$1028, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$146

$C$DW$1029	.dwtag  DW_TAG_TI_far_type
	.dwattr $C$DW$1029, DW_AT_type(*$C$DW$T$146)
$C$DW$T$142	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$1029)
$C$DW$T$143	.dwtag  DW_TAG_typedef, DW_AT_name("subindex")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$T$143, DW_AT_language(DW_LANG_C)
$C$DW$T$144	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$T$144, DW_AT_address_class(0x16)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 78
	.dwcfi	cfa_register, 20
	.dwcfi	cfa_offset, 0
	.dwcfi	undefined, 0
	.dwcfi	undefined, 1
	.dwcfi	undefined, 2
	.dwcfi	undefined, 3
	.dwcfi	undefined, 20
	.dwcfi	undefined, 21
	.dwcfi	undefined, 22
	.dwcfi	undefined, 23
	.dwcfi	undefined, 24
	.dwcfi	undefined, 25
	.dwcfi	undefined, 26
	.dwcfi	same_value, 28
	.dwcfi	undefined, 29
	.dwcfi	undefined, 30
	.dwcfi	undefined, 31
	.dwcfi	undefined, 32
	.dwcfi	undefined, 33
	.dwcfi	undefined, 34
	.dwcfi	undefined, 35
	.dwcfi	undefined, 36
	.dwcfi	undefined, 37
	.dwcfi	undefined, 38
	.dwcfi	undefined, 75
	.dwcfi	undefined, 76
	.dwcfi	undefined, 77
	.dwcfi	undefined, 4
	.dwcfi	same_value, 6
	.dwcfi	same_value, 8
	.dwcfi	same_value, 10
	.dwcfi	undefined, 12
	.dwcfi	undefined, 14
	.dwcfi	undefined, 16
	.dwcfi	undefined, 17
	.dwcfi	undefined, 18
	.dwcfi	undefined, 19
	.dwcfi	undefined, 5
	.dwcfi	same_value, 7
	.dwcfi	same_value, 9
	.dwcfi	same_value, 11
	.dwcfi	undefined, 13
	.dwcfi	undefined, 15
	.dwcfi	undefined, 43
	.dwcfi	undefined, 44
	.dwcfi	undefined, 47
	.dwcfi	undefined, 48
	.dwcfi	undefined, 51
	.dwcfi	undefined, 52
	.dwcfi	undefined, 55
	.dwcfi	undefined, 56
	.dwcfi	same_value, 59
	.dwcfi	same_value, 60
	.dwcfi	same_value, 63
	.dwcfi	same_value, 64
	.dwcfi	same_value, 67
	.dwcfi	same_value, 68
	.dwcfi	same_value, 71
	.dwcfi	same_value, 72
	.dwcfi	undefined, 73
	.dwcfi	undefined, 74
	.dwcfi	undefined, 39
	.dwcfi	undefined, 40
	.dwcfi	undefined, 27
	.dwcfi	undefined, 78
	.dwendentry

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$1030	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AL")
	.dwattr $C$DW$1030, DW_AT_location[DW_OP_reg0]
$C$DW$1031	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AH")
	.dwattr $C$DW$1031, DW_AT_location[DW_OP_reg1]
$C$DW$1032	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PL")
	.dwattr $C$DW$1032, DW_AT_location[DW_OP_reg2]
$C$DW$1033	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PH")
	.dwattr $C$DW$1033, DW_AT_location[DW_OP_reg3]
$C$DW$1034	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$1034, DW_AT_location[DW_OP_reg20]
$C$DW$1035	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XT")
	.dwattr $C$DW$1035, DW_AT_location[DW_OP_reg21]
$C$DW$1036	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("T")
	.dwattr $C$DW$1036, DW_AT_location[DW_OP_reg22]
$C$DW$1037	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST0")
	.dwattr $C$DW$1037, DW_AT_location[DW_OP_reg23]
$C$DW$1038	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ST1")
	.dwattr $C$DW$1038, DW_AT_location[DW_OP_reg24]
$C$DW$1039	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$1039, DW_AT_location[DW_OP_reg25]
$C$DW$1040	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RPC")
	.dwattr $C$DW$1040, DW_AT_location[DW_OP_reg26]
$C$DW$1041	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$1041, DW_AT_location[DW_OP_reg28]
$C$DW$1042	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$1042, DW_AT_location[DW_OP_reg29]
$C$DW$1043	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SXM")
	.dwattr $C$DW$1043, DW_AT_location[DW_OP_reg30]
$C$DW$1044	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PM")
	.dwattr $C$DW$1044, DW_AT_location[DW_OP_reg31]
$C$DW$1045	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVM")
	.dwattr $C$DW$1045, DW_AT_location[DW_OP_regx 0x20]
$C$DW$1046	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PAGE0")
	.dwattr $C$DW$1046, DW_AT_location[DW_OP_regx 0x21]
$C$DW$1047	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMODE")
	.dwattr $C$DW$1047, DW_AT_location[DW_OP_regx 0x22]
$C$DW$1048	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("INTM")
	.dwattr $C$DW$1048, DW_AT_location[DW_OP_regx 0x23]
$C$DW$1049	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$1049, DW_AT_location[DW_OP_regx 0x24]
$C$DW$1050	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$1050, DW_AT_location[DW_OP_regx 0x25]
$C$DW$1051	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("V")
	.dwattr $C$DW$1051, DW_AT_location[DW_OP_regx 0x26]
$C$DW$1052	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PSEUDOH")
	.dwattr $C$DW$1052, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$1053	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("VOL")
	.dwattr $C$DW$1053, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$1054	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR0")
	.dwattr $C$DW$1054, DW_AT_location[DW_OP_reg4]
$C$DW$1055	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR1")
	.dwattr $C$DW$1055, DW_AT_location[DW_OP_reg6]
$C$DW$1056	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR2")
	.dwattr $C$DW$1056, DW_AT_location[DW_OP_reg8]
$C$DW$1057	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR3")
	.dwattr $C$DW$1057, DW_AT_location[DW_OP_reg10]
$C$DW$1058	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR4")
	.dwattr $C$DW$1058, DW_AT_location[DW_OP_reg12]
$C$DW$1059	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR5")
	.dwattr $C$DW$1059, DW_AT_location[DW_OP_reg14]
$C$DW$1060	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR6")
	.dwattr $C$DW$1060, DW_AT_location[DW_OP_reg16]
$C$DW$1061	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR6")
	.dwattr $C$DW$1061, DW_AT_location[DW_OP_reg17]
$C$DW$1062	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AR7")
	.dwattr $C$DW$1062, DW_AT_location[DW_OP_reg18]
$C$DW$1063	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR7")
	.dwattr $C$DW$1063, DW_AT_location[DW_OP_reg19]
$C$DW$1064	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR0")
	.dwattr $C$DW$1064, DW_AT_location[DW_OP_reg5]
$C$DW$1065	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR1")
	.dwattr $C$DW$1065, DW_AT_location[DW_OP_reg7]
$C$DW$1066	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR2")
	.dwattr $C$DW$1066, DW_AT_location[DW_OP_reg9]
$C$DW$1067	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR3")
	.dwattr $C$DW$1067, DW_AT_location[DW_OP_reg11]
$C$DW$1068	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR4")
	.dwattr $C$DW$1068, DW_AT_location[DW_OP_reg13]
$C$DW$1069	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("XAR5")
	.dwattr $C$DW$1069, DW_AT_location[DW_OP_reg15]
$C$DW$1070	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0HL")
	.dwattr $C$DW$1070, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$1071	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R0H")
	.dwattr $C$DW$1071, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$1072	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1HL")
	.dwattr $C$DW$1072, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$1073	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R1H")
	.dwattr $C$DW$1073, DW_AT_location[DW_OP_regx 0x30]
$C$DW$1074	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2HL")
	.dwattr $C$DW$1074, DW_AT_location[DW_OP_regx 0x33]
$C$DW$1075	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R2H")
	.dwattr $C$DW$1075, DW_AT_location[DW_OP_regx 0x34]
$C$DW$1076	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3HL")
	.dwattr $C$DW$1076, DW_AT_location[DW_OP_regx 0x37]
$C$DW$1077	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R3H")
	.dwattr $C$DW$1077, DW_AT_location[DW_OP_regx 0x38]
$C$DW$1078	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4HL")
	.dwattr $C$DW$1078, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$1079	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R4H")
	.dwattr $C$DW$1079, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$1080	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5HL")
	.dwattr $C$DW$1080, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$1081	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R5H")
	.dwattr $C$DW$1081, DW_AT_location[DW_OP_regx 0x40]
$C$DW$1082	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6HL")
	.dwattr $C$DW$1082, DW_AT_location[DW_OP_regx 0x43]
$C$DW$1083	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R6H")
	.dwattr $C$DW$1083, DW_AT_location[DW_OP_regx 0x44]
$C$DW$1084	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7HL")
	.dwattr $C$DW$1084, DW_AT_location[DW_OP_regx 0x47]
$C$DW$1085	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("R7H")
	.dwattr $C$DW$1085, DW_AT_location[DW_OP_regx 0x48]
$C$DW$1086	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RBL")
	.dwattr $C$DW$1086, DW_AT_location[DW_OP_regx 0x49]
$C$DW$1087	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RB")
	.dwattr $C$DW$1087, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$1088	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STFL")
	.dwattr $C$DW$1088, DW_AT_location[DW_OP_regx 0x27]
$C$DW$1089	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("STF")
	.dwattr $C$DW$1089, DW_AT_location[DW_OP_regx 0x28]
$C$DW$1090	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FPUHAZ")
	.dwattr $C$DW$1090, DW_AT_location[DW_OP_reg27]
$C$DW$1091	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$1091, DW_AT_location[DW_OP_regx 0x4e]
	.dwendtag $C$DW$CU

