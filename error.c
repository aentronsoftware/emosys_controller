/**********************************************************************

   error.c - error handling

 ----------------------------------------------------------------------
   Author: PTM: PT/RP                   Date: 03.04.12
   For   : PTM
   Compil: TI CCS5.2
   Target: PTM060
   descr.: handles system errors
   Edit  :

 *********************************************************************/


#include <stdio.h>
#include "uc.h"
#include "dspspecs.h"
#include "powerman.h"
#include "gateway_dict.h"
#include "error.h"
#include "param.h"
#include "hal.h"


/* ERROR PRIORITY concept :
   MAX priority errors = Serious hardware or software failures
   ===========================================================
   MAX priority errors cannot be overwritten, even not by another MAX priority
   MAX priority errors can overwrite MID and STD errors

   MID priority errors = Exception errors
   ======================================
   MID priority errors can only be overwritten by MAX priority errors
   MID priority errors can overwrite STD errors

   STD priority errors = Positionning & Safety errors
   ==================================================
   STD priority errors can be overwritten by any other error
   STD priority errors can overwrite STD errors
*/

/******************************************************************************
                      SERIOUS HARDWARE FAILURES
*******************************************************************************/

/* presently the current offset is measured during startup only. If bad the
   current regulation is not started at all so it is not necessary to stop
   it here. Keep in mind to change it if the offset test is made periodically */

/* overload on digital outputs STD */
void ERR_ErrorDigOvld(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_DIGOVLD;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_DIGOVLD);
}

/* overload on digital outputs STD */
void ERR_ISOLATION(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_ISO;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_ISO);
}

/* overload on digital outputs STD */
void ERR_CONTACTOR_PLUS(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_CONTACTOR_WELDED_PLUS;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_CONTACTOR_WELDED_PLUS);
}

void ERR_CONTACTOR_NEG(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_CONTACTOR_WELDED_NEGATIVE;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_CONTACTOR_WELDED_NEGATIVE);
}

void ERR_CONTACTOR_PRE(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_CONTACTOR_PRECHARGE;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_CONTACTOR_PRECHARGE);
}

void ERR_ErrorParam(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_PARAMINIT;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_PARAMINIT);
}

/******************************************************************************
                          EMERGENCY & SAFETY ERRORS
*******************************************************************************/
#ifdef OVERCUR_DETECT
/* absolute peak current limit exceeded STD */
void ERR_ErrorOverCurrent(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_OVERCURRENT;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_OVERCURRENT);
}
#endif

#ifdef TEMP_SENSOR
/* absolute maximum PCB temperature reached STD */
void ERR_ErrorOverTemp(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_TEMPERATURE;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_TEMPERATURE);
}
#endif

void ERR_ErrorComm(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_COMM;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_COMM);
}

void WARN_Insulation(void){
  //if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MAX)
 // {
 //    ODV_ErrorDsp_WarningNumber = WARN_INSULATION;
     //ODV_ErrorDsp_ErrorLevel = ERRPRIO_MAX;
 // }
 // ERR_HandleWarning(WARN_INSULATION);
}

void ERR_ErrorOverVoltage(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_OVERVOLTAGE;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_OVERVOLTAGE);
}

void ERR_ErrorUnderVoltage(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = ERR_UNDERVOLTAGE;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(ERR_UNDERVOLTAGE);
}

void ERR_DELTA_Voltage(void)
{
  if (ODV_ErrorDsp_ErrorLevel < ERRPRIO_MID)
  {
     ODV_ErrorDsp_ErrorNumber = Delta_ERR;
     ODV_ErrorDsp_ErrorLevel = ERRPRIO_STD;
  }
  ERR_SetError(Delta_ERR);
}

void ERR_SetError(uint32 error_code){
  ODV_Gateway_Errorcode |= error_code;
  ODV_Gateway_State |= GATEWAY_STATE_ERROR_BIT;
  ODV_MachineMode = GATEWAY_MODE_ERROR_IN;
  GpioDataRegs.GPADAT.bit.GPIO25 = 0;
}
/******************************************************************************
                             SOFTWARE RUNTIME ERRORS
*******************************************************************************/


/******************************************************************************
                          ERROR HANDLING FUNCTIONS
*******************************************************************************/

/*  Charger Error Data    */
void ChargerError(void) {
    /// ************** Start Charger Error Processing ************* ///
    if ((ODV_ChargerData_MasterErrorByte & BULKS) | (ODV_ChargerData_SlaveErrorByte & BULKS)){
    	ODV_ChargerData_ChargerError |=  BULKS;
    }else {ODV_ChargerData_ChargerError &= ~(BULKS);}

    if ((ODV_ChargerData_MasterErrorByte & WARNINGHV) | (ODV_ChargerData_SlaveErrorByte & WARNINGHV)){
    	ODV_ChargerData_ChargerError |=  WARNINGHV;
    }else {ODV_ChargerData_ChargerError &= ~(WARNINGHV);}

    if ((ODV_ChargerData_MasterErrorByte & LIMTEMP) | (ODV_ChargerData_SlaveErrorByte & LIMTEMP)){
    	ODV_ChargerData_ChargerError |=  LIMTEMP;
    }else {ODV_ChargerData_ChargerError &= ~(LIMTEMP);}

    if ((ODV_ChargerData_MasterErrorByte & WARNLIMIT) | (ODV_ChargerData_SlaveErrorByte & WARNLIMIT)){
    	ODV_ChargerData_ChargerError |=  WARNLIMIT;
    }else {ODV_ChargerData_ChargerError &= ~(WARNLIMIT);}

    if ((ODV_ChargerData_MasterErrorByte & ERRORLATCH) | (ODV_ChargerData_SlaveErrorByte & ERRORLATCH)){
    	ODV_ChargerData_ChargerError |=  ERRORLATCH;
    }else {ODV_ChargerData_ChargerError &= ~(ERRORLATCH);}
    /// **************  End Charger Error Processing  ************* ///
}


bool1 ERR_ClearError(void) {
/* Clear the ERR_ErrorNumber.
*/
  bool1 res = FALSE;
  if (ODV_Gateway_Errorcode == 0)
  {
    ODV_Gateway_State &= ~GATEWAY_STATE_ERROR_BIT;
    ODV_ErrorDsp_ErrorNumber = 0;
    ODV_ErrorDsp_ErrorLevel = 0;
    ODV_Statusword.AnyMode.Fault = 0;
    ODV_Statusword.AnyMode.SwitchOnDisabled = 1;
    ODV_Controlword.AnyMode.ResetFault = 0;
    GpioDataRegs.GPADAT.bit.GPIO25 = 1;
    /* clear error on programmed outputs */
    res = TRUE;
  }
  return(res);
}

UNS32 ClearErrorCallback(CO_Data* d, const indextable * indextable, UNS8 bSubindex, UNS8 access){
  if (access == DIC_WRITE){
    if (ODV_ErrorDsp_ErrorNumber == 0)
      ERR_ClearError();
    if (ODV_ErrorDsp_WarningNumber == 0)
      ERR_ClearWarnings();
  }
  return(OD_SUCCESSFUL);
}

/******************************************************************************
                             WARNING HANDLING
*******************************************************************************/

void ERR_HandleWarning(uint32 warningno)
{
  // Error output is not activated by this type of error
  ODV_ErrorDsp_WarningNumber = warningno;
  ODV_Statusword.AnyMode.Warning = 1;
  ODV_Gateway_Errorcode |= warningno;
  ODV_Gateway_State |= GATEWAY_STATE_WARNING_BIT;
}

bool1 ERR_ClearWarnings(void)
{
  ODV_ErrorDsp_WarningNumber = 0;
  ODV_Statusword.AnyMode.Warning = 0;
  ODV_Gateway_Errorcode &= 0xFF00FFFF; //0x7FC0FF;	//ODV_Gateway_Errorcode = 0; //
  ODV_Gateway_State &= ~GATEWAY_STATE_WARNING_BIT;
  return TRUE;
}

void ERR_ClearWarning(uint32 warningno){
  ODV_ErrorDsp_WarningNumber &= ~warningno;
  ODV_Gateway_Errorcode &= ~warningno;
  if (ODV_ErrorDsp_WarningNumber == 0) ERR_ClearWarnings();
}

/* END OF ERROR.C */
