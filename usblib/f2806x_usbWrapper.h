//###########################################################################
//
// FILE:   f2806x_usbWrapper.c
//
// TITLE:  Wrapper for interrupt functions and USB support pins.
//
//###########################################################################
// $TI Release: F2806x C/C++ Header Files and Peripheral Examples V136 $
// $Release Date: Apr 15, 2013 $
//###########################################################################


extern void USB_USB0DeviceIntHandler(void);
extern void USB_USB0HostIntHandler(void);
extern void USB_USB0DualModeIntHandler(void);
extern void USB_USBVBusIntHandler(void);
extern void USB_VBusIntInit(tDeviceInfo *psDevice);
extern void USB_USBPFLTIntHandler(void);
extern void USB_Unlock(void);


